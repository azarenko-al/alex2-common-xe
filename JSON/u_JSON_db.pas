unit u_JSON_db;

interface

uses
  Data.DB,    System.SysUtils,

  u_JSON

  ;

// type TDatasetFlags = set of (flAll, fl);


function db_Dataset_to_JSON(aDataset: TDataSet; aIsDataOnly: Boolean = True): string;



implementation



//------------------------------------------------------------------------
function db_Dataset_to_JSON(aDataset: TDataSet; aIsDataOnly: Boolean = True): string;
//------------------------------------------------------------------------

var
  I: Integer;
  oRecord: TJSONobject;
  oFeatures: TJSONlist;
  j: Integer;
  oField: TField;
  oRoot: TJSONobject;
  s: string;

begin

  if aIsDataOnly then
  begin
    oFeatures:=TJSONlist.Create;

  end else
  begin
    oRoot:= TJSONobject.Create;

    oRoot.Add('count', aDataset.RecordCount);

    oFeatures:=oRoot.AddList('items');

  end;



  aDataset.DisableControls;
  aDataset.First;


  with aDataset do
    while not EOF do
  begin
    oRecord:=oFeatures.AddObject;

    for I := 0 to FieldCount-1 do
    begin
      oField:=Fields[i];

      case oField.DataType of
        ftFloat,

        ftGUID,

        ftDateTime,

        ftBoolean,

        ftAutoInc,
        ftInteger,

        ftWideString,
        ftString:

                    begin
                      s:=FieldByName(oField.FieldName).AsString;

                      oRecord.Add( LowerCase( oField.FieldName), s);
                    end;


        ftMemo:
                    begin
                      s:=FieldByName(oField.FieldName).AsString;

                      oRecord.Add( LowerCase( oField.FieldName), s);
                    end;

      else
        raise Exception.Create('oField.DataType');

      end;


    end;

   // oRecord.Add('id', FieldByName('id').AsString );

    Next;
  end;


//  result := TJSON.GenerateText(oRoot);


 // for I := 0 to aDataset.  Count-1  do
  //  Items[i].GetJSON (oFeatures);


//  ShowMessage(result);


  aDataset.EnableControls;


  //obj: TJSONbase

  if aIsDataOnly then
  begin
    result := TJSON.GenerateText(oFeatures, True);
    FreeAndNil(oFeatures);
  end
  else
  begin
    result := TJSON.GenerateText(oRoot, True);
    FreeAndNil(oRoot);
  end;





end;



end.




{
        ftSmallint: ;
        ftInteger: ;
        ftWord: ;
        ftBoolean: ;
        ftFloat: ;
        ftCurrency: ;
        ftBCD: ;
        ftDate: ;
        ftTime: ;
        ftDateTime: ;
        ftBytes: ;
        ftVarBytes: ;
//        ftAutoInc: ;
        ftBlob: ;
        ftMemo: ;
        ftGraphic: ;
        ftFmtMemo: ;
        ftParadoxOle: ;
        ftDBaseOle: ;
        ftTypedBinary: ;
        ftCursor: ;
        ftFixedChar: ;
        ftWideString: ;
}

      else

