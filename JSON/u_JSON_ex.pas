unit u_JSON_ex;

interface

uses
  Variants,Classes,    Data.DBXJSON;

//   u_JSON;

function JSON_encode(aValues: array of Variant): string;

function JSON_ConvertString(AValue: string): string;

function JSON_ConvertString_new(AValue: string): string;

implementation

uses
  System.SysUtils;


function JSON_encode(aValues: array of Variant): string;
var
  I: Integer;
  iCount: Integer;
  js: TJSONobject;

//  oSL: TStringList;
  sParam: string;
  sValue: string;
begin
 // oSL:=TStringList.Create;

  Result:='';

  iCount:=(Length(aValues) div 2);

  for I := 0 to iCount - 1 do
  begin
    sParam:=aValues[i*2];
    sValue:=VarToStr( aValues[i*2+1]);

  //  oSl.Add( Format('"%s": "%s"', [sParam, sValue]));


    Result:=Result + Format('"%s": "%s"', [sParam, sValue]);

    if i<> (iCount - 1) then
      Result:=Result + ',' ;

  end;

 // oSL.QuoteChar:=' ';
 // oSL.Delimiter:=',';


 // Result:=oSL.CommaText;

  Result:=Format('{%s}', [Result]);

 // FreeAndNil(oSL);

end;



// ---------------------------------------------------------------
function JSON_ConvertString(AValue: string): string;
// ---------------------------------------------------------------
const
  ESCAPE = '\';
  // QUOTATION_MARK = '"';
  REVERSE_SOLIDUS = '\';
  SOLIDUS = '/';
  BACKSPACE = #8;
  FORM_FEED = #12;
  NEW_LINE = #10;
  CARRIAGE_RETURN = #13;
  HORIZONTAL_TAB = #9;
var
  I: Integer;
  AChar: Char;
  k: Integer;
  s: string;
begin
  Result := '';

//  AValue := UTF8Encode(AValue);



  for I := 1 to Length(AValue)  do
  begin
    AChar:=AValue[i];
//  end;

//  for AChar in AValue do
 // begin

    case AChar of
      // !! Double quote (") is handled by TJSONString
      // QUOTATION_MARK: Result := Result + ESCAPE + QUOTATION_MARK;
      REVERSE_SOLIDUS: Result := Result + ESCAPE + REVERSE_SOLIDUS;
      SOLIDUS: Result := Result + ESCAPE + SOLIDUS;
      BACKSPACE: Result := Result + ESCAPE + 'b';
      FORM_FEED: Result := Result + ESCAPE + 'f';
      NEW_LINE: Result := Result + ESCAPE + 'n';
      CARRIAGE_RETURN: Result := Result + ESCAPE + 'r';
      HORIZONTAL_TAB: Result := Result + ESCAPE + 't';
      else
      begin
        if (Integer(AChar) < 32) or (Integer(AChar) > 126) then
        begin
          k:=Integer(AChar);

          //� u0430 =  1072

          s:=ESCAPE + 'u' + IntToHex(Integer(AChar) - 224 +1072, 4);

          Result := Result + s;
        end
        else
          Result := Result + AChar;
      end;
    end;
  end;
end;



// ---------------------------------------------------------------
function JSON_ConvertString_new(AValue: string): string;
// ---------------------------------------------------------------
const
  ESCAPE = '\';
  // QUOTATION_MARK = '"';
  REVERSE_SOLIDUS = '\';
  SOLIDUS = '/';
//  BACKSPACE = #8;
//  FORM_FEED = #12;
//  NEW_LINE = #10;
//  CARRIAGE_RETURN = #13;
//  HORIZONTAL_TAB = #9;
var
  I: Integer;
  AChar: Char;
  k: Integer;
  s: string;
begin
  Result := '';

//  AValue := UTF8Encode(AValue);



  for I := 1 to Length(AValue)  do
  begin
    AChar:=AValue[i];
//  end;

//  for AChar in AValue do
 // begin

    case AChar of
      // !! Double quote (") is handled by TJSONString
      // QUOTATION_MARK: Result := Result + ESCAPE + QUOTATION_MARK;
      REVERSE_SOLIDUS: Result := Result + ESCAPE + REVERSE_SOLIDUS;
      SOLIDUS: Result := Result + ESCAPE + SOLIDUS;
   //   BACKSPACE: Result := Result + ESCAPE + 'b';
   ////   FORM_FEED: Result := Result + ESCAPE + 'f';
   //   NEW_LINE: Result := Result + ESCAPE + 'n';
   //   CARRIAGE_RETURN: Result := Result + ESCAPE + 'r';
 //     HORIZONTAL_TAB: Result := Result + ESCAPE + 't';
      else
      begin
//        if (Integer(AChar) < 32) or (Integer(AChar) > 126) then
        if  (Integer(AChar) > 126) then
        begin
          k:=Integer(AChar);

          //� u0430 =  1072

          s:=ESCAPE + 'u' + IntToHex(Integer(AChar) - 224 +1072, 4);

          Result := Result + s;
        end
        else
          Result := Result + AChar;
      end;
    end;
  end;
end;





end.
