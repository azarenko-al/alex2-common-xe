// Sample 1: how to generate text of an object and obtain object from text;
// how to add an field to object and get it back.
//
// Leonid Koninin, 02/03/2007 modified 14/03/2008

program sample1;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  u_JSON in 'u_JSON.pas';

var
  js: TJSONobject;
  ws: TJSONstring;
  s: String;
  i: Integer;

  js_1: TJSONobject;
  js_2: TJSONobject;

  l: TJSONlist;

begin
  
  js_1 := TJSONobject.Create;
  js_1.Add('aaaaaaaaaaaaa','hhhhhhhhhh');
  js_1.Add('a33333333aaaaaaaaaaaa','222hhhhhhhhhh');


  l:=TJSONlist.Create;

  js_2 := TJSONobject.Create;
  js_2.Add('aaaaaaaaaaaaa','hhhhhhhhhh');
  js_2.Add('a33333333aaaaaaaaaaaa','222hhhhhhhhhh');

  l.Add(js_2);
 // l.Add(js_2);
 // l.Add(js_2);   

  js_1.Add('aaa',l);
  

  js := TJSONobject.Create;
//  js.add('namestring', TlkJSONstring.Generate('namevalue'));
  js.Add('namestring',  'namevalue');
  js.Add('namestring1', 'namevalue2');
  js.Add('namestring1', js_1);

  
(*
  l:=TJSONlist.create;
  l.Add(js_1);
  l.Add(js_1);
  l.Add(js_1);
  l.Add(js_1);
*)


// get the text of object
  s := TJSON.GenerateText(js);
  writeln(s);
  writeln;
  writeln('more readable variant:');
// (ver 1.03+) generate readable text
  i := 0;
  s := GenerateReadableText(js,i);
  writeln(s);

  js.Free;
// restore object (parse text)
  js := TJSON.ParseText(s) as TJSONobject;
// and get string back
// old syntax
  ws := js.Field['namestring'] as TJSONstring;
  s := ws.Value;
  writeln(s);
// syntax of 0.99+
  s := js.getString('namestring');
  writeln(s);

  readln;
  js.Free;
end.
