unit u_dll;

interface
uses
  SysUtils,
  Windows;


//function GetInterface_BPL(const aBplFileName: string; var aHandle: Integer; const IID:
//    TGUID; var Obj): HResult;

function GetInterface_dll(const aDllFileName: string; var aHandle: Integer;
    const IID: TGUID; var Obj): HResult;

procedure UnloadPackage_dll(var aHandle: Integer);


implementation


// ---------------------------------------------------------------
function GetInterface_dll(const aDllFileName: string; var aHandle: Integer;
    const IID: TGUID; var Obj): HResult;
// ---------------------------------------------------------------
type
  tfunc = function(const IID: TGUID; var Obj): HResult; stdcall;
var
  func: tfunc;

//  p: PAnsiChar;

 // sProc: AnsiString;
begin
 // sProc:=aProcName;

 // p:=PAnsiChar(AnsiString(aProcName));

  Result:=S_FALSE;
  Integer(Obj):=0;

  if aHandle = 0 then
    aHandle:=LoadLibrary(PAnsiChar(aDllFileName));

  if aHandle >= 32 then
  begin
//    @func:=GetProcAddress(aHandle, PAnsiChar(aProcName)); //'DllGetInterface');
    @func:=GetProcAddress(aHandle, 'DllGetInterface');
//    @func:=GetProcAddress(aHandle, PAnsiChar(AnsiString(aProcName)));
    if Assigned(func) then
      Result:=func(IID,Obj)
    else
//      raise Exception.Create('function '+aProcName+'(const aDllFileName: string; var aHandle: Integer; const IID:');
      raise Exception.Create('function DllGetInterface(const aDllFileName: string; var aHandle: Integer; const IID:');
  end;

  if Result = S_FALSE then
     Exception.Create('function GetInterface_dll(const aDllFileName: string; var aHandle: Integer; const IID:');

end;



procedure UnloadPackage_dll(var aHandle: Integer);
begin
  if aHandle > 0 then
  begin
    FreeLibrary(aHandle);
    aHandle :=0;
  end;
end;



end.


(*

procedure TForm6.Button1Click(Sender: TObject);
var
  iPackageModule: HModule;
  AClass: TPersistentClass;
begin
  iPackageModule := LoadPackage('Link_Freq_report.bpl');

  if iPackageModule <> 0 then
  begin
    AClass := GetClass('TForm5');

    if AClass <> nil then
      with TComponentClass(AClass).Create(Application)
        as TCustomForm do
      begin
        ShowModal;
        Free;
      end;

    UnloadPackage(iPackageModule);
  end;
end;
*)



 {
     
// ---------------------------------------------------------------
function GetInterface_BPL(const aBplFileName: string; var aHandle: Integer; const IID:
    TGUID; var Obj): HResult;
// ---------------------------------------------------------------
type
  tfunc = function(const IID: TGUID; var Obj): HResult; stdcall;
var
  func: tfunc;
begin
  Result:=S_FALSE;
  Integer(Obj):=0;

  if aHandle = 0 then
    aHandle:=LoadPackage(aBplFileName);

  if aHandle > 0 then
  begin
    @func:=GetProcAddress(aHandle, 'DllGetInterface');
    if Assigned(func) then
      Result:=func(IID,Obj)
    else
      raise Exception.Create('function GetInterface_dll(const aDllFileName: string; var aHandle: Integer; const IID:');
  end;

  if Result = S_FALSE then
     Exception.Create('function GetInterface_dll(const aDllFileName: string; var aHandle: Integer; const IID:');

end;




