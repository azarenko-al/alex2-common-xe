unit fra_DB_login;

interface

uses SysUtils, Classes, Controls, Forms, Dialogs, StdCtrls, ExtCtrls,
  Db, ADODB, ActnList, Graphics,

//  i_db_login,

  d_Templates,

  d_DB_ConnectAdditionalParams_,
  dm_Connection,

  u_func,
  u_sql,

  Menus, System.Actions
  ;

type

  Tframe_DB_Login = class(TForm)
    lbLogin: TLabel;
    lbPass: TLabel;
    ed_Login: TEdit;
    ed_Password: TEdit;
    lbDatabase: TLabel;
    lbAuth: TLabel;
    cb_Database: TComboBox;
    lbServer: TLabel;
    ActionList1: TActionList;
    act_Ok: TAction;
    btn_Additional: TButton;
    cb_Server: TComboBox;
    Button1: TButton;
    act_Test: TAction;
    PopupMenu1: TPopupMenu;
    N11: TMenuItem;
    N21: TMenuItem;
    test21: TMenuItem;
    misha1: TMenuItem;
    gb_Authorization1: TGroupBox;
    rb_SQL_server: TRadioButton;
    rb_Windows: TRadioButton;
    onega2013rfpandlink1: TMenuItem;
    Button2: TButton;
    act_Template: TAction;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_TemplateExecute(Sender: TObject);
//    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//    procedure act_Database_Refresh111Execute(Sender: TObject);
//    procedure act_OkExecute(Sender: TObject);
    procedure act_TestExecute(Sender: TObject);
    procedure btn_AdditionalClick(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
  //  procedure Button3Click(Sender: TObject);
//    procedure btn_OkClick(Sender: TObject);
    procedure cb_Auth1Change(Sender: TObject);
    procedure cb_Authorization1Change(Sender: TObject);
    procedure cb_DatabaseDropDown(Sender: TObject);
    procedure cb_ServerChange(Sender: TObject);
    procedure cb_ServerDropDown(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ed_ServerButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//    procedure misha1Click(Sender: TObject);
//    procedure N11Click(Sender: TObject);
//    procedure N21Click(Sender: TObject);
//    procedure onega2013rfpandlink1Click(Sender: TObject);
//    procedure rg_AuthClick(Sender: TObject);
//    procedure test21Click(Sender: TObject);
  private
    FLoginRec: TdbLoginRec;

    procedure CheckConnect;
    procedure LoadEditsFromRec;
    procedure Refresh_Database_List;
    procedure SaveEditsToRec();
    procedure Select_Server_Name;
//    procedure Set1;
  //  procedure Set2;
    procedure Update_Auth;

  public
    IniFileName : string;

    IsModified : Boolean;

    MaxHeight : Integer;

    function GetLoginRec: TdbLoginRec;

    procedure SaveToIni(aIniFileName: string = '');
    procedure LoadFromIni(aIniFileName: string = '');

    function GetADOConnectionString: String;

//    class function ExecDlg(var aLoginRec: TdbLoginRec): boolean;
  end;



//=================================================================
implementation {$R *.DFM}      
//=================================================================


//-------------------------------------------------------------------
procedure Tframe_DB_Login.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
(*  FRegPath:=REGISTRY_COMMON_FORMS + ClassName + '\';

  FormStorage1.IniFileName:=FRegPath;
  FormStorage1.Active := True;
*)
  //Width:=493;

//  MaxHeight := 205;
  MaxHeight := 175 ;//+ 30;


  Caption:='����������� � ��';

  lbLogin.Caption   :='������������';
  lbPass.Caption    :='������';
  LbServer.Caption  :='������';
  lbDatabase.Caption:='���� ������';
  lbAuth.Caption    :='�����������';


//  cb_ShowOnStart.Caption:='���������� ��� ������';
  btn_Additional.Caption:='�������������';


  TdmConnection.Init;
end;


//--------------------------------------------------------------------
procedure Tframe_DB_Login.SaveEditsToRec;
//--------------------------------------------------------------------
begin
  with FLoginRec do
  begin
    Server      :=cb_Server.Text;

    if not rb_Windows.Checked then
    begin
      Login       :=ed_Login.Text;
      Password    :=ed_Password.Text
    end
    else begin
      Login       :='';
      Password    :='';
    end;

    Database    :=cb_Database.Text;

//    IsUseWinAuth:=cb_Authorization.ItemIndex=1;
    IsUseWinAuth:= rb_Windows.Checked;


  end;
end;

//--------------------------------------------------------------------
procedure Tframe_DB_Login.LoadEditsFromRec;
//--------------------------------------------------------------------
begin
  with FLoginRec do
  begin
    cb_Server.Text:=      Server;
    ed_Login.Text:=       Login;
    ed_Password.Text:=    Password;
    cb_Database.Text:=    Database;

//    cb_Authorization.ItemIndex:=IIF(IsUseWinAuth,1,0);

    rb_Windows.Checked := IsUseWinAuth;
  end;
end;


procedure Tframe_DB_Login.act_TestExecute(Sender: TObject);
begin
  if Sender=act_Test then
    CheckConnect;
end;


procedure Tframe_DB_Login.btn_AdditionalClick(Sender: TObject);
begin
  TDlg_DB_ConnectAdditionalParams_.ExecDlg(FLoginRec);
end;


procedure Tframe_DB_Login.cb_Auth1Change(Sender: TObject);
var
  b: Boolean;
begin
  b:=rb_SQL_server.Checked;

  ed_Login.Enabled:=b;
  ed_Password.Enabled:=b;

end;

procedure Tframe_DB_Login.cb_DatabaseDropDown(Sender: TObject);
begin
  refresh_Database_List;
end;

procedure Tframe_DB_Login.cb_ServerChange(Sender: TObject);
begin
  IsModified := True;
end;


procedure Tframe_DB_Login.cb_ServerDropDown(Sender: TObject);
begin
  if cb_Server.Items.Count=0 then
    db_ListAvailableSQLServers(cb_Server.Items);
end;

//----------------------------------------------------------------------
procedure Tframe_DB_Login.CheckConnect;
//----------------------------------------------------------------------
begin
  SaveEditsToRec();

 // TdmConnection.Init;
  dmConnection.CheckConnect(FLoginRec);
end;

//----------------------------------------------------------------------
procedure Tframe_DB_Login.ed_ServerButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//----------------------------------------------------------------------
begin
  Select_Server_Name();
end;


function Tframe_DB_Login.GetADOConnectionString: String;
begin
  Result := db_ADO_MakeConnectionString(GetLoginRec());
end;


function Tframe_DB_Login.GetLoginRec: TdbLoginRec;
begin
  SaveEditsToRec();

  Result := FLoginRec;
end;


//----------------------------------------------------------------------
procedure Tframe_DB_Login.Select_Server_Name;
//----------------------------------------------------------------------
var sName: string;
begin
  sName := db_Dlg_GetSqlServerName_old (Self.Handle);

  if sName<>'' then
  begin
    cb_Server.Text := sName;
    Refresh_Database_List;
  end;
end;


//-----------------------------------------------------------------
procedure Tframe_DB_Login.Refresh_Database_List;
//-----------------------------------------------------------------
begin
  SaveEditsToRec();

 // TdmConnection.Init;
  dmConnection.SQLServer_GetDatabaseList(FLoginRec, cb_Database.Items);

  if cb_Database.Items.IndexOf(cb_Database.Text)=-1 then
    cb_Database.Text:= '';
end;




procedure Tframe_DB_Login.Update_Auth;
var
  b: Boolean;
begin
//  b := cb_Authorization.ItemIndex=0;
  b := rb_SQL_server.Checked;

  ed_Login.Enabled:=b;
  ed_Password.Enabled:=b;

  ed_Login.Color   :=IIF(b, clWindow, Color);
  ed_Password.Color:=IIF(b, clWindow, Color);
end;



procedure Tframe_DB_Login.SaveToIni(aIniFileName: string = '');
begin
  if aIniFileName='' then
    aIniFileName:=IniFileName;

  SaveEditsToRec();

  db_LoginRec_SaveToIni1(aIniFileName, '', FLoginRec);
  
end;


procedure Tframe_DB_Login.LoadFromIni(aIniFileName: string = '');
begin
  if aIniFileName='' then
    aIniFileName:=IniFileName;

  db_LoginRec_LoadFromIni(aIniFileName, 'main',  FLoginRec);
  LoadEditsFromRec();
end;


procedure Tframe_DB_Login.cb_Authorization1Change(Sender: TObject);
begin
  Update_Auth
end;


// ---------------------------------------------------------------
procedure Tframe_DB_Login.ActionList1Update(Action: TBasicAction; var Handled:
    Boolean);
// ---------------------------------------------------------------
begin
  ed_Login.ReadOnly := rb_Windows.Checked;
  ed_Password.ReadOnly := rb_Windows.Checked;


  if not rb_Windows.Checked then
  begin
    ed_Login.Color :=clWindow;
    ed_Password.Color :=clWindow;
  end else
  begin
    ed_Login.ParentColor:=True;
    ed_Password.ParentColor:=True;

//    ComboEdit1.ParentColor:=True;

  end;
end;



procedure Tframe_DB_Login.act_TemplateExecute(Sender: TObject);
begin

 if Sender=act_Template then
  begin
//    SaveEditsToClass(rLogin);
    SaveEditsToRec;

    if Tdlg_Templates.ExecDlg(FLoginRec) then
    begin
   //  ShowMessage('1');

      LoadEditsFromRec();
//      LoadFromRec(rLogin);
    end;
  end;

end;



end.



