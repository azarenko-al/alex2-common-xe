object dlg_db_ViewDataset: Tdlg_db_ViewDataset
  Left = 467
  Top = 221
  Caption = 'View Dataset'
  ClientHeight = 285
  ClientWidth = 531
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 531
    Height = 209
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 266
    Width = 531
    Height = 19
    Panels = <>
    SimplePanel = True
    ExplicitTop = 278
    ExplicitWidth = 539
  end
  object DataSource1: TDataSource
    Left = 72
    Top = 72
  end
end
