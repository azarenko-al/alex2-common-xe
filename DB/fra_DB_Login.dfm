object frame_DB_Login: Tframe_DB_Login
  Left = 1218
  Top = 584
  Cursor = crArrow
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'frame_DB_Login'
  ClientHeight = 194
  ClientWidth = 601
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poDefault
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbLogin: TLabel
    Left = 5
    Top = 64
    Width = 26
    Height = 13
    Caption = 'Login'
    FocusControl = ed_Login
  end
  object lbPass: TLabel
    Left = 5
    Top = 86
    Width = 46
    Height = 13
    Caption = 'Password'
    FocusControl = ed_Password
  end
  object lbDatabase: TLabel
    Left = 5
    Top = 113
    Width = 46
    Height = 13
    Caption = 'Database'
    FocusControl = cb_Database
  end
  object lbAuth: TLabel
    Left = 5
    Top = 34
    Width = 22
    Height = 13
    Caption = 'Auth'
    WordWrap = True
  end
  object lbServer: TLabel
    Left = 5
    Top = 7
    Width = 31
    Height = 13
    Caption = 'Server'
  end
  object ed_Login: TEdit
    Left = 90
    Top = 58
    Width = 290
    Height = 21
    Cursor = crIBeam
    TabOrder = 1
    OnChange = cb_ServerChange
  end
  object ed_Password: TEdit
    Left = 90
    Top = 83
    Width = 290
    Height = 21
    Cursor = crIBeam
    TabOrder = 2
    OnChange = cb_ServerChange
  end
  object cb_Database: TComboBox
    Left = 90
    Top = 110
    Width = 290
    Height = 21
    DropDownCount = 30
    Sorted = True
    TabOrder = 3
    OnChange = cb_ServerChange
    OnDropDown = cb_DatabaseDropDown
  end
  object btn_Additional: TButton
    Left = 396
    Top = 37
    Width = 100
    Height = 23
    Caption = 'Additional'
    PopupMenu = PopupMenu1
    TabOrder = 4
    OnClick = btn_AdditionalClick
  end
  object cb_Server: TComboBox
    Left = 90
    Top = 3
    Width = 290
    Height = 21
    DropDownCount = 20
    Sorted = True
    TabOrder = 0
    OnChange = cb_ServerChange
    OnDropDown = cb_ServerDropDown
  end
  object Button1: TButton
    Left = 396
    Top = 5
    Width = 100
    Height = 23
    Action = act_Test
    TabOrder = 5
  end
  object gb_Authorization1: TGroupBox
    Left = 90
    Top = 25
    Width = 177
    Height = 30
    TabOrder = 6
    object rb_SQL_server: TRadioButton
      Left = 8
      Top = 10
      Width = 73
      Height = 17
      Caption = 'SQL Server'
      Checked = True
      TabOrder = 0
      TabStop = True
    end
    object rb_Windows: TRadioButton
      Left = 93
      Top = 10
      Width = 73
      Height = 17
      Caption = 'Windows'
      TabOrder = 1
    end
  end
  object Button2: TButton
    Left = 400
    Top = 80
    Width = 97
    Height = 25
    Action = act_Template
    TabOrder = 7
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 312
    Top = 136
    object act_Ok: TAction
      Caption = 'Ok'
    end
    object act_Test: TAction
      Caption = #1058#1077#1089#1090
      OnExecute = act_TestExecute
    end
    object act_Template: TAction
      Caption = 'Template'
      OnExecute = act_TemplateExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 264
    Top = 136
    object N11: TMenuItem
      Caption = 'server onega_link'
    end
    object N21: TMenuItem
      Caption = 'alex   onega_link_test1'
    end
    object test21: TMenuItem
      Caption = 'test2  onega_link_2012'
    end
    object misha1: TMenuItem
      Caption = 'MISHA\SQLEXPRESS'
    end
    object onega2013rfpandlink1: TMenuItem
      Caption = 'test 2 - onega_2013_rfp_and_link'
    end
  end
end
