object dlg_Templates: Tdlg_Templates
  Left = 1043
  Top = 676
  Caption = 'dlg_Templates'
  ClientHeight = 425
  ClientWidth = 664
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 664
    Height = 137
    Align = alTop
    DataSource = DataSource1
    PopupMenu = PopupMenu1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'server'
        Width = 134
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'database'
        Width = 109
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'login'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'password'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'auth'
        PickList.Strings = (
          'sql server'
          'windows')
        Width = 104
        Visible = True
      end>
  end
  object Button1: TButton
    Left = 32
    Top = 208
    Width = 75
    Height = 25
    Caption = 'load'
    TabOrder = 1
    Visible = False
  end
  object Button2: TButton
    Left = 32
    Top = 256
    Width = 75
    Height = 25
    Caption = 'save'
    TabOrder = 2
    Visible = False
  end
  object CheckBox1: TCheckBox
    Left = 183
    Top = 258
    Width = 97
    Height = 22
    Caption = 'Editing'
    TabOrder = 3
    Visible = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 384
    Width = 664
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 4
    object Panel2: TPanel
      Left = 480
      Top = 0
      Width = 184
      Height = 41
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btn_Ok: TButton
        Left = 14
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Ok'
        Default = True
        ModalResult = 1
        TabOrder = 0
      end
      object Button4: TButton
        Left = 94
        Top = 8
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        ModalResult = 2
        TabOrder = 1
      end
    end
    object FilenameEdit1: TFilenameEdit
      Left = 200
      Top = 12
      Width = 225
      Height = 21
      NumGlyphs = 1
      TabOrder = 1
      Text = 'd:\templates.ini'
      Visible = False
    end
    object Button5: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Action = act_Add
      TabOrder = 2
    end
  end
  object RxMemoryData1: TRxMemoryData
    Active = True
    FieldDefs = <
      item
        Name = 'server'
        Attributes = [faRequired]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'database'
        Attributes = [faRequired]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'login'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'password'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'auth'
        DataType = ftString
        Size = 20
      end>
    Left = 400
    Top = 168
    object RxMemoryData1server: TStringField
      DisplayWidth = 26
      FieldName = 'server'
      Required = True
      Size = 30
    end
    object RxMemoryData1database: TStringField
      DisplayWidth = 27
      FieldName = 'database'
      Required = True
      Size = 30
    end
    object RxMemoryData1login: TStringField
      DisplayWidth = 20
      FieldName = 'login'
      Required = True
    end
    object RxMemoryData1password: TStringField
      DisplayWidth = 24
      FieldName = 'password'
    end
    object RxMemoryData1auth: TStringField
      FieldName = 'auth'
    end
  end
  object DataSource1: TDataSource
    DataSet = RxMemoryData1
    Left = 400
    Top = 216
  end
  object ActionList1: TActionList
    Left = 280
    Top = 176
    object act_Add: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = act_DelExecute
    end
    object act_Select: TAction
      Caption = 'Select'
    end
    object act_Del: TAction
      Caption = 'Delete'
      OnExecute = act_DelExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 328
    Top = 288
    object actDel1: TMenuItem
      Action = act_Del
    end
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    UseRegistry = True
    Left = 552
    Top = 176
  end
end
