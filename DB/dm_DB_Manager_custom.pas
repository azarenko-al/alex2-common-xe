unit dm_DB_Manager_custom;

interface

uses
  SysUtils, Classes, ADODB, DB, Forms, Variants,

//  u_log,

//  u_kpt_const,

  
  u_db;


type
  TdmDB_Manager_custom = class(TDataModule)
    ADOQuery1: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    ADOCommand1: TADOCommand;
    ADOConnection1: TADOConnection;
//    procedure DataModuleCreate(Sender: TObject);
  private
    FADOConnectionRef :  TADOConnection;

//    function OpenStoredProc(aADOStoredProc: TADOStoredProc; aProcName: string;
 //       aParams: array of Variant): integer; overload;

    function ExecCommand(aSQL: string; aParamList: TdbParamList = nil): Boolean;
    procedure Log(aMsg: string);
    procedure SetADOConnection(const Value: TADOConnection);
  public
    function OpenStoredProc(aADOStoredProc: TADOStoredProc; aProcName: string;
        aParams: array of Variant): integer; overload;

                     
    function ExecStoredProc(aProcName: string; aParams: array of Variant): Integer;  overload;

    function ExecStoredProc(aADOStoredProc: TAdoStoredProc; aProcName: string;
        aParams: array of Variant): Integer; overload;

    procedure OpenQuery(aADOQuery: TADOQuery; aSQL : string); overload;
    function OpenQuery(aQuery : TADOQuery; aSQL : string; aParams : array of
        Variant): boolean; overload;

//    function UpdateRecordByID(aTableName : string; aID : integer; aParams: array of
 //       TDBParamRec): boolean; overload;
        
  //  function UpdateRecordByID_list(aTableName : string; aID : integer; aParamList:
   //     TdbParamList): boolean;
        

    function UpdateRecordByID(aTableName : string; aID : integer; aParams: array of
        Variant): Boolean;
  public
//    class procedure Init;



// TODO: Template_Site_select_item
//  procedure Template_Site_select_item(aADOStoredProc: TADOStoredProc; aID: Integer);

//    function TransferTech_Copy(aID: Integer; aName: string): integer;

  public


//    function UpdateRecordByID(aTableName : string; aID : integer; aParamList:
 //       TdbParamList): boolean; overload;

    property ADOConnection: TADOConnection read FADOConnectionRef write
        SetADOConnection;

  end;


//var
//  dmManager_custom: TdmManager_custom;
//
//                              


implementation



{$R *.dfm}
//
//const
//  FLD_PMP_CALC_REGION_ID = 'PMP_CALC_REGION_ID';
//

//
//const
//  FLD_GEOMETRY_STR = 'GEOMETRY_STR';
//  FLD_Map_ID = 'Map_ID';
//  
//  //FLD_GEOMETRY_LINE_STR ='GEOMETRY_LINE_STR';
////  FLD_GEOMETRY_REVERSE_STR = 'GEOMETRY_REVERSE_STR';
// // FLD_GEOMETRY_ERRORS = 'GEOMETRY_ERRORS';
//  
//
//  FLD_FILTER_ID = 'FILTER_ID';
//

//class procedure TdmManager_custom.Init;
//begin
//  Assert(not Assigned(dmManager));
//
//  Application.CreateForm(TdmManager, dmManager);
//end;
//
             


//----------------------------------------------------------------------------
function TdmDB_Manager_custom.UpdateRecordByID(aTableName : string; aID : integer;
    aParams: array of Variant): Boolean;
//----------------------------------------------------------------------------
var
  s: string;
  oParamList: TdbParamList;
begin
  Assert (aTableName<>'', 'function TDBManager.UpdateRecordByID - aTableName=''''');


  oParamList:= TdbParamList.Create;
  oParamList.LoadFromArr_variant(aParams);

  s:=db_MakeUpdateString(aTableName, aID, oParamList);

  Result := ExecCommand (s, oParamList);

  FreeAndNil(oParamList);

end;



//--------------------------------------------------------------------
function TdmDB_Manager_custom.ExecCommand(aSQL: string; aParamList: TdbParamList = nil):
    Boolean;
//--------------------------------------------------------------------
var i: integer;
  oParameter: TParameter;
  oStrStream: TStringStream;

  vValue: Variant;
   v: Variant; 
begin
  Assert(aSQL<>'');

  ADOCommand1.CommandText:=aSQL;

  if Assigned(aParamList) then
    for i:=0 to aParamList.Count-1 do
    begin
  //    sFieldName:=aParams[i*2];
    //  vValue    :=aParams[i*2+1];
          
      oParameter:=ADOCommand1.Parameters.FindParam (aParamList[i].FieldName);

      vValue:=aParamList[i].FieldValue;
      
      
      Assert(oParameter<>nil);
      
      if Assigned(oParameter) then 
      try
        if oParameter.DataType = ftString then
        begin
          oStrStream:=TStringStream.Create;
          oStrStream.Clear;
          oStrStream.WriteString(vValue);
          
//          oParameter.LoadFromStream(oStrStream, ftString);
          oParameter.LoadFromStream(oStrStream, ftMemo);

          v:=oParameter.Value;
        
//          oParameter.Value:= vValue;

          FreeAndNil(oStrStream);
          
        end else
          oParameter.Value:= vValue;
      
//        oParameter.Value :=aParamList[i].FieldValue;
      except
      end;
    end;

  try
    ADOCommand1.Execute;
    Result:=True;
  except
    Result:=False;
  end;

end;

// ---------------------------------------------------------------
function TdmDB_Manager_custom.ExecStoredProc(aProcName: string; aParams: array of
    Variant): Integer;
// ---------------------------------------------------------------
begin
//  Assert(Assigned(FADOConnectionRef), 'Value not assigned');

  Result := db_ExecStoredProc__new(ADOStoredProc1, aProcName, aParams,[]);

//  Result := db_ExecStoredProc_(FADOConnectionRef, aProcName, aParams);
end;

// ---------------------------------------------------------------
function TdmDB_Manager_custom.ExecStoredProc(aADOStoredProc: TAdoStoredProc; aProcName:
    string; aParams: array of Variant): Integer;
begin
  aADOStoredProc.Connection := FADOConnectionRef;

  Result := db_ExecStoredProc__new(aADOStoredProc, aProcName, aParams,[]);
end;





procedure TdmDB_Manager_custom.Log(aMsg: string);
begin
  //g_Log.SysMsg (aMsg);
end;

// ---------------------------------------------------------------
procedure TdmDB_Manager_custom.OpenQuery(aADOQuery: TADOQuery; aSQL : string);
// ---------------------------------------------------------------
begin
  aADOQuery.Connection := FADOConnectionRef;
  db_OpenQuery(aADOQuery, aSQL, []);
end;

function TdmDB_Manager_custom.OpenQuery(aQuery : TADOQuery; aSQL : string; aParams :
    array of Variant): boolean;
begin
  aQuery.Connection := FADOConnectionRef;
  db_OpenQuery(aQuery, aSQL, aParams);
end;



function TdmDB_Manager_custom.OpenStoredProc(aADOStoredProc: TADOStoredProc; aProcName:
    string; aParams: array of Variant): integer;
begin
  Assert(Assigned(ADOStoredProc1), 'Value not assigned');

  aADOStoredProc.Connection := FADOConnectionRef;

  Result := db_ExecStoredProc__new(aADOStoredProc, aProcName, aParams, [flOPenQuery]);
end;




procedure TdmDB_Manager_custom.SetADOConnection(const Value: TADOConnection);
begin
  FADOConnectionRef := Value;

  ADOQuery1.Connection      :=FADOConnectionRef;
  ADOStoredProc1.Connection :=FADOConnectionRef;
  ADOCommand1.Connection    :=FADOConnectionRef;

end;







end.

