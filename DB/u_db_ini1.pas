unit u_db_ini1;

interface
uses
  SysUtils, IniFiles,DB;

procedure db_LoadDataFromIni(DataSet: TDataSet; aIniFileName, aSection: string);
procedure db_SaveDataToIni(DataSet: TDataSet; aIniFileName, aSection: string);

implementation

// ---------------------------------------------------------------
procedure db_SaveDataToIni(DataSet: TDataSet; aIniFileName, aSection: string);
// ---------------------------------------------------------------
var
  I: Integer;
  oField: TField;
  s: string;
  sParam: string;
  sValue: string;
  v: Variant;
begin
  with TIniFile.Create (aIniFileName) do
  begin
    EraseSection(aSection);

    WriteInteger(aSection, 'Count', DataSet.RecordCount);

    DataSet.First;



    with DataSet do
      while not EOF do
    begin

      for I := 0 to Fields.Count - 1 do
      begin
        oField := Fields[i];

        sValue:=DataSet.FieldByName(oField.FieldName).AsString;

     //   sValue:=

        sParam:= IntToStr(DataSet.RecNo-1) + '_'+ oField.FieldName;

        WriteString(aSection, sParam, sValue);
      end;


      Next;
    end;


    Free;
  end;
end;

// ---------------------------------------------------------------
procedure db_LoadDataFromIni(DataSet: TDataSet; aIniFileName, aSection: string);
// ---------------------------------------------------------------
var
  I: Integer;
  iCount: Integer;
  k: Integer;
  oField: TField;
  sParam: string;
  sValue: string;
begin
//  TIniFile.Create (aIniFileName).

  with TIniFile.Create (aIniFileName) do
  begin
   //                  
    iCount:=ReadInteger(aSection, 'Count', 0);

    for k := 0 to iCount - 1 do
    begin
      DataSet.Append;

      for I := 0 to DataSet.Fields.Count - 1 do
      begin
        oField := DataSet.Fields[i];

        sParam:= IntToStr(k) + '_'+ oField.FieldName;

  //      if oField.Name<>oField.DisplayLabel then

      //  oField.DisplayLabel :=
        sValue:=  ReadString(aSection, sParam, '');

        DataSet.FieldByName(oField.FieldName).AsString := sValue;
      end;

      DataSet.Post;

    end;

    Free;
  end;
end;




end.
