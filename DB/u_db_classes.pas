unit u_db_classes;

interface

uses
  Classes,

  u_DB;

type
  TdbParamList = class(TList)
  private
    function GetItems(Index: Integer): TdbParamObj;
  public
    function AddItem(aFieldName : string; aFieldValue: Variant): TdbParamObj;
    function FindBYFieldName(aFieldName : string): TdbParamObj;


    function MakeArr: TdbParamRecArr;

    function MakeInsertSQL(aTableName: string): string;
    function MakeSelectCountSQL(aTableName: string): string;
    function MakeSelectSQL(aTableName, aFieldName: string): string;

    function GetValueByName(aFieldName : string): Variant;
    procedure SetValueByName(aFieldName : string; aValue: Variant);

    property Items[Index: Integer]: TdbParamObj read GetItems; default;

    property FieldValues[aFieldName : string]: Variant read GetValueByName write
        SetValueByName; 

  end;

implementation

function TdbParamList.AddItem(aFieldName : string; aFieldValue: Variant):
    TdbParamObj;
//var
//  oParam: TdbParamObj;

begin
//  oParam :=FindBYFieldName (aFieldName);


  Result := TdbParamObj.Create;

  if Eq(aFieldName, 'linkend_id') then
    aFieldName :=aFieldName;



  Result.FieldName :=aFieldName;
  Result.FieldValue :=aFieldValue;

  Add(Result);
end;

function TdbParamList.FindBYFieldName(aFieldName : string): TdbParamObj;
var
  I: Integer;
begin
  Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].FieldName, aFieldName) then
    begin
      result := Items[i];
      exit;
    end;
end;

function TdbParamList.GetValueByName(aFieldName : string): Variant;
var
  oParam: TdbParamObj;
begin
  oParam :=FindBYFieldName (aFieldName);

  if Assigned(oParam) then
    Result := oParam.FieldValue
  else
    Result := null;
end;

procedure TdbParamList.SetValueByName(aFieldName : string; aValue: Variant);
var
  oParam: TdbParamObj;

begin

//  if Eq(aFieldName, 'property_id') then
//    Assert( AsInteger(aValue) > 0 );


  //if Eq(aFieldName, 'link_id') then
 //   Assert( AsInteger(aValue) > 0 );


  oParam :=FindBYFieldName (aFieldName);

  if Assigned(oParam) then
    oParam.FieldValue :=aValue
  else
    AddItem (aFieldName, aValue);

end;

(*
function TdbParamList.Validate;
var
  I: Integer;
begin
  Result := null;

  for I := 0 to Count - 1 do
    if Eq(Items[i].FieldName, aFieldName) then
    begin
      result := Items[i].FieldValue;
      exit;
    end;
end;
*)


function TdbParamList.GetItems(Index: Integer): TdbParamObj;
begin
  Result := TdbParamObj(inherited Items[Index]);
end;

function TdbParamList.MakeArr: TdbParamRecArr;
var
  I: Integer;
begin
  SetLength(Result, Count);

  for I := 0 to Count - 1 do
  begin
    Result[i].FieldName  := Items[i].FieldName;
    Result[i].FieldValue := Items[i].FieldValue;
  end;

//  Result := ;
  // TODO -cMM: TdbParamList.MakeArr default body inserted
end;

// ---------------------------------------------------------------
function TdbParamList.MakeInsertSQL(aTableName: string): string;
// ---------------------------------------------------------------
var
  sParamStr,sValueStr: string;
  i: Integer;
begin
  sParamStr:='';
  sValueStr:='';

  for i:=0 to Count-1 do
  //  if not VarIsNull (Items[i].FieldValue)  then
  begin
    sParamStr:=sParamStr + Format(',[%s]',[Items[i].FieldName]);
    sValueStr:=sValueStr + Format(',:%s', [Items[i].FieldName]);
  end;

  Result:=Format('INSERT INTO %s (%s) VALUES (%s)', [aTableName,
      Copy(sParamStr,2,100000),  Copy(sValueStr,2,100000)]);

end;

// ---------------------------------------------------------------
function TdbParamList.MakeSelectSQL(aTableName, aFieldName: string): string;
// ---------------------------------------------------------------
var
  sWhere: string;
  i: Integer;
begin
  sWhere:='';

  for i:=0 to Count-1 do
    if Items[i].FieldValue=null then
      sWhere:=sWhere + Format(' and ([%s] IS NULL)', [Items[i].FieldName])
    else
      sWhere:=sWhere + Format(' and ([%s]=:%s)', [Items[i].FieldName, Items[i].FieldName]);

  sWhere:=Copy(sWhere,6,1000000);

  Result:=Format('SELECT %s FROM %s WHERE %s ', [aFieldName, aTableName, sWhere]);

end;

// ---------------------------------------------------------------
function TdbParamList.MakeSelectCountSQL(aTableName: string): string;
// ---------------------------------------------------------------
var
  sWhere: string;
  i: Integer;
begin
  sWhere:='';

  for i:=0 to Count-1 do
    sWhere:=sWhere + Format(',[%s]=:s', [Items[i].FieldName, Items[i].FieldName]);

  Result:=Format('SELECT Count(*) FROM %s WHERE %s ', [aTableName,
      Copy(sWhere,2,1000000)]);

end;

end.
