unit I_DB_login;

interface

{$DEFINE test11}

uses  ADOInt
  ;

type

  TdbLoginRec = packed record
    Server         : ShortString;
    DataBase       : ShortString;
    Login          : ShortString;
    Password       : ShortString;

    IsUseWinAuth   : boolean;  //win or SQL server

    NetworkLibrary : ShortString;
    NetworkAddress : ShortString;

    ConnectionStatusStr : ShortString;

    //results
    Results: packed record
               Is_DB_owner      : Boolean;
               UserName         : ShortString;
              // DatabaseListText : WideString;
                   
             end;
  end;


  TDBLoginOption = (loFromReg, loShowDlg);
  TDBLoginOptions = set of TDBLoginOption;


  IDBLoginX = interface(IInterface)
  ['{8C95F13A-660E-4D15-B285-A11A90E8BFA8}']

    procedure InitAppHandle(aAppHandle: Integer);stdcall;

    function ExecDlg(var aLoginRec: TdbLoginRec; aAppHandle: Integer): Boolean; stdcall;

    function Open(const aRegPath: ShortString; var aLoginRec: TdbLoginRec; aOptions:
        TDBLoginOptions; aConnectionObject: _Connection; aAppHandle: Integer): boolean;
        stdcall;

    function OpenDatabase_(var aLoginRec: TdbLoginRec; aConnectionObject:_Connection): Boolean; stdcall;

    function DatabaseExists(aRec: TdbLoginRec; aDatabaseName: ShortString): Boolean; stdcall;


  //  procedure Test(aConnectionObject:_Connection);stdcall;

  end;


{$IFNDEF test}
  function db_ADO_MakeConnectionString(aRec: TdbLoginRec): WideString; stdcall;
{$ENDIF}


  function Get_IDBLoginX: IDBLoginX;

//  function db_DatabaseExists(aRec: TdbLoginRec; aDatabaseName: WideString): Boolean; stdcall;



implementation


{$IFDEF test}
uses
  x_db_login;
{$ELSE}

const
  DEF_DLL = 'dll_db_login.dll';

//  function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall; external
  //   DEF_DLL;

  function db_ADO_MakeConnectionString(aRec: TdbLoginRec): WideString; stdcall; external DEF_DLL;
 // function db_DatabaseExists(aRec: TdbLoginRec; aDatabaseName: WideString): Boolean; external DEF_DLL;

  function DllGetInterface_IDBLoginX (var Obj): HResult; stdcall; external DEF_DLL;


{$ENDIF}

{$IFDEF test}

{$ELSE}

{$ENDIF}

var
  IDBLogin: IDBLoginX;



var
  LHandle : Integer;


function IDBLogin_Load: Boolean;
begin

{$IFDEF test}
/////  Result := x_db_login.GetInterface (IDBLoginX, IDBLogin)=0;
{$ELSE}
//  Result := DllGetInterface (IDBLoginX, IDBLogin)=0;

  Result := DllGetInterface_IDBLoginX (IDBLogin)=0;


//  Result := GetInterface_dll(DEF_DLL, LHandle, IDBLoginX, IDBLogin)=0;
{$ENDIF}

  Assert(Assigned(IDBLogin), 'IDBLogin not assigned');

  Result := Assigned(IDBLogin);
end;


procedure IDBLogin_UnLoad;
begin
  IDBLogin := nil;

{$IFNDEF test}
 // UnloadPackage_dll(LHandle);
{$ENDIF}
end;


function Get_IDBLoginX: IDBLoginX;
var
  b: boolean;
begin
{$IFDEF test}
//  x_db_login.DllGetInterface (IDBLoginX, Result);
  x_db_login.DllGetInterface_IDBLoginX (Result);

{$ELSE}
//  DllGetInterface (IDBLoginX, Result);

  b := DllGetInterface_IDBLoginX (Result)=0;

 // GetInterface_dll(DEF_DLL, LHandle, IDBLoginX, Result);
//  if Assigned(Result) then
 //   Result.InitAppHandle(Application.Handle);

{$ENDIF}
  Assert(Assigned(Result), 'IDBLogin not assigned');

end;


end.



