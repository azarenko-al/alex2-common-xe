﻿unit u_sql;

interface
uses
  Windows, Sysutils, classes,  Registry, Dialogs,IniFiles,
  DB, ADODB, ActiveX, ComObj, AdoInt, OleDB,

  u_db,
  u_func;

//  I_DB_login;

type
  TdbLoginRec = packed record
    Server:     ShortString;
    DataBase:   ShortString;

    Login:      ShortString;
    Password:   ShortString;

    IsUseWinAuth: boolean;  //win or SQL server

    NetworkLibrary: ShortString;
    NetworkAddress: ShortString;


    Results:  record
               Is_DB_owner      : Boolean;
               UserName         : String;
              // DatabaseListText : WideString;
                   
             end;

    //ConnectionString : string;
             
    
  //  User1: ShortString;

    //extended

//    DynamicPort: Boolean;
 //   PortNumber: integer;


   // DefaultDB:   string;
  end;



  function db_ADO_MakeConnectionString(aRec: TdbLoginRec): WideString; stdcall;

  //-----------------------------
  //
  //-----------------------------
//  function db_GetLocalComputerName: string;
  procedure db_ListAvailableSQLServers(aNames: TStrings);

//  function db_Dlg_GetSqlServerName_old(AHandle: THandle): string;


  function db_Dlg_GetSqlServerName_old(AHandle: THandle): string;



  //-----------------------------
  function db_OpenLoginRec(aADOConnection: TADOConnection; var aLoginRec: TdbLoginRec):
     boolean;

  function db_GetConnectionStatusStr(aLoginRec: TdbLoginRec):  String;  //; aLanguage: string = 'ru'

  //-----------------------------
  procedure db_LoginRec_SaveToReg(aRegPath: String; aDBLoginRec: TdbLoginRec);
  function db_LoginRec_LoadFromReg(aRegPath: String; var aDBLoginRec: TdbLoginRec): Boolean;

  function db_LoginRec_LoadFromIni(aIniFileName, aSection: String; var aRec:
      TdbLoginRec): Boolean;
  procedure db_LoginRec_SaveToIni1(aIniFileName, aSection: String; aDBLoginRec:
      TdbLoginRec);

  function db_OpenADOConnectionFromIni(aADOConnection: TADOConnection; aIniFileName:
      String; var aConnectionString: String): Boolean;


function db_OpenFromReg(aRegPath: String; var aLoginRec: TdbLoginRec; aConnectionObject:
    _Connection): boolean;

function db_GetConnectionStatusStr_new111111(aLoginRec: TdbLoginRec): String;


  function db_OpenFromReg_(aADOConnection: TADOConnection; aRegPath: String ): boolean;

function db_OpenFromIni(aIniPath, aSection: String; var aLoginRec: TdbLoginRec;
    aConnectionObject: _Connection): boolean;




exports
  db_ADO_MakeConnectionString;


implementation


// ---------------------------------------------------------------
function db_OpenADOConnectionString(aADOConnection: TADOConnection; aConnectionString:
    string): boolean;
// ---------------------------------------------------------------
begin
  with aADOConnection do
  begin
    Close;
    ConnectionString:=aConnectionString;
    try
      Open;
    except
    end;

    Result := Connected;
  end;
end;



//------------------------------- -------------------------------------
function db_OpenLoginRec(aADOConnection: TADOConnection; var aLoginRec: TdbLoginRec):
    boolean;
//--------------------------------------------------------------------
//var sConn: string;
begin
//  sConn:=db_ADO_MakeConnectionString (aLoginRec);
  if (aLoginRec.Server='') or (aLoginRec.DataBase='') then
  begin
    Result := False;
    Exit;
  end;

  aADOConnection.LoginPrompt:=False;


  aADOConnection.Close;
  aADOConnection.ConnectionString:=db_ADO_MakeConnectionString (aLoginRec);
  aADOConnection.DefaultDatabase:=aLoginRec.DataBase;

  try
    aADOConnection.Open;
////////    aLoginRec.User1:= db_GetLoggedUserName(aADOConnection);

  except
    ShowMessage ('Не удаётся подключиться к базе данных.');
  end;

  Result:= aADOConnection.Connected;

end;


//--------------------------------------------------------------
function db_GetLocalComputerName1111: string;
//--------------------------------------------------------------
var
  PRes  : PChar;
  bRes  : boolean;
  iSize : DWORD;
begin
  iSize := MAX_COMPUTERNAME_LENGTH + 1;
  PRes := StrAlloc(iSize);
  BRes := GetComputerName(PRes, iSize);
  if BRes then
    Result := StrPas(PRes);

  StrDispose(PRes);

{
  if Eq(Result, 'local')     or Eq(Result, '(local)') or
     Eq(Result, '127.0.0.1') or Eq(Result, sServerName)
  then
    Result := '(local)';
}

end;

//
////----------------------------------------------------------------------
////  Диалог выбора SQL серверов, находящихся в сети
////----------------------------------------------------------------------
//function db_Dlg_GetSqlServerName_old(AHandle: THandle): string;
////----------------------------------------------------------------------
//type
//  TServerBrowseDialogA0 = function(hwnd: HWND; pchBuffer: Pointer; cchBufSize: DWORD) : bool; stdcall;
//
//var
//  i: integer;
//  ServerBrowseDialogA0 : TServerBrowseDialogA0;
//  LANMAN_DLL : DWORD;
//  buffer : array[0..1024] of char;
//  bLoadLib : Boolean;
//  sCompName: string;
//  sServerName: string;
//begin
//  bLoadLib := False;
//  LANMAN_DLL := GetModuleHandle('NTLANMAN.DLL');
//
//  if LANMAN_DLL = 0 then
//  begin
//    LANMAN_DLL := LoadLibrary('NTLANMAN.DLL');
//    bLoadLib := True;
//  end;
//
//  if LANMAN_DLL <> 0 then begin
//    @ServerBrowseDialogA0 := GetProcAddress(LANMAN_DLL, 'ServerBrowseDialogA0');
//
//    ServerBrowseDialogA0 (AHandle, @buffer, 1024);
//
//    if buffer[0] = '\' then
//      sServerName := buffer;
//
//    if bLoadLib then FreeLibrary(LANMAN_DLL);
//
//    if Length(sServerName) > 2 then
//      for i := 2 downto 1 do
//        if sServerName[i] = '\' then
//          Delete(sServerName, i, 1);
//  end;
//
//
//  sCompName:= db_GetLocalComputerName();
//
//  if Eq(sServerName, 'local')     or Eq(sServerName, '(local)') or
//     Eq(sServerName, '127.0.0.1') or Eq(sServerName, sCompName)
//  then
//    sServerName := '(local)';
//
//  Result:=sServerName;
//
//  //ed_Server.Text := sStr;
//
//end;


// ---------------------------------------------------------------
procedure db_ListAvailableSQLServers(aNames: TStrings);
// ---------------------------------------------------------------
// http://delphi.about.com/od/sqlservermsdeaccess/l/aa090704a.htm
// ---------------------------------------------------------------
var
  RSCon: ADORecordsetConstruction;
  Rowset: IRowset;
  SourcesRowset: ISourcesRowset;
  SourcesRecordset: _Recordset;
  SourcesName, SourcesType: TField;

    function PtCreateADOObject
             (const ClassID: TGUID): IUnknown;
    var
      Status: HResult;
      FPUControlWord: Word;
    begin
      asm
        FNSTCW FPUControlWord
      end;
      Status := CoCreateInstance(
                  CLASS_Recordset,
                  nil,
                  CLSCTX_INPROC_SERVER or
                  CLSCTX_LOCAL_SERVER,
                  IUnknown,
                  Result);
      asm
        FNCLEX
        FLDCW FPUControlWord
      end;
      OleCheck(Status);
    end;
begin
  aNames.Clear;


  SourcesRecordset :=
      PtCreateADOObject(CLASS_Recordset)
      as _Recordset;
  RSCon :=
      SourcesRecordset
      as ADORecordsetConstruction;
  SourcesRowset :=
      CreateComObject(ProgIDToClassID('SQLOLEDB Enumerator'))
      as ISourcesRowset;
  OleCheck(SourcesRowset.GetSourcesRowset(
       nil,
       IRowset, 0,
       nil,
       IUnknown(Rowset)));

  RSCon.Rowset := RowSet;
  with TADODataSet.Create(nil) do
  try
    Recordset := SourcesRecordset;
    SourcesName := FieldByName('SOURCES_NAME');
    SourcesType := FieldByName('SOURCES_TYPE');
    aNames.BeginUpdate;

    try
      while not EOF do
      begin
        if (SourcesType.AsInteger = DBSOURCETYPE_DATASOURCE)
           and (SourcesName.AsString <> '')
        then
          aNames.Add(SourcesName.AsString);

        Next;
      end;

    finally
      aNames.EndUpdate;
    end;
  finally
    Free;
  end;
end;

//----------------------------------------------------------------------------
function db_ADO_MakeConnectionString_old(aRec: TdbLoginRec): WideString;
//----------------------------------------------------------------------------
//Provider=SQLNCLI11;Server=myServerAddress;Database=myDataBase;Uid=myUsername;Pwd=myPassword;
//Provider=SQLNCLI11;Server=myServerAddress;Database=myDataBase;Trusted_Connection=yes;
const
//  DEFAULT_NETWORK_LIB = 'DBMSSOCN';

//  Provider=SQLOLEDB.1;Integrated Security=SSPI;
//Persist Security Info=False;Initial Catalog=onega_13;
//Data Source=MAXSELECT

  CONNECTION_STRING_WINDOWS_AUTH =
    'Provider=SQLOLEDB.1;'+
    'Integrated Security=SSPI;'+
    'Persist Security Info=False;'+
    'Initial Catalog=%s;'+    //Database
    'Data Source=%s;';        //Server

  //  'Data Source=%s;';      //Server
 //  // +
    //'Initial Catalog=%s;';

{
   CONNECTION_STRING_SQL_AUTH_LOCAL =
  //  'Provider=MSDASQL.1;'+
    'Provider=SQLOLEDB.1;'+
    'Persist Security Info=True;'+
//    'Persist Security Info=False;'+
    'Initial Catalog=%s;'+   //Database
    'Data Source=%s;'+       //Server
    'User ID=%s;'+
    'Password=%s;';
    //Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ID=sa;Initial Catalog=onega_link;Data Source=server1
 }

 CONNECTION_STRING_SQL_AUTH =
//    'Provider=MSDASQL.1;'+
    'Provider=SQLOLEDB.1;'+
//    'Network Library=%s;'+
//    'Network Address=%s;'+
    'Integrated Security=SSPI;'+
    'Persist Security Info=False;'+
//        'Persist Security Info=True;'+
    'Use Encryption for Data=False;'+
    'Initial Catalog=%s;'+    //Database
    'Data Source=%s;'+        //Server
    'User ID=%s;'+
    'Password=%s;';

    //Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;Initial Catalog=onega_link;Data Source=server1;Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=ALEX2;Use Encryption for Data=False;Tag with column collation when possible=False;

var
  sLocalCompName: string;

begin
  with aRec do
  begin
    if IsUseWinAuth then
    begin
//      if NetworkLibrary='' then
//        NetworkLibrary:=DEFAULT_NETWORK_LIB;

//      if NetworkAddress='' then
//        NetworkAddress:=Server;

      Result:= Format(CONNECTION_STRING_WINDOWS_AUTH,
                    [//NetworkLibrary, NetworkAddress,
                     Database,Server,Login,Password]); //Database,

    end
    else begin
//      sLocalCompName:= db_GetLocalComputerName();
//
//      if Eq(Server, 'local')       or
//         Eq(Server, '(local)')     or
//         Eq(Server, '127.0.0.1')   or
//         Eq(Server, sLocalCompName) or
//         Eq(NetworkLibrary, DEFAULT_NETWORK_LIB)
//      then
//        Result:= Format(CONNECTION_STRING_SQL_AUTH_LOCAL,
//                    [Database, Server, Login, Password])
//      else

      Result:=Format(CONNECTION_STRING_SQL_AUTH, [Database, Server, Login, Password]);

    end;

  end;

  //Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ID=sa;Initial Catalog=onega_link;Data Source=server1
end;

//----------------------------------------------------------------------------
function db_ADO_MakeConnectionString(aRec: TdbLoginRec): WideString;
//----------------------------------------------------------------------------
//Provider=SQLNCLI11;Server=myServerAddress;Database=myDataBase;Uid=myUsername;Pwd=myPassword;
//Provider=SQLNCLI11;Server=myServerAddress;Database=myDataBase;Trusted_Connection=yes;
const


  DEF_CONNECTION_STRING_WINDOWS_AUTH =
    'Provider=SQLNCLI11;'+
    'Trusted_Connection=yes;'+
    'Database=:Database;'+
    'Server=:Server;';

  DEF_CONNECTION_STRING_SQL_AUTH =
    'Provider=SQLNCLI11;'+
    'Database=:Database;'+
    'Server=:Server;'+
    'Uid=:Uid;'+
    'Pwd=:Pwd;';

var
  sLocalCompName: string;

begin
  with aRec do
  begin
    if IsUseWinAuth then
      Result:=DEF_CONNECTION_STRING_WINDOWS_AUTH //Database,
    else
      Result:=DEF_CONNECTION_STRING_SQL_AUTH;


    Result:=ReplaceStr(Result, ':Server', Server);
    Result:=ReplaceStr(Result, ':Database', Database);
    Result:=ReplaceStr(Result, ':Uid', Login);
    Result:=ReplaceStr(Result, ':Pwd', Password);

  end;


end;


//--------------------------------------------------------------------
function db_LoginRec_LoadFromReg(aRegPath: String; var aDBLoginRec: TdbLoginRec): Boolean;
//--------------------------------------------------------------------
begin
 /////////// FillChar(aDBLoginRec, SizeOf(aDBLoginRec), 0);

  with TRegIniFile.Create(aRegPath), aDBLoginRec do
  begin

    Server        := ReadString ('', 'Server',     Server);
    DataBase      := ReadString ('', 'DataBase',   DataBase);
    Login         := ReadString ('', 'Login',      Login);
    Password      := ReadString ('', 'Password',   Password);
    IsUseWinAuth  := ReadBool   ('', 'UseWinAuth', IsUseWinAuth);
    NetworkLibrary:= ReadString ('', 'NetworkLibrary', NetworkLibrary);
    NetworkAddress:= ReadString ('', 'NetworkAddress', NetworkAddress);

(*
    Server        := ReadString ('', 'Server',   '');
    DataBase      := ReadString ('', 'DataBase', '');
    Login         := ReadString ('', 'Login',    '');
    Password      := ReadString ('', 'Password',   '');
    IsUseWinAuth  := ReadBool   ('', 'UseWinAuth', False);
    NetworkLibrary:= ReadString ('', 'NetworkLibrary', 'DBNMPNTW');
    NetworkAddress:= ReadString ('', 'NetworkAddress', Server);
*)

  //  if Login='' then Login:='sa';

    Free;
  end;

  Result := (aDBLoginRec.Server <> '') and (aDBLoginRec.DataBase <> '');
end;

//--------------------------------------------------------------------
//procedure db_LoginRec_LoadFromIni(aIniFileName: String; var aRec: TdbLoginRec);
function db_LoginRec_LoadFromIni(aIniFileName, aSection: String; var aRec: TdbLoginRec): Boolean;
//--------------------------------------------------------------------
const
  DEF_MAIN='main';
  
begin
  Result:=False;

  if aSection='' then
    aSection:=DEF_MAIN;


  FillChar(aRec, SizeOf(aRec), 0);


  if not FileExists(aIniFileName) then
    exit;

  with TIniFile.Create(aIniFileName), aRec do
  begin
    Server        := ReadString (aSection, 'Server',     Server);
    DataBase      := ReadString (aSection, 'DataBase',   DataBase);
    Login         := ReadString (aSection, 'Login',      Login);
    Password      := ReadString (aSection, 'Password',   Password);
    IsUseWinAuth  := ReadBool   (aSection, 'UseWinAuth', IsUseWinAuth);

//    NetworkLibrary:= ReadString (aSection, 'NetworkLibrary', 'DBNMPNTW');
//    NetworkAddress:= ReadString (aSection, 'NetworkAddress', Server);

    NetworkLibrary:= ReadString (aSection, 'NetworkLibrary', '');
    NetworkAddress:= ReadString (aSection, 'NetworkAddress', '');

    Free;
  end;


  Result:=True;
  
 // else
  //  raise Exception.Create('procedure db_LoginRec_LoadFromIni(aIniFileName, aSection: String; var aRec: TdbLoginRec);');
end;


//--------------------------------------------------------------------
procedure db_LoginRec_SaveToReg(aRegPath: String; aDBLoginRec: TdbLoginRec);
//--------------------------------------------------------------------
begin
  with TRegIniFile.Create (aRegPath), aDBLoginRec do
  begin
    WriteString ('', 'Server',     Server);
    WriteString ('', 'DataBase',   DataBase);
    WriteString ('', 'Login',      Login);
    WriteString ('', 'Password',   Password);
    WriteBool   ('', 'UseWinAuth', IsUseWinAuth);

    WriteString ('', 'NetworkLibrary', NetworkLibrary);
    WriteString ('', 'NetworkAddress', NetworkAddress);
    Free;
  end;
end;


//--------------------------------------------------------------------
procedure db_LoginRec_SaveToIni1(aIniFileName, aSection: String; aDBLoginRec:
    TdbLoginRec);
//--------------------------------------------------------------------
const
//  DEF_MAIN='Connection';
  DEF_MAIN='Main';

begin
  if aSection='' then
    aSection:=DEF_MAIN;


  ForceDirectories (ExtractFileDir (aIniFileName));


  with TIniFile.Create (aIniFileName), aDBLoginRec do
  begin
    WriteString (aSection, 'Server',     Server);
    WriteString (aSection, 'DataBase',   DataBase);
    WriteString (aSection, 'Login',      Login);
    WriteString (aSection, 'Password',   Password);
    WriteBool   (aSection, 'UseWinAuth', IsUseWinAuth);
    WriteString (aSection, 'NetworkLibrary', NetworkLibrary);
    WriteString (aSection, 'NetworkAddress', NetworkAddress);

    
    Free;
  end;

end;

// ---------------------------------------------------------------
function db_GetConnectionStatusStr(aLoginRec: TdbLoginRec):
// ---------------------------------------------------------------
    String;
const
//--; aLanguage: string = 'ru'
  DEF_RU  = 'Сервер: %s | БД: %s | Логин: %s';
//  DEF_RU  = 'Сервер: %s | БД: %s | Пользователь: %s | Логин: %s';
 // DEF_ENG = 'Server: %s | Database: %s | Login: %s';

//var
// .. s : string;
begin
// .. s:=DEF_RU;
//  if Eq(aLanguage,'ru') then s:=DEF_RU;
//  if Eq(aLanguage,'eng') then s:=DEF_ENG;



  with aLoginRec do
//    Result:=Format(s, [Server, Database, Login]); //, Password]); //  |  Пароль: "%s"',
//    Result:=Format(s, [Server, Database, aLoginRec.Results.UserName, Login]); //, Password]); //  |  Пароль: "%s"',
    Result:=Format(DEF_RU, [Server, Database, Login]); //, Password]); //  |  Пароль: "%s"',
end;

//---------------------------------------------------------------------------
function db_GetConnectionStatusStr_new111111(aLoginRec: TdbLoginRec): String;
//---------------------------------------------------------------------------
const
  DEF_STR_SQL  = 'Server: %s,  Database: %s,  Login: %s,  Pass: %s,  Auth: sql server';

  DEF_STR_WIN  = 'Server: %s,  Database: %s,  Auth: windows';

//  DEF_RU  = 'Сервер: %s | БД: %s | Пользователь: %s | Логин: %s';
 // DEF_ENG = 'Server: %s | Database: %s | Login: %s';

//var
//  sAuth: string;
begin

//  if Eq(aLanguage,'ru') then s:=DEF_RU;
//  if Eq(aLanguage,'eng') then s:=DEF_ENG;


 // sAuth:=IIF(aLoginRec.IsUseWinAuth, 'sql', 'windows');

  with aLoginRec do
    if IsUseWinAuth then
      Result:=Format(DEF_STR_WIN, [Server, Database])
    else
      Result:=Format(DEF_STR_SQL, [Server, Database, Login, Password ]);


end;



// ---------------------------------------------------------------
function db_OpenADOConnectionFromIni(aADOConnection: TADOConnection; aIniFileName:
    String; var aConnectionString: String): Boolean;
// ---------------------------------------------------------------
var
  r: TdbLoginRec;
  s: string;
begin
  db_LoginRec_LoadFromIni(aIniFileName, 'Connection', r);

  s:=db_ADO_MakeConnectionString(r);

  Result := db_OpenADOConnectionString(aADOConnection, s);


//  aConnectionString := db_GetConnectionStatusStr(r, 'eng');
end;

// ---------------------------------------------------------------
function db_OpenFromReg_(aADOConnection: TADOConnection; aRegPath: String ): boolean;
// ---------------------------------------------------------------
var
  rLoginRec: TdbLoginRec;

  sConnectionString: string;

begin
  Result := False;

  if not db_LoginRec_LoadFromReg (aRegPath, rLoginRec) then
    Exit;


 Result := db_OpenLoginRec(aADOConnection, rLoginRec);


end;


//
//
//   if db_LoginRec_LoadFromIni(sSrc_Ini, 'main', r) then 
//   begin
//     sConnectionString :=db_ADO_MakeConnectionString (r);
//
//     try
//       if (adStateOpen and aConnectionObject.State) <> 0 then
//         oConnectionObject.Close;
//
//     
////       oConnectionObject.Close;
//       oConnectionObject.Open(sConnectionString,'','',0);
//       
//       aRec.connection_string:=sConnectionString;
//       
//     except on E: Exception do
//     
//       Exit;
//     
//     end;
//     
//     Result:=S_OK;
//
//   end;
// 

//--------------------------------------------------------------------
function db_OpenFromIni(aIniPath, aSection: String; var aLoginRec: TdbLoginRec;    aConnectionObject: _Connection): boolean;
//--------------------------------------------------------------------
var
  sConnectionString: string;
 // r: TdbLoginRec;
 
begin
  Assert(Assigned(aConnectionObject), 'Value not assigned');

  Result := False;

//  if not db_LoginRec_LoadFromIni (aRegPath, aLoginRec) then
 //   Exit;


  if not db_LoginRec_LoadFromIni(aIniPath, aSection, aLoginRec) then 
    Exit;

//  ifaLoginRec

(*  TdmLogin.Init;
  Result := dmLogin.OpenDatabase_(aLoginRec, aConnectionObject);
*)

  sConnectionString:=db_ADO_MakeConnectionString (aLoginRec);

  try
    if (adStateOpen and aConnectionObject.State) <> 0 then
      aConnectionObject.Close;

//    aConnectionObject.
      
    aConnectionObject.Open(sConnectionString,'','',0);

    Result := True;
  except
  end;

end;
 

//--------------------------------------------------------------------
function db_OpenFromReg(aRegPath: String; var aLoginRec: TdbLoginRec; aConnectionObject:
    _Connection): boolean;
//--------------------------------------------------------------------
var
  sConnectionString: string;

begin
  Result := False;

  Assert(Assigned(aConnectionObject), 'Value not assigned');

  if not db_LoginRec_LoadFromReg (aRegPath, aLoginRec) then
    Exit;


//  ifaLoginRec

(*  TdmLogin.Init;
  Result := dmLogin.OpenDatabase_(aLoginRec, aConnectionObject);
*)

  sConnectionString:=db_ADO_MakeConnectionString (aLoginRec);

  try
    if (adStateOpen and aConnectionObject.State) <> 0 then
      aConnectionObject.Close;

    aConnectionObject.Open(sConnectionString,'','',0);

    Result := True;
  except
  end;

end;


//----------------------------------------------------------------------
//  Äèàëîã âûáîðà SQL ñåðâåðîâ, íàõîäÿùèõñÿ â ñåòè
//----------------------------------------------------------------------
function db_Dlg_GetSqlServerName_old(AHandle: THandle): string;
//----------------------------------------------------------------------
type
  TServerBrowseDialogA0 = function(hwnd: HWND; pchBuffer: Pointer; cchBufSize: DWORD) : bool; stdcall;

var
  i: integer;
  ServerBrowseDialogA0 : TServerBrowseDialogA0;
  LANMAN_DLL : DWORD;
  buffer : array[0..1024] of char;
  bLoadLib : Boolean;
  sCompName: string;
  sServerName: string;
begin
  bLoadLib := False;
  LANMAN_DLL := GetModuleHandle('NTLANMAN.DLL');

  if LANMAN_DLL = 0 then
  begin
    LANMAN_DLL := LoadLibrary('NTLANMAN.DLL');
    bLoadLib := True;
  end;

  if LANMAN_DLL <> 0 then begin
    @ServerBrowseDialogA0 := GetProcAddress(LANMAN_DLL, 'ServerBrowseDialogA0');

    ServerBrowseDialogA0 (AHandle, @buffer, 1024);

    if buffer[0] = '\' then
      sServerName := buffer;

    if bLoadLib then FreeLibrary(LANMAN_DLL);

    if Length(sServerName) > 2 then
      for i := 2 downto 1 do
        if sServerName[i] = '\' then
          Delete(sServerName, i, 1);
  end;


  sCompName:= db_GetLocalComputerName();

  if Eq(sServerName, 'local')     or Eq(sServerName, '(local)') or
     Eq(sServerName, '127.0.0.1') or Eq(sServerName, sCompName)
  then
    sServerName := '(local)';

  Result:=sServerName;

  //ed_Server.Text := sStr;

end;




end.





