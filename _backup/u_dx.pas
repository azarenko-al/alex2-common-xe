unit u_dx;

interface

uses Windows,SysUtils,Forms,Dialogs,Classes, Graphics, DB, Registry, Variants,
     dxTL, dxDBCtrl, dxDBGrid, dxDBTL, dxInspct, 
     dxInspRw, Controls, dxDBTLCl,
     PBFolderDialog,

     u_func,
     u_DB,
     u_classes,

     d_Customize_Grid;


  procedure dx_CheckSelectedItems   (aGrid: TdxDBGrid; aFieldName: String ; aChecked: boolean);

  function  dx_BrowseFile (Sender: TObject; aFilter: String; aDefaultExt: String=''): boolean; 
  procedure dx_GetSelectedItems (aDBGrid: TdxDBGrid; aIDList: TIDList; aKeyField: string );

  procedure dx_CheckColumnSizes_DBTreeList (aDXTree: TdxDBTreeList);
  procedure dx_CheckColumnSizes_DBGrid     (aDXGrid: TdxDBGrid);      

  procedure dx_ClearDBGridColumnsExcept (adxDBGrid: TdxDBGrid;
                                         aExceptColumnArr: array of TdxDBTreeListColumn);  

  procedure dx_SetValuesForSelectedItems (aGrid: TCustomdxTreeList; aFieldName: String; aValue: variant);

//  procedure dx_TreeList_SaveToXLS   (AGrid: TdxTreeList;   aFileName: String; aSaveAll: Boolean);
  procedure dx_DBTreeList_SaveToXLS (AGrid: TdxDBTreeList; aFileName: String; aSaveAll: Boolean);

  procedure dx_ColorRow         (aSender: TdxInspectorRow;
                                 aRowArr: array of TdxInspectorRow;
                                 ACanvas: TCanvas;
                                 var AColor: TColor;
                                 aIsRequiredParam: boolean = false;
                                 Color: TColor = clSilver );    


  procedure dx_ShowHint (aDXTree:        TCustomdxTreeListControl;
                         aHotTrackInfo:  TdxTreeListHotTrackInfo;
                         aColumnArray:   array of TdxTreeListColumn); 



  procedure dx_HideEditorsForButtonRows   (aGrid: TdxDBTreeList);     overload;
  procedure dx_HideEditorsForButtonRows   (aInspector: TdxInspector); overload;

  function  dx_CheckRegistry (aGrid: TCustomdxTreeListControl; aKey: String): Boolean; 

  procedure dx_BrowseFileFolder(aOwnerForm: TComponent; aRow: TdxInspectorTextButtonRow);


  function  dx_Dlg_Customize_DBGrid     (aGrid: TdxDBGrid): boolean;
  function  dx_Dlg_Customize_DBTreeList (aTreeList: TdxDBTreeList): boolean;
  procedure dx_Dlg_ExportToFile         (aGrid: TdxDBGrid);

  procedure dx_FillGridCaptionsByFieldNames(aGrid: TdxDbGrid; aFieldCaptionsIDList: TStrIDList);

//new
  procedure dx_SaveStateToReg (aRegPath: string; dxTreeList: TdxTreeList;
                               aGuidColumn: TdxTreeListColumn);


  procedure dx_SetReadOnly(aRowArray: array of TdxInspectorRow;  aReadOnly: boolean = true);


  function dx_CheckCursorInGrid1(Sender: TdxTreeList): Boolean;


  //--------------------------------------------------------------
  // Find
  //--------------------------------------------------------------
  function dx_FindFirstNode(aTreeList: TdxTreeList;
                            aColumnArr: array of TdxTreeListColumn;
                            aValueArr:  array of Variant;
                            aParentNode: TdxTreeListNode=nil): TdxTreeListNode;



//========================================================================
//========================================================================

implementation
const
  REG_SECTION_EXPANDED = 'Expanded nodes';
  REG_FOCUSED_GUID     = 'Focused GUID';

{
  function  dx_FindNodeByValue  (aTreeList: TdxTreeList;
                                 aColumn: TdxTreeListColumn;
                                 aValue: Variant ): TdxTreeListNode; forward;

  function dx_FindNodes(aTreeList: TdxTreeList;
                        aColumnArr: array of TdxTreeListColumn;
                        aValueArr:  array of Variant;
                        aNodeList: TList;
                        aParentNode: TdxTreeListNode=nil): Boolean; forward;

}


function ReplaceStr (Value, OldPattern, NewPattern: string): string;
begin
  Result:=StringReplace (Value, OldPattern, NewPattern, [rfReplaceAll, rfIgnoreCase]);
end;


function dx_CheckCursorInGrid1(Sender: TdxTreeList): Boolean;
Var
  P: TPoint;
  hi: TdxTreeListHitInfo;
begin
  P := Sender.ScreenToClient(Mouse.CursorPos);
  hi := Sender.GetHitInfo(P);
  Result:=(hi.hitType = htLabel);

(*
  P := TdxTreeList(Sender).ScreenToClient(Mouse.CursorPos);
  hi := TdxTreeList(Sender).GetHitInfo(P);
  Result:=(hi.hitType = htLabel);
*)

end;




//-------------------------------------------------------------------
procedure dx_SaveStateToReg (aRegPath: string; dxTreeList: TdxTreeList;
                             aGuidColumn: TdxTreeListColumn);
//-------------------------------------------------------------------
var
  oRegStrList: TStringList;
  
  //---------------------------------------------------------
  procedure DoSaveExpanedNode (aNode: TdxTreeListNode);
  var i: integer;
    s: string;
  begin
    s:=aNode.Values[AGuidColumn.Index];
    oRegStrList.Add (s);

    for i:=0 to aNode.Count-1 do
     if aNode.Items[i].Expanded then
       DoSaveExpanedNode (aNode.Items[i]);
  end;
  //---------------------------------------------------------


var
  oRegIni: TRegistryIniFile;
  oNode: TdxTreeListNode;
  s: string;
  i: integer;

begin
  if aRegPath='' then
    Exit;

  oRegStrList:=TStringList.Create;


  for i:=0 to dxTreeList.Count-1 do
   if dxTreeList.Items[i].Expanded then
     DoSaveExpanedNode (dxTreeList.Items[i]);

  oRegIni:=TRegistryIniFile.Create (aRegPath);
  oRegIni.EraseSection (REG_SECTION_EXPANDED);

  for i:=0 to oRegStrList.Count-1 do
    oRegIni.WriteString (REG_SECTION_EXPANDED, oRegStrList[i], '');

  oNode:=dxTreeList.FocusedNode;

  if Assigned(oNode) then
  begin
    s:=oNode.Values[aGuidColumn.Index];

    oRegIni.WriteString ('', REG_FOCUSED_GUID, s);
  end;

  oRegIni.Free;

  oRegStrList.Free;
end;


//------------------------------------------------------------------------------
procedure dx_HideEditorsForButtonRows (aGrid: TdxDBTreeList);
//------------------------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to aGrid.ColumnCount-1 do
    if (aGrid.Columns[i] is TdxDBTreeListButtonColumn) then
       (aGrid.Columns[i] as TdxDBTreeListButtonColumn).HideEditCursor:= true;
end;

//------------------------------------------------------------------------------
procedure dx_HideEditorsForButtonRows (aInspector: TdxInspector);
//------------------------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to aInspector.TotalRowCount-1 do
    if (aInspector.Rows[i] is TdxInspectorTextButtonRow) then
       (aInspector.Rows[i] as TdxInspectorTextButtonRow).HideEditCursor:= true;
end;


//------------------------------------------------------------------------------
//  ���������  ��������� caption-� �������� ����� �� ��������������� ������,
//  ����������� fieldName � �������� ID � caption � �������� name
procedure dx_FillGridCaptionsByFieldNames(aGrid: TdxDbGrid; aFieldCaptionsIDList: TStrIDList);
//------------------------------------------------------------------------------
var
  I, iCount: Integer;
begin
  for I := 0 to aGrid.ColumnCount-1 do
  begin
    iCount:= aFieldCaptionsIDList.IndexBySID (aGrid.Columns[i].FieldName);
    if iCount>=0 then
      aGrid.Columns[i].Caption:= aFieldCaptionsIDList[iCount].Name;
  end;
end;



//----------------------------------------------------
procedure dx_SetReadOnly(aRowArray: array of TdxInspectorRow;  aReadOnly: boolean = true);
//----------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to High(aRowArray) do
    aRowArray[i].ReadOnly:= aReadOnly;
end;





procedure dx_ExpandFirstLevel(aTreeList:TdxTreeList; aRecurse: boolean);
var  i: integer;
begin
  for i := 0 to aTreeList.Count - 1 do
    aTreeList.Items[i].Expand(aRecurse);
end;

//============================================================================//
//  ���������� �������� � TreeList
//
//============================================================================//
procedure dx_DBTreeListAdd (aTreeList: TdxDBTreeList);
var  bMulti: boolean;
begin
  bMulti := (etoMultiSelect IN aTreeList.OptionsBehavior);
  if bMulti then
    aTreeList.OptionsBehavior := aTreeList.OptionsBehavior - [etoMultiSelect];
  aTreeList.DataSource.DataSet.Insert;
  if bMulti then
    aTreeList.OptionsBehavior := aTreeList.OptionsBehavior + [etoMultiSelect];
  aTreeList.ShowEditor;
end;


//-----------------------------------------------------
// TREE LIST functions
//-----------------------------------------------------

procedure dx_MoveNodeTop (aTreeList: TdxTreeList; aNode: TdxTreeListNode);
begin
//  with aTreeList do
//   if Count>1 then
  aNode.MoveTo (aNode, natlAddFirst);
end;

{
//-----------------------------------------------------
function dx_FindNodeByData (
           aTreeList: TdxTreeList;
           AData: Pointer;
           AParentNode: TdxTreeListNode=nil): TdxTreeListNode;
//-----------------------------------------------------
var i,j:integer;
begin
  Result:=nil;

  if Assigned(AParentNode) then with AParentNode do
    for i:=0 to Count-1 do
    begin
      if (Items[i].Data=AData) then Result:=Items[i] else
      if (Items[i].Count>0)    then Result:=dx_FindNodeByData (ATreeList,AData,Items[i])
                               else Continue;
      if Assigned(Result) then Break;
    end else

  if not Assigned(AParentNode) then with ATreeList do
    for i:=0 to Count-1 do
    begin
      if (Items[i].Data=AData) then Result:=Items[i] else
      if (Items[i].Count>0)    then Result:=dx_FindNodeByData (ATreeList,AData,Items[i])
                               else Continue;
      if Assigned(Result) then Break;
    end;
end;}


//------------------------------------------------------------------------------
procedure dx_FocuseNode(aNode: TdxTreeListNode);
//------------------------------------------------------------------------------
begin
 // aNode.MakeVisible;


  try

(*    if Assigned(aNode.Parent) then
      if not aNode.Parent.Expanded then
        exit;
*)

  //  aNode.MakeVisible;
    aNode.Focused := True;


//    TdxTreeList(ANode.Owner).FocusedNumber:=ANode.AbsoluteIndex;
  except end;


end;


(*//-----------------------------------------------------
procedure dx_FocuseNode(aNode: TdxTreeListNode; adxTreeList: TdxTreeList);
//-----------------------------------------------------
begin
  aNode.MakeVisible;

//  aNode.Focused := True;

//  adxTreeList.FocusedNode:=aNode;

 // adxTreeList.FocusedNumber:=ANode.AbsoluteIndex;
//  TdxTreeList(ANode.Owner).FocusedNumber:=ANode.AbsoluteIndex;
end;

*)

function dx_GetFocusedNode (aTreeList:TdxTreeList): TdxTreeListNode;
begin
  Result:=aTreeList.FocusedNode;
end;

function dx_GetFocusedClass (aTreeList:TdxTreeList): TObject;
begin
  with aTreeList do
    if Assigned(FocusedNode) then Result:=TObject(FocusedNode.Data) else Result:=nil;
end;


function dx_GetFocusedColumn (aTreeList: TdxTreeList): TdxTreeListColumn;
var ind: integer;
begin
  ind:=aTreeList.FocusedAbsoluteIndex;
  if (ind>=0) then Result:=aTreeList.Columns[ind] else Result:=nil;
end;


//===========================================================================//
//  ��������� ������ �������� ��������� ���� �� ���������� �������
//  � ���� ������ (���� ��������� � ����� ������)
//
//===========================================================================//

function dx_GetSelectedIdAsString(aDBGrid: TdxDBGrid; aFieldName: string = 'id'): string;
var  i: integer;
begin
  Result := '';
  with aDBGrid do
    for i := 0 to SelectedCount - 1 do
  begin
    DataSource.DataSet.GotoBookmark(Pointer(SelectedRows[i]));
    Result := Result + DataSource.DataSet.FieldByName(aFieldName).AsString + ',';
  end;
  Delete(Result, Length(Result), 1);
end;


//-------------------------------------------------------------------
procedure dx_ClearDBGridColumnsExcept (adxDBGrid: TdxDBGrid;
                      aExceptColumnArr: array of TdxDBTreeListColumn);
//-------------------------------------------------------------------
// bands[0] - not deleted !
//-------------------------------------------------------------------
var  i,j: integer;
  sName: string;
label
  label_Continue;
begin
  adxDBGrid.BeginUpdate;
 // try

  for i := adxDBGrid.ColumnCount - 1 downto 0 do
  begin
  //  sName:=adxDBGrid.Columns[i].Name;

    for j:=0 to High(aExceptColumnArr) do
      if adxDBGrid.Columns[i] = aExceptColumnArr[j]
        then Goto label_Continue;

    if (adxDBGrid.Columns[i].Tag <> 0) then
      TStringList(adxDBGrid.Columns[i].Tag).Free;
    adxDBGrid.Columns[i].Free;

    label_Continue:
  end;

  //del all except 1
  for i := adxDBGrid.Bands.Count - 1 downto 1 do
    adxDBGrid.Bands[i].Free;

//  finally
  adxDBGrid.EndUpdate;
//  end;
end;

//-------------------------------------------------------------------
procedure dx_CheckColumnSizes_DBTreeList (aDXTree: TdxDBTreeList);
//-------------------------------------------------------------------
// bands[0] - not deleted !
//-------------------------------------------------------------------
var i: integer;
begin
  aDXTree.BeginUpdate;

  for i:=0 to aDXTree.ColumnCount - 1  do
    if (aDXTree.Columns[i].Width > 300) then
      aDXTree.Columns[i].Width:=100;

  aDXTree.EndUpdate;
end;

//-------------------------------------------------------------------
procedure dx_CheckColumnSizes_DBGrid (aDXGrid: TdxDBGrid);
//-------------------------------------------------------------------
// bands[0] - not deleted !
//-------------------------------------------------------------------
var i: integer;
begin
  aDXGrid.BeginUpdate;

  for i:=0 to aDXGrid.ColumnCount - 1  do
    if (aDXGrid.Columns[i].Width > 200) then
      aDXGrid.Columns[i].Width:=50;

  aDXGrid.EndUpdate;
end;



//-------------------------------------------------------------------
function dx_BrowseFile (Sender: TObject; aFilter: String; aDefaultExt: String=''): boolean;
//-------------------------------------------------------------------
var oRow: TdxInspectorTextButtonRow;
  oDialog: TOpenDialog;
begin
  oDialog:=TOpenDialog.Create(Application);
  oDialog.Filter:=aFilter;

  oRow:=TdxInspectorTextButtonRow(Sender);
  oDialog.FileName:=oRow.Text;
  oDialog.InitialDir:=ExtractFileDir(oDialog.FileName);

  Result:=oDialog.Execute;
  if Result then begin
    oRow.Text:=oDialog.FileName;

    if aDefaultExt<>'' then
      oRow.Text:=ChangeFileExt(oRow.Text,aDefaultExt);
  end;

  oDialog.Free;
end;


//-------------------------------------------------------------------
procedure dx_SetValuesForSelectedItems_DBGrid (aGrid: TdxDBGrid; aFieldName: string; aValue: variant);
//-------------------------------------------------------------------
//var i,iID: integer;
begin
  dx_CheckSelectedItems (aGrid, aFieldName, aValue);

{
  with aGrid do
    for i := 0 to SelectedCount - 1 do
    begin
      iID:=SelectedNodes[i].Values [ColumnByFieldName(KeyField).Index];

      if DataSource.Dataset.Locate (KeyField, iID, []) then
        db_UpdateRecord (DataSource.Dataset, [db_Par(aFieldName, aValue)]);
    end;
}

end;


//-------------------------------------------------------------------
procedure dx_SetValuesForSelectedItems_DBTreeList (aGrid: TdxDBTreeList; aFieldName: string; aValue: variant);
//-------------------------------------------------------------------
// KeyField for BEELINE TN  - hopsname
// -------------------------------------------------------------------
var i,j,iID: integer; b: boolean;
  s,sName: string;
  oDataset: TDataset;
  oNode: TdxTreeListNode;

//  oIDList: TIDList;
  oIDList: TStringLIst;
begin
  oIDList:=TStringList.Create;
//  oIDList:=TIDList.Create;

  with aGrid do
    for i := 0 to SelectedCount - 1 do
    begin
      oNode:=SelectedNodes[i];

      if Assigned(oNode) then
      begin
//       oIDList.AddID (oNode.Values [ColumnByFieldName(KeyField).Index]);
        s:=AsString(oNode.Values [ColumnByFieldName(KeyField).Index]);
        oIDList.Add (s);
      end;
    end;

   oDataset:=aGrid.DataSource.Dataset;

   oDataset.DisableControls;

   with aGrid do
     for i := 0 to oIDList.Count - 1 do
      if oDataset.Locate (KeyField, oIDList[i], []) then
//      if oDataset.Locate (KeyField, oIDList[i].ID, []) then
        db_UpdateRecord (oDataset, [db_Par(aFieldName, aValue)]);

   oDataset.EnableControls;


  oIDList.Free;
end;





//-------------------------------------------------------------------
procedure dx_SetValuesForSelectedItems (aGrid: TCustomdxTreeList; aFieldName: String ; aValue: variant);
//-------------------------------------------------------------------
begin
//  ShowMessage ( aGrid.ClassName );

  if Eq(aGrid.ClassName , 'TdxDBTreeList') then begin
   //.. ShowMessage ( 'aGrid is TdxDBTreeList' );

    dx_SetValuesForSelectedItems_DBTreeList (TdxDBTreeList(aGrid), aFieldName, aValue);
  end else

  if Eq(aGrid.ClassName , 'TdxDBGrid') then begin
   // ShowMessage ( 'aGrid is TdxDBGrid' );

    dx_SetValuesForSelectedItems_DBGrid (TdxDBGrid(aGrid), aFieldName, aValue);
  end
  else
    raise Exception.Create('dx_SetValuesForSelectedItems');

end;

 
//-------------------------------------------------------------------
procedure dx_Dlg_ExportToFile (aGrid: TdxDBGrid);
//-------------------------------------------------------------------
const
  DX_FILTER_STR =
     'Excel|.xls|' +
     'HTML|*.html|'+
     'XML|.xml|'   +
     'Text|.txt';

  ARRAY_FILTER_EXT : array[1..4] of string =
    ('.xls','.html','.xml','.txt');

var
  oSaveDialog: TSaveDialog;
begin
  oSaveDialog:=TSaveDialog.Create(Application);
  oSaveDialog.Filter:=DX_FILTER_STR;

  with oSaveDialog do  if Execute then
  begin
     if ExtractFileExt(FileName) = '' then
       FileName := FileName + ARRAY_FILTER_EXT[FilterIndex];

    case FilterIndex of
      1: aGrid.SaveToXLS (FileName, True);
      2: aGrid.SaveToHTML(FileName, True);
      3: aGrid.SaveToXML (FileName, True);
      4: aGrid.SaveToText(FileName, True, #9, '', '');
    end;
  end;

  oSaveDialog.Free;
end;



//----------------------------------------------
procedure dx_ColorRow(aSender: TdxInspectorRow;
                      aRowArr: array of TdxInspectorRow;
                      ACanvas: TCanvas;
                      var AColor: TColor;
                      aIsRequiredParam: boolean = false;
                      Color: TColor = clSilver );
//----------------------------------------------
var
  I: Integer;
begin
  for I := 0 to High(aRowArr) do
    if aSender = aRowArr[i] then
    begin
      if aRowArr[i].ReadOnly then
        ACanvas.Font.Color:=clGrayText
      else

      if (aIsRequiredParam) then
        AColor:=Color;
    end;
end;



{
procedure dx_TreeList_SaveToXLS (AGrid: TdxTreeList; AFileName: String ; ASaveAll: Boolean);
begin
  SaveToXLS (AGrid, AFileName, ASaveAll);
//       SaveToXLS (TdxTreeList(dxDBTreeList), SaveDialog1.FileName, True); //False); //True);
end;}


procedure dx_DBTreeList_SaveToXLS (AGrid: TdxDBTreeList; AFileName: String ; ASaveAll: Boolean);
begin
///////////  SaveToXLS (AGrid, AFileName, ASaveAll);
//       SaveToXLS (TdxTreeList(dxDBTreeList), SaveDialog1.FileName, True); //False); //True);
end;

//------------------------------------------------------------------------------
procedure dx_ShowHint (aDXTree:        TCustomdxTreeListControl;
                       aHotTrackInfo:  TdxTreeListHotTrackInfo;
                       aColumnArray:   array of TdxTreeListColumn);
//------------------------------------------------------------------------------
var
  i: Integer;
  iColNumb: integer;
begin
//  Assert(LApplication<>nil);
{
  if LApplication = nil then
    Exit;
}

  if (not Assigned(AHotTrackInfo.Node)) and (AHotTrackInfo.Column <> -1) then
  begin
    Application.HideHint;
    aDXTree.Hint := aDXTree.Columns[AHotTrackInfo.Column].Caption;
    Application.ActivateHint(Mouse.CursorPos);
  end else

  if (AHotTrackInfo.Column <> -1) and Assigned(AHotTrackInfo.Node) and
     (AHotTrackInfo.Node.Values[AHotTrackInfo.Column] <> NULL)
  then
  begin
    iColNumb := AHotTrackInfo.Column;

    Application.HideHint;

    for i := 0 to High(aColumnArray) do     
	  if (iColNumb = aColumnArray[i].Index) then
      begin
        aDXTree.Hint := AHotTrackInfo.Node.Values[AHotTrackInfo.Column];
        if Pos('|', aDXTree.Hint)>0 then
          aDXTree.Hint:= ReplaceStr(aDXTree.Hint, '|', '/');

        Application.ActivateHint(Mouse.CursorPos);
        break;
      end;

  end;
end;



function dx_Dlg_Customize_DBGrid (aGrid: TdxDBGrid): boolean;
begin
  Result := Tdlg_Customize_dx_Grid.ExecDlg_DBGRID(aGrid);
end;


function dx_Dlg_Customize_DBTreeList (aTreeList: TdxDBTreeList): boolean;
begin
  Result := Tdlg_Customize_dx_Grid.ExecDlg_DBTreeList(aTreeList);
end;

//-------------------------------------------------------------------
procedure dx_BrowseFileFolder (aOwnerForm: TComponent; aRow: TdxInspectorTextButtonRow);
//-------------------------------------------------------------------
var oRow: TdxInspectorTextButtonRow;
   oDialog: TPBFolderDialog;
begin
  oDialog:=TPBFolderDialog.Create(aOwnerForm);

 // oRow:=TdxInspectorTextButtonRow(Sender);
  oDialog.Folder:=aRow.Text;
  if oDialog.Execute then
    aRow.Text := oDialog.Folder;

  oDialog.Free;
end;


//------------------------------------------------------------------------------
function  dx_CheckRegistry(aGrid: TCustomdxTreeListControl; aKey: String): Boolean;
//------------------------------------------------------------------------------
var
  info: TRegKeyInfo;
begin
  Result:= True;

  with TRegistry.Create do
  begin
    if OpenKey(IncludeTrailingBackslash(aKey)+'Columns',False) then
    begin
      GetKeyInfo(info);
      if info.NumSubKeys<>aGrid.ColumnCount then
        Result:=False;

      CloseKey;
    end;

    if OpenKey(IncludeTrailingBackslash(aKey)+'Band',False) then
    begin
      GetKeyInfo(info);
      if info.NumSubKeys<>aGrid.Bands.Count then
        Result:=False;

      CloseKey;
    end;

    if not Result then
      DeleteKey(aKey);

    Free;
  end;
end;

// ---------------------------------------------------------------
procedure dx_GetSelectedItems (aDBGrid: TdxDBGrid; aIDList: TIDList; aKeyField: string );
// ---------------------------------------------------------------
var
  i,iID: Integer;
begin
  aIDList.Clear;

  for I := 0 to aDBGrid.Count-1 do
    if aDBGrid.Items[i].Selected then
    begin
      iID:=aDBGrid.Items[i].Values [aDBGrid.ColumnByFieldName(aKeyField).Index];
      aIDList.AddID (iID);
    end;

end;



//-------------------------------------------------------------------
function dx_CheckSelectedItemsByNameID (
                    aDxGrid: TdxDBGrid;
                    aChecked: boolean;
                    aCheckedFieldName,aKeyFieldName: string): integer;
//-------------------------------------------------------------------
//        aID_FieldName, aName_FieldName): integer;
//-------------------------------------------------------------------
var i, iID: integer;
  sKey: string;
  //,sID, sName: string;
begin


  Result:= 0;

  with aDxGrid do
  begin
    DataSource.Dataset.DisableControls;

    for i := 0 to SelectedCount - 1 do
    begin
    {
      sID  :=SelectedNodes[i].Values [ColumnByFieldName(aIndexFieldName).Index];
      sName:=SelectedNodes[i].Values [ColumnByFieldName(aName_FieldName).Index];
    }
    
      sKey  :=SelectedNodes[i].Values [ColumnByFieldName(aKeyFieldName).Index];

//      if DataSource.Dataset.Locate (aIndexFieldName, VarArrayOf([sID]), []) then
{ TODO : update }

      if DataSource.Dataset.Locate (aKeyFieldName, sKey, []) then
//      if DataSource.Dataset.Locate (aID_FieldName+';'+aName_FieldName, VarArrayOf([sID,sName]), []) then
      begin
        DataSource.Dataset.Edit;
        DataSource.Dataset[aCheckedFieldName]:= aChecked;
        DataSource.Dataset.Post;

   //     db_UpdateRecord (DataSource.Dataset, [db_Par(aCheckedFieldName, aChecked)]);

        Inc(Result);
      end;
    end;

    DataSource.Dataset.EnableControls;
  end;
end;



//-------------------------------------------------------------------
procedure dx_CheckSelectedItems (aGrid: TdxDBGrid; aFieldName: string; aChecked: boolean);
//-------------------------------------------------------------------
// KeyField for BEELINE TN  - hopsname
// -------------------------------------------------------------------
var i,j,iID: integer; b: boolean;
  s,sName: string;
  oDataset: TDataset;
  oNode: TdxTreeListNode;

//  oIDList: TIDList;
  oIDList: TStringLIst;
  sKeyField: string;
begin
  Assert(aGrid.KeyField<>'');

  oIDList:=TStringList.Create;
//  oIDList:=TIDList.Create;

  with aGrid do
    for i := 0 to SelectedCount - 1 do
    begin
      oNode:=SelectedNodes[i];

      if Assigned(oNode) then
      begin
//       oIDList.AddID (oNode.Values [ColumnByFieldName(KeyField).Index]);
        s:=AsString(oNode.Values [ColumnByFieldName(KeyField).Index]);
        oIDList.Add (s);
      end;
    end;

   oDataset:=aGrid.DataSource.Dataset;

   oDataset.DisableControls;

   sKeyField := aGrid.KeyField;

//   with aGrid do
   for i := 0 to oIDList.Count - 1 do
    if oDataset.Locate (sKeyField, oIDList[i], []) then
      db_UpdateRecord (oDataset, [db_Par(aFieldName, aChecked)]);

   oDataset.EnableControls;

   oIDList.Free;
end;



//-----------------------------------------------------
function dx_FindFirstNode(aTreeList: TdxTreeList;
                          aColumnArr: array of TdxTreeListColumn;
                          aValueArr:  array of Variant;
                          aParentNode: TdxTreeListNode=nil): TdxTreeListNode;
//-----------------------------------------------------

     function DoCheckNode(aNode: TdxTreeListNode): Boolean;

     var  I: integer;
       v: Variant;
       s1,s2: String;
       k: integer;
     begin
       Result := True;

       for I := 0 to High(aColumnArr) do
         if Result = True then
       begin
         try
           v:=aNode.Values[aColumnArr[i].Index];
         except
         //	on e: Exception do
        // 		Application.HandleException(Sender);
         end;  // try/except

         // ���� 2 ������ - �������� ��� ��������

         k:=VarType(v);

         Assert((k =varString) or (k=varInteger));


         if (VarType(v)            = varString) and
            (VarType(AValueArr[i]) = varString) then
         begin


           try
             s1:=v;
             s2:=AValueArr[i];
  
             Result :=CompareText(Trim(s1),Trim(s2))=0;
           except
          // 	on e: Exception do
          // 		Application.HandleException(Sender);
           end;  // try/except



{
         case VarType(aValue) of
            varString,
            varOleStr:  begin
                          s:=Trim (aValue);
                          s:=ReplaceStr (s, ',','.');
                          Val(s, Result, err);

                          if err<>0 then Result:=aDefault;
                        end;}
         end else

         if (v<>AValueArr[i]) then
       //  begin
           Result := False;
          // Break;
        // end;
       end;
     end;
     //---------------------------------------------------------

var i: integer;
begin
  Result:=nil;

  //---------------------------------------------------------
  if Assigned(AParentNode) then// with AParentNode do
    for i:=0 to AParentNode.Count-1 do
    begin
      if DoCheckNode(AParentNode.Items[i]) then
        Result:=AParentNode.Items[i] else

    //  if (Items[i].Values[aColumn.Index]=AValue) then Result:=Items[i] else

      if (AParentNode.Items[i].Count>0) then
        Result:=dx_FindFirstNode (ATreeList,aColumnArr,aValueArr,AParentNode.Items[i])
      else
        Continue;

      if Assigned(Result) then
        Exit;
    end else

  //---------------------------------------------------------
  if not Assigned(AParentNode) then //with ATreeList do
    for i:=0 to AParentNode.Count-1 do
    begin
      if DoCheckNode(AParentNode.Items[i]) then
        Result:=AParentNode.Items[i] else
//        if (Items[i].Values[aColumn.Index]=AValue) then  Result:=Items[i] else

      if (AParentNode.Items[i].Count>0) then
        Result:=dx_FindFirstNode (ATreeList,aColumnArr,aValueArr,AParentNode.Items[i])
      else Continue;

      if Assigned(Result) then
        Exit;
    end;
end;





begin
 // dx_Init (Application, Mouse);
end.


{


//-----------------------------------------------------
function dx_FindNodeByValue (aTreeList: TdxTreeList; aColumn: TdxTreeListColumn; aValue: Variant ): TdxTreeListNode;
//-----------------------------------------------------
var i:integer;
begin
  Result:=nil;

  with aTreeList do
    for i:=0 to Count-1 do
      if (Items[i].Values[aColumn.Index]=aValue)
        then begin Result:=Items[i]; Exit; end;

end;



//-----------------------------------------------------
function dx_FindNodes(aTreeList: TdxTreeList;
    aColumnArr: array of TdxTreeListColumn;
    aValueArr:  array of Variant;
    aNodeList: TList;
    aParentNode: TdxTreeListNode=nil): Boolean;
//-----------------------------------------------------

     function DoCheckNode(aNode: TdxTreeListNode): Boolean;
     var  I: integer;
     begin
       Result := True;

       for I := 0 to High(aColumnArr) do
         if (aNode.Values[aColumnArr[i].Index]<>AValueArr[i]) then
         begin
           Result := False;
           Break;
         end;
     end;
     //---------------------------------------------------------

var i: integer;
begin
  assert(Assigned(aNodeList));

//  Result:=nil;


  //---------------------------------------------------------
  // root level
  //---------------------------------------------------------
  if not Assigned(AParentNode) then
  //---------------------------------------------------------
  begin
    aNodeList.Clear;

    with ATreeList do
      for i:=0 to Count-1 do
    begin
      if DoCheckNode(Items[i]) then  aNodeList.Add(Items[i]) else
//        if (Items[i].Values[aColumn.Index]=AValue) then  Result:=Items[i] else

      if (Items[i].Count>0) then
        dx_FindNodes (ATreeList,aColumnArr,aValueArr, aNodeList, Items[i])
      else
        Continue;

     // if Assigned(Result) then Exit;
    end;


    Result := aNodeList.Count>0;

  end else

  //---------------------------------------------------------
  // second level
  //---------------------------------------------------------

  //---------------------------------------------------------
  if Assigned(AParentNode) then
  //---------------------------------------------------------
    with AParentNode do
      for i:=0 to Count-1 do
    begin
      if DoCheckNode(Items[i]) then  aNodeList.Add(Items[i]) else

    //  if (Items[i].Values[aColumn.Index]=AValue) then Result:=Items[i] else

      if (Items[i].Count>0) then
        dx_FindNodes (ATreeList,aColumnArr,aValueArr, aNodeList, Items[i])
      else
        Continue;

     // if Assigned(Result) then Exit;
    end ;

end;



}


{
  procedure dx_ShowHintForDbTreeColumns (aTree: TdxDbTreeList;
    								     	              aHotTrackInfo: TdxTreeListHotTrackInfo;
	  							                      aColumnArray: array of TdxTreeListColumn);

  procedure dx_ShowHintForDbGridColumns (aGrid: TdxDbGrid;
									                      aHotTrackInfo: TdxTreeListHotTrackInfo;
								                        aColumnArray: array of TdxTreeListColumn);
}


{  IdxTools = interface(IInterface)

  end;
}




{//------------------------------------------------------------------------------
procedure dx_ShowHintForDbTreeColumns(aTree: TdxDbTreeList;
								  	  aHotTrackInfo: TdxTreeListHotTrackInfo;
								      aColumnArray: array of TdxTreeListColumn);
//------------------------------------------------------------------------------
begin
  dx_ShowHint (aTree, aHotTrackInfo, aColumnArray);
end;


//------------------------------------------------------------------------------
procedure dx_ShowHintForDbGridColumns(aGrid: TdxDbGrid;
									  aHotTrackInfo: TdxTreeListHotTrackInfo;
								      aColumnArray: array of TdxTreeListColumn);
//------------------------------------------------------------------------------
begin
  dx_ShowHint (aGrid, aHotTrackInfo, aColumnArray);
end;
}



//-------------------------------------------------------------------
procedure cx_CheckSelectedItems(aGrid: TcxGridDBTableView; aFieldName: string; aChecked: boolean);
//-------------------------------------------------------------------
// KeyField for BEELINE TN  - hopsname
// -------------------------------------------------------------------
var i: integer;
  v: Variant;
  s: string;
  iKeyFieldInd: integer;
  sKeyField: string;
  iColumnInd: integer;
  oList: TStringLIst;
  oDataset: TDataset;
begin
  oList:=TStringList.Create;

  Assert(Assigned(aGrid.GetColumnByFieldName(aFieldName)));
//  iColumnInd:=aGrid.GetColumnByFieldName(aFieldName).Index;

  sKeyField:=aGrid.DataController.KeyFieldNames;

  iColumnInd:=aGrid.GetColumnByFieldName(aFieldName).Index;
  iKeyFieldInd:=aGrid.GetColumnByFieldName(sKeyField).Index;

//  else
  //  raise Exception.Create('if Assigned(aGrid.GetColumnByFieldName(aFieldName)) then');
{

  with aGrid.Controller do
    for i := 0 to SelectedRowCount - 1 do
      aGrid.DataController.Values[SelectedRows[i].RecordIndex, iColumnInd]:=aChecked;


  oDataset:=aGrid.DataSource.Dataset;

  // oDataset.DisableControls;

   with aGrid do
     for i := 0 to oList.Count - 1 do
      if oDataset.Locate (KeyField, oList[i], []) then
        db_UpdateRecord (oDataset, [db_Par(aFieldName, aChecked)]);
}

  with aGrid.Controller do
    for i := 0 to SelectedRowCount - 1 do
    begin
      v:=aGrid.DataController.Values[SelectedRows[i].RecordIndex, iKeyFieldInd];

      try
        s:=aGrid.DataController.Values[SelectedRows[i].RecordIndex, iKeyFieldInd];
      except

      end;

      oList.Add (s);
    //  aGrid.DataController.Values[SelectedRows[i].RecordIndex, iColumnInd]:=aChecked;
    end;

   oDataset:=aGrid.DataController.DataSet;//  DataSource.Dataset;

  // oDataset.DisableControls;

   with aGrid do
     for i := 0 to oList.Count - 1 do
      if oDataset.Locate (sKeyField, oList[i], []) then
        db_UpdateRecord (oDataset, [db_Par(aFieldName, aChecked)]);



  oList.Free;
end;



//-------------------------------------------------------------------
procedure cx_CheckSelectedItems(aTableView: TcxGridDBBandedTableView; aFieldName: string; aChecked:
    boolean);
//-------------------------------------------------------------------
// KeyField for BEELINE TN  - hopsname
// -------------------------------------------------------------------

 function SelectedRowCount: Integer;
  begin
    Result := aTableView.Controller.SelectedRowCount;
  end;

  function SelectedColumnCount: Integer;
  begin
    Result := aTableView.Controller.SelectedColumnCount;
  end;
{

  function GetSummOfSelection: Integer;
  var
    I, J: Integer;
    val: Variant;
  begin
    Result := 0;
    for I := 0 to SelectedRowCount - 1 do
      for J := 0 to SelectedColumnCount - 1 do
      begin
        val := TableView.DataController.GetValue(
          TableView.Controller.SelectedRows[I].RecordIndex,
          TableView.Controller.SelectedColumns[J].Index);
        if not VarIsNull(val) then
          Inc(Result,  Integer(val));
      end;
  end;}

var
  j: integer;
  v: Variant;
  s: string;
  sKeyField: string;
 // sKeyField: string;
  i: integer;
  iKeyFieldInd,iColumnInd: integer;
  oList: TStringLIst;
  oDataset: TDataset;
begin
  oList:=TStringList.Create;
//  oIDList:=TIDList.Create;




  Assert( Assigned(aTableView.GetColumnByFieldName(aFieldName)));

  sKeyField:=aTableView.DataController.KeyFieldNames;

  iColumnInd:=aTableView.GetColumnByFieldName(aFieldName).Index;
  iKeyFieldInd:=aTableView.GetColumnByFieldName(sKeyField).Index;

//  else
 //   raise Exception.Create('if Assigned(aTableView.GetColumnByFieldName(aFieldName)) then');

//  aTableView.DataController.aTableView.Controller.SelectedRecords[aTableView.Controller.SelectedRecordCount-3].Enabled := ;               

{

  i:=aTableView.ViewData.Rows[0].
}
//  aTableView.DataController.r

//    aTableView.DataController.selectedr

   i:=aTableView.Controller.SelectedRecordCount;



{
    for I := 0 to SelectedRowCount - 1 do
      begin
        v := aTableView.DataController.GetValue(
          aTableView.Controller.SelectedRows[I].RecordIndex,  iKeyFieldInd);
      end;
}

    with aTableView.Controller do
      for i := 0 to SelectedRecordCount - 1 do
         if Assigned(aTableView.Controller.SelectedRecords[i]) then
      begin
     //    assert(Assigned(aTableView.Controller.SelectedRecords[i]));

         v:=aTableView.Controller.SelectedRecords[i].Values[iKeyFieldInd];

      //   j:=aTableView.Controller.SelectedRows[i].RecordIndex;

       //  v:=aTableView.DataController.Values[aTableView.Controller.SelectedRows[i].RecordIndex, iKeyFieldInd];

         s:=AsString(v);

  //      s:=aTableView.DataController.Values[SelectedRows[i].RecordIndex, iKeyFieldInd];

        oList.Add (s);
      //  aTableView.DataController.Values[SelectedRows[i].RecordIndex, iColumnInd]:=aChecked;
      end else
        j:=0;



{    with aTableView.Controller do
      for i := 0 to SelectedRowCount - 1 do
         if Assigned(aTableView.Controller.SelectedRows[i]) then
      begin
         assert(Assigned(aTableView.Controller.SelectedRows[i]));

         v:=aTableView.Controller.SelectedRows[i].Values[iKeyFieldInd];

      //   j:=aTableView.Controller.SelectedRows[i].RecordIndex;

       //  v:=aTableView.DataController.Values[aTableView.Controller.SelectedRows[i].RecordIndex, iKeyFieldInd];

         s:=AsString(v);

  //      s:=aTableView.DataController.Values[SelectedRows[i].RecordIndex, iKeyFieldInd];

        oList.Add (s);
      //  aTableView.DataController.Values[SelectedRows[i].RecordIndex, iColumnInd]:=aChecked;
      end;
}

   oDataset:=aTableView.DataController.DataSet;;
   oDataset.DisableControls;
  // oDataset.DisableControls;



   with aTableView do
     for i := 0 to oList.Count - 1 do
      if oDataset.Locate (sKeyField, oList[i], []) then
        db_UpdateRecord (oDataset, [db_Par(aFieldName, aChecked)]);

  oDataset.EnableControls;

  oList.Free;
end;

{

procedure cx_AddComponentProp(aPropertiesStore: TcxPropertiesStore; aComponent: TComponent; aField: string);
var
  oStoreComponent: TcxPropertiesStoreComponent;
begin
  oStoreComponent :=  TcxPropertiesStoreComponent(aPropertiesStore.Components.Add);

  oStoreComponent.Component:=aComponent;
  oStoreComponent.Properties.Add(aField);
end;

}




{

 var i,j,iID: integer; b: boolean;
  s,sName: string;
  oDataset: TDataset;
  oNode: TdxTreeListNode;

//  oIDList: TIDList;
  oIDList: TStringLIst;
begin
  oIDList:=TStringList.Create;
//  oIDList:=TIDList.Create;

  with aGrid do
    for i := 0 to SelectedCount - 1 do
    begin
      oNode:=SelectedNodes[i];

      if Assigned(oNode) then
      begin
//       oIDList.AddID (oNode.Values [ColumnByFieldName(KeyField).Index]);
        s:=AsString(oNode.Values [ColumnByFieldName(KeyField).Index]);
        oIDList.Add (s);
      end;
    end;

   oDataset:=aGrid.DataSource.Dataset;

   oDataset.DisableControls;

   with aGrid do
     for i := 0 to oIDList.Count - 1 do
      if oDataset.Locate (KeyField, oIDList[i], []) then
        db_UpdateRecord (oDataset, [db_Par(aFieldName, aChecked)]);

   oDataset.EnableControls;


  oIDList.Free;
end;
}


  //   u_func

  //////////


{const

  DLL_PATH = 'common.dll';
}
{
  procedure cx_AddComponentProp(aPropertiesStore: TcxPropertiesStore; aComponent: TComponent; aField:
      string);

//  procedure dx_Init (aApplication: TApplication; aMouse: TMouse);

  procedure cx_CheckSelectedItems(aGrid: TcxGridDBTableView; aFieldName: string; aChecked: boolean); overload;
  procedure cx_CheckSelectedItems(aTableView: TcxGridDBBandedTableView; aFieldName: string; aChecked:
      boolean); overload;
}


{

//-------------------------------------------------------------------
procedure dx_CheckSelectedItems (aGrid: TdxDBGrid; aFieldName: String; aChecked: boolean);
//-------------------------------------------------------------------
begin
  dx_SetValuesForSelectedItems (aGrid, FLD_CHECKED, aChecked);
end;

}


{
//-----------------------------------------------------
procedure dx_MakeNodeFocusedByData (ATreeList: TdxTreeList; AData: Pointer; ANode: TdxTreeListNode=nil);
//-----------------------------------------------------
var node: TdxTreeListnode;
begin
  node:=dx_FindNodeByData (ATreeList, AData, ANode);
  if Assigned(node) then  dx_FocuseNode (node);
end;
}
