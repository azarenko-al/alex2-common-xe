unit u_Excel_to_Class;

interface
uses
  Classes, SysUtils, XMLDoc, XMLIntf,

  u_xml_document
  ;

type
  TXY = record X,Y : double; end;


  //-------------------------------------------------------------------
  TGraphItem = class(TCollectionItem)
  //-------------------------------------------------------------------
  public
    Name : String;

    Items: array of //TXY;
                    record
                      X,Y : double;
                    end;

    function GetY(aX : double): double;

 // zz  Name : String;
   // ID: Integer;
  end;



  //-------------------------------------------------------------------
  TGraphData = class(TCollection)
  //-------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TGraphItem;
  public
    constructor Create;

    function AddItem: TGraphItem;
    function FindByName(aName: string): TGraphItem;

    procedure LoadFromXMLFile(aFileName: string);
    procedure SaveToXMLFile(aFileName: string);

    property Items[Index: Integer]: TGraphItem read GetItems; default;
  end;




implementation


constructor TGraphData.Create;
begin
  inherited Create(TGraphItem);
end;


function TGraphData.AddItem: TGraphItem;
begin
  Result := TGraphItem (inherited Add);
end;

function TGraphData.FindByName(aName: string): TGraphItem;
var I: Integer;
begin
  Result := nil;

  for I := 0 to Count - 1 do
    if (Items[i].Name = aName) then
    begin
      Result := Items[i];
      Break;
    end;

end;

function TGraphData.GetItems(Index: Integer): TGraphItem;
begin
  Result := TGraphItem(inherited Items[Index]);
end;

//---------------------------------------------------------------------
procedure TGraphData.LoadFromXMLFile(aFileName: string);
//---------------------------------------------------------------------
var
  I: Integer;
  j: Integer;

  vItem,
  vGroup,
  vDocumentNode,
  vNodeSrc : IXMLNode;

  oDoc: TXmlDocumentEx;

  oGraphItem: TGraphItem;
  oGraphData: TGraphData;


begin
  DecimalSeparator:='.';

  oDoc:=TXmlDocumentEx.Create;
  oDoc.LoadFromFile(aFileName);

  vDocumentNode:=oDoc.Document;

  Clear;

  for I := 0 to vDocumentNode.ChildNodes.Count-1 do
  begin
    vGroup:=vDocumentNode.ChildNodes[i];

    oGraphItem:=AddItem;
    oGraphItem.Name:=vGroup.Attributes['name'];

    SetLength(oGraphItem.Items, vGroup.ChildNodes.Count);


    for j := 0 to vGroup.ChildNodes.Count-1 do
    begin
      vItem:=vGroup.ChildNodes[j];

      oGraphItem.Items[j].X := vItem.Attributes['x'];
      oGraphItem.Items[j].Y := vItem.Attributes['y'];


//      vItem:=vGroup.AddChild('item');
//      vItem.Attributes['x']:=Items[i].
//      vItem.Attributes['y']:=Items[i].Items[j].Y;

    end;

//
//
//    for j := 0 to dmExcel.RowsCount-1 do
//    begin
//      v1:=dmExcel.cells[j,0];
//      v2:=dmExcel.cells[j,1];
//
//      try
//
//      oGraphItem.Items[j].X:=v1;
//      oGraphItem.Items[j].Y:=v2;
//


//    vGroup:=vDocumentNode.AddChild('sheet');
//    vGroup.Attributes['name']:=Items[i].name;
//


  end;


  oDoc.Free;

end;


//---------------------------------------------------------------------
procedure TGraphData.SaveToXMLFile(aFileName: string);
//---------------------------------------------------------------------
var
  I: Integer;
  j: Integer;

  vItem,
  vGroup,
  vDocumentNode,
  vNodeSrc : IXMLNode;

  oDoc: TXmlDocumentEx;

begin
  DecimalSeparator:='.';

  oDoc:=TXmlDocumentEx.Create;

  vDocumentNode:=oDoc.Document;

  for I := 0 to Count-1 do
  begin
    vGroup:=vDocumentNode.AddChild('sheet');
    vGroup.Attributes['name']:=Items[i].name;

    for j := 0 to High(Items[i].Items) do
    begin
      vItem:=vGroup.AddChild('item');
      vItem.Attributes['x']:=Items[i].Items[j].X;
      vItem.Attributes['y']:=Items[i].Items[j].Y;
    end;
  end;


  oDoc.SaveToFile(aFileName);
  oDoc.Free;

end;

function TGraphItem.GetY(aX : double): double;
var
  I: Integer;
begin
  Result :=0;

  for I := 0 to High(Items)-1 do
    if (aX<=Items[i].X) then
      Result :=Items[i].Y;
end;


end.


{
unit u_collections;

interface

uses
  Classes, DB, u_sync_classes;

type
  //-------------------------------------------------------------------
  TClass = class(TCollectionItem)
  //-------------------------------------------------------------------
  public
    ID: Integer;

  public
    procedure LoadFromDataset(aDataset: TDataset);
  end;

  //-------------------------------------------------------------------
  TClassList = class(TCollection)
  //-------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TClass;
  public
    constructor Create;
    function AddItem: TClass;
    function FindByName(aName: string): TDBObject;
    procedure LoadFromDataset(aDataset: TDataset);

    property Items[Index: Integer]: TClass read GetItems; default;
  end;

implementation

//-------------------------------------------------------------------
procedure TClass.LoadFromDataset(aDataset: TDataset);
//-------------------------------------------------------------------
begin
  with aDataset do
    if not IsEmpty then
  begin
 (* //  ID :=FieldByName(FLD_Linkend_id).AsInteger;
    // -------------------------

    Azimuth           :=FieldByName(FLD_Azimuth).AsFloat;

    Gain_dB           :=FieldByName(FLD_GAIN).AsFloat;
    Diameter          :=FieldByName(FLD_Diameter).AsFloat;
    Polarization_str  :=FieldByName(FLD_POLARIZATION_str).Asstring;

    Height          :=FieldByName(FLD_Height).AsFloat;

    POLARIZATION_RATIO:=FieldByName(FLD_POLARIZATION_RATIO).AsFloat;

    // -------------------------
    BLPoint.B :=FieldByName(FLD_LAT).AsFloat;
    BLPoint.L :=FieldByName(FLD_LON).AsFloat;*)

  end;
end;

constructor TClassList.Create;
begin
  inherited Create(TClass);
end;

function TClassList.AddItem: TClass;
begin
  Result := TClass (inherited Add);
end;

function TClassList.FindByName(aName: string): TDBObject;
var I: Integer;
begin
 Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].Name, aName) then
    begin
      Result := Items[i];
      Break;
    end;
end;

function TClassList.GetItems(Index: Integer): TClass;
begin
  Result := TClass(inherited Items[Index]);
end;

procedure TClassList.LoadFromDataset(aDataset: TDataset);
begin
  aDataset.First;
  while not aDataset.EOF do
  begin
    AddItem.LoadFromDataset(aDataset);
    aDataset.Next;
  end;
end;

end.

//-----------------------------------------------------------------
function TdmParams_bin_to_MIF.SaveCellsSchemaXmlFile(aMatrixFileName, aMapFileName:
    string): Integer;
//-----------------------------------------------------------------
var
  I: Integer;
  oXMLDoc: TXMLDoc;
  vNode,vNodeSrc : IXMLNode;
begin
  oXMLDoc:=TXMLDoc.Create;

  xml_AddNode(oXMLDoc.DocumentElement, 'MATRIX',
                  [xml_Par('MatrixFileName', aMatrixFileName)]);

{  vNode :=xml_AddNodeTag(oXMLDoc.DocumentElement, 'COLOR_SCHEMAS');

  vNodeSrc:=FvColorSchemasNode.ChildNodes.Item(FColorSchemaList[i].Index);
}

/////////////  oXMLDoc.Document.ChildNodes. appendChild (FvCellsNode);

////////  oXMLDoc.SaveToFile( 'd:\_cells.xml');

  oXMLDoc.SaveToFile( ChangeFileExt(aMapFileName, '.xml'));
  oXMLDoc.Free;

  //dmParams.SaveColorSchemaXmlFile (FColorSchemaID, FMatrixFileName, FMapFileName);
end;

