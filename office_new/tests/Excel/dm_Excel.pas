﻿unit dm_Excel;

interface

uses
  SysUtils, Classes, OleServer, Forms, ExcelXP;

type
  TdmExcel = class(TDataModule)
    ExcelApplication1: TExcelApplication;
    ExcelWorksheet1: TExcelWorksheet;
    ExcelWorkbook1: TExcelWorkbook;
  private
    procedure OpenFile(aFileName: string);
    { Private declarations }
  public
    class procedure Init;

  end;

var
  dmExcel: TdmExcel;

implementation

{$R *.dfm}

class procedure TdmExcel.Init;
begin
  if not Assigned(dmExcel) then
    Application.CreateForm(TdmExcel, dmExcel);
end;


procedure TdmExcel.OpenFile(aFileName: string);
var
  //  XLSXReader: TExcelApplication;
  newWorkbook: _Workbook;
 // objWorkbook: TExcelWorkbook;
 // objWorksheet: TExcelWorksheet;
  i: Integer;
  k: Integer;
  m: Integer;
  v: Variant;


  FCells: OleVariant; //массив загруженных ячеек

begin
 //  XLSXReader:= ExcelApplication1;

    newWorkbook := ExcelApplication1.Workbooks.Add (aFileName, GetUserDefaultLCID);
    ExcelWorkbook := TExcelWorkbook.Create(nil);
    ExcelWorkbook.ConnectTo(newWorkbook);
    objExcelWorksheet := TExcelWorksheet.Create(nil);

//    objWorkbook.

    for i := 1 to ExcelWorkbook.Worksheets.count do
    begin
 //       objWorksheet.ConnectTo(objWorkbook.Worksheets[i] as _Worksheet);

      objExcelWorksheet.ConnectTo(ExcelWorkbook.Worksheets[i] as _Worksheet);


      v:=GetUserDefaultLCID;

      k:=objExcelWorksheet.UsedRange.Columns.Count;


      k:=objExcelWorksheet.UsedRange[GetUserDefaultLCID].Columns.Count;
      m:=objExcelWorksheet.UsedRange[GetUserDefaultLCID].Rows.Count;

      ComboBox1.Items.Add(objExcelWorksheet.Name);
    end;

    newWorkbook.Close(false, EmptyParam, EmptyParam, GetUserDefaultLCID);

    FreeAndNil(ExcelWorkbook);
    FreeAndNil(objExcelWorksheet);

end;

end.
