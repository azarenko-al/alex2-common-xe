unit dm_Excel_file;

interface

uses
  u_db,

  Windows, SysUtils, Dialogs, Classes, Forms, Variants, ExcelXP, OleServer,
  Excel2000, Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc;


  //http://basicsprog.ucoz.ru/blog/2011-09-30-8

  //http://www.delphikingdom.com/asp/viewitem.asp?catalogid=1274#04

type
  TdmExcel_file = class(TDataModule)
    ExcelApplication1: TExcelApplication;
    ExcelWorksheet1: TExcelWorksheet;
    ExcelWorkbook1: TExcelWorkbook;
//    procedure DataModuleCreate(Sender: TObject);
//    procedure DataModuleCreate(Sender: TObject);
  private
    FArrData: Variant;


//            s:=VarToStr(dmExcel.Cells[iRow,iCol]);


    FvWorkbook: _Workbook;

    function Get_CellValue_1(ARow, ACol: Cardinal): Variant;
    function Get_CellValue_0(ARow, ACol: Cardinal): Variant;
    
    procedure LoadWorksheetNames(aStrings: TStrings);
//    procedure Log(aMsg : string);

  public
    Active : Boolean;

    RowCount: Integer;
    ColCount: Integer;

    ExcelWorksheet_Name: string;

    FValueArr: array of array of Variant;


    function Open(aFileName: string): Boolean;    
    procedure Close;

//    procedure SaveToXML(aFileName: string);

    
    procedure GetSheets(aFileName: string; aStrings: TStrings);

    function LoadWorksheetByIndex_0(aIndex: Integer): Boolean;
    function LoadWorksheetByName(aName: string): Boolean;

    function LoadWorksheetNamesToArray: TArray<string>;
                             
    procedure SaveToText(aFileName: string);

    property Cells_1[ARow: Cardinal; ACol: Cardinal]: Variant read Get_CellValue_1;
    property Cells_0[ARow: Cardinal; ACol: Cardinal]: Variant read Get_CellValue_0;

  end;



  function dmExcel_file: TdmExcel_file;


implementation


{$R *.dfm}

var
  FdmExcel_file: TdmExcel_file;

// ---------------------------------------------------------------
function dmExcel_file: TdmExcel_file;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmExcel_file) then
    Application.CreateForm(TdmExcel_file, FdmExcel_file);

  Result := FdmExcel_file;
end;

//--------------------------------------------------------
function Eq(aValue1, aValue2: string): boolean;
//--------------------------------------------------------
begin
  Result := AnsiCompareText(Trim(aValue1),Trim(aValue2))=0;
end;
 

//
//
//procedure TdmExcel_file.DataModuleCreate(Sender: TObject);
//begin
//
//end;

// ---------------------------------------------------------------
procedure TdmExcel_file.GetSheets(aFileName: string; aStrings: TStrings);
// ---------------------------------------------------------------
begin
 // TdmExcel_file.Init;
  // Assert(FileExists(aFileName), aFileName);


  Open(aFileName);

  LoadWorksheetNames(aStrings);
  Close;

end;



//------------------------------------------------------------------------------
function TdmExcel_file.Get_CellValue_1(ARow, ACol: Cardinal): Variant;
//------------------------------------------------------------------------------
// ARow, ACol  0..count-1
begin
  Assert (ARow>0);
  Assert (ACol>0);

 // Inc(ARow);
 // Inc(ACol);

  Result:=Null;

  if not VarIsEmpty(FArrData) then
    if (ARow<=RowCount) and (ACol<=ColCount) then   
       try
        Result:=FArrData[ARow,ACol];
  //      Result:= dmExcel_file.Cells_1[aRow, aCol];
      except
        Result:=null;
      end;   

      
end;


//------------------------------------------------------------------------------
function TdmExcel_file.Get_CellValue_0(ARow, ACol: Cardinal): Variant;
//------------------------------------------------------------------------------
// ARow, ACol  0..count-1
begin
 // Inc(ARow);
 // Inc(ACol);

  Result:=Null;

  if not VarIsEmpty(FArrData) then
    if (ARow<RowCount) and (ACol<ColCount) then
      Result:=FArrData[ARow+1,ACol+1];
end;


//------------------------------------------------------------------------------
function TdmExcel_file.LoadWorksheetByIndex_0(aIndex: Integer): Boolean;
//------------------------------------------------------------------------------
var
  c: Integer;
  I: Integer;
  r: Integer;
  s: string;
//  vNewWorkbook: _Workbook;
  vExcelRange: ExcelRange;

  v: Variant;
begin    
// vNewWorkbook := ExcelApplication1.Workbooks.Add (aFileName, GetUserDefaultLCID);
//  ExcelWorkbook1.ConnectTo(vNewWorkbook);
  Result:=False;

  if aIndex<0 then
    Exit;


  if not Active then
    Exit;


 // Inc(aIndex);

  if aIndex<ExcelWorkbook1.Worksheets.count then
  begin
    ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[aIndex+1] as _Worksheet);


    ExcelWorksheet_Name:=ExcelWorksheet1.Name;

//      ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[i] as _Worksheet);
//
//    if Eq(ExcelWorksheet1.Name, aName) then
//    begin
//      iIndex:=i;
//      Break;
//    end;

    
//    ExcelWorksheet1.S
//    
//      if ExcelApp.Workbooks.Count >= AWorkbookIndex then
//  begin
//   AWorkbook := ExcelApp.Workbooks[AWorkbookIndex];
//   try
//    if AWorkbook.Worksheets.Count >= AWorksheetIndex then
//    begin;
//     ARange := AWorkbook.WorkSheets[AWorksheetIndex].Range[AWorkbook.WorkSheets[AWorksheetIndex].Cells[firstRow, firstCol],
//                                   AWorkbook.WorkSheets[AWorksheetIndex].Cells[lastRow, lastCol]];
//     Result := ARange.Value2;
//    end;
//   finally
//    AWorkbook := Unassigned;
//    ARange := Unassigned;
//   end;

    
    ColCount:=ExcelWorksheet1.UsedRange[0].Columns.Count;
    RowCount:=ExcelWorksheet1.UsedRange[0].Rows.Count;
                                     
    vExcelRange:=ExcelWorksheet1.UsedRange[0];
//    FArrData:=vExcelRange.Value;
    FArrData:=vExcelRange.Value2;

    Result := not VarIsEmpty(FArrData);

    SetLength(FValueArr, RowCount);
    for r := 0 to RowCount - 1 do
      SetLength(FValueArr[r], ColCount);


    for r := 0 to RowCount - 1 do
     for c := 0 to ColCount - 1 do
       FValueArr[r,c] := FArrData[r+1,c+1];

//            s:=VarToStr(dmExcel.Cells[iRow,iCol]);

 //   v:=FArrData[5,1];
  end;

  ExcelWorksheet1.Disconnect;
  
//  vNewWorkbook.Close(false, EmptyParam, EmptyParam, GetUserDefaultLCID);
end;

//------------------------------------------------------------------------------
function TdmExcel_file.LoadWorksheetByName(aName: string): Boolean;
//------------------------------------------------------------------------------
var
  iIndex: Integer;
  c: Integer;
  I: Integer;
  r: Integer;
//  vNewWorkbook: _Workbook;
  vExcelRange: ExcelRange;

  v: Variant;
begin
  Assert(aName<>'');

  SetLength(FValueArr, 0);

  
// vNewWorkbook := ExcelApplication1.Workbooks.Add (aFileName, GetUserDefaultLCID);
//  ExcelWorkbook1.ConnectTo(vNewWorkbook);
  Result:=False;

 // if aIndex<0 then
  //  Exit;


  if not Active then
    Exit;

  iIndex:=-1;

  for i := 1 to ExcelWorkbook1.Worksheets.count do
  begin
    ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[i] as _Worksheet);

    if Eq(ExcelWorksheet1.Name, aName) then
    begin
      iIndex:=i;
      Break;
    end;
  end;



//    ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[i] as _Worksheet);

  if iIndex<0 then
    Exit;
    

 // Inc(aIndex);

  if iIndex<=ExcelWorkbook1.Worksheets.count then
  begin
//    ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[aIndex+1] as _Worksheet);
    ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[iIndex] as _Worksheet);

    ColCount:=ExcelWorksheet1.UsedRange[0].Columns.Count;
    RowCount:=ExcelWorksheet1.UsedRange[0].Rows.Count;

    if (RowCount<=1) or (ColCount<=0) then
      Exit;

                                     
    vExcelRange:=ExcelWorksheet1.UsedRange[0];
    FArrData:=vExcelRange.Value2;

    Result := not VarIsEmpty(FArrData);
    
    
    SetLength(FValueArr, RowCount);
    for r := 0 to RowCount - 1 do
      SetLength(FValueArr[r], ColCount);

            

    for r := 0 to RowCount - 1 do
     for c := 0 to ColCount - 1 do
     try
       FValueArr[r,c] := FArrData[r+1,c+1];
     except
       ShowMessage( varToStr(FArrData[r+1,c+1]));
     
     end;
//            s:=VarToStr(dmExcel.Cells[iRow,iCol]);

 //   v:=FArrData[5,1];
  end;


  ExcelWorksheet1.Disconnect;
  
//  vNewWorkbook.Close(false, EmptyParam, EmptyParam, GetUserDefaultLCID);
end;



//------------------------------------------------------------------------------
procedure TdmExcel_file.LoadWorksheetNames(aStrings: TStrings);
//------------------------------------------------------------------------------
var
  i: Integer;
begin

  aStrings.Clear;

  if not Active then
    Exit;


  for i := 1 to ExcelWorkbook1.Worksheets.count do
  begin
    ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[i] as _Worksheet);
    aStrings.Add(ExcelWorksheet1.Name);
  end;

end;

// ---------------------------------------------------------------
function TdmExcel_file.Open(aFileName: string): Boolean;
// ---------------------------------------------------------------
var
  s: string;
  v: Variant;
begin
  Active := False;

   Assert(FileExists(aFileName), aFileName);
  

  if FileExists(aFileName) then
  begin
//    Log('TdmExcel_file.Open');

    // ������ EXCEL
  //  vExcel := CreateOleObject('Excel.Application');

    // ���� �� ������� ������ � ���������� ���������
   // vExcel.DisplayAlerts := false;
    // ����� ��������
   // excel.WorkBooks.Add;

     try
      FvWorkbook := ExcelApplication1.Workbooks.Add (aFileName, 0);
//..      FvWorkbook := ExcelApplication1.Workbooks.Add (aFileName, GetUserDefaultLCID);
    except
      ShowMessage(aFileName);

    end;

    v:=FvWorkbook.FileFormat[0];

//    s:= ChangeFileExt(aFileName,'.xlsx');  
//    FvWorkbook.SaveCopyAs(s,  GetUserDefaultLCID);
    
    
    //xlWorkbookDefault	51	Workbook default
    
    
  //  Log('ExcelWorkbook1.ConnectTo(FvWorkbook);');


    ExcelWorkbook1.ConnectTo(FvWorkbook);


    
    
    Active := True;

//    Assert();
  end;

  Result:=Active;
end;


procedure TdmExcel_file.Close;
begin
 //  Log('TdmExcel_file.Close');
  ExcelWorkbook1.Disconnect;

  if Active then
  begin
    FvWorkbook.Close(false, EmptyParam, EmptyParam, 0);
  //  FvWorkbook:=nil;
    Active := False;
  end;
end;

// ---------------------------------------------------------------
function TdmExcel_file.LoadWorksheetNamesToArray: TArray<string>;
// ---------------------------------------------------------------
var
  i: Integer;
begin
  SetLength(result,  ExcelWorkbook1.Worksheets.count);
  
  for i := 1 to ExcelWorkbook1.Worksheets.count do 
  begin
    ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[i] as _Worksheet);
    result[i-1]:=ExcelWorksheet1.Name;
  end;  
  
end;



procedure TdmExcel_file.SaveToText(aFileName: string);
var
  iCol: Integer;
  iRow: Integer;
  oList: TstringList;
  s: string;

begin
  oList:=TstringList.Create;

  

  for iRow := 1 to RowCount-1 do
  begin
    s:='';

    for iCol := 0 to ColCount-1 do
    begin      
      s:=s+ VarToStr(Cells_0[iRow,iCol]) + ';' ;

      //sTag.Replace('�','N');
//      
//      sTag:=sTag.Replace('�','N').
//                 Replace('-','').
//                 Replace(' ','').
//                 Replace('(','').
//                 Replace(')','');                 
      
    end; 

    oList.Add (s);
  end;

  
  oList.SaveToFile(aFileName);
  
//  k:=RowCount;

end;



end.


{

procedure TdmExcel_file.SaveToXML(aFileName: string);
var
  iCol: Integer;
  iRow: Integer;
  oRow: IXMLNode;
  oRoot: IXMLNode;
  s: string;
  sTag: string;
begin
  XmlDocument1.Version:='1.0';

//
//  XmlDocument1.DOMDocument.appendChild(
//      XmlDocument1.DOMDocument.createProcessingInstruction(
//      'xml-stylesheet', 'type="text/xsl" href=""'));


 // XmlDocument.Encoding:='UTF-8';

 // XMLDocument1.Encoding:='windows-1251';

      //<?xml version="1.0" encoding="windows-1251"?>
  //--------------------------------------------

  XMLDocument1.ChildNodes.Clear;
  oRoot:=XMLDocument1.AddChild('Document');

  for iRow := 1 to RowCount-1 do
  begin
    oRow:= oRoot.AddChild('row');

    for iCol := 0 to ColCount-1 do
    begin
      sTag:= LowerCase( VarToStr(Cells_0[0,iCol]) );
      s:=VarToStr(Cells_0[iRow,iCol]);

      //sTag.Replace('�','N');
//
//      sTag:=sTag.Replace('�','N').
//                 Replace('-','').
//                 Replace(' ','').
//                 Replace('(','').
//                 Replace(')','');

      oRow.AddChild(sTag).Text:= s;

    end;

  end;


  XMLDocument1.SaveToFile(aFileName);

//  k:=RowCount;

end;


