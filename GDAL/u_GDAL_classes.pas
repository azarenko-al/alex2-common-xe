﻿unit u_GDAL_classes;

interface
uses
  Graphics, StrUtils, System.SysUtils, Dialogs,

  Data.DBXJSON,
 // JSON,
  IOUtils;
  


type
  // ---------------------------------------------------------------
  TGDAL_json_rec = record
  // ---------------------------------------------------------------
  type
    Tcorner = record
                x,y: double;
              end;
  private

  public

    Size: record
            rows,cols: LongWord;
          end;

    coordinateSystem : record
      wkt : string;
    end;

    CornerCoordinates: record
      upperLeft,
      lowerLeft,
    
      lowerRight,
      upperRight : Tcorner;
    end;

    Bounds : record
                Lat_min: Double;
                Lat_max: double;

                Lon_min: Double;
                Lon_max: double;
             end;



    Points_WGS: array of record
                      Lat,Lon: double;
                    end;
    Bands : record
                Type_: string;
                noDataValue: double;
             end;

    Zone_UTM: byte;


    function LoadFromFile(aFileName: string): Boolean;
    function ToGeometry: string;

    procedure SaveToFile_TAB_3395(aFileName, aImgFileName: string);
    procedure SaveToFile_TAB_Pulkovo(aFileName, aImgFileName: string);


  end;

  // ---------------------------------------------------------------

  TGDAL_Bounds = record
                  Lat_min: Double;
                  Lat_max: double;

                  Lon_min: Double;
                  Lon_max: double;
               end;

  // ---------------------------------------------------------------
  TGDAL_Envi_HDR = record
  // ---------------------------------------------------------------
  private
    procedure SaveToFile_HDR_Pulkovo(aFileName: string);
    procedure SaveToFile_HDR_Conic(aFileName: string);
    procedure SaveToFile_HDR_UTM(aFileName: string);
  public
    ColCount,
    RowCount: word;

    Lon_min,
    Lat_max: double;

    CellSize: double;

    DataType: byte;   //   'data type = :data type '+ LF+
    Zone_UTM: byte;
    Zone_GK: byte;

    Null_value: integer;
//    data_ignore_value: integer;

    PROJCS: string;

    procedure Clear;
    procedure SaveToFile(aFileName: string);

  end;


implementation



//uses
//  System.JSON;


function ReplaceStr (Value, OldPattern, NewPattern: string): string;
begin
  Result:=StringReplace (Value, OldPattern, NewPattern, [rfReplaceAll, rfIgnoreCase]);
end;


function geo_Get6ZoneL(aLon: double): integer;
begin
//in degree
  Result:=Trunc(aLon/6) + 1; //Номер 6-градусной зоны
end;



// ---------------------------------------------------------------
function TGDAL_json_rec.LoadFromFile(aFileName: string): Boolean;
// ---------------------------------------------------------------

      procedure DoGetCorner(aJSONArray: TJSONArray; var aCorner: Tcorner);
      var
        s1: string;
        s2: string;  
      begin      
        s1:=(aJSONArray.Get(0) as TJSONValue).Value;
        s2:=(aJSONArray.Get(1) as TJSONValue).Value;
                 

        aCorner.y:=s1.ToDouble();
        aCorner.x:=s2.ToDouble();      


        assert (aCorner.x<>0);
        assert (aCorner.y<>0);      
            
      end;


var
  I: Integer;
  k1: Integer;
  k2: Integer;
  k3: Integer;
  oCornerCoordinates: TJSONObject;
  oNode: TJSONObject;
  oObject: TJSONObject;
  oWgs84Extent: TJSONObject;


  oValue: TJSONValue;

  oArray: TJSONArray;
  oArray1: TJSONArray;
  oArray2: TJSONArray;
  obands: TJSONArray;
  oJSONObject: TJSONObject;
  oJSONValue: TJSONValue;
  s: string;

  s1: string;
  s2: string;
  v: Variant;


begin
  FillChar(Self, SizeOf(Self),0);

  Result:=False;

  aFileName:=ChangeFileExt(aFileName,'.json');
  Assert (FileExists (aFileName), aFileName);

 // FileName:=aFileName;
  Assert (TFile.ReadAllText(aFileName) <> '', 'blank file: ' + aFileName);



  
  oObject := TJSONObject.ParseJSONValue(TFile.ReadAllText(aFileName)) as TJSONObject;

  if not Assigned (oObject) then
    Exit;
  
  Assert (Assigned (oObject));
        
// ---------------------------------------------------------------
// size
// ---------------------------------------------------------------
  oArray:=oObject.GetValue('size') as TJSONArray;
    Size.cols:=(oArray.Get(0) as TJSONValue).Value.ToInteger;
    Size.rows:=(oArray.Get(1) as TJSONValue).Value.ToInteger;

//      "size":[
//    4479,
//    5043
//  ],


  assert (Size.rows>0);
  assert (Size.cols>0);

// ---------------------------------------------------------------
  oCornerCoordinates:=oObject.GetValue('cornerCoordinates') as TJSONObject;;

    DoGetCorner (oCornerCoordinates.GetValue('upperLeft')  as TJSONArray, CornerCoordinates.upperLeft);
    DoGetCorner (oCornerCoordinates.GetValue('lowerLeft')  as TJSONArray, CornerCoordinates.lowerLeft);
    DoGetCorner (oCornerCoordinates.GetValue('upperRight') as TJSONArray, CornerCoordinates.upperRight);
    DoGetCorner (oCornerCoordinates.GetValue('lowerRight') as TJSONArray, CornerCoordinates.lowerRight);

// ---------------------------------------------------------------
// ---------------------------------------------------------------
  oNode:=oObject.GetValue('coordinateSystem') as TJSONObject;;
  if Assigned(oNode) then
  begin
    coordinateSystem.wkt:= lowerCase( oNode.GetValue('wkt').Value );

    k1:=Pos('utm zone', coordinateSystem.wkt);
    if k1>0 then
    begin
      k2:=Pos('"', coordinateSystem.wkt, k1);
      k3:=k1+ Length('utm zone ');
      s:=Copy(coordinateSystem.wkt, k3, k2 - k3-1);

      Zone_UTM:=StrToIntDef(s, 0);

    end;

    //Gauss-Kruger zone 8
//    "wkt":"PROJCRS[\"Pulkovo 1942 \/ Gauss-Kruger zone 8\",\n    BASEGEOGCRS[\"Pulkovo 1942\",\n        DATUM[\"Pulkovo 1942\",\n            ELLIPSOID[\"Krassowsky 1940\",6378245,298.3,\n                LENGTHUNIT[\"metre\",1]]],\n        PRIMEM[\"Greenwich\",0,\n            ANGLEUNIT[\"degree\",0.0174532925199433]],\n        ID[\"EPSG\",4284]],\n    CONVERSION[\"6-degree Gauss-Kruger zone 8\",\n        METHOD[\"Transverse Mercator\",\n            ID[\"EPSG\",9807]],\n        PARAMETER[\"Latitude of natural origin\",0,\n            ANGLEUNIT[\"degree\",0.0174532925199433],\n            ID[\"EPSG\",8801]],\n        PARAMETER[\"Longitude of natural origin\",45,\n            ANGLEUNIT[\"degree\",0.0174532925199433],\n            ID[\"EPSG\",8802]],\n        PARAMETER[\"Scale factor at natural origin\",1,\n            SCALEUNIT[\"unity\",1],\n            ID[\"EPSG\",8805]],\n        PARAMETER[\"False easting\",8500000,\n            LENGTHUNIT[\"metre\",1],\n            ID[\"EPSG\",8806]],\n        PARAMETER[\"False northing\",0,\n            LENGTHUNIT[\"metre\",1],\n            ID[\"EPSG\",8807]]],\n    CS[Cartesian,2],\n        AXIS[\"northing (X)\",north,\n            ORDER[1],\n            LENGTHUNIT[\"metre\",1]],\n        AXIS[\"easting (Y)\",east,\n            ORDER[2],\n            LENGTHUNIT[\"metre\",1]],\n    USAGE[\n        SCOPE[\"unknown\"],\n        AREA[\"Europe - FSU onshore 42В°E to 48В°E\"],\n        BBOX[38.84,42,80.91,48.01]],\n    ID[\"EPSG\",28408]]",

    k1:=Pos('gauss-kruger zone', coordinateSystem.wkt);
    if k1>0 then
    begin
      k2:=Pos('"', coordinateSystem.wkt, k1);
      k3:=k1+ Length('gauss-kruger zone ');
      s:=Copy(coordinateSystem.wkt, k3, k2 - k3);

      Zone_UTM:=StrToIntDef(s, 0) + 30;

    end;


  end;




//    "coordinateSystem":{
//    "wkt":"PROJCRS[\"WGS 84 \/ UTM Zone_UTM 48N\",\n    BASEGEOGCRS[\"WGS 84\",\n
//    "dataAxisToSRSAxisMapping":[
//      1,
//      2
//    ]



// ---------------------------------------------------------------
// wgs84Extent
// ---------------------------------------------------------------
  oWgs84Extent:=oObject.GetValue('wgs84Extent') as TJSONObject;

  if Assigned(oWgs84Extent) then
  begin
    oArray:=oWgs84Extent.GetValue('coordinates') as TJSONArray;

    oArray1:=(oArray.Get(0) as TJSONArray); //.Value;

  //  k:=oArray1.Size;

    SetLength (Points_WGS, oArray1.Size);

    for I := 0 to oArray1.Size-1 do
    begin
      oArray2:=(oArray1.Get(i) as TJSONArray);

      Points_WGS[i].Lon:=(oArray2.Get(0) as TJSONValue).Value.ToDouble;
      Points_WGS[i].Lat:=(oArray2.Get(1) as TJSONValue).Value.ToDouble;

      if Points_WGS[i].Lon < 0 then
        Points_WGS[i].Lon:=360 + Points_WGS[i].Lon;


    end;



  end;

// ---------------------------------------------------------------
// bands
// ---------------------------------------------------------------
  oBands:=oObject.GetValue('bands') as TJSONArray;
  if Assigned(oBands) then
  begin
    oJSONObject:=oBands.Get(0) as TJSONObject;

    Bands.Type_      :=oJSONObject.GetValue('type').Value;

    oJSONValue:=oJSONObject.GetValue('noDataValue');
    if Assigned(oJSONValue) then
       Bands.noDataValue:= StrToFloat( oJSONValue.Value);
  end;

//    Bands.Type_:=(oBands.Get(0) as TJSONValue).Value.ToInteger;


  Bounds.Lat_max:=CornerCoordinates.upperLeft.x;
  Bounds.Lat_min:=CornerCoordinates.lowerRight.x;

  Bounds.Lon_min:=CornerCoordinates.upperLeft.y;
  Bounds.Lon_max:=CornerCoordinates.lowerRight.y;




  Result:=True;
  
end;

 
//------------------------------------------------------
procedure TGDAL_json_rec.SaveToFile_TAB_Pulkovo(aFileName, aImgFileName:
    string);
//------------------------------------------------------
// ñôîðìèðîâàòü çàãîëîâîê äëÿ ôàéëà Mapinfo
// - Bounds - êîîðäèíàòû öåíòðîâ êðàéíèõ ïëîùàäîê
//------------------------------------------------------
const
  CRLF = #13+#10;

const

  DEF_FILE = 
  '!table'+ CRLF +
  '!version 300'+ CRLF +
  '!charset WindowsCyrillic'+ CRLF +
  ''+ CRLF +
  'Definition Table'+  CRLF +
  '  File ":file"'+ CRLF +
  '  Type "RASTER"'+  CRLF +
  
  '  (:point1_lon,:point1_lat) (:img_width,:img_height) Label "point 1",'+ CRLF +
  '  (:point2_lon,:point2_lat) (0,:img_height) Label "point 2",'+CRLF +
  '  (:point3_lon,:point3_lat) (0,0) Label "point 3",'+ CRLF +
  '  (:point4_lon,:point4_lat) (:img_width,0) Label "point 4"'+ CRLF +

  ' CoordSys Earth Projection 8, 1001, "m", :Zone_data ' + CRLF + //39, 0, 1, 7500000, 0 ' + CRLF +

//  '  CoordSys Earth Projection 8, 1001, "m", :central_meridian, 0, 1, :false_easting, 0' + CRLF + //39, 0, 1, 7500000, 0 ' + CRLF +
  '  Units "m" '+ CRLF +

//    'PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",:central_meridian],'+
//    'PARAMETER["scale_factor",1],PARAMETER["false_easting",:false_easting],PARAMETER["false_northing",0],UNIT["Meter",1]]}'+ LF+ 

                   
//  '  CoordSys Earth Projection 1, 104'+ CRLF +
//  '  CoordSys Earth Projection 10, 104, "m", 0'+ CRLF +
 // '  Units "m"'+ CRLF +
//  '  Units "degree"'+ CRLF +  
  '  RasterStyle 4 1'+ CRLF +
  '  RasterStyle 7 0'; //+ CRLF +
     
  {
  ZONE_INFO_ARR : array[4..29] of string =
    (
      '21, 0, 1,  4500000, 0',  //5
      '27, 0, 1,  5500000, 0',  //5
      '33, 0, 1,  6500000, 0',  //6
      '39, 0, 1,  7500000, 0',  //7
      '45, 0, 1,  8500000, 0',  //8
      '51, 0, 1,  9500000, 0',  //9
      '57, 0, 1, 10500000, 0',  //10
      '63, 0, 1, 11500000, 0',  //11
      '69, 0, 1, 12500000, 0',  //12

      '75, 0, 1, 13500000, 0',  //13
      '81, 0, 1, 14500000, 0',  //14
      '87, 0, 1, 15500000, 0',  //15
      '93, 0, 1, 16500000, 0',  //16
      '99, 0, 1, 17500000, 0',  //17
     '105, 0, 1, 18500000, 0',  //18
     '111, 0, 1, 19500000, 0',   //19

     '117, 0, 1, 20500000, 0',   //19
     '123, 0, 1, 21500000, 0',   //19
     '129, 0, 1, 22500000, 0',   //19
     '135, 0, 1, 23500000, 0',   //19
     '141, 0, 1, 24500000, 0',   //19

     '147, 0, 1, 25500000, 0',   //19
     '153, 0, 1, 26500000, 0',   //19
     '159, 0, 1, 27500000, 0',   //19
     '165, 0, 1, 28500000, 0',   //19
     '171, 0, 1, 29500000, 0'   //19


    );
   }
  
var
  sZone: string;

  FPoints: array[0..3] of TCorner;
   
//  xyPoints : array[0..3] of TXYPoint;
//  bmpPoints: array[0..3] of  record x,y: integer end;

  i: integer;
  iZone: Integer;
  s: string;
  
  sFile: string;
   
begin
  aFileName:=ChangeFileExt(aFileName, '.tab');

  // we get center points of cells

  s:=DEF_FILE;

  sFile:=ExtractFileName(aImgFileName);
  
  s:=ReplaceStr(s, ':file', sFile);
  
  s:=ReplaceStr(s, ':img_width',  IntToStr(Size.cols));
  s:=ReplaceStr(s, ':img_height', IntToStr(Size.rows));  


  FPoints[0]:=CornerCoordinates.lowerRight;
  FPoints[1]:=CornerCoordinates.lowerLeft;  
  FPoints[2]:=CornerCoordinates.upperLeft;  
  FPoints[3]:=CornerCoordinates.upperRight;


  for I := 0 to 3 do
  begin
    s:=ReplaceStr(s, Format(':point%d_lon',[i+1]), FloatToStr(FPoints[i].y));
    s:=ReplaceStr(s, Format(':point%d_lat',[i+1]), FloatToStr(FPoints[i].x));

  end;



  s:=ReplaceStr(s, ':central_meridian', IntToStr(Zone_UTM*6 - 3));
  s:=ReplaceStr(s, ':false_easting',    IntToStr(Zone_UTM*10000000 + 5000000));

  //  ' CoordSys Earth Projection 8, 1001, "m", :central_meridian, 0, 1,  :false_easting, 0' + CRLF + //39, 0, 1, 7500000, 0 ' + CRLF +

  Assert (Zone_UTM > 0);


  iZone:=Zone_UTM-30;
  sZone:= Format( '%d, 0, 1,  %d, 0', [iZone*6-3, iZone*1000000 + 500000 ]);

  s:=ReplaceStr (s, ':Zone_data',   sZone);

//
//  if (Zone_UTM >= Low(ZONE_INFO_ARR)) and
//     (Zone_UTM <= High(ZONE_INFO_ARR))
//  then
//    sZone:=ZONE_INFO_ARR[Zone_UTM]
//  else
//    raise Exception.Create('');
////    sZone:='';
//
//  s:=ReplaceStr (s, ':Zone_data',   sZone);


//  oGDAL_bat.SaveToFile (sFile_bat, TEncoding.GetEncoding(866));

 // DeleteFile(aFileName);

  IOUtils.TFile.WriteAllText(aFileName, s, TEncoding.GetEncoding(1251));


end;


// ---------------------------------------------------------------
procedure TGDAL_json_rec.SaveToFile_TAB_3395(aFileName, aImgFileName: string);
// ---------------------------------------------------------------

const

  CRLF = #13+#10;
  LF = CRLF;
  CR = CRLF;


  DEF_FILE = 
  '!table'+ CRLF +
  '!version 300'+ CRLF +
  '!charset WindowsCyrillic'+ CRLF +
  ''+ CRLF +
  'Definition Table'+  CRLF +
  '  File ":file"'+ CRLF +
  '  Type "RASTER"'+  CRLF +
  
  '  (:point1_lon,:point1_lat) (:img_width,:img_height) Label "point 1",'+ CRLF +
  '  (:point2_lon,:point2_lat) (0,:img_height) Label "point 2",'+CRLF +
  '  (:point3_lon,:point3_lat) (0,0) Label "point 3",'+ CRLF +
  '  (:point4_lon,:point4_lat) (:img_width,0) Label "point 4"'+ CRLF +
                   
//  '  CoordSys Earth Projection 1, 104'+ CRLF +
  '  CoordSys Earth Projection 10, 104, "m", 0'+ CRLF +
 // '  Units "m"'+ CRLF +
//  '  Units "degree"'+ CRLF +  
  '  RasterStyle 4 1'+ CRLF +
  '  RasterStyle 7 0'; //+ CRLF +
     
           
var
 
  s: string;  

  I: Integer;
  myEncoding: TEncoding;

  sFile: string;

 // oSList: TStringList;

  FPoints: array[0..3] of TCorner;

begin
  aFileName:=ChangeFileExt(aFileName, '.tab');

  //oSList:=TStringList.Create;;


  s:=DEF_FILE;

  sFile:=ExtractFileName(aImgFileName);
  
  s:=ReplaceStr(s, ':file', sFile);
  
  s:=ReplaceStr(s, ':img_width',  IntToStr(Size.cols));
  s:=ReplaceStr(s, ':img_height', IntToStr(Size.rows));  


  FPoints[0]:=CornerCoordinates.lowerRight;
  FPoints[1]:=CornerCoordinates.lowerLeft;  
  FPoints[2]:=CornerCoordinates.upperLeft;  
  FPoints[3]:=CornerCoordinates.upperRight;
  

  for I := 0 to 3 do
  begin
    s:=ReplaceStr(s, Format(':point%d_lon',[i+1]), FloatToStr(FPoints[i].y));
    s:=ReplaceStr(s, Format(':point%d_lat',[i+1]), FloatToStr(FPoints[i].x));

  end;


  
//  s:=ReplaceStr(s, ':central_meridian',  IntToStr(Zone_UTM*6-3));
//  s:=ReplaceStr(s, ':false_easting',     IntToStr(Zone_UTM*1000000 + 500000));

                 

 // DeleteFile(aFileName);
  IOUtils.TFile.WriteAllText(aFileName, s, TEncoding.GetEncoding(1251));

end;


function TGDAL_json_rec.ToGeometry: string;
var
 // arr: TStringDynArray;
  I: Integer;
begin
  Result:='POLYGON((';

  for I := 0 to High(Points_WGS) do
    Result:=Result+ Format('%1.6f %1.6f',[Points_WGS[i].Lon, Points_WGS[i].Lat]) +
                    IfThen(i<High(Points_WGS), ', ','');

  Result:=Result + '))';


//
//DECLARE @g geography;
//SET @g = geography::STPolyFromText('POLYGON((-122.358 47.653, -122.348 47.649, -122.348 47.658, -122.358 47.658, -122.358 47.653))', 4326);
//SELECT @g.ToString();

//  Result := ;
end;




// ---------------------------------------------------------------
procedure TGDAL_Envi_HDR.Clear;
// ---------------------------------------------------------------
begin
  FillChar (Self, SIZEOF(Self), 0);

  Null_value:=-999;
end;


// ---------------------------------------------------------------
procedure TGDAL_Envi_HDR.SaveToFile(aFileName: string);
// ---------------------------------------------------------------
begin
  if PROJCS<>'' then
    SaveToFile_HDR_Conic (aFileName)

  else if Zone_UTM>0 then
    SaveToFile_HDR_UTM(aFileName)

  else if Zone_GK>0 then
    SaveToFile_HDR_Pulkovo(aFileName)

  else
    raise Exception.Create('Error Message');

  //ChangeFileExt(aFileName,'.hdr')

end;

  
// ---------------------------------------------------------------
procedure TGDAL_Envi_HDR.SaveToFile_HDR_Pulkovo(aFileName: string);
// ---------------------------------------------------------------
//  sParam:= Format(' -a_srs "+proj=utm  +zone=%d +datum=WGS84" -of GTiff  -co "COMPRESS=LZW" -a_ullr %1.12f  %1.12f %1.12f %1.12f  %s %s ',

const
  CRLF = #13+#10;
  LF = CRLF;      
  
const

  DEF_old =
    'ENVI'+ LF+
    'description = {'+ LF+
    ' :description}'+ LF+
    'samples = :samples'+ LF+
    'lines   = :lines'+ LF+
    'bands   = 1 '+ LF+
    'header offset = 0  '+ LF+
    'file type = ENVI Standard '+ LF+
    'data type = :data type '+ LF+
    'interleave = bsq '+ LF+
    'byte order = 0 '+ LF+
  //      'map info = {Transverse Mercator,  1, 1, %d, %d, %d, %d, %d}';
//map info = {Transverse Mercator, 1, 1, 29376365.9767268, 8018695.73473788, 100.040318333087, 100.040318333087}

//    'map info = {Transverse Mercator, 1, 1, :Lon_min, :Lat_max, :CellSize, :CellSize}' + LF+

//    'map info = {Transverse Mercator,  1, 1, %d, %d,  %d, %d,  %d}';
    'map info = {Transverse Mercator, 1, 1, :Lon_min,:Lat_max, :CellSize,:CellSize, :Zone}' ; //+ LF+

//    'map info = {UTM, 1, 1, :Lon_min, :Lat_max, :CellSize, :CellSize, :Zone, North,WGS-84}' + LF+
    
//map info = {Transverse Mercator, 1, 1, 29376365.9767268, 8018695.73473788, 100.040318333087, 100.040318333087}

//    'projection info = {3, 6378245, 6356863.018773047, 0, :center_minus_3, :center_meters, 0, 1, Transverse Mercator}'+ LF+
////    'projection info = {3, 6378245, 6356863.018773047, 0, 177, 30500000, 0, 1, Transverse Mercator}'+ LF+
//    'coordinate system string = {PROJCS["Pulkovo_1942_Gauss_Kruger_zone_:zone",GEOGCS["GCS_Pulkovo 1942",DATUM["D_Pulkovo_1942",SPHEROID["Krasovsky_1940",6378245,298.3]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],'+
//    'PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",:central_meridian],'+
//    'PARAMETER["scale_factor",1],PARAMETER["false_easting",:false_easting],PARAMETER["false_northing",0],UNIT["Meter",1]]}'+ LF+
//    'band names = {'+ LF+
//    'Band 1}';

  DEF =
    'ENVI'+ LF+
    'description = {'+ LF+
    ' :description}'+ LF+
    'samples = :samples'+ LF+
    'lines   = :lines'+ LF+
    'bands   = 1 '+ LF+
    'header offset = 0  '+ LF+
    'file type = ENVI Standard '+ LF+
    'data type = :data type '+ LF+
    'interleave = bsq '+ LF+
    'byte order = 0 '+ LF+
    'data ignore value = :data ignore value'+ LF+

     //data ignore value   --The value of NODATA in this dataset.

  //      'map info = {Transverse Mercator,  1, 1, %d, %d, %d, %d, %d}';
//map info = {Transverse Mercator, 1, 1, 29376365.9767268, 8018695.73473788, 100.040318333087, 100.040318333087}

    'map info = {Transverse Mercator, 1, 1, :Lon_min, :Lat_max, :CellSize, :CellSize}' + LF+

    'projection info = {3, 6378245, 6356863.018773047, 0, :center_minus_3, :center_meters, 0, 1, Transverse Mercator}'+ LF+
////    'projection info = {3, 6378245, 6356863.018773047, 0, 177, 30500000, 0, 1, Transverse Mercator}'+ LF+
    'coordinate system string = {PROJCS["Pulkovo_1942_Gauss_Kruger_zone_:zone",GEOGCS["GCS_Pulkovo 1942",DATUM["D_Pulkovo_1942",SPHEROID["Krasovsky_1940",6378245,298.3]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],'+
    'PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",:central_meridian],'+
    'PARAMETER["scale_factor",1],PARAMETER["false_easting",:false_easting],PARAMETER["false_northing",0],UNIT["Meter",1]]}'+ LF+

    'band names = {'+ LF+
    'Band 1}';


//    'map info = {Transverse Mercator,  1, 1, %d, %d,  %d, %d,  %d}';
//    'map info = {Transverse Mercator, 1, 1, :Lon_min,:Lat_max, :CellSize,:CellSize, :Zone}' ; //+ LF+


(*
ENVI
description = {
28_DV_AmurObl_utm52_100m_work_dtm_.envi}
samples = 9934
lines   = 9618
bands   = 1
header offset = 0
file type = ENVI Standard
data type = 2
interleave = bsq
byte order = 0
map info = {Transverse Mercator, 1, 1, 21909536.123, 6355534.913, 100, 100}
projection info = {3, 6378245, 6356863.018773047, 0, 129, 22500000, 0, 1, Transverse Mercator}
coordinate system string = {PROJCS["Pulkovo_1942_GK_Zone_22",GEOGCS["GCS_Pulkovo_1942",DATUM["D_Pulkovo_1942",SPHEROID["Krasovsky_1940",6378245.0,298.3]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433]],PROJECTION["Gauss_Kruger"],PARAMETER["False_Easting",22500000.0],PARAMETER["False_Northing",0.0],PARAMETER["Central_Meridian",129.0],PARAMETER["Scale_Factor",1.0],PARAMETER["Latitude_Of_Origin",0.0],UNIT["Meter",1.0]]}
band names = {
Band 1}
  *)

//
//map info = {Transverse Mercator, 1, 1, 21909536.123, 6355534.913, 100, 100}
//projection info = {3, 6378245, 6356863.018773047, 0, 129, 22500000, 0, 1, Transverse Mercator}
//coordinate system string = {PROJCS["Pulkovo_1942_GK_Zone_22",GEOGCS["GCS_Pulkovo_1942",DATUM["D_Pulkovo_1942",SPHEROID["Krasovsky_1940",6378245.0,298.3]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433]],PROJECTION["Gauss_Kruger"],PARAMETER["False_Easting",22500000.0],PARAMETER["False_Northing",0.0],PARAMETER["Central_Meridian",129.0],PARAMETER["Scale_Factor",1.0],PARAMETER["Latitude_Of_Origin",0.0],UNIT["Meter",1.0]]}
//band names = {
//Band 1}
//

                
// coordinate system string = {PROJCS["Pulkovo_1942_Gauss_Kruger_zone_7", GEOGCS["GCS_Pulkovo 1942",DATUM["D_Pulkovo_1942",SPHEROID["Krasovsky_1940",6378245,298.3]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",39], PARAMETER["scale_factor",1],PARAMETER["false_easting",7500000], PARAMETER["false_northing",0],UNIT["Meter",1]]}



var
  s: string;
begin
  Assert (Zone_GK>0);

  Assert(DataType<10);

  
  s:=DEF;
        
  
  s:=ReplaceStr(s, ':description', ExtractFileName(aFileName));

  s:=ReplaceStr(s, ':samples',   IntToStr(ColCount));
  s:=ReplaceStr(s, ':lines',     IntToStr(RowCount));
  s:=ReplaceStr(s, ':data type', IntToStr(DataType));
  
  s:=ReplaceStr(s, ':Lon_min',   FloatToStr(Lon_min));
  s:=ReplaceStr(s, ':Lat_max',   FloatToStr(Lat_max));

  s:=ReplaceStr(s, ':CellSize',  FloatToStr(CellSize));
  s:=ReplaceStr(s, ':Zone',      IntToStr(Zone_GK));

  s:=ReplaceStr(s, ':center_minus_3', IntToStr(Zone_GK*6-3));
  s:=ReplaceStr(s, ':center_meters',  IntToStr(Zone_GK*1000000 + 500000));

  s:=ReplaceStr(s, ':central_meridian',  IntToStr(Zone_GK*6-3));
  s:=ReplaceStr(s, ':false_easting',     IntToStr(Zone_GK*1000000 + 500000));

  s:=ReplaceStr(s, ':data ignore value', IntToStr(Null_value));


  TFile.WriteAllText(ChangeFileExt(aFileName,'.hdr'), s  );
           

end;

// ---------------------------------------------------------------
procedure TGDAL_Envi_HDR.SaveToFile_HDR_Conic(aFileName: string);
// ---------------------------------------------------------------
//  sParam:= Format(' -a_srs "+proj=utm  +zone=%d +datum=WGS84" -of GTiff  -co "COMPRESS=LZW" -a_ullr %1.12f  %1.12f %1.12f %1.12f  %s %s ',

const
  CRLF = #13+#10;
  LF = CRLF;      
  
const

  DEF =
    'ENVI'+ LF+ 
    'description = {'+ LF+ 
    ' :description}'+ LF+ 
    'samples = :samples'+ LF+ 
    'lines   = :lines'+ LF+ 
    'bands   = 1 '+ LF+ 
    'header offset = 0  '+ LF+ 
    'file type = ENVI Standard '+ LF+ 
    'data type = :data type '+ LF+ 
    'interleave = bsq '+ LF+ 
    'byte order = 0 '+ LF+
    'data ignore value = :data ignore value'+ LF+
  //      'map info = {Transverse Mercator,  1, 1, %d, %d, %d, %d, %d}';         
//map info = {Transverse Mercator, 1, 1, 29376365.9767268, 8018695.73473788, 100.040318333087, 100.040318333087}
      
    'map info = {Equidistant_Conic, 1, 1, :Lon_min, :Lat_max, :CellSize, :CellSize}' + LF+ 

//    'map info = {UTM, 1, 1, :Lon_min, :Lat_max, :CellSize, :CellSize, :Zone, North,WGS-84}' + LF+ 
    
//map info = {Transverse Mercator, 1, 1, 29376365.9767268, 8018695.73473788, 100.040318333087, 100.040318333087}    

    'coordinate system string = {PROJCS["Equidistant_Conic_Yakutiya",GEOGCS["GCS_WGS_1984",DATUM["WGS_1984",SPHEROID["WGS_1984",6378137,298.257223563]],'+
        'PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Equidistant_Conic"],PARAMETER["False_Easting",0],PARAMETER["False_Northing",0],'+
        'PARAMETER["Longitude_Of_Center",132],PARAMETER["Standard_Parallel_1",49.4],PARAMETER["Standard_Parallel_2",67.8],PARAMETER["Latitude_Of_Center",64],UNIT["Meter",1],AUTHORITY["EPSG","54027"]]}'+ LF+ 

//                                 PROJCS["Equidistant_Conic_Yakutiya",GEOGCS["GCS_WGS_1984",DATUM["WGS_1984",SPHEROID["WGS_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Equidistant_Conic"],PARAMETER["False_Easting",0],PARAMETER["False_Northing",0],PARAMETER["Longitude_Of_Center",132],PARAMETER["Standard_Parallel_1",49.4],PARAMETER["Standard_Parallel_2",67.8],PARAMETER["Latitude_Of_Center",64],UNIT["Meter",1],AUTHORITY["EPSG","54027"]]
    
    'band names = {'+ LF+ 
    'Band 1}';

//ENVI
//description = {
//C:\_files\111111111.}
//samples = 16135
//lines   = 2481
//bands   = 3
//header offset = 0
//file type = ENVI Standard
//data type = 2
//interleave = bsq
//byte order = 0
//map info = {Equidistant_Conic, 1, 1, 2232379.5768, 6865105.3327, 0.0276369383328326, 0.138538573156098}
//coordinate system string = {PROJCS["Sphere_Equidistant_Conic",GEOGCS["GCS_Sphere",DATUM["D_Sphere",SPHEROID["Sphere",6371000.0,0.0]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433]],PROJECTION["Equidistant_Conic"],PARAMETER["False_Easting",0.0],PARAMETER["False_Northing",0.0],PARAMETER["Central_Meridian",0.0],PARAMETER["Standard_Parallel_1",60.0],PARAMETER["Standard_Parallel_2",60.0],PARAMETER["Latitude_Of_Origin",0.0],UNIT["Meter",1.0]]}
//band names = {
//Band 1,
//Band 2,
//Band 3}
    

// coordinate system string = {PROJCS["Pulkovo_1942_Gauss_Kruger_zone_7", GEOGCS["GCS_Pulkovo 1942",DATUM["D_Pulkovo_1942",SPHEROID["Krasovsky_1940",6378245,298.3]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",39], PARAMETER["scale_factor",1],PARAMETER["false_easting",7500000], PARAMETER["false_northing",0],UNIT["Meter",1]]}



var
  s: string;
begin
//  Assert (Zone>0);

  s:=DEF;
        
  Assert(DataType<10);
        
  
  s:=ReplaceStr(s, ':description', ExtractFileName(aFileName));

  s:=ReplaceStr(s, ':samples',   IntToStr(ColCount));
  s:=ReplaceStr(s, ':lines',     IntToStr(RowCount));
  s:=ReplaceStr(s, ':data type', IntToStr(DataType));
  
  s:=ReplaceStr(s, ':Lon_min',   FloatToStr(Lon_min));
  s:=ReplaceStr(s, ':Lat_max',   FloatToStr(Lat_max));

  s:=ReplaceStr(s, ':CellSize',  FloatToStr(CellSize));
 // s:=ReplaceStr(s, ':zone',      IntToStr(Zone));

//  s:=ReplaceStr(s, ':center_minus_3', IntToStr(Zone*6-3));
//  s:=ReplaceStr(s, ':center_meters',  IntToStr(Zone*1000000 + 500000));
  
//  s:=ReplaceStr(s, ':central_meridian',  IntToStr(Zone*6-3));
//  s:=ReplaceStr(s, ':false_easting',     IntToStr(Zone*1000000 + 500000));

  s:=ReplaceStr(s, ':data ignore value', IntToStr(Null_value));


  TFile.WriteAllText(ChangeFileExt(aFileName,'.hdr'), s  );
           

end;

// ---------------------------------------------------------------
procedure TGDAL_Envi_HDR.SaveToFile_HDR_UTM(aFileName: string);
// ---------------------------------------------------------------
//  sParam:= Format(' -a_srs "+proj=utm  +zone=%d +datum=WGS84" -of GTiff  -co "COMPRESS=LZW" -a_ullr %1.12f  %1.12f %1.12f %1.12f  %s %s ',

//https://docs.safe.com/fme/html/FME_Desktop_Documentation/FME_ReadersWriters/envihdr/envihdr.htm
const
  CRLF = #13+#10;
  LF = CRLF;

const

  DEF =
    'ENVI'+ LF+
    'description = {'+ LF+
    ' :description}'+ LF+
    'samples = :samples'+ LF+
    'lines   = :lines'+ LF+
    'bands   = 1 '+ LF+

    //data ignore value   --The value of NODATA in this dataset.
    'header offset = 0  '+ LF+
    'file type = ENVI Standard '+ LF+
    'data type = :data type '+ LF+
    'interleave = bsq '+ LF+
    'byte order = 0 '+ LF+
    'data ignore value = :data ignore value'+ LF+

  //      'map info = {Transverse Mercator,  1, 1, %d, %d, %d, %d, %d}';
//map info = {Transverse Mercator, 1, 1, 29376365.9767268, 8018695.73473788, 100.040318333087, 100.040318333087}

//    'map info = {Transverse Mercator, 1, 1, :Lon_min, :Lat_max, :CellSize, :CellSize}' + LF+

    'map info = {UTM, 1, 1, :Lon_min, :Lat_max, :CellSize, :CellSize, :Zone, North,WGS-84}' + LF+

//map info = {Transverse Mercator, 1, 1, 29376365.9767268, 8018695.73473788, 100.040318333087, 100.040318333087}

//    'projection info = {3, 6378245, 6356863.018773047, 0, :center_minus_3, :center_meters, 0, 1, Transverse Mercator}'+ LF+
////    'projection info = {3, 6378245, 6356863.018773047, 0, 177, 30500000, 0, 1, Transverse Mercator}'+ LF+
//    'coordinate system string = {PROJCS["Pulkovo_1942_Gauss_Kruger_zone_:zone",GEOGCS["GCS_Pulkovo 1942",DATUM["D_Pulkovo_1942",SPHEROID["Krasovsky_1940",6378245,298.3]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],'+
//    'PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",:central_meridian],'+
//    'PARAMETER["scale_factor",1],PARAMETER["false_easting",:false_easting],PARAMETER["false_northing",0],UNIT["Meter",1]]}'+ LF+
    'band names = {'+ LF+
    'Band 1}';


// coordinate system string = {PROJCS["Pulkovo_1942_Gauss_Kruger_zone_7", GEOGCS["GCS_Pulkovo 1942",DATUM["D_Pulkovo_1942",SPHEROID["Krasovsky_1940",6378245,298.3]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",39], PARAMETER["scale_factor",1],PARAMETER["false_easting",7500000], PARAMETER["false_northing",0],UNIT["Meter",1]]}



var
  s: string;
begin
  Assert (Zone_UTM>0);

  Assert(DataType<10);


  s:=DEF;


  s:=ReplaceStr(s, ':description', ExtractFileName(aFileName));

  s:=ReplaceStr(s, ':samples',   IntToStr(ColCount));
  s:=ReplaceStr(s, ':lines',     IntToStr(RowCount));
  s:=ReplaceStr(s, ':data type', IntToStr(DataType));

  s:=ReplaceStr(s, ':Lon_min',   FloatToStr(Lon_min));
  s:=ReplaceStr(s, ':Lat_max',   FloatToStr(Lat_max));

  s:=ReplaceStr(s, ':CellSize',  FloatToStr(CellSize));
  s:=ReplaceStr(s, ':zone',      IntToStr(Zone_UTM));

  s:=ReplaceStr(s, ':center_minus_3', IntToStr(Zone_UTM*6-3));
  s:=ReplaceStr(s, ':center_meters',  IntToStr(Zone_UTM*1000000 + 500000));

  s:=ReplaceStr(s, ':central_meridian',  IntToStr(Zone_UTM*6-3));
  s:=ReplaceStr(s, ':false_easting',     IntToStr(Zone_UTM*1000000 + 500000));

  s:=ReplaceStr(s, ':data ignore value', IntToStr(Null_value));


  TFile.WriteAllText(ChangeFileExt(aFileName,'.hdr'), s  );


end;




//var
//  r: TGDAL_json_rec;
//begin
//  r.LoadFromFile('p:\_megafon\NENETZ\Height\all_pulkovo.json');

end.


{



  // ---------------------------------------------------------------


  sFile_bat:=  ChangeFileExt( aBmpFileName , '.bat');

  oBAT.SaveToFile (sFile_bat, TEncoding.GetEncoding(866));
  
  oGDAL_bat.SaveToFile (sFile_bat+'_', TEncoding.GetEncoding(866));
  
  FreeAndNil(oBAT);

 //!!!!!!!!!!!!!!!!!!! 
  SetCurrentDir(sDir);
  
  RunApp(sFile_bat,'');

  sTab          :=ChangeFileExt(Params.TabFileName, '.tab');
  sFileName_json:=ChangeFileExt(Params.TabFileName, '.3395.json');  //sFile + '%s.3395.tif';
  aTifFileName  :=ChangeFileExt(Params.TabFileName, '.3395.tif');  //sFile + '%s.3395.tif';

  
  Assert (FileExists (sFileName_json), sFileName_json);
  
  if rGDAL_json.LoadFromFile (sFileName_json) then 
    rGDAL_json.SaveToFile_TAB_3395 (sTab, aTifFileName );  


    SetCurrentDir(sDir);
  
  RunApp(sFile_bat,'');
//  RunApp(ChangeFileExt(Params.RelFileName, '.bat'),'');

  // ---------------------------------------------------------------
//  rGDAL_json.LoadFromFile(ChangeFileExt(Params.TabFileName_CLU, '_clu.json'));
//  rGDAL_json.SaveToFile_TAB(Params.TabFileName_CLU,  ChangeFileExt(Params.TabFileName_CLU, '_clu.tif'));  

//  rGDAL_json.LoadFromFile (ChangeFileExt(sFile_bat, '_clu.json'));
  if rGDAL_json.LoadFromFile (ChangeFileExt(sFile_bat, '_clu.3395.color.json')) then 
    rGDAL_json.SaveToFile_TAB_3395(Params.TabFileName_CLU,  ChangeFileExt(Params.TabFileName_CLU, '.tif') );  



    
                                         
   s:= Format('gdal_translate.exe  -a_srs EPSG:%d -of GTiff  -co "COMPRESS=LZW" -a_ullr %d %d %d %d  "%s.bmp" "%s.tif" ',
                             [28400+ FRelMatrix.ZoneNum_GK,

                      Round(FRelMatrix.XYBounds.TopLeft.Y),
                      Round(FRelMatrix.XYBounds.TopLeft.X),

                      Round(FRelMatrix.XYBounds.BottomRight.Y),
                      Round(FRelMatrix.XYBounds.BottomRight.X),

                      sFile, 
                      sFile

                     ]);

   oBAT.Add(s);      
   oGDAL_bat.Add(s);
   
// ---------------------------------------------------------------
//  TIF -> TIF EPSG:3395
// ---------------------------------------------------------------
  
  s:=  Format('gdalwarp -t_srs EPSG:3395 "%s.tif"  "%s.3395.tif" ',[sFile, sFile]);  //_TIF]);
  oBAT.Add(s);
  oBAT.Add('');
  
  oGDAL_bat.Add(s);
  oGDAL_bat.Add('');

  
  s:=Format('gdalinfo -json   "%s.3395.tif" >  "%s.3395.json" ',[sFile,sFile]);  //_TIF,sFile_TIF]);
  oBAT.Add(s);  
  oBAT.Add('');

  oGDAL_bat.Add(s);
  oGDAL_bat.Add('');


  ENVI
description = {
C:\_files\111111111.}
samples = 16135
lines   = 2481
bands   = 3
header offset = 0
file type = ENVI Standard
data type = 2
interleave = bsq
byte order = 0
map info = {Equidistant_Conic, 1, 1, 2232379.5768, 6865105.3327, 0.0276369383328326, 0.138538573156098}
coordinate system string = {PROJCS["Sphere_Equidistant_Conic",GEOGCS["GCS_Sphere",DATUM["D_Sphere",SPHEROID["Sphere",6371000.0,0.0]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433]],PROJECTION["Equidistant_Conic"],PARAMETER["False_Easting",0.0],PARAMETER["False_Northing",0.0],PARAMETER["Central_Meridian",0.0],PARAMETER["Standard_Parallel_1",60.0],PARAMETER["Standard_Parallel_2",60.0],PARAMETER["Latitude_Of_Origin",0.0],UNIT["Meter",1.0]]}
band names = {
Band 1,
Band 2,
Band 3


// ---------------------------------------------------------------
procedure TGDAL_bat_1111111.Init_11111111(aDir: string);
// ---------------------------------------------------------------
var
  oIni: TIniFile;
  s: string;
  sFile: string;
//  sGDAL_rel_Path: string;
  sPath: string;
  sPath_bin: string;
begin

  sFile:=ExtractFilePath(Application.ExeName) + 'gdal.ini';
  Assert(FileExists(sFile), sFile);

  // ---------------------------------------------------------------

  oIni:=TIniFile.Create(sFile);
  sPath:=oIni.ReadString('main','path','');
  FreeAndNil(oIni);

  // ---------------------------------------------------------------
//  sPath:=IncludeTrailingBackslash(sPath);

  sPath:= IncludeTrailingBackslash ( ExtractFilePath(Application.ExeName) +  sPath );
  sPath_bin:=sPath + 'bin\';

//  FGDAL_rel_Path:=sPath_bin;

  Add('set path='+ sPath_bin + ';'+ aDir);


//    oBAT.Add('set path='+ sPath_bin + ';'+ sDir);


  Add(Format('set GDAL_DATA=%s',[sPath])+ 'share\epsg_csv') ;

  Add('set GDAL_FILENAME_IS_UTF8=NO');
  Add('');

end;



  // ---------------------------------------------------------------
  TGDAL_bat_1111111 = class(TStringList)
  private
    FPath: string;
    FPath_bin: string;


    procedure ColorTo_R_G_B(Value: TColor; var R,G,B: byte);


 //   procedure Init_11111111(aDir: string);
    procedure LoadFromFile_GDAL_ini;
  public
    GDAL_json: TGDAL_json_rec;

    ColorList: TStringList;

    constructor Create;
    destructor Destroy; override;

    procedure ColorList_Add_Value_Color(aValue: Integer; aColor: TColor);
    procedure InitHeader;
  end;



constructor TGDAL_bat_1111111.Create;
begin
  inherited Create;
  ColorList := TStringList.Create();

  LoadFromFile_GDAL_ini;

end;

destructor TGDAL_bat_1111111.Destroy;
begin
  FreeAndNil(ColorList);
  inherited Destroy;
end;


//--------------------------------------------------------
procedure TGDAL_bat_1111111.ColorList_Add_Value_Color(aValue: Integer; aColor: TColor);
//--------------------------------------------------------
var
  s: string;
  R,G,B: byte;

begin
   ColorTo_R_G_B(aColor, r,g,b);

   s:= Format('%d %d %d %d',[aValue,  r,g,b]);
   ColorList.Add(s);

end;

// ---------------------------------------------------------------
procedure TGDAL_bat_1111111.ColorTo_R_G_B(Value: TColor; var R,G,B: byte);
// ---------------------------------------------------------------
var cl: packed record
        case z:byte of
          0: (int: TColor);
          1: (RGB: array[0..4] of byte);
        end;
begin
  cl.int:=Value;
  R:=cl.rgb[0];  G:=cl.rgb[1];  B:=cl.rgb[2];
end;






// ---------------------------------------------------------------
procedure TGDAL_bat_1111111.LoadFromFile_GDAL_ini;
// ---------------------------------------------------------------
var

  oIni: TIniFile;
  sFile: string;
  sGDAL_rel_Path: string;
//  sPath: string;
//  sPath_bin: string;
begin
  sFile:=ExtractFilePath(Application.ExeName) + 'gdal.ini';
  Assert(FileExists(sFile));

  // ---------------------------------------------------------------

  oIni:=TIniFile.Create(sFile);

  FPath:=oIni.ReadString('main','abs_path','');

  if FPath='' then
    FPath:= ExtractFilePath(Application.ExeName) +  oIni.ReadString('main','path','');

//..  else
//..    FPath:= IncludeTrailingBackslash ( ExtractFilePath(Application.ExeName) +  FPath );

  FreeAndNil(oIni);

  // ---------------------------------------------------------------
//  sPath:=IncludeTrailingBackslash(sPath);

//  FPath:= IncludeTrailingBackslash ( ExtractFilePath(Application.ExeName) +  sGDAL_rel_Path );
 // FPath:= sGDAL_rel_Path ;
  FPath_bin:=FPath + 'bin\';


end;


// ---------------------------------------------------------------
procedure TGDAL_bat_1111111.InitHeader;
// ---------------------------------------------------------------
begin
  Add('set path='+ FPath_bin + ';'); //+ ExtractFilePath(FFileName));

  Add(Format('set GDAL_DATA=%s',[FPath])+ 'share\epsg_csv');
  Add('set GDAL_FILENAME_IS_UTF8=NO');
  Add('');


//  sFile:=ExtractFilePath(Application.ExeName) + 'gdal.ini';
//  Assert(FileExists(sFile));
//
//  // ---------------------------------------------------------------
//
//  oIni:=TIniFile.Create(sFile);
//  sGDAL_rel_Path:=oIni.ReadString('main','path','');
//  FreeAndNil(oIni);
//
//  // ---------------------------------------------------------------
////  sPath:=IncludeTrailingBackslash(sPath);
//
//  sPath:= IncludeTrailingBackslash ( ExtractFilePath(Application.ExeName) +  sGDAL_rel_Path );
//  sPath_bin:=sPath + 'bin\';


end;


