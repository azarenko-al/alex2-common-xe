unit u_GDAL_bat;

interface

uses
  System.SysUtils, System.Classes, Forms,  IniFiles, IOUtils,   Vcl.Graphics,

  u_GDAL_classes
  ;



type
  TGDAL_Bat = class(TStringList)
  private

    FPath: string;
    FPath_bin: string;

    procedure ColorTo_R_G_B(aValue: TColor; var R,G,B: byte);
    procedure LoadFromFile_GDAL_ini;
  public

    ColorList: TStringList;
    GDAL_json: TGDAL_json_rec;

    constructor Create;
    destructor Destroy; override;

    procedure ColorList_Add_Value_Color(aValue: Integer; aColor: TColor);

    procedure Init_Header;

    procedure SaveColorTXTFile(aFileName: string);
  end;

implementation



//--------------------------------------------------------
procedure TGDAL_Bat.ColorList_Add_Value_Color(aValue: Integer; aColor: TColor);
//--------------------------------------------------------
var
  s: string;
  R,G,B: byte;
begin
   ColorTo_R_G_B(aColor, r,g,b);

   s:= Format('%d %d %d %d',[aValue,  r,g,b]);
   ColorList.Add(s);

end;

constructor TGDAL_Bat.Create;
begin
  inherited;

  ColorList := TStringList.Create();

  LoadFromFile_GDAL_ini();

//  Init_Header;
end;


destructor TGDAL_Bat.Destroy;
begin
  FreeAndNil(ColorList);
  inherited Destroy;
end;


 {
//--------------------------------------------------------
procedure TGDAL_Bat.ColorList_Add_Value_Color(aValue: Integer; aColor: TColor);
//--------------------------------------------------------
var
  s: string;
  R,G,B: byte;
begin
   ColorTo_R_G_B(aColor, r,g,b);

   s:= Format('%d %d %d %d',[aValue,  r,g,b]);
   ColorList.Add(s);

end;
 }

//--------------------------------------------------------
procedure TGDAL_Bat.ColorTo_R_G_B(aValue: TColor; var R,G,B: byte);
//--------------------------------------------------------
var cl: packed record
        case z:byte of
          0: (int: TColor);
          1: (RGB: array[0..4] of byte);
        end;
begin
  cl.int:=aValue;
  R:=cl.rgb[0];  G:=cl.rgb[1];  B:=cl.rgb[2];
end;


// ---------------------------------------------------------------
procedure TGDAL_Bat.LoadFromFile_GDAL_ini;
// ---------------------------------------------------------------
var

  oIni: TIniFile;
  sFile: string;
  sGDAL_rel_Path: string;
//  sPath: string;
//  sPath_bin: string;
begin
  sFile:=ExtractFilePath(Application.ExeName) + 'gdal.ini';
  Assert(FileExists(sFile));

  // ---------------------------------------------------------------

  oIni:=TIniFile.Create(sFile);

  FPath:=oIni.ReadString('main','path_full','');

//ShowMessage ( FPath );


  if not DirectoryExists(FPath) then
  begin
    FPath:=oIni.ReadString('main','path','');
//    if not DirectoryExists(FPath) then
    FPath:=ExtractFilePath(Application.ExeName) + FPath;

  end;


  if not DirectoryExists(FPath) then
    raise Exception.Create(FPath);

//
//  sFile:=ExtractFilePath(Application.ExeName) + 'gdal.ini';
//  Assert(FileExists(sFile));
//
//  // ---------------------------------------------------------------
//
//  oIni:=TIniFile.Create(sFile);
//
//  FPath:=oIni.ReadString('main','path','');
//  if not DirectoryExists(FPath) then
//    FPath:=ExtractFilePath(Application.ExeName) + FPath;
//
//  if not DirectoryExists(FPath) then
//    raise Exception.Create(FPath);


//  if FPath='' then
//    FPath:= ExtractFilePath(Application.ExeName) +  oIni.ReadString('main','path','');

//;path=OSGeo4W64\
//abs_path=C:\OSGeo4W64\


//..  else
//..    FPath:= IncludeTrailingBackslash ( ExtractFilePath(Application.ExeName) +  FPath );

  FreeAndNil(oIni);
                     //ChildNodes
  // ---------------------------------------------------------------
//  sPath:=IncludeTrailingBackslash(sPath);

//  FPath:= IncludeTrailingBackslash ( ExtractFilePath(Application.ExeName) +  sGDAL_rel_Path );
 // FPath:= sGDAL_rel_Path ;
  FPath_bin:=FPath + 'bin\';


end;


// ---------------------------------------------------------------
procedure TGDAL_Bat.Init_Header;
// ---------------------------------------------------------------
begin
  Clear;

  Add('set path=%path%;'+ FPath_bin + ';'); //+ ExtractFilePath(FFileName));

  Add(Format('set GDAL_DATA=%s',[FPath])+ 'share\epsg_csv');
//  Add('set GDAL_FILENAME_IS_UTF8=NO');
  Add('set GDAL_FILENAME_IS_UTF8=YES');
  Add('');


end;


//-------------------------------------------------------------------
procedure TGDAL_Bat.SaveColorTXTFile(aFileName: string);
//-------------------------------------------------------------------
const

  DEF_CLU_OPEN_AREA = 0;  // 1- ��� (1)
  DEF_CLU_FOREST    = 1;  // 1- ��� (1)
  DEF_CLU_WATER     = 2;  // 3- ���
  DEF_CLU_COUNTRY   = 3;  // 3- ���
  DEF_CLU_ROAD      = 4;
  DEF_CLU_RailROAD  = 5;
  DEF_CLU_CITY      = 7;  // 7- �����
  DEF_CLU_BOLOTO    = 8; // 31- ���
  DEF_CLU_ONE_BUILD = 73; // 73- ���.����
  DEF_CLU_ONE_HOUSE = 31; // 31- ���

var
  _Clutters: array[0..High(256)] of Integer;
  I: Integer;
  oColorList: TStringList;

  R,G,B: byte;
  s: string;

begin
  oColorList:=TStringList.Create;


  FillChar(_Clutters, SizeOf(_Clutters), 0);

  _Clutters[DEF_CLU_OPEN_AREA] := clBlack;  // 1- ��� (1)
//  FClutters[DEF_CLU_OPEN_AREA] := clWhite;  // 1- ��� (1)
  _Clutters[DEF_CLU_FOREST   ] := clGreen;  // 1- ��� (1)
  _Clutters[DEF_CLU_WATER    ] := clNavy;  // 3- ���
  _Clutters[DEF_CLU_COUNTRY  ] := clGray;  // 3- ���
  _Clutters[DEF_CLU_ROAD     ] := clYellow;
  _Clutters[DEF_CLU_RailROAD ] := clYellow;
  _Clutters[DEF_CLU_CITY     ] := clRed;  // 7- �����
  _Clutters[DEF_CLU_BOLOTO   ] := clOlive; // 31- ���
  _Clutters[DEF_CLU_ONE_BUILD] := clRed; // 73- ���.����
  _Clutters[DEF_CLU_ONE_HOUSE] := clMaroon; // 31- ���



   oColorList.Add('0 0 0 0');
   oColorList.Add('255 0 0 0');

  for I := 0 to High(_Clutters) do
    if _Clutters[i]>0 then
    begin
      ColorTo_R_G_B(_Clutters[i], r,g,b);

      s:= Format('%d %d %d %d',[i,  r,g,b]);
      oColorList.Add(s);


    end;

//  s:=oColorList.text;

  oColorList.SaveToFile(aFileName);

  FreeAndNil(oColorList);
end;



end.
