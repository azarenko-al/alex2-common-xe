unit u_GDAL_envi;

interface
uses
  System.SysUtils, System.Classes, Forms,  IniFiles, IOUtils,
  XMLDoc, XMLIntf, DB,

  u_func,
  u_GDAL_classes,
  u_BufferedFileStream;


type
  // ---------------------------------------------------------------
  TEnvi_File = class(TObject)
  // ---------------------------------------------------------------
  private
    FStream: TReadOnlyCachedFileStream;

    FNullValue : double;
    FNullValue2 : smallint;

    FDataLen   : byte;
    FFieldType : TFieldType;


    function FindValueByXY_byte(aX, aY: double; var aValue: byte): Boolean;
//    function GetDataByIndex_byte(aIndex: Int64): byte;
  public

    RowCount : longword;
    ColCount : longword;

    CellSize_lat : double;
    CellSize_lon : double;

    Bounds: TGDAL_Bounds;

    GK_zone : byte;

    constructor Create;
    destructor Destroy; override;

    function FindValueByXY(aX, aY: double; var aValue: Integer): Boolean;

    function FindCellByXY(aX, aY: double; var aRow, aCol: integer): boolean;

    procedure OpenFile(aFileName: string);
    procedure CloseFile;

    function GetDataByIndex(aIndex: int64): Integer;

    procedure SaveToIniFile(aFileName: string);

    procedure SaveToTxt(aFileName: string);
    procedure SaveToXmlFile(aFileName: string);

  end;
  

implementation


function LatLonPointInRect(aLat, aLon: double; aRect: TGDAL_Bounds): Boolean;
begin
  Result :=
     (aLat <= aRect.Lat_max) and
     (aLon <= aRect.Lon_max) and
     (aLat >= aRect.Lat_min) and
     (aLon >= aRect.Lon_min);
end;


constructor TEnvi_File.Create;
begin
  inherited;

  //FNullValue2 := $F1D8;
  FNullValue2 := -9999;
  FNullValue := -9999;

end;


//---------------------------------------------------------------
function TEnvi_File.FindCellByXY(aX, aY: double; var aRow, aCol: integer):
    boolean;
//--------------------------------------------------------------
var
  b: boolean;
begin
  Result:=False;

  aRow:=0; aCol:=0;

//  if not (Active) then
 //   Exit;
//  assert( XYBounds.BottomRight.x>0);

//  if MatrixType=mtXY_ then
//  begin
(*
  b:=(aX < XYBounds.TopLeft.x);
  b:=(XYBounds.BottomRight.x < aX);

  b:=(XYBounds.TopLeft.y < aY);
  b:= aY < XYBounds.BottomRight.y;

    Items[i].Bounds.Lon_min := AsFloat(strArr[1]);
    Items[i].Bounds.Lon_max := AsFloat(strArr[2]);
    Items[i].Bounds.Lat_min := AsFloat(strArr[3]);
    Items[i].Bounds.Lat_max := AsFloat(strArr[4]);

*)


 assert (CellSize_lat>0);


  if LatLonPointInRect(aX, aY, Bounds) then
//  if (XYBounds.BottomRight.x < aX) and (aX < XYBounds.TopLeft.x) and
 //    (XYBounds.TopLeft.y     < aY) and (aY < XYBounds.BottomRight.y) then
  begin
    aRow := Integer(Round((Bounds.Lat_max - aX) / CellSize_lat));
    aCol := Integer(Round((aY - Bounds.Lon_min) / CellSize_lon));

    Result:=(aRow>=0) and (aCol>=0) and (aRow<=RowCount-1) and (aCol<=ColCount-1);

//      Result:=true;
  end;
// end;

end;

destructor TEnvi_File.Destroy;
begin
  FreeAndNil(FStream);

  inherited;
end;

// ---------------------------------------------------------------
procedure TEnvi_File.SaveToIniFile(aFileName: string);
// ---------------------------------------------------------------
var
  oIni: TIniFile;
begin
  Assert(GK_zone>0);


  oIni:=TIniFile.Create (aFileName);

  oIni.WriteInteger('main','RowCount', RowCount);
  oIni.WriteInteger('main','ColCount', ColCount);
                                                     
  oIni.WriteInteger('main','Lat_min', Round(bounds.Lat_min));
  oIni.WriteInteger('main','lat_max', Round(bounds.lat_max));

  oIni.WriteInteger('main','lon_min', Round(bounds.lon_min));
  oIni.WriteInteger('main','Lon_max', Round(bounds.Lon_max));

  oIni.WriteInteger('main','GK_zone', GK_zone);

  FreeAndNil(oIni);
end;


// ---------------------------------------------------------------
procedure TEnvi_File.SaveToXmlFile(aFileName: string);
// ---------------------------------------------------------------
var
  oDocument: TXMLDocument;
  
begin
  Assert(GK_zone>0);

  oDocument:=TXMLDocument.Create(Application);
  oDocument.Active:=True;
  oDocument.Options:=oDocument.Options + [doNodeAutoIndent];
    
  with oDocument.AddChild('bounds') do 
  begin
    AddChild('row_count').Text:=IntToStr( RowCount );
    AddChild('col_count').Text:=IntToStr( ColCount );
    AddChild('GK_zone').Text  :=IntToStr( GK_zone );

    AddChild('lat_min').Text:=IntToStr(Round(bounds.Lat_min));
    AddChild('lat_max').Text:=IntToStr(Round(bounds.lat_max));

    AddChild('lon_min').Text:=IntToStr(Round(bounds.lon_min));
    AddChild('lon_max').Text:=IntToStr(Round(bounds.lon_max));

    // ---------------------------------------------------------------
     
  end;  
 

  oDocument.SaveToFile(aFileName);
  
  FreeAndNil(oDocument);
  
end;



// ---------------------------------------------------------------
function TEnvi_File.FindValueByXY(aX, aY: double; var aValue: Integer): Boolean;
// ---------------------------------------------------------------

var
  iRow,iCol: Integer;
  iValue: Integer;

begin
  assert (CellSize_lat>0);
  assert (CellSize_lon>0);

  aValue :=-11111;

  Result := FindCellByXY(aX, aY, iRow,iCol);

  if Result and Assigned(FStream) then
  begin
    iValue:=GetDataByIndex (ColCount*iRow + iCol);

//    Result:= (w<>NullValue) and (w<>$FFFF);

    Result:= (iValue <> FNullValue) and (iValue <> FNullValue2);

    if Result then
      aValue :=iValue;
        
  end;

end;



// ---------------------------------------------------------------
function TEnvi_File.FindValueByXY_byte(aX, aY: double; var aValue: byte):
    Boolean;
// ---------------------------------------------------------------

var
  iRow,iCol: Integer;
  bt: byte;

begin
  assert (CellSize_lat>0);
  assert (CellSize_lon>0);


  Result := FindCellByXY(aX, aY, iRow,iCol);

  if Result and Assigned(FStream) then
  begin
    bt:=GetDataByIndex (ColCount*iRow + iCol);

//    Result:= (w<>NullValue) and (w<>$FFFF);

    Result:=bt <> FNullValue;// (iW < 50000) and (iW <> -9999);

//    if Result then


    aValue :=bt;

  end;

end;


  //  oAsset_File.SaveToTxt( ChangeFileExt(oItem.FileName,'.txt'));

// ---------------------------------------------------------------
procedure TEnvi_File.SaveToTxt(aFileName: string);
// ---------------------------------------------------------------
var
  c: Integer;
  i: Integer;
  w: word;

  oSList: TStringList;
  r: Integer;
  s: string;

begin
  oSList:=TStringList.Create;

  for r := 0 to RowCount-1 do
  begin
    s:='';

    for c := 0 to ColCount-1 do
    begin
      w := GetDataByIndex (ColCount * r + c );

      if w = FNullValue then
        s:=s + '   '
      else
        s:=s + Format('%3d',[w]);
        
    end;

    oSList.Add(s);
  end;         


  oSList.SaveToFile(aFileName);


  FreeAndNil(oSList);

//  Assert(not Assigned(FFileStream), 'Value not assigned');

 // aFileName :=ChangeFileExt(aFileName, '.bil');
//  FFileStream:=TFileStream.Create(aFileName, fmShareDenyWrite);
//  i:=FFileStream.Size;




end;

// ---------------------------------------------------------------
procedure TEnvi_File.OpenFile(aFileName: string);
// ---------------------------------------------------------------
var
  i: Integer;
  w: word;

  r: TGDAL_json_rec;
  sFile_json: string;


begin
//  Assert(not Assigned(FFileStream), 'Value not assigned');

  aFileName :=ChangeFileExt(aFileName, '.envi');

//  FFileStream:=TFileStream.Create(aFileName, fmShareDenyWrite);
//  i:=FFileStream.Size;

  assert(FileExists(aFileName), aFileName);

  sFile_json:=aFileName + '.json';
  if not FileExists(sFile_json) then
    sFile_json:=ChangeFileExt(aFileName,'.json');


  assert(FileExists(sFile_json), sFile_json);

  r.LoadFromFile(sFile_json);

  if Eq(r.Bands.Type_,'Byte') then begin
    FDataLen:=1;
    FFieldType:=ftByte;
  end else

  if Eq(r.Bands.Type_,'Int16') then begin
    FDataLen:=2;
    FFieldType:=ftSmallint;
  end else

  if Eq(r.Bands.Type_,'Float32') then begin
    FDataLen:=4;
    FFieldType:=ftSingle;
  end
    else
      raise Exception.Create('Error Message');


  RowCount:=r.Size.rows;
  ColCount:=r.Size.cols;

  FNullValue:=r.Bands.noDataValue;

  assert (RowCount>0);
  assert (ColCount>0);
  
//  CellSize:=r.   
  
  Bounds.Lat_max:=r.CornerCoordinates.upperLeft.x;
  Bounds.Lon_min:=r.CornerCoordinates.upperLeft.y;  

  Bounds.Lat_min:=r.CornerCoordinates.lowerRight.x;
  Bounds.Lon_max:=r.CornerCoordinates.lowerRight.y;  

  assert (Bounds.Lat_max  > Bounds.Lat_min);
     
//   assert (CellSize>0);

  
  CellSize_lat:=  (Bounds.Lat_max - Bounds.Lat_min)  / RowCount;
  CellSize_lon:=  (Bounds.Lon_max - Bounds.Lon_min)  / ColCount;
  
  assert (CellSize_lat>0);
  assert (CellSize_lon>0);

  
  FStream:=TReadOnlyCachedFileStream.Create(aFileName);


 // FFileStream.Position :=FFileStream.Size;


 // w:=GetDataByIndex(0);
  
//  w:=GetDataByIndex(1);
//  w:=GetDataByIndex(2);


end;


procedure TEnvi_File.CloseFile;
begin
  FreeAndNil(FStream); 
end;


// ---------------------------------------------------------------
function TEnvi_File.GetDataByIndex(aIndex: int64): Integer;
// ---------------------------------------------------------------

//
    function Swap_longword(aValue: longword): longword; assembler; register;
    asm
      xchg  ah,al
      ror   eax,16
      xchg  ah,al
    end;


type
  TConverter = record
  case integer of
    0 : (longword_: longword);
    1 : (single_: single);
    2 : (arr_4: array[0..3] of byte);
  end;

  TConverter_16 = record
  case integer of
    0 : (w: word);
    1 : (arr_2: array[0..1] of byte);
  end;


  function MacWordToPC(Value : word): smallint;
  asm xchg Al,Ah
  end;


var
  iOffset: int64;
  w: word;

  sm: smallint;

  bt :byte;
//  lw: longword;
  e : double;

  rec: TConverter;
  rec_16: TConverter_16;

  s: string;
begin


  try
    iOffset := aIndex*FDataLen;
  except
  end;


//  FFileStream.Seek (iOffset,  soFromBeginning);
//
//  FFileStream.Read(b1, 1);
//  FFileStream.Read(b2, 1);

//  FStream.se
//  FStream.Seek (iOffset,  soFromBeginning);
//  FStream.Read(bt, 1);
//
//  FStream.Seek (iOffset,  soFromBeginning);
//  FStream.Read(w, 2);


  FStream.Seek (iOffset,  soFromBeginning);

  Assert (FDataLen>0);

  case FDataLen of
    1: begin FStream.Read(bt, 1);
             Result:=bt;
       end;

    2: begin
         FStream.Read(rec_16.w, 2);

         Result:=MacWordToPC(rec_16.w);

     //    Result:=rec_16.w;
       end;

    4: begin
         FStream.Read(rec.single_, 4);
       //  s:=IntToHex(lw,8);
       //  rec.c:=lw; //Swap_longword (lw);
       //  e:=rec.s;

//         if e<>-9999 then
//           e:=e;

         Result:=Round(rec.single_);

       end;
  else
    raise Exception.Create('Error Message');
  end;

//https://www.l3harrisgeospatial.com/docs/enviheaderfiles.html
//  The type of data representation:
//
//1 = Byte: 8-bit unsigned integer
//2 = Integer: 16-bit signed integer
//3 = Long: 32-bit signed integer
//4 = Floating-point: 32-bit single-precision
//5 = Double-precision: 64-bit double-precision floating-point
//6 = Complex: Real-imaginary pair of single-precision floating-point
//9 = Double-precision complex: Real-imaginary pair of double precision floating-point
//12 = Unsigned integer: 16-bit
//13 = Unsigned long integer: 32-bit
//14 = 64-bit long integer (signed)
//15 = 64-bit unsigned long integer (unsigned)

//  Result := MacWordToPC(w);
//
//  if Eq(r.Bands.Type_,'Int16') then begin
//    FDataLen:=2;
//    FFieldType:=ftSmallint;
//  end else


end;



end.


