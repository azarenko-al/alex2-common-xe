object dlg_Wizard: Tdlg_Wizard
  Left = 1570
  Top = 831
  Caption = 'Custom Wizard'
  ClientHeight = 381
  ClientWidth = 523
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Buttons: TPanel
    Left = 0
    Top = 346
    Width = 523
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 0
    ExplicitWidth = 598
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 523
      Height = 2
      Align = alTop
      Shape = bsTopLine
      ExplicitWidth = 500
    end
    object Panel3: TPanel
      Left = 344
      Top = 2
      Width = 179
      Height = 33
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 419
      object btn_Ok: TButton
        Left = 14
        Top = 6
        Width = 75
        Height = 23
        Action = act_Ok
        Default = True
        ModalResult = 1
        TabOrder = 0
      end
      object btn_Cancel: TButton
        Left = 102
        Top = 6
        Width = 75
        Height = 23
        Action = act_Cancel
        Cancel = True
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
  object pn_Top_: TPanel
    Left = 0
    Top = 0
    Width = 523
    Height = 60
    Align = alTop
    BevelOuter = bvNone
    Constraints.MaxHeight = 60
    Constraints.MinHeight = 60
    TabOrder = 1
    Visible = False
    ExplicitWidth = 598
    object Bevel2: TBevel
      Left = 0
      Top = 57
      Width = 523
      Height = 3
      Align = alBottom
      Shape = bsTopLine
      ExplicitWidth = 500
    end
    object pn_Header: TPanel
      Left = 0
      Top = 0
      Width = 523
      Height = 57
      Align = alClient
      BevelOuter = bvLowered
      BorderWidth = 6
      Color = clInfoBk
      TabOrder = 0
      ExplicitWidth = 598
      object lb_Action: TLabel
        Left = 8
        Top = 8
        Width = 44
        Height = 13
        Caption = 'lb_Action'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object ActionList1: TActionList
    Left = 433
    Top = 4
    object act_Ok: TAction
      Category = 'Main'
      Caption = 'Ok'
      OnExecute = act_OkExecute
    end
    object act_Cancel: TAction
      Category = 'Main'
      Caption = #1054#1090#1084#1077#1085#1072
      OnExecute = act_CancelExecute
    end
  end
  object FormStorage1: TFormStorage
    Active = False
    IniFileName = 'Software\Onega\'
    Options = [fpPosition]
    UseRegistry = True
    StoredValues = <>
    Left = 400
    Top = 4
  end
  object cxPropertiesStore: TcxPropertiesStore
    Active = False
    Components = <>
    StorageName = 'cxPropertiesStore'
    StorageType = stRegistry
    Left = 375
    Top = 5
  end
end
