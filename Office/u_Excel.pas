unit u_Excel;

interface
uses Windows,SysUtils,ComObj,ActiveX,Forms,Dialogs,dxmdaset,Variants,

  u_func, DB, Classes;

type
  TStr2DArray = array of TStrArray;


  TExcel = class
  private
    FActiveSheet: OleVariant; //������ �� �������� ����
    FWorkBook: OleVariant; //������ �� �������� �����
    FCells: OleVariant; //������ ����������� �����
    FSheetReaded: Boolean; //�� Excel �������� ���� ����
    FColsCount: Integer;
    FRowsCount: Integer;
    FExcel : OleVariant;

    function GetColumnName(ACol: Cardinal): string;
    function Get_CellValue(ACol, ARow: Cardinal): Variant; overload;
    function OpenBook11111111111111(const AFileName: String): Boolean;
    procedure Set_CellValue(ACol, ARow: Cardinal; const Value: Variant);
  protected
    procedure Update;
  public
    Excel : Variant;
    WorkSheet : Variant;

    constructor Create(ACreateWorkBook: Boolean=False; const AFileName: String='');
    constructor CreateSimple();

    destructor Destroy; override;


    function Open (aFileName: string): boolean;
    procedure Close;

////////////    function GetColumnIndexByName (aColumnName: string): integer;
    function GetColumnIndexByName(const AColumnName: String): integer;
    function GetCellValue (aRow,aCol: integer): Variant; overload;
        function GetCellValue(aRow: integer; CONST sColName: string): Variant; overload;
    function GetCellValue(aAdress: string): Variant; overload;

    function GetCellValue_(ACol: Cardinal; CONST sColName: string): Variant;

    function GetCellColumnIndex(aAdress: string): integer;
    function GetCellRowIndex(aAdress: string): integer;

//    function GetRowCount (): integer;

    function GetColCount (): integer;
    function GetRowCount (): integer;




    function CloseBook: Boolean;
    procedure LoadSheetList(AList: TStrings);
    function SaveBook: Boolean;

    function SheetExists(const ASheetName: String): Boolean;
    function SetSheet(const ASheetName: String): Boolean;    overload;
    function SetSheet(const ASheetNumber: Integer): Boolean; overload;
    function ReadSheet: Boolean;
    function WriteSheet(AStartCol, AStartRow: Integer; aDataset: TDataset): Boolean;
    function SetNameSheet(const ASheetName: String): Boolean;
    //function GetColumnIndexByName (aColumnName: string): integer;

    property Cells[ACol: Cardinal; ARow: Cardinal]: Variant read Get_CellValue write Set_CellValue;
    property ColsCount: Integer read FColsCount;
    property RowsCount: Integer read FRowsCount;

    property ColumnName[ACol: Cardinal]: string read GetColumnName;
  end;

//  function Eq (Value1,Value2: string): boolean;

 // function xls_LoadFromFile (aFileName: string; aRowIndex, aColCount: integer): TStr2DArray;

  function xls_LoadFromFile(aFileName: string; var aArr: TStr2DArray; var aRowCount,aColCount: Integer): Boolean;



//==================================================
implementation
//==================================================

const
  DEF_EXCEL_APPLICATION = 'Excel.Application';

{ TExcel }

//-------------------------------------------------------------------
constructor TExcel.CreateSimple();
//-------------------------------------------------------------------
begin
  try
    FExcel := CreateOleObject(DEF_EXCEL_APPLICATION);
  except
    on E: EOLESysError do
      if E.ErrorCode=CO_E_CLASSSTRING then //Excel �� ����������
        Raise;
  end;

  FExcel.Visible:= false;
  //�� ���������� ��������������� ���������
  FExcel.DisplayAlerts:=False;
end;


//-------------------------------------------------------------------
constructor TExcel.Create(ACreateWorkBook: Boolean=False; const AFileName: String='');
//-------------------------------------------------------------------
var
  ClassID: TCLSID;
  Unknown: IUnknown;
  excel: IDispatch;
begin
  ClassID := ProgIDToClassID (DEF_EXCEL_APPLICATION);

  if Succeeded(GetActiveObject(ClassID, nil, Unknown)) AND
     Succeeded(Unknown.QueryInterface(IDispatch, excel))
  then
    FExcel := excel
  else
  begin
    try
      FExcel := CreateOleObject(DEF_EXCEL_APPLICATION);
    except
      on E: EOLESysError do
        if E.ErrorCode=CO_E_CLASSSTRING then //Excel �� ����������
          Raise;
    end;

    FExcel.Visible:= false;
  end;

  try
    //��������� Excel �� ������ �����
    //FExcel.WindowState:=-4137;
    //�� ���������� ��������������� ���������
    FExcel.DisplayAlerts:=False;

    if ACreateWorkBook then
    begin
      FWorkBook:=FExcel.WorkBooks.Add;
      while FWorkBook.WorkSheets.Count>1 do
        FWorkBook.ActiveSheet.Delete;
      Update;
      FActiveSheet.Name:='Sheet1';
      FWorkBook.SaveAs(AFileName)
    end else
      if AFileName <> '' then
        FWorkBook:=FExcel.WorkBooks.Open(AFileName);

    Update;
  except
    Raise;
  end;
end;

//-------------------------------------------------------------------
destructor TExcel.Destroy;
//-------------------------------------------------------------------
begin
  try
    //SaveBook;
    CloseBook;
    FExcel.DisplayAlerts:=True;
    if FExcel.WorkBooks.Count=0 then
      FExcel.Quit;
  except
  end;

  VarClear(FExcel);

  inherited;
end;

//-------------------------------------------------------------------
function TExcel.GetCellValue(aRow, aCol: integer): Variant;
//-------------------------------------------------------------------
begin
  Result:=WorkSheet.Cells[aRow+1, aCol+1]
end;

//-------------------------------------------------------------------
function TExcel.GetCellValue(aRow: integer; CONST sColName: string): Variant;
//-------------------------------------------------------------------
begin
  Result:=WorkSheet.Cells[aRow+1, GetColumnIndexByName(sColName)+1]
end;

//-------------------------------------------------------------------
function TExcel.GetCellValue(aAdress: string): Variant;
//-------------------------------------------------------------------
begin
  Result:= WorkSheet.Range[aAdress, EmptyParam].Value;
end;

//-------------------------------------------------------------------
function TExcel.GetCellColumnIndex(aAdress: string): integer;
//-------------------------------------------------------------------
begin
  Result:= WorkSheet.Range[aAdress, EmptyParam].Column;
end;

//-------------------------------------------------------------------
function TExcel.GetCellRowIndex(aAdress: string): integer;
//-------------------------------------------------------------------
begin
  Result:= WorkSheet.Range[aAdress, EmptyParam].Row;
end;

function TExcel.GetColumnIndexByName(const AColumnName: String): integer;
var
  i: integer;
begin
  Result:=0;

  for i:=1 to ColsCount do
    if AnsiCompareText(AColumnName, Cells[i,1])=0 then
    begin
      Result:=i;
      Exit;
    end;
end;

function TExcel.GetColumnName(ACol: Cardinal): string;
begin
  Result:= Cells[aCol,1];
end;


function TExcel.OpenBook11111111111111(const AFileName: String): Boolean;
begin
  if not FileExists(AFileName) then
  begin
    Result:=False;
    exit;
  end;


  try
    FWorkBook:=FExcel.WorkBooks.Open(AFileName);
    Update;

    Result:=True;
  except
    Result:=False;
  end;
end;

function TExcel.CloseBook: Boolean;
begin
  try
    FWorkBook.Close;

    Result:=True;
  except
    Result:=False;
  end;
end;

function TExcel.SaveBook: Boolean;
begin
  try
    FWorkBook.Save;

    Result:=True;
  except
    Result:=False;
  end;
end;


function TExcel.ReadSheet: Boolean;
begin
  try
    FCells:=FActiveSheet.Range['A1',
        FActiveSheet.Cells.Item[RowsCount, ColsCount]].Value;

    Result:=True;
  except
    Result:=False;
  end;
end;


function TExcel.GetCellValue_(ACol: Cardinal; CONST sColName: string): Variant;
var
  iIndex: Integer;
begin
  iIndex:= GetColumnIndexByName(sColName);
  if iIndex>0 then
    Result:=FCells[ACol, iIndex];
end;

function TExcel.Get_CellValue(ACol, ARow: Cardinal): Variant;
begin
  if (ARow<=FRowsCount) and (ACol<=FColsCount) then
    Result:=FCells[ARow, ACol];
end;

procedure TExcel.Set_CellValue(ACol, ARow: Cardinal; const Value: Variant);
begin
  if (ARow<=FRowsCount) and (ACol<=FColsCount) then
    FCells[ARow, ACol]:=Value;
end;

procedure TExcel.Update;
begin
  FActiveSheet:=FExcel.ActiveSheet;
  FColsCount:=FActiveSheet.UsedRange.Columns.Count;
  FRowsCount:=FActiveSheet.UsedRange.Rows.Count;
end;


function TExcel.WriteSheet(AStartCol, AStartRow: Integer; aDataset: TDataset):
    Boolean;
var
  cells, cellLeftTop, cellRightBottom: OleVariant;
  rowCount, colCount, curRec: Integer;
begin
  Result:=False;

  try
    if (aDataset.RecordCount=0) or (aDataset.FieldCount=1) then
      Exit;

    aDataset.DisableControls;
    curRec:=aDataset.RecNo;

    //� ����� ���� ���� id � �������� 0, ��� ��� �� �����
    cells:=VarArrayCreate([1, aDataset.RecordCount, 1, aDataset.FieldCount-1],
      varVariant);

    aDataset.First;

    for rowCount:=1 to aDataset.RecordCount do
    begin
      for colCount:=1 to aDataset.FieldCount-1 do
        cells[rowCount, colCount]:=aDataset.Fields.Fields[colCount].Value;

      aDataset.Next;
    end;
    //������� ����� ������� ������
    cellLeftTop    :=FActiveSheet.Cells[AStartRow, AStartCol];
    //������� ������ ������ ������
    cellRightBottom:=FActiveSheet.Cells[AStartRow+aDataset.RecordCount-1,
      AStartCol+aDataset.FieldCount-2];
    //������� ������ cells � �������, ����������� ����� ��������
    FActiveSheet.Range[cellLeftTop, cellRightBottom].Value:=cells;

    aDataset.RecNo:=curRec;
    aDataset.EnableControls;
    VarClear(cells);

    Result:=True;
  except
    Result:=False;
  end;
end;

function TExcel.SetNameSheet(const ASheetName: String): Boolean;
begin
  try
    FActiveSheet.Name:=ASheetName;
    Result:=True;
  except
    Result:=False;
  end;
end;

function TExcel.SetSheet(const ASheetName: String): Boolean;
var
  i, count: Integer;
begin
  Result:=False;

  try
    for i:=1 to FExcel.WorkSheets.Count do
      if Eq(ASheetName, FExcel.WorkSheets[i].Name) then
      begin
        FExcel.WorkSheets[i].Activate;
        Update;

        Result:=True;
      end;
  except
  end;
end;

function TExcel.SetSheet(const ASheetNumber: Integer): Boolean;
begin
  Result:=False;

  try
    FExcel.WorkSheets[ASheetNumber].Activate;
    Update;

    Result:=True;
  except end;
end;


function TExcel.SheetExists(const ASheetName: String): Boolean;
var
  i: Integer;
begin
  Result:=False;

  try
    for i:=1 to FExcel.WorkSheets.Count do
      if Eq(FExcel.WorkSheets[i].Name, ASheetName) then
        Result:=True;
  except
  end;
end;


procedure TExcel.LoadSheetList(AList: TStrings);
var
  i: Integer;
begin   
  AList.Clear;

  try
    for i:=1 to FExcel.WorkSheets.Count do
      AList.Add(FExcel.WorkSheets[i].Name)
  except
  end;
end;



//-------------------------------------------------------------------
function TExcel.Open(aFileName: string): boolean;
//-------------------------------------------------------------------
begin
  try
    Excel := CreateOleObject('Excel.Application');
    Excel.Visible := False;
    Excel.Workbooks.Open (aFileName);

    WorkSheet := Excel.Workbooks[1].WorkSheets[1];

    Result:=True;
  except
    Result:=False;
  end;
end;



//-----------------------------------------------------------------------------------------
function xls_LoadFromFile(aFileName: string; var aArr: TStr2DArray; var aRowCount,aColCount: Integer): Boolean;
//-----------------------------------------------------------------------------------------
var
   vExcel : Variant;
   vWorkSheet : Variant;
   iCount, r,c: integer;
   bool: boolean;
begin
   try
      vExcel := CreateOleObject('Excel.Application');
      vExcel.Visible := False;
      vExcel.Workbooks.Open (aFileName);

      vWorkSheet := vExcel.Workbooks[1].WorkSheets[1];
      vWorkSheet.Activate;

      //define matrix size
      aRowCount:= vWorkSheet.Cells.CurrentRegion.Rows.Count;
      aColCount := vWorkSheet.Cells.CurrentRegion.Columns.Count;
                     
      SetLength (aArr, aRowCount);
      for r:=0 to aRowCount-1 do
      begin
        SetLength (aArr[r], aColCount);
        for c := 0 to aColCount-1 do
           aArr [r,c] := VarToStr(vWorkSheet.Cells[r+1,c+1]);
      end;

      vExcel.Workbooks.Close;
      vExcel.Quit;

      Result:=True;
   except
//      SetLength(aArr,0);
      Result:=False;
  end;
end;

{
}


procedure TExcel.Close;
begin
  Excel.Workbooks.Close;
  Excel.Quit;

end;

//-----------------------------------------------------------------------------------------
function TExcel.GetRowCount (): integer;
//-----------------------------------------------------------------------------------------
begin
  Result:= WorkSheet.Cells.CurrentRegion.Rows.Count;
end;

//-----------------------------------------------------------------------------------------
function TExcel.GetColCount (): integer;
//-----------------------------------------------------------------------------------------
begin
  Result:= WorkSheet.Cells.CurrentRegion.Columns.Count;
end;




var
  oExcel: TExcel;
  i: integer;

begin
{
  Application.Initialize;

  oExcel:=TExcel.Create;
  oExcel.Open('t:\����������_nnovgorod.xls');

  i:=oExcel.GetColumnIndexByName ('���_�');
  i:=oExcel.GetColumnIndexByName ('������__����');
 }

end.
