unit f_Excel_Preview;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Placemnt,
  u_Worksheet
  ;


type
  Tfrm_Excel_Preview = class(TForm)
    ListView1: TListView;
    FormPlacement1: TFormPlacement;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure ShowWindow (aOwner: TComponent; aFileName: string);
    procedure LoadFromFile (aFileName: string);
  end;


//===================================================
implementation {$R *.DFM}
//===================================================
var
  frm_Excel_Preview: Tfrm_Excel_Preview;

//---------------------------------------------------
class procedure Tfrm_Excel_Preview.ShowWindow;
//---------------------------------------------------
begin
  if not Assigned(frm_Excel_Preview) then
     frm_Excel_Preview:=Tfrm_Excel_Preview.Create (aOwner);
//                               else frm_Preview.ShowModal;
  frm_Excel_Preview.LoadFromFile (aFileName);
  frm_Excel_Preview.ShowModal;

end;


procedure Tfrm_Excel_Preview.FormCreate(Sender: TObject);
begin
  ListView1.Align:=alClient;
end;


procedure Tfrm_Excel_Preview.LoadFromFile (aFileName: string);
var
   i,j : Integer;
   arrWorksheet: TVariant2DArray;
begin
  ListView1.Items.Clear;

  ListView1.Columns.Clear;

  if not FileExists (aFileName) then Exit;

  Screen.Cursor:=crHourGlass;

  arrWorksheet := ExcelWorkSheetToArray (aFileName);

  for I := 0 to High(arrWorksheet[0]) do
     with ListView1.Columns.Add do begin
       Caption := ( arrWorksheet[0,I] );
       Width   := 90;
     end;

  for I := 1 to High(arrWorksheet) do
    with ListView1.Items.Add do begin
      Caption := ( arrWorksheet[I,1] );
      for J := 2 to High(arrWorksheet[i]) do
        SubItems.Add( ( arrWorksheet[I,J] ));
    end;

  Screen.Cursor:=crDefault;

end;



begin
  frm_Excel_Preview:=nil;
end.

