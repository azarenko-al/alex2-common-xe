unit u_Excel_load;

interface
uses SysUtils,ComObj, DB, Variants;

type
  TVariant2DArray = array of array of Variant;
//  TString2DArray  = array of array of string;


  function ExcelWorkSheetToArray (aFileName: string): TVariant2DArray;


//==================================================
implementation
//==================================================


//--------------------------------------------------------
// function loads Excel worksheet to variant array
//--------------------------------------------------------
function ExcelWorkSheetToArray (aFileName: string): TVariant2DArray;
//--------------------------------------------------------
var Excel : Variant;
    v,WorkSheet : Variant;
    i,j,iColCount,iRowCount : integer;
    arrGrid: TVariant2DArray;
    s: string;
begin
  try
    Excel := CreateOleObject('Excel.Application');
    Excel.Visible := False;
    Excel.Workbooks.Open (aFileName);

    WorkSheet := Excel.Workbooks[1].WorkSheets[1];

    iColCount:=WorkSheet.Cells.CurrentRegion.Columns.Count;
    iRowCount:=WorkSheet.Cells.CurrentRegion.Rows.Count;

    s:=WorkSheet.Cells[7,3];

    SetLength (arrGrid, iRowCount);
    for i:=0 to High(arrGrid) do
      SetLength (arrGrid[i], iColCount);

    for i := 0 to WorkSheet.Cells.CurrentRegion.Rows.Count-1 do
    for j := 0 to WorkSheet.Cells.CurrentRegion.Columns.Count-1 do
       arrGrid [i,j] := VarToStr(WorkSheet.Cells[I+1,J+1]);

    Excel.Workbooks.Close;
    Excel.Quit;
    Result:=arrGrid;
  except
    SetLength(Result,0);
  end;
end;


end.