unit u_geo_convert_;

interface

uses
  Math;


procedure geo_UTM_to_WGS84_ (aX,aY : Double; aUTM_Zone: Integer; var aLat,aLon : Double);


implementation

function geo_GK_GetCenterMeridian_Rad(aUTM_Zone: double): Integer;
begin
  Assert(aUTM_Zone>0, 'Value <=0');
  Result:=Round((aUTM_Zone-30)*6 - 3);
end;


procedure geo_UTM_to_WGS84_(aX,aY : Double; aUTM_Zone: Integer; var aLat,aLon : Double);
const
 // latd : Double = 6180493; //northing
//  east : Double = 405390;  //easting

//Datum Constants
  b    : Double =	6356752.314;    //Polar Axis	b	6356752,314
  a    : Double =	6378137;    //Equ Rad	    a	6378137
  ec    : Double =	0.081819191;    //ecc	e	0,081819191
  eisq : Double =	0.006739497;    //e'2	eisq	0,006739497
  k0   : Double =	0.9996;    //Scale	k0	0,9996

var
  arc: Double;
  mu: Double;
  ei: Double;
  ca: Double;
  cb: Double;
  cd: Double;
  phi1: Double;
  ccc: Double;
  sin1: Double;
  q0: Double;
  n0: Double;
  r0: Double;
  t0: Double;
  dd0: Double;
  lond2: Double;
  Fact1: Double;
  lat: Double;
  Fact2: Double;
  Fact3: Double;
  Fact4: Double;
  lof2: Double;
  lof3: Double;
  lof1: Double;
  zone_CM: Double;
  delta_long: Double;
  d_long_dec: Double;
  lon: Double;
begin
  //Calculate Footprint Latitude
  arc:=aX / k0;

  // =arc/(a*(1-ec^2/4-3*ec^4/64-5*ec^6/256))
  mu := arc / (a*(1- Power(ec,2)/4 - 3*Power(ec,4)/64 - 3*Power(ec,6)/256  ));

  ei :=(1-Power((1-ec*ec),(1/2))) / (1+ Power((1-ec*ec),(1/2)));
//  =(1-(1-ec*ec)^(1/2))/(1+(1-ec*ec)^(1/2))

  //=3*ei/2-27*ei^3/32
  ca:=3*ei/2-27* power(ei,3)/32;

  //=21*ei^2/16-55*ei^4/32
  cb:=21*power(ei,2)/16-55*power(ei,4)/32;

  // := 151*ei^3/96;
  ccc:=151*power(ei,3)/96;

  // := 1097*ei^4/512;
  cd:=1097*power(ei,4)/512;

  //=mu+ca*SIN(2*mu)+cb*SIN(4*mu)+ccc*SIN(6*mu)+cd*SIN(8*mu)
  phi1 :=mu+ca*SIN(2*mu)+cb*SIN(4*mu)+ccc*SIN(6*mu)+cd*SIN(8*mu);

  // Constants for Formulas
  sin1 :=0;

  q0 := eisq* Power(COS(phi1),2);
  t0 := Power(TAN(phi1),2);
  n0 := a / Power(1-(Power(ec*SIN(phi1), 2)), (1/2));

  r0 :=a*(1-ec*ec) /
        Power((1-Power((ec*SIN(phi1)),2)), (3/2));

  lond2 := 500000-aY;
  dd0 :=lond2/(n0*k0);

  //Coefficients for Calculating Latitude


  Fact1	:=n0*TAN(phi1)/r0; // := n0*TAN(phi1)/r0;
	Fact2	:=dd0*dd0/2;      //=dd0*dd0/2

  // := (5+3*t0+10*Q0-4*Q0*Q0-9*eisq)*dd0^4/24;
	Fact3	:=(5+3*t0+10*Q0-4*Q0*Q0-9*eisq)* power(dd0,4)/24 ;

  // := (61+90*t0+298*Q0+45*t0*t0-252*eisq-3*Q0*Q0)*dd0^6/720;
	Fact4 :=(61+90*t0+298*Q0+45*t0*t0-252*eisq-3*Q0*Q0)* power(dd0,6)/720;


//  =180*(phi1-fact1*(fact2+fact3+fact4))/��()
// -------------------------
  aLat:=180*(phi1-fact1*(fact2+fact3+fact4))/Pi;
  // -------------------------

  //Coefficients for Calculating Longitude
  lof1 :=dd0;
  lof2 :=(1+2*t0+Q0)* power(dd0,3)/6;
  lof3 :=(5-2*Q0+28*t0-3*Power(Q0,2)+8*eisq+24*Power(t0,2)) * Power(dd0,5)/120;

  delta_long:=(lof1-lof2+lof3)/COS(phi1);

  d_long_dec := delta_long * 180 / Pi;
  d_long_dec := RadToDeg(delta_long);

(*  d_long_dd  := Trunc(d_long_dec);
  d_long_mm  := Trunc((d_long_dec-d_long_dd) * 60);
  d_long_ss  := 3600* (d_long_dec-d_long_dd-d_long_mm/ 60);
*)
  zone_CM := geo_GK_GetCenterMeridian_Rad (aUTM_Zone);

  // -------------------------
  aLon := zone_CM- d_long_dec;
  // -------------------------

end;



end.

{
procedure TForm17.FormCreate(Sender: TObject);
const
  latd : Double = 6180493; //northing
  east : Double = 405390;  //easting
var
  aLat,aLon : Double;
begin
  geo_UTM_to_WGS84_(latd,east, 37, aLat,aLon);

end;


