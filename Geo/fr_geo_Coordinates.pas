unit fr_geo_Coordinates;

interface

uses
  SysUtils, rxCurrEdit, Menus,  StdCtrls, ExtCtrls,
  Variants,Classes, Controls, Forms,

  u_func,
  u_Geo,

  u_geo_convert,
  u_geo_convert_new,

  Mask, rxToolEdit
  ;

type
  Tframe_geo_Coordinates = class(TForm)
    rg_CoordSystem: TRadioGroup;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    CurrencyEdit24: TCurrencyEdit;
    PopupMenu1: TPopupMenu;
    N11: TMenuItem;
    CurrencyEdit14: TCurrencyEdit;
    Panel1: TPanel;
    CurrencyEdit11: TCurrencyEdit;
    CurrencyEdit12: TCurrencyEdit;
    CurrencyEdit13: TCurrencyEdit;
    cb_North: TComboBox;
    cb_East: TComboBox;
    CurrencyEdit23: TCurrencyEdit;
    CurrencyEdit22: TCurrencyEdit;
    CurrencyEdit21: TCurrencyEdit;
    lb_Lat: TLabel;
    lb_Lon: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Button1: TButton;
    Button2: TButton;
    N21: TMenuItem;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
//    procedure cb_NorthClick(Sender: TObject);
    procedure CurrencyEdit11Change(Sender: TObject);
    procedure CurrencyEdit14Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ed_BL_Change(Sender: TObject);
//    procedure ed_B_hChange(Sender: TObject);
//..    procedure ed_B_hEditing(Sender: TObject; var CanEdit: Boolean);
    procedure ed_LAtEditing(Sender: TObject; var CanEdit: Boolean);
    procedure N11Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure rg_CoordSystemClick(Sender: TObject);
//    procedure ed_B_sPropertiesEditValueChanged(Sender: TObject);
  //  procedure ed_LAtPropertiesEditValueChanged(Sender: TObject);
  private
    FBLPoint_Pulkovo: TBLPoint;
    FDisplayBLPoint: TBLPoint;

//    FInitialCoordSys: integer;
    FDisplayCoordSys: integer;

    FIsEdited: Boolean;
    FOnChange: TnotifyEvent;

    procedure BLPointToEdit(aIndex: integer);
    procedure EditToBLPoint(aIndex: Integer);

    procedure UpdateDisplayBLPoint;
  private
    procedure deg_Changed;
    procedure Deg_min_sec_changed;

    procedure SetBLPoint_pulkovo(const Value: TBLPoint);
    function GetBLPoint_pulkovo: TBLPoint;

    function GetDisplayCoordSys: Integer;
    procedure SetDisplayCoordSys(aValue: Integer);

  public
    property OnChange: TnotifyEvent read FOnChange write FOnChange;

// TODO: SetBLPoint_new
//  procedure SetBLPoint_new(aBLPoint: TBLPoint; aCoordSys : Integer);

    /// read: Pulkovo
    property BLPoint_Pulkovo: TBLPoint read GetBLPoint_pulkovo write SetBLPoint_pulkovo;

//    property DisplayCoordSys: Integer read GetDisplayCoordSys write SetDisplayCoordSys;
    property DisplayCoordSys: Integer write SetDisplayCoordSys;

//EK_KRASOVSKY42, EK_WGS84);

  end;

const
  DEF_frame_geo_Coordinates_HEIGHT = 100;


var
  frame_geo_Coordinates: Tframe_geo_Coordinates;  


implementation
{$R *.dfm}


//--------------------------------------------------------------
procedure Tframe_geo_Coordinates.FormCreate(Sender: TObject);
//--------------------------------------------------------------  
begin
// ed_Lon.ValueType:=vtFloat;
// ed_Lat.ValueType:=vtFloat;

//  lb_Lat.Caption := '������';
//  lb_Lon.Caption := '�������';


  with rg_CoordSystem.Items do
  begin
    Clear;
    AddObject(DEF_GEO_STR_CK42,  Pointer(EK_CK_42));
    AddObject(DEF_GEO_STR_CK95,  Pointer(EK_CK_95));
    AddObject(DEF_GEO_STR_WGS84, Pointer(EK_WGS84));
  end;


  rg_CoordSystem.OnClick := nil;
  rg_CoordSystem.ItemIndex :=0;
  rg_CoordSystem.OnClick := rg_CoordSystemClick;

  CurrencyEdit14.DisplayFormat :=',0.0000000000000';
  CurrencyEdit24.DisplayFormat :=',0.0000000000000';

  CurrencyEdit14.DecimalPlaces :=13;
  CurrencyEdit24.DecimalPlaces :=13;
                         
  if Owner is TControl then
//    TControl(Owner).Height:=115;
    TControl(Owner).Height:=DEF_frame_geo_Coordinates_HEIGHT;  //85

end;

//----------------------------------------------
procedure Tframe_geo_Coordinates.BLPointToEdit(aIndex: integer);
//----------------------------------------------
var
  rAngle1,rAngle2: TAngle;
 // d: integer;
begin
  FIsEdited := True;

  case aIndex of
    1: begin
        rAngle1:=geo_DecodeDegree (FDisplayBLPoint.B);
        rAngle2:=geo_DecodeDegree (FDisplayBLPoint.L);

    //    if FDisplayBLPoint.B then
                                     //  41.116638891
    //    d:=Trunc((rAngle1.sec - Trunc(rAngle1.sec)) * 100);

{
        MaskEdit1.Text:= Format('%3d� %d'' %d,%d"',
              [rAngle1.deg, rAngle1.min, Trunc(rAngle1.sec), d ]);

}

        CurrencyEdit11.Value:=Abs(rAngle1.deg);
        CurrencyEdit12.Value:=rAngle1.min;
  //      CurrencyEdit13.Value:=TruncFloat (Round(rAngle1.sec*100000000)/100000000, 2);
        CurrencyEdit13.Value:=rAngle1.sec;

        CurrencyEdit21.Value:=Abs(rAngle2.deg);
        CurrencyEdit22.Value:=rAngle2.min;
//        CurrencyEdit23.Value:=TruncFloat (Round(rAngle2.sec*100000000)/100000000, 2)
        CurrencyEdit23.Value:=rAngle2.sec;
       end;

    2: begin
        CurrencyEdit14.Value:=FDisplayBLPoint.B;
        CurrencyEdit24.Value:=FDisplayBLPoint.L;
       end;
  end;

  FIsEdited := False;
end;



procedure Tframe_geo_Coordinates.Button2Click(Sender: TObject);
var
  bl: TBLPoint;
begin
  bl.b:=34.4444;
  bl.L:=44.4444;

  BLPoint_Pulkovo :=bl;

  bl:=BLPoint_Pulkovo;

  edit1.Text := FloatToStr(bl.B);
  edit2.Text := FloatToStr(bl.L);

end;


//----------------------------------------------
procedure Tframe_geo_Coordinates.EditToBLPoint(aIndex: Integer);
//----------------------------------------------
var
  rAngle1: TAngle;
  rAngle2: TAngle;
  iCoordSys: integer;
begin
  case aIndex of
    1: begin
         rAngle1.deg :=Round(CurrencyEdit11.Value);
         rAngle1.min :=Round(CurrencyEdit12.Value);
         rAngle1.sec :=CurrencyEdit13.Value;

         rAngle2.deg :=Round(CurrencyEdit21.Value);
         rAngle2.min :=Round(CurrencyEdit22.Value);
         rAngle2.sec :=CurrencyEdit23.Value;

         FDisplayBLPoint.B := geo_EncodeDegreeRec (rAngle1);
         FDisplayBLPoint.L := geo_EncodeDegreeRec (rAngle2);
       end;
    2: begin
         FDisplayBLPoint.B := CurrencyEdit14.Value;
         FDisplayBLPoint.L := CurrencyEdit24.Value;
       end;
  end;

  //------------------------------------------------------
  iCoordSys :=GetDisplayCoordSys();

  case iCoordSys of
    EK_CK_42  : FBLPoint_Pulkovo := FDisplayBLPoint;
    EK_CK_95  : FBLPoint_Pulkovo := geo_Pulkovo_BL_to_BL_95(FDisplayBLPoint, EK_CK_95, EK_CK_42);
    EK_WGS84  : FBLPoint_Pulkovo := geo_WGS84_to_Pulkovo42(FDisplayBLPoint);

  end;

//  FBLPoint_Pulkovo := geo_BL_to_BL_new(FDisplayBLPoint, EK_WGS84, EK_CK_42);

end;

// ---------------------------------------------------------------
procedure Tframe_geo_Coordinates.ed_BL_Change(Sender: TObject);
// ---------------------------------------------------------------
begin
  if FIsEdited then
    Exit;

  EditToBLPoint(2);
  BLPointToEdit(1);

  if Assigned(FOnChange) then
    FOnChange(Self);
end;


// ---------------------------------------------------------------
procedure Tframe_geo_Coordinates.ed_LAtEditing(Sender: TObject; var CanEdit:  Boolean);
// ---------------------------------------------------------------
begin
 if FIsEdited then
    Exit;

  EditToBLPoint(2);
  BLPointToEdit(1);

  if Assigned(FOnChange) then
    FOnChange(Self);
end;

// ---------------------------------------------------------------
function Tframe_geo_Coordinates.GetBLPoint_pulkovo: TBLPoint;
// ---------------------------------------------------------------
begin
  //��������� ��������� �����
//  EditToBLPoint(1);
  Result:=FBLPoint_Pulkovo;

  if cb_North.ItemIndex=0
 // if cb_North.Text='N'
    then Result.B:=Abs(Result.B)
    else Result.B:=-Abs(Result.B);

  if cb_East.ItemIndex=0
//  if cb_North.Text='E'
    then Result.L:=Abs(Result.L)
    else Result.L:=-Abs(Result.L);

end;

// ---------------------------------------------------------------
function Tframe_geo_Coordinates.GetDisplayCoordSys: Integer;
// ---------------------------------------------------------------
begin
  with rg_CoordSystem do
    Result := Integer(Items.Objects[ItemIndex]);
end;


procedure Tframe_geo_Coordinates.rg_CoordSystemClick(Sender: TObject);
begin
  UpdateDisplayBLPoint();
end;

// ---------------------------------------------------------------
procedure Tframe_geo_Coordinates.SetBLPoint_pulkovo(const Value: TBLPoint);
// ---------------------------------------------------------------
begin
  FBLPoint_Pulkovo:=Value;
  UpdateDisplayBLPoint();

  cb_North.ItemIndex := IIF(FDisplayBLPoint.B>=0, 0,1 );
  cb_East.ItemIndex  := IIF(FDisplayBLPoint.L>=0, 0,1 );


(*

  if FBLPoint_Pulkovo.B>=0
    then cb_North.ItemIndex := 0
    else cb_North.ItemIndex := 1;

  if FBLPoint_Pulkovo.L>=0
    then cb_East.ItemIndex := 0
    else cb_East.ItemIndex := 1;
    *)
    
end;


procedure Tframe_geo_Coordinates.SetDisplayCoordSys(aValue: Integer);
var
  ind: integer;
begin
  FDisplayCoordSys:=aValue;
 // rg_CoordSystem.ItemIndex:=IIF(aValue=EK_KRASOVSKY42, 0, 1);

  ind := rg_CoordSystem.Items.IndexOfObject(Pointer(aValue));
  if ind>=0 then
    rg_CoordSystem.ItemIndex :=ind;
end;

// ---------------------------------------------------------------
procedure Tframe_geo_Coordinates.UpdateDisplayBLPoint;
// ---------------------------------------------------------------
var
  iCoordSys: integer;
begin


  iCoordSys :=GetDisplayCoordSys();

(*
  function geo_WGS84_to_Pulkovo42(aBLPoint: TBLPoint): TBLPoint;
  function geo_Pulkovo42_to_WGS84(aBLPoint: TBLPoint): TBLPoint;
  function geo_Pulkovo_BL_to_BL_95(aBLPoint: TBLPoint; aFrom,aTo: byte): TBLPoint;
*)


  if (FBLPoint_Pulkovo.b=0) and (FBLPoint_Pulkovo.L=0) then
  begin
    FDisplayBLPoint:=FBLPoint_Pulkovo;
  end else

    case iCoordSys of
      EK_CK_42 : FDisplayBLPoint:=FBLPoint_Pulkovo;
      EK_CK_95 : FDisplayBLPoint:=geo_Pulkovo_BL_to_BL_95(FBLPoint_Pulkovo, EK_CK_42, EK_CK_95);
      EK_WGS84 : FDisplayBLPoint:=geo_Pulkovo42_to_WGS84(FBLPoint_Pulkovo);

  //  else
  //    raise Exception.Create('');
    end;


//  if FDisplayCoordSys=EK_KRASOVSKY42 then
  //  Result := geo_BL_to_BL(FBLPoint, EK_KRASOVSKY42, EK_WGS84);

  BLPointToEdit(1);
  BLPointToEdit(2);

  cb_North.ItemIndex := IIF(FDisplayBLPoint.B>=0, 0,1 );
  cb_East.ItemIndex  := IIF(FDisplayBLPoint.L>=0, 0,1 );

end;

// ---------------------------------------------------------------
procedure Tframe_geo_Coordinates.Deg_min_sec_changed;
// ---------------------------------------------------------------
begin
 if FIsEdited then
    Exit;

  EditToBLPoint(1);
  BLPointToEdit(2);

  if Assigned(FOnChange) then
    FOnChange(Self);

end;       

// ---------------------------------------------------------------
procedure Tframe_geo_Coordinates.deg_Changed;
// ---------------------------------------------------------------
begin
  if FIsEdited then
    Exit;

  EditToBLPoint(2);
  BLPointToEdit(1);

  if Assigned(FOnChange) then
    FOnChange(Self);
end;



procedure Tframe_geo_Coordinates.CurrencyEdit11Change(Sender: TObject);
begin
  Deg_min_sec_changed();
end;

procedure Tframe_geo_Coordinates.CurrencyEdit14Change(Sender: TObject);
begin
  deg_Changed();
end;


procedure Tframe_geo_Coordinates.N11Click(Sender: TObject);
var
  bl: TBLPoint;
begin
  bl.B:=-34.5345;
  bl.L:=-37.5345;

  BLPoint_Pulkovo:=bl;

end;


procedure Tframe_geo_Coordinates.Button1Click(Sender: TObject);
var
  bl: TBLPoint;
begin
  bl.b:=-34.4444;
  bl.L:=-44.4444;

  BLPoint_Pulkovo :=bl;

  bl:=BLPoint_Pulkovo;

  edit1.Text := FloatToStr(bl.B);
  edit2.Text := FloatToStr(bl.L);

end;

procedure Tframe_geo_Coordinates.Button3Click(Sender: TObject);
begin
//  cb_North.ItemIndex := IIF(FDisplayBLPoint.B>=0, 0,1 );
 // cb_East.ItemIndex  := IIF(FDisplayBLPoint.L>=0, 0,1 );

end;

procedure Tframe_geo_Coordinates.N21Click(Sender: TObject);
begin
 // ShowMessage('');
end;


end.


//
//
//procedure Tframe_geo_Coordinates.ed_B_hEditing(Sender: TObject; var CanEdit:
//    Boolean);
//begin
//{ if FIsEdited then
//    Exit;
//
//  EditToBLPoint(1);
//  BLPointToEdit(2);
//
//  if Assigned(FOnChange) then
//    FOnChange(Self);
//}
//end;




// TODO: SetBLPoint_new
//// ---------------------------------------------------------------
//procedure Tframe_geo_Coordinates.SetBLPoint_new(aBLPoint: TBLPoint; aCoordSys :
//  Integer);
//// ---------------------------------------------------------------
//begin
//FDisplayBLPoint := aBLPoint;
//
//rg_CoordSystem.OnClick :=nil;
//
//FInitialCoordSys := aCoordSys;
//
//DisplayCoordSys := aCoordSys;
//
//rg_CoordSystem.OnClick :=rg_CoordSystemClick;
//
//end;

