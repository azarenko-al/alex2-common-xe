﻿unit u_GEO;

//===================================================================
interface
//===================================================================
uses Sysutils, Math, DB,

  u_geo_neva

//     u_geo_neva
   ;

const
  // Типы флага "Вид эллипсоида"
  //константы определены в u_geo_neva !!
  EK_UNDEFINED   =  0;  // Не установлено (использовать прямоугольные Гаусса)
  EK_CK_42 =  1;  // Красовского 1942г.

  EK_KRASOVSKY42 = 1;

  EK_WGS84  =  9; //EK_WGS_84;  // WGS 1984г.
  EK_WGS_84 = EK_WGS84;

  EK_CK_95    = 10;  // СК 95 - система по ГОСТ 51794

  EK_GCK_2011 = 2011;


//  EK_CK95        =  11;  // Красовского 1942г.
//  EK_WGS84      =  101;  // WGS 1984г.


(*
  EK_SK_95       =  10;  // СК 95 - система по ГОСТ 51794

  // точноcть на карте после запятой
  MIF_PRECISION = 6;

const
  DEF_GEO_STR_KRASOVSKY42 = 'СК-42';
  DEF_GEO_STR_WGS84       = 'WGS-84';
  DEF_GEO_STR_SK95        = 'СК-95';

*)

  // точноcть на карте после запятой
  MIF_PRECISION = 12;//6;

const
  DEF_GEO_STR_CK42 = 'CK-42';// (Пулково)';
  DEF_GEO_STR_CK95 = 'CK-95';// (Пулково)';
  DEF_GEO_STR_WGS84= 'WGS-84';
  DEF_GEO_STR_GCK_2011= 'ГСК-2011';

//const

 // CONST_EK_KRASOVSKY42 = 'Красовский 1942';
  //CONST_EK_WGS84       = 'WGS 1984';
      
type

  //---------------------------------------------------
  // geo common types
  //---------------------------------------------------
  TBLPoint  = packed record B,L: double; 
                 function ToString: String; 
              end;

  TLatLonMinMax = record
                        Lat_min: Double;
                        Lat_max: double;

                        Lon_min: Double;
                        Lon_max: double;
                     end;

 

  TXYPoint  = packed record X,Y: Double;  end;          //Zone: Byte;

  TBLRect   = packed record 
                TopLeft,BottomRight: TBLPoint; 

                function ToString: String;
              end;
                  
  TBLRectBL = packed record BottomLeft,TopRight: TBLPoint; end;
  TXYRect   = packed record TopLeft,BottomRight: TXYPoint; end;
  TBLVector = record Point1,Point2: TBLPoint; end;
  TXYVector = record Point1,Point2: TXYPoint; end;

  TAngle    = packed record Deg,Min: integer; Sec: double; end;


type

  TBLPointArray  = array of TBLPoint;
  TXYPointArray  = array of TXYPoint;

  TBLPolyPointArray  = array of TBLPointArray;  
    
 // TBLPointArrayPointer = ^TBLPointArrayF;
  TBLPointArrayF = packed record
                     Count: integer;
                     Items: array[0..10000] of TBLPoint;
                   end;

  TXYPointArrayF = packed record
                     Count: integer;
                     Items: array[0..10000] of TXYPoint;
                   end;


  TBLPolygones   = array of TBLPointArrayF;
  TBLRectArray   = array of TBLRect;
  TXYRectArray   = array of TXYRect;

  TXYBounds_MinMax = record
                MinLat  : Double;
                MaxLat  : Double;
                MinLon  : Double;
                MaxLon  : Double;


                Min_X  : Double;
                Max_X  : Double;
                Min_Y  : Double;
                Max_Y  : Double;

 
                function ToString: string;
              end;

  TLatLon  = packed record Lat,Lon: double; end;



  TTileBounds = record
               block_X_min : integer;
               block_X_max : integer;

               block_Y_min : integer;
               block_Y_max : integer;

               Z : integer;
               Scale: integer;
             end;
              


type
  TDegreeType = (dtLat,dtLon); //,dtLatRus,dtLonRus
//  TDegreeType = (dtNone,dtLat,dtLon,dtLatRus,dtLonRus);

const
  NULL_BLPOINT  : TBLPoint  = (b:0; L:0);
  NULL_POINT : TBLPoint  = (b:0; L:0);

  NULL_XYRECT : TXYRect  = (TopLeft: (x:0; y:0); BottomRight: (x:0; y:0));



//new !!!!!!!!!!!!
///  function geo_XYRect_to_BLRect (aXYRect: TXYRect): TBLRect;

//  procedure geo_RotateBLVector111(var aBLVector: TBLVector);


  function geo_IsBLRects_Crossed (aBLRect1, aBLRect2: TBLRect): boolean;

  function geo_MoveBLRectToCenter (aNewRectCenter: TBLPoint; aBLRect: TBLRect): TBLRect;
  function geo_MoveBLRect(const aBLRect: TBLRect; aLat, aLon: double): TBLRect;

  function geo_GetRoundBLRect (aRects: TBLRectArray): TBLRect;

  function  MakeBLRect (b1,l1, b2,l2: double): TBLRect;
  function  MakeXYRect (x1,y1, x2,y2: double): TXYRect;

  function geo_GetBLRectCenter (aBLRect: TBLRect): TBLPoint;

  function geo_StrToDegree (aValue: String): double;
  function geo_DegreeToStr (aValue: double): String;

  // град -> град, мин, сек
  function  geo_DecodeDegree  (aValue: double): TAngle;

  function  geo_Get6ZoneY (Y: double): integer;
  function geo_Get6ZoneL(aLon: double): integer;

  function geo_Get6ZoneBL(aBLPoint: TBLPoint): integer;
  function  geo_Get6ZoneXY (aXYPoint: TXYPoint): integer;

  function  geo_BLPointInRect (aPoint:TBLPoint; aRect:TBLRect): boolean;
  function geo_XYPointInRect(aXY: TXYPoint; aRect: TXYRect): boolean;

  function geo_EncodeDegree(aDeg: integer; aMin, aSec: Double): double;
  function  geo_EncodeAngle     (Deg,Min,Sec: integer): TAngle;
  function  geo_AnglePlusAngle  (Value1,Value2: TAngle): TAngle;
  function  geo_AngleMinusAngle (Value1,Value2: TAngle): TAngle;

  function  geo_GetNearestAngle (Value: TAngle; aStepMinutes: integer): TAngle;

  function  geo_GetCenterMeridianXY (aZone: integer): double;
  function  geo_GetCenterMeridianBL (aZone: integer): double;

  function  geo_ZoomRect (aBLRect: TBLRect; aOffset: double): TBLRect;

  function geo_DistanceBL_m(aPoint1,aPoint2: TBLPoint): double;
  function geo_DistanceBLV(aVector: TBLVector): double;
  function  geo_DistanceXY_m   (aPoint1,aPoint2: TXYPoint): double;
  function  geo_DistanceXY1  (x1,y1,x2,y2: double): double;

  procedure DegToRad2 (var aPoint: TBLPoint);
  procedure RadToDeg2 (var aPoint: TBLPoint);

  //------------------------------------------------------
  // Format functions
  //------------------------------------------------------
  function  geo_FormatBLPoint  (aBLPoint: TBLPoint): String; // в градусах
// TODO: geo_FormatBLPoint2__
//function geo_FormatBLPoint2__(aBLPoint: TBLPoint): String;
  function  geo_FormatXYRect   (aXYRect : TXYRect):  String;
  function  geo_FormatBLRect   (aBLRect: TBLRect): String;
  function  geo_FormatXYPoint  (aXYPoint: TXYPoint): String;
  function  geo_FormatAngleEx  (aValue: double): String;

//  function geo_FormatDegree (aValue: double; aType: TDegreeType = dtNone): String;

  function geo_FormatDegree_Lon(aValue: double): String;
  function geo_FormatDegree_Lat(aValue: double): String;


  function geo_AzimuthXY1(aPoint1,aPoint2: TBLPoint): double;
  function  geo_Azimuth    (aPoint1,aPoint2: TBLPoint): double;
  function  geo_TiltAngle  (aPoint1,aPoint2: TBLPoint; aSiteHeight1,aSiteHeight2: double): double;

// TODO: geo_TruncPoint__
//// DONE: geo_TruncFloat__
////function geo_TruncFloat__(Value: double; Count: integer=MIF_PRECISION): double;
//function geo_TruncPoint__(aBLPoint: TBLPoint; aCount: integer=MIF_PRECISION):
//    TBLPoint;


  function  geo_BoundBLVector (aBLVector: TBLVector): TBLRect;
  procedure geo_RoundedBLRect (aBLVector: TBLVector; var aBLRect: TBLRect);

  procedure geo_ReduceBLPoint (var aBLPoint: TBLPoint);

  function geo_ComparePoints (aBLPoint1,aBLPoint2: TBLPoint): boolean;

  function  NULL_VECTOR: TBLVector;
//  function  NULL_RECT:   TBLRect;
  function NULL_BLRECT: TBLRect;


//  function geo_GetBLRectByMapName (aMapName: String): TBLRect;
  function geo_GetMapName (aBLPoint: TBLPoint; aScale: integer): String;


  function geo_EncodeDegreeFromString (Value: String): double;

  function geo_RotateByAzimuth(aBLPoint: TBLPoint; aRadius_m, aAzimuth: double):
      TBLPoint;

  function geo_GetEarthHeight(aOffset_KM, aDistance_KM: double; aRefraction:
      double=1.33): double;

  function geo_CompareBLRects (Rect1,Rect2: TBLRect): boolean;

  function geo_ZoomIn  (aBLRect: TBLRect): TBLRect;
  function geo_ZoomOut (aBLRect: TBLRect; aTimes: integer): TBLRect;

  procedure geo_BLPoints_to_XYPoints (aBLPoints: TBLPointArrayF; var aXYPoints: TXYPointArrayF);

  function geo_IsXYPointInXYPolygonF(aXYPoint: TXYPoint; var aXYPolygon:
      TXYPointArrayF): boolean;
  function geo_IsBLPointInBLPolygon(aBLPoint: TBLPoint; var aBLPolygon: TBLPointArrayF): boolean;

  function geo_EncodeDegreeRec (Value: TAngle): double;

  function geo_Eq_Point  (aBLPoint1, aBLPoint2: TBLPoint): boolean;
  function geo_Eq_vector (aBLVector1, aBLVector2: TBLVector): boolean;
  function geo_Eq_Rect   (aBLRect1, aBLRect2: TBLRect): boolean;

  procedure geo_UpdateRectForPoint (aPoint: TBLPoint; var aRect: TBLRect);

  function  geo_LatOrLonIsZero_rect   (aBLRect: TBLRect): boolean;
  function  geo_LatOrLonIsZero_vector (aBLVector: TBLVector): boolean;
  function  geo_LatOrLonIsZero_point  (aBLPoint: TBLPoint): boolean;

  procedure geo_BLRectToBLPointsF (aBLRect: TBLRect; var aPoints: TBLPointArrayF);
  procedure geo_BLRectToBLPoints (aBLRect: TBLRect; var aPoints: TBLPointArray);
  procedure geo_XYRectToXYPointsF(aXYRect: TXYRect; var aPoints: TXYPointArrayF);


  function  MakeXYPoint (aX,aY: double): TXYPoint;
  function  MakeBLPoint (aB,aL: double): TBLPoint;

// TODO: geo_UpdateVector11111
//procedure geo_UpdateVector11111(var aBLVector: TBLVector; var aID1,aID2:
//    integer; aDirection: integer);


  function geo_DefineRoundBLRectForArrayF(var aPoints: TBLPointArrayF): TBLRect;

  procedure geo_MakeCirclePointsF(aCenter: TBLPoint; aRadiusM: double;
      aPointCount: integer; var aPoints: TBLPointArrayF);

 // function  geo_GetRoundedBLRectByCircles (var aPoints: TBLPointArrayF; aRadiusArr: TDoubleArray): TBLRect;
  function geo_MakeBLRectByCircle(aBLCenter: TBLPoint; aRadius_m: double):
      TBLRect;


  function geo_GetSquareAroundPoint(aBLPoint: TBLPoint; aBorderWidth: integer):
      TBLRect;

  function  geo_GetVectorCenterP  (aPoint1,aPoint2: TBLPoint): TBLPoint;
  function  geo_GetVectorCenterV (aVector: TBLVector): TBLPoint;


  function geo_MakeXYRectByCircle (aCenter: TXYPoint;
                                   aRadius: double;
                                   aBoundRect: TXYRect): TXYRect;

  function geo_XYRectByCircle(aBLCenter: TBLPoint; aRadius_m: double): TXYRect;
      export;

  procedure geo_BLRectByCircle(aBLCenter: TBLPoint; aRadius: double; var
      aBLPoints: TBLPointArrayF);

  function  geo_GetRoundXYRect (aBLRect: TBLRect; aZone: integer=0): TXYRect;
  function geo_GetRoundXYRect_(aRects: TXYRectArray): TXYRect;

  procedure geo_BLRectToXYPoints(aBLRect: TBLRect; var aXYPoints: TXYPointArrayF;
      aZone: integer = 0);

  procedure geo_SetCoordSys ( aCoordSys: integer);
  function  geo_GetCoordSys: Integer;


  procedure geo_MakeBezierF(var aBLPoints: TBLPointArrayF; var aOutBLPoints: TBLPointArrayF);

  function db_ExtractBLRect   (aDataset: TDataset): TBLRect;  //export;
  function db_ExtractBLRect_LatLonMinMax (aDataset: TDataset): TBLRect; export;
  function db_ExtractBLVector (aDataset: TDataset): TBLVector; //export;
  function db_ExtractBLPoint  (aDataset: TDataset): TBLPoint; //export;
                      
// TODO: geo_GetCellXYSize
//function geo_GetCellXYSize(aBounds: TBLRect; aStepB,aStepL: double): integer;

  procedure geo_MakeBLPointsByCircle(aCenter: TBLPoint; aRadius_m: Double; var
      aPoints: TBLPointArrayF);

  function geo_RoundBLPointsToXYRect(var aPoints: TBLPointArrayF; aZone: integer): TXYRect;

  function geo_RoundBLPointsToBLRect_F(var aPoints: TBLPointArrayF): TBLRect;
  function geo_RoundBLPointsToBLRect_ (aPoints: TBLPointArray): TBLRect;


  function geo_RoundXYRect(aXYRect: TXYRect): TBLRect;

  function  MakeBLVector (aBLPoint1,aBLPoint2: TBLPoint): TBLVector; overload;
  function  MakeBLVector (aB1,aL1,aB2,aL2: double): TBLVector; overload;

  function  geo_Distance_m (aPoint1,aPoint2: TBLPoint): double; overload;
  function  geo_Distance_km(aVector: TBLVector): double;        overload;
  function  geo_Distance_KM(aPoint1,aPoint2: TBLPoint): double;  overload;
  function  geo_Distance_m (aVector: TBLVector): double; overload;

(*
 function  geo_Distance (aPoint1,aPoint2: TXYPoint): double; overload;
  function  geo_Distance (x1,y1,x2,y2: double): double;       overload;
*)
  function geo_Eq (aBLPoint1, aBLPoint2: TBLPoint): boolean; overload;
  function geo_Eq (aBLVector1, aBLVector2: TBLVector): boolean; overload;
  function geo_Eq (aBLRect1, aBLRect2: TBLRect): boolean; overload;

  function  geo_LatOrLonIsZero (aBLRect: TBLRect): boolean; overload;
  function  geo_LatOrLonIsZero (aBLVector: TBLVector): boolean; overload;
  function  geo_LatOrLonIsZero (aBLPoint: TBLPoint): boolean;  overload;

  function  geo_GetVectorCenter (aPoint1,aPoint2: TBLPoint): TBLPoint; overload;
  function  geo_GetVectorCenter (aVector: TBLVector): TBLPoint; overload;

  function geo_Azimuth_Plus_Azimuth (aValue1, aValue2: double): double;


  function geo_FormatDegreeWithCoordSys (aValue: double; aType: TDegreeType; aCoordSys: integer): String ;overload;

  function geo_FormatDegreeWithCoordSys (aBLPoint: TBLPoint; aDestCoordSys: integer): String ; overload;

  function geo_Azimuth_Plus  (aValue1, aValue2: double): double;
  function geo_Azimuth_Minus  (aValue1, aValue2: double): double;

  function geo_GetCoordSysStr(aValue: Integer): string;

  procedure geo_XYPointArrayF_to_XYPointArray  (var aXYPointArrayF: TXYPointArrayF;
                                                var aXYPointArray: TXYPointArray);

  function geo_BLPointArrayF_to_BLPointArray(var aBLPointArrayF: TBLPointArrayF):
      TBLPointArray;

  function geo_BLPointArrayToStr(aBLPointArray: TBLPointArrayF): string;

  function geo_RoundXYPointsToXYRect(var aXYPoints: TXYPointArrayF): TXYRect;

  function geo_DefineRoundBLRectForArrayDinamic(aPoints: TBLPointArray): TBLRect;

  function geo_RotateBLVector_(aBLVector: TBLVector): TBLVector;

  function geo_UTM_to_GK_zone(aZone: integer): Integer;

procedure geo_DecodeDegree_into_Degree_and_Minute(aValue: Double; var aDegree:
    Integer; var aMin: Double);

function geo_RoundXYPointsToXYRectNew(var aXYPoints: TXYPointArray): TXYRect;

function geo_EncodeDegree_Deg_Min(aDeg: integer; aMin: double): double;

procedure geo_Check_XYRect(aRect: TXYRect);

function geo_XYRectToXYPoints_BottomLeft_first(aXYRect: TXYRect): TXYPointArray;

function geo_XYRectToXYPoints_(aXYRect: TXYRect): TXYPointArray;

function geo_BLRectToBLPoints_(aBLRect: TBLRect): TBLPointArray;

function MakeBLVector_LatLon(aValues : array of double): TBLVector;

function geo_MakeBLPointsByCircle_new(aCenter: TBLPoint; aRadius_m: Double):
    TBLPointArray;

function geo_GetRectCenter(aRect: TBLRect): TBLPoint;

function geo_BLRect_AsString(aBLRect: TBLRect): string;


  // ---------------------------------------------------------------

(*  function geo_XY_to_BL(aXYPoint: TXYPoint; aZone: integer=0; aFromElp: byte = EK_KRASOVSKY42;
      aToElp: Integer = EK_KRASOVSKY42): TBLPoint;
  function geo_BL_to_XY(aBLPoint: TBLPoint; aZone: integer=0; aElp: byte = EK_KRASOVSKY42): TXYPoint;
  // ---------------------------------------------------------------
*)

//===================================================================
implementation
//===================================================================

uses
//  u_geo_convert,
  u_geo_convert_new;


var
  LCoordSys : integer = EK_CK_42; // format XY с учетом этого эллипсоида


//const
//  CRLF = #13+#10;

const
  CRLF = #13+#10;
  LF = CRLF;  
  CR = CRLF;  


  
//--------------------------------------------------------
function IIF (Condition: boolean; TrueValue,FalseValue: variant): variant;
//--------------------------------------------------------
begin
  if Condition then Result:=TrueValue else Result:=FalseValue;
end;

function TruncFloat (aValue: double; aCount: integer=2): double;
begin
  Result:=Trunc(aValue*Power(10,aCount))/Power(10,aCount);
end;


//--------------------------------------------------------
function AsFloat (aValue: String): double;
//--------------------------------------------------------
var err: integer;
   S: string;
begin
  s:=Trim (aValue);

  s:=StringReplace (s, ',','.', [rfReplaceAll, rfIgnoreCase]);

 // s:=ReplaceStr (s, ',','.');
  Val(s, Result, err);

  if err<>0 then Result:=0;

end;

//const
 // NULL_POINT  : TBLPoint  = (b:0; L:0);


function geo_EncodeDegree(aDeg: integer; aMin, aSec: Double): double;
begin
  Result:=aDeg + aMin/60 + aSec/3600;
end;


function geo_EncodeDegree_Deg_Min(aDeg: integer; aMin: double): double;
var
  e: Double;
  eSec: Double;
begin
  e:=Frac(aMin);
  if e>0 then
    eSec:=e * 60
  else
    eSec:=0;

//  Result:=aDeg + aMin/60;// + Sec/3600;
  Result:=aDeg + Trunc(aMin)/60 + eSec/3600;

end;



//---------------------------------------------------------
function geo_StrToDegree (aValue: String): double;
//---------------------------------------------------------
var iPos,iDeg,iMin: integer;
  fSec: double;
//  s: string;
begin

  iPos:=Pos('.', aValue);
  if iPos>5 then begin
 //   s:=Copy(aValue, iPos-2, 2);
   // s:=Copy(aValue, iPos-4, 2);

    iDeg:=StrToIntDef(Copy(aValue, iPos-4, 2), 0);
    iMin:=StrToIntDef(Copy(aValue, iPos-2, 2), 0);

    fSec:=StrToIntDef(Copy(aValue, iPos+1, 4), 0) / 10000;

    Result:=geo_EncodeDegree(iDeg,iMin,fSec);
  end else
    Result:=0;
end;


//------------------------------------------------------------
function geo_DegreeToStr (aValue: double): String;
//------------------------------------------------------------
const
//  STR_BLPOINT_FORMAT1 = '%d°%.2d''%s%1.2f''''';
  STR_BLPOINT_FORMAT1 = '%d° %.2d'' %s%1.2f''''';
var
  angle: TAngle;

begin
  angle:=geo_DecodeDegree (aValue);
 // angle.Sec:=TruncFloat(angle.Sec,2);

  with angle do
    Result:=Format(STR_BLPOINT_FORMAT1, [deg, min, IIF(Trunc(sec)<10,'0',''),sec]);

  if aValue<0 then
    Result:='-'+Result;
end;



//------------------------------------------------------------
function geo_DecodeDegree (aValue: double): TAngle;
//------------------------------------------------------------


//const
// EXP_ = 0.000000001;
     //97,033333333
var
  bIsNegative: Boolean;
  e: Double;
  eSec: Double;
  s: string;
begin
  bIsNegative := aValue<0;

  aValue :=Abs(aValue);

  Result.Deg:=Trunc(aValue);

  Result.Min:=Trunc(Frac(aValue)*60);


  e:= (aValue - Trunc(aValue));
  e:= (aValue - Trunc(aValue)) * 60;


  Result.Min:=Trunc(e);
  Result.Sec:=(((aValue - Result.Deg) *60) - Result.Min) * 60;

  while Result.Sec>(60 - 0.001) do
  begin
    Result.min:=Result.min+1;
    Result.Sec:=Result.Sec-60;

{
  if Result.Sec<0 then
      Result.Sec:=0;

    if Result.min=60 then
    begin
      Result.min:=0;
      Result.Deg:=Result.Deg+1;
    end;
}

  end;

  while Result.Min>=60 do
  begin
    Result.min:=Result.min-60;
    Result.Deg:=Result.Deg+1;

  end;


  if Result.Sec<0 then
    Result.Sec:=0;




  if bIsNegative then
    Result.Deg:=-Result.Deg;

  assert(Result.Sec<60);
  assert(Result.Sec>=0);



end;


function geo_GetCenterMeridianBL (aZone: integer): double;
begin
  if aZone>0 then Result:=aZone*6 - 3
             else Result:=0;
end;

function geo_GetCenterMeridianXY (aZone: integer): double;
begin
  Result:=aZone*1000*1000 + 500*1000;
end;

function geo_EncodeAngle (Deg,Min,Sec: integer): TAngle;
begin
  Result.Deg:=Deg;
  Result.Min:=Min;
  Result.Sec:=Sec;
end;

//------------------------------------------------------
function geo_GetNearestAngle (Value: TAngle; aStepMinutes: integer): TAngle;
//------------------------------------------------------
var iVal: double;
begin
  Value.Sec:=0;
  iVal:=Value.Min mod aStepMinutes;

  if iVal < aStepMinutes/2 then Value.Min:=aStepMinutes * (Value.Min div aStepMinutes)
                           else Value.Min:=aStepMinutes * (Value.Min div aStepMinutes + 1);
  if Value.Min=60 then begin
    Value.Min:=0;
    Inc(Value.Deg);
  end;
  Result:=Value;
end;



//-------------------------------------------------------------
function geo_AngleMinusAngle (Value1,Value2: TAngle): TAngle;
//-------------------------------------------------------------
begin
  Result.Deg:=Value1.Deg - Value2.Deg;
  Result.Min:=Value1.Min - Value2.Min;
  Result.Sec:=Value1.Sec - Value2.Sec;

  if Result.Sec < 0 then begin
    Dec(Result.Min);  Result.Sec:=Result.Sec + 60;
  end;

  if Result.Min < 0 then begin
    Dec(Result.Deg);  Result.Min:=Result.Min + 60;
  end;

end;

//-------------------------------------------------------------
function geo_AnglePlusAngle (Value1,Value2: TAngle): TAngle;
//-------------------------------------------------------------
begin
  Result.Deg:=Value1.Deg + Value2.Deg;
  Result.Min:=Value1.Min + Value2.Min;
  Result.Sec:=Value1.Sec + Value2.Sec;

  if Result.Sec >= 60 then begin
    Inc(Result.Min);  Result.Sec:=Result.Sec - 60;
  end;

  if Result.Min >= 60 then begin
    Inc(Result.Deg);  Result.Min:=Result.Min - 60;
  end;

end;

//---------------------------------------------------------
function geo_FormatXYRect (aXYRect: TXYRect): String;
//---------------------------------------------------------
begin
  Result:=geo_FormatXYPoint (aXYRect.TopLeft) + ' - ' +
          geo_FormatXYPoint (aXYRect.BottomRight);
end;

//---------------------------------------------------------
function geo_FormatBLRect (aBLRect: TBLRect): String;
//---------------------------------------------------------
begin
  Result:=geo_FormatBLPoint (aBLRect.TopLeft) + ' - ' +
          geo_FormatBLPoint (aBLRect.BottomRight);
end;


//---------------------------------------------------------
function geo_FormatXYPoint (aXYPoint: TXYPoint): String;
//---------------------------------------------------------
const
//  STR_BLPOINT_FORMAT = '%d°%s%d''%s%1.1f'''''; //append '0'
//  STR_XYPOINT_FORMAT = '%1.3f N : %1.3f E'; //append '0'
//  STR_XYPOINT_FORMAT = '%1.3f X : %1.3f Y'; //append '0'
  STR_XYPOINT_FORMAT = '%1.3fX : %1.3fY'; //append '0'

begin
  Result:=Format(STR_XYPOINT_FORMAT, [aXYPoint.X/1000, aXYPoint.Y/1000]);
end;


//------------------------------------------------------------
function geo_FormatBLPoint (aBLPoint: TBLPoint): String;
// в градусах
//------------------------------------------------------------
const
//  STR_BLPOINT_FORMAT = '%d°%s%d''%s%1.1f'''''; //append '0'
//  STR_XYPOINT_FORMAT = '%1.3f N : %1.3f E'; //append '0'
//  STR_FORMAT_BLPOINT = '%s | %s';
  STR_FORMAT_BLPOINT = '%s  %s';
//  STR_FORMAT_BLPOINT = '%sN  |  %sE';

begin
  Result:=Format (STR_FORMAT_BLPOINT,
     [geo_FormatDegree_lat(aBLPoint.B),
      geo_FormatDegree_lon(aBLPoint.L)]);

(*

  if (aBLPoint.b=0) and (aBLPoint.l=0) then
    Result:=''
  else
    Result:=Format (STR_FORMAT_BLPOINT,
       [geo_DegreeToStr(aBLPoint.B),
        geo_DegreeToStr(aBLPoint.L)]);*)

end;


// -------------------------------------------------------------------
function geo_FormatAngleEx (aValue: double): String;
// -------------------------------------------------------------------
var angle: TAngle;
begin
  angle:=geo_DecodeDegree(Abs(aValue));
  Result:=Format('%d°', [angle.Deg]);
  if angle.Min<>0 then
    Result:=Result + Format('%2d''', [angle.Min]);

  if aValue<0 then
   Result:='-'+Result;
end;



//------------------------------------------------------------
function geo_FormatDegree_Lon(aValue: double): String;
//------------------------------------------------------------
var s: string;
begin
   if aValue<0 then begin
     s:='W';
     aValue:=Abs(aValue);
   end else

   if aValue>180 then begin
     s:='W';
     aValue:=360 - aValue
   end else
     s:='E';

  Result:=geo_DegreeToStr (aValue) + s;

end;



//------------------------------------------------------------
function geo_FormatDegree_Lat(aValue: double): String;
//------------------------------------------------------------
var s: string;
begin
  if aValue>0 then s:='N' else s:='S';

   aValue:=Abs(aValue);

  Result:=geo_DegreeToStr (aValue) + s;

end;




//-------------------------------------------------------------
function geo_TiltAngle  (aPoint1,aPoint2: TBLPoint; aSiteHeight1,aSiteHeight2: double): double;
//-------------------------------------------------------------
var tang,fDistance_m: double;
begin
  fDistance_m:=geo_DistanceBL_m (aPoint1,aPoint2);

  if Abs(fDistance_m) > 0.001 then
  begin
    tang:=(aSiteHeight2 - aSiteHeight1) / (fDistance_m);
    Result:=RadToDeg(ArcTan(tang)); //в градусах
  end else
    Result:=0;

end;


//-------------------------------------------------------------
function geo_AzimuthXY1(aPoint1,aPoint2: TBLPoint): double;
//-------------------------------------------------------------
var e: double;
  xyPoint1,xyPoint2: TxyPoint;
begin

  xyPoint1:=geo_Pulkovo42_BL_to_XY (aPoint1);
  xyPoint2:=geo_Pulkovo42_BL_to_XY (aPoint2);

(*  xyPoint1:=geo_BL_to_XY (aPoint1);
  xyPoint2:=geo_BL_to_XY (aPoint2);
*)

  if Abs(xyPoint1.Y-xyPoint2.Y)<0.01 then
  begin
    if xyPoint1.Y>xyPoint2.Y then Result := 90
                             else Result := 270;
  end else

  begin
    e:=(xyPoint1.X-xyPoint2.X) / (xyPoint1.Y-xyPoint2.Y);
    Result:=ArcTan (e);
  end;
end;

//-------------------------------------------------------------
function geo_Azimuth (aPoint1,aPoint2: TBLPoint): double;
//-------------------------------------------------------------
var fi: double;
begin
  //check small length < 100 m
  if geo_DistanceBL_m (aPoint1,aPoint2)<10 then
  begin
   { xyPoint1:=geo_BL_to_XY (aPoint1);
    xyPoint2:=geo_BL_to_XY (aPoint2);
    }

    Result:=geo_AzimuthXY1 (aPoint1,aPoint2);
  end;


  DegToRad2(aPoint1);  DegToRad2(aPoint2);
  Geoid_Dist (aPoint1.b,aPoint1.l, aPoint2.b,aPoint2.l, fi);
  Result:=RadToDeg(fi);
end;


function geo_BoundBLVector (aBLVector: TBLVector): TBLRect;
begin
  Result.TopLeft.B:=Max(aBLVector.Point1.B, aBLVector.Point2.B);
  Result.TopLeft.L:=Min(aBLVector.Point1.L, aBLVector.Point2.L);
  Result.BottomRight.B:=Min(aBLVector.Point1.B, aBLVector.Point2.B);
  Result.BottomRight.L:=Max(aBLVector.Point1.L, aBLVector.Point2.L);
end;


procedure geo_RoundedBLRect (aBLVector: TBLVector; var aBLRect: TBLRect);
begin
  if (aBLRect.TopLeft.B = 0) then
  begin
    aBLRect:=geo_BoundBLVector (aBLVector);
  end else begin
    aBLRect.TopLeft.B:=Max(aBLRect.TopLeft.B, Max(aBLVector.Point1.B, aBLVector.Point2.B) );
    aBLRect.TopLeft.L:=Min(aBLRect.TopLeft.L, Min(aBLVector.Point1.L, aBLVector.Point2.L) );
    aBLRect.BottomRight.B:=Min(aBLRect.BottomRight.B, Min(aBLVector.Point1.B, aBLVector.Point2.B) );
    aBLRect.BottomRight.L:=Max(aBLRect.BottomRight.L, Max(aBLVector.Point1.L, aBLVector.Point2.L) );
  end;
end;


//--------------------------------------------------------------------
procedure geo_ReduceBLPoint (var aBLPoint: TBLPoint);
//--------------------------------------------------------------------
begin
  aBLPoint.B:=TruncFloat (aBLPoint.B, MIF_PRECISION);
  aBLPoint.L:=TruncFloat (aBLPoint.L, MIF_PRECISION);
end;


function geo_ZoomRect (aBLRect: TBLRect; aOffset: double): TBLRect;
begin
  Result.TopLeft.B:=aBLRect.TopLeft.B + aOffset;
  Result.TopLeft.L:=aBLRect.TopLeft.L - aOffset;
  Result.BottomRight.B:=aBLRect.BottomRight.B - aOffset;
  Result.BottomRight.L:=aBLRect.BottomRight.L + aOffset;
end;


function geo_ComparePoints (aBLPoint1,aBLPoint2: TBLPoint): boolean;
begin
  geo_ReduceBLPoint (aBLPoint1);
  geo_ReduceBLPoint (aBLPoint2);

  Result:=(aBLPoint1.B=aBLPoint2.B) and (aBLPoint1.L=aBLPoint2.L);
end;



function NULL_VECTOR: TBLVector;// = (Point1:NULL_POINT; L:0);// MakeBLPoint()
begin
  Result.Point1:=NULL_BLPOINT;
  Result.Point2:=NULL_BLPOINT;
end;

(*
function NULL_RECT: TBLRect;// = (Point1:NULL_POINT; L:0);// MakeBLPoint()
begin
  Result.TopLeft:=NULL_BLPOINT;
  Result.BottomRight:=NULL_BLPOINT;
end;
*)
function NULL_BLRECT: TBLRect;
begin
  Result.TopLeft:=NULL_BLPOINT;
  Result.BottomRight:=NULL_BLPOINT;
end;


//---------------------------------------------------------
function geo_GetMapName (aBLPoint: TBLPoint; aScale: integer): String;
//---------------------------------------------------------
// sample: O-37
var
   b_angle, l_angle: TAngle;
   blTopLeft,blBottomLeft: TBLPoint;
   d,deltaB,deltaL: double;
   iRow,iCol,deg_b,deg_l,iIndex: integer;
   sName: string;
begin
  b_angle:=geo_DecodeDegree(aBLPoint.B);
  l_angle:=geo_DecodeDegree(aBLPoint.L);


  // Определить главный прямоугольник
  deg_b:=4 * (b_angle.Deg div 4);
  deg_l:=6 * (l_angle.Deg div 6);

  blTopLeft.B   :=geo_EncodeDegree (deg_b+4, 0,0);
  blTopLeft.L   :=geo_EncodeDegree (deg_l, 0,0);

  blBottomLeft.B:=geo_EncodeDegree (deg_b, 0,0);
  blBottomLeft.L:=geo_EncodeDegree (deg_l, 0,0);

  deltaB:=Abs(aBLPoint.B - blTopLeft.B);
  deltaL:=Abs(aBLPoint.L - blTopLeft.L);

  // 1.000.000
  iRow:=b_angle.Deg div 4;
  iCol:=l_angle.Deg div 6;
//  sName:=Char(Ord('A')+iRow) + Format('-%d', [31+iCol]);
  sName:=Char(Ord('A')+iRow) + Format('%.2d', [31+iCol]);

  case aScale of
    100: begin
           iRow:=Trunc(deltaB / (geo_EncodeDegree(4,0,0)/ 12) );
           iCol:=Trunc(deltaL / (geo_EncodeDegree(6,0,0)/ 12) );
           iIndex:=1+ 12 * iRow + iCol;
           Result:=sName + '-' + Format('%.3d',[iIndex]);
         end;
    200: begin
           iRow:=Trunc(deltaB / (geo_EncodeDegree(4,0,0)/ 6) );
           iCol:=Trunc(deltaL / (geo_EncodeDegree(6,0,0)/ 6) );
           iIndex:=1+ 6 * iRow + iCol;
           Result:=sName + '' + Format('%.2d',[iIndex]);
         end;

    500: begin
           iRow:=Trunc(deltaB / (geo_EncodeDegree(4,0,0)/ 3) );
           iCol:=Trunc(deltaL / (geo_EncodeDegree(6,0,0)/ 3) );
           iIndex:=1+ 3 * iRow + iCol;
           Result:=sName + '' + Format('%.2d_500',[iIndex]);
         end;

    1000: begin
           Result:=sName;
         end;

  else
    raise Exception.Create('scale');
  end;

end;



//------------------------------------------------------------
procedure geo_DecodeDegree_into_Degree_and_Minute(aValue: Double; var aDegree:
    Integer; var aMin: Double);
//------------------------------------------------------------
begin
  aDegree:=Trunc(aValue);
  aMin:=(Abs(aValue) - Abs(aDegree)) *60;
end;



//--------------------------------------------------------------------
function geo_EncodeDegreeFromString (Value: String): double;
//--------------------------------------------------------------------
var i1,i2,i3,i3_: integer;  ang: TAngle;
begin
  FillChar(ang, SizeOf(ang), 0);

  i1:=Pos('°',Value);
  ang.Deg:=StrToIntDef(Copy(Value,1,i1-1), 0);
  i2:=Pos('''',Value);
  ang.Min:=StrToIntDef(Copy(Value,i1+1,i2-i1-1), 0);
  i3:=Pos('''''',Value);
  if i3>0 then
    ang.Sec:=AsFloat(Copy(Value,i2+1,i3-i2-1))
  else begin
    i3_:=Pos('"',Value);
    if i3_>0 then ang.Sec:=AsFloat(Copy(Value,i2+1,i3_-i2-1));
  end;

  Result:=geo_EncodeDegreeRec(ang);
end;


//-------------------------------------------------------------------
function geo_RotateByAzimuth(aBLPoint: TBLPoint; aRadius_m, aAzimuth: double):
    TBLPoint;
//-------------------------------------------------------------------
// aRadius_m - meter
//0 ??????? ?? ?????
//-------------------------------------------------------------------
var
//  fRadAzimuth,fNewAzimuth,fTempAzimuth,dA: double;
  xyPoint,xyPoint2: TXYPoint;
  blPoint2: TblPoint;
  eAzimuth_rad: Double;
//  iCount: integer;
  iGK_Zone: Integer;

//  iZone : Integer;
//const
 // MAX_OFFSET = 1; //degree

begin
  iGK_Zone := geo_Get6ZoneL(aBLPoint.L);

  xyPoint:=geo_Pulkovo42_BL_to_XY(aBLPoint, iGK_Zone);
//  xyPoint:=geo_  BL_to_XY (aBLPoint);

  Assert(aRadius_m>0.01);

 // dA:=0;
 // iCount:=0;
 // fTempAzimuth:=aAzimuth;

 // repeat
    eAzimuth_rad:=DegToRad(90-(aAzimuth));

    xyPoint2.y:=xyPoint.y + cos(eAzimuth_rad)*aRadius_m;
    xyPoint2.x:=xyPoint.x + sin(eAzimuth_rad)*aRadius_m;

    blPoint2:=geo_Pulkovo42_XY_to_BL (xyPoint2, iGK_Zone);
    
//    blPoint2:=geo_XY_to_BL (xyPoint2);

 //   fNewAzimuth:=geo_Azimuth (aBLPoint, blPoint2);

 //   dA:=aAzimuth - fNewAzimuth;

  //  dA:=geo_Azimuth_Minus(aAzimuth ,fNewAzimuth);
  //
   // fTempAzimuth:=fTempAzimuth + dA / 2;

  //  Inc(iCount);
   // if iCount>1000 then
   //   Break;

 // until
  //   Abs(dA) < MAX_OFFSET;

  Result:=blPoint2;

end;



//-------------------------------------------------------------------
function geo_RotateByAzimuth_old(aBLPoint: TBLPoint; aRadius_m, aAzimuth: double):
    TBLPoint;
//-------------------------------------------------------------------
// aRadius_m - meter
//0 смотрит на север
//-------------------------------------------------------------------
var
  fRadAzimuth,fNewAzimuth,fTempAzimuth,dA: double;
  xyPoint,xyPoint2: TXYPoint;
  blPoint2: TblPoint;
  iCount: integer;
  iGK_Zone: Integer;

  iZone : Integer;
const
  MAX_OFFSET = 1; //degree
begin
  iGK_Zone := geo_Get6ZoneL(aBLPoint.L);

  xyPoint:=geo_Pulkovo42_BL_to_XY(aBLPoint, iGK_Zone);
//  xyPoint:=geo_  BL_to_XY (aBLPoint);

  if aRadius_m<0.01 then
    raise Exception.Create('');

  dA:=0;
  iCount:=0;
  fTempAzimuth:=aAzimuth;

  repeat
    fRadAzimuth:=DegToRad(90-(fTempAzimuth));
    xyPoint2.y:=xyPoint.y+cos(fRadAzimuth)*aRadius_m;
    xyPoint2.x:=xyPoint.x+sin(fRadAzimuth)*aRadius_m;

    blPoint2:=geo_Pulkovo42_XY_to_BL (xyPoint2, iGK_Zone);
    
//    blPoint2:=geo_XY_to_BL (xyPoint2);

    fNewAzimuth:=geo_Azimuth (aBLPoint, blPoint2);

    dA:=aAzimuth - fNewAzimuth;

  //  dA:=geo_Azimuth_Minus(aAzimuth ,fNewAzimuth);

    fTempAzimuth:=fTempAzimuth + dA / 2;

    Inc(iCount);
    if iCount>1000 then
      Break;

  until
     Abs(dA) < MAX_OFFSET;

  Result:=blPoint2;

end;

//------------------------------------------------------
function geo_GetEarthHeight(aOffset_KM, aDistance_KM: double; aRefraction:
    double=1.33): double;
//------------------------------------------------------
var fRadiusZem: double; //const Radius_Zem=8500; //эквив.радиус Земли (км)
begin
  Assert(aOffset_KM   >=0, 'aOffset_KM >=0');
  Assert(aDistance_KM >=0,'aDistance_KM >=0');

  if aOffset_KM >= aDistance_KM then
  begin
    Result := 0;
    Exit;
  end;


  // погрешность
  if Abs(aOffset_KM - aDistance_KM) < 0.00000001 then
    aOffset_KM := aDistance_KM;

  Assert(aOffset_KM <= aDistance_KM,
    Format('aOffset_KM <= aDistance_KM : %1.10f-%1.10f',[aOffset_KM, aDistance_KM]));


  if aRefraction=0 then
    aRefraction:=1.33;

  fRadiusZem:=12.740*aRefraction;  // коэфф.рефракции
  Result:=aOffset_KM*(aDistance_KM-aOffset_KM) / (fRadiusZem); {добавка на кривизну Земли (м)}

  Result:=TruncFloat(Result, 3);
end;


function geo_CompareBLRects (Rect1,Rect2: TBLRect): boolean;
// true - equal size and position
// false - not
begin
  Result:=((Rect1.TopLeft.B - Rect2.TopLeft.B)=0) and
          ((Rect1.TopLeft.L - Rect2.TopLeft.L)=0) and
          ((Rect1.BottomRight.B - Rect2.BottomRight.B)=0) and
          ((Rect1.BottomRight.L - Rect2.BottomRight.L)=0);
end;


function geo_DistanceBLV(aVector: TBLVector): double;
begin
  Result:=geo_DistanceBL_m (aVector.Point1, aVector.Point2);
end;

function geo_DistanceXY1 (x1,y1,x2,y2: double): double;
begin
  Result:=Sqrt(Sqr(x1-x2)+Sqr(y1-y2));
end;

function geo_DistanceXY_m (aPoint1,aPoint2: TXYPoint): double;
begin
  Result:=Sqrt(Sqr(aPoint1.X-aPoint2.X) +
               Sqr(aPoint1.Y-aPoint2.Y));
end;


//-------------------------------------------------------------
function geo_DistanceBL_m(aPoint1,aPoint2: TBLPoint): double;
//-------------------------------------------------------------
var fi: double;
begin

(*  if (Abs(aPoint1.B-aPoint2.B)<0.00000000001) and
     (Abs(aPoint1.L-aPoint2.L)<0.00000000001)
  then begin
     Result:=0; Exit;
  end;
*)
  DegToRad2(aPoint1);
  DegToRad2(aPoint2);

  try
    Result:=Trunc(Geoid_Dist (aPoint1.b,aPoint1.l, aPoint2.b,aPoint2.l, fi));
  //  Result:=Geoid_Dist (aPoint1.b,aPoint1.l, aPoint2.b,aPoint2.l, fi);
  except
  end;
end;


function geo_Get6ZoneXY (aXYPoint: TXYPoint): integer;
begin
  Result:=geo_Get6ZoneY (aXYPoint.Y);
end;

function geo_Get6ZoneBL(aBLPoint: TBLPoint): integer;
begin //in degree
  Result:=geo_Get6ZoneL (aBLPoint.L);
end;


function geo_Get6ZoneY (Y: double): integer;
begin
  Result:=Trunc(Y/(1000*1000)); //Номер 6-градусной зоны
end;

function geo_Get6ZoneL(aLon: double): integer;
begin
  //in degree
  Result:=Trunc(aLon/6) + 1; //Номер 6-градусной зоны
end;

//-----------------------------------------------
function geo_ZoomOut (aBLRect: TBLRect; aTimes: integer): TBLRect;
//-----------------------------------------------
var  // во сколько раз уменьшить
  fWidth,fHeight: double;
begin
  fHeight:=Abs(aBLRect.TopLeft.B - aBLRect.BottomRight.B) / 4;
  fWidth :=Abs(aBLRect.TopLeft.L - aBLRect.BottomRight.L) / 4;

  with aBLRect do
  begin
    Result.TopLeft.B    :=TopLeft.B + fHeight*aTimes;
    Result.TopLeft.L    :=TopLeft.L - fWidth*aTimes;
    Result.BottomRight.B:=BottomRight.B - fHeight*aTimes;
    Result.BottomRight.L:=BottomRight.L + fWidth*aTimes;
  end;
end;

//-----------------------------------------------
function geo_ZoomIn (aBLRect: TBLRect): TBLRect;
//-----------------------------------------------
var
  fWidth,fHeight: double;
begin
  fHeight:=Abs(aBLRect.TopLeft.B - aBLRect.BottomRight.B) / 4;
  fWidth :=Abs(aBLRect.TopLeft.L - aBLRect.BottomRight.L) / 4;

  with aBLRect do
  begin
    Result.TopLeft.B    :=TopLeft.B - fHeight;
    Result.TopLeft.L    :=TopLeft.L + fWidth;
    Result.BottomRight.B:=BottomRight.B + fHeight;
    Result.BottomRight.L:=BottomRight.L - fWidth;
  end;
end;


//---------------------------------------------------
function geo_IsBLRects_Crossed (aBLRect1, aBLRect2: TBLRect): boolean;
//---------------------------------------------------
    //---------------------------------------------------
    function geo_Is_BLRect1_in_BLRect2 (aBLRect1, aBLRect2: TBLRect): boolean;
    //---------------------------------------------------
    begin
      Result:=
       (((aBLRect2.TopLeft.L <= aBLRect1.TopLeft.L) and (aBLRect1.TopLeft.L <= aBLRect2.BottomRight.L))
       or
        ((aBLRect2.TopLeft.L <= aBLRect1.BottomRight.L) and (aBLRect1.BottomRight.L <= aBLRect2.BottomRight.L))
        )
         and
       (
       ((aBLRect2.TopLeft.B >= aBLRect1.TopLeft.B) and (aBLRect1.TopLeft.B >= aBLRect2.BottomRight.B))
       or
       ((aBLRect2.TopLeft.B >= aBLRect1.BottomRight.B) and (aBLRect1.BottomRight.B >= aBLRect2.BottomRight.B))
       )
    end;

begin
  Result:=geo_Is_BLRect1_in_BLRect2(aBLRect1, aBLRect2) or
          geo_Is_BLRect1_in_BLRect2(aBLRect2, aBLRect1);

end;


//--------------------------------------------------------------------
function geo_MoveBLRectToCenter (aNewRectCenter: TBLPoint; aBLRect: TBLRect): TBLRect;
//--------------------------------------------------------------------
var dDelta_B, dDelta_L: double;
    blOldCenter: TBLPoint;
begin
  blOldCenter:= geo_GetBLRectCenter (aBLRect);

  dDelta_B:= aNewRectCenter.B - blOldCenter.B;
  dDelta_L:= aNewRectCenter.L - blOldCenter.L;

  Result.TopLeft.B    := aBLRect.TopLeft.B + dDelta_B;
  Result.TopLeft.L    := aBLRect.TopLeft.L + dDelta_L;
  Result.BottomRight.B:= aBLRect.BottomRight.B + dDelta_B;
  Result.BottomRight.L:= aBLRect.BottomRight.L + dDelta_L;
end;


//---------------------------------------------------------
function geo_GetRoundBLRect (aRects: TBLRectArray): TBLRect;
//---------------------------------------------------------
var
  i: integer;
  b: Boolean;
begin
  Result:=MakeBLRect(0,0,0,0);
  b:=False;

  for i:=0 to High(aRects) do
  begin
    if geo_Eq_Rect(aRects[i], NULL_BLRECT) then
      Continue;

    if not b then
    begin
      Result:=aRects[i];
      b := True;
      Continue;
    end;


    with aRects[i] do
    begin
      if (TopLeft.B     > Result.TopLeft.B)     then Result.TopLeft.B:=TopLeft.B;
      if (BottomRight.L > Result.BottomRight.L) then Result.BottomRight.L:=BottomRight.L;

      if (TopLeft.L     < Result.TopLeft.L)     then Result.TopLeft.L:=TopLeft.L;
      if (BottomRight.B < Result.BottomRight.B) then Result.BottomRight.B:=BottomRight.B;
    end;

  end;
end;

//---------------------------------------------------------
procedure geo_Check_XYRect(aRect: TXYRect);
//---------------------------------------------------------
begin
  Assert(aRect.TopLeft.X > aRect.BottomRight.X, 'aRect.TopLeft.X > aRect.BottomRight.X');
  Assert(aRect.TopLeft.Y < aRect.BottomRight.Y, 'aRect.TopLeft.Y < aRect.BottomRight.Y');
end;


//---------------------------------------------------------
function geo_GetRoundXYRect_(aRects: TXYRectArray): TXYRect;
//---------------------------------------------------------
var i: integer;
begin
  Result:=MakeXYRect(0,0,0,0);

  for i:=0 to High(aRects) do
    with aRects[i] do
    begin
      geo_Check_XYRect (aRects[i]);

      // -------------------------------
     // Assert(TopLeft.X > BottomRight.X);
     // Assert(TopLeft.Y < BottomRight.Y);
      // -------------------------------

      if (TopLeft.Y=0) and (TopLeft.X=0) and (BottomRight.X=0) and (BottomRight.Y=0)
        then  Continue;

      if (TopLeft.X     > Result.TopLeft.X)     then Result.TopLeft.X:=TopLeft.X;
      if (BottomRight.Y > Result.BottomRight.Y) then Result.BottomRight.Y:=BottomRight.Y;

      if (Result.TopLeft.Y=0)     or (TopLeft.Y < Result.TopLeft.Y) then Result.TopLeft.Y:=TopLeft.Y;
      if (Result.BottomRight.X=0) or (BottomRight.X < Result.BottomRight.X) then Result.BottomRight.X:=BottomRight.X;
    end;
end;


//-------------------------------------------------
function geo_IsXYPointInXYPolygonF(aXYPoint: TXYPoint; var aXYPolygon:
    TXYPointArrayF): boolean;
//-------------------------------------------------
    //-------------------------------------------------
    function PointDelta (A,B: TXYPoint): TXYPoint;
    //-------------------------------------------------
    // - вычитание точек
    begin
      Result.x:=A.x-B.x;
      Result.y:=A.y-B.y;
    end;

    //-------------------------------------------------
    function PolarAngle (aPoint: TXYPoint): double;
    //-------------------------------------------------
    // - полярные координаты точки
    var alpha: double;
    begin
      with aPoint do
      begin
        if (x=0) and (y=0) then Result:=-1  else
        if (x=0) then Result:=IIF(y>0, 90, 270)
        else begin
          alpha:=RadToDeg(ArcTan (y/x));
          if x>0 then Result:=IIF(y>=0, alpha, 360+alpha)
                 else Result:=180+alpha;
        end;
      end;
    end;

    //-------------------------------------------------
    function SignedAngle (aPoint, aStartPoint,aEndPoint : TXYPoint ): double;
    //-------------------------------------------------
    var p1,p2: TXYPoint;  angle1,angle2,delta: double;
    begin
      p1:=PointDelta (aStartPoint, aPoint);
      p2:=PointDelta (aEndPoint, aPoint);
      angle1:=PolarAngle (p1);
      angle2:=PolarAngle (p2);

      if (angle1<0) or (angle2<0) then begin Result:=180; exit; end;

      delta:=angle1 - angle2 ;
      if (delta=180) or (delta=-180) then Result:=180 else
      if (delta<-180) then Result:=delta+360 else
      if (delta>180)  then Result:=delta-360
                      else Result:=delta;
    end;
    //-------------------------------------------------

// - определить, находится ли точка внутри полигона
var
  total,x: double;
  i,iCount: integer;
  p1,p2: TXYPoint;
begin
  iCount:=aXYPolygon.Count;

  total := 0;
  for i:=0 to iCount-1 do begin
    p1:=aXYPolygon.Items[i];
    if i<iCount-1 then p2:=aXYPolygon.Items[i+1]
                  else p2:=aXYPolygon.Items[0];
    x:=SignedAngle (aXYPoint, p1,p2);
    if (x=180) then begin Result:=true; Exit; end;
    total := total + x;
  end;
  total := Abs(total);
  if (total > -0.1) and (total < 0.1) then Result := False
  else if (total > 359.9) and (total < 360.1) then Result := True;



end;

//------------------------------------------------
function geo_GetBLPolygonLeft6Zone(var aBLPolygon: TBLPointArrayF): integer;
// get 6Zone for left point
//geo_GetBLPolygonLeft6Zone
//-------------------------------------------------
var
  min_l: double;
  i: Integer;
begin
  min_l:=0;

  //get most left
  for i:=0 to aBLPolygon.Count-1 do
    if (min_l=0) or (min_l > aBLPolygon.Items[i].L) then
      min_l := aBLPolygon.Items[i].L;

  Result:=geo_Get6ZoneL(min_l);
end;

//------------------------------------------------
function geo_IsBLPointInBLPolygon(aBLPoint: TBLPoint; var aBLPolygon: TBLPointArrayF): boolean;
//-------------------------------------------------
var
  xyPoint: TxyPoint;
  xyPolygon: TXYPointArrayF;
  i,iZone: integer;
begin
  iZone:=geo_GetBLPolygonLeft6Zone(aBLPolygon);

  xyPoint:=geo_Pulkovo42_BL_to_XY (aBLPoint, iZone);
//  xyPoint:=geo_BL_to_XY (aBLPoint, iZone);

  xyPolygon.Count:=aBLPolygon.Count;
  for i:=0 to aBLPolygon.Count-1 do
    xyPolygon.Items[i]:=geo_Pulkovo42_BL_to_XY (aBLPolygon.Items[i], iZone);
//    xyPolygon.Items[i]:=geo_BL_to_XY (aBLPolygon.Items[i], iZone);

  Result:=geo_IsXYPointInXYPolygonF (xyPoint, xyPolygon);

end;



function geo_EncodeDegreeRec (Value: TAngle): double;
begin
  Result:=geo_EncodeDegree (Value.Deg, Value.Min, Value.Sec);
end;

function geo_GetBLRectCenter (aBLRect: TBLRect): TBLPoint;
begin
  Result.B := aBLRect.BottomRight.B + Abs(aBLRect.TopLeft.B - aBLRect.BottomRight.B)/2;
  Result.L := aBLRect.TopLeft.L     + Abs(aBLRect.TopLeft.L - aBLRect.BottomRight.L)/2;

end;

function MakeBLRect (b1,l1, b2,l2: double): TBLRect;
begin
  FillChar (Result, SizeOf(Result), 0);

  Result.TopLeft.B:=b1;     Result.TopLeft.L:=l1;
  Result.BottomRight.B:=b2; Result.BottomRight.L:=l2;
end;


function MakeXYRect (x1,y1, x2,y2: double): TXYRect;
begin
  Result.TopLeft.X:=x1;     Result.TopLeft.Y:=y1;
  Result.BottomRight.X:=x2; Result.BottomRight.Y:=y2;
end;


//---------------------------------------------------------
function  geo_Eq_Point (aBLPoint1, aBLPoint2: TBLPoint): boolean;
//---------------------------------------------------------
begin
  Result:= geo_ComparePoints(aBLPoint1, aBLPoint2);
end;

//---------------------------------------------------------
function  geo_Eq_vector (aBLVector1, aBLVector2: TBLVector): boolean;
//---------------------------------------------------------
begin
  Result:=geo_Eq_Point (aBLVector1.Point1, aBLVector2.Point1) and
          geo_Eq_Point (aBLVector1.Point2, aBLVector2.Point2);
end;

//---------------------------------------------------------
function  geo_Eq_Rect (aBLRect1, aBLRect2: TBLRect): boolean;
//---------------------------------------------------------
begin
  Result:= geo_CompareBLRects (aBLRect1, aBLRect2);
end;


//--------------------------------------------------------------------
function geo_MoveBLRect(const aBLRect: TBLRect; aLat, aLon: double): TBLRect;
//--------------------------------------------------------------------
begin
  Result.TopLeft.B:=aBLRect.TopLeft.B + aLat;
  Result.BottomRight.B:=aBLRect.BottomRight.B + aLat;

  Result.TopLeft.L:=aBLRect.TopLeft.L + aLon;
  Result.BottomRight.L:=aBLRect.BottomRight.L + aLon;

end;



procedure geo_UpdateRectForPoint (aPoint: TBLPoint; var aRect: TBLRect);
begin
  if (aPoint.B=0) or (aPoint.L=0) then Exit;

  if aRect.TopLeft.B=0 then begin
    aRect.TopLeft:=aPoint;
    aRect.BottomRight:=aPoint;
    Exit;
  end;

  if aPoint.B > aRect.TopLeft.B     then  aRect.TopLeft.B:=aPoint.B;
  if aPoint.L < aRect.TopLeft.L     then  aRect.TopLeft.L:=aPoint.L;
  if aPoint.B < aRect.BottomRight.B then  aRect.BottomRight.B:=aPoint.B;
  if aPoint.L > aRect.BottomRight.L then  aRect.BottomRight.L:=aPoint.L;
end;


//---------------------------------------------------------
function  geo_LatOrLonIsZero_Rect (aBLRect: TBLRect): boolean;
//---------------------------------------------------------
begin
  Result:= false;

  if (aBLRect.TopLeft.B = 0) or (aBLRect.TopLeft.L = 0) or
     (aBLRect.BottomRight.B = 0) or (aBLRect.BottomRight.L = 0) then
  Result:= true;
end;

//---------------------------------------------------------
function  geo_LatOrLonIsZero_Vector (aBLVector: TBLVector): boolean;
//---------------------------------------------------------
begin
  Result:= false;

  if (aBLVector.Point1.B = 0) or (aBLVector.Point1.L = 0) or
     (aBLVector.Point2.B = 0) or (aBLVector.Point2.L = 0) then
  Result:= true;
end;


//---------------------------------------------------------
function  geo_LatOrLonIsZero_Point (aBLPoint: TBLPoint): boolean;
//---------------------------------------------------------
begin
  Result:= false;

  if (aBLPoint.B = 0) or (aBLPoint.L = 0) then
  Result:= true;
end;

//------------------------------------------------------
procedure geo_BLRectToBLPoints (aBLRect: TBLRect; var aPoints: TBLPointArray);
//------------------------------------------------------
begin
  if aBLRect.TopLeft.B = 0 then
  begin
   // aPoints.Count:=0;
    SetLength (aPoints, 0);
    Exit;
  end;

//  aPoints.Count:=4;
  SetLength (aPoints, 4);

  with aBLRect do begin
    aPoints[0]:=TopLeft;
    aPoints[1]:=MakeBLPoint (TopLeft.B, BottomRight.L);
    aPoints[2]:=BottomRight;
    aPoints[3]:=MakeBLPoint (BottomRight.B, TopLeft.L);
  end;
end;


//------------------------------------------------------
function geo_BLRectToBLPoints_(aBLRect: TBLRect): TBLPointArray;
//------------------------------------------------------
begin
  if aBLRect.TopLeft.B = 0 then
  begin
   // aPoints.Count:=0;
    SetLength (result, 0);
    Exit;
  end;

//  aPoints.Count:=4;
  SetLength (result, 4);

  with aBLRect do
  begin
    result[0]:=TopLeft;
    result[1]:=MakeBLPoint (TopLeft.B, BottomRight.L);
    result[2]:=BottomRight;
    result[3]:=MakeBLPoint (BottomRight.B, TopLeft.L);
  end;
end;



//------------------------------------------------------
procedure geo_BLRectToBLPointsF (aBLRect: TBLRect; var aPoints: TBLPointArrayF);
//------------------------------------------------------
begin
  if aBLRect.TopLeft.B = 0 then begin
    aPoints.Count:=0;
 //   SetLength (Result, 0);
    Exit;
  end;

  aPoints.Count:=4;
//  SetLength (Result, 4);
  with aBLRect do begin
    aPoints.Items[0]:=TopLeft;
    aPoints.Items[1]:=MakeBLPoint (TopLeft.B, BottomRight.L);
    aPoints.Items[2]:=BottomRight;
    aPoints.Items[3]:=MakeBLPoint (BottomRight.B, TopLeft.L);
  end;
end;



function MakeXYPoint (aX,aY: double): TXYPoint;
begin
  Result.X:=aX;
  Result.Y:=aY;
end;

function MakeBLPoint (aB,aL: double): TBLPoint;
begin
  Result.B:=aB;
  Result.L:=aL;
end;


function geo_UTM_to_GK_zone(aZone: integer): Integer;
begin
 (*
   Счет зон в проекции Гаусса-Крюгера ведется от Гринвичского меридиана на восток. Средний меридиан зоны называется осевым. Долгота осевого меридиана L0 любой зоны в восточном полушарии подсчитывается по формуле:
   L0=6°*n - 3°
   а в западном – по формуле:
   L0=360° - (6°*n - 3°),
   где n - номер зоны.
 *)


  Result := aZone - 30;
end;




//-------------------------------------------------------------------
function geo_RotateBLVector_(aBLVector: TBLVector): TBLVector;
//-------------------------------------------------------------------
// Direction: 0-левый->правый; 1-правый ->левый; 2 - not def
//-------------------------------------------------------------------
begin
  result.Point1:=aBLVector.Point2;
  result.Point2:=aBLVector.Point1;
end;


//------------------------------------------------------
procedure geo_XYRectToXYPointsF(aXYRect: TXYRect; var aPoints: TXYPointArrayF);
//  procedure geo_XYRectToXYPoints(aXYRect: TXYRect; var aPoints: TXYPointArrayF);
//------------------------------------------------------
begin
  aPoints.Count:=4;
  //SetLength (Result, 4);

  with aXYRect do
  begin
    aPoints.Items[0]:=TopLeft;
    aPoints.Items[1]:=MakeXYPoint (TopLeft.X, BottomRight.Y);
    aPoints.Items[2]:=BottomRight;
    aPoints.Items[3]:=MakeXYPoint (BottomRight.X, TopLeft.Y);
  end;
end;



(*
  geo_XYRectToXYPointsF(rXYRect_UTM, arrXYPointArrayF_new);

  arrXYPointArrayF_GK.Count :=4;


  for i:=0 to arrXYPointArrayF_new.Count-1 do
  begin
    xy :=arrXYPointArrayF_new.Items[i];

    xy_gk := geo_UTM_to_GK(xy, aUTM_zone);

    arrXYPointArrayF_GK.Items[i] := xy_gk;
  end;


*)

//------------------------------------------------------
function geo_XYRectToXYPoints_(aXYRect: TXYRect): TXYPointArray;
//  procedure geo_XYRectToXYPoints(aXYRect: TXYRect; var aPoints: TXYPointArrayF);
//------------------------------------------------------
begin
 // aPoints.Count:=4;
  SetLength (Result, 4);

  with aXYRect do
  begin
    Result[0]:=TopLeft;
    Result[1]:=MakeXYPoint (TopLeft.X, BottomRight.Y);
    Result[2]:=BottomRight;
    Result[3]:=MakeXYPoint (BottomRight.X, TopLeft.Y);
  end;
end;



//------------------------------------------------------
function geo_XYRectToXYPoints_BottomLeft_first(aXYRect: TXYRect): TXYPointArray;
//------------------------------------------------------
    //   Указывают координаты вершин четырехугольника, определяющего зону наложения.
    //   Координаты следует указывать против часовой стрелки,
    //   начиная с нижнего левого угла наложенного изображения.

begin
 // aPoints.Count:=4;
  SetLength (Result, 4);

  with aXYRect do
  begin
    Result[0]:=MakeXYPoint (BottomRight.X, TopLeft.Y);
    result[1]:=BottomRight;
    result[2]:=MakeXYPoint (TopLeft.X, BottomRight.Y);
    result[3]:=TopLeft;
  end;
end;



//----------------------------------------------------
function geo_DefineRoundBLRectForArrayF(var aPoints: TBLPointArrayF): TBLRect;
//----------------------------------------------------
//function geo_DefineRoundBLRect(var aPoints: TBLPointArrayF): TBLRect;
// определение обрамляющего прямоугольника по точкам
//----------------------------------------------------
var i: integer;  b,l: double;
    b1,l1, b2,l2: double;
begin
  b1:=0; l1:=0; b2:=0; l2:=0;

  for i:=0 to aPoints.Count-1 do
  begin
    b:=aPoints.Items[i].B;
    l:=aPoints.Items[i].L;  //if gX=0 then Continue;

    if (b > b1) or (b1=0) then b1:=b;
    if (l < l1) or (l1=0) then l1:=l;
    if (b < b2) or (b2=0) then b2:=b;
    if (l > l2) or (l2=0) then l2:=l;
  end;

  Result.TopLeft.B:=b1;
  Result.TopLeft.L:=l1;
  Result.BottomRight.B:=b2;
  Result.BottomRight.L:=l2;

end;

//----------------------------------------------------
function geo_DefineRoundBLRectForArrayDinamic(aPoints: TBLPointArray): TBLRect;
//----------------------------------------------------
//function geo_DefineRoundBLRect(var aPoints: TBLPointArrayF): TBLRect;
// определение обрамляющего прямоугольника по точкам
//----------------------------------------------------
var i: integer;  b,l: double;
    b1,l1, b2,l2: double;
begin
  b1:=0; l1:=0; b2:=0; l2:=0;

  for i:=0 to High(aPoints) do
  begin
    b:=aPoints[i].B;
    l:=aPoints[i].L;  //if gX=0 then Continue;

    if (b > b1) or (b1=0) then b1:=b;
    if (l < l1) or (l1=0) then l1:=l;
    if (b < b2) or (b2=0) then b2:=b;
    if (l > l2) or (l2=0) then l2:=l;
  end;

  Result.TopLeft.B:=b1;
  Result.TopLeft.L:=l1;
  Result.BottomRight.B:=b2;
  Result.BottomRight.L:=l2;

end;



//------------------------------------------------------
procedure geo_MakeCirclePointsF(aCenter: TBLPoint; aRadiusM: double;
    aPointCount: integer; var aPoints: TBLPointArrayF);
//------------------------------------------------------

var i: integer;  b,l: double;  blPoint: TBLPoint;
begin
  if aRadiusM<1 then
  begin
    aPoints.Count:=0;
    Exit;
  end;

  aPoints.Count:=aPointCount;

  for i:=0 to aPointCount-1 do
    aPoints.Items[i]:=geo_RotateByAzimuth (aCenter, aRadiusM, (i/aPointCount)*360);
end;

//---------------------------------------------------------
procedure geo_BLRectByCircle(aBLCenter: TBLPoint; aRadius: double; var
    aBLPoints: TBLPointArrayF);
//---------------------------------------------------------
var xyPoints: TXYPointArrayF;
//    blPoints: TBLPointArrayF;
    xyRect: TXYRect;
    i: integer;
  iZone: Integer;
begin
  iZone:=geo_Get6ZoneBL(aBLCenter);

  xyRect:=geo_XYRectByCircle (aBLCenter, aRadius);
  geo_XYRectToXYPointsF (xyRect, xyPoints);

  aBLPoints.Count:=xyPoints.Count;

//  SetLength (blPoints, xyPoints.Count);// High(xyPoints)+1);

  for i:=0 to xyPoints.Count-1 do //High(xyPoints) do
    aBLPoints.Items[i]:=geo_Pulkovo42_XY_to_BL (xyPoints.Items[i], iZone);

//    aBLPoints.Items[i]:=geo_XY_to_BL (xyPoints.Items[i]);

//  Result:=blPoints;
end;



//---------------------------------------------------------
function geo_MakeBLRectByCircle(aBLCenter: TBLPoint; aRadius_m: double):
    TBLRect;
//---------------------------------------------------------
var
  x,y: double; //определить описанный прямоугольник
  iZone: Integer;
  xyPoint,xyPos1,xyPos2: TXYPoint;
begin
  iZone:=geo_Get6ZoneBL(aBLCenter);


  xyPoint:=geo_Pulkovo42_BL_to_XY (aBLCenter);
  xyPos1:=MakeXYPoint(xyPoint.x+aRadius_m, xyPoint.y-aRadius_m);
  xyPos2:=MakeXYPoint(xyPoint.x-aRadius_m, xyPoint.y+aRadius_m);

  Result.TopLeft    :=geo_Pulkovo42_XY_to_BL (xyPos1, iZone);
  Result.BottomRight:=geo_Pulkovo42_XY_to_BL (xyPos2, iZone);

//  Result.BottomRight:=geo_XY_to_BL (xyPos2);

end;



//----------------------------------------------------
function geo_GetSquareAroundPoint(aBLPoint: TBLPoint; aBorderWidth: integer):
    TBLRect;
//----------------------------------------------------
var xyPos,xyPos1,xyPos2: TXYPoint;
  iZone: Integer;
begin
  iZone:=geo_Get6ZoneBL(aBLPoint);

  xyPos:=geo_Pulkovo42_BL_to_XY (aBLPoint, iZone);

  xyPos1.X:=xyPos.X + aBorderWidth/2;
  xyPos1.Y:=xyPos.Y - aBorderWidth/2;
  xyPos2.X:=xyPos.X - aBorderWidth/2;
  xyPos2.Y:=xyPos.Y + aBorderWidth/2;

  Result.TopLeft    :=geo_Pulkovo42_XY_to_BL (xyPos1, iZone);
  Result.BottomRight:=geo_Pulkovo42_XY_to_BL (xyPos2, iZone);

(*  Result.TopLeft    :=geo_XY_to_BL (xyPos1);
  Result.BottomRight:=geo_XY_to_BL (xyPos2);
*)

end;



function geo_GetVectorCenterP(aPoint1,aPoint2: TBLPoint): TBLPoint;
begin
  Result.B:= (aPoint1.B + aPoint2.B) / 2;
  Result.L:= (aPoint1.L + aPoint2.L) / 2;
end;


function geo_GetVectorCenterV(aVector: TBLVector): TBLPoint;
begin
  Result:=geo_GetVectorCenterP (aVector.Point1, aVector.Point2);
end;


//---------------------------------------------------------
function geo_MakeXYRectByCircle (aCenter: TXYPoint;
                                 aRadius: double;
                                 aBoundRect: TXYRect): TXYRect;
//---------------------------------------------------------
//определить описанный прямоугольник
//- aBoundRect: TXYRect - ограничивающий прямоугольник
var xyInRect: TXYRect;
begin
  xyInRect.TopLeft.X    :=Round(aCenter.X + aRadius + 1);
  xyInRect.TopLeft.Y    :=Round(aCenter.Y - aRadius - 1);
  xyInRect.BottomRight.X:=Round(aCenter.X - aRadius);
  xyInRect.BottomRight.Y:=Round(aCenter.Y + aRadius);

  // проверить на пересечение
  if (xyInRect.TopLeft.X < aBoundRect.BottomRight.X) or
     (xyInRect.BottomRight.X > aBoundRect.TopLeft.X) or
     (xyInRect.TopLeft.Y > aBoundRect.BottomRight.Y) or
     (xyInRect.BottomRight.Y < aBoundRect.TopLeft.Y)
   then begin
     Result:=MakeXYRect (0,0,0,0);
     Exit;
   end;

  if xyInRect.TopLeft.X > aBoundRect.TopLeft.X         then xyInRect.TopLeft.X:=aBoundRect.TopLeft.X;
  if xyInRect.TopLeft.Y < aBoundRect.TopLeft.Y         then xyInRect.TopLeft.Y:=aBoundRect.TopLeft.Y;
  if xyInRect.BottomRight.X < aBoundRect.BottomRight.X then xyInRect.BottomRight.X:=aBoundRect.BottomRight.X;
  if xyInRect.BottomRight.Y > aBoundRect.BottomRight.Y then xyInRect.BottomRight.Y:=aBoundRect.BottomRight.Y;
  Result:=xyInRect;
end;



//-------------------------------------------------------
function geo_GetRoundXYRect (aBLRect: TBLRect; aZone: integer=0): TXYRect;
//-------------------------------------------------------
var xyPoints: TXYPointArrayF;
    i: integer;
    blPoints: TBLPointArrayF;
    blBottomCenter: TBLPoint;
    xyBottomCenter: TXYPoint;
  str: string;
begin
  if aBLRect.TopLeft.B = 0 then
  begin
    Result:=MakeXYRect (0,0,0,0);
    Exit;
  end;

  //xyPoints:=
  geo_BLRectToXYPoints (aBLRect, xyPoints, aZone);
  geo_BLRectToBLPointsF (aBLRect, blPoints);

  blBottomCenter.B:=(aBLRect.BottomRight.B);
  blBottomCenter.L:=(aBLRect.BottomRight.L  +  aBLRect.TopLeft.L) / 2;

  xyBottomCenter:=geo_Pulkovo42_BL_to_XY (blBottomCenter, aZone);
//  xyBottomCenter:=geo_BL_to_XY (blBottomCenter, aZone);

  str:='';

  for i:=0 to 3 do
  begin
    if i=0 then begin
      Result.TopLeft    :=xyPoints.Items[0];
      Result.BottomRight:=xyPoints.Items[0];
    end else begin
      Result.TopLeft.X:=Max(Result.TopLeft.X, xyPoints.Items[i].X);
      Result.TopLeft.Y:=Min(Result.TopLeft.Y, xyPoints.Items[i].Y);

      Result.BottomRight.X:=Min(Result.BottomRight.X, xyPoints.Items[i].X);
      Result.BottomRight.Y:=Max(Result.BottomRight.Y, xyPoints.Items[i].Y);
    end;

    str:=str + geo_FormatBLPoint (blPoints.Items[i]) + ' ---> ' +
               geo_FormatXYPoint (xyPoints.Items[i]); // + CRLF;
  end;
        
  Result.BottomRight.X:=Min(Result.BottomRight.X, xyBottomCenter.X);

end;


function geo_BLPointInRect (aPoint:TBLPoint; aRect:TBLRect): boolean;
begin
  with aPoint, aRect do
    Result:=(TopLeft.B>=B) and (B>=BottomRight.B) and
            (TopLeft.L<=L) and (L<=BottomRight.L);
end;


function geo_XYPointInRect(aXY: TXYPoint; aRect: TXYRect): boolean;
begin
  with aRect do
    Result:=(TopLeft.X>=aXY.X) and (aXY.X>=BottomRight.X) and
            (TopLeft.Y<=aXY.Y) and (aXY.Y<=BottomRight.Y);
end;

{
function geo_XYRectInXYRect(aSrc: TXYRect; aDest: TXYRect): boolean;
begin
 // with aDest do
 //   Result:=(aDest.TopLeft.X>=aXY.X) and (X>=BottomRight.aXY.X) and
  //          (TopLeft.Y<=aXY.Y) and (Y<=BottomRight.aXY.Y);
end;
}


//-------------------------------------------------------
procedure geo_BLRectToXYPoints(aBLRect: TBLRect; var aXYPoints: TXYPointArrayF;
    aZone: integer = 0);
//-------------------------------------------------------
var blPoints: TBLPointArrayF;  i: integer;
begin
  if aZone=0 then
    aZone:=geo_Get6ZoneL(aBLRect.TopLeft.L);

  geo_BLRectToBLPointsF (aBLRect, blPoints);

  aXYPoints.Count:=4;

  for i:=0 to 3 do
    aXYPoints.Items[i]:=geo_Pulkovo42_BL_to_XY (blPoints.Items [i], aZone);

//    aXYPoints.Items[i]:=geo_BL_to_XY (blPoints.Items [i], aZone);
end;

procedure geo_SetCoordSys(aCoordSys: integer);
begin
  LCoordSys:=aCoordSys;
end;



function geo_GetCoordSys: Integer;
begin
  Result := LCoordSys;
end;


//------------------------------------------------------
procedure geo_MakeBezierF(var aBLPoints: TBLPointArrayF; var aOutBLPoints: TBLPointArrayF);
//------------------------------------------------------

var
  arrPointsIn: array of TXYPoint;
  arC: array of single;

  //----------------------------------
  function GetBinomialCoefficient(m, i: integer): single;
  //----------------------------------
    function Factorial(x: integer): double;
    var
      i: integer;
    begin
      result := 1;
      for i := 2 to x do
        result := result * i;
    end;
  begin
    result := Factorial(m) / (Factorial(i) * Factorial(m - i));
  end;

  //----------------------------------
  procedure SetSizeBezier(aN: integer);
  //----------------------------------
  var i: integer;
  begin
    SetLength(arC, aN+1);

    for i := 0 to aN do
      arC[i] := GetBinomialCoefficient(aN, i);
  end;

const
  ARR_COUNT = 50;

var
  arrXYPointsOut: array of TXYPoint;
  dStep, qx, qy, t, q: single;
  iZone, i, j, iCount: integer;
  tempX, tempY: single;
  dAzimuth: double;
  blVector: TBLVector;
begin
  if aBLPoints.Count=0 then
  begin
    aOutBLPoints.Count:=0;
    Exit;
  end;


  SetLength(arrXYPointsOut, ARR_COUNT+1);
  SetLength(arrPointsIn, aBLPoints.Count);

  SetSizeBezier(aBLPoints.Count-1);

  iZone:=geo_Get6ZoneBL (aBLPoints.Items[0]);

  for i := 0 to aBLPoints.Count - 1 do
    arrPointsIn[i]:= geo_Pulkovo42_BL_to_XY(aBLPoints.Items[i], iZone);
//    arrPointsIn[i]:= geo_BL_to_XY(aBLPoints.Items[i],  iZone);

  dStep := 1.0 / (ARR_COUNT);

  for i := 0 to ARR_COUNT do
  begin
    t := i * dStep;
    qx := 0;
    qy := 0;

    for j := 0 to aBLPoints.Count-1 do
    begin
      q := arC[j] * IntPower(1 - t, j) * IntPower(t, aBLPoints.Count - 1 - j);
      qx := qx + q * arrPointsIn[j].x;
      qy := qy + q * arrPointsIn[j].y;
    end;

    arrXYPointsOut[i].X := round(qx);
    arrXYPointsOut[i].Y := round(qy);
  end;

  aOutBLPoints.Count:=ARR_COUNT+1;

  for i := 0 to ARR_COUNT do
    aOutBLPoints.Items[i]:= geo_Pulkovo42_XY_to_BL(arrXYPointsOut[i], iZone);
//    aOutBLPoints.Items[i]:= geo_XY_to_BL(arrXYPointsOut[i], iZone);

end;


const
  FLD_LAT1       = 'lat1';
  FLD_LON1       = 'lon1';
  FLD_LAT2       = 'lat2';
  FLD_LON2       = 'lon2';

  FLD_LAT_MIN       = 'LAT_MIN';
  FLD_LON_MIN       = 'LON_MIN';
  FLD_LAT_MAX       = 'LAT_MAX';
  FLD_LON_MAX       = 'LON_MAX';

  FLD_LAT        = 'lat';
  FLD_LON        = 'lon';


//-------------------------------------------------------------------
function db_ExtractBLRect (aDataset: TDataset): TBLRect;
//-------------------------------------------------------------------
begin
  with aDataset do
  if not IsEmpty then
  begin
    Result.TopLeft.B    :=Max(FieldByName(FLD_LAT1).AsFloat, FieldByName(FLD_LAT2).AsFloat);
    Result.TopLeft.L    :=Min(FieldByName(FLD_LON1).AsFloat, FieldByName(FLD_LON2).AsFloat);
    Result.BottomRight.B:=Min(FieldByName(FLD_LAT2).AsFloat, FieldByName(FLD_LAT1).AsFloat);
    Result.BottomRight.L:=Max(FieldByName(FLD_LON2).AsFloat, FieldByName(FLD_LON1).AsFloat);
  end else
    Result:=NULL_BLRECT();
end;



//-------------------------------------------------------------------
function db_ExtractBLRect_LatLonMinMax (aDataset: TDataset): TBLRect;
//-------------------------------------------------------------------
begin
  with aDataset do
  if not IsEmpty then begin
    Result.TopLeft.B    :=FieldByName(FLD_LAT_MAX).AsFloat;
    Result.TopLeft.L    :=FieldByName(FLD_LON_MIN).AsFloat;
    Result.BottomRight.B:=FieldByName(FLD_LAT_MIN).AsFloat;
    Result.BottomRight.L:=FieldByName(FLD_LON_MAX).AsFloat;
  end else
    Result:=NULL_BLRECT();
end;

//-------------------------------------------------------------------
function db_ExtractBLVector (aDataset: TDataset): TBLVector;
//-------------------------------------------------------------------
begin
  with aDataset do
  if not IsEmpty then
  begin
    Result.Point1.B:=FieldByName(FLD_LAT1).AsFloat;
    Result.Point1.L:=FieldByName(FLD_LON1).AsFloat;
    Result.Point2.B:=FieldByName(FLD_LAT2).AsFloat;
    Result.Point2.L:=FieldByName(FLD_LON2).AsFloat;
  end else
    Result:=NULL_VECTOR;
end;

//-------------------------------------------------------------------
function db_ExtractBLPoint (aDataset: TDataset): TBLPoint;
//-------------------------------------------------------------------
begin
  with aDataset do
  if not IsEmpty then
  begin
    Result.B:=FieldByName(FLD_LAT).AsFloat;
    Result.L:=FieldByName(FLD_LON).AsFloat;
  end else
    Result:=NULL_BLPOINT;
end;


//---------------------------------------------------------
function geo_XYRectByCircle(aBLCenter: TBLPoint; aRadius_m: double): TXYRect;
//---------------------------------------------------------
var
  xyPoint: TXYPoint;
  iZone: integer;
begin
  iZone:=geo_Get6ZoneBL (aBLCenter);
  xyPoint:=geo_Pulkovo42_BL_to_XY (aBLCenter, iZone);

  Result.TopLeft.X    :=xyPoint.X + aRadius_m;
  Result.TopLeft.Y    :=xyPoint.Y - aRadius_m;
  Result.BottomRight.X:=xyPoint.X - aRadius_m;
  Result.BottomRight.Y:=xyPoint.Y + aRadius_m;
end;


//-------------------------------------------------
procedure geo_BLPoints_to_XYPoints (aBLPoints: TBLPointArrayF; var aXYPoints: TXYPointArrayF);
//-------------------------------------------------
var i: integer;
begin
  aXYPoints.Count:=aBLPoints.Count;
  for i:=0 to aBLPoints.Count-1 do
    aXYPoints.Items[i]:=geo_Pulkovo42_BL_to_XY (aBLPoints.Items[i]);
end;



//-------------------------------------------------------------------
function geo_RoundBLPointsToXYRect(var aPoints: TBLPointArrayF; aZone: integer): TXYRect;
//-------------------------------------------------------------------
var
  i: integer;  x,y: double;
  x1,y1, x2,y2: double;
  xy: TXYPoint;
begin

  x1:=0; y1:=0; x2:=0; y2:=0;

  for i:=0 to aPoints.Count-1 do
  begin
    xy:=geo_Pulkovo42_BL_to_XY(aPoints.Items[i], aZone);
    x:=xy.X;
    y:=xy.Y;

//    b:=aPoints.Items[i].B; l:=aPoints.Items[i].L;  //if gX=0 then Continue;

    if (x > x1) or (x1=0) then x1:=x;
    if (y < y1) or (y1=0) then y1:=y;
    if (x < x2) or (x2=0) then x2:=x;
    if (y > y2) or (y2=0) then y2:=y;
  end;

  Result.TopLeft.X:=x1;
  Result.TopLeft.Y:=y1;
  Result.BottomRight.X:=x2;
  Result.BottomRight.Y:=y2;

end;



//-------------------------------------------------------------------
function geo_RoundXYPointsToXYRect(var aXYPoints: TXYPointArrayF): TXYRect;
//-------------------------------------------------------------------
var
  i: integer;  x,y: double;
  x1,y1, x2,y2: double;
  xy: TXYPoint;
begin
  x1:=0; y1:=0; x2:=0; y2:=0;

  for i:=0 to aXYPoints.Count-1 do
  begin
    xy:=aXYPoints.Items[i];
//    xy:=geo_BL_to_XY(aPoints.Items[i], aZone);
    x:=xy.X;
    y:=xy.Y;

//    b:=aPoints.Items[i].B; l:=aPoints.Items[i].L;  //if gX=0 then Continue;

    if (x > x1) or (x1=0) then x1:=x;
    if (y < y1) or (y1=0) then y1:=y;
    if (x < x2) or (x2=0) then x2:=x;
    if (y > y2) or (y2=0) then y2:=y;
  end;

  Result.TopLeft.X:=x1;
  Result.TopLeft.Y:=y1;
  Result.BottomRight.X:=x2;
  Result.BottomRight.Y:=y2;

end;



//-------------------------------------------------------------------
function geo_RoundXYPointsToXYRectNew(var aXYPoints: TXYPointArray): TXYRect;
//-------------------------------------------------------------------
var
  i: integer;  x,y: double;
  x1,y1, x2,y2: double;
  xy: TXYPoint;
begin
  x1:=0; y1:=0; x2:=0; y2:=0;

  for i:=0 to High(aXYPoints) do
  begin
    xy:=aXYPoints[i];
//    xy:=geo_BL_to_XY(aPoints.Items[i], aZone);
    x:=xy.X;
    y:=xy.Y;

//    b:=aPoints.Items[i].B; l:=aPoints.Items[i].L;  //if gX=0 then Continue;

    if (x > x1) or (x1=0) then x1:=x;
    if (y < y1) or (y1=0) then y1:=y;
    if (x < x2) or (x2=0) then x2:=x;
    if (y > y2) or (y2=0) then y2:=y;
  end;

  Result.TopLeft.X:=x1;
  Result.TopLeft.Y:=y1;
  Result.BottomRight.X:=x2;
  Result.BottomRight.Y:=y2;

end;



//----------------------------------------------------
function geo_RoundBLPointsToBLRect_F(var aPoints: TBLPointArrayF): TBLRect;
// определение обрамляющего прямоугольника по точкам
//----------------------------------------------------
var i: integer;  b,l: double;
    b1,l1, b2,l2: double;
begin
  b1:=0; l1:=0; b2:=0; l2:=0;

  for i:=0 to aPoints.Count-1 do
  begin
    b:=aPoints.Items[i].B;
    l:=aPoints.Items[i].L;  //if gX=0 then Continue;

    if (b > b1) or (b1=0) then b1:=b;
    if (l < l1) or (l1=0) then l1:=l;
    if (b < b2) or (b2=0) then b2:=b;
    if (l > l2) or (l2=0) then l2:=l;
  end;

  Result.TopLeft.B:=b1;
  Result.TopLeft.L:=l1;
  Result.BottomRight.B:=b2;
  Result.BottomRight.L:=l2;

end;



//----------------------------------------------------
function geo_RoundBLPointsToBLRect_(aPoints: TBLPointArray): TBLRect;
// определение обрамляющего прямоугольника по точкам
//----------------------------------------------------
var i: integer;  b,l: double;
    b1,l1, b2,l2: double;
begin
  b1:=0; l1:=0; b2:=0; l2:=0;

  for i:=0 to High(aPoints) do
  begin
    b:=aPoints[i].B;
    l:=aPoints[i].L;  //if gX=0 then Continue;

    if (b > b1) or (b1=0) then b1:=b;
    if (l < l1) or (l1=0) then l1:=l;
    if (b < b2) or (b2=0) then b2:=b;
    if (l > l2) or (l2=0) then l2:=l;
  end;

  Result.TopLeft.B:=b1;
  Result.TopLeft.L:=l1;
  Result.BottomRight.B:=b2;
  Result.BottomRight.L:=l2;

end;


// ---------------------------------------------------------------
procedure geo_MakeBLPointsByCircle(aCenter: TBLPoint; aRadius_m: Double; var
    aPoints: TBLPointArrayF);
// ---------------------------------------------------------------
var
  i: integer;
  b,l: double;
  blPoint: TBLPoint;
begin
  aPoints.Count:=20;

//  SetLength (Result, aPointCount);
  for i:=0 to aPoints.Count-1 do begin
    aPoints.Items[i]:=geo_RotateByAzimuth (aCenter, aRadius_m, (i/aPoints.Count)*360);
  //  Result[i]:=blPoint;
  end;
end;


// ---------------------------------------------------------------
function geo_MakeBLPointsByCircle_new(aCenter: TBLPoint; aRadius_m: Double):
    TBLPointArray;
// ---------------------------------------------------------------
var
  i: integer;
  b,l: double;
  blPoint: TBLPoint;
//  iCount: Integer;

const
  DEF_COUNT=20;

begin
//  iCount:=20;

  SetLength (Result, DEF_COUNT);

  for i:=0 to DEF_COUNT-1 do
    Result[i]:=geo_RotateByAzimuth (aCenter, aRadius_m, (i/DEF_COUNT)*360);


end;




//-------------------------------------------------------------------
function geo_RoundXYRect(aXYRect: TXYRect): TBLRect;
//-------------------------------------------------------------------
var
  i: integer;
  xyPoints: TXYPointArrayF;
  blPoints: TBLPointArrayF;
  iZone: Integer;

begin
  geo_XYRectToXYPointsF(aXYRect, xyPoints);

  iZone:=geo_Get6ZoneXY(xyPoints.Items[0]);

  blPoints.Count:=xyPoints.Count;

  for i := 0 to xyPoints.Count - 1 do
    blPoints.Items[i]:=geo_Pulkovo42_XY_to_BL(xyPoints.Items[i], iZone);

  Result := geo_RoundBLPointsToBLRect_F(blPoints);

end;



function MakeBLVector (aBLPoint1,aBLPoint2: TBLPoint): TBLVector;
begin
  Result.Point1:=aBLPoint1;
  Result.Point2:=aBLPoint2;
end;

function MakeBLVector(aB1,aL1,aB2,aL2: double): TBLVector;
begin
  Result:=MakeBLVector (MakeBLPoint(aB1,aL1),  MakeBLPoint(aB2,aL2));
end;

// ---------------------------------------------------------------
function MakeBLVector_LatLon(aValues : array of double): TBLVector;
// ---------------------------------------------------------------
begin
  Result.Point1.B := aValues[0];
  Result.Point1.L := aValues[1];

  Result.Point2.B := aValues[2];
  Result.Point2.L := aValues[3];

end;



function geo_Distance_m (aPoint1,aPoint2: TBLPoint): double;
begin
  Result:=geo_DistanceBL_m (aPoint1,aPoint2);
end;

function geo_Distance_m (aVector: TBLVector): double;
begin
  Result:=geo_DistanceBLV  (aVector);
end;

(*function  geo_Distance (aPoint1,aPoint2: TXYPoint): double;
begin
  Result:=geo_DistanceXY (aPoint1,aPoint2);
end;

function  geo_Distance (x1,y1,x2,y2: double): double;
begin
  Result:=geo_DistanceXY1 (x1,y1,x2,y2);
end;
*)

//---------------------------------------------------------
function  geo_Eq (aBLPoint1, aBLPoint2: TBLPoint): boolean;
//---------------------------------------------------------
begin
  Result:= geo_Eq_Point(aBLPoint1, aBLPoint2);
end;

//---------------------------------------------------------
function  geo_Eq (aBLVector1, aBLVector2: TBLVector): boolean;
//---------------------------------------------------------
begin
  Result:=geo_Eq_vector (aBLVector1, aBLVector2);
end;

//---------------------------------------------------------
function  geo_Eq (aBLRect1, aBLRect2: TBLRect): boolean;
//---------------------------------------------------------
begin
  Result:= geo_Eq_rect (aBLRect1, aBLRect2);
end;


//---------------------------------------------------------
function  geo_LatOrLonIsZero (aBLRect: TBLRect): boolean;
//---------------------------------------------------------
begin
  Result:= geo_LatOrLonIsZero_rect(aBLRect)
end;

//---------------------------------------------------------
function  geo_LatOrLonIsZero (aBLVector: TBLVector): boolean;
//---------------------------------------------------------
begin
  Result:=geo_LatOrLonIsZero_vector(aBLVector);
end;


//---------------------------------------------------------
function  geo_LatOrLonIsZero (aBLPoint: TBLPoint): boolean;
//---------------------------------------------------------
begin
  Result:=geo_LatOrLonIsZero_point(aBLPoint);
end;


function geo_Azimuth_Plus_Azimuth (aValue1, aValue2: double): double;
begin
  result:=aValue1 + aValue2 + 360;
  while result-360>=0 do // MOD for double
    result:=result-360;

end;



function geo_FormatDegreeWithCoordSys (aValue: double; aType: TDegreeType; aCoordSys: integer): String ;
begin
  if aType=dtLat then
    Result:=geo_FormatDegree_lat (aValue)
  else
    Result:=geo_FormatDegree_lon (aValue);

  if aCoordSys=EK_WGS84 then
    Result:=Result + '  WGS-84';
end;

// ---------------------------------------------------------------
function geo_FormatDegreeWithCoordSys (aBLPoint: TBLPoint; aDestCoordSys: integer): String ;
// ---------------------------------------------------------------
begin
  if aDestCoordSys=EK_WGS84 then
    aBLPoint:=geo_BL_to_BL_new (aBLPoint, EK_CK_42, aDestCoordSys);

//    aBLPoint:=geo_BL_to_BL (aBLPoint, EK_CK_42, aDestCoordSys);

  Result:=geo_FormatDegree_lat (aBLPoint.B) + '  |  ' +
          geo_FormatDegree_lon (aBLPoint.L);

  if aDestCoordSys=EK_WGS84 then
    Result:=Result + '  WGS-84';
end;



function geo_Azimuth_Plus  (aValue1, aValue2: double): double;
begin
  Result := geo_Azimuth_Plus_Azimuth (aValue1, aValue2);
end;

function geo_Azimuth_Minus  (aValue1, aValue2: double): double;
begin
  Result := geo_Azimuth_Plus_Azimuth (aValue1, - aValue2);

end;



function geo_GetRectCenter(aRect: TBLRect): TBLPoint;
begin
  Result.B := (aRect.TopLeft.B+ aRect.BottomRight.B) / 2;
  Result.L := (aRect.TopLeft.L+ aRect.BottomRight.L) / 2;
                                                  
end;


function  geo_GetVectorCenter (aPoint1,aPoint2: TBLPoint): TBLPoint;
begin
  Result := geo_GetVectorCenterP (aPoint1,aPoint2);

end;

function  geo_GetVectorCenter (aVector: TBLVector): TBLPoint;
begin
  Result := geo_GetVectorCenterV (aVector);

end;

(*
function geo_TestBLPoint: TBLPoint;
begin
  Result.B:=59.444;
  Result.L:=30.114;
end;
*)


procedure DegToRad2 (var aPoint: TBLPoint);
begin
  aPoint.B:=DegToRad(aPoint.B);
  aPoint.L:=DegToRad(aPoint.L);

 // with aPoint do begin b:=DegToRad(b);
 //   l:=DegToRad(l);
 // end;
end;


procedure RadToDeg2 (var aPoint: TBLPoint);
begin
  aPoint.B:=RadToDeg(aPoint.B);
  aPoint.L:=RadToDeg(aPoint.L);


{  with aPoint do begin
    b:=RadToDeg(b); l:=RadToDeg(l);
  end;}
end;



function geo_RandomBLPoint: TBLPoint;
begin
  Result.B:=geo_EncodeDegree(RandomRange(30,50), RandomRange(0,59), RandomRange(0,59));
  Result.L:=geo_EncodeDegree(RandomRange(30,100), RandomRange(0,59), RandomRange(0,59));

//  59.444;
 // Result.L:=30.114;
end;



function geo_BLPointArrayF_to_BLPointArray(var aBLPointArrayF: TBLPointArrayF):
    TBLPointArray;
var
  I: Integer;
begin
  SetLength(result, aBLPointArrayF.Count);
  for I := 0 to aBLPointArrayF.Count-1 do
     result[i]:=aBLPointArrayF.Items[i];
end;

(*
procedure geo_BLPointArrayF_to_BLPointArray  (var aBLPointArrayF: TBLPointArrayF;
                                              var aBLPointArray: TBLPointArray);
var
  I: Integer;
begin
  SetLength(aBLPointArray, aBLPointArrayF.Count);
  for I := 0 to High(aBLPointArray) do
     aBLPointArray[i]:=aBLPointArrayF.Items[i];
end;

*)

procedure geo_XYPointArrayF_to_XYPointArray  (var aXYPointArrayF: TXYPointArrayF;
                                              var aXYPointArray: TXYPointArray);
var
  I: Integer;
begin
  SetLength(aXYPointArray, aXYPointArrayF.Count);
  for I := 0 to High(aXYPointArray) do
     aXYPointArray[i]:=aXYPointArrayF.Items[i];
end;



function geo_BLPointArrayToStr(aBLPointArray: TBLPointArrayF): string;
var
  I: integer;
begin
  Result := '';

//  for I := 0 to High(aBLPointArray) do
 //   Result:=Result+ Format('%1.7f  %1.7f',[aBLPointArray[i].B, aBLPointArray[i].L]) + CRLF;

  for I := 0 to aBLPointArray.Count-1 do
    Result:=Result+ Format('%1.7f  %1.7f',[aBLPointArray.Items[i].B, aBLPointArray.Items[i].L]) + CRLF;

end;

function geo_Distance_KM(aPoint1,aPoint2: TBLPoint): double;
begin
  Result := geo_Distance_m(aPoint1,aPoint2)  /1000;
end;

function geo_Distance_KM(aVector: TBLVector): double;
begin
  Result := geo_Distance_KM(aVector.Point1, aVector.Point2);
end;



// ---------------------------------------------------------------
function geo_GetCoordSysStr(aValue: Integer): string;
// ---------------------------------------------------------------
begin
  case aValue of
    EK_CK_42   : Result:=DEF_GEO_STR_CK42;
    EK_CK_95   : Result:=DEF_GEO_STR_CK95;
    EK_GCK_2011: Result:=DEF_GEO_STR_GCK_2011; //'ГCK 2011';
    EK_WGS_84  : Result:=DEF_GEO_STR_WGS84;
  else
    Result :='';
  end;

{
 DEF_GEO_STR_CK42 = 'CK-42';// (Пулково)';
  DEF_GEO_STR_CK95 = 'CK-95';// (Пулково)';
  DEF_GEO_STR_WGS84= 'WGS-84';
  DEF_GEO_STR_GCK_2011= 'ГСК-2011';



  DEF_GEO_STR_WGS84= 'WGS-84';
  DEF_GEO_STR_GCK_2011= 'ГСК-2011';
}


end;


// ---------------------------------------------------------------
procedure Test;
// ---------------------------------------------------------------
var
  e: Double;
   r: TAngle;

begin
  e:=geo_EncodeDegree(30,0,0);
  r:=geo_DecodeDegree (e);
  assert(r.min=0);
  assert(r.sec=0);

  e:=geo_EncodeDegree(60,0,0);
  r:=geo_DecodeDegree (e);
  assert(r.min=0);
  assert(r.sec=0);


  e:=geo_EncodeDegree(20,0,0);
  r:=geo_DecodeDegree (e);
  assert(r.min=0);
  assert(r.sec=0);


  e:=geo_EncodeDegree(55,17,0);
  r:=geo_DecodeDegree (e);
  assert(r.min=17);
  assert(r.sec=0);



  e:=geo_EncodeDegree(64,31,10);
  r:=geo_DecodeDegree (e);



  e:=geo_EncodeDegree(64,32,0);
  r:=geo_DecodeDegree (e);

end;


function TXYBounds_MinMax.ToString: string;
begin
  Result:= 
    Format('Min_X: %1.1f',[Min_X]) + LF +    
    Format('Max_X: %1.1f',[Max_X]) + LF +    
    Format('Min_Y: %1.1f',[Min_Y]) + LF +    
    Format('Max_Y: %1.1f',[Max_Y]) + LF;
    


end;


function TBLPoint.ToString: String;
begin
  Result := Format('lat: %1.6f, lon: %1.6f,  ', [b,l]);
end;


// ---------------------------------------------------------------
function geo_BLRect_AsString(aBLRect: TBLRect): string;
// ---------------------------------------------------------------
begin
  with aBLRect do
    Result := Format('TopLeft.B:%1.3f, '+
                   'TopLeft.L:%1.3f  '+

                   'BottomRight.B:%1.3f  '+
                   'BottomRight.L:%1.3f  '
                   ,
                    [TopLeft.B, TopLeft.L,
                     BottomRight.B, BottomRight.L] );


end;

function TBLRect.ToString: String;
begin

  Result := Format('TopLeft.lat: %1.6f, TopLeft.lon: %1.6f,  ', [TopLeft.b,TopLeft.l])  +
            Format('BottomRight.lat: %1.6f, BottomRight.lon: %1.6f,  ', [BottomRight.b,BottomRight.l])

end;




begin


end.




{
//------------------------------------------------------
function geo_XYRectToBLPoints(aXYRect: TXYRect; aZone6: Integer): TBLPointArray;
//------------------------------------------------------
var
  I: Integer;
  xyArr: TXYPointArray;
begin

 // aPoints.Count:=4;
  SetLength (Result, 4);
  SetLength (xyArr, 4);


  with aXYRect do
  begin
    xyArr[0]:=TopLeft;
    xyArr[1]:=MakeXYPoint (TopLeft.X, BottomRight.Y);
    xyArr[2]:=BottomRight;
    xyArr[3]:=MakeXYPoint (BottomRight.X, TopLeft.Y);
  end;

  geo_xy

  for I := 0 to High(xyArr) do
    Result[i]:= geo_ ;

end;

  }

