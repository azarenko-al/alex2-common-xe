unit u_geo_convert_new;

interface

uses Math, SysUtils, IniFiles, Dialogs,
 
  u_neva_dm_bl,
  u_GEO;




  // ---------------------------------------------------------------
  //GK_XY
  // ---------------------------------------------------------------

  function geo_Pulkovo42_to_WGS84(aBLPoint: TBLPoint): TBLPoint;
  function geo_WGS84_to_Pulkovo42(aBLPoint: TBLPoint): TBLPoint;
  
//  function geo_Pulkovo42_to_XY(aBLPoint: TBLPoint; aGK_Zone: word = 0): TXYPoint;


  // ---------------------------------------------------------------
  //GK_XY
  // ---------------------------------------------------------------
//  function geo_GK_XY_to_Pulkovo42_BL(aXYPoint: TXYPoint; aGK_Zone: Integer): TBLPoint;
  function geo_Pulkovo42_XY_to_BL(aXYPoint: TXYPoint; aGK_Zone: Integer): TBLPoint;
  function geo_Pulkovo42_BL_to_XY(aBLPoint: TBLPoint; aGK_Zone: word = 0):  TXYPoint;

  function geo_Pulkovo42_XY_to_WGS84_BL(aXYPoint: TXYPoint; aGK_Zone: Integer):   TBLPoint;

  //GK,Pulkovo42 -> UTM,WGS84
  function geo_GK_XY_to_UTM(aXYPoint: TXYPoint; aGK_Zone: Integer): TXYPoint;


  // ---------------------------------------------------------------
  //WGS84
  // ---------------------------------------------------------------
//  function geo_WGS84_to_Pulkovo42(aBLPoint: TBLPoint): TBLPoint;

  // ---------------------------------------------------------------
  //UTM_
  // ---------------------------------------------------------------
  //UTM,WGS84 -> GK,Pulkovo42
  function geo_UTM_to_GK(aXYPoint: TXYPoint; aUTM_Zone: Integer): TXYPoint;
  // UTM -> LAT,LON
  function geo_UTM_to_WGS84(aXYPoint: TXYPoint; aUTM_Zone: Integer): TBLPoint;
  function geo_WGS84_to_UTM(aBLPoint: TBLPoint; aUTM_Zone: Integer): TXYPoint;

function geo_BL_to_BL_new(aBLPoint: TBLPoint; aDatumSrc, aDatumDest: byte): TBLPoint;

function geo_Pulkovo42_to_CK_95(aBLPoint: TBLPoint): TBLPoint;

function geo_Pulkovo_CK_95_to_CK_42(aBLPoint: TBLPoint): TBLPoint;

function geo_XYRect_UTM_to_GK(aXYBounds: TXYRect; aUTM_zone: Integer): TXYRect;

procedure geo_GK_to_UTM_to_File(aXYBounds: TXYRect; aGK_zone: Integer;   aFileName: string);


(*

exports
  geo_UTM_to_WGS84;
*)

implementation

function geo_UTM_GetCenterMeridian_Rad(aUTM_Zone: double): double; forward;

function geo_GK_XY_to_Pulkovo42_BL(aXYPoint: TXYPoint; aGK_Zone: Integer): TBLPoint; forward;
function geo_Pulkovo42_to_XY(aBLPoint: TBLPoint; aGK_Zone: word = 0): TXYPoint;  forward;

function geo_CK_95(aBLPoint: TBLPoint; aFrom: byte = EK_CK_42; aTo: byte =
    EK_CK_95): TBLPoint; forward;



// ---------------------------------------------------------------
function geo_UTM_to_WGS84(aXYPoint: TXYPoint; aUTM_Zone: Integer): TBLPoint;
// ---------------------------------------------------------------

//  rXYRect.TopLeft.X:=6180493;
//  rXYRect.TopLeft.Y:=405390;

var
  b,l,lc,lc_deg: Double;
begin
  lc_deg:=(aUTM_Zone-30)*6 - 3;
  lc:=DegToRad(lc_deg);

  aXYPoint.y:=aXYPoint.y-500000;

  XY_to_BL(aXYPoint.x, aXYPoint.y, lc,0,0, 9,2, b,l); //9 WGS-1984

  Result.B :=RadToDeg(b);
  Result.L :=RadToDeg(l);
 //
 // aXYPoint.y:=aXYPoint.y-500000;

end;

// ---------------------------------------------------------------
function geo_Pulkovo42_to_XY(aBLPoint: TBLPoint; aGK_Zone: word = 0):    TXYPoint;
// ---------------------------------------------------------------
var
  b,l: Double;
  x,y: Double;
  lc: Double;
begin
  Assert(aGK_Zone<100, 'aGK_Zone<100, GK_Zone:'+ IntToStr(aGK_Zone));


  if aGK_Zone=-100 then
  begin
    aGK_Zone := geo_Get6ZoneL(aBLPoint.L);
  end;


  lc:=aGK_Zone*6 - 3;
  lc:=DegToRad(lc);

  b :=DegToRad(aBLPoint.B);
  l :=DegToRad(aBLPoint.L);

  BL_to_XY(B, L, lc,0,0, 1,1, x,y); //1 Pulkovo42

  Y:= aGK_Zone*1000*1000 + Y + 500000;

  Result.x:=x;
  Result.y:=y;
end;


function geo_GK_GetCenterMeridian_Rad(aGK_Zone: double): double;
begin
 ////// Assert(aGK_Zone>0, 'Value <=0');

  Result:=DegToRad(aGK_Zone*6 - 3);
end;

function geo_UTM_GetCenterMeridian_Rad(aUTM_Zone: double): double;
begin
 /////// Assert((aUTM_Zone-30)>0, 'Value <=0');

  Result:=(aUTM_Zone-30)*6 - 3;
  Result:=DegToRad(Result);
end;


// ---------------------------------------------------------------
function geo_Pulkovo42_XY_to_BL(aXYPoint: TXYPoint; aGK_Zone: Integer): TBLPoint;
// ---------------------------------------------------------------
begin
  result:= geo_GK_XY_to_Pulkovo42_BL(aXYPoint, aGK_Zone);

end;

// ---------------------------------------------------------------
function geo_GK_XY_to_Pulkovo42_BL(aXYPoint: TXYPoint; aGK_Zone: Integer): TBLPoint;
// ---------------------------------------------------------------
var
  b,l: Double;
  d: Double;
  x,y: Double;
  lc: Double;
  iOldZone: integer;
begin
  iOldZone:=Trunc(aXYPoint.Y) div (1000*1000);

 // iOldZone :=aGK_Zone;
//  aGK_Zone:=iOldZone;

///////////  Assert(iOldZone>0, 'Value <=0');

  if aGK_Zone>0 then d:=geo_GetCenterMeridianBL (aGK_Zone)
                else d:=geo_GetCenterMeridianBL (iOldZone);

  lc:=DegToRad (d);
//  lc := geo_GK_GetCenterMeridian_Rad(iOldZone);

  x:=aXYPoint.x;
//  y:=Frac (aXYPoint.Y/1000000)*1000000 - 500000;  //frac-������� �����
  y:=Frac (aXYPoint.Y/1000000)*1000000 - 500000;  //frac-������� �����

  if (aGK_Zone>0) then
    Y:=Y + (iOldZone-aGK_Zone)*(1000*1000);


  XY_to_BL(x, y, lc,0,0, 1,1, b,l); //1 GK

  Result.b :=RadToDeg(B);
  Result.l :=RadToDeg(L);

end;


function geo_BL_to_BL_new(aBLPoint: TBLPoint; aDatumSrc, aDatumDest: byte):
    TBLPoint;
begin
  GEO_GEO_deg (aBLPoint.B, aBLPoint.L,  aDatumSrc, aDatumDest, Result.B, Result.L);;
end;


function geo_WGS84_to_Pulkovo42(aBLPoint: TBLPoint): TBLPoint;
begin
  GEO_GEO_deg (aBLPoint.B, aBLPoint.L,  DEF_DATUM_WGS_84, DEF_DATUM_PULKOVO_42, Result.B, Result.L);;
end;


function geo_Pulkovo42_to_WGS84(aBLPoint: TBLPoint): TBLPoint;
begin
  GEO_GEO_deg (aBLPoint.B, aBLPoint.L,  DEF_DATUM_PULKOVO_42, DEF_DATUM_WGS_84,  Result.B, Result.L);;
end;


// ---------------------------------------------------------------
function geo_UTM_to_GK(aXYPoint: TXYPoint; aUTM_Zone: Integer): TXYPoint;
// ---------------------------------------------------------------
var bl_WGS84, bl_pulkovo : TBLPoint;
begin
  bl_WGS84 := geo_UTM_to_WGS84(aXYPoint, aUTM_Zone);

  GEO_GEO_deg(bl_WGS84.B, bl_WGS84.L,
     DEF_DATUM_WGS_84, DEF_DATUM_PULKOVO_42, bl_pulkovo.B, bl_pulkovo.L);

  Result :=geo_Pulkovo42_to_XY(bl_pulkovo, aUTM_Zone-30);
end;


function geo_RadToDeg(aBLPoint: TBLPoint): TBLPoint;
begin
  Result.b :=RadToDeg(aBLPoint.B);
  Result.l :=RadToDeg(aBLPoint.L);
end;


function geo_DegToRad(aBLPoint: TBLPoint): TBLPoint;
begin
  Result.b :=DegToRad(aBLPoint.B);
  Result.l :=DegToRad(aBLPoint.L);
end;


// ---------------------------------------------------------------
function geo_WGS84_to_UTM(aBLPoint: TBLPoint; aUTM_Zone: Integer): TXYPoint;
// ---------------------------------------------------------------
var
  bl_WGS84, bl_pulkovo : TBLPoint;
  lc: Double;
  x,y,b: Double;
  l: Double;
begin
   Assert (aUTM_Zone>0);


// iOldZone:=Trunc(aXYPoint.Y) div (1000*1000);

  lc := geo_UTM_GetCenterMeridian_Rad(aUTM_Zone);

  b :=DegToRad(aBLPoint.B);
  l :=DegToRad(aBLPoint.L);

  bl_to_xy(b, l, lc,0,0, 9,2, x,y); //1 GK

  Result.X :=x;
  Result.Y :=y + 500000;
end;


// ---------------------------------------------------------------
function geo_GK_XY_to_UTM(aXYPoint: TXYPoint; aGK_Zone: Integer): TXYPoint;
// ---------------------------------------------------------------
var
  bl_WGS84, bl_pulkovo : TBLPoint;
begin
  Assert(aGK_Zone<1000);


  bl_Pulkovo := geo_GK_XY_to_Pulkovo42_BL(aXYPoint, aGK_Zone);

  GEO_GEO_deg(bl_Pulkovo.B, bl_Pulkovo.L,
      DEF_DATUM_PULKOVO_42, DEF_DATUM_WGS_84,  bl_WGS84.B, bl_WGS84.L);

  Result :=geo_WGS84_to_UTM(bl_WGS84, aGK_Zone+30);

end;


//-------------------------------------------------------------
function geo_Pulkovo42_to_CK_95(aBLPoint: TBLPoint): TBLPoint;
//-------------------------------------------------------------
begin
  Result := geo_CK_95(aBLPoint, EK_CK_42, EK_CK_95);
end;

//-------------------------------------------------------------
function geo_Pulkovo_CK_95_to_CK_42(aBLPoint: TBLPoint): TBLPoint;
//-------------------------------------------------------------
begin
  Result := geo_CK_95(aBLPoint, EK_CK_95, EK_CK_42);
end;



//-------------------------------------------------------------
function geo_CK_95(aBLPoint: TBLPoint; aFrom: byte = EK_CK_42; aTo: byte =
    EK_CK_95): TBLPoint;
//-------------------------------------------------------------
var
  blPoint: TBLPoint;
  pDatum95, pDatum42 : PDatum7;
  tDatum95, tDatum42 : TDatum7;
  dH : double; //������ - �� ������������....
begin
  tDatum42.dX:= 23.9;
  tDatum42.dY:= -141.3;
  tDatum42.dZ:= -80.9;
  tDatum42.wX:= 0;
  tDatum42.wY:= -0.371277 / 60 / 60 / 180 * Pi;
  tDatum42.wZ:= -0.849811 / 60 / 60 / 180 * Pi;
  tDatum42.m := -0.12*Power(10, -6);

  tDatum95.dX:= 24.8;
  tDatum95.dY:= -131.24;
  tDatum95.dZ:= -82.66;
  tDatum95.wX:= 0;
  tDatum95.wY:= 0;
  tDatum95.wZ:= -0.169137/ 60 / 60 / 180 * Pi;
  tDatum95.m := -0.12*Power(10, -6);

  pDatum42:= @tDatum42;
  pDatum95:= @tDatum95;

  blPoint:= aBLPoint;
  DegToRad2(blPoint);
  //--------------------------
  if (aFrom = EK_CK_42) and (aTo = EK_CK_95) then
  begin
    GEO_GEO(blPoint.B, blPoint.L, 0, 1, 1, pDatum42, pDatum95, Result.B, Result.L, dH);

    RadToDeg2(Result);
  end else

  //--------------------------
  if (aFrom = EK_CK_95) and (aTo = EK_CK_42) then
  begin
    GEO_GEO(blPoint.B, blPoint.L, 0, 1, 1, pDatum95, pDatum42, Result.B, Result.L, dH);

    RadToDeg2(Result);
  end
end;




function geo_Pulkovo42_XY_to_WGS84_BL(aXYPoint: TXYPoint; aGK_Zone: Integer):
    TBLPoint;
var
  BLPoint: TBLPoint;
begin
  BLPoint := geo_GK_XY_to_Pulkovo42_BL(aXYPoint, aGK_Zone);

//   geo_Pulkovo42_XY_to_WGS84_BL(aXYPoint: TXYPoint; aGK_Zone: Integer): TBLPoint;

  Result := geo_Pulkovo42_to_WGS84(BLPoint);

end;      


// ---------------------------------------------------------------
function geo_XYRect_UTM_to_GK(aXYBounds: TXYRect; aUTM_zone: Integer): TXYRect;
// ---------------------------------------------------------------
var

  arrXYRectArray_UTM: TXYRectArray;
  arrXYPointArrayF_new: TXYPointArrayF;
  arrXYPointArrayF_GK: TXYPointArrayF;
  i: Integer;

  rXYRect_UTM: TXYRect;
 // rXYRect_GK: TXYRect;

  xy_CK1, xy_CK2,
  xy_UTM1, xy_UTM2, xy, xy_gk,xy_UTM: TXYPoint;


begin
  Assert( aXYBounds.TopLeft.X<>0, 'Value <=0');


  SetLength(arrXYRectArray_UTM, 1);

  arrXYRectArray_UTM[0] := aXYBounds;

  rXYRect_UTM:=geo_GetRoundXYRect_(arrXYRectArray_UTM);


  geo_XYRectToXYPointsF(rXYRect_UTM, arrXYPointArrayF_new);

  arrXYPointArrayF_GK.Count :=4;


  for i:=0 to arrXYPointArrayF_new.Count-1 do
  begin
    xy :=arrXYPointArrayF_new.Items[i];

    xy_gk := geo_UTM_to_GK(xy, aUTM_zone);

(*
    if Projection=ptUTM then
      xy_gk := geo_UTM_to_GK(xy, UTM_zone)
    else
      xy_gk := xy;
*)

    arrXYPointArrayF_GK.Items[i] := xy_gk;
  end;

  Result :=geo_RoundXYPointsToXYRect(arrXYPointArrayF_GK);

 // Result := rXYRect_GK;

  // TODO -cMM: TCustomTask.UTM_to_GK default body inserted
end;


// ---------------------------------------------------------------
procedure geo_GK_to_UTM_to_File(aXYBounds: TXYRect; aGK_zone: Integer;
    aFileName: string);
// ---------------------------------------------------------------
const
  DEF_UTM = 'utm';
  DEF_UTM_X = 'utm_x';
  DEF_UTM_Y = 'utm_y';
  DEF_GK  = 'GK';

  DEF_UTM_BOUNDS  = 'UTM_BOUNDS';


var
  arrXYPointArrayF: TXYPointArrayF;
  arr: TXYPointArray;
  i: Integer;
  xy_UTM, xy_gk: TXYPoint;

  ini: TIniFile;

  rec_min_max: TXYBounds_MinMax;

begin
  Assert( aXYBounds.TopLeft.X<>0, 'Value <=0');


  SetLength(arr, 4);


  geo_XYRectToXYPointsF(aXYBounds, arrXYPointArrayF);


  for i:=0 to arrXYPointArrayF.Count-1 do
  begin
    xy_gk :=arrXYPointArrayF.Items[i];

    xy_UTM := geo_GK_XY_to_UTM(xy_gk, aGK_zone);

    arr[i] := xy_UTM;
  end;



  ini:=TIniFile.Create ( aFileName) ;//  'd:\1.ini');


  ini.WriteFloat(DEF_GK, 'TopLeft.X' , aXYBounds.TopLeft.X);
  ini.WriteFloat(DEF_GK, 'TopLeft.Y' , aXYBounds.TopLeft.Y);
  ini.WriteFloat(DEF_GK, 'BottomRight.X' , aXYBounds.BottomRight.X);
  ini.WriteFloat(DEF_GK, 'BottomRight.Y' , aXYBounds.BottomRight.Y);


{  rec_min_max.Min_X:=arr[0].X;
  rec_min_max.Max_Y:=arr[0].X;

  rec_min_max.Min_Y:=arr[0].Y;
  rec_min_max.Max_Y:=arr[0].Y;

 }

  rec_min_max.Max_X:=Round(arr[0].X);
  rec_min_max.Min_X:=Round(arr[2].X);

  rec_min_max.Min_Y:=Round(arr[0].Y);
  rec_min_max.Max_Y:=Round(arr[2].Y);


  ini.WriteInteger(DEF_UTM_BOUNDS,   'zone' ,  aGK_zone + 30);

  ini.WriteFloat(DEF_UTM_BOUNDS, 'min_X' , rec_min_max.Min_X);
  ini.WriteFloat(DEF_UTM_BOUNDS, 'max_X' , rec_min_max.Max_X);

  ini.WriteFloat(DEF_UTM_BOUNDS, 'min_Y' , rec_min_max.Min_Y);
  ini.WriteFloat(DEF_UTM_BOUNDS, 'max_Y' , rec_min_max.Max_Y);




  for i:=0 to arrXYPointArrayF.Count-1 do
  begin
//    Result[i] := xy_UTM;


    ini.WriteFloat(DEF_UTM, 'X_'+ IntToStr(i) , arr[i].X);
    ini.WriteFloat(DEF_UTM, 'Y_'+ IntToStr(i) , arr[i].Y);
  end;


  for i:=0 to arrXYPointArrayF.Count-1 do
  begin
//    Result[i] := xy_UTM;


    ini.WriteFloat(DEF_UTM_X, 'X_'+ IntToStr(i) , arr[i].X);
    ini.WriteFloat(DEF_UTM_Y, 'Y_'+ IntToStr(i) , arr[i].Y);
  end;



  FreeAndNIl (ini);


end;


function geo_Pulkovo42_BL_to_XY(aBLPoint: TBLPoint; aGK_Zone: word = 0):  TXYPoint;
begin
  Result := geo_Pulkovo42_to_XY (aBLPoint, aGK_Zone);
end;


//
//procedure Test;
//var
//  bl_wgs, bl_pulkovo: TBLPoint;
//begin
////  bl_wgs.B:=43.113238;
////  bl_wgs.L:=131.888876;
//
//  bl_wgs.B:=55.983285;
//  bl_wgs.L:=37.793224;
//                      
//  bl_pulkovo:=geo_WGS84_to_Pulkovo42(bl_wgs);
//
// 
//  
//end;


begin
 ////////// test;


 // ShowMessage ( Format ('lon %1.7f  lat %1.7f', [bl_pulkovo.L, bl_pulkovo.B]) );

end.

{
		X		Y
WGS		131.888876		43.113238


�������		131.888576		43.112957



function geo_WGS84_to_Pulkovo42(aBLPoint: TBLPoint): TBLPoint;
begin
  GEO_GEO_deg (aBLPoint.B, aBLPoint.L,  DEF_DATUM_WGS_84, DEF_DATUM_PULKOVO_42, Result.B, Result.L);;
end;
}



end.


//var
//  xy_UTM, xy_gk, xy,xy1: TXYPoint;
//  bl1,bl_pulkovo, bl_WGS84 : TBLPoint;
//
//begin
//
//(*
//  xy.X :=6201711;
//  xy.Y :=7386111;
//
//
//  /////////////////////////////////////
// bl1 := geo_GK_XY_to_Pulkovo42_BL(xy, 7);
// xy1 := geo_Pulkovo42_to_XY(bl1, 7);
//  /////////////////////////////////////
//
//*)
//
//  xy.X :=6180493;
//  xy.Y :=405390;
//
//
//  bl_WGS84:=geo_UTM_to_WGS84(xy,37);
//  xy1:=geo_WGS84_to_UTM(bl_WGS84,37);
//
//(*
//  GEO_GEO_deg(bl_WGS84.B, bl_WGS84.L,
//     DEF_DATUM_WGS_84, DEF_DATUM_PULKOVO_42, bl_pulkovo.B, bl_pulkovo.L);
//
//
//  GEO_GEO_deg(bl_pulkovo.B, bl_pulkovo.L,
//     DEF_DATUM_PULKOVO_42, DEF_DATUM_WGS_84,  bl1.B, bl1.L);
//
//*)
//
//  xy_gk := geo_UTM_to_GK (xy, 37);
//
//  // xy :=arrXYPointArrayF_new.Items[i];
//
// // xy_gk := geo_UTM_to_GK(xy, 37);
//
//   //test
//  xy_UTM := geo_GK_XY_to_UTM(xy_gk, 7);
//
//
//
//  xy.Y :=405390.5;
//  xy.X :=6180492.5;
//
//
end.




{
// ---------------------------------------------------------------
function geo_WGS84_to_UTM(aBLPoint: TBLPoint; aUTM_Zone: Integer): TXYPoint;
// ---------------------------------------------------------------
var
  bl_WGS84, bl_pulkovo : TBLPoint;
  lc: Double;
  x,y,b: Double;
  l: Double;
begin
// iOldZone:=Trunc(aXYPoint.Y) div (1000*1000);

  lc := geo_UTM_GetCenterMeridian_Rad(aUTM_Zone);

  b :=DegToRad(aBLPoint.B);
  l :=DegToRad(aBLPoint.L);

  bl_to_xy(b, l, lc,0,0, 9,2, x,y); //1 GK

  Result.X :=x;
  Result.Y :=y + 500000;
end;
 }

 

 // function geo_WGS84_to_Pulkovo42(aBLPoint: TBLPoint): TBLPoint;
 // function geo_Pulkovo42_to_WGS84(aBLPoint: TBLPoint): TBLPoint;

//  function geo_Pulkovo_BL_to_BL_95(aBLPoint: TBLPoint; aFrom: byte = EK_CK_42;
//      aTo: byte = EK_CK_95): TBLPoint;


//  EK_CK_42, EK_CK_95)

