unit u_neva_dm_bl;

interface

  type
    PDatum7 = ^TDatum7;
    TDatum7 = record
      dX,dY,dZ, wX,wY,wZ, m: Double;
    end;

  //  a//a  tDatum = record dX,dY,dZ,wx,wy,wz,m: Double end;


  procedure GEO_GEO(b1,l1,h1: double; elp1,elp2: Integer;
                  geo1,geo2: PDatum7; out b2,l2,h2: double); stdcall;

  procedure GEO_GEO_deg(aLat, aLon: double; elp1, elp2: Integer; var aLat_out, aLon_out:
      double);


  procedure XY_to_BL(x,y, lc,b1,b2: double; elp,prj: byte; var b,l: double); stdcall;
  procedure BL_to_XY(b,l, lc,b1,b2: double; elp,prj: byte; var x,y: double); stdcall;

  function  Geoid_Dist (b1,l1, b2,l2: double; var fi: double): double;




const
  DEF_DATUM_PULKOVO_42 = 1;
  DEF_DATUM_WGS_84     = 9;
  DEF_DATUM_PULKOVO_95 = 10;

(*type
  TDatum = record dX,dY,dZ, wx,wy,wz, m: Double; end;
*)
var
  Datums: array[1..10] of TDatum7 =
  ( //(dX:27; dY:-135; dZ:-89, wX:0; wY:0; wZ:0; m:0), // Tolik Krasovsky-1942
   // (dX:25; dY:-141; dZ:-79; wX:0; wY:0.35; wZ:0.866; m:0), // ������� ��������� 1942 �.
  (dX:23.9; dY:-141.3; dZ:-80.9; wX:0; wY:-0.0000018; wZ:-0.00000412; m:-0.00000012), //1 PULKOVO 1942
  (dX:0;    dY:0;      dZ:0;     wX:0; wY:0;          wZ:0;           m:0),           //2 WGS-1976
  (dX:0;    dY:0;      dZ:0;     wX:0; wY:0;          wZ:0;           m:0),           //3 ��������-1909
  (dX:0;    dY:0;      dZ:0;     wX:0; wY:0;          wZ:0;           m:0),           //4 ������-1880
  (dX:0;    dY:0;      dZ:0;     wX:0; wY:0;          wZ:0;           m:0),           //5 ������-1866
  (dX:0;    dY:0;      dZ:0;     wX:0; wY:0;          wZ:0;           m:0),           //6 �������-1857
  (dX:0;    dY:0;      dZ:0;     wX:0; wY:0;          wZ:0;           m:0),           //7 �������-1841
  (dX:0;    dY:0;      dZ:0;     wX:0; wY:0;          wZ:0;           m:0),           //8 ���-1830
  (dX:-1.1; dY:-0.3;   dZ:-0.9;  wX:0; wY:0;          wZ:-0.00000082; m:-0.00000012), //9 WGS-1984
  (dX:24.8; dY:-131.24;dZ:-82.66;wX:0; wY:0;          wZ:-0.00000082; m:-0.00000012)  //10 ��-95
   );



//[KA] "PULKOVO-42(photomod)" 23.9 -141.3 -80.9 0 -0.371277 -0.849811 -0.12
//[KA] "PULKOVO-95(photomod)" 24.8 -131.24 -82.66 0  0  -0.169137 -0.12
// dX, dY, dZ, wX, wY, wZ, m

// ������� �������� ����������������� ����� / 60 / 60 / 180 * Pi
// �����. m *Power(10, -6)

// b, l - � ��������

implementation

uses Math;

const
 dll_path = 'DLL_BL.dll';

  function  Geoid_Dist (b1,l1, b2,l2: double; var fi: double): double;external dll_path;


  procedure GEO_GEO(b1,l1,h1: double; elp1,elp2: Integer;
                  geo1,geo2: PDatum7; out b2,l2,h2: double); external dll_path;

  procedure XY_to_BL(x,y, lc,b1,b2: double; elp,prj: byte;
                                           var b,l: double); external dll_path;
  procedure BL_to_XY(b,l, lc,b1,b2: double; elp,prj: byte;
                                           var x,y: double); external dll_path;



procedure GEO_GEO_deg(aLat, aLon: double; elp1, elp2: Integer; var aLat_out, aLon_out:
    double);
var
 // aLat_out,aLon_out,
  h2: Double;
  geo1,geo2: PDatum7;

  el1, el2: Integer;

  PDatum_from, PDatum_to: PDatum7;
begin
  aLat := DegToRad(aLat);
  aLon := DegToRad(aLon);

//  if elp1=DEF_DATUM_PULKOVO_95 then
 //   elp1 :=DEF_DATUM_PULKOVO_42;
//  if  then
  PDatum_from := @Datums[elp1];
  PDatum_to :=@Datums[elp2];


  GEO_GEO(aLat,aLon,0,elp1,elp2, PDatum_from, PDatum_to, aLat_out,aLon_out,h2);

  aLat_out := RadToDeg(aLat_out);
  aLon_out := RadToDeg(aLon_out);


end;



begin

end.

