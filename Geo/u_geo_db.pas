unit u_geo_db;

interface

uses
  CodeSiteLogging,  

  System.Generics.Collections,
  
  System.Classes,  dialogs,  SysUtils, StrUtils,
//  StrUtils,

  Math,  
                                   
  System.RegularExpressions,

  u_func
  
  
//  
//
//  mappoina,    mapmet,  mapfind,  maptype, mapobj, 
//   
//  u_log,

//  u_func,
  
//  u_geo
  ;
          
type

  //LINESTRING (30.303877300000003 59.955222600000006, 
//              30.304077300000003 59.9551773, 
//              30.3041665 59.955157400000004, 30.304678900000003 59.955042600000006, 30.3047733 59.9550215, 30.3049372 59.954991500000006, 30.3048125 59.9548672, 30.303758300000002 59.955096100000006, 
//              30.303877300000003 59.955222600000006)
                                                        
  TSTPoint  = record B,L: Double; end;      
 // TBLPoint_  = record B,L: Double; end;      
//  TGeoPoint  = record B,L: Double; X,Y: Double;  end;      
   
  TSTPointArray= array of TSTPoint; // TBLPoint_;
  TSTMultiPoly = array of TSTPointArray;

 // TBLPoly = TBLPointArray_;
  
//  TBLPoly      = array of TBLPointArray_; // ������� ����� ���� � �������
  
  TSTPointObj  = class B,L: Double; end;      

    
  TSTPointList = TObjectList<TSTPointObj>;
    
  TSTPartList = TObjectList<TSTPointList>;

    // geom.Parce_SQL('MULTIPOLYGON(((36.961393 55.653477,36.961392 55.653088,36.961215), 55.653096,36.960817 55.653117,36.960818 55.653471,36.961393 55.653477))');

// geom.Parce_SQL('POLYGON((36.961393 55.653477,36.961392 55.653088,36.961215 55.653096,36.960817 55.653117,36.960818 55.653471,36.961393 55.653477))');

    
  TSTGeometry = record
  private
//    function BLPointArrayAsString(aArr: TBLPointArray_; aBackDirection: boolean =
//        false): string;
////    procedure Corrrect_direction_ByClock1;
//    function IsDirectionByClock (aArr: TBLPointArray_): Boolean;
//    
//    procedure SetPartCount(aCount : integer);
//    procedure SetPointCount(aPartIndex,aPointCount : integer);
 //   function MakeCopy: TBLGeometry;
  public
   // Dictionary: TDictionary<String, TCity>;
  
    Type_: (gtNone,gtLINESTRING,gtPOLYGON_,gtMULTIPOLYGON,gtPOINT);  //gtVector,
  
    Point: TSTPoint;
    Points: TSTPointArray;
  
//    MultiPoly: TBLMultiPoly;
    Poly: TSTMultiPoly;    
    
//    Line: TBLPointArray_;    
//  
//  //  Metric: TBLPoly; //!!!!!!!!!
//  
// //   Items: TBLMultiPoly; // array of TBLPointArray_;
//    POINT: TGeoPoint; 

  public
    procedure Parce_SQL(aValue: string);
  
//    function ToString: string;
//
//  
//    function PolyAsString(aBackDirection: boolean = false): string;
//    procedure Clear;
//
//    function To_POLYGON_SQL(aBackDirection: boolean = false): string;
//
//    procedure From_Vector(aLat1, aLon1, aLat2, aLon2: Double);
//    procedure From_Point(aLat, aLon: Double);
////    procedure LoadFromMapObj (aMapObj: TMapObj);
//
////    function From_Point(aLat, aLon: Double): Integer;
//
////    function From_Vector(aLat1, aLon1, aLat2, aLon2: Double) ; 
//
//    function To_LINESTRING_SQL: string;
//
//    procedure Parce_SQL(aValue: string);
//    procedure SaveToFile1(aFileName : string);
//    function ToBLPointArray: TBLPointArray;
//    function To_SQL(aBackDirection: boolean = false): string;
//
//    procedure Validate;

  end;

//
//  TBLGeometryArr = array of TBLGeometry;
//      
//
//  function TBLGeometryArr_to_SQL(aArr: TBLGeometryArr): string;
//                   
//  function BLPoint_Eq(aP1,aP2: TGeoPoint): Boolean;
//
  
        
implementation
//


//function geo_ComparePoints (aBLPoint1,aBLPoint2: TBLPoint): boolean;
//begin
//  geo_ReduceBLPoint (aBLPoint1);
//  geo_ReduceBLPoint (aBLPoint2);
//
//  Result:=(aBLPoint1.B=aBLPoint2.B) and (aBLPoint1.L=aBLPoint2.L);
//end;




function PointsEqual(aP1, aP2: TSTPoint): boolean;
begin
  Result:=(Abs(aP1.B-aP2.B)<0.0001) and (Abs(aP1.L-aP2.L)<0.0001);
  
end;


//------------------------------------------------------------------------
function PosRight(aSubStr, aValue: string): Integer;
//------------------------------------------------------------------------
var
  k: Integer;
  s: string;
begin
//  PosEx()

  s:= ReverseString(aValue);
  k:=Pos(aSubStr,s);

  if k>0 then
    Result:=Length(aValue)-k+1
  else
    Result:=-1;          
  
                        
end;



// ---------------------------------------------------------------
procedure TSTGeometry.Parce_SQL(aValue: string);
// ---------------------------------------------------------------


//TBLPoly
    // ---------------------------------------------------------------
    function StrTo_PointsArr(aValue: string): TSTPointArray;
    // ---------------------------------------------------------------
    var
      arr: TStrArray;
      arr2: TStrArray;      

 //     bl1: TGeoPoint;
  //    bl2: TGeoPoint;
      I: Integer;
      j: Integer;            
     
    begin          
//
      
      arr:=StringToStrArray_new(aValue, ',');

      SetLength(Result, Length(arr));
      
      for I := 0 to High(arr) do
      begin
        arr2:=StringToStrArray_new(arr[i], ' ');

        if Length(arr2)=2 then
        begin
          Result[i].L:=AsFloat(arr2[0]);  //!!!
          Result[i].B:=AsFloat(arr2[1]);

          Assert(Result[i].L<>0);
          Assert(Result[i].B<>0);          
          
        end;              
      end;

      
      {

      // ������ ������ � ��������� ����� .
      if Length(Result)>2 then       
      begin
         bl1:=Result[0];
         bl2:=Result[High(Result)];

         if (Abs(bl1.B-bl2.B)<0.000001) and  (Abs(bl1.L-bl2.L)<0.000001)  then
           SetLength(Result, Length(Result)-1);
         
      end;
      }

      
    end;
  

    // -----------------------------------------------------------
    function StrToPoly(aValue: string): TSTMultiPoly;
    // -----------------------------------------------------------
    var
       regexpr : TRegEx;
       match   : TMatch;
       group   : TGroup;
       i    : integer;      
    var
      oSList: TStringList;
    begin

       oSList:=TStringList.Create;
     

       regexpr := TRegEx.Create('\(([^()]*)\)',[roIgnoreCase,roMultiline]);
  //     regexpr := TRegEx.Create('\(\(([^()]*)\)\)',[roIgnoreCase,roMultiline]);     
       match := regexpr.Match(aValue);
     
        while match.Success do
        begin
       //   ShowMessage('Match : [' + match.Value + ']');
          //group 0 is the entire match, so count will always be at least 1 for a match
       //   if match.Groups.Count > 1 then    
            for i := 1 to match.Groups.Count -1 do
         //   begin
            //  s:=   match.Groups[i].Value;
       
              oSList.Add(match.Groups[i].Value);
       
          //    ShowMessage('     Group[' + IntToStr(i) + '] : [' + match.Value + ']');
           // end;
       
          match := match.NextMatch;
        end;
     

      SetLength(Result, oSList.Count);

    
      for I := 0 to oSList.Count-1 do
      begin
        Result[i]:=StrTo_PointsArr (oSList[i]);
      end;
                          
      
      FreeAndNil(oSList);
       
    end;

   
    // -----------------------------------------------------------
    function StrToMultyPoly(aValue: string): TSTMultiPoly;
    // -----------------------------------------------------------
    var
       regexpr : TRegEx;
       match   : TMatch;
       group   : TGroup;
       i    : integer;      
    var
      k1: Integer;
      k2: Integer;
      oSList: TStringList;
      s: string;
    begin

       oSList:=TStringList.Create;
     

       regexpr := TRegEx.Create('\(\(([^()]*)\)\)',[roIgnoreCase,roMultiline]);
  //     regexpr := TRegEx.Create('\(\(([^()]*)\)\)',[roIgnoreCase,roMultiline]);     
       match := regexpr.Match(aValue);
     
        while match.Success do
        begin
        //'((36.961393 55.653477,36.961392 55.653088, 36.960817 55.653117,36.960818 55.653471,36.961393 55.653477))'
          s:=match.Value;
          
          oSList.Add(match.Value);
          
       //   ShowMessage('Match : [' + match.Value + ']');
          //group 0 is the entire match, so count will always be at least 1 for a match
       //   if match.Groups.Count > 1 then    
          {
            for i := 1 to match.Groups.Count -1 do
            begin
              s:=   match.Groups[i].Value;
       
              oSList.Add(match.Groups[i].Value);
       
          //    ShowMessage('     Group[' + IntToStr(i) + '] : [' + match.Value + ']');
            end;
             }
          match := match.NextMatch;
        end;
     

      SetLength(Result, oSList.Count);

    
      for I := 0 to oSList.Count-1 do
      begin
        s:=oSList[i];
      
        k1:=Pos('(',s);
        k2:=PosRight(')',s);
      
        s:=Copy (s, k1+1, k2-k1-1);

      //!!!!!!!!!!!!!!!!!!!!!!
      
     //   Result[i]:=StrToPoly (s);
      end;
                          
      
      FreeAndNil(oSList);
       
    end;

    
    
var
  k: Integer;
  k1: Integer;
  k2: Integer;
  s: string;

 // blPoints: TBLPointArray_;
  
  I: Integer;
            
begin           
  aValue:=Trim(UpperCase(aValue));

//  Clear;

  // ---------------------------------------------------------------
  if Pos('LINESTRING',aValue)=1 then
  // ---------------------------------------------------------------
  begin
    Type_:=gtLINESTRING;

    k1:=Pos('(',aValue);
    k2:=PosRight(')',aValue);

    // LINESTRING(..)
    s:=Copy (aValue, k1+1, k2-k1-1);
    
    
    Points:=StrTo_PointsArr(s);
    
// compare first + last points
   if Length(Points)>0 then
    if PointsEqual (Points[0],Points[High (Points)] )  then
    begin
      SetLength(Poly, 1);

      Poly[0]:=Points;
      
      Type_:=gtPOLYGON_;

    end;
     
    
    //PointsEqual
    
  end else
  
//  

  // ---------------------------------------------------------------
  if Pos('POINT',aValue)=1 then
  // ---------------------------------------------------------------
  begin
    Type_:=gtPOINT;


    k1:=Pos('(',aValue);
    k2:=PosRight(')',aValue);
    
    s:=Copy (aValue, k1+1, k2-k1-1);
        
    Points:=StrTo_PointsArr(s);
    
    if Length(Points)>0 then
      POINT:=Points[0];
    
  end else


//  // ---------------------------------------------------------------
//  if Pos('MULTIPOLYGON',aValue)=1 then
//  // ---------------------------------------------------------------
//  begin
// //    ShowMessage('procedure TBLGeometry.Parce_SQL   MULTIPOLYGON');
//     Type_:=gtMULTIPOLYGON;   
//
//     k1:=Pos('(',aValue);
//     k2:=PosRight(')',aValue);
//     
// 
//     s:=Copy (aValue, k1+1, k2-k1-1);
//     
//     MultiPoly:=StrToMultyPoly (s);                    
//    
//     
//  end else

  
  // ---------------------------------------------------------------
  if Pos('POLYGON',aValue)=1 then
  // ---------------------------------------------------------------
  begin
    Type_:=gtPOLYGON_;   
                             
     k1:=Pos('(',aValue);
     k2:=PosRight(')',aValue);
     
     s:=Copy (aValue, k1+1, k2-k1-1);
                           
   ////////////////  
   Poly:=  StrToPoly(s);
                 
  
  end else    

     
  begin
    Type_:=gtNone;

   ShowMessage('Error Message: '+ aValue); 
  end;
  //  raise Exception.Create('Error Message: '+ aValue);
    
                   

end;

var 
  r: TSTGeometry;
begin
  r.Parce_SQL  (
'LINESTRING (30.303877300000003 59.955222600000006, '+
'              30.304077300000003 59.9551773, '+
'              30.3041665 59.955157400000004, 30.304678900000003 59.955042600000006, 30.3047733 59.9550215, 30.3049372 59.954991500000006, 30.3048125 59.9548672, 30.303758300000002 59.955096100000006,  '+
'              30.303877300000003 59.955222600000006)');


  r.Parce_SQL  (
'POLYGON ((30.303877300000003 59.955222600000006, '+
'              30.304077300000003 59.9551773, '+
'              30.3041665 59.955157400000004, 30.304678900000003 59.955042600000006, 30.3047733 59.9550215, 30.3049372 59.954991500000006, 30.3048125 59.9548672, 30.303758300000002 59.955096100000006,  '+
'              30.303877300000003 59.955222600000006))');


    //LINESTRING (30.303877300000003 59.955222600000006, 
//              30.304077300000003 59.9551773, 
//              30.3041665 59.955157400000004, 30.304678900000003 59.955042600000006, 30.3047733 59.9550215, 30.3049372 59.954991500000006, 30.3048125 59.9548672, 30.303758300000002 59.955096100000006, 
//              30.303877300000003 59.955222600000006)

  
end.


(*


//------------------------------------------------------------------------
procedure TBLGeometry.From_Point(aLat, aLon: Double);
//------------------------------------------------------------------------
begin                      
  POINT.B:=aLat;
  POINT.L:=aLon;  

//  SetLength(Items, 1);    
//  SetLength(Items[0], 1);
//
//  Items[0][0].B:=aLat;
//  Items[0][0].L:=aLon;  

                        
end;



//------------------------------------------------------------------------
procedure TBLGeometry.From_Vector(aLat1, aLon1, aLat2, aLon2: Double);
//------------------------------------------------------------------------
begin                      

  SetLength(Line, 2);    
//  SetLength(Items[0], 2);

  Line[0].B:=aLat1;
  Line[0].L:=aLon1;  
  
  Line[1].B:=aLat2;
  Line[1].L:=aLon2;  
                        
end;


// ---------------------------------------------------------------
function TBLGeometry.ToString: string;
// ---------------------------------------------------------------
begin
  Result:=PolyAsString; 
end;


// ---------------------------------------------------------------
function TBLGeometry.BLPointArrayAsString(aArr: TBLPointArray_; aBackDirection:
    boolean = false): string;
// ---------------------------------------------------------------
var
 // bByClock: Boolean;
  i: Integer;
  iPart: Integer;
  s,s_part: string;

//  bl: TBLPoint_;
  bl: TGeoPoint;
  
begin
 // bByClock:= aBackDirection; //IsDirectionByClock();

          
 // Result := '';
   s:=''           ;

  // -------------------------------------------
  if not aBackDirection then
  // -------------------------------------------
    for i := 0 to High(aArr) do
    begin                      
      bl:=aArr[i];

      if i>0 then
        s:=s + ',  ';
      
      s:=s+ Format('%1.8f %1.8f',[ bl.l, bl.B]);
        
    end
  else  
    for i := High(aArr) downto 0 do
    begin                      
      bl:=aArr[i];

      if i<>High(aArr) then
        s:=s + ',  ';
      
      s:=s+ Format('%1.8f %1.8f',[ bl.l, bl.B]);
        
    end; 


   Result:=s;        

end;


// ---------------------------------------------------------------
function TBLGeometry.PolyAsString(aBackDirection: boolean = false): string;
// ---------------------------------------------------------------
var
 // bByClock: Boolean;
  i: Integer;
  iPart: Integer;
  s,s_part: string;

//  bl: TBLPoint_;
  bl1,bl2: TGeoPoint;
  iLen: Integer;

begin
 // bByClock:= aBackDirection; //IsDirectionByClock();

          
  Result := '';
              
 
  for iPart := 0 to High(Poly) do
  begin                                                     
  //  s:='';
  
    s:=BLPointArrayAsString (Poly[iPart], aBackDirection);
  
  
  
    if Type_=gtPOLYGON_ then
    begin
      iLen:=Length(Poly[iPart]);
      Assert(iLen>0);

      bl1:=Poly[iPart][0];
      bl2:=Poly[iPart][iLen-1];

      Assert(Abs(bl1.B-bl2.B)< 0.00001);
      Assert(Abs(bl1.L-bl2.L)< 0.00001);
            
    end;

  

  
  {
    // -------------------------------------------
    if not aBackDirection then
    // -------------------------------------------
        for i := 0 to High(Items[iPart]) do
        begin                      
          bl:=Items[iPart][i];

          if i<>0 then
            s:=s + ',  ';
      
          s:=s+ Format('%1.8f %1.8f',[ bl.l, bl.B]);
        
        end
    else
        for i := High(Items[iPart]) downto 0 do
        begin                      
          bl:=Items[iPart][i];

          if i<>High(Items[iPart]) then
            s:=s + ',  ';
      
          s:=s+ Format('%1.8f %1.8f',[ bl.l, bl.B]);
        
        end;    
    }

    //first point = lasr point    
//    bl:=Items[iPart][0];
//    s:=s+ Format(', %1.8f %1.8f',[ bl.l, bl.B]);    
    
    if s<>'' then
    begin
    
  //    s:= Format('(%s)',[s]);

  //    if iPart>0 then
   //     Result := Result + ',';

      Result := Result + Format('(%s),',[s]);                          
          
    end;
   //   Continue;

  end;


  Result := Copy (Result, 1, Length(Result)-1);
  

  {

  if s<>'' then
    Result:= s //Format('POLYGON(%s)',[Result])
  else
    Result:='';
   }
end;            




// ---------------------------------------------------------------
function TBLGeometry.To_SQL(aBackDirection: boolean = false): string;
// ---------------------------------------------------------------
var
  s: string;
begin          
//  Corrrect_direction_ByClock1();
  Result:='';

  case Type_ of
    gtPOLYGON_: begin
                  s:= PolyAsString(aBackDirection);
                              
                  if s<>'' then
                    Result:= Format('POLYGON(%s)',[s])
                                    
                end;
  end;              
         
  
end;       


// ---------------------------------------------------------------
function TBLGeometry.To_POLYGON_SQL(aBackDirection: boolean = false): string;
// ---------------------------------------------------------------
var
  s: string;
begin          
//  Corrrect_direction_ByClock1();




  s:= PolyAsString(aBackDirection);
   

  if s<>'' then
    Result:= Format('POLYGON(%s)',[s])
  else
    Result:='';
  
end;            
             

// ---------------------------------------------------------------
function TBLGeometry.To_LINESTRING_SQL: string;
// ---------------------------------------------------------------
var
  i: Integer;
  iPart: Integer;
  s: string;   
  bl: TGeoPoint;  
begin  
 // Result:='';
                             
  s:=BLPointArrayAsString (Line);
             

  if s<>'' then
    Result:= Format('LINESTRING(%s)',[s])
  else
   Result:= '';
   
  {
  for iPart := 0 to High(Items) do
  begin   
  //  s:='';
   
//    s:=BLPointArrayAsString (Items[iPart]);

//   
//    for i := 0 to High(Items[iPart]) do
//    begin
//      bl:=Items[iPart][i];
//
//      if s<>'' then
//        s:=s + ',';
//             
//      s:=s+ Format('%1.6f %1.6f',[bl.l, bl.B]);
//         
//    end;


    if s<>'' then
    begin
      if Result<>'' then
        Result:=Result + ',';

      Result:=Result + s;
          
    end;


    
//        s:= Format('(%s)',[s]);    
//
//    if iPart>0 then
//      Result := Result + ',';

              
    
  end;

  //= 'MULTILINESTRING((1 1, 3 5), (-5 3, -8 -2))';

  if Result<>'' then
    Result:= Format('LINESTRING(%s)',[Result])
  }
  
end;





procedure Test;
var
  obj: TBLGeometry;
  s: string;
begin
 // SetLength(obj.Items,1);

 // SetLength(obj.Items[0],4);
//
//
//  obj[0][0].B:=0;
//  obj[0][0].l:=0;  
//
//  obj[0][1].B:=10;
//  obj[0][1].l:=10;  
//
//  obj[0][2].B:=30;
//  obj[0][2].l:=0;  
//
//  obj[0][3].B:=0;
//  obj[0][3].l:=0;  
//
//  
// // s:=Geometry_POLYGON_to_SQL(obj);
//  s:=Geometry_LINESTRING_to_SQL (obj);  

 // obj:=Parce_SQL_to_Geometry(s);
  
end;





// ---------------------------------------------------------------
function TBLGeometry.IsDirectionByClock (aArr: TBLPointArray_): Boolean;
// ---------------------------------------------------------------
var
  bl1,bl2,bl3: TGeoPoint;
  e: Double;
  e1: Double;
  e1_: Double;
  e2: Double;
  e2_: Double;
  eSum1: Double;
  eSum2: Double;  
 // eSum2: Double;
  I: Integer;
  iPointCount: Integer;
  j: Integer;

  geo_new : TBLGeometry;

begin
  Result:=False;


//  geo_new := MakeCopy ();

  
//  Result := aBLGeometry;
//  geo := aBLGeometry;  

  
//  for I := 0 to Count-1 do

//  for j := 0 to Length(Items)-1 do
  begin
    iPointCount:=Length(aArr);
    
  //  for I := 0 to iPointCount-1 do
   //    geo_new.Items[j][i]:=Items[j][iPointCount-1 -i ];


    eSum1 :=0;
    eSum2 :=0;    
       

    for I := 0 to iPointCount-1 do
    begin
      bl1:=aArr[(i+1 + iPointCount) mod iPointCount ];
      bl2:=aArr[(i+0 + iPointCount) mod iPointCount ];      
      bl3:=aArr[(i-1 + iPointCount) mod iPointCount ];      

  //    if (Abs(bl2.B-bl3.B)<0.0001) and (Abs(bl2.L-bl3.L)<0.0001) then
   //     Continue;
      
      if (bl2.B=bl3.B) and (bl2.L=bl3.L) then
        Continue;

      
      Assert((bl2.B<>bl3.B) or (bl2.L<>bl3.L), 'bl2.B<>bl3.B or bl2.L<>bl3.L');
    //  Assert(, 'bl2.L<>bl3.L');      
      
      e1:= geo_Azimuth_LatLon (bl2.B, bl2.L, bl1.B, bl1.L);
      e2:= geo_Azimuth_LatLon (bl2.B, bl2.L, bl3.B, bl3.L);       

   ///   e1_:=e1-e2;
    //  e2_:=e2-e1;      

      
       e1_:=Round(e1-e2+360) mod 360;
       e2_:=Round(e2-e1+360) mod 360;       
               
        
//Log(FloatToStr(e1_));
//log(FloatToStr(e2_));      
        
      eSum1:=eSum1+ e1_;        
      eSum2:=eSum2+ e2_;    

    end;
    

    Result:=eSum1<=eSum2;

    
//    if eSum1>eSum2 then
   //   for I := 0 to iPointCount-1 do
    //     Items[j][i]:=geo_new.Items[j][iPointCount-1 -i ];


    

//log(FloatToStr(eSum1));  
//log(FloatToStr(eSum2));    

 
  end;


  //if Length(aBLGeometry) >0 then

 
end;





procedure TBLGeometry.Clear;
begin
  FillChar(POINT,SizeOf(POINT),0);
  SetLength(Line,0);  
  SetLength(Poly,0);  
end;


// ---------------------------------------------------------------
procedure TBLGeometry.LoadFromMapObj (aMapObj: TMapObj);
// ---------------------------------------------------------------
var
 // geo: TBLGeometry;
  iObj: Integer;
  iPoint: Integer;
  iPointCount: Integer;

  pt: TCompMapPoint;

  
  bl1,bl2: TGeoPoint;
  bl: TGeoPoint;  
  k: Integer;

begin
//  k:= mapObjectDirect (aMapObj.FObj);

//  k:=aMapObj.ChangeDirectObject;

  
//
//  function  mapObjectDirect(Obj : HObj) : integer;
// {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF}
//external sGisAcces;
//

    

//    aMapObj.
 //   k:=aMapObj.ChangeDirectObject;

  //  k:=aMapObj.ChangeDirectObject;




  Clear;
  
  case aMapObj.Local of
    OL_PATTERN: Exit;
    
    OL_SQUARE:  Type_:=gtPOLYGON_;
    
    OL_TEXT, //:  geo.Type_:=gtLINESTRING;    
    
    OL_VECTOR,
    OL_LINE:    Type_:=gtLINESTRING;    

//    OL_VECTOR: geo.Type_:=gtVector;    
    
    OL_MARK: begin
               Type_:=gtPOINT;
                   
               pt:= aMapObj.Metric.Points[0, 1];

               Point.B:=RadToDeg(pt.X);
               Point.L:=RadToDeg(pt.Y);         
            //   Point.H:=pt.FH;

              // Assert(Point.H>=0);
               
             //  Result:=geo;
               Exit;
                
             end; 

   
  else
    raise Exception.Create(  IntToStr(aMapObj.Local));
  end;

  
//  if (aMapObj.Local in [OL_SQUARE]) then
 //  ;
   
  
//  aMapObj.

  SetLength(Poly, aMapObj.Metric.SubObjCount + 1);

//  geo.SetPartCount(aMapObj.Metric.SubObjCount + 1);

  for iObj := 0 to aMapObj.Metric.SubObjCount do
  begin
    iPointCount:=aMapObj.Metric.GetPointCount(iObj);

//    geo.SetPointCount(iObj, iPointCount);
   // SetLength(Poly[iObj], iPointCount);

    
      //  oSList.Add(Format('part: %d',[iObj]));
      //  oSList.Add(Format('points:%d', [iPointCount]));      
                 
         
    SetLength(Poly[iObj], iPointCount);
//    SetLength(geo.Items[iObj], iPointCount + 1);    
//        SetLength(geo.Items[iObj], iPointCount + 1);        

      
    for iPoint := 1 to iPointCount do    //!!!!!!!!!!!  
//        for iPoint := 0 to iPointCount-1 do        
    begin
      //-----------------------
      aMapObj.PlaceOut:=PP_GEO;
    
      pt:= aMapObj.Metric.Points[iObj, iPoint];
      bl.B:=RadToDeg(pt.X);
      bl.L:=RadToDeg(pt.Y);         

      //-----------------------
      aMapObj.PlaceOut:=PP_PLANE;
    
      pt:= aMapObj.Metric.Points[iObj, iPoint];
   //   bl.x:=0;
   //   bl.y:=0;      
      
      bl.x:=pt.X;
      bl.y:=pt.Y;         
      //-----------------------

          
      Poly[iObj][iPoint-1]:=bl;

   //   s:= Format('point:%2d - b:%1.8f - l:%1.8f',[iPoint, bl.B, bl.l]);        
    //  oSList.Add(s);
          
//         ;    
//        pt.Y;         
    end;


   // iLen:=Length(Poly[iPart]);
  
//    Assert(iLen>0);
  
    if Type_=gtPOLYGON_ then
    begin
    /////////////////
    //  asdasdasd
    
      bl1:=Poly[iObj][0];
      bl2:=Poly[iObj][iPointCount-1];

      Assert(Abs(bl1.B-bl2.B)< 0.00001);
      Assert(Abs(bl1.L-bl2.L)< 0.00001);
            
    end;
  
  

  
    
   //  geo.Items[iObj][iPointCount]:=geo.Items[iObj][0];

  end;


//  Result:=geo;
  
end;






function TBLGeometry.ToBLPointArray: TBLPointArray;
var
  I: Integer;
begin
//  for I := 0 to High(Poly) do
     
end;


// blPoints: TBLPointArray;


// ---------------------------------------------------------------
procedure TBLGeometry.SaveToFile1(aFileName : string);
// ---------------------------------------------------------------
var
  iPart: Integer;
  iPoint: Integer;
  oSList: TStringList;
  s: string;

  bl: TGeoPoint;
  
begin
  oSList:=TStringList.Create;  
                      
  {
  for iPart := 0 to High(Items) do
  begin
    s:= Format('part: %d',[iPart]);        
    oSList.Add(s);

    
    for iPoint := 0 to High(Items[iPart]) do      
    begin
   /////   bl:= Items[iPart][iPoint];
         
      //bl.B:=RadToDeg(pt.X);
///          bl.L:=RadToDeg(pt.Y);         
          
//      geo.Items[iObj][iPoint]:=bl;

      s:= Format('point:%2d - b:%1.8f - l:%1.8f',[iPoint, bl.B, bl.l]);        
      oSList.Add(s);
          
    end;
  
  end;

  oSList.Add('');

  }
  
  s:=To_POLYGON_SQL();
  oSList.Add(s);

  
  oSList.SaveToFile(aFileName);
  
  FreeAndNil(oSList);
  
end;


// ---------------------------------------------------------------
procedure TBLGeometry.Validate;
// ---------------------------------------------------------------
var
  iPart: Integer;
 /// iPoint: Integer;
 
 // s: string;

  bl1: TGeoPoint;
  bl2: TGeoPoint;  
  k: Integer;

begin
{
  for iPart := 0 to High(Items) do
  begin
  //  s:= Format('part: %d',[iPart]);        


    if Type_=gtPOLYGON_ then
    begin
      k:=Length(Items[iPart] );
    
      Assert( Length(Items[iPart] )>2); 

    //  bl1:=Items[iPart][0];
    //  bl2:=Items[iPart][High(Items[iPart])];

     // Assert( BLPoint_Eq( Items[iPart][0],  Items[iPart][High(Items[iPart])] ));
    end;

   
//    
//    for iPoint := 0 to High(Items[iPart]) do      
//    begin
//      bl:= Items[iPart][iPoint];
//         
//      //bl.B:=RadToDeg(pt.X);
/////          bl.L:=RadToDeg(pt.Y);         
//          
////      geo.Items[iObj][iPoint]:=bl;
//
//      s:= Format('point:%2d - b:%1.8f - l:%1.8f',[iPoint, bl.B, bl.l]);        
//     
//          
//    end;
// 

 
  end;
  }
  
  
end;



procedure TBLGeometry.SetPartCount(aCount : integer);
begin
 ///// SetLength(Items,aCount);
end;


procedure TBLGeometry.SetPointCount(aPartIndex,aPointCount : integer);
begin
 //// SetLength(Items[aPartIndex],aPointCount);
end;




// ---------------------------------------------------------------
function TBLGeometryArr_to_SQL(aArr: TBLGeometryArr): string;
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;

//  geo: TBLGeometry;
  s1: string;
  
begin
  // TODO -cMM: TBLGeometryArr_to_SQL default body inserted

  s:='' ;

  
  for I := 0 to High(aArr) do
  begin
  
    if s<>'' then
      s := s + ',';

     s1:=aArr[i].PolyAsString();
      
//    s := s + '('+ aArr[i].AsString;  
    s := s +  Format('(%s)',[aArr[i].PolyAsString()])

//    geo:=aArr[i];

    
    
  end;



 if s<>'' then
    Result:= Format('MULTIPOLYGON(%s)',[s])
  else
    Result:='';
    

  //MULTIPOLYGON

{
DECLARE @g geography;  
SET @g = geography::STMPolyFromText('MULTIPOLYGON( ((-122.358 47.653, -122.348 47.649, -122.358 47.658, -122.358 47.653)), ((-122.341 47.656, -122.341 47.661, -122.351 47.661, -122.341 47.656)))', 4326);  
SELECT @g.ToString();  

}
  
    

  
 // Result := ;
  
end;




procedure u_Panorama_types_test1();
var
  geom: TBLGeometry;
  s: string;
begin
  geom.Parce_SQL('POLYGON((131.86205441 43.91219062,  131.86403367 43.91176496,  131.86507439 43.91435254,  131.86312397 43.91484480,  131.86205441 43.91219062))');
//  geom.Corrrect_direction_ByClock;


  s:=geom.to_POLYGON_SQL();

  
end;


function BLPoint_Eq(aP1,aP2: TGeoPoint): Boolean;
begin
  Result:=
    (aP1.B=aP2.B) and (aP1.L=aP2.L);
  
end;



procedure Test1;
var
 geom: TBLGeometry;
  k: Integer;

begin
{

//  k:=PosRight(')','ffff)s');

//  geom.Parce_SQL('LINESTRING (36.961393 55.653477,36.961392 55.653088,36.961215)');

  geom.Parce_SQL('MULTIPOLYGON(((36.961393 55.653477,36.961392 55.653088, 36.960817 55.653117,36.960818 55.653471,36.961393 55.653477)))');


    geom.Parce_SQL('POINT (36.964885500009757 55.653265500230347)');  
  geom.Parce_SQL('POLYGON((36.961393 55.653477,36.961392 55.653088,36.961215 55.653096,36.960817 55.653117,36.960818 55.653471,36.961393 55.653477))');
 }

end;




//
// //.Parce_SQL_to_Geometry(aValue: string);


  
begin
  Test1;

 // u_Panorama_types_test1
  {


 }

  //

//'MULTIPOLYGON (((37.42697821000008 55.19442161999995, 37.426961990000038 55.193284140000074, 37.429290619999875 55.19149896000004, 37.430626999999937 55.189054089999921, 37.431224389999961 55.189204409999881, 37.431643480000083 55.188643349999879, 37.432100309999967 55.187722300000011, 37.431695559999973 55.187602479999946, 37.4331800300002 55.1861596600001, 37.434309260000177 55.186562659999936, 37.435311499999
  

end.      


   {


// ---------------------------------------------------------------
procedure TBLGeometry.Corrrect_direction_ByClock1;
// ---------------------------------------------------------------
var
  bl1,bl2,bl3: TGeoPoint;
  e: Double;
  e1: Double;
  e1_: Double;
  e2: Double;
  e2_: Double;
  eSum1: Double;
  eSum2: Double;  
 // eSum2: Double;
  I: Integer;
  iPointCount: Integer;
  j: Integer;

  geo_new : TBLGeometry;

begin

  geo_new := MakeCopy ();

  
//  Result := aBLGeometry;
//  geo := aBLGeometry;  

  
//  for I := 0 to Count-1 do

  for j := 0 to Length(Items)-1 do
  begin
    iPointCount:=Length(Items[j]);
    
  //  for I := 0 to iPointCount-1 do
   //    geo_new.Items[j][i]:=Items[j][iPointCount-1 -i ];


    eSum1 :=0;
    eSum2 :=0;    
       

    for I := 0 to iPointCount-1 do
    begin
      bl1:=Items[j][(i+1 + iPointCount) mod iPointCount ];
      bl2:=Items[j][(i+0 + iPointCount) mod iPointCount ];      
      bl3:=Items[j][(i-1 + iPointCount) mod iPointCount ];      

  //    if (Abs(bl2.B-bl3.B)<0.0001) and (Abs(bl2.L-bl3.L)<0.0001) then
   //     Continue;
      
      if (bl2.B=bl3.B) and (bl2.L=bl3.L) then
        Continue;

      
      Assert((bl2.B<>bl3.B) or (bl2.L<>bl3.L), 'bl2.B<>bl3.B or bl2.L<>bl3.L');
    //  Assert(, 'bl2.L<>bl3.L');      
      
      e1:= geo_Azimuth_LatLon (bl2.B, bl2.L, bl1.B, bl1.L);
      e2:= geo_Azimuth_LatLon (bl2.B, bl2.L, bl3.B, bl3.L);       

   ///   e1_:=e1-e2;
    //  e2_:=e2-e1;      

      
       e1_:=Round(e1-e2+360) mod 360;
       e2_:=Round(e2-e1+360) mod 360;       
               
        
//Log(FloatToStr(e1_));
//log(FloatToStr(e2_));      
        
      eSum1:=eSum1+ e1_;        
      eSum2:=eSum2+ e2_;    
      
    end;
    
    if eSum1>eSum2 then
      for I := 0 to iPointCount-1 do
         Items[j][i]:=geo_new.Items[j][iPointCount-1 -i ];


    

//log(FloatToStr(eSum1));  
//log(FloatToStr(eSum2));    

 
  end;


  //if Length(aBLGeometry) >0 then

 
end;



// ---------------------------------------------------------------
function TBLGeometry.MakeCopy: TBLGeometry;
// ---------------------------------------------------------------
var
  I: Integer;
  j: Integer;
  
begin
  Result.SetPartCount( Length(Items) );


//  SetLength(Result.Items, Length(Items));
    
  for j := 0 to Length(Items)-1 do
  begin
    Result.SetPointCount (j, Length(Items[j]));

  //  SetLength(Result.Items[j], Length(Items[j]));

    for I := 0 to Length(Items[j])-1 do
      Result.Items[j][i]:=Items[j][i];

  end;

end;
   


// ---------------------------------------------------------------
function TEntitySpatial.SaveToXML: string;
// ---------------------------------------------------------------
var
  I: Integer;
  j: Integer;
  vXMLDocument: IXMLDocument;

  vEntitySpatial,vUnit,vSpatialElement: IXMLNode;
  oSpelementUnit: TSpelementUnit;
  
begin
  vXMLDocument := NewXMLDocument();
  vXMLDocument.Options:=[doNodeAutoIndent];
  //  TXMLDocOption = (doNodeAutoCreate, doNodeAutoIndent, doAttrNull,
//    doAutoPrefix, doNamespaceDecl, doAutoSave);]
       
       
  vEntitySpatial:=vXMLDocument.AddChild('EntitySpatial');
  vEntitySpatial.Attributes['EntSys']:=EntSys;

  vEntitySpatial.Attributes['Count']:=SpatialElements.Count;

       
 // vSpatialElements:=vXMLDocument.AddChild('SpatialElements');
//  vSpatialElements.Attributes['EntSys']:=EntSys;
       
  for I := 0 to SpatialElements.Count-1 do
  begin    
    vSpatialElement:=vEntitySpatial.AddChild('SpatialElement');
    vSpatialElement.Attributes['Count']:=SpatialElements[i].SpelementUnits.Count;

    for j := 0 to SpatialElements[i].SpelementUnits.Count-1 do
    begin
      oSpelementUnit:=SpatialElements[i].SpelementUnits[j];

      vUnit:=vSpatialElement.AddChild('Unit');
      
      vUnit.Attributes['TypeUnit']:=oSpelementUnit.TypeUnit_Str;
      vUnit.Attributes['x']:=oSpelementUnit.X;
      vUnit.Attributes['y']:=oSpelementUnit.y;      
      vUnit.Attributes['SuNmb']:=oSpelementUnit.SuNmb;    

      // WGS
      vUnit.Attributes['Lat']:=oSpelementUnit.Runtime.Lat;
      vUnit.Attributes['Lon']:=oSpelementUnit.Runtime.Lon;

      
    //  Lat,: double;  
      
    end;
    
  //  SpatialElements[i].SpelementUnits.Count
  end;
    

 // vSpatialElements.AddChild()
               

  vXMLDocument.SaveToXML(Result);

//  vXMLDocument.xm


 // Result := '';
  // TODO -cMM: TEntitySpatial.SaveToXML default body inserted
end;



unit u_KPT_file;

interface

uses
  Xml.XMLIntf, System.Classes,  DB, Forms, SysUtils, Variants, XMLDoc,  Dialogs,
  StrUtils,
  
  u_Panorama_geo,

  u_Panorama_coord_MCK,
  
  u_xml1,          
  
 // u_XML_new,
     
  u_func
  


   http://www.bl2.ru/programing/regular.html


   /\(\(([^()]*)\)\)/

   ((������)), ((fghf))


   
procedure TForm29.FindSomething(const searchMe : string);
var
   regexpr : TRegEx;
   match   : TMatch;
   group   : TGroup;
   i    : integer;
  s: string;
begin
// create our regex instance, and we want to do a case insensitive search, in multiline mode
 
  regexpr := TRegEx.Create('\(\(([^()]*)\)\)',[roIgnoreCase,roMultiline]);
  match := regexpr.Match(searchMe);
  if not match.Success then
  begin
    ShowMessage('No Match Found');
    exit;
  end;
 
  while match.Success do
  begin
    ShowMessage('Match : [' + match.Value + ']');
    //group 0 is the entire match, so count will always be at least 1 for a match
    if match.Groups.Count > 1 then    
      for i := 1 to match.Groups.Count -1 do
      begin
        s:=   match.Groups[i].Value;
       
        ShowMessage('     Group[' + IntToStr(i) + '] : [' + match.Value + ']');
      end;
       
    match := match.NextMatch;
  end;
end;


Remarks
The OGC type names that can be returned by STGeometryType() are 
Point, LineString, CircularString, CompoundCurve, Polygon, CurvePolygon, GeometryCollection, MultiPoint, MultiLineString, and MultiPolygon.

LINESTRING (30.303877300000003 59.955222600000006, 30.304077300000003 59.9551773, 30.3041665 59.955157400000004, 30.304678900000003 59.955042600000006, 30.3047733 59.9550215, 30.3049372 59.954991500000006, 30.3048125 59.9548672, 30.303758300000002 59.955096100000006, 30.303877300000003 59.955222600000006)
