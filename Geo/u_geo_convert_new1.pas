unit u_geo_convert_new1;

interface

uses Math, SysUtils,

//u_geo_types,

  u_geo_convert,

  u_neva_dm_bl,
  u_GEO;


  // UTM -> LAT,LON
  function geo_UTM_to_WGS84(aXYPoint: TXYPoint; aUTM_Zone: Integer): TBLPoint;
  function geo_Pulkovo42_to_XY(aBLPoint: TBLPoint; aGK_Zone: Integer = -100):
      TXYPoint;


  //GK,Pulkovo42 -> UTM,WGS84
  function geo_GK_to_UTM(aXYPoint: TXYPoint; aGK_Zone: Integer): TXYPoint;

  function geo_Pulkovo42_to_WGS84(aBLPoint: TBLPoint): TBLPoint;



 // function geo_WGS84_to_Pulkovo42(aBLPoint: TBLPoint): TBLPoint;
 // function geo_Pulkovo42_to_WGS84(aBLPoint: TBLPoint): TBLPoint;

  function geo_Pulkovo_BL_to_BL_95(aBLPoint: TBLPoint; aFrom: byte = EK_CK_42;
      aTo: byte = EK_CK_95): TBLPoint;


//  EK_CK_42, EK_CK_95)

function geo_BL_to_BL_new(aBLPoint: TBLPoint; aDatumSrc, aDatumDest: byte):
    TBLPoint;

  //------------------------------------------------------------------------
  // GK_XY
  //------------------------------------------------------------------------
  function geo_GK_XY_to_Pulkovo42_BL(aXYPoint: TXYPoint; aGK_Zone: Integer): TBLPoint;
  function geo_GK_XY_to_WGS84_BL    (aXYPoint: TXYPoint; aGK_Zone: Integer): TBLPoint;


  //------------------------------------------------------------------------
  // WGS84
  //------------------------------------------------------------------------
  function geo_WGS84_to_UTM(aBLPoint: TBLPoint; aUTM_Zone: Integer): TXYPoint;
  function geo_WGS84_to_Pulkovo42(aBLPoint: TBLPoint): TBLPoint;

  //------------------------------------------------------------------------
  // UTM
  //------------------------------------------------------------------------
  //UTM,WGS84 -> GK,Pulkovo42
  function geo_UTM_to_GK(aXYPoint: TXYPoint; aUTM_Zone: Integer): TXYPoint;

function geo_Get_UTM_Zone(aGK_Zone: Integer): Integer;


(*

exports
  geo_UTM_to_WGS84;
*)

implementation

// ---------------------------------------------------------------
function geo_UTM_to_WGS84(aXYPoint: TXYPoint; aUTM_Zone: Integer): TBLPoint;
// ---------------------------------------------------------------

//  rXYRect.TopLeft.X:=6180493;
//  rXYRect.TopLeft.Y:=405390;

var
  b,l,lc,lc_deg: Double;
begin
  lc_deg:=(aUTM_Zone-30)*6 - 3;
  lc:=DegToRad(lc_deg);

  aXYPoint.y:=aXYPoint.y-500000;

  XY_to_BL(aXYPoint.x, aXYPoint.y, lc,0,0, 9,2, b,l); //9 WGS-1984

  Result.B :=RadToDeg(b);
  Result.L :=RadToDeg(l);
 //
 // aXYPoint.y:=aXYPoint.y-500000;

end;

// ---------------------------------------------------------------
function geo_Pulkovo42_to_XY(aBLPoint: TBLPoint; aGK_Zone: Integer = -100):
    TXYPoint;
// ---------------------------------------------------------------
var
  b,l: Double;
  x,y: Double;
  lc: Double;
begin
  Assert(aGK_Zone<100, 'aGK_Zone<100, GK_Zone:'+ IntToStr(aGK_Zone));


  if aGK_Zone=-100 then
  begin
    aGK_Zone := geo_Get6ZoneL(aBLPoint.L);
  end;


  lc:=aGK_Zone*6 - 3;
  lc:=DegToRad(lc);

  b :=DegToRad(aBLPoint.B);
  l :=DegToRad(aBLPoint.L);

  BL_to_XY(B, L, lc,0,0, 1,1, x,y); //1 Pulkovo42

  Y:= aGK_Zone*1000*1000 + Y + 500000;

  Result.x:=x;
  Result.y:=y;
end;


function geo_GK_GetCenterMeridian_Rad(aGK_Zone: double): double;
begin
 ////// Assert(aGK_Zone>0, 'Value <=0');

  Result:=DegToRad(aGK_Zone*6 - 3);
end;

function geo_UTM_GetCenterMeridian_Rad(aUTM_Zone: double): double;
begin
 /////// Assert((aUTM_Zone-30)>0, 'Value <=0');

  Result:=(aUTM_Zone-30)*6 - 3;
  Result:=DegToRad(Result);
end;


// ---------------------------------------------------------------
function geo_GK_XY_to_Pulkovo42_BL(aXYPoint: TXYPoint; aGK_Zone: Integer): TBLPoint;
// ---------------------------------------------------------------
var
  b,l: Double;
  d: Double;
  x,y: Double;
  lc: Double;
  iOldZone: integer;
begin
  iOldZone:=Trunc(aXYPoint.Y) div (1000*1000);

 // iOldZone :=aGK_Zone;
//  aGK_Zone:=iOldZone;

///////////  Assert(iOldZone>0, 'Value <=0');

  if aGK_Zone>0 then d:=geo_GetCenterMeridianBL (aGK_Zone)
                else d:=geo_GetCenterMeridianBL (iOldZone);

  lc:=DegToRad (d);
//  lc := geo_GK_GetCenterMeridian_Rad(iOldZone);

  x:=aXYPoint.x;
//  y:=Frac (aXYPoint.Y/1000000)*1000000 - 500000;  //frac-������� �����
  y:=Frac (aXYPoint.Y/1000000)*1000000 - 500000;  //frac-������� �����

  if (aGK_Zone>0) then
    Y:=Y + (iOldZone-aGK_Zone)*(1000*1000);


  XY_to_BL(x, y, lc,0,0, 1,1, b,l); //1 GK

  Result.b :=RadToDeg(B);
  Result.l :=RadToDeg(L);

end;


function geo_BL_to_BL_new(aBLPoint: TBLPoint; aDatumSrc, aDatumDest: byte):
    TBLPoint;
begin
  GEO_GEO_deg (aBLPoint.B, aBLPoint.L,  aDatumSrc, aDatumDest, Result.B, Result.L);;
end;


function geo_WGS84_to_Pulkovo42(aBLPoint: TBLPoint): TBLPoint;
begin
  GEO_GEO_deg (aBLPoint.B, aBLPoint.L,  DEF_DATUM_WGS_84, DEF_DATUM_PULKOVO_42, Result.B, Result.L);;
end;


function geo_Pulkovo42_to_WGS84(aBLPoint: TBLPoint): TBLPoint;
begin
  GEO_GEO_deg (aBLPoint.B, aBLPoint.L,  DEF_DATUM_PULKOVO_42, DEF_DATUM_WGS_84,  Result.B, Result.L);;
end;


// ---------------------------------------------------------------
function geo_UTM_to_GK(aXYPoint: TXYPoint; aUTM_Zone: Integer): TXYPoint;
// ---------------------------------------------------------------
var bl_WGS84, bl_pulkovo : TBLPoint;
begin
  bl_WGS84 := geo_UTM_to_WGS84(aXYPoint, aUTM_Zone);

  GEO_GEO_deg(bl_WGS84.B, bl_WGS84.L,
     DEF_DATUM_WGS_84, DEF_DATUM_PULKOVO_42, bl_pulkovo.B, bl_pulkovo.L);

  Result :=geo_Pulkovo42_to_XY(bl_pulkovo, aUTM_Zone-30);
end;


function geo_RadToDeg(aBLPoint: TBLPoint): TBLPoint;
begin
  Result.b :=RadToDeg(aBLPoint.B);
  Result.l :=RadToDeg(aBLPoint.L);
end;


function geo_DegToRad(aBLPoint: TBLPoint): TBLPoint;
begin
  Result.b :=DegToRad(aBLPoint.B);
  Result.l :=DegToRad(aBLPoint.L);
end;


// ---------------------------------------------------------------
function geo_WGS84_to_UTM(aBLPoint: TBLPoint; aUTM_Zone: Integer): TXYPoint;
// ---------------------------------------------------------------
var
  bl_WGS84, bl_pulkovo : TBLPoint;
  lc: Double;
  x,y,b: Double;
  l: Double;
begin
// iOldZone:=Trunc(aXYPoint.Y) div (1000*1000);

  lc := geo_UTM_GetCenterMeridian_Rad(aUTM_Zone);

  b :=DegToRad(aBLPoint.B);
  l :=DegToRad(aBLPoint.L);

  bl_to_xy(b, l, lc,0,0, 9,2, x,y); //1 GK

  Result.X :=x;
  Result.Y :=y + 500000;
end;


// ---------------------------------------------------------------
function geo_GK_to_UTM(aXYPoint: TXYPoint; aGK_Zone: Integer): TXYPoint;
// ---------------------------------------------------------------
var
  bl_WGS84, bl_pulkovo : TBLPoint;
begin
  Assert(aGK_Zone<1000);
//  Assert(aGK_Zone>0);


  bl_Pulkovo := geo_GK_XY_to_Pulkovo42_BL(aXYPoint, aGK_Zone);

  GEO_GEO_deg(bl_Pulkovo.B, bl_Pulkovo.L,
      DEF_DATUM_PULKOVO_42, DEF_DATUM_WGS_84,  bl_WGS84.B, bl_WGS84.L);

  Result :=geo_WGS84_to_UTM(bl_WGS84, aGK_Zone+30);

end;



// ---------------------------------------------------------------
function geo_Get_UTM_Zone(aGK_Zone: Integer): Integer;
// ---------------------------------------------------------------
begin
  Result := aGK_Zone+30;
end;


//
//
//// ---------------------------------------------------------------
//function geo_WGS_to_UTM(aBLPoint: TBLPoint): TXYPoint;
//// ---------------------------------------------------------------
//var
//  bl_WGS84, bl_pulkovo : TBLPoint;
//begin
//  Assert(aGK_Zone<1000);
//
//
//  bl_Pulkovo := geo_GK_XY_to_Pulkovo42_BL(aXYPoint, aGK_Zone);
//
//  GEO_GEO_deg(bl_Pulkovo.B, bl_Pulkovo.L,
//      DEF_DATUM_PULKOVO_42, DEF_DATUM_WGS_84,  bl_WGS84.B, bl_WGS84.L);
//
//  Result :=geo_WGS84_to_UTM(bl_WGS84, aGK_Zone+30);
//
//end;


//-------------------------------------------------------------
function geo_Pulkovo_BL_to_BL_95(aBLPoint: TBLPoint; aFrom: byte = EK_CK_42;
    aTo: byte = EK_CK_95): TBLPoint;
//-------------------------------------------------------------
var
  blPoint: TBLPoint;
  pDatum95, pDatum42 : PDatum7;
  tDatum95, tDatum42 : TDatum7;
  dH : double; //������ - �� ������������....
begin
  tDatum42.dX:= 23.9;
  tDatum42.dY:= -141.3;
  tDatum42.dZ:= -80.9;
  tDatum42.wX:= 0;
  tDatum42.wY:= -0.371277 / 60 / 60 / 180 * Pi;
  tDatum42.wZ:= -0.849811 / 60 / 60 / 180 * Pi;
  tDatum42.m := -0.12*Power(10, -6);

  tDatum95.dX:= 24.8;
  tDatum95.dY:= -131.24;
  tDatum95.dZ:= -82.66;
  tDatum95.wX:= 0;
  tDatum95.wY:= 0;
  tDatum95.wZ:= -0.169137/ 60 / 60 / 180 * Pi;
  tDatum95.m := -0.12*Power(10, -6);

  pDatum42:= @tDatum42;
  pDatum95:= @tDatum95;

  blPoint:= aBLPoint;
  DegToRad2(blPoint);
  //--------------------------
  if (aFrom = EK_CK_42) and (aTo = EK_CK_95) then
  begin
    GEO_GEO(blPoint.B, blPoint.L, 0, 1, 1, pDatum42, pDatum95, Result.B, Result.L, dH);

    RadToDeg2(Result);
  end else

  //--------------------------
  if (aFrom = EK_CK_95) and (aTo = EK_CK_42) then
  begin
    GEO_GEO(blPoint.B, blPoint.L, 0, 1, 1, pDatum95, pDatum42, Result.B, Result.L, dH);

    RadToDeg2(Result);
  end
end;



function geo_GK_XY_to_WGS84_BL(aXYPoint: TXYPoint; aGK_Zone: Integer): TBLPoint;
var
  BLPoint: TBLPoint;
begin
  BLPoint := geo_GK_XY_to_Pulkovo42_BL(aXYPoint, aGK_Zone);

//   geo_GK_XY_to_WGS84_BL(aXYPoint: TXYPoint; aGK_Zone: Integer): TBLPoint;

  Result := geo_Pulkovo42_to_WGS84(BLPoint);
             
end;


var
  aXYPoint, xy: TXYPoint;

begin
  aXYPoint.X:=550000;
  aXYPoint.Y:=5500000;  
  
  //xy := geo_GK_to_UTM(aXYPoint,5);



end.


//var
//  xy_UTM, xy_gk, xy,xy1: TXYPoint;
//  bl1,bl_pulkovo, bl_WGS84 : TBLPoint;
//
//begin
//
//(*
//  xy.X :=6201711;
//  xy.Y :=7386111;
//
//
//  /////////////////////////////////////
// bl1 := geo_GK_XY_to_Pulkovo42_BL(xy, 7);
// xy1 := geo_Pulkovo42_to_XY(bl1, 7);
//  /////////////////////////////////////
//
//*)
//
//  xy.X :=6180493;
//  xy.Y :=405390;
//
//
//  bl_WGS84:=geo_UTM_to_WGS84(xy,37);
//  xy1:=geo_WGS84_to_UTM(bl_WGS84,37);
//
//(*
//  GEO_GEO_deg(bl_WGS84.B, bl_WGS84.L,
//     DEF_DATUM_WGS_84, DEF_DATUM_PULKOVO_42, bl_pulkovo.B, bl_pulkovo.L);
//
//
//  GEO_GEO_deg(bl_pulkovo.B, bl_pulkovo.L,
//     DEF_DATUM_PULKOVO_42, DEF_DATUM_WGS_84,  bl1.B, bl1.L);
//
//*)
//
//  xy_gk := geo_UTM_to_GK (xy, 37);
//
//  // xy :=arrXYPointArrayF_new.Items[i];
//
// // xy_gk := geo_UTM_to_GK(xy, 37);
//
//   //test
//  xy_UTM := geo_GK_to_UTM(xy_gk, 7);
//
//
//
//  xy.Y :=405390.5;
//  xy.X :=6180492.5;
//
//
end.
