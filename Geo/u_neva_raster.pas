unit u_neva_raster;

interface
uses
  classes,SysUtils,

  u_func_arrays,
  u_func

;


type
  TNevaPrintFile = class(TObject)
  private
  public
    BitmapFileName : string;

    GK_Zone: Integer;
  //  Meridian : Integer;

    Points: array[0..3] of record
                             img_x, img_y : Integer;
                             x,y : double;
                           end;
      
  public
    procedure LoadFromFile(aFileName: string);

  end;

implementation

procedure TNevaPrintFile.LoadFromFile(aFileName: string);
var
  I: Integer;
  oStrList: TStringList;
 // Points: array[0..3] of record img_x, img_y : integer; x, y : double; end;
  s: string;
  arr: TStrArray;
  eMeridian: Integer;
begin
  oStrList:=TStringList.Create;

  oStrList.LoadFromFile(aFileName);


  arr:=StringToStrArray(oStrList[1], ' ');

  s:=replaceStr(arr[3],'.','');

  eMeridian := AsInteger(s);
  GK_Zone     := (eMeridian+3) div 6;


  for I := 0 to 3 do
  begin
    s:=oStrList[i+2];

    arr:=StringToStrArray(s, ' ');

    Points[i].img_x :=  Round(AsFloat(arr[0]));
    Points[i].img_y :=  Round(AsFloat(arr[1]));

    Points[i].x :=  Trunc(AsFloat(arr[2]));
    Points[i].y :=  Trunc(AsFloat(arr[3]));

  end;

  oStrList.Free;


  BitmapFileName := ChangeFileExt (ExtractFileName(aFileName), '.bmp');

end;

end.
