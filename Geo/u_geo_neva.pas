unit u_geo_neva;

interface
uses
  Math;

  procedure XY_to_BL_(x, y, lc, b1, b2: double; aElp, aPrj: byte; var b, l: double);
  procedure BL_to_XY_(b,l, lc,b1,b2: double; elp,prj: byte; var x,y: double);
  procedure BL_BL_(b, l, H: double; aElp, aElp_: Integer; var b_, l_, H_: double);
  function  Geoid_Dist (b1,l1, b2,l2: double; var fi: double): double;

  procedure BL_BL_new (b,l: double; elp,elp_: Integer; v,v_: Pointer; out b_,l_: double);
                          

implementation


  procedure BLH_BLH(b,l,h: double; elp,elp_: Integer; v,v_: Pointer; out b_,l_,h_: double); forward;

  //---------------------------------------------------
  //������� ��� �������� ��������� �� ����
  //---------------------------------------------------

type
  tgauss = record x,y: Double end;
  tDatum = record dX,dY,dZ,wx,wy,wz,m: Double end;



type
  TEllipsoid = record
    A,A2,Alfa, B,F,Es,Es1,E: Extended
  end;


const
  Ellipsoids_Max = 10;

var
  Ellipsoids: array[0..Ellipsoids_Max-1] of tEllipsoid =

    ((A:6378245;   Alfa:298.3),          //0
     (A:6378245;   Alfa:298.3),          //1 �����������-1942
     (A:6378388;   Alfa:297.0),          //2 WGS-1976
     (A:6378388;   Alfa:297.0),          //3 ��������-1909
     (A:6378249;   Alfa:293.5),          //4 ������-1880
     (A:6378206;   Alfa:295.0),          //5 ������-1866
     (A:6377276;   Alfa:300.0),          //6 �������-1857
     (A:6377397;   Alfa:299.2),          //7 �������-1841
     (A:6377491;   Alfa:299.3),          //8 ���-1830
     (A:6378137;   Alfa:298.257223563)   //9 WGS-1984
    );

const
  ZR  = 6367558.497;
  E2  = 6.693421623E-3;
  rad = 57.29577951308;

  Datums: array[1..10] of tDatum =
  ( //(dX:27; dY:-135; dZ:-89, wX:0; wY:0; wZ:0; m:0), // Tolik Krasovsky-1942
   // (dX:25; dY:-141; dZ:-79; wX:0; wY:0.35; wZ:0.866; m:0), // ������� ��������� 1942 �.
  (dX:23.9; dY:-141.3; dZ:-80.9; wX:0; wY:-0.0000018; wZ:-0.00000412; m:-0.00000012), // PULKOVO 1942
  (dX:0;    dY:0;      dZ:0;     wX:0; wY:0;          wZ:0;           m:0),           // WGS-1976
  (dX:0;    dY:0;      dZ:0;     wX:0; wY:0;          wZ:0;           m:0),           // ��������-1909
  (dX:0;    dY:0;      dZ:0;     wX:0; wY:0;          wZ:0;           m:0),           // ������-1880
  (dX:0;    dY:0;      dZ:0;     wX:0; wY:0;          wZ:0;           m:0),           // ������-1866
  (dX:0;    dY:0;      dZ:0;     wX:0; wY:0;          wZ:0;           m:0),           // �������-1857
  (dX:0;    dY:0;      dZ:0;     wX:0; wY:0;          wZ:0;           m:0),           // �������-1841
  (dX:0;    dY:0;      dZ:0;     wX:0; wY:0;          wZ:0;           m:0),           // ���-1830
  (dX:-1.1; dY:-0.3;   dZ:-0.9;  wX:0; wY:0;          wZ:-0.00000082; m:-0.00000012), // WGS-1984
  (dX:24.8; dY:-131.24;dZ:-82.66;wX:0; wY:0;          wZ:-0.00000082; m:-0.00000012)  // ��-95
   );




var
  c_ : tEllipsoid;
  pi2,pi_4 : double; //Extended;


  procedure m_XY_BL(X,Y, B0: double; var B,L: double); stdcall; forward;
  procedure m_BL_XY(B,L, B0: double; var X,Y: double); stdcall; forward;
  procedure conus_Init(b1,b2,l1: double); forward;
  procedure conus_BL_XY(b,l: Double; out x,y: Double); forward;
  procedure conus_XY_BL(x,y: Double; out b,l: Double); forward;
  procedure SimplePolykon_BL_XY(b,l,L0: Double; out x,y: Double); forward;
  function  SimplePolykon_XY_BL(X,Y, L0,eps: double; var B,L: double): integer; forward;



procedure Ellipsoids_Init;
var i: Integer;
begin

  for i:=0 to Ellipsoids_Max-1 do
    with Ellipsoids[i] do
    begin
      F:=1/Alfa; B:=A*(1-F);
      A2:=Sqr(A);
      Es:=2*F - F*F;
      E:=Sqrt(Es);
      Es1:=1 - Es
    end;


  {
  for i:=0 to Ellipsoids_Max-1 do
  with Ellipsoids[i] do begin
    F:=1/Alfa; B:=A*(1-F);
    Es:=2*F - F*F
  end;
  }

  pi2:=2*Pi; pi_4:=Pi/4;
  c_:=Ellipsoids[1]


end;

function c_E: double;
begin
  Result:=(Sqrt(Sqr(c_.A)-Sqr(c_.B)))/c_.A
end;

function c_R0(Fi0,E: double): double;
var sin,cos: Extended;
begin
  SinCos(Fi0, sin,cos);
  Result:=c_.A* cos / Sqrt(1-Sqr(E)*Sqr(sin))
end;

//-------------------------------------------------------------------
procedure g_XY_BL(X,Y, L0: double; var B,L: double); stdcall;
//-------------------------------------------------------------------
var
  N,N1,B2,B4,B6,F,Z,P,R,
  a11,a21,a31,b11,b21,b31,c11,c21,c31,d11,d21,d31,
  K2,K4,K6,U,V,sp,chp,SinF,TanL: double;
begin
  N:=c_.F/(2-c_.F);

  N1:=Sqr(N); R:=c_.A*(1+N1/4+N1*N1/64)/(1+N);
  B2:=N/2-N1*(2/3)+N*N1*(37/96); B4:=N1/48+N*N1/15;
  B6:=N*N1*17/480; K2:=2*(N-N1/3-N*N1);
  K4:=N1*7/3-N1*N*8/5; K6:=N1*N*56/15;
  U:=X/R; V:=Y/R; a11:=Sin(2*U); b11:=Cos(2*U);
  a21:=2*a11*b11; b21:=1-2*(a11*a11);
  a31:=a11*b21+a21*b11; b31:=b11*b21-a11*a21;
  c11:=(exp(2*V)-exp(-2*V))/2; d11:=sqrt(1+sqr(c11));
  c21:=2*c11*d11; d21:=1+2*sqr(c11);
  c31:=c11*d21+c21*d11; d31:=c11*c21+d11*d21;
  Z:=U-B2*a11*d11-B4*a21*d21-B6*a31*d31;
  P:=V-B2*b11*c11-B4*b21*c21-B6*b31*c31;
  sp:=(exp(P)-exp(-P))/2; chp:=sqrt(1+Sqr(sp));
  SinF:=Sin(Z)/chp; F:=ArcTan((SinF)/(Sqrt(1-Sqr(SinF))));
  TanL:=sp/Cos(Z); L:=ArcTan(TanL);
  B:=F+K2*Sin(2*F)+K4*sin(4*F)+K6*sin(6*F);

  IF B > 88/rad THEN B:=B-Pi;
  L:=L+L0;
end;

//-------------------------------------------------------------------
procedure g_BL_XY(B,L, L0: double; var X,Y: double); stdcall;
//-------------------------------------------------------------------
var
  N,N1,RAS,A2,A4,A6,F,Z,P,R,ALF,TZ,TZ2,TH,T2,
  a11,a21,a31,b11,b21,b31,c11,c21,c31,d11,d21,d31,
  K2,K4,K6,LL,LLL: double;
begin
  N:=c_.F/(2-c_.F);

  N1:=Sqr(N); R:=c_.A*(1+N1/4+N1*N1/64)/(1+N);
  A2:=N/2-N1*(2/3)+N*N1*(5/16);
  A4:=N1*13/48-N*N1*3/5;
  A6:=N*N1*61/240;
  K2:=2*(N-N1/3-N*N1*2/3);
  K4:=N1*5/3-N1*N*16/15;
  K6:=N1*N*26/15;

  LLL:=ABS(L); IF LLL >= Pi THEN
  begin LLL:=pi2-LLL; L:=-LLL END;
  IF L < 0 THEN LL:=L+pi2 ELSE LL:=L;

  IF Abs(L0) >= Pi THEN L0:=L0-pi2;
  IF L0 < 0 THEN L0:=L0+pi2; RAS:=LL-L0;

  IF B < 0 THEN BEGIN B:=Abs(B);
    F:=K2*SIN(2*B)+K4*SIN(4*B)-K6*SIN(6*B)-B
  END
  ELSE F:=B-K2*SIN(2*B)+K4*SIN(4*B)-K6*SIN(6*B);

  TZ:=(Sin(F)/Cos(F))/Cos(RAS); TZ2:=Sqr(TZ);
  Z:=ArcTan(TZ); TH:=COS(F)*SIN(RAS);
  T2:=Sqr(TH); P:=Ln((1+TH)/(1-TH))/2;
  a11:=2*TZ/(1+TZ2); b11:=(1-TZ2)/(1+TZ2);
  c11:=2*TH/(1-T2); d11:=(1+T2)/(1-T2);
  a21:=2*a11*b11; c21:=2*c11*d11;
  b21:=1-2*Sqr(a11); d21:=1+2*Sqr(c11);
  a31:=a11*b21+a21*b11; c31:=c11*d21+c21*d11;
  b31:=b11*b21-a11*a21; d31:=d11*d21+c11*c21;

  X:=R*(Z+A2*a11*d11+A4*a21*d21+A6*a31*d31);
  Y:=R*(P+A2*b11*c11+A4*b21*c21+A6*b31*c31)
end;

//-------------------------------------------------------------------
procedure m_XY_BL(X,Y, B0: double; var B,L: double); stdcall;
//-------------------------------------------------------------------
var
  i: integer; grad,old: double;
  e, fi, r0, k,k1, x2,dX, u,w: double;
BEGIN
  e:=c_E; r0:=c_R0(B0,e);

  fi:=0; grad:=Pi/180; dX:=-x;

  for i:=1 to 2048 do begin
    w:=e*Sin(fi);
    w:=ArcTan(w/Sqrt(1-Sqr(w)));
    k:=Pi/4 + fi/2;
    k1:=Pi/4 + w/2;

    u:=(Sin(k)/Cos(k)) / (Exp(e*Ln(Sin(k1)/Cos(k1))));

    x2:=r0*Ln(u); Old:=dX; dX:=x2-x;

    if Abs(dX) > 111000 then begin

      if (Old < 0) <> (dX < 0) then
      grad:=grad/2;

      if dX > 0 then fi:=fi-grad
      else fi:=fi+grad
    end else
    if Abs(dX) > 0.1 then
      fi:=fi-(dX/30/36000*grad)
    else
      Break;
  end;

  B:=fi; L:=y/r0
end;

//-------------------------------------------------------------------
procedure m_BL_XY(B,L, B0: double; var X,Y: double); stdcall;
//-------------------------------------------------------------------
var e, r0, k,k1, u, w: double;
begin
  e:=c_E; r0:=c_R0(B0,e);

  w:=e*Sin(B);
  w:=ArcTan(w/Sqrt(1-Sqr(w)));
  k:=PI/4+B/2; k1:=PI/4+w/2;

  u:=(Sin(k)/Cos(k)) / (Exp(e*Ln(Sin(k1)/Cos(k1))));

  X:=r0*Ln(u); Y:=r0*L
end;


function elp_(elp: byte): byte;
begin
  if elp > 6 then elp:=0;
  Result:=elp
end;

//-------------------------------------------------------------------
function Geoid_Dist(b1,l1, b2,l2: double; var fi: double): double;
//-------------------------------------------------------------------
var
  w1,w2,su1,cu1,su2,alfa,sinsg,
  cu2,c1,d1,c2,b,d2,beta,cossg,
  lam,sg,l,p,q,sina0,x_cosa0,
  x,y,del,dl,a: double;
begin
  if Abs(b2-b1) < 0.000001 then
  if Abs(l2-l1) < 0.000001 then
  begin fi:=0; Result:=0; Exit end;

  b1:=b1 + 1.0E-20; b2:=b2 + 1.0E-20;
  l1:=l1 + 1.0E-20; l2:=l2 + 1.0E-20;

  if l1 > 2*pi then l1:=(l1/pi2 -  Trunc(l1 /pi2))*pi2;
  if b1 > 2*pi then b1:=(b1/pi2 -  Trunc(b1 /pi2))*pi2;
  if l2 > 2*pi then l2:=(l2/pi2 -  Trunc(l2 /pi2))*pi2;
  if b2 > 2*pi then b2:=(b2/pi2 -  Trunc(b2 /pi2))*pi2;

  w1 :=Sqrt(1.0 - e2*sin(b1)*sin(b1)) + 1.0E-10;
  w2 :=Sqrt(1.0 - e2*sin(b2)*sin(b2)) + 1.0E-10;
  su1:=sin(b1) * Sqrt(1.0 - e2) / w1;
  su2:=sin(b2) * Sqrt(1.0 - e2) / w2;
  cu1:=cos(b1) / w1  + 1.0E-10      ;
  cu2:=cos(b2) / w2  + 1.0E-10      ;

  l  := l2-l1  ;
  c1 := su1*su2;
  c2 := cu1*cu2;
  d1 := cu1*su2;
  d2 := su1*cu2;

  del:=0;

  REPEAT
    dl  := del;
    lam := l + dl ;
    p   := cu2* sin (lam);
    q   := d1 - d2*cos(lam) + 1.0E-10 ;
    fi  := arctan(p/q);

    if p >= 0.0 then if q >= 0.0 then fi :=      abs(fi)
                                 else fi := pi - abs(fi)
                else if q >= 0.0 then fi := 2.0* pi - abs(fi)
                                 else fi := pi      + abs(fi);

    sinsg := p* sin(fi) + q* cos(fi) ;
    cossg := c1 + c2* cos(lam) ;
    if cossg >= 0.0 then sg :=      abs (arctan(sinsg/cossg))
                    else sg := pi - abs (arctan(sinsg/cossg));

    sina0  :=cu1 * sin(fi);
    x_cosa0:=1 - sina0*sina0;

    x   := 2*c1 - cos(sg)*x_cosa0;
    alfa:= 3.352329869E-3 - (2.8189E-6 - 7.0E-9 * x_cosa0) * x_cosa0;
    beta:= 2.8189E-6 - 9.4E-9 * x_cosa0;
    del := (alfa* sg - beta* x* sin(sg)) * sina0;

  UNTIL Abs(del - dl) <= 1.0E-10;

  y:=(Sqr(x_cosa0) - 2*x*x) * cos(sg);

  a:=6356863.020 + (10708.949 - 13.474  * x_cosa0) * x_cosa0;

  b:=10708.938 - 17.956 * x_cosa0;
  Result:=a* sg + (b* x + 4.487* y) * sinsg
end;



//----------------------------------------------------------
procedure BL_BL_(b, l, H: double; aElp, aElp_: Integer; var b_, l_, H_: double);
//----------------------------------------------------------
var
  from_dat,to_dat,from_idx,to_idx: Integer;
  X1,Y1,Z1,X2,Y2,Z2,dX,dY,dZ, WX, WY, WZ, a1,b1,a2,b2, es1,es2, h1,dm: double;

  phi1, lam1, M, N, temp: double;
  fTemp, R2,Teta,sinphi, cosphi, sinlam, coslam: extended;
begin
 {$DEBUGINFO OFF}
   {$RANGECHECKS OFF}


 try
  b_:=b; l_:=l; H_:=H;
  if aElp<1 then aElp:=1;
  if aElp_<1 then aElp_:=1;
  if aElp <> aElp_ then
  if aElp in [1..9] then
  if aElp_ in [1..9] then begin

    from_dat:=aElp; to_dat:=aElp_;

    DX:=Datums[from_dat].dx - Datums[to_dat].dx;
    DY:=Datums[from_dat].dy - Datums[to_dat].dy;
    DZ:=Datums[from_dat].dz - Datums[to_dat].dz;
    WX:=(Datums[from_dat].wx - Datums[to_dat].wx)/3600*pi/180;
    WY:=(Datums[from_dat].wy - Datums[to_dat].wy)/3600*pi/180;
    WZ:=(Datums[from_dat].wz - Datums[to_dat].wz)/3600*pi/180;
    dm:=Datums[from_dat].m - Datums[to_dat].m;

    from_idx:=aElp; to_idx:=aElp_;

    a1:=Ellipsoids[from_idx].A;
    b1:=Ellipsoids[from_idx].B;
    a2:=Ellipsoids[to_idx].A;
    b2:=Ellipsoids[to_idx].B;

    // get ec   centricity squared
    es1:=Ellipsoids[from_idx].Es;
    es2:=Ellipsoids[to_idx].Es;

    phi1:=b; lam1:=l; h1:=H;

    // calculate sin and cos of lat and lng
    SinCos(phi1, sinphi,cosphi);
    SinCos(lam1, sinlam,coslam);

    // calculate M and N
    temp:= 1.0 - es1 * SQR(sinphi);
    try
    fTemp:=Power(temp,1.5);
    except end;

    M:= a1 * (1.0 - es1) / Power(temp,1.5);
    N:= a1 / sqrt(temp);
    X1:=(N+h1)*cosphi*coslam;
    Y1:=(N+h1)*cosphi*sinlam;
    if Y1<0.001 then Y1:=0.001;
    Z1:=(N+h1-N*ES1)*sinphi;

    X2:=(X1-wZ*Y1+Wy*Z1)*(1+dm)+DX;
    Y2:=(Y1+wZ*X1-Wx*Z1)*(1+dm)+DY;
    Z2:=(Z1-wy*X1+Wx*Y1)*(1+dm)+DZ;
    R2:=SQRT(SQR(X2)+SQR(Y2));
    TETA:=Arctan(Z2/((1-Ellipsoids[to_idx].f)*r2));
    b_:=Arctan( ((Es2/SQRT(1-ES2))*a2*Power(sin(TETA),3)+Z2)/
                 (R2-A2*es2*Power(cos(TETA),3)) );
    if Y2>0 then
      l_:=Pi*0.5-arctan(X2/Y2)
    else
      l_:=-Pi*0.5+arctan(X2/(-Y2));

    H_:=Z2*sin(b_)+r2*cos(b_)-a2*SQRT(1-es2*sqr(sin(b_)));
  end;
    // calculate delta latitude

  except
  end;

   {$RANGECHECKS ON}

{$DEBUGINFO ON}
end;


var
  RN: record
     alf,C,e,Ro1,Bmin,Bmax,
     Lmin,Lmax,Lmain_O,MS,MC,X0,Y0,MSb,MCb,GX0,GY0,
     C2,C4,C6,C8, _B1,_B2,_L1: double;
   end;

//----------------------------------------------------------
procedure Calc_U_R(bl_b: double; var U,R: double);
//----------------------------------------------------------
var B,Ux,Rx: double;
begin
  B:=RN.e*sin(bl_b);
  B:=Arctan(B/Sqrt(1-Sqr(B)));
  Ux:=pi_4+bl_b*0.5;
  Rx:=pi_4+b*0.5;
  Rx:=exp(RN.e*ln(cos(Rx)/Sin(Rx)));
  Ux:=sin(Ux)/cos(Ux);
  U:=Ux*Rx;
  Rx:=c_.A*cos(Bl_b);
  R:=Rx/Sqrt(1-Sqr(RN.e*sin(BL_b)));
end;

//----------------------------------------------------------
procedure conus_Init(b1,b2,l1: double);
//----------------------------------------------------------
var B,Fx,Ux,Rx,U1,R1,U2,R2: double;
    e2,e4,e6,e8: double;
begin
  if (RN._B1 <> b1)
  or (RN._B2 <> b2)
  or (RN._L1 <> l1) then begin

    RN._B1:=b1; RN._B2:=b2; RN._L1:=l1;

    RN.e:=Sqrt(c_.Es);
    Calc_U_R(B1,U1,R1);
    Calc_U_R(B2,U2,R2);
    RN.alf:=(Ln(R1)-Ln(R2))/(Ln(U2)-Ln(U1));

    FX:=Arctan(RN.alf/Sqrt(1-Sqr(RN.alf)));
    B:=RN.e*sin(Fx);
    B:=arctan(B/Sqrt(1-Sqr(B)));
    Ux:=pi_4+FX*0.5;
    Rx:=pi_4+B*0.5;
    Rx:=Cos(Rx)/Sin(Rx);
    Rx:=exp(ln(Rx)*RN.e);
    Ux:=sin(Ux)*Rx/Cos(Ux);
    Rx:=c_.A*cos(Fx)/sqrt(1-sqr(RN.e*sin(fx)));
    RN.C:=sqrt(R1*Rx*exp(ln(U1*ux)*RN.alf))/RN.alf;
    RN.Ro1:=RN.C/exp(ln(u1)*RN.alf);

    e2:=c_.Es; e4:=sqr(e2);
    e6:=e2*e4; e8:=sqr(e4);

    RN.c2:=e2/2+5/24*e4+e6/12+13/360*e8;
    RN.c4:=7/48*e4+29/240*e6+811/11520*e8;
    RN.c6:=7/120*e6+81/1120*e8;
    RN.c8:=4279/161280*e8;
  end
end;

//----------------------------------------------------------
procedure conus_BL_XY(b,l: Double; out x,y: Double);
//----------------------------------------------------------
var U,R,RO: double;
     sin,cos: extended;
begin
   Calc_U_R(b,U,R);
   RO:=RN.c/exp(ln(u)*RN.alf);

   SinCos(RN.alf*(l-RN._L1), sin,cos);
   x:=RN.RO1-RO*cos; y:=RO*sin;
end;

//----------------------------------------------------------
procedure conus_XY_BL(x,y: Double; out b,l: Double);
//----------------------------------------------------------
var u2,fi: double;
begin
  x:=RN.Ro1-x;
  L:=Arctan2(y,x)/RN.alf+RN._L1;
  u2:=sqr(exp(ln(RN.c/ Hypot(x,y) )/RN.alf));
  fi:=arcsin((u2-1)/(u2+1));
  B:=fi+RN.C2*sin(2*fi)+RN.C4*sin(4*fi)+
        RN.C6*sin(6*fi)+RN.C8*sin(8*fi)
end;

//----------------------------------------------------------
procedure SimplePolykon_BL_XY(b,l,L0: Double; out x,y: Double);
//----------------------------------------------------------
var
  temp,dX, NR, ro, delt, N,N1,A2,A4,A6,F,R,TZ,TZ2,
  a11,a21,a31,b11,bb,
  K2,K4,K6: double;
  sinphi,cosphi: extended;
begin
  // ���������� ���������� �� ��������
  N:=c_.F/(2-c_.F);

  N1:=Sqr(N); R:=c_.A*(1+N1/4+N1*N1/64)/(1+N);
  A2:=N/2-N1*(2/3)+N*N1*(5/16);
  A4:=N1*13/48-N*N1*3/5;
  A6:=N*N1*61/240;
  K2:=2*(N-N1/3-N*N1*2/3);
  K4:=N1*5/3-N1*N*16/15;
  K6:=N1*N*26/15;

  IF B < 0 THEN BEGIN bb:=Abs(B);
    F:=K2*SIN(2*Bb)+K4*SIN(4*Bb)-K6*SIN(6*Bb)-Bb
  END
  ELSE F:=B-K2*SIN(2*B)+K4*SIN(4*B)-K6*SIN(6*B);

  TZ:=(Sin(F)/Cos(F));  TZ2:=Sqr(TZ);
  a11:=2*TZ/(1+TZ2); b11:=(1-TZ2)/(1+TZ2);
  a21:=2*a11*b11;

//  b21:=1;
//  a31:=a11*b21+a21*b11;
//  b31:=b11*b21-a11*a21;

  //DX:=R*(Z+A2*a11*d11+A4*a21*d21+A6*a31*d31);
  // ������� �������� ���������� ��������� � ��������� ������� ��������� �������������

  a31:=0;
  DX:=R*(F+A2*a11+A4*a21+A6*a31);

    SinCos(b, sinphi,cosphi);
    temp:= 1.0 - c_.Es * SQR(sinPhi);
    NR:=  c_.a / sqrt(temp);
   if abs(b)<0.00001 then begin
   X:=DX;
   Y:=NR*(L-L0);
   end else begin
    ro:=NR*cosphi/sinPhi;
    delt:=(L-L0)*sinphi;
    X:=DX+ro*(1-cos(delt));
    Y:=ro*sin(Delt)
   end;
end;

//----------------------------------------------------------
function  SimplePolykon_XY_BL(X,Y, L0,eps: double; var B,L: double):integer;
//----------------------------------------------------------
var
  i: integer;
  tgc:tgauss;
  tau,grad,old,e, fi, lpr,r0, k,k1, x2,dX, u, db,dl: double;
  N,N1,B2,B4,B6,F,Z,P,R,
  a11,a21,a31,b11,b21,b31,c11,c21,c31,d11,d21,d31,
  K2,K4,K6,V,sp,chp,SinF,TanL: double;
begin
  N:=c_.F/(2-c_.F);

  N1:=Sqr(N); R:=c_.A*(1+N1/4+N1*N1/64)/(1+N);
  B2:=N/2-N1*(2/3)+N*N1*(37/96); B4:=N1/48+N*N1/15;
  B6:=N*N1*17/480; K2:=2*(N-N1/3-N*N1);
  K4:=N1*7/3-N1*N*8/5; K6:=N1*N*56/15;
  U:=X/R; V:=Y/R; a11:=Sin(2*U); b11:=Cos(2*U);
  a21:=2*a11*b11; b21:=1-2*(a11*a11);
  a31:=a11*b21+a21*b11; b31:=b11*b21-a11*a21;
  c11:=(exp(2*V)-exp(-2*V))/2; d11:=sqrt(1+sqr(c11));
  c21:=2*c11*d11; d21:=1+2*sqr(c11);
  c31:=c11*d21+c21*d11; d31:=c11*c21+d11*d21;
  Z:=U-B2*a11*d11-B4*a21*d21-B6*a31*d31;
  P:=V-B2*b11*c11-B4*b21*c21-B6*b31*c31;
  sp:=(exp(P)-exp(-P))/2; chp:=sqrt(1+Sqr(sp));
  SinF:=Sin(Z)/chp; F:=ArcTan((SinF)/(Sqrt(1-Sqr(SinF))));
  TanL:=sp/Cos(Z);
  Lpr:=ArcTan(TanL);
  fi:=F+K2*Sin(2*F)+K4*sin(4*F)+K6*sin(6*F);
   tau:=1.9e-7;
 i:=0;

   repeat
     inc(i);
     SimplePolykon_BL_XY(fi,Lpr,0,tgc.x,tgc.y);
     dl:=tau*(y-tgc.y);
     Lpr:=Lpr+dl;

     db:=tau*(x-tgc.x);
     fi:=fi+db;
    if i>100000 then break;
    until max(ABS(db),ABS(dl))<eps;

    B:=fi; L:=Lpr+L0;
    Result:=i;
end;

//----------------------------------------------------------
procedure XY_to_BL_(x, y, lc, b1, b2: double; aElp, aPrj: byte; var b, l: double);
//----------------------------------------------------------
begin
  c_:=Ellipsoids[elp_(aElp)];

  case aPrj of
      0,
      1:  g_XY_BL(x,y,lc,b,l);
      2:  begin
            x:=x/0.9996; y:=y/0.9996;
            g_XY_BL(x,y,lc,b,l);
          end;
      3:  m_XY_BL(x,y,b1,b,l);

      4:  begin
            conus_Init(b1,b2,lc);
            conus_XY_BL(x,y, b,l);
          end;

      5:  begin
            b:=x/180000;
            l:=y/120000
          end
  end
end;

//----------------------------------------------------------
procedure BL_to_XY_ (b,l, lc,b1,b2: double; elp,prj: byte; var x,y: double);
//----------------------------------------------------------
begin
  c_:=Ellipsoids[elp_(elp)];

  case prj of
      0,
      1:  g_BL_XY(b,l,lc,x,y);
      2:  begin
            g_BL_XY(b,l,lc,x,y);
            x:=x*0.9996; y:=y*0.9996;
          end;

      3:  m_BL_XY(b,l,b1,x,y);

      4:  begin
            conus_Init(b1,b2,lc);
            conus_BL_XY(b,l, x,y);
          end;

      5:  begin
            x:=b*180000;
            y:=l*120000;
          end
  end
end;



procedure BL_BL_new (b,l: double; elp,elp_: Integer; v,v_: Pointer; out b_,l_: double);
var h_: Double;
begin
  BLH_BLH(b,l,0,elp,elp_,v,v_,b_,l_,h_)
end;



//------------------------------------------------------
procedure BLH_BLH (b,l,h: double; elp,elp_: Integer; v,v_: Pointer; out b_,l_,h_: double);
//------------------------------------------------------
var
  from_dat,to_dat,from_idx,to_idx: Integer;
  dX,dY,dZ, a1,b1,a2,b2, es1,es2, h1: Double;
  vx,vy,vz, vx_,vy_,vz_: Integer;

  phi1, lam1, M, N, temp: Double;
  sinphi, cosphi, sinlam, coslam: Extended;

  Dlat, Dlng, Dh, Da, Df: Double;

begin
  b_:=b; l_:=l;

  vx:=0; vy:=0; vz:=0;

  if elp=1 then // Added by Aleksey 16.09.2009 14:50:14
  begin
    vx:=28; vy:=-130; vz:=-95;
  end;


  if Assigned(v) then begin
  //  vx:=v.x; vy:=v.y; vz:=v.z;
  end else
  if elp = 0 then
  begin
    vx:=28; vy:=-130; vz:=-95
  end;

  vx_:=0; vy_:=0; vz_:=0;

  if Assigned(v_) then begin
   // vx_:=v_.x; vy_:=v_.y; vz_:=v_.z;
  end else
  if elp_ = 0 then
  begin
    vx_:=28; vy_:=-130; vz_:=-95
  end;

  if elp = 0 then elp:=1;
  if elp_ = 0 then elp_:=1;

  if (elp > 0) and (elp_ > 0) then
  if elp <= Ellipsoids_Max then
  if elp_ <= Ellipsoids_Max then

  if (elp <> elp_)
  or (vx <> vx_)
  or (vy <> vy_)
  or (vz <> vz_) then begin

    from_dat:=elp; to_dat:=elp_;

    DX:=vx - vx_; DY:=vy - vy_; DZ:=vz - vz_;

    from_idx:=elp; to_idx:=elp_;

    a1:=Ellipsoids[from_idx].A;
    b1:=Ellipsoids[from_idx].B;
    a2:=Ellipsoids[to_idx].A;
    b2:=Ellipsoids[to_idx].B;

    // get eccentricity squared
    es1:=Ellipsoids[from_idx].Es;
    es2:=Ellipsoids[to_idx].Es;

    // calculate delta major radius
    Da:= a2 - a1;

    // calculate delta flattening
    Df:=Ellipsoids[to_idx].F - Ellipsoids[from_idx].F;

    phi1:=b; lam1:=l; h1:=0;

    // calculate sin and cos of lat and lng
    SinCos(phi1, sinphi,cosphi);
    SinCos(lam1, sinlam,coslam);

    // calculate M and N
    temp:= 1.0 - es1 * sinphi * sinphi;
    M:= a1 * (1.0 - es1) / Power(temp,1.5);
    N:= a1 / sqrt(temp);

    // calculate delta latitude
    temp:=Sqrt(1.0 - es1);
    Dlat:=(-DX * sinphi * coslam - DY * sinphi * sinlam +
          DZ * cosphi + Da  * (N * es1 * sinphi * cosphi) / a1 +
          Df * (M / temp + N * temp) * sinphi * cosphi) / (M + h1);

    while Dlat > Pi do Dlat:=Dlat - Pi;

    // calculate delta longitude
    Dlng:=(-DX * sinlam + DY * coslam) / ((N + h1) * cosphi);

    // calculate delta height
    Dh:=DX * cosphi * coslam + DY * cosphi * sinlam + DZ * sinphi -
        Da * (a1 / N) + Df * temp * N * sinphi * sinphi;

    // save new latitude, longitude
    b_:=b + Dlat; l_:=l + Dlng; h_:=h + Dh
  end;
end;


begin
//initialization

  Ellipsoids_Init;
  FillChar(RN,SizeOf(RN),0);
  conus_Init(1.11,1.22,0.5);


end.






{
type
  tEllipsoid = record A,Alfa, B,F,Es: double end;

const
  Ellipsoids_Max = 10;

  Ellipsoids: array[0..Ellipsoids_Max-1]d_DB_select of tEllipsoid =

    ((A:6378245;   Alfa:298.3),
     (A:6378245;   Alfa:298.3),          // �����������-1942
     (A:6378388;   Alfa:297.0),          // WGS-1976

     (A:6378388;   Alfa:297.0),          // ��������-1909
     (A:6378249;   Alfa:293.5),          // ������-1880
     (A:6378206;   Alfa:295.0),          // ������-1866
     (A:6377276;   Alfa:300.0),          // �������-1857
     (A:6377397;   Alfa:299.2),          // �������-1841
     (A:6377491;   Alfa:299.3),          // ���-1830
     (A:6378137;   Alfa:298.257223563)   // WGS-1984
    );
}




(*
const
  Ellipsoids_Max = 22;

  Ellipsoids: array[0..Ellipsoids_Max] of TEllipsoid =

    ((A:6378245;     Alfa:298.3),
     (A:6378245;     Alfa:298.3),              // [KA] Krasovsky 1940
     (A:6378388;     Alfa:298.26),             // [WD] WGS 1972

     (A:6378200;     Alfa:298.3),              // [HE] Helmert 1906
     (A:6378270;     Alfa:297.0),              // [HO] Hough 1960
     (A:6378249.145; Alfa:293.465),            // [CD] Clark 1880
     (A:6378206.4;   Alfa:294.9786982),        // [CC] Clark 1866
     (A:6377397.155; Alfa:299.1528128),        // [BR] Bessel 1841
     (A:6377483.865; Alfa:299.1528128),        // [BN] Bessel-N 1841
     (A:6378137;     Alfa:298.257223563),      // WGS-1984

     (A:6377563.396; Alfa:299.3249646),        // [AA] Airy 1830
     (A:6377340.189; Alfa:299.3249646),        // [AM] Modified Airy
     (A:6377276.345; Alfa:300.8017),           // [EA] Everest 1830
     (A:6377309.613; Alfa:300.8017),           // [EE] Everest 1948
     (A:6377301.243; Alfa:300.8017),           // [EC] Everest 1956
     (A:6377298.556; Alfa:300.8017),           // [EB] Everest Brunei
     (A:6377309.613; Alfa:300.8017),           // [EF] Everest Pakistan

     (A:6378137;     Alfa:298.257222101),      // [RF] GRS 1980
     (A:6378160;     Alfa:298.247),            // [ID] Indonesian 1974
     (A:6378388;     Alfa:297),                // [IN] International 1924
     (A:6378155;     Alfa:298.3),              // [FA] Modified Fischer 1960
     (A:6378160;     Alfa:298.25),             // [SA] South American 1969
     (A:6378160;     Alfa:298.25)              // [AN] Australian
    );



  ZR  = 6367558.497;
  E2  = 6.693421623E-3;
  rad = 57.29577951308;

  Datums: array[1..9] of tDatum =
  (
  (dX:28; dY:-130; dZ:-95;  wX:0; wY:0; wZ:0; m:0),      // PULKOVO 1942
   (dX:0; dY:0; dZ:0; wX:0; wY:0; wZ:0; m:0),             // WGS-1976
   (dX:0; dY:0; dZ:0; wX:0; wY:0; wZ:0; m:0),             // ��������-1909
   (dX:0; dY:0; dZ:0; wX:0; wY:0; wZ:0; m:0),             // ������-1880
   (dX:0; dY:0; dZ:0; wX:0; wY:0; wZ:0; m:0),             // ������-1866
   (dX:0; dY:0; dZ:0; wX:0; wY:0; wZ:0; m:0),             // �������-1857
   (dX:0; dY:0; dZ:0; wX:0; wY:0; wZ:0; m:0),             // �������-1841
   (dX:0; dY:0; dZ:0; wX:0; wY:0; wZ:0; m:0),             // ���-1830
   (dX:0; dY:0; dZ:0; wX:0; wY:0; wZ:0; m:0)              // WGS-1984
   );
*)




