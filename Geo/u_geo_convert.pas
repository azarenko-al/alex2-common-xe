unit u_geo_convert;

interface
uses Math,

  dm_bl,
  u_geo_neva,
  u_GEO;


//  EK_WGS84       =  9; //EK_WGS_84;  // WGS 1984�.
//  EK_WGS_84 = EK_WGS84;


  function geo_XY_to_BL(aXYPoint: TXYPoint; aZone: integer=0; aFromElp: byte =
      EK_CK_42; aToElp: Integer = EK_CK_42): TBLPoint;

  function geo_BL_to_XY(aBLPoint: TBLPoint; aZone: integer=0; aElp: byte = EK_CK_42):
      TXYPoint;

  function geo_BL_to_BL(aBLPoint: TBLPoint; aFrom,aTo: byte): TBLPoint;


  function geo_Pulkovo42_to_WGS84(aBLPoint: TBLPoint): TBLPoint;
//  procedure geo_Pulkovo42_to_WGS84(var aLat,aLon: double);

  function geo_WGS84_to_Pulkovo42(aBLPoint: TBLPoint): TBLPoint;


  function geo_BL_to_BL_95(aBLPoint: TBLPoint; aFrom,aTo: byte): TBLPoint;



implementation

//-------------------------------------------------------------
function geo_BL_to_BL_95(aBLPoint: TBLPoint; aFrom,aTo: byte): TBLPoint;
//-------------------------------------------------------------
var
  blPoint: TBLPoint;
  pDatum95, pDatum42 : PDatum7;
  tDatum95, tDatum42 : TDatum7;
  dH : double; //������ - �� ������������....
  
begin
  tDatum42.dX:= 23.9;
  tDatum42.dY:= -141.3;
  tDatum42.dZ:= -80.9;
  tDatum42.wX:= 0;
  tDatum42.wY:= -0.371277 / 60 / 60 / 180 * Pi;
  tDatum42.wZ:= -0.849811 / 60 / 60 / 180 * Pi;
  tDatum42.m := -0.12*Power(10, -6);

  tDatum95.dX:= 24.8;
  tDatum95.dY:= -131.24;
  tDatum95.dZ:= -82.66;
  tDatum95.wX:= 0;
  tDatum95.wY:= 0;
  tDatum95.wZ:= -0.169137/ 60 / 60 / 180 * Pi;
  tDatum95.m := -0.12*Power(10, -6);

  pDatum42:= @tDatum42;
  pDatum95:= @tDatum95;

  blPoint:= aBLPoint;
  DegToRad2(blPoint);
  //--------------------------
  if (aFrom = EK_CK_42) and (aTo = EK_CK_95) then
  begin
    GEO_GEO(blPoint.B, blPoint.L, 0, 1, 1, pDatum42, pDatum95, Result.B, Result.L, dH);

    RadToDeg2(Result);
  end else

  //--------------------------
  if (aFrom = EK_CK_95) and (aTo = EK_CK_42) then
  begin
    GEO_GEO(blPoint.B, blPoint.L, 0, 1, 1, pDatum95, pDatum42, Result.B, Result.L, dH);

    RadToDeg2(Result);
  end
end;




//-------------------------------------------------------------
function geo_BL_to_XY(aBLPoint: TBLPoint; aZone: integer=0; aElp: byte = EK_CK_42):
    TXYPoint;
//-------------------------------------------------------------
// aZone - � ����� ���� ���������
(*
    //-----------------------------------------
    function R_to_G(r: double; long: Integer; out g,m: Integer): Double;
    //-----------------------------------------
    var sign: Boolean;
    begin
      sign:=r < 0; r:=Abs(r); r:=r * 180 / System.Pi;
      g:=trunc(r); r:=(r-g)*60; m:=trunc(r); Result:=(r-m)*60;
      if long = 0 then begin if Result >= 30 then Inc(m); Result:=0 end;
      if Abs(Result-59.9) < 0.1 then begin Inc(m); Result:=0 end;
      if m = 60 then begin Inc(g); m:=0 end;
      if sign then g:=-g
    end;

    //-----------------------------------------
    function Get_BL_Zone_degree(l: double): double;
    //����� �������� ����
    //-----------------------------------------
    var g,m: Integer;   d:double;
    begin
      R_to_G(Abs(l),0, g,m);

      d:=g-(g mod 6)+3;
      Result := ABS(DegToRad(d));

      if d < 0 then Result:=-Result;
    end;
    //-----------------------------------------
*)

var lc,iZone, h,b,l,x,y: double;
begin
  Assert(aElp in [EK_UNDEFINED, EK_CK_42, EK_WGS84],'aElp in [EK_UNDEFINED, EK_KRASOVSKY42, EK_WGS84]');

//  lc:=Get_BL_Zone(L);
  DegToRad2 (aBLPoint);

  if aZone=0 then
    aZone:=geo_Get6ZoneL (aBLPoint.L);

//  begin
  //  aZone:=geo_Get6ZoneL (aBLPoint.L);

  //  lc:=Get_BL_Zone_degree(aBLPoint.L);

//                aZone:=geo_Get6ZoneL (aBLPoint.L);
  //  aZone := Trunc(lc * (180/Pi) / 6) + 1;

//  end;
  // else
   // lc:=DegToRad(aZone*6-3);

  lc:=DegToRad(aZone*6-3);


//  if aElp=EK_AUTODETECT then aElp:=SystemEllipsoid;
 // BLH_BLH(aBLPoint.b,aBLPoint.l,0,aElp,EK_KRASOVSKY42, nil, nil,  b,l,h);


  BL_BL_ (aBLPoint.b,aBLPoint.l,0, aElp, EK_CK_42, b,l,h);

  BL_to_XY_ (b,l, lc,0,0, aElp,1, x,y);

  iZone := Trunc(lc * (180/Pi) / 6) + 1;
  Y:= iZone*1000*1000 + Y + 500000;

  Result.x:=x;
  Result.y:=y;
 //  Result.Zone:=aZone;
end;



//-------------------------------------------------------------
function geo_XY_to_BL(aXYPoint: TXYPoint; aZone: integer=0; aFromElp: byte =
    EK_CK_42; aToElp: Integer = EK_CK_42): TBLPoint;
//-------------------------------------------------------------
var lc,h,  b1,l1, b2,l2: double;
  iOldZone: integer;
  xy: TXYPoint;
  d: Double;
begin
  Assert(aFromElp in [EK_UNDEFINED, EK_CK_42, EK_WGS84],
          'aFromElp in [EK_UNDEFINED, EK_KRASOVSKY42, EK_WGS84]');

  xy:=aXYPoint;
//  Get_XY_Zone
  iOldZone:=Trunc(xy.Y) div (1000*1000);

  if aZone>0 then d:=geo_GetCenterMeridianBL (aZone)
             else d:=geo_GetCenterMeridianBL (iOldZone);

  lc:=DegToRad (d);

//  lc:=Get_XY_Zone (xy.Y);
  xy.Y:=Frac (xy.Y/1000000)*1000000 - 500000;  //frac-������� �����

  if (aZone>0) then
    xy.Y:=xy.Y + (iOldZone-aZone)*(1000*1000);

  //convert tp Krasovsky 42

 // XY_to_BL_ (xy.X,xy.Y, lc,0,0, 0,EK_KRASOVSKY42, b1,l1);

  XY_to_BL_ (xy.X, xy.Y, lc,0,0,  aFromElp,0, b1, l1);
 // XY_to_BL_ (xy.X,xy.Y, lc,0,0, 0,EK_KRASOVSKY42, b1,l1);

  //from Krasovsky to another system
 BL_BL_ (b1,l1, 0, aFromElp,aToElp,  b2,l2,h);

  ////////BLH_BLH(b1,l1,0,aFromElp,aToElp, nil, nil,  b2,l2,h);


  BL_BL_ (b1,l1, 0, 0,aToElp,  b2,l2,h);

  Result.B:=b2;
  Result.L:=l2;

  RadToDeg2 (Result);
end;

//-------------------------------------------------------------
function geo_BL_to_BL(aBLPoint: TBLPoint; aFrom,aTo: byte): TBLPoint;
//-------------------------------------------------------------
var
  b_,l_,h: double;
begin
  if aFrom=aTo then
  begin
    Result := aBLPoint;
    Exit;
  end;

  DegToRad2 (aBLPoint);  

  try
    BL_BL_ (aBLPoint.b,aBLPoint.l,  0, aFrom,aTo, b_,l_,h);

  //  BLH_BLH(aBLPoint.b,aBLPoint.l,0,aFrom,aTo, nil, nil,  b_,l_,h);


{  procedure BLH_BLH(b,l,h: double; elp,elp_: Integer;
                    v,v_: Pointer; out b_,l_,h_: double);
}
  except end;

  Result.B:=b_;
  Result.L:=l_;

  RadToDeg2 (Result);
end;


function geo_Pulkovo42_to_WGS84(aBLPoint: TBLPoint): TBLPoint;
begin
  Result := geo_BL_to_BL (aBLPoint, EK_CK_42, EK_WGS84);
end;


function geo_WGS84_to_Pulkovo42(aBLPoint: TBLPoint): TBLPoint;
begin
  Result := geo_BL_to_BL (aBLPoint, EK_WGS84, EK_CK_42);
end;


//
//procedure geo_Pulkovo42_to_WGS84(var aLat,aLon: double);
//begin
//  Result := geo_BL_to_BL (aBLPoint, EK_CK_42, EK_WGS84);
//
//  // TODO -cMM: geo_Pulkovo42_to_WGS84 default body inserted
//end;
//


end.
