unit u_xml_new;

interface

uses

  
  System.SysUtils, System.Variants, 
  
  
//  Xml.XMLIntf,
//  Xml.XMLDoc, Vcl.DBGrids, Data.DB, dxmdaset, Xml.xmldom, Vcl.Grids,

  Xml.XMLIntf;


  
function xml_GetNodeByPath(aNode: IXMLNode; aPathArr: array of string):  IXMLNode;


implementation




//--------------------------------------------------------
function Eq(aValue1, aValue2: string): boolean;
//--------------------------------------------------------
begin
  Result := AnsiCompareText(Trim(aValue1),Trim(aValue2))=0;
end;


//-----------------------------------------------------------
function xml_GetNodeByPath(aNode: IXMLNode; aPathArr: array of string):
    IXMLNode;
//-----------------------------------------------------------
var i,j,iCOunt:integer;   
   vElement: IXMLNode;   strArr: array of string;
  sTag: string;
begin
  Result:=nil;

  if VarIsNull(aNode) then
    Exit;

//  iCOunt:=aNode.childNodes.Count;

 // if High(aPathArr)>=0 then

  for i:=0 to aNode.childNodes.Count-1 do
  begin
    vElement:=aNode.childNodes[i];

//    if not xml_IsElement(vElement) then
//      Continue;

    sTag:=vElement.LocalName;

    if sTag='' then Continue;

    if Eq(sTag ,aPathArr[0]) then
      if High(aPathArr)=0
       then begin 
         Result:=vElement; 
         Exit; 
       end
       else begin
         SetLength(strArr, High(aPathArr));
         for j:=0 to High(aPathArr)-1 do 
           strArr[j]:=aPathArr[j+1];

         Result:=xml_GetNodeByPath (vElement, strArr);
       end;
  end;


end;



end.



{




//-----------------------------------------------------------
function xml_GetNodeByPath (aNode: IXMLNode; aPathArr: array of string): IXMLNode;
//-----------------------------------------------------------
var i,j,iCOunt:integer;   vElement: IXMLNode;   strArr: array of string;
  sTag: string;
begin
  Result:=nil;

  if VarIsNull(aNode) then
    Exit;

  iCOunt:=aNode.childNodes.Count;

  if High(aPathArr)>=0 then

  for i:=0 to aNode.childNodes.Count-1 do
  begin
    vElement:=aNode.childNodes[i];

//    if not xml_IsElement(vElement) then
//      Continue;

    sTag:=vElement.LocalName;

    if sTag='' then Continue;

    if Eq(sTag ,aPathArr[0]) then
      if High(aPathArr)=0
       then begin Result:=vElement; Exit; end
       else begin
         SetLength(strArr, High(aPathArr));
         for j:=0 to High(aPathArr)-1 do strArr[j]:=aPathArr[j+1];

         Result:=xml_GetNodeByPath (vElement, strArr);
       end;
  end;


end;
