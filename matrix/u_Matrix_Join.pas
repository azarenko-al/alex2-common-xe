unit u_Matrix_Join;

//==================================================
interface
//==================================================
uses Windows,Extctrls,Math,Graphics,
  u_GEO,
  u_progress,
  u_Matrix,
  u_func;

  // ����������, ��������� �� ����� ������ ��������
type
  //-------------------------------------------------------
  TMatrixJoin = class(TProgress)
  //-------------------------------------------------------
  public
    Params: record
      Sense: double; //���������������� ���������:

      Zone6 : Integer;

      CI_requirement: Integer;

      IsPo_to_BS    : boolean;


      {
      KgrMatrixList: TkgrMatrixList;
      KupMatrixList: TKupMatrixList;
      KupMatrix:     TkupMatrix;
      KgrMatrix:     TkgrMatrix;
      }
    end;



      procedure JoinKgrMatrixListDual (aCRO : double;
                       aKupMatrixList: TKupMatrixList;
                       aKgrMatrixList: TkgrMatrixList;
                       aKgrMatrix:     TkgrMatrix);

      function JoinKupMatrixListDual (//aImageMatrix : TByteMatrix;
                       aKupMatrixList: TKupMatrixList;
                       aKupMatrix:     TkupMatrix;
                       aKgrMatrix:     TkgrMatrix=nil): Boolean;

    procedure JoinMatrixListByPriority (
                                aKgrMatrixList: TkgrMatrixList;
                                aKgrMatrix:     TkgrMatrix);

    function JoinKupMatrixList1 (aKupMatrixList: TKupMatrixList;
                                aKupMatrix:     TkupMatrix;
                                aKgrMatrix:     TkgrMatrix=nil): boolean;


      function JoinKupMatrixList (//aImageMatrix : TByteMatrix;
                       aKupMatrixList: TKupMatrixList;
                       aKupMatrix:     TkupMatrix;
                       aKgrMatrix:     TkgrMatrix=nil): Boolean;
  end;


//======================================================
implementation

uses u_Matrix_base;
//======================================================



//------------------------------------------------------------------------------
procedure TMatrixJoin.JoinKgrMatrixListDual (aCRO : double;
                       aKupMatrixList: TKupMatrixList;
                       aKgrMatrixList: TkgrMatrixList;
                       aKgrMatrix:     TkgrMatrix);
//------------------------------------------------------------------------------
var
  i,c,r, FBlankValue, dest_value, iMaxCells, kup_value0, kup_value1: integer; // ��� KGR - ��� ������� � ���� ���
  x,y: double;
begin
  with aKgrMatrixList do
  begin
    if Count=0 then
       Exit;

    iMaxCells:= aKgrMatrix.RowCount * aKgrMatrix.ColCount;

    FBlankValue:= aKgrMatrix.BlankValue;
    aKgrMatrix.ZoneNum:= Items[0].ZoneNum;
    // �������� ������ ������

    for r:=0 to aKgrMatrix.RowCount-1 do
    begin
      for c:=0 to aKgrMatrix.ColCount-1 do
      begin
        // ����� �����. ����� ������
        x:= aKgrMatrix.TopLeft.X - (r+1/2)* aKgrMatrix.step;
        y:= aKgrMatrix.TopLeft.Y + (c+1/2)* aKgrMatrix.step;

        dest_value:=FBlankValue;  // �������� ������ � �������� �������

        if akupMatrixList.Count>1 then
        begin
          aKupMatrixList.Items[0].FindPointXY (x,y, kup_value0);  // 1800
          aKupMatrixList.Items[1].FindPointXY (x,y, kup_value1);  // 900

          if (kup_value0 <> aKupMatrixList.Items[0].BlankValue) and
             (kup_value1 <> aKupMatrixList.Items[1].BlankValue)
          then begin
            if (kup_value1 > kup_value0 + aCRO) then
              Items[1].FindPointXY (x,y, dest_value)
            else
              Items[0].FindPointXY (x,y, dest_value);
          end;
        end;

        if (dest_value = FBlankValue) {and (Count > 2)} then
          for i:=0 to Count-1 do
          begin
            if Items[i].FindPointXY(x,y, dest_value) then
               Break; // ������ ��������
          end;

        if dest_value < 0 then  // CellID ������������� - ������ ��� ���� ����������������
          dest_value:= FBlankValue;

        aKgrMatrix[r,c]:=dest_value;
      end;

      DoProgress(r*aKgrMatrix.ColCount, iMaxCells);
    end;
  end;

end;


//------------------------------------------------------------------------------
function TMatrixJoin.JoinKupMatrixListDual (//aImageMatrix : TByteMatrix;
                       aKupMatrixList: TKupMatrixList;
                       aKupMatrix:     TkupMatrix;
                       aKgrMatrix:     TkgrMatrix=nil): boolean;
//------------------------------------------------------------------------------
var
  i,c,r, iCount, iMaxCells: integer; // ��� KGR - ��� ������� � ���� ���
begin
  Result:=false;

  //if aKupMatrixList.Count < 2 then
  //   Exit;

 // assert(aImageMatrix<>nil);


  iMaxCells:= aKupMatrix.RowCount*aKupMatrix.ColCount;
  iCount:=0;

  aKupMatrix.FillBlank;

//  for r:=0 to aImageMatrix.RowCount-1 do
  for r:=0 to aKupMatrix.RowCount-1 do
  begin
  //  for c:=0 to aImageMatrix.ColCount-1 do
    for c:=0 to aKupMatrix.ColCount-1 do
  //  if aImageMatrix[r,c]=1 then
    begin
      aKupMatrix[r,c]:= aKupMatrixList[0].Items[r,c];

    //  if aKupMatrixList.Count>1 then
         for i:=1 to aKupMatrixList.Count-1 do
         begin
           if (aKupMatrix[r,c]=aKupMatrix.BlankValue) and (aKupMatrixList[i].Items[r,c]<>aKupMatrix.BlankValue) then
              aKupMatrix[r,c]:= aKupMatrixList[i].Items[r,c] else

           if (aKupMatrixList[i].Items[r,c]> aKupMatrix[r,c]) and (aKupMatrixList[i].Items[r,c]<>aKupMatrix.BlankValue) then
              aKupMatrix[r,c]:= aKupMatrixList[i].Items[r,c];
         end;

      inc(iCount);
    end;

    DoProgress(iCount,iMaxCells);
  end;

  Result:=True;
end;




//------------------------------------------------------
procedure TMatrixJoin.JoinMatrixListByPriority (
                       aKgrMatrixList: TkgrMatrixList;
                       aKgrMatrix:     TkgrMatrix);
//------------------------------------------------------
var i,ind,c,r, iValue, FListBlankValue, FBlankValue, dest_value,
    iMaxCells,kup_value: integer; // ��� KGR - ��� ������� � ���� ���
    x,y: double;
begin
  assert(Params.Zone6<>0);


  with aKgrMatrixList do
  begin
    if Count=0 then
      Exit;

    iMaxCells:=aKgrMatrix.RowCount * aKgrMatrix.ColCount;

    FBlankValue:=aKgrMatrix.BlankValue;
//    aKgrMatrix.ZoneNum:=Items[0].ZoneNum;
    aKgrMatrix.ZoneNum:=Params.Zone6;
    // �������� ������ ������

    for r:=0 to aKgrMatrix.RowCount-1 do
    begin
      for c:=0 to aKgrMatrix.ColCount-1 do
      begin
        // ����� �����. ����� ������
        x:=aKgrMatrix.TopLeft.X - (r+1/2)* aKgrMatrix.step;
        y:=aKgrMatrix.TopLeft.Y + (c+1/2)* aKgrMatrix.step;

        dest_value:=FBlankValue;  // �������� ������ � �������� �������

        for i:=0 to Count-1 do
        begin
          if Items[i].FindPointXY (x,y, dest_value) then
            Break; // ������ ��������
        end;

        aKgrMatrix[r,c]:=dest_value;
      end;

      DoProgress(r*aKgrMatrix.ColCount, iMaxCells);
    end;
  end;

end;
       {

//------------------------------------------------------------------------------
function TMatrixJoin.JoinKupMatrixListDual (aImageMatrix : TByteMatrix;
                       aKupMatrixList: TKupMatrixList;
                       aKupMatrix:     TkupMatrix;
                       aKgrMatrix:     TkgrMatrix=nil): boolean;
//------------------------------------------------------------------------------
var
  i,c,r, iCount, iMaxCells: integer; // ��� KGR - ��� ������� � ���� ���
begin
  Result:=false;

  //if aKupMatrixList.Count < 2 then
  //   Exit;

  iMaxCells:= aKupMatrix.RowCount*aKupMatrix.ColCount;
  iCount:=0;

  aKupMatrix.FillBlank;

  for r:=0 to aImageMatrix.RowCount-1 do
  begin
    for c:=0 to aImageMatrix.ColCount-1 do
  
    if aImageMatrix[r,c]=1 then
    begin
      aKupMatrix[r,c]:= aKupMatrixList[0].Items[r,c];
  
      if aKupMatrixList.Count>1 then
         for i:=1 to aKupMatrixList.Count-1 do
         begin
           if (aKupMatrix[r,c]=aKupMatrix.BlankValue) and (aKupMatrixList[i].Items[r,c]<>aKupMatrix.BlankValue) then
              aKupMatrix[r,c]:= aKupMatrixList[i].Items[r,c] else

           if (aKupMatrixList[i].Items[r,c]> aKupMatrix[r,c]) and (aKupMatrixList[i].Items[r,c]<>aKupMatrix.BlankValue) then
              aKupMatrix[r,c]:= aKupMatrixList[i].Items[r,c];
         end;

      inc(iCount);
    end;

    DoProgress(iCount,iMaxCells);
  end;

  Result:=True;
end;


  }

//------------------------------------------------------------------------------
function TMatrixJoin.JoinKupMatrixList (//aImageMatrix : TByteMatrix;
                       aKupMatrixList: TKupMatrixList;
                       aKupMatrix:     TkupMatrix;
                       aKgrMatrix:     TkgrMatrix=nil): boolean;
//------------------------------------------------------------------------------
var
  i,ind,c,r, iValue, iZone,
  FkupBlankValue, FkgrBlankValue,
  iMaxCells, iBestSectorID, iBestSectorInd,
  iBestKupValue, iKupValue: integer; // ��� KGR - ��� ������� � ���� ���

  dRadius_m, fDist1_m, fDist2_m, x,y: double;
  bIsServing: boolean;
begin
  Result:=false;
  aKupMatrix.FillBlank;

  with aKupMatrixList do
  begin
    if Count=0 then
       Exit;

    FkupBlankValue:= aKupMatrix.BlankValue;
    FkgrBlankValue:= High(Integer);

    iZone:=Items[0].ZoneNum;
    aKupMatrix.ZoneNum:=iZone;

    if aKgrMatrix<>nil then
       aKgrMatrix.ZoneNum:=iZone;

    // �������� ������ ������

    for r:=0 to aKupMatrix.RowCount-1 do
    for c:=0 to aKupMatrix.ColCount-1 do


//    for r:=0 to aImageMatrix.RowCount-1 do
  //    for c:=0 to aImageMatrix.ColCount-1 do
      begin
    //    if aImageMatrix[r,c] = 1 then
//        begin
          // ����� �����. ����� ������
          with aKupMatrix do begin
            x:= TopLeft.X - (r+1/2)* Step;
            y:= TopLeft.Y + (c+1/2)* Step;
          end;

          iBestKupValue:=FkupBlankValue;  // �������� ������ � �������� �������
          iBestSectorID:=FkgrBlankValue;
          iBestSectorInd:=0;
          bIsServing:=false;

          for i:=0 to Count-1 do
          begin
            if not Items[i].FindPointXY(x,y, iKupValue) then
               Continue; // ������ ��������

            // ���� ����������� ������� �� �� � �� - ������� ���������������� ��
            if (Params.IsPo_to_BS) then
              Params.Sense:=Items[i].Sensitivity;

            if Items[i].IsNoiseBS then
              iKupValue:= iKupValue + Params.CI_requirement;

            // ���� �� �� �������� ���������� �����
///            if (not Items[i].IsNoiseBS) then
///              bIsServing:= (Params.Sense=0) or (iKupValue >= Params.Sense) or bIsServing;

            // ������ ��������
            //--------------------------------------------------------
            if (iBestKupValue=FkupBlankValue) then
            //--------------------------------------------------------
            begin
              fDist1_m:= Trunc(geo_DistanceXY(MakeXYPoint(x,y), Items[i].Site_XYPoint));

              dRadius_m:= Items[i].CalcRadius_m;

              if fDist1_m<=Items[i].CalcRadius_m then
              begin
                //��� ��������� �� iKupValue �����������,
                //� ID � ind �� ����������� ��� ���������� ���������� ����
                //� ���� ������������ ��������� ��
                iBestKupValue:=iKupValue;

                if not Items[i].IsNoiseBS then
                begin
                  iBestSectorID:=Items[i].SectorID;
                  iBestSectorInd:=i;
                  bIsServing:= (iKupValue >= Params.Sense);
                end;
              end;
            end else

            //----------------------------------------------
            if iKupValue<>FkupBlankValue then
            //----------------------------------------------
            begin
              //���� � ����� ����� ���������� ������
              // - ����� ��������� ������
              if (iKupValue=iBestKupValue) then
              begin
                fDist1_m:= Trunc(geo_DistanceXY(MakeXYPoint(x,y), Items[i].Site_XYPoint));
                fDist2_m:= Trunc(geo_DistanceXY(MakeXYPoint(x,y), Items[iBestSectorInd].Site_XYPoint));

                if (fDist1_m < fDist2_m) and (fDist1_m<=Items[i].CalcRadius_m) then
                begin
                // ���� ��������� �� ����� - iKupValue �����������,
                //� ID � ind �� ����������� (� ���������� bIsServing)
                //��� ������� ������ �������� ��� "����������" (��-�� �����).
                //�������� "��������" ����� ��� ���������� ������� �������
                  iBestKupValue:=iKupValue;

                  if not Items[i].IsNoiseBS then
                  begin
                    iBestSectorID:=Items[i].SectorID;
                    iBestSectorInd:=i;
                    bIsServing:= (iKupValue >= Params.Sense) or (bIsServing);
                  end else
                    bIsServing:= False;

                end;

              end else
              //� ����� ����� ���� ������ �� ������ �������
              begin
                fDist1_m:= Trunc(geo_DistanceXY(MakeXYPoint(x,y), Items[i].Site_XYPoint));

{ TODO : ����� �� ��� ��������� �� ����� �������� (fDist1_m<=Items[i].CalcRadius)? }
                if (iKupValue>iBestKupValue) and (fDist1_m<=Items[i].CalcRadius_m) then
                begin
                //��� ��������� �� iKupValue �����������,
                //� ID � ind �� ����������� (� ���������� bIsServing)
                //��� ������� ������ �������� ��� "����������" (��-�� �����).
                //�������� "��������" ����� ��� ���������� ������� �������
                  iBestKupValue:=iKupValue;

                  if not Items[i].IsNoiseBS then
                  begin
                    iBestSectorID:=Items[i].SectorID;
                    iBestSectorInd:=i;
                    bIsServing:= (iKupValue >= Params.Sense) or (bIsServing);
                  end else
                    bIsServing:= False;

                end;
              end;
            end;

          end;  // End of <for i:=0 to Count-1 do>
          bIsServing:= (Params.Sense=0) or (bIsServing);

          if bIsServing then //������ �������� , ���� ������ ����������������
            aKupMatrix[r,c]:=iBestKupValue
          else
           ;
          //  if (iBestSectorID>0) and (iBestSectorID<>FkgrBlankValue) then
           //   iBestSectorID:=-iBestSectorID;
       //..//   if iBestKupValue<>255 then
       //      iBestKupValue:=255;

          if aKgrMatrix<>nil then
             aKgrMatrix[r,c]:= iBestSectorID;
        end;

        if (c = 0) then
           DoProgress(r*aKupMatrix.ColCount, aKupMatrix.RowCount*aKupMatrix.ColCount);
  
        if Terminated then
           Exit;
     // end;
  end;

  Result:=True;
end;



//------------------------------------------------------
function TMatrixJoin.JoinKupMatrixList1 (
                       aKupMatrixList: TKupMatrixList;
                       aKupMatrix:     TkupMatrix;
                       aKgrMatrix:     TkgrMatrix=nil): boolean;
//------------------------------------------------------
var i,ind,c,r, iValue,
    FkupBlankValue, FkgrBlankValue,
    iMaxCells,iBestSectorID, iBestSectorInd,
   // iZone,
    iBestKupValue,iKupValue: integer; // ��� KGR - ��� ������� � ���� ���

    fDist1,fDist2, x,y: double;

//label
 // label_EXIT;

begin
  Result:=false;

 // Assert(Params.Sense<0, 'Value <=0');
  Assert(Params.Zone6<>0, 'Value <=0');

  with aKupMatrixList do
  begin
    if Count=0 then
      Exit;

    iMaxCells:=aKupMatrix.RowCount * aKupMatrix.ColCount;

    FkupBlankValue :=aKupMatrix.NullValue;// High(Byte);
    FkgrBlankValue :=High(Integer);

   // iZone:=Items[0].ZoneNum;
    aKupMatrix.ZoneNum:=Params.Zone6;

    if aKgrMatrix<>nil then
      aKgrMatrix.ZoneNum:=Params.Zone6;

    // �������� ������ ������

    for r:=0 to aKupMatrix.RowCount-1 do
    for c:=0 to aKupMatrix.ColCount-1 do
    begin
      // ����� �����. ����� ������
      with aKupMatrix do begin
        x:=TopLeft.X - (r+1/2)* Step;
        y:=TopLeft.Y + (c+1/2)* Step;
      end;

      iBestKupValue:=FkupBlankValue;  // �������� ������ � �������� �������
      iBestSectorID:=FkgrBlankValue;
      iBestSectorInd:=0;

      for i:=0 to Count-1 do
      begin
        if not Items[i].FindPointXY (x,y, iKupValue) then
          Continue; // ������ ��������

        // ���� ��� ���������������� ���������
//         if (Params.Sense<>0) and (iKupValue < Params.Sense) then
         if (Params.Sense<>0) then
           if (iKupValue < Params.Sense) then
             Continue;

        // ������ ��������
        //----------------------------
        if (iBestKupValue=FkupBlankValue) then
        begin
        { TODO : optimise }
        
          iBestKupValue:=iKupValue;
          iBestSectorID:=Items[i].SectorID;
          iBestSectorInd:=i;
        end else

        //----------------------------
        if iKupValue<>FkupBlankValue then
        begin
          //���� � ����� ����� ���������� ������
          // - ����� ��������� ������
          if (iKupValue=iBestKupValue) then
          begin
            fDist1:=Trunc(geo_DistanceXY (MakeXYPoint(x,y), Items[i].Site_XYPoint));
            fDist2:=Trunc(geo_DistanceXY (MakeXYPoint(x,y), Items[iBestSectorInd].Site_XYPoint));
          end;

          if ((iKupValue=iBestKupValue) and (fDist1 > fDist2)) or
             ((iKupValue>iBestKupValue)) then
          begin
            iBestKupValue:=iKupValue;
            iBestSectorID:=Items[i].SectorID;
            iBestSectorInd:=i;
          end;
        end;

      end;

      aKupMatrix[r,c]:=iBestKupValue;

      if aKgrMatrix<>nil then
        aKgrMatrix[r,c]:=iBestSectorID;


      if (c = 0) then
        DoProgress (r*aKupMatrix.ColCount, iMaxCells);

      if Terminated then
        Exit;
    end;
  end;

  Result:=True;
end;


end.
