unit Unit5;

interface

type
  TCoverageMatrix = class(TObject)
  private
    FMemoryStream : TMemoryStream;
    function GetItem(aRow, aCol: integer): Integer;


  public
    RowCount: integer;
    ColCount: integer;

    TopLeft_X : integer; // top left corner in meters
    TopLeft_Y : integer; // top left corner in meters

    Step     : integer;
    ZoneNum  : integer;

    NullValue: integer; // value for NULL element

    constructor Create;
    destructor Destroy; override;

    function LoadFromFile (aFileName: string): boolean;

    function GetItem(aRow, aCol: integer): Integer;
  end;

implementation

constructor TCoverageMatrix.Create;
begin
  inherited;
  FMemoryStream:=TMemoryStream.Create;

  NullValue:=1;
end;


destructor TCoverageMatrix.Destroy;
begin
  FMemoryStream.Free;

  inherited Destroy;
end;


//------------------------------------------------------
function TCoverageMatrix.LoadFromFile (aFileName: string): boolean;
//------------------------------------------------------
var
  oIni: TMemIniFile;
begin
  Result:=False;

  if not FileExists(aFileName) then
  begin
    raise Exception.Create('');

    Exit;
  end;

  oIni:=TMemIniFile.Create (aFileName);

  with oIni do
  begin
   TopLeft.X := ReadInteger (INI_GENERAL, 'StartX',   0);
   TopLeft.Y := ReadInteger (INI_GENERAL, 'StartY',   0);
   Step      := ReadInteger (INI_GENERAL, 'Step',     0);
   ZoneNum   := ReadInteger (INI_GENERAL, 'ZoneNum', 0);

//     NotNullCellCount := ReadInteger (INI_GENERAL, 'NotNullCellCount',  0);

   RowCount  := ReadInteger (INI_GENERAL, 'RowCount', 0);
   ColCount  := ReadInteger (INI_GENERAL, 'ColCount', 0);
  end;

  oIni.Free;


  FMemoryStream.LoadFromFile(sBinFileName+ '.bin');


  Result:=True;

end;


//-------------------------------------------------
function TCoverageMatrix.GetItem(aRow, aCol: integer): Integer;
//-------------------------------------------------
var
  src: Pointer;
  iOffs, iTmpOffs : longint;
 // an : TAccessMode;
begin
  Assert(FRecordLen<>0);

  if not ((aRow>=0) and (aCol>=0) and
          (aRow<RowCount) and (aCol<ColCount)) then
  begin
    Result := true;  Exit;
  end;

  if FOpenMode = mtInMemory then
  begin
    src := Pointer(Longint(FMemoryStream.Memory) + (ColCount*aRow + aCol) * FRecordLen);
    Move (src^, aBuffer^, FRecordLen);
    Result:=true;
  end else

  if FOpenMode = mtAsVirtFile then
  begin
    Result := FVirtualFile.ReadRecord (ColCount*aRow + aCol, aBuffer);
  end;


//  Result:=true;
end;




end.
