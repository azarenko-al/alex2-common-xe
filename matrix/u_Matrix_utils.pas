unit u_Matrix_utils;

//==================================================
interface
//==================================================
uses Windows,Extctrls,Math,Graphics,SysUtils,

  u_GEO_convert,
  u_GEO,

  u_img,
  u_files,
  u_Progress,
  u_Matrix,
  u_func;

  // ����������, ��������� �� ����� ������ ��������
type
  TColorRangesRec = record
                      Items: array of record
                               MinValue,MaxValue: integer;
                               Color: integer;
                             end;
                      BlankColor: integer;
                    end;

  //-------------------------------------------------------
  TMatrixTools = class(TProgress)
  //-------------------------------------------------------
  public
    Params: record
      Sense: integer; //���������������� ���������:
    end;

    class procedure SaveToMifRaster (aMatrix: TOrdinalMatrix;
                               aFileName: string);

    class procedure SaveHeaderToMIF (aMatrix: TOrdinalMatrix;
                                    aFileName,aBitmapFileName: string);
   {
    function JoinKupMatrixList (
                           aKupMatrixList: TKupMatrixList;
                           aKupMatrix:     TkupMatrix
                           ): boolean;
    }

    procedure SaveToBitmap      (aMatrix: TOrdinalMatrix;
                                 aBitmap: TBitmap;
                                 aColorRanges: TColorRangesRec);

{    procedure CutMatrixByBLPoly (aMatrix: TOrdinalMatrix;
                                 aBLPoints: TBLPointArray);

}

//   procedure CutMatrixByBLRect (aMatrix: TOrdinalMatrix;
  ///                               aBLRect: TBLRect);

{    procedure MakeMaskMatrixByBLPolygon (
                                 aSrcMatrix: TOrdinalMatrix;
                                 aMaskMatrix: TOrdinalMatrix;
                                 aBLPoints: TBLPointArray);
}
  end;


  //-------------------------------------------------
  TMatrixToBitmapMaker = class
  //-------------------------------------------------
  private
    function GetColorByValue (aValue: integer): integer;
  public
    Matrix      : TOrdinalMatrix;
    BmpFileName : string;
    BlankColor  : integer;
    ColorLevels : array of record
                    Value,Color: integer;
                  end;
    procedure Execute();
  end;




//==================================================
implementation
//==================================================

//------------------------------------------------------
procedure TMatrixTools.SaveToBitmap (
              aMatrix: TOrdinalMatrix;
              aBitmap: TBitmap;
              aColorRanges: TColorRangesRec);
//------------------------------------------------------
var r,c,iValue,i,iColor: integer;
begin
  with aMatrix do
  begin
    aBitmap.Width :=ColCount;
    aBitmap.Height:=RowCount;

    for r:=0 to RowCount-1 do
    for c:=0 to ColCount-1 do begin
      iValue:=Items[r,c];

      iColor:=aColorRanges.BlankColor;

      if iValue<>BlankValue then
        for i:=0 to High(aColorRanges.Items) do
          with aColorRanges.Items[i] do
            if (MinValue <= iValue) and (iValue < MaxValue) then
              begin iColor:=Color; Break; end;

       aBitmap.Canvas.Pixels[c,r]:=iColor; //clWhite; //iColor;
    end;
  end;
end;




//------------------------------------------------------
class procedure TMatrixTools.SaveToMifRaster (aMatrix: TOrdinalMatrix;
                                          aFileName: string);
//------------------------------------------------------
var bmp: TBitmap;  sGifFileName: string;  r,c: integer;
begin
//
//  sGifFileName:=ChangeFileExt(aFileName,'.bmp');
//
//  with aMatrix do begin
//    bmp:=TBitmap.Create;
//    bmp.Width :=ColCount;
//    bmp.Height:=RowCount;
//
//    for r:=0 to RowCount-1 do
//    for c:=0 to ColCount-1 do begin
//       bmp.Canvas.Pixels[c,r]:=Items[r,c];
//    end;
//
//    img_SaveBitmapToFile (bmp, sGifFileName);
//    SaveHeaderToMIF (aMatrix, aFileName, sGifFileName);
//
//    bmp.Free;
//  end;

end;


//------------------------------------------------------
class procedure TMatrixTools.SaveHeaderToMIF (
                   aMatrix: TOrdinalMatrix;
                   aFileName,aBitmapFileName: string);
//------------------------------------------------------
const MIF_RASTER =
    '!table'       + CRLF +
    '!version 300' + CRLF +
    '!charset WindowsCyrillic' + CRLF + CRLF +
    'Definition Table   '+ CRLF +
    '  File ":FileName" '+ CRLF +
    '  Type "RASTER"    '+ CRLF +
    ':Points'+
  //  '  CoordSys NonEarth Units "m" ' + CRLF +
  //  '  Units "m"';

    '  CoordSys Earth Projection 1, 1001 '+ CRLF +
    '  Units "degree" '
     + CRLF +
    '  RasterStyle 4 1';

var sContent,sPoints,sMifFileName: string;

  xyPoints : array[0..3] of TXYPoint;
  blPoints : array[0..3] of TBLPoint;
  bmpPoints: array[0..3] of  record x,y: integer end;
  i: integer;
begin
  // we get center points of cells

  with aMatrix do begin
    // top left point
    bmpPoints[0].x:=0;   bmpPoints[0].y:=0;
    xyPoints[0].X:=TopLeft.X {- Step/2};
    xyPoints[0].Y:=TopLeft.Y {+ Step/2};

    // top right corner
    bmpPoints[1].x:=ColCount-1;   bmpPoints[1].y:=0;
    xyPoints[1].X:=TopLeft.X; // - Step/2;
    xyPoints[1].Y:=TopLeft.Y {+ Step/2} + (ColCount-1)*Step;

    // bottom right corner
    bmpPoints[2].x:=ColCount-1;   bmpPoints[2].y:=RowCount-1;
    xyPoints[2].X:=TopLeft.X {- Step/2} - (RowCount-1)*Step;
    xyPoints[2].Y:=TopLeft.Y {+ Step/2} + (ColCount-1)*Step;

    // bottom left corner
    bmpPoints[3].x:=0;   bmpPoints[3].y:=RowCount-1;
    xyPoints[3].X:=TopLeft.X {- Step/2} - (RowCount-1)*Step;
    xyPoints[3].Y:=TopLeft.Y {+ Step/2};


  end;

  for i:=0 to 3 do
    blPoints[i]:=geo_XY_to_BL (xyPoints[i]);

  sPoints:='';
  for i:=0 to 3 do
    sPoints:=sPoints + Format('  (%1.10f,%1.10f) (%d,%d) Label "Pt %d"',
      [blPoints[i].L, blPoints[i].B, bmpPoints[i].x, bmpPoints[i].y, i+1])
      + IIF(i<3,',','') + CRLF;

  sContent:=MIF_RASTER;
  sContent:=ReplaceStr (sContent, ':FileName', ExtractFileName(aBitmapFileName));
  sContent:=ReplaceStr (sContent, ':Points',   sPoints);

  sMifFileName:=ChangeFileExt (aFileName, '.tab');
  
  StrToTxtFile(sContent, sMifFileName);
//  SaveStringToFile (sContent, sMifFileName);

end;




{
//------------------------------------------------------
function TMatrixTools.JoinMatrixList (
                       aKupMatrixList: TKupMatrixList;
                       aKupMatrix:     TkupMatrix;
                       aKgrMatrix:     TkgrMatrix): boolean;
//------------------------------------------------------
var i,ind,c,r, iValue, FListBlankValue, FkupBlankValue, FkgrBlankValue,
    iMaxCells,iBestSectorID,iZone,iBestKupValue,iKupValue: integer; // ��� KGR - ��� ������� � ���� ���
    x,y: double;

label
  label_EXIT;
  
begin
  Result:=false;

  //  aDestMatrix.FillBlank;
    with aKupMatrixList do
    begin
      if Count=0 then
        Exit;

      iMaxCells:=aKupMatrix.RowCount * aKupMatrix.ColCount;

      FListBlankValue:=Items[0].BlankValue; // ��� KUP � KGR ������
      FkupBlankValue :=High(Byte);
      FkgrBlankValue :=High(Integer);

      iZone:=Items[0].ZoneNum;
      aKupMatrix.ZoneNum:=iZone;

      if aKgrMatrix<>nil then
        aKgrMatrix.ZoneNum:=iZone;

      // �������� ������ ������

      for r:=0 to aKupMatrix.RowCount-1 do
      begin
        for c:=0 to aKupMatrix.ColCount-1 do
        begin
          // ����� �����. ����� ������
          with aKupMatrix do begin
            x:=TopLeft.X - (r+1/2)* Step;
            y:=TopLeft.Y + (c+1/2)* Step;
          end;

          iBestKupValue:=FkupBlankValue;  // �������� ������ � �������� �������
          iBestSectorID:=FkgrBlankValue;

          for i:=0 to Count-1 do
          begin
            if not TOrdinalMatrix(Items[i]).FindPointXY (x,y, iKupValue) then
              Continue; // ������ ��������

            // ���� ��� ���������������� ���������
            if (Params.Sense<>0) and (iKupValue < Params.Sense) then
              Continue;

            if iKupValue<>FkupBlankValue then
            if (iBestKupValue=FkupBlankValue) or (iKupValue > iBestKupValue) then
            begin
              iBestKupValue:=iKupValue;
              iBestSectorID:=Items[i].SectorID;
            end;

          end;

          aKupMatrix[r,c]:=iBestKupValue;

          if aKgrMatrix<>nil then
            aKgrMatrix[r,c]:=iBestSectorID;
        end;

        if (r mod 100) = 0 then
          DoProgress (r*aKupMatrix.ColCount, iMaxCells);

        if Terminated then
          Goto label_EXIT;

      end;
    end;

  Result:=True;

label_EXIT:

end;
}
{
//------------------------------------------------
procedure TMatrixTools.CutMatrixByBLRect (aMatrix: TOrdinalMatrix;
                             aBLRect: TBLRect);
//------------------------------------------------
var BLPoints: TBLPointArray;
begin
  blPoints:=geo_BLRectToBLPoints (aBLRect);
  CutMatrixByBLPoly (aMatrix, blPoints);
end;}
{
//------------------------------------------------
procedure TMatrixTools.CutMatrixByBLPoly (
                             aMatrix: TOrdinalMatrix;
                             aBLPoints: TBLPointArray);
//------------------------------------------------
var xyPoints: TXYPointArray;
    oMaskMatrix: TByteMatrix;
    r,c: integer;
begin
  oMaskMatrix:=TByteMatrix.Create;

  MakeMaskMatrixByBLPolygon (aMatrix, oMaskMatrix, aBLPoints);

  for c:=0 to oMaskMatrix.ColCount-1 do
  for r:=0 to oMaskMatrix.RowCount-1 do
    if oMaskMatrix[r,c] = oMaskMatrix.BlankValue then
      aMatrix[r,c] := aMatrix.BlankValue;

  oMaskMatrix.Free;
end;}

{
//------------------------------------------------
procedure TMatrixTools.CutMatrixByXYPoly (
                             aMatrix: TOrdinalMatrix;
                             aXYPoints: TXYPointArray);
//------------------------------------------------
var
  oImg: TImage;
  xyPoint: TXYPoint;
  x,y: double;
  iR,iC: integer;
  iMatrixStep,iRectRows,iRectCols: integer;
  xyRect,xyRectNew: TXYRect;
  points: array of TPoint;
  xyPointArr: TXYPointArray;
  iRow,iCol,iOffsetX,iOffsetY,iStep,iWidth,iHeight,r,c,i: integer;
begin



  xyRect:=MakeXYRect (0,0,0,0);
  iStep:=aMatrix.Step;
  iMatrixStep:=aMatrix.Step;

  if iStep=0 then
    Exit;

  xyRect:=geo_GetCircumscribedXYRect (aXYPoints);

  iWidth :=1+Round(xyRect.BottomRight.Y - xyRect.TopLeft.Y) div iStep;
  iHeight:=1+Round(xyRect.TopLeft.X - xyRect.BottomRight.x) div iStep;

  iRectCols:=Round(xyRect.BottomRight.Y - xyRect.TopLeft.Y) div iMatrixStep;
  iRectRows:=Round(xyRect.TopLeft.X - xyRect.BottomRight.x) div iMatrixStep;


  iOffsetX:=Round(xyRect.BottomRight.X);
  iOffsetY:=Round(xyRect.TopLeft.Y);

  SetLength (points, High(aXYPoints)+1);

  for i:=0 to High(aXYPoints) do begin
    points[i].x:=Round(aXYPoints[i].Y-iOffsetY) div iStep;
    points[i].y:=iHeight - Round(aXYPoints[i].X-iOffsetX) div iStep;
  end;


  oImg:=TImage.Create(nil);
  oImg.Width :=iWidth;
  oImg.Height:=iHeight;
//  oImg.Picture.Bitmap.PixelFormat:=pf8bit;

  oImg.Canvas.Brush.Color:=clWhite;
  oImg.Canvas.FillRect (oImg.BoundsRect);

  oImg.Canvas.Pen.Color:=clBlack;
  oImg.Canvas.Brush.Color:=clBlack;
  oImg.Canvas.Polygon (points);

  oImg.Picture.SaveToFile ('t:\aaa11.bmp');


  for r:=0 to iRectRows-1 do
  for c:=0 to iRectCols-1 do
  begin
    x:=xyRect.TopLeft.X - r*iMatrixStep;
    y:=xyRect.TopLeft.Y + c*iMatrixStep;

    iR:=r*iMatrixStep div iStep;
    iC:=c*iMatrixStep div iStep;


//    if (iC<iWidth) and (iR<iHeight) then
    if oImg.Canvas.Pixels[iC,iR]<>clBlack then
    begin
      if aMatrix.FindCellXY (x,y, iRow,iCol) then
        aMatrix[iRow,iCol]:=aMatrix.BlankValue;
    end;
  end;

  oImg.Free;
end;

}

{
//------------------------------------------------
procedure TMatrixTools.MakeMaskMatrixByBLPolygon (
                             aSrcMatrix: TOrdinalMatrix;
                             aMaskMatrix: TOrdinalMatrix;
                             aBLPoints: TBLPointArray);
//------------------------------------------------
var
  oImg: TImage;
  xyPoint: TXYPoint;
  x,y: double;
  iR,iC: integer;
  iMatrixStep,iRectRows,iRectCols: integer;
  xyRect,xyRectNew: TXYRect;
  points: array of TPoint;
  xyPointArr: TXYPointArray;
  xyPoints: TXYPointArray;
  iRow,iCol,iOffsetX,iOffsetY,iStep,iWidth,iHeight,r,c,i: integer;
begin
  xyRect:=MakeXYRect (0,0,0,0);
  iStep:=aSrcMatrix.Step;
  iMatrixStep:=aSrcMatrix.Step;

  if iStep=0 then
    Exit;

  xyPoints:=geo_BLPoints_to_XYPoints (aBLPoints);

  aMaskMatrix.AssignHeader (aSrcMatrix);
  aMaskMatrix.FillBlank;
//  aMaskMatrix.FillValue (0);


  // �������� ��������� �������������
  xyRect:=geo_GetCircumscribedXYRect (xyPoints);

  iWidth :=1+Round(xyRect.BottomRight.Y - xyRect.TopLeft.Y) div iStep;
  iHeight:=1+Round(xyRect.TopLeft.X - xyRect.BottomRight.x) div iStep;

  iRectCols:=Round(xyRect.BottomRight.Y - xyRect.TopLeft.Y) div iMatrixStep;
  iRectRows:=Round(xyRect.TopLeft.X - xyRect.BottomRight.x) div iMatrixStep;


  iOffsetX:=Round(xyRect.BottomRight.X);
  iOffsetY:=Round(xyRect.TopLeft.Y);

  SetLength (points, High(xyPoints)+1);

  for i:=0 to High(xyPoints) do begin
    points[i].x:=Round(xyPoints[i].Y-iOffsetY) div iStep;
    points[i].y:=iHeight - Round(xyPoints[i].X-iOffsetX) div iStep;
  end;


  oImg:=TImage.Create(nil);
  oImg.Width :=iWidth;
  oImg.Height:=iHeight;
//  oImg.Picture.Bitmap.PixelFormat:=pf8bit;

  oImg.Canvas.Brush.Color:=clWhite;
  oImg.Canvas.FillRect (oImg.BoundsRect);

  oImg.Canvas.Pen.Color:=clBlack;
  oImg.Canvas.Brush.Color:=clBlack;
  oImg.Canvas.Polygon (points);

 // oImg.Picture.SaveToFile ('t:\aaa11.bmp');


  for r:=0 to iRectRows-1 do
  for c:=0 to iRectCols-1 do
  begin
    x:=xyRect.TopLeft.X - r*iMatrixStep;
    y:=xyRect.TopLeft.Y + c*iMatrixStep;

    iR:=r*iMatrixStep div iStep;
    iC:=c*iMatrixStep div iStep;

    // ���� ���� ���������� - ������ (������ ��������)
    if oImg.Canvas.Pixels[iC,iR]=clBlack then
    begin
      if aSrcMatrix.FindCellXY (x,y, iRow,iCol) then
        aMaskMatrix[iRow,iCol]:=1; //aMaskMatrix.BlankValue;
    end;
  end;

//  aMaskMatrix.SaveToTxtFile ('t:\test.txt');

  oImg.Free;
end;
//------------------------------------------------

}

//------------------------------------------------------
// TMatrixToBitmapMaker
//------------------------------------------------------
function TMatrixToBitmapMaker.GetColorByValue (aValue: integer): integer;
//------------------------------------------------------
var i:integer;
begin
  for i:=1 to High(ColorLevels) do
    if (ColorLevels[i-1].Value <= aValue) and (aValue < ColorLevels[i].Value)
      then begin Result:=img_MakeGradientColor (aValue,
                            ColorLevels[i-1].Value, ColorLevels[i].Value,
                            ColorLevels[i-1].Color, ColorLevels[i].Color);
           Exit;  end;

  Result:=BlankColor;
end;

//------------------------------------------------------
procedure TMatrixToBitmapMaker.Execute();
//------------------------------------------------------
var bmp: TBitmap;
    r,c,iValue,iColor: integer;
begin
  bmp:=TBitmap.Create;
  bmp.Width :=Matrix.ColCount;
  bmp.Height:=Matrix.RowCount;

  for r:=0 to Matrix.RowCount-1 do
  for c:=0 to Matrix.ColCount-1 do begin
    iValue:=Matrix.Items[r,c];
    if iValue<>Matrix.BlankValue then iColor:=GetColorByValue(iValue)
                                 else iColor:=BlankColor;
    bmp.Canvas.Pixels[r,c]:=iColor;

  end;


  bmp.SaveToFile (BmpFileName);
  bmp.Free;
end;




var
  point: TXYPoint;
  polygon: TXYPointArrayF;
  bool: boolean;
  oMatrix: TIntMatrix;
  i,j: integer;

  oImg: TImage;

{
  oMatrix:=TIntMatrix.Create;
  oMatrix.SetParams (200,0,20,20,10);

  SetLength (polygon, 4);
  polygon[0].x := 190;  polygon[0].y := 0;
  polygon[1].x := 160;  polygon[1].y := 100;
  polygon[2].x := 100;  polygon[2].y := 90;
  polygon[3].x := 60;   polygon[3].y := 10;


  for i := 0 to 20-1 do
  for j := 0 to 20-1 do
  begin
    oMatrix[i,j]:=11;

    point:=oMatrix.GetCellGeoCenter (i,j);
    bool:=IsPointInPolygon (point, polygon);
    if not bool then
      oMatrix[i,j]:=0; //oMatrix.BlankValue;
  end;
 }
//  oMatrix.SaveToTxtFile ('t:\test.txt');


  {

  point.x:=-50;
  point.y:=150;

  bool:=PointInPolygon (point, polygon);
  bool:=bool;
 }

begin
{
  oImg:=TImage.Create(nil);
  oImg.Width :=iWidth;
  oImg.Height:=iHeight;
}

end.
