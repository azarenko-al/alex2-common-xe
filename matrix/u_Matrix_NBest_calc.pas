unit u_Matrix_NBest_calc;

interface

uses u_progress,
     u_Matrix,
     u_Matrix_NBest,
     u_geo,
     u_func;

type

  TMatrixNBestCalc = class(TProgress)
  private
    FMatrix: TMatrixNBest;

    procedure CreateFile;
    procedure CloseFile;

    procedure ProcessMatrix;

  public
    Params: record
      MaxRecordCount : integer;
      Sensitivity    : double;
      FileName       : string;
      TopLeft        : TXYPoint; // �����. ������ �������� ���� � ������
      RowCount,ColCount,Step     : integer;
      ZoneNum  : integer;


    end;

    KupMatrixList: TkupMatrixList;

    procedure Execute;

  end;

implementation


//-------------------------------------------------------------------
procedure TMatrixNBestCalc.CreateFile;
//-------------------------------------------------------------------
begin
  FMatrix:=TMatrixNBest.Create;

  FMatrix.TopLeft  :=Params.TopLeft;

  FMatrix.Step    :=Params.Step;
  FMatrix.RowCount:=Params.RowCount;
  FMatrix.ColCount:=Params.ColCount;
  FMatrix.ZoneNum :=Params.ZoneNum;

  FMatrix.MaxRecordCount:=Params.MaxRecordCount;

  with FMatrix do begin
//    SetLength (arrSort, RecordRowCount);
//    SetLength (file_rec.Items, RecordRowCount);

    Summary.MinEMP:=0;
    Summary.MaxEMP:=0;

  //  if KupMatrixList.Count>0 then
  //    ZoneNum:=KupMatrixList[0].ZoneNum;
  end;

  FMatrix.CreateFile(Params.FileName);

end;


procedure TMatrixNBestCalc.CloseFile;
begin

//  FMatrix.
//  FMatrix.CloseFile;
  FMatrix.SaveHeaderToFile(Params.FileName);
  FMatrix.Free;
end;


//--------------------------------------------------
// TMatrix6MaxCalc
//--------------------------------------------------
procedure TMatrixNBestCalc.Execute;
//--------------------------------------------------
begin
  Assert(Assigned(KupMatrixList));
  Assert(Params.Sensitivity<0);


  CreateFile;

  ProcessMatrix;

  CloseFile;

end;

//--------------------------------------------------
// TMatrix6MaxCalc
//--------------------------------------------------
procedure TMatrixNBestCalc.ProcessMatrix;
//--------------------------------------------------
var
   rec: TNBestRecArrayF;

//            if (MinEMP=0) or (-ABS(arrSort[i].EMP_level) < MinEMP) then MinEMP:=-ABS(arrSort[i].EMP_level);
 //z           if (MaxEMP=0) or (-ABS(arrSort[i].EMP_level) > MaxEMP) then MaxEMP:=-ABS(arrSort[i].EMP_level);


     tmpRec: TNBestRec;
   //  min_ind, max_ind: integer;
    // ������� ��� - ��� ����� (80,90,120)
    // � ������� ��������  ������������ �������� ���
    // � ���������� ������� �������

    //---------------------------------------------
    procedure DoPush(aID, aValue: integer);
    //---------------------------------------------
    var i: integer;
    begin
      if aValue=0 then
        Exit;

      if rec.RecordCount<Params.MaxRecordCount then
      begin
         Inc(rec.RecordCount);
         rec.Items[rec.RecordCount-1].Value:=aValue;
         rec.Items[rec.RecordCount-1].ID:=aID;
         Exit;
      end;


      for i:=0 to rec.RecordCount-1 do
    {  begin
        // ����� MIN � MAX �������� � �������
        // - optimize it
        if (rec.Items[i].Emp_level < aValue) then min_ind:=i;
        if (rec.Items[i].Emp_level > aValue) then max_ind:=i;
      end;
}
      // ���� �������� EMP < max -> �������� �� ����� max
      if (aValue < rec.Items[i].Value) then
      begin
        rec.Items[i].Value:=aValue;
        rec.Items[i].ID:=aID;
        Exit;
      end;
    end;

    //---------------------------------------------
    procedure DoSortArr(); // ������������� ������ �� �����������
    //---------------------------------------------
    var i: integer; isChanged: boolean;
    begin
      repeat
        isChanged:=false;

        for i:=0 to rec.RecordCount-1-1 do
        begin
        //  if rec.Items[i+1].Value=0 then
        //    Break; // �������� 2 ������ <> 0

          if (rec.Items[i].Value > rec.Items[i+1].Value)// or
          //   (rec.Items[i].Value=0) // ���� �������� 0, � ������ - �� 0
          then begin
            tmpRec:=rec.Items[i];
            rec.Items[i]:=rec.Items[i+1];
            rec.Items[i+1]:=tmpRec;
            isChanged:=true;
          end;
        end;

      until
        isChanged=false;
    end;
    //---------------------------------------------

var
  r,c,i,k,iSectorID,iValue: integer;
  x,y: double;
begin
  FillChar(rec, SizeOf(rec), 0);


  with FMatrix do
   for r:=0 to RowCount-1 do
  begin
    for c:=0 to ColCount-1 do
    begin
    //  FillChar (rec.Items, SizeOf(rec), 0);
      x:=TopLeft.X - (r+1/2)*Step;
      y:=TopLeft.Y + (c+1/2)*Step;


      rec.RecordCount := 0;
     //  for k:=0 to MaxRecordCount-1 do
    //    FillChar(arrSort[k], SizeOf(arrSort[k]), 0);

   //   min_ind:=0; max_ind:=0;

      with KupMatrixList do
        for i:=0 to Count-1 do
        begin

          if Items[i].FindPointXY (x,y, iValue) then
           if (iValue >= Params.Sensitivity) then
             begin
               iSectorID:=Items[i].SectorID;
               DoPush (iSectorID, Abs(iValue));
             end;
        end;

      // sort by EMP level
      if rec.RecordCount>0 then
      begin
        DoSortArr(); // ������������� ������
        WriteRecord (rec);
      end else
        WriteBlankRecord;

{
      for i:=0 to High(arrSort) do
        if arrSort[i].EMP_level <> 0 then
        begin
          file_rec.Items[i].iSectorID := arrSort[i].ID;
          file_rec.Items[i].Value := arrSort[i].EMP_level;

          with Summary do begin
            if (MinEMP=0) or (-ABS(arrSort[i].EMP_level) < MinEMP) then MinEMP:=-ABS(arrSort[i].EMP_level);
            if (MaxEMP=0) or (-ABS(arrSort[i].EMP_level) > MaxEMP) then MaxEMP:=-ABS(arrSort[i].EMP_level);
          end;
        end else begin
          // ���� �������� ��� ������� - �������� ID ������� = -1
          file_rec.Items[i].iSectorID := 0;
          file_rec.Items[i].Value := 255;
        end;
}

      // ��� ������ ������ (��������)
  //    Items[r,c]:=file_rec;

    end;

    if Assigned(FOnProgress) then
      FOnProgress (r, RowCount, Terminated);

    if Terminated then Break;
  end;
end;


end.
