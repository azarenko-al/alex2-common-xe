unit u_Matrix;

interface
uses Classes, SysUtils, Math, IniFiles,Graphics, FileCtrl, Dialogs,

     u_Geo,
     u_geo_convert_new,

     u_dlg,

     u_matrix_base;

type
  TMatrixProfileLine = record
                         Items: array of record
                            Value : integer; // �������� ���
                         end;

                         BlankValue       : integer;
                         Offset,Distance  : double;
                       end;


  TMatrixParamsRec =  record
    StartX   : Double;
    StartY   : double;
    RowCount : Integer;
    ColCount : integer;
    Step     : Integer;
    Zone6    : Integer;
  end;


  //-------------------------------------------------
  TOrdinalMatrix = class (TCustomLenMatrix)
  //-------------------------------------------------
  private
    function  GetValue (aRow,aCol: integer): integer; virtual;
    procedure SetValue (aRow,aCol: integer; Value:integer); virtual;
    function  GetFloatValue (aRow,aCol:integer): double; virtual;
    procedure SetFloatValue (aRow,aCol:integer; aValue:double); virtual;

    function CellToStr (ARow,ACol: integer): string; override;


    // ��������� ������� �������� ����� �������
    function BuildProfileLine (aX1,aY1,aX2,aY2: double; aStep:integer=0): TMatrixProfileLine;

  public
    NegativeValues: boolean;

    // �������� ������� ��� �����
    procedure SetParams (aRec: TMatrixParamsRec;
                         aStartX,aStartY: double;
                         aRowCount,aColCount: integer;
                         aStep:    Integer;
                         aZoneNum: integer);

    procedure SetParams1(aStartX,aStartY:double;
                         aRowCount,aColCount:integer;
                         aStep, aMaxValue:integer;
                         aFileName: string);
    procedure InitMemory ();

    procedure SetNullCell (aRow,aCol: integer); override;

    // ���������� �������� ����� �� �����������
    function FindPointXY (X,Y: double; var Value: integer): boolean;
    function FindPointBL (aBLPoint: TBLPoint; var Value: integer): boolean;


    // service functions
    procedure GetMinAndMaxValues (var AMinValue,AMaxValue: integer);
    function  GetNotNullCellCount: integer;

    procedure FillBlank;
    procedure FillValue (aValue: integer);
    procedure FillFloatValue (aValue: integer);

    procedure Clear;

    function  IsNull (aRow,aCol:integer): boolean; override;
    procedure Assign (Source: TOrdinalMatrix); virtual;
    procedure AssignHeader (Source: TOrdinalMatrix);
    procedure ApplyBoundsFromMatrix (aMatrix: TOrdinalMatrix);

    property Items[Row,Col: integer]: integer read GetValue write SetValue; default;
    property ItemsAsFloat[Row,Col: integer]: double read GetFloatValue write SetFloatValue;
  end;

  TIntMatrix = class (TOrdinalMatrix)  // ������� integer ��������
    constructor Create;
  end;

  TByteMatrix = class (TOrdinalMatrix)  // ������� byte ��������
    constructor Create;
  end;

  TShortintMatrix = class (TOrdinalMatrix)  // ������� integer ��������
    constructor Create;
  end;

  TkmoMatrix = class (TByteMatrix)  // ����� ��������
  private
    function GetKmoCellCount(): integer;
  end;

{
  TtpMatrix   = class (TOrdinalMatrix)
    constructor Create;
  end;  // �������

  TMosMatrix   = class (TOrdinalMatrix)  // �������
    constructor Create;
  end;
 }

  TkupMatrix = class (TByteMatrix)  // ����� ������ ����
  private
    function GetCellCountBySense (aSense: integer): integer;
  public
    Sensitivity: double; // ���������������� ��������� ��������
    Site_XYPoint: TXYPoint;

    Hysteresis : double;


     IsNoiseBS : boolean; // kup ��, ������� �������� ���������� �����


    constructor Create;
    function  CellHasCoverage (aRow,aCol: integer): boolean; // �������� ����� ��������
    procedure ApplyRangeFilter (aMinValue,aMaxValue: double);
  end;



  TkgrMatrix   = class (TIntMatrix);  // ����� ������ ����
  TciMatrix    = class (TShortintMatrix);   // �������
//  TciPomMatrix = class (TShortintMatrix);  // �������
  TciaMatrix   = class (TShortintMatrix);  // �������


  //-------------------------------------------------
  TMatrixList = class(TList)
  //-------------------------------------------------
  public
    //�� ������� �� ������ ��� ������� �������
    IsStoreItemsOnClear1: boolean;

    function  GetItem (Index: integer): TOrdinalMatrix;
    function  AddMatrix (aFileName: string): TOrdinalMatrix;
    procedure Clear; override;

    property  Items  [Index: integer]: TOrdinalMatrix read GetItem; default;
  end;

  //-------------------------------------------------
  TKupMatrixList = class (TMatrixList)
  //-------------------------------------------------
  private
    function GetItem (Index: integer): TkupMatrix;
  public
    function AddMatrix (aFileName: string;
                        aSensitivity: integer;
                        aSectorID: integer=0): boolean;


    // ��������� ������� �������� ����� �������
    // �� ������������ ���������
    function BuildProfileLine (aX1,aY1,aX2,aY2: double): TMatrixProfileLine;
    property Items [Index: integer]: TkupMatrix read GetItem; default;
  end;

  //-------------------------------------------------
  TKgrMatrixList = class (TMatrixList)
  //-------------------------------------------------
    function AddMatrix (aFileName: string): boolean;
  end;


  //-------------------------------------------------
  TFloatMatrix = class (TCustomLenMatrix)
  //-------------------------------------------------
  private
    FBlankValue : double;
    function  GetValue (aRow,aCol: integer): double; virtual;
    procedure SetValue (aRow,aCol: integer; Value:double); virtual;

  protected
    function CellToStr (aRow,aCol: integer): string; override;
    function  IsNull (aRow,aCol:integer): boolean; override;

  public
    constructor Create;

    procedure GetMinAndMaxValues (var AMinValue,AMaxValue: double);

    property Items [Row,Col: integer]: double read GetValue write SetValue; default;
    property BlankValue: double read FBlankValue;
  end;




 function matrix_CreateMatrixByFileName (aFileName: string): TOrdinalMatrix;




//=================================================
implementation
//=================================================

type
 TBufferRec= packed record case Byte of
               1:(AsByte     : byte);
               2:(AsShortint : shortint);
               3:(AsInteger  : integer);
               4:(AsFloat    : single);
             end;


//*************************************************
//  TFloatMatrix
//*************************************************
constructor TFloatMatrix.Create;
begin
  inherited Create;
  FRecordLen:=SizeOf(double);
  FBlankValue:=-High(Integer);
end;

procedure TFloatMatrix.SetValue (aRow,aCol:integer; Value:double);
begin
  WriteBuffer (aRow,aCol, @Value);
end;

function TFloatMatrix.GetValue (aRow,aCol:integer): double;
begin
  if not ReadBuffer (aRow,aCol, @Result) then
    Result:=BlankValue;
end;


//-------------------------------------------------
procedure TFloatMatrix.GetMinAndMaxValues (var AMinValue,AMaxValue: double);
//-------------------------------------------------
var r,c: integer;
   fValue: double;
begin // ���������� ��� � ���� �������� �������
  aMinValue:=BlankValue;
  aMaxValue:=BlankValue;

  for r:=0 to RowCount-1 do
  for c:=0 to ColCount-1 do if not IsNull(r,c) then
  begin
    fValue:=Items[r,c]; // set MIN and MAX values
    if aMinValue=BlankValue then begin aMinValue:=fValue; aMaxValue:=fValue; end else
    if fValue < aMinValue then aMinValue:=fValue else
    if fValue > aMaxValue then aMaxValue:=fValue;
  end;
end;


procedure TOrdinalMatrix.SetNullCell (aRow,aCol: integer);
begin
  Items[aRow,aCol] := FBlankValue;
end;


function TOrdinalMatrix.IsNull (aRow,aCol:integer): boolean; // NUKK value ?
begin
   if RecordType = dtFloat then Result:= (ItemsAsFloat[aRow,aCol] = FBlankValue)

                           else Result:= (Items[aRow,aCol] = FBlankValue);
end;

function TOrdinalMatrix.GetNotNullCellCount: integer;
var r,c:integer;
begin
  Result:=0;
  for r:=0 to RowCount-1 do
  for c:=0 to ColCount-1 do  if not IsNull(r,c) then Inc(Result);
end;

procedure TOrdinalMatrix.FillBlank;
begin
  if RecordType = dtFloat then
    FillFloatValue(0)
  else
    FillValue (BlankValue);
end;

procedure TOrdinalMatrix.FillValue (aValue: integer);
var r,c:integer;
begin
  for r:=0 to RowCount-1 do
  for c:=0 to ColCount-1 do
    Items[r,c]:=aValue;
end;


procedure TOrdinalMatrix.FillFloatValue (aValue: integer);
var
  r,c:integer;   
begin
  for r:=0 to RowCount-1 do
  for c:=0 to ColCount-1 do ItemsAsFloat[r,c]:=aValue;

end;



//-------------------------------------------------
function TOrdinalMatrix.GetFloatValue (ARow,ACol:integer): double;
//-------------------------------------------------
var buf: TBufferRec;
begin
  if ReadBuffer (aRow,aCol, @buf)
    then Result:=buf.AsFloat
    else Result:=FBlankValue;
end;

//-------------------------------------------------
procedure TOrdinalMatrix.SetFloatValue (ARow,ACol:integer; aValue:double);
//-------------------------------------------------
var buf: TBufferRec;
begin // �������� �� MAX ��������
//  if Abs(aValue)>FBlankValue then Value:=FBlankValue;

  buf.AsFloat := aValue;
  WriteBuffer (aRow,aCol, @buf);
end;

procedure TOrdinalMatrix.AssignHeader (Source: TOrdinalMatrix);
begin
  SetGeoParams (Source.TopLeft.X,Source.TopLeft.Y, Source.Step, Source.ZoneNum);
  inherited SetMatrixSize(Source.RowCount, Source.ColCount);
end;

//-------------------------------------------------
procedure TOrdinalMatrix.Assign (Source: TOrdinalMatrix);
//-------------------------------------------------
var r,c: integer;
begin
  SetGeoParams (Source.TopLeft.X,Source.TopLeft.Y, Source.Step, Source.ZoneNum);
  NegativeValues:=Source.NegativeValues;
//  ZoneNum       :=Source.ZoneNum;

  RecordType    :=Source.RecordType;
  if RecordType=dtCustomLen then
    RecordLen:=Source.RecordLen;

  try
    inherited SetMatrixSize(Source.RowCount, Source.ColCount);

  except // wrap up
    ErrorDlg ('SetMatrixSize(Source.RowCount, Source.ColCount) - failed');
  end;    // try/finally

  for r:=0 to RowCount-1 do
   for c:=0 to ColCount-1 do Items[r,c]:=Source[r,c];
end;

//-------------------------------------------------
function TOrdinalMatrix.GetValue (ARow,ACol: integer): integer;
//-------------------------------------------------
var buf: TBufferRec;
begin
  if ReadBuffer (aRow,aCol, @buf) then begin
    case FRecordType of dtByte    : begin
                                    Result:=buf.AsByte;
                                    if NegativeValues and (Result<>FBlankValue) then Result:=-Result;
                                  end;
                      dtShortint: Result:=buf.AsShortint;
                      dtInteger : Result:=buf.AsInteger;  end;
  end else
    Result:=FBlankValue;
end;

//-------------------------------------------------
procedure TOrdinalMatrix.SetValue (ARow,ACol:integer; Value:integer);
//-------------------------------------------------
var
  buf: TBufferRec;
begin // �������� �� MAX ��������
  try
    if Abs(Value)>FBlankValue then
      Value:=FBlankValue;

    case FRecordType of
      dtByte     : buf.AsByte    :=Byte(Abs(Value));
      dtShortint : buf.AsShortint:=Shortint(Value);
      dtInteger  : buf.AsInteger :=Integer(Value);
   //   dtFloat    : buf.AsFloat   :=Integer(Value);

    end;

    WriteBuffer (aRow,aCol, @buf);
  except
    on E: Exception do
      ShowMessage(Format('row:%d ; col:%d', [aRow,aCol]));
  end;
end;


procedure TOrdinalMatrix.Clear;
begin
  SetMatrixSize (0,0);
//  FDataPtr:=nil;
  inherited;
end;


procedure TOrdinalMatrix.InitMemory ();
begin
  SetMatrixSize (RowCount,ColCount);
end;


procedure TOrdinalMatrix.SetParams (aRec: TMatrixParamsRec;
                                    aStartX,aStartY: double;
                                    aRowCount,aColCount: integer;
                                    aStep:    Integer;
                                    aZoneNum: integer);
begin

  assert(aRec.RowCount>0);
  assert(aRec.ColCount>0);
  assert(aRec.Zone6>0);
                   

  TopLeft.X:=AStartX;
  TopLeft.Y:=AStartY;
  Step     :=AStep;
  ZoneNum  :=aZoneNum;

  SetMatrixSize(ARowCount,AColCount);
end;


procedure TOrdinalMatrix.SetParams1(aStartX,aStartY:double;
                                    aRowCount,aColCount:integer;
                                    aStep, aMaxValue:integer;
                                    aFileName: string);
var
  oFileStream : TFileStream;
  iLen : integer;
begin

  TopLeft.X:=AStartX;
  TopLeft.Y:=AStartY;
  Step  :=AStep;

  if ARowCount<0 then ARowCount:=0;
  if AColCount<0 then AColCount:=0;


  ForceDirectories(ExtractFileDir (aFileName));

  SaveHeaderToFile(aFileName, aRowCount, aColCount, aMaxValue, 0);
//  SaveHeaderToFile(aFileName);


  FileName := (aFileName+'.bin');

  if not FileExists(FileName) then
  begin
    oFileStream := TFileStream.Create(FileName, fmCreate);
    oFileStream.Size := ARowCount* AColCount* RecordLen;
    oFileStream.Free;
  end;

  iLen := Length(aFileName+'.bin');
  FVirtualFile.RecordLen1 := RecordLen;
  FVirtualFile.Open(aFileName+'.bin', fmOpenReadWrite);

  FOpenMode :=mtAsVirtFile;

end;


//------------------------------------------------------
function TOrdinalMatrix.FindPointBL (aBLPoint: TBLPoint; var Value: integer): boolean;
//------------------------------------------------------
var x,y: double;  iZone:integer;  xyPoint: TXYPoint;
begin
  xyPoint:=geo_Pulkovo42_BL_to_XY (aBLPoint,  ZoneNum); // aZone);
//  xyPoint:=geo_Pulkovo42_to_XY (aBLPoint,  ZoneNum); // aZone);

//  xyPoint:=geo_BL_to_XY (aBLPoint,  ZoneNum); // aZone);

  Result:=FindPointXY (xyPoint.x, xyPoint.y, Value);
end;

//------------------------------------------------------
function TOrdinalMatrix.FindPointXY (X,Y: double; var Value: integer): boolean;
//------------------------------------------------------
var r,c:integer;
begin
  Result:=FindCellXY (x,y, r,c);
  if Result=true then begin
    Value:=GetValue(r,c);
    Result:=(Value<>FBlankValue);
  end else
    Value :=BlankValue;
end;

//-------------------------------------------------
procedure TOrdinalMatrix.GetMinAndMaxValues (var AMinValue,AMaxValue: integer);
//-------------------------------------------------
var r,c,value: integer;
begin // ���������� ��� � ���� �������� �������
  aMinValue:=BlankValue;
  aMaxValue:=BlankValue;

  for r:=0 to RowCount-1 do
  for c:=0 to ColCount-1 do
    if not IsNull(r,c) then
  begin
    value:=Items[r,c]; // set MIN and MAX values
    if aMinValue=BlankValue then
    begin
      aMinValue:=value;
      aMaxValue:=value;
    end
      else

    if value < aMinValue then aMinValue:=value else
    if value > aMaxValue then aMaxValue:=value;
  end;
end;



//--------------------------------------------------------
function TOrdinalMatrix.BuildProfileLine (aX1,aY1,aX2,aY2:double; aStep:integer=0): TMatrixProfileLine;
//--------------------------------------------------------
// ��������� ����� ������� �� 2 �����������
// ��������� �������������� �������
var
  dX,dY,dR,stepX,stepY: Double; //��� ����� ��������� (�)
  Xi,Yi    : double; //���������� ������� �����}
  i,Npr    : integer;
  iValue   : integer;
begin
  //���������� ������� ������� �� ���������
  if aStep=0 then aStep:=Step;

  if (ColCount=0) or (RowCount=1) or (aStep=0) then Exit;

  stepX:=aX2-aX1; //��������� ���������� ���X=��������X
  stepY:=aY2-aY1; //��������� ���������� ���Y=��������Y

  if Abs(stepX)>Abs(stepY) {����������� ���������� ��������}
    then Npr:=Trunc(Abs(stepX)/aStep)+1
    else Npr:=Trunc(Abs(stepY)/aStep)+1;
  if Npr<2 then Npr:=2; {�������� - �������� � ����� ������ ���}


  dX:=stepX/(Npr); //���X
  dY:=stepY/(Npr); //���Y
  dR:=1/1000 * Sqrt(Sqr(dX)+Sqr(dY)); // �����.� k�

  SetLength (Result.Items, Npr+1); // ������ ������ �������

  for i:=0 to Npr do
  begin
    Xi:=aX1+dX*i; Yi:=aY1+dY*i;

    if FindPointXY (Xi,Yi, iValue) then Result.Items[i].Value:=iValue
                                   else Result.Items[i].Value:=BlankValue;
  end;

  Result.Offset    :=dR;
  Result.Distance  :=dR * (Npr);
  Result.BlankValue:=FBlankValue;
end;

//-------------------------------------------------
// TIntMatrix, TByteMatrix, TkmoMatrix, TkgrMatrix, TkupMatrix
//-------------------------------------------------
constructor TByteMatrix.Create;
begin
  inherited Create;
  RecordType:=dtByte;
end;


constructor TIntMatrix.Create;
begin
  inherited Create;
  RecordType:=dtInteger;
end;


//-------------------------------------------------
procedure TOrdinalMatrix.ApplyBoundsFromMatrix (aMatrix: TOrdinalMatrix);
//-------------------------------------------------
var r,c: integer;
begin
  for r:=0 to RowCount-1 do
  for c:=0 to ColCount-1 do if not IsNull(r,c) then
  begin
    if aMatrix.IsNull(r,c) then Items[r,c]:=BlankValue;
  end;
end;

//-------------------------------------------------
// TkmoMatrix
//-------------------------------------------------
function TkmoMatrix.GetKmoCellCount(): integer;
//���-�� �������� �������� ������
var r,c: integer;
begin
  Result:=0;
  for r:=0 to RowCount-1 do
  for c:=0 to ColCount-1 do
    if IsNull(r,c) then Inc(Result);
end;



//-------------------------------------------------
// TkupMatrix
//-------------------------------------------------
constructor TkupMatrix.Create;
begin
  inherited Create;
  NegativeValues:=true;
end;


function TkupMatrix.CellHasCoverage (aRow,aCol:integer): boolean; // �������� ����� ��������
var iValue: integer;
begin
  iValue:=Items[aRow,aCol];
  Result:=(iValue<>BlankValue) and (iValue>=Sensitivity);
end;


function TkupMatrix.GetCellCountBySense (aSense: integer): integer;
var r,c,iValue: integer;
begin
  Result:=0;
  for r:=0 to RowCount-1 do
  for c:=0 to ColCount-1 do if not IsNull(r,c) then begin
    iValue:=Items[r,c];
    if Items[r,c] > aSense then Inc(Result);
  end;
end;


procedure TkupMatrix.ApplyRangeFilter (aMinValue,aMaxValue: double);
var r,c,iValue: integer;
begin
  for r:=0 to RowCount-1 do
  for c:=0 to ColCount-1 do
  begin
    iValue:=Items[r,c];
    if (iValue<>BlankValue) and
      ((iValue<aMinValue) or (iValue>aMaxValue))
    then Items[r,c]:=BlankValue;
  end;
end;



//==================================================
//  TciMatrix, TciaMatrix
//==================================================
constructor TShortintMatrix.Create;
begin
  inherited Create;
  RecordType:=dtShortint;
end;

//------------------------------------------------------
//  TCustomMatrixList
//------------------------------------------------------
function TMatrixList.GetItem (Index:integer): TOrdinalMatrix;
begin
  Result:=TOrdinalMatrix(inherited Items[Index]);
end;

procedure TMatrixList.Clear;
var i:integer;
begin
  if not IsStoreItemsOnClear1 then
    for i:=0 to Count-1 do begin Items[i].Destroy; end;
  inherited;
end;

//------------------------------------------------------
function TMatrixList.AddMatrix (aFileName: string): TOrdinalMatrix;
//------------------------------------------------------
begin
  Result:=TOrdinalMatrix.Create;
  Add (Result);
  Result.LoadFromFile (AFileName);
end;


function TKupMatrixList.GetItem (Index: integer): TkupMatrix;
begin
  Result:=TkupMatrix(inherited Items[Index]);
end;


function TKgrMatrixList.AddMatrix (aFileName: string): boolean;
var oMatrix: TkgrMatrix;
begin
  oMatrix:=TkgrMatrix.Create;

  if oMatrix.LoadFromFile (aFileName) then
  begin
    Add(oMatrix);
    Result:=True;
  end else begin
    oMatrix.Free;
    Result:=False;
  end;
end;

//-------------------------------------------------
function TKupMatrixList.AddMatrix (aFileName: string;
                   aSensitivity: integer;
                   aSectorID: integer=0): boolean;
//-------------------------------------------------
var oMatrix: TkupMatrix;
begin
  oMatrix:=TkupMatrix.Create;

  if oMatrix.LoadFromFile (aFileName) then
  begin
    oMatrix.SectorID:=aSectorID;
    Add(oMatrix);
    Result:=True;
  end else begin
    oMatrix.Free;
    Result:=False;
  end;
end;

// ��������� ������� �������� ����� �������
// �� ������������ ���������
//------------------------------------------------------
function TKupMatrixList.BuildProfileLine (aX1,aY1,aX2,aY2: double): TMatrixProfileLine;
//------------------------------------------------------
const
    BLANK_VALUE=High(Byte);

var i,j,iStep,iMaxCount,iCount: integer;
    arrProf: array of TMatrixProfileLine;
begin
  SetLength (arrProf,Count);
  if Count>0 then iStep:=Items[0].Step
             else Exit;

  //��� ������� ����� ���������� �����
  for i:=0 to Count-1 do
    arrProf[i]:=Items[i].BuildProfileLine (ax1,ay1,ax2,ay2, iStep);

  Result.BlankValue:=BLANK_VALUE;

  //��������� ��������� ����� ��������
//  for i:=1 to High(arrProf) do
//    if High(arrProf[i].Items) <> High(arrProf[i-1].Items) then Exit;

  iMaxCount:=0; //High(arrProf[0].Items)+1;
  for i:=0 to High(arrProf) do begin
    iCount:=High(arrProf[i].Items)+1;
    if iCount > iMaxCount then  iMaxCount:=iCount;
//    if High(arrProf[i].Items) > 0) <> High(arrProf[i-1].Items) then Exit;
  end;

  SetLength (Result.Items, iMaxCount);
  for i:=0 to High(Result.Items) do
    Result.Items[i].Value:=BLANK_VALUE;

  for j:=0 to iMaxCount-1 do
  for i:=0 to High(arrProf) do
    if High(arrProf[i].Items)+1 = iMaxCount then
      if (arrProf[i].Items[j].Value <> BLANK_VALUE) and
         ((Result.Items[j].Value = BLANK_VALUE) or
          (Result.Items[j].Value < arrProf[i].Items[j].Value))
       then begin
         Result.Items[j].Value:=arrProf[i].Items[j].Value;
       end;
//  Result:=arrProf[0];

  Result.BlankValue:=BLANK_VALUE;

  for i:=0 to High(arrProf) do
   if arrProf[i].Distance > 0 then begin
     Result.Distance  :=arrProf[i].Distance;
     Result.Offset    :=arrProf[i].Offset;
     Break;
   end;

end;


//------------------------------------------------------
function TOrdinalMatrix.CellToStr (ARow,ACol: integer): string;
//------------------------------------------------------
begin
  if IsNull(ARow,ACol)
    then Result:='          '
    else Result:=Format('%10d',[Items[ARow,ACol]]);
end;

function TFloatMatrix.CellToStr(ARow, ACol: integer): string;
begin
  if IsNull(ARow,ACol)
    then Result:='       '
    else Result:=Format('%1.6f ', [Items[ARow,ACol]]);
end;


function TFloatMatrix.IsNull(aRow, aCol: integer): boolean;
begin
  Result:= (Items[aRow,aCol] = FBlankValue);

end;

//-------------------------------------------------------------------
function matrix_CreateMatrixByFileName (aFileName: string): TOrdinalMatrix;
//-------------------------------------------------------------------
var s: string;
begin
  s:=LowerCase(ExtractFileExt (aFileName));

//  if s='.hoff' then Result:=TIntMatrix.Create else
  if (s='.los') or (s='.int')
               then Result:=TIntMatrix.Create else

  if s='.kup'  then Result:=TkupMatrix.Create else
  if s='.kgr'  then Result:=TkgrMatrix.Create else
//////////////////////////////////////////////////////

//  if s='.lac'     then Result:=TkgrMatrix.Create else
//  if s='.bsic'    then Result:=TkgrMatrix.Create else
//  if s='.frgr'    then Result:=TkgrMatrix.Create else

  if s='.kmo'  then Result:=TkmoMatrix.Create else
//  if s='.kpp'  then Result:=TkmoMatrix.Create else
  if s='.ci'   then Result:=TciMatrix.Create  else
  if s='.ca'   then Result:=TciMatrix.Create  else
  if s='.cia'  then Result:=TciaMatrix.Create else

  raise Exception.Create('');


 // if s='.tp'   then Result:=TtpMatrix.Create else
 // if s='.trf' then Result:=TtpMatrix.Create else
//  if s='.rsk' then Result:=TtpMatrix.Create else
//  if s='.prb' then Result:=TtpMatrix.Create else
//  if s='.mos'  then Result:=TMosMatrix.Create else
//  if s='.stat' then Result:=TMosMatrix.Create else


  //  Result:=nil;

end;



var
  oKupMatrix: TkupMatrix;

begin
 {
  oKupMatrix:=TkupMatrix.Create;

  oKupMatrix.SetParams1 (6836083,5500594,
                        733, 1052,
                        //733, 1052,
                        500, 1, 'd:\aaa\test.bin'
                        );
 // oKupMatrix[540,336]:=10;
  oKupMatrix.FillBlank; //////////////////////

  oKupMatrix.Free;
 }

end.



(*
constructor TtpMatrix.Create;
begin
  inherited Create;
  RecordType :=dtFloat;
end;

constructor TMosMatrix.Create;
begin
  inherited Create;
  RecordType :=dtFloat;
end;
*)

