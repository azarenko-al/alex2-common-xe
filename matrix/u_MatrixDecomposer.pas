unit u_MatrixDecomposer;

interface
uses Classes, Dialogs, SysUtils,
     u_Geo,

     u_func,
     u_files,

     u_Matrix_base,
     
     u_progress,
     u_Matrix;


type
  TMapPoint       = class;
  TMatrixPolyline = class;
  TMatrixCellList = class;

  TOnMapNewBLShapeEvent = procedure (Sender: TObject;
                                     var aBLPoints: TBLPointArray;
                                     aFillValue: integer) of object;
//  TOnMapNewXYShapeEvent = procedure (Sender: TObject; aXYPoints: TXYPointList; aFillValue: integer) of object;

  //-------------------------------------------------
  // ������� � ������������ ������������ �� �������
  //-------------------------------------------------
  TMatrixDecomposer = class  (TProgress)
  //-------------------------------------------------
  private
    FProgress     : integer; // ���-�� ������������ ����������� �����
//    FOnNewXYShape : TOnMapNewXYShapeEvent; // ��������� �����
    FOnNewBLShape : TOnMapNewBLShapeEvent; // ��������� �����

    FLeftCol,FRightCol : integer;
    FLastVacantCol     : integer;
    FTopLineCells      : TMatrixCellList; // used to MakePolygon
    FBottomLineCells   : TMatrixCellList; // used to MakePolygon

    FTopLinePoints     : TMatrixPolyline;
    FBottomLinePoints  : TMatrixPolyline;

    FShapeLine         : TMatrixPolyline;
    FShapesTotal       : integer; // �� ������� �������� �������� �����

    FSrcMatrix         : TOrdinalMatrix;
    FMirrorMatrix      : TOrdinalMatrix; // ����� ������� (��� ����������� �����)
    FZoneNum: integer;

    function  FindMapVacantPoint (var aRow,aCol: integer): boolean;
    procedure GetTopAndBottomRows (aRow,aCol: integer; var aTopRow,aBottomRow: integer);
    procedure MakePolygon (aStartRow,aStartCol: integer);
    procedure DecomposePolygon;
    procedure ClearPolygon;

    procedure DoOnNewShape (aShapeLine: TMatrixPolyline; aValue: integer);

  public
    constructor Create;
    destructor  Destroy; override;

    procedure Decompose (aMatrix: TOrdinalMatrix);

    property ShapesTotal  : integer read FShapesTotal; // �� ������� �������� �������� �����
//    property OnNewXYShape : TOnMapNewXYShapeEvent read FOnNewXYShape write FOnNewXYShape;
    property OnNewBLShape : TOnMapNewBlShapeEvent read FOnNewBLShape write FOnNewBLShape;

  end;


  //---------------------------------
  //  TMatrixPolyline
  //---------------------------------
  TMapPoint = class
    X,Y     : double;
    B,L     : double;
    Row,Col : integer; // � ����� ������ ����������� �����
  end;

  TMatrixPolyline = class(TList)
  private
    function GetMapPoint (Index: integer): TMapPoint;
  public
    constructor Create;
    procedure Clear; override;
    procedure DeletePoint (Index: Integer);
    procedure AddPoint    (aX,aY: double; aRow:integer=0; aCol:integer=0);
    procedure InsertPoint (Index: Integer; aX,aY: double; aRow:integer=0; aCol:integer=0);
    function  Compare  (Index1,Index2: integer): boolean; // true-equal

    procedure CopyToBLPoints (var aBLPoints: TBLPointArray);
//    procedure CopyToXYPoints (aXYPoints: TXYPointList);

    function  MakeBLPointArray: TBLPointArray;
    function  MakeXYPointArray: TXYPointArray;

    property  Points [Index: integer]: TMapPoint read GetMapPoint; default;
  end;

  TMatrixCell = class  // ������ �������
    Row,Col : integer;
    TopValue,BottomValue: integer; // �������� ������� � ������� ������ � �����
    Location: (ptTop,ptLeft,ptRight);
  end;

  //---------------------------------
  //  TMatrixCellList
  //---------------------------------
  TMatrixCellList = class(TList)
  private
    function GetItem (Index: integer): TMatrixCell;
  public
    constructor Create;
    procedure Clear; override;
    procedure AddCell    (aRow,aCol: integer; aTopValue,aBottomValue: integer);
    procedure InsertCell (aRow,aCol: integer; aTopValue,aBottomValue: integer);
    procedure DeleteItem (Index: integer);
    property  Items [Index : integer]: TMatrixCell read GetItem; default;
  end;


//======================================================
implementation
//======================================================

//------------------------------------------------------
// TMatrixDecomposer
// - ��������� ������� �� �������
//------------------------------------------------------

//------------------------------------------------------
constructor TMatrixDecomposer.Create;
//------------------------------------------------------
begin
  inherited;

  // ���������� ��� �������
  FShapeLine:=TMatrixPolyline.Create;

  FTopLineCells   :=TMatrixCellList.Create; // used to MakePolygon
  FBottomLineCells:=TMatrixCellList.Create; // used to MakePolygon

  FTopLinePoints    := TMatrixPolyline.Create;
  FBottomLinePoints := TMatrixPolyline.Create;

  FMirrorMatrix := TOrdinalMatrix.Create;

end;

//------------------------------------------------------
destructor TMatrixDecomposer.Destroy;
//------------------------------------------------------
begin
  FShapeLine.Free;
  FTopLineCells.Free;
  FBottomLineCells.Free;
  FTopLinePoints.Free;
  FBottomLinePoints.Free;
  FMirrorMatrix.Free;
  inherited;
end;


//------------------------------------------------------
procedure TMatrixDecomposer.GetTopAndBottomRows (aRow,aCol: integer; var aTopRow,aBottomRow: integer);
//------------------------------------------------------
var r,iValue: integer;
begin
  iValue:=FSrcMatrix[aRow,aCol]; aTopRow:=aRow; aBottomRow:=aRow;

  for r:=aTopRow-1 downto 0 do
    if FSrcMatrix[r,aCol]=iValue then aTopRow:=r else break;

  for r:=aRow+1 to FSrcMatrix.RowCount-1 do
    if FSrcMatrix[r,aCol] = iValue then aBottomRow:=r else break;
end;

//------------------------------------------------------
// ��������e ����������� ��������
//------------------------------------------------------
procedure TMatrixDecomposer.MakePolygon (aStartRow,aStartCol: integer);
//------------------------------------------------------
var iContent, r,c, top_r,bottom_r,middle_r : integer;
    canMoveRight,canMoveLeft: boolean;
begin
  iContent := FSrcMatrix[aStartRow,aStartCol];

  FLeftCol:=aStartCol; FRightCol:=aStartCol;

  GetTopAndBottomRows (aStartRow,aStartCol, top_r,bottom_r);
  if (top_r-1)>=0 then FTopLineCells.AddCell (top_r, FLeftCol, FMirrorMatrix[top_r-1,FLeftCol], 0)
                  else FTopLineCells.AddCell (top_r, FLeftCol, FMirrorMatrix.BlankValue, 0);
  FBottomLineCells.AddCell (bottom_r, FLeftCol, 0, FMirrorMatrix[bottom_r+1,FLeftCol]);

  for c:=aStartCol+1 to FSrcMatrix.ColCount-1 do //��������� ������� ������
  begin
    canMoveRight:=false;
    // ����� ������ ����������
    middle_r:=top_r + (bottom_r-top_r) div 2;
    for r:=middle_r to bottom_r do // ����� ����� ������
      if FSrcMatrix[r,c]=iContent then begin canMoveRight:=true; break; end;

    if (not canMoveRight) then
    for r:=middle_r downto top_r do // ����� ����� ������
      if FSrcMatrix[r,c]=iContent then begin canMoveRight:=true; break; end;

    if canMoveRight then begin
      GetTopAndBottomRows (r,c, top_r,bottom_r);
      if (top_r-1)>=0 then FTopLineCells.AddCell (top_r, c, FMirrorMatrix[top_r-1,c], 0)
                      else FTopLineCells.AddCell (top_r, c, FMirrorMatrix.BlankValue, 0);
      FBottomLineCells.AddCell (bottom_r, c, 0, FMirrorMatrix[bottom_r+1,c]);
      FRightCol:=c;
    end else
      Break;
  end;

  for c:=aStartCol-1 downto 0 do //��������� ������� ������
  begin
    canMoveLeft:=false;
    for r:=top_r to bottom_r do // ����� ����� ������
      if FSrcMatrix[r,c]=iContent then begin canMoveLeft:=true; break; end;

    if canMoveLeft then begin
      GetTopAndBottomRows (r,c, top_r,bottom_r);
      FTopLineCells.InsertCell    (top_r, c, FMirrorMatrix[top_r-1,c], 0);
      FBottomLineCells.InsertCell (bottom_r, c, 0, FMirrorMatrix[bottom_r+1,c]);

      FLeftCol:=c;
    end else
      Break;
  end;
end;

//------------------------------------------------------
procedure TMatrixDecomposer.ClearPolygon;  // fill polygon with 0
//------------------------------------------------------
var i,c,r: integer;
begin
  for i:=0 to FTopLineCells.Count-1 do
  begin
    c:=FTopLineCells[i].Col;
    Inc(FProgress, FBottomLineCells[i].Row - FTopLineCells[i].Row+1);
    for r:=FTopLineCells[i].Row to FBottomLineCells[i].Row do begin
      FSrcMatrix[r,c] := FSrcMatrix.BlankValue;
    end;
  end;

  DoProgress (FProgress, FSrcMatrix.ColCount * FSrcMatrix.RowCount);
end;

//------------------------------------------------------
// ������� ������� �� ����� � �������������� ��������� �����
//------------------------------------------------------
procedure TMatrixDecomposer.DecomposePolygon;
//------------------------------------------------------
   //-------------------------------------------------
   procedure DoOptimizeTopLine(); // �������������� (������� ������ �����)
   // ������� ������ cells from top line
   //-------------------------------------------------
   var  i1,i2,iRow,k:integer; iTopValue:integer; // �������� ������ ������
   begin
       if FTopLineCells.Count < 3 then Exit;

       i1:=FTopLineCells.Count-1;
       repeat
           iRow:=FTopLineCells[i1].Row;
           iTopValue:=FTopLineCells[i1].TopValue;

           i2:=i1-1;
           while (i2>=0) and (FTopLineCells[i2].Row=iRow) and (iTopValue=FTopLineCells[i2].TopValue)
             do i2:=i2-1;

           for k:=i2+2 to i1-1 do  //iCount:=i1-(i2+2) - ���-�� �����, ������� ����� �������
              FTopLineCells.DeleteItem (i2+2); // ������� ������ with index: i2+2... i1-1
           i1:=i2;
       until (i1 < 0);
   end;

   //-------------------------------------------------
   procedure DoOptimizeBottomLine();
   // ������� ������ cells from bottom line
   //-------------------------------------------------
   var  i1,i2,iRow,k:integer; iBottomValue:integer; // �������� ������ ������
   begin
       if FBottomLineCells.Count < 3 then Exit;

       i1:=FBottomLineCells.Count-1;
       repeat
           iRow:=FBottomLineCells[i1].Row;
           iBottomValue:=FBottomLineCells[i1].BottomValue;

           i2:=i1-1;
           while (i2>=0) and (FBottomLineCells[i2].Row=iRow) and (iBottomValue=FBottomLineCells[i2].BottomValue)
             do i2:=i2-1;

           for k:=i2+2 to i1-1 do  //iCount:=i1-(i2+2) - ���-�� �����, ������� ����� �������
              FBottomLineCells.DeleteItem (i2+2); // ������� ������ with index: i2+2... i1-1
           i1:=i2;
       until (i1 < 0);
   end;

   //------------------------------------------------------
   procedure DoMakeGeoPointsLine(); // ������� ������� ��� ����������� �� �����
   //------------------------------------------------------
   var i:integer; tmp_x:double;
   begin
      with FTopLinePoints do for i:=0 to Count-1 do
        FShapeLine.AddPoint (FSrcMatrix.TopLeft.X - Points[i].Y,
                             FSrcMatrix.TopLeft.Y + Points[i].X, 0,0);

      with FBottomLinePoints do for i:=Count-1 downto 0 do
        FShapeLine.AddPoint (FSrcMatrix.TopLeft.X - Points[i].Y,
                             FSrcMatrix.TopLeft.Y + Points[i].X, 0,0);
   end;

   //-------------------------------------------------
   procedure DoInsertTopLineEdgePoints();
   // �������� ������ �� ��������
   // - ��� ���������� ������������ ����� �� �����
   //-------------------------------------------------
   var i,col, row,row_L,row_R, iValue1,iValue2: integer; x,y: double;
   begin
     for i:=FTopLinePoints.Count-1 downto 1 do begin
       // ���� ������ �� ������ ������ - ��������� ������� �����
       row_L:=FTopLinePoints[i-1].Row;
       row_R:=FTopLinePoints[i].Row;
       col  :=FTopLinePoints[i-1].Col;

       if row_L < row_R then // ����� ������ ���� ������ ������ (� �������)
       //--------------------------------------------
         for row:=row_R-1 downto row_L do begin
           iValue1:=FMirrorMatrix[row,col+1];   // right value
           iValue2:=FMirrorMatrix[row+1,col+1]; // right value

           if iValue1<>iValue2 then begin
             x:=FTopLinePoints[i-1].X;  y:=(row+1)*FSrcMatrix.Step;
             FTopLinePoints.InsertPoint(i, x,y);
           end;
          end else

       if row_L > row_R then // ����� ������ ���� ������ ������ (� �������)
       //--------------------------------------------
         for row:=row_R to row_L-1 do begin
           iValue1:=FMirrorMatrix[row,col];   // left value
           iValue2:=FMirrorMatrix[row+1,col]; // left value

           if iValue1<>iValue2 then begin
             x:=FTopLinePoints[i-1].X;  y:=(row+1)*FSrcMatrix.Step;
             FTopLinePoints.InsertPoint(i, x,y);
           end;
         end;
     end;
   end;

   //-------------------------------------------------
   procedure DoInsertBottomLineEdgePoints();
   // �������� ������ �� ��������
   // - ��� ���������� ������������ ����� �� �����
   //-------------------------------------------------
   var i,col, row,row_L,row_R, iValue1,iValue2: integer; x,y: double;
   begin
     for i:=FBottomLinePoints.Count-1 downto 1 do begin
       // ���� ������ �� ������ ������ - ��������� ������� �����
       row_L:=FBottomLinePoints[i-1].Row;
       row_R:=FBottomLinePoints[i].Row;
       col  :=FBottomLinePoints[i-1].Col;

       if row_L < row_R then // ����� ������ ���� ������ ������ (� �������)
       //--------------------------------------------
         for row:=row_R downto row_L+1 do begin
           iValue1:=FMirrorMatrix[row,col];   // right value
           iValue2:=FMirrorMatrix[row+1,col]; // right value

           if iValue1<>iValue2 then begin
             x:=FBottomLinePoints[i-1].X;  y:=(row+1)*FSrcMatrix.Step;
             FBottomLinePoints.InsertPoint(i, x,y);
           end;
          end else

       if row_L > row_R then // ����� ������ ���� ������ ������ (� �������)
       //--------------------------------------------
         for row:=row_R+1 to row_L do begin
           iValue1:=FMirrorMatrix[row,col+1];   // left value
           iValue2:=FMirrorMatrix[row+1,col+1]; // left value

           if iValue1<>iValue2 then begin
             x:=FBottomLinePoints[i-1].X;  y:=(row+1)*FSrcMatrix.Step;
             FBottomLinePoints.InsertPoint(i, x,y);
           end;
         end;
     end;
   end;

   //-------------------------------------------------
   procedure DoInsertEdgePoints(); // �������� ������ �� ������� ��������
   //-------------------------------------------------
   var col, row,row_t,row_b, iValue1,iValue2: integer; x,y: double;
   begin
     // left edge
     row_t:=FTopLinePoints[0].Row;
     row_b:=FBottomLinePoints[0].Row;
     col  :=FTopLinePoints[0].Col;
     x    :=FBottomLinePoints[0].X;

     for row:=row_t to row_b-1 do begin
       iValue1:=FMirrorMatrix[row,col-1];   // left value
       iValue2:=FMirrorMatrix[row+1,col-1]; // left value

       if iValue1<>iValue2 then
         FTopLinePoints.InsertPoint(0, x, (row+1)*FSrcMatrix.Step);
     end;

     // right edge
     row_t:=FTopLinePoints[FTopLinePoints.Count-1].Row;
     row_b:=FBottomLinePoints[FBottomLinePoints.Count-1].Row;
     col  :=FTopLinePoints[FTopLinePoints.Count-1].Col;
     x    :=FBottomLinePoints[FBottomLinePoints.Count-1].X;

     for row:=row_t to row_b-1 do begin
       iValue1:=FMirrorMatrix[row,col+1];   // left value
       iValue2:=FMirrorMatrix[row+1,col+1]; // left value

       if iValue1<>iValue2 then
         FTopLinePoints.AddPoint(x, (row+1)*FSrcMatrix.Step);
     end;
   end;

   //-------------------------------------------------

var i,r,col,row,iValue: integer; x1,x2,y1,y2: double;
    bool: boolean;
begin
  //ivalue - �������� ������ ������� (��� ���� �������)
  iValue := FMirrorMatrix[FTopLineCells[0].Row,FTopLineCells[0].Col];
  FTopLinePoints.Clear;
  FBottomLinePoints.Clear;

  DoOptimizeTopLine();    // �������������� (������� ������ ����������)
  DoOptimizeBottomLine(); // �������������� (������� ������ ����������)

  // ����� ������������ �����
  for i:=0 to FTopLineCells.Count-1 do
  begin
    row:=FTopLineCells[i].Row; col:=FTopLineCells[i].Col; // process top square (top line)

    // 2 ������� ����� � ����������
    x1:=(col)  *FSrcMatrix.Step;  y1:=(row)*FSrcMatrix.Step;
    x2:=(col+1)*FSrcMatrix.Step;  y2:=(row)*FSrcMatrix.Step;

    if (FTopLinePoints.Count=0) or (FTopLineCells[i].Row<>FTopLineCells[i-1].Row) then
      FTopLinePoints.AddPoint(x1,y1, row,col);

    if (i=FTopLineCells.Count-1) or (FTopLineCells[i].Row<>FTopLineCells[i+1].Row) or (FTopLineCells[i].TopValue<>FTopLineCells[i+1].TopValue)
      then FTopLinePoints.AddPoint(x2,y2, row,col);
  end;

  // process bottom square (bottom line)
  for i:=0 to FBottomLineCells.Count-1 do
  begin
    row:=FBottomLineCells[i].Row; col:=FBottomLineCells[i].Col; // process top square (top line)

    x1:=(col)  *FSrcMatrix.Step;  y1:=(row+1)*FSrcMatrix.Step;
    x2:=(col+1)*FSrcMatrix.Step;  y2:=(row+1)*FSrcMatrix.Step;

    if (FBottomLinePoints.Count=0) or // ����� ������ ������
       (FBottomLineCells[i].Row<>FBottomLineCells[i-1].Row) then
      FBottomLinePoints.AddPoint(x1,y1, row,col);

    if (i=FBottomLineCells.Count-1) or // ����� ��������� ������
       (FBottomLineCells[i].Row<>FBottomLineCells[i+1].Row) or (FBottomLineCells[i].BottomValue<>FBottomLineCells[i+1].BottomValue)
      then FBottomLinePoints.AddPoint(x2,y2, row,col);
  end;

  DoInsertTopLineEdgePoints();
  DoInsertBottomLineEdgePoints();
  DoInsertEdgePoints();

  // �������� � ������ ����� delta
  DoMakeGeoPointsLine(); // ������� ������� ��� ����������� �� �����
                         // �������� �������������� ���������� ������ � ������ �����

  DoOnNewShape(FShapeLine, iValue);


  FShapeLine.Clear;
  Inc(FShapesTotal);

  FTopLineCells.Clear;
  FBottomLineCells.Clear;
end;

//-------------------------------------------------------------------
procedure TMatrixDecomposer.DoOnNewShape(aShapeLine: TMatrixPolyline; aValue: integer);
//-------------------------------------------------------------------
var x,y,b,l: double;  i:integer;
     bl: TBLPoint;
     xy: TXYPoint;
  oBLPoints: TBLPointArray;
//  oXYPoints: TXYPointList;
begin
//  oXYPoints:=TXYPointList.Create;
//  oBLPoints:=TBLPointList.Create;

{  if Assigned(FOnNewXYShape) then begin
    oXYPoints:=TXYPointList.Create;
    aShapeLine.CopyToXYPoints (oXYPoints);
    FOnNewXYShape (Self, oXYPoints, aValue);
    oXYPoints.Free;
  end;
}
  if Assigned(FOnNewBLShape) then begin
    for i:=0 to FShapeLine.Count-1 do
    begin
      xy.X:=aShapeLine[i].X;
      xy.Y:=aShapeLine[i].Y;
      bl:=geo_XY_to_BL (xy, EK_KRASOVSKY42, FZoneNum);
      aShapeLine[i].b := bl.B;
      aShapeLine[i].l := bl.L;
    end;

 //   oBLPoints:=TBLPointList.Create;
    aShapeLine.CopyToBLPoints (oBLPoints);
    FOnNewBLShape(Self, oBLPoints, aValue);
 //   oBLPoints.Free;
  end;
end;

//------------------------------------------------------
function TMatrixDecomposer.FindMapVacantPoint (var aRow,aCol: integer): boolean;
//------------------------------------------------------
// ���������� �������������� ����� �������
var c,r: integer;
begin
  for c:=FLastVacantCol to FSrcMatrix.ColCount-1 do
  for r:=0 to FSrcMatrix.RowCount-1 do begin
    if not FSrcMatrix.IsNull(r,c) then begin
       aRow:=r; aCol:=c; FLastVacantCol:=c; Result:=true; Exit;
    end;
  end;
  Result:=false;
end;

//------------------------------------------------------
//   TMatrixPolyline
//------------------------------------------------------
constructor TMatrixPolyline.Create;
begin
  inherited; Capacity:=2000;
end;


procedure TMatrixPolyline.AddPoint (aX,aY : double; aRow,aCol: integer);
var point: TMapPoint;
begin
  point:=TMapPoint.Create;  Add (point);
  with point do begin X:=aX; Y:=aY; Row:=aRow; Col:=aCol; end;
end;


procedure TMatrixPolyline.InsertPoint (Index: Integer; aX,aY: double; aRow,aCol: integer);
var point: TMapPoint;
begin
  point:=TMapPoint.Create;  Insert (Index, point);
  with point do begin X:=aX; Y:=aY; Row:=aRow; Col:=aCol; end;
end;


function TMatrixPolyline.GetMapPoint (Index: integer): TMapPoint;
begin
  Result:=TMapPoint(inherited Items[Index]);
end;


procedure TMatrixPolyline.DeletePoint (Index: Integer);
begin
  Points[Index].Destroy;
  Delete(Index);
end;


function TMatrixPolyline.Compare (Index1,Index2: integer): boolean; // true-equal
begin
  Result:=(Points[Index1].X=Points[Index2].X) and (Points[Index1].Y=Points[Index2].Y);
end;


procedure TMatrixPolyline.Clear;
var i:integer;
begin
  for i:=0 to Count-1 do Points[i].Destroy; inherited;
end;


//-----------------------------------------------
// TMatrixCellList
//-----------------------------------------------
constructor TMatrixCellList.Create;
begin
  inherited; Capacity:=1500;
end;

function TMatrixCellList.GetItem (Index: integer): TMatrixCell;
begin
  Result:=TMatrixCell(inherited Items[Index]);
end;

procedure TMatrixCellList.AddCell(aRow,aCol: integer; aTopValue,aBottomValue: integer);
begin
  with Items[Add(TMatrixCell.Create)] do begin Row:=aRow; Col:=aCol; TopValue:=aTopValue; BottomValue:=aBottomValue; end;
end;

procedure TMatrixCellList.InsertCell (aRow,aCol: integer; aTopValue,aBottomValue: integer);
var obj: TMatrixCell;
begin
  obj:=TMatrixCell.Create;  with obj do begin Row:=aRow; Col:=aCol; TopValue:=aTopValue; BottomValue:=aBottomValue; end;
  Insert(0, obj);
end;

procedure TMatrixCellList.Clear;
var i:integer;
begin
  for i:=0 to Count-1 do Items[i].Destroy;  inherited;
end;

procedure TMatrixCellList.DeleteItem (Index: integer);
begin
  Items[Index].Destroy;  Delete(Index);
end;

//-------------------------------------------------------------------
function TMatrixPolyline.MakeBLPointArray: TBLPointArray;
//-------------------------------------------------------------------
// ���������� ��� ������������ ������� �������
var i:integer;
begin
//  Result.Count:=Count;

  SetLength (Result, Count);

  for i:=0 to Count-1 do
  begin
    Result[i].b:=Points[i].b;
    Result[i].l:=Points[i].l;
  end;
end;

//-------------------------------------------------------------------
function TMatrixPolyline.MakeXYPointArray: TXYPointArray;
//-------------------------------------------------------------------
// ���������� ��� ������������ ������� �������
var i:integer;
begin
//  Result.Count:=Count;

   SetLength (Result, Count);
  for i:=0 to Count-1 do
  begin
    Result[i].x:=Points[i].x;
    Result[i].y:=Points[i].Y;
  end;
end;


procedure TMatrixPolyline.CopyToBLPoints(var aBLPoints: TBLPointArray);
var i: integer;
begin
//  aBLPoints.Clear;
  SetLength (aBLPoints, Count);

//  aBLPoints.Count:=Count;

  for i:=0 to Count-1 do
    aBLPoints[i]:=MakeBLPoint (Points[i].B, Points[i].L);

//    AddItem (MakeBLPoint (Points[i].B, Points[i].L));
end;


{procedure TMatrixPolyline.CopyToXYPoints(aXYPoints: TXYPointList);
var i: integer;
begin
  aXYPoints.Clear;
  for i:=0 to Count-1 do
    aXYPoints.AddItem (MakeXYPoint (Points[i].X, Points[i].Y));
end;
}

//------------------------------------------------------
procedure TMatrixDecomposer.Decompose (aMatrix: TOrdinalMatrix);
//------------------------------------------------------
var r,c: integer;
  sFile: string;

  oMatrix: TOrdinalMatrix;

begin
  if aMatrix.Step=0 then begin
    ShowMessage('Decompose map: Step=0'); Exit;
  end;

  FSrcMatrix:=aMatrix;

  FZoneNum:=geo_Get6ZoneY (aMatrix.TopLeft.Y);

  FProgress:=0;
  FShapesTotal:=0; // �� ������� �������� (���-��) ������� �����
  FLastVacantCol:=0;
  Terminated:=false;

  // ���������� ����� ������� - ����� ��� ����������� �����
//  sFile:='c:\temp\rpls\mirror_matrix'; //GetTempFileName_();

{
  sFile:='t:\mirror_matrix.int'; //GetTempFileName_();
  aMatrix.SaveToFile (sFile);
  aMatrix.LoadFromFile (sFile);
}
//  sFile:='t:\mirror_matrix.int'; //GetTempFileName_();

 // sFile:='t:\region.kup'; //GetTempFileName_();


 // FMirrorMatrix.LoadFromFile (sFile, fmOpenReadWrite );

  FMirrorMatrix.Assign(aMatrix);


  oMatrix:=TOrdinalMatrix.Create;
  oMatrix.LoadFromFile (sFile);

  for r:=0 to FMirrorMatrix.RowCount-1 do
   for c:=0 to FMirrorMatrix.ColCount-1 do
     if FMirrorMatrix[r,c]<>oMatrix[r,c] then
       raise Exception.Create('');


  oMatrix.Free;

//  inherited SetMatrixSize(Source.RowCount, Source.ColCount);
    {
  for r:=0 to FMirrorMatrix.RowCount-1 do
   for c:=0 to FMirrorMatrix.ColCount-1 do
     if FMirrorMatrix[r,c]<>aMatrix[r,c] then
       raise Exception.Create('');
   }

  //���������� �������������� ����� �� �����
  while FindMapVacantPoint(r,c) and (not Terminated) do
  begin
    MakePolygon (r,c); //������������ ���������� �������
    ClearPolygon;      //��������� ������� ������ ���������
    DecomposePolygon;  //������� ������������� �� ������� (��������������)
  end;

 ///// FMirrorMatrix.ClearMemory;
end;



end.
