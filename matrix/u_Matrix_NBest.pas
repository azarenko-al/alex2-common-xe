unit u_Matrix_NBest;

interface
uses Classes,SysUtils,IniFiles,

  u_func,
  u_Geo,
  u_geo_convert_new,

  u_VirtFile;

type

  TNBestRec = packed record
    ID    : integer;  // ��� ������� (��������)
    Value : byte;     // ������� ���
  end;

  TNBestRecArrayF = packed record
     RecordCount: Byte;
     Items: array[0..High(Byte)] of TNBestRec;
   end;


  TMatrixNBest = class
  private
    FOpenMode : word; //fmCreate,fmOpenRead;

    FIndexFileStream  : TFileStream;
    FDataFileStream   : TFileStream;

    FIndexFileStreamV: TVirtualFile;
    FDataFileStreamV: TVirtualFile;


    FmemData          : TMemoryStream;
    FmemIndex         : TMemoryStream;

    FDataFilePosition : integer;
  private

    procedure Commit();
    
    function FindCellXY(aXYPoint: TXYPoint; var aRow, aCol: integer): boolean;
    function LoadHeaderFromFile(aFileName: string): Boolean;


  public
    Active   : boolean; // readonly

    TopLeft  : TXYPoint; // �����. ������ �������� ���� � ������
    ZoneNum  : integer;
    Version  : integer;

    RowCount,ColCount,Step : integer;

    MaxRecordCount : integer;
 //   Sensitivity    : integer;

    Summary : record
      MinEMP : integer; // min ������� �������
      MaxEMP : integer; // max ������� �������
    end;


    constructor Create;
    destructor Destroy; override;

    function CreateFile(aFileName: string): Boolean;
    procedure SaveHeaderToFile(aFileName: string);
    procedure Close;

    function OpenFile(aFileName: string): Boolean;

    procedure WriteBlankRecord();
    procedure WriteRecord(var aRec: TNBestRecArrayF);

    //array of TNBestRec
    function FindBL (aPoint: TBLPoint; aZone: integer; var aRec: TNBestRecArrayF): boolean;     //; aZone: integer
    function FindXY (aXYPoint: TXYPoint; var aRec: TNBestRecArrayF): boolean;

    function GetItem(aRow,aCol: integer; var aRec: TNBestRecArrayF): Boolean;

    function Test_GetItem(aRow,aCol: integer; var aRec: TNBestRecArrayF): Boolean;

    procedure SaveToTXT(aFileName: string);
//    procedure SaveToTxtFile(aFileName: string);
  end;



//===================================================================
implementation
//===================================================================
const
  MAX_BUFFER_SIZE = 1024*1024*2;


type
    //-------------------------------------------
  // file records
  //-------------------------------------------
  TciIndexFileRecord = packed record
  //-------------------------------------------
    Position   : integer; // ������� ����� ������
  end;

  //-------------------------------------------
  TciDataFileRecord = packed record
  //-------------------------------------------
    RecordCount: Byte;
    Items: array[0..High(Byte)] of TNBestRec;
  end;



constructor TMatrixNBest.Create;
begin
  inherited Create;
  FIndexFileStreamV := TVirtualFile.Create();
  FDataFileStreamV := TVirtualFile.Create();

end;

//---------------------------------------------
// TMatrixNBest
//---------------------------------------------


destructor TMatrixNBest.Destroy;
begin
  Close;

  FreeAndNil(FIndexFileStreamV);
  FreeAndNil(FDataFileStreamV);

  inherited;
end;

//--------------------------------------------------------------------
function TMatrixNBest.OpenFile(aFileName: string): Boolean;
//--------------------------------------------------------------------
var
  bt: byte;
  
  w: packed record
    b1: byte;
    b2: Byte;
  end;

begin
  Close;

  Active:=False;
 // FOpenMode:=aMode;

 // if FOpenMode=fmOpenRead then
  begin
    if
      // (not FileExists(aFileName + '.bin'))
       (not FileExists(aFileName + '.idx')) or
       (not FileExists(aFileName + '.dat')) or
       (not FileExists(ChangeFileExt(aFileName,'.nbest')))
    then begin
//      Result := False;
      Exit;
    end;

    if not LoadHeaderFromFile (ChangeFileExt(aFileName,'.nbest')) then
      Exit;
  end;

 // FIndexFileStream:=TFileStream.Create (aFileName + '.bin', fmOpenRead);



  { TODO : do }

  FIndexFileStreamV.Open(aFileName + '.idx', fmOpenRead);
  FDataFileStreamV.Open(aFileName + '.dat', fmOpenRead);

{

  FDataFileStreamV.ReadBuffer(327679, 1, bt);
  FDataFileStreamV.ReadBuffer(327679, 2, w);
}

{
  FIndexFileStream:=TFileStream.Create (aFileName + '.idx', fmOpenRead);
  FDataFileStream :=TFileStream.Create (aFileName + '.dat', fmOpenRead);
}

{
   FDataFileStreamV.SaveToTxtFile('x:\_Virt.txt');

  SaveToTxtFile('x:\_FileStream.txt');

}


  Active:=true;
  Result := True;
end;


procedure TMatrixNBest.WriteBlankRecord();
var
  rec: TciIndexFileRecord;
begin
  rec.Position:=High(Integer);
  FmemIndex.Write(rec, SizeOf(rec));

  if (FmemIndex.Position >= MAX_BUFFER_SIZE) then
    Commit();
end;

//---------------------------------------------
procedure TMatrixNBest.Commit();
//---------------------------------------------
var iSize1,iSize2: integer;
begin
  iSize1:=FmemIndex.Position;
  FmemIndex.Position:=0;

  iSize2:=FmemData.Position;
  FmemData.Position:=0;

  if iSize1>0 then FIndexFileStream.CopyFrom (FmemIndex,iSize1);
  if iSize2>0 then FDataFileStream.CopyFrom  (FmemData, iSize2);

  FmemIndex.Position:=0;
  FmemData.Position:=0;
end;

//---------------------------------------------
procedure TMatrixNBest.WriteRecord (var aRec: TNBestRecArrayF);
//---------------------------------------------
var i,iStartPos,iDataSize: integer;
    rIndexRec : TciIndexFileRecord;
begin
  // ����������, ������� ���� ���� ��������
  iStartPos:=FmemData.Position;

  FmemData.Write (aRec.RecordCount, SizeOf(aRec.RecordCount));
  for i:=0 to aRec.RecordCount-1 do
    FmemData.Write (aRec.Items[i], SizeOf(aRec.Items[i]));

  iDataSize:=FmemData.Position-iStartPos;

  // write index record: position(offset), datasize
  rIndexRec.Position   :=FDataFilePosition;
  FmemIndex.Write(rIndexRec, SizeOf(rIndexRec));

  // ������� �������� ������
  Inc(FDataFilePosition, iDataSize);

  if (FmemIndex.Position >= MAX_BUFFER_SIZE) or (FmemData.Position >= MAX_BUFFER_SIZE) then
    Commit();

end;

//------------------------------------------------
function TMatrixNBest.GetItem(aRow,aCol: integer; var aRec: TNBestRecArrayF): Boolean;
//------------------------------------------------
var  i,j,iIndex,iRead : integer;
     rec_: TciDataFileRecord;
     rIndexRec_: TciIndexFileRecord;

begin
  Result:=false;

  FillChar(aRec, SizeOf(aRec), 0);
  FillChar(rec_, SizeOf(rec_), 0);

  if (aRow<0) or (aRow>=RowCount) or (aCol<0) or (aCol>=ColCount) then
    Exit;

  iIndex:=aRow*ColCount + aCol;

  // read position and datasize

  FIndexFileStreamV.ReadBuffer (iIndex * SizeOf(TciIndexFileRecord),
                                SizeOf(TciIndexFileRecord), @rIndexRec_);

  if (rIndexRec_.Position <> High(Integer)) then
  begin
    FDataFileStreamV.ReadBuffer (rIndexRec_.Position,
                                 SizeOf(rec_.RecordCount),
                                 @rec_.RecordCount) ;

    FDataFileStreamV.ReadBuffer (rIndexRec_.Position + SizeOf(rec_.RecordCount),
                                 rec_.RecordCount * SizeOf(rec_.Items[0]),
                                 @rec_.Items);

    //---------------------------------------------------------
    aRec.RecordCount:=rec_.RecordCount;
    for i := 0 to aRec.RecordCount - 1 do
      aRec.Items[i]:=rec_.Items[i];

    Result:=true;   
  end;

end;



//------------------------------------------------------
function TMatrixNBest.FindBL(aPoint: TBLPoint; aZone: integer; var aRec: TNBestRecArrayF): boolean;
//------------------------------------------------------
var xyPoint: TXYPoint;
begin
  if not Active then
  begin
    Result:= False;
    Exit;
  end;

  xyPoint:=geo_Pulkovo42_to_XY (aPoint, aZone);
//  xyPoint:=geo_BL_to_XY (aPoint,aZone);

  Result:=FindXY (xyPoint, aRec);
  Result:=Result and (aRec.RecordCount>0);
end;


//-----------------------------------------------------------
function TMatrixNBest.FindXY (aXYPoint: TXYPoint; var aRec: TNBestRecArrayF): boolean;
//-----------------------------------------------------------
var r,c:integer;
begin
  Result := FindCellXY(aXYPoint, r,c);

  if result then
    GetItem (r,c, aRec);
end;

//--------------------------------------------------------------------
function TMatrixNBest.CreateFile(aFileName: string): Boolean;
//--------------------------------------------------------------------
begin
  Close;

  FmemData :=TMemoryStream.Create;
  FmemIndex:=TMemoryStream.Create;
  FmemIndex.SetSize (MAX_BUFFER_SIZE);
  FmemData.SetSize (MAX_BUFFER_SIZE);

  FIndexFileStream:=TFileStream.Create (aFileName + '.idx', fmCreate);
  FDataFileStream :=TFileStream.Create (aFileName + '.dat', fmCreate);

  FOpenMode:=fmCreate;
  Active:=True;

end;

//--------------------------------------------------------------------
procedure TMatrixNBest.Close;
//--------------------------------------------------------------------
begin
  if not Active then
    Exit;

  if FOpenMode=fmCreate then
  begin
    Commit();

    FreeAndNil(FmemData);
    FreeAndNil(FmemIndex);
  end;

  FreeAndNil(FIndexFileStream);
  FreeAndNil(FDataFileStream);

  Active:=False;

end;

//------------------------------------------------------
function TMatrixNBest.FindCellXY(aXYPoint: TXYPoint; var aRow, aCol: integer): boolean;
//------------------------------------------------------
// - ����� �������� ������� �� �����������
var r,c:integer;
begin
  // ������������ �������������� ����������; StartX,StartY - left top corner
  if (TopLeft.X >= aXYPoint.x) and (aXYPoint.x >= TopLeft.X-RowCount*(Step)) and
     (TopLeft.Y <= aXYPoint.y) and (aXYPoint.y <= TopLeft.Y+ColCount*(Step)) and
     (Step>0)
  then begin
    aRow:= trunc ((TopLeft.X-aXYPoint.X) / Step) ;
    aCol:= trunc ((aXYPoint.Y - TopLeft.Y) / Step);
    Result:=true; 
  end else
    Result:=false;
end;

//------------------------------------------------
function TMatrixNBest.Test_GetItem(aRow,aCol: integer; var aRec: TNBestRecArrayF): Boolean;
//------------------------------------------------
var  i,j,iIndex,iRead,iSectorCount : integer;

    rec: TciDataFileRecord;
    rec_: TciDataFileRecord;
    rIndexRec: TciIndexFileRecord;
    rIndexRec_: TciIndexFileRecord;
    iSize: Integer;

   //  p2,p3: PByteArray;
begin
  Result:=false;

  FillChar(aRec, SizeOf(aRec), 0);

 // FillChar(rec, SizeOf(rec), 0);
  FillChar(rec_, SizeOf(rec_), 0);


  if (aRow<0) or (aRow>=RowCount) or (aCol<0) or (aCol>=ColCount) then
    Exit;

  iIndex:=aRow*ColCount + aCol;

  // read position and datasize

  FIndexFileStreamV.ReadBuffer (iIndex * SizeOf(TciIndexFileRecord),
                                SizeOf(TciIndexFileRecord), @rIndexRec_);


  FIndexFileStream.Position := iIndex * SizeOf(TciIndexFileRecord);
  iRead:=FIndexFileStream.Read (rIndexRec, SizeOf(TciIndexFileRecord));
  if iRead <> SizeOf(TciIndexFileRecord) then
    Exit;

  Assert(rIndexRec.Position=rIndexRec_.Position,   Format('row: %d;  col: %d', [aRow,aCol])) ;


 // if (rIndexRec.Position <> High(Integer)) then
  if (rIndexRec_.Position <> High(Integer)) then
  begin


    FDataFileStreamV.ReadBuffer (rIndexRec_.Position,
                                 SizeOf(rec_.RecordCount),
                                 @rec_.RecordCount) ;



    FDataFileStreamV.ReadBuffer (rIndexRec_.Position + SizeOf(rec_.RecordCount),
                                 rec_.RecordCount * SizeOf(rec_.Items[0]),
                                 @rec_.Items);

    //---------------------------------------------------------

    FDataFileStream.Position:=rIndexRec.Position;
    iRead:=FDataFileStream.Read (rec.RecordCount, SizeOf(rec.RecordCount));

    iSize:=SizeOf(rec.Items[0]) * rec.RecordCount;
    iRead:=FDataFileStream.Read (rec.Items, iSize);


    FDataFileStreamV.ReadBuffer (rIndexRec.Position, SizeOf(rec.RecordCount), @rec.Items);

    //---------------------------------------------------------

    Assert(rec.RecordCount=rec_.RecordCount,   Format('row: %d;  col: %d', [aRow,aCol]));

    for I := 0 to rec_.RecordCount - 1 do
    begin
      Assert(rec.Items[i].id=rec_.Items[i].ID,   Format('row: %d;  col: %d', [aRow,aCol]));
      Assert(rec.Items[i].Value=rec_.Items[i].Value,   Format('row: %d;  col: %d', [aRow,aCol]))

    end;

  end;

  aRec.RecordCount:=rec_.RecordCount;
  for i := 0 to aRec.RecordCount - 1 do
    aRec.Items[i]:=rec_.Items[i];

  Result:=true;
end;

//----------------------------------------
procedure TMatrixNBest.SaveHeaderToFile(aFileName: string);
//----------------------------------------
var sSection: string;
    ini: TIniFile;
begin
  ini:=TIniFile.Create (aFileName);

  sSection:='main';
  ini.WriteInteger (sSection, 'StartX',   Round(TopLeft.X));
  ini.WriteInteger (sSection, 'StartY',   Round(TopLeft.Y));
  ini.WriteInteger (sSection, 'RowCount', RowCount);
  ini.WriteInteger (sSection, 'ColCount', ColCount);
  ini.WriteInteger (sSection, 'Step',     Step);
  ini.WriteInteger (sSection, 'ZoneNum',  ZoneNum);
  ini.WriteInteger (sSection, 'Version',  1);


  sSection:='NBest';
  ini.WriteInteger (sSection, 'MinEMP', Summary.MinEMP);
  ini.WriteInteger (sSection, 'MaxEMP', Summary.MaxEMP);
  ini.WriteInteger (sSection, 'MaxRecordCount',  MaxRecordCount);

  ini.Free;
end;

//----------------------------------------
function TMatrixNBest.LoadHeaderFromFile(aFileName: string): Boolean;
//----------------------------------------
var sSection: string;
    ini: TMemIniFile;
begin
  Assert(FileExists(aFileName), aFileName);

  Result := False;

  try
    if not FileExists(aFileName) then
      Exit;

    ini:=TMemIniFile.Create (aFileName);

    sSection:='main';
    TopLeft.X   := ini.ReadInteger (sSection, 'StartX',      0);
    TopLeft.Y   := ini.ReadInteger (sSection, 'StartY',      0);
    RowCount    := ini.ReadInteger (sSection, 'RowCount',    0);
    ColCount    := ini.ReadInteger (sSection, 'ColCount',    0);
    Step        := ini.ReadInteger (sSection, 'Step',        0);
    ZoneNum     := ini.ReadInteger (sSection, 'ZoneNum',     0);
    Version     := ini.ReadInteger (sSection, 'Version',     0);


    sSection:='NBest';
    Summary.MinEMP := ini.ReadInteger (sSection, 'MinEMP', 0);
    Summary.MaxEMP := ini.ReadInteger (sSection, 'MaxEMP', 0);
    MaxRecordCount := ini.ReadInteger (sSection, 'MaxRecordCount', 0);


    ini.Free;
  except end;


  Result := Version>0;
end;


//-------------------------------------------------------------------
procedure TMatrixNBest.SaveToTXT(aFileName: string);
//-------------------------------------------------------------------
var
  i,r,c: integer;
  rec: TNBestRecArrayF;
  S: string;

  F: TextFile;
begin
  if OpenFile(aFileName) then
  begin
    ForceDirectories(ExtractFileDir(aFileName));

    AssignFile(F, aFileName + '.txt');
    ReWrite(F);



    for r := 0 to RowCount - 1 do
    begin
      s:='';

      for c:=0 to ColCount-1 do
      begin
        GetItem(r,c,rec);

 
        if rec.RecordCount>0 then
          s:=s+ Format('%5d', [rec.Items[0].Value ] )
        else
          s:=s+'     ';

      end;

      Writeln(F,s);
//        s:=S + CellToStr (r,c);
    end;

    CloseFile(F);

    Close;
  end;

end;


end.


(*

procedure TMatrixNBest.SaveToTxtFile(aFileName: string);
const
//  DEF_ROW_COUNT= 100;
  DEF_COL_COUNT= 20;
  DEF_START = 327679;

var
    dir,str: string;
    bt: Byte;
    r,c: integer;
   // list: TStringList;

    F: TextFile;
  i: integer;
  iRead: integer;
begin
  dir:=ExtractFileDir (aFileName);
  if not DirectoryExists (dir) then  ForceDirectories(dir);


  AssignFile(F, aFileName + '.txt');
  ReWrite(F);



{
  for i:=0 to DEF_START+10 do

//  for i:=0 to FDataFileStream.Size-1 do
  begin
    iRead:=FDataFileStream.Read (bt, 1);

    str:=Format('%10d - %4d',[i,bt]);

    Writeln(F,str);
  end;
}
{
  for i:=0 to DEF_START-1  do
    iRead:=FDataFileStream.Read (bt, 1);
 }

// list:=TStringList.Create;
  for i:=0 to FDataFileStream.Size -1 do
  begin
    iRead:=FDataFileStream.Read (bt, 1);

  //ReadBuffer(i,1,bt);

    str:=Format('%4d',[bt]);

   // for c:=0 to ColCount-1 do
 //    str:=Str + Format('%3d',[]); ;

    Write(F,str);

    if (i>0) and (i mod DEF_COL_COUNT = 0) then

  //  if i mod DEF_COL_COUNT = 0 then
      Writeln(F);

  // list.Add(str);
  end;


// list.SaveToFile(aFileName + '.txt');
// list.Free;

  Writeln(F);
  CloseFile(F);

end;


