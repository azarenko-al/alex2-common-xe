unit u_Matrix_NBest_base;

interface
uses Classes,SysUtils,IniFiles,

     u_func,
     u_Geo;

type
{
  TNBestRec = packed record
    ID    : integer;  // ��� ������� (��������)
    Value : byte;     // ������� ���
  end;

  TNBestRecArrayF = record
     RecordCount: Byte;
     Items: array[0..High(Byte)] of TNBestRec;
   end;
}

  TCustomMatrixNBest = class
  private
    FOpenMode : word; //fmCreate,fmOpenRead;

    FIndexFileStream  : TFileStream;
    FDataFileStream   : TFileStream;
    FmemData          : TMemoryStream;
    FmemIndex         : TMemoryStream;

    FDataFilePosition : integer;
  private

    procedure Commit();
    
    function FindCellXY(aXYPoint: TXYPoint; var aRow, aCol: integer): boolean;
    function LoadHeaderFromFile(aFileName: string): Boolean;


  public
    Active   : boolean; // readonly

    TopLeft  : TXYPoint; // �����. ������ �������� ���� � ������
    ZoneNum  : integer;
    Version  : integer;

    RowCount,ColCount,Step : integer;

    MaxRecordCount : integer;

  {  Sensitivity    : integer;

    Summary : record
      MinEMP : integer; // min ������� �������
      MaxEMP : integer; // max ������� �������
    end;
}

    destructor Destroy; override;

    function CreateFile(aFileName: string): Boolean;
    procedure SaveHeaderToFile(aFileName: string);
    procedure Close;

    function OpenFile(aFileName: string): Boolean;

    procedure WriteBlankRecord();
    procedure WriteRecord(var aRec; aSize: integer);

    //array of TNBestRec
//    function FindBL (aPoint: TBLPoint; aZone: integer; var aRec: TNBestRecArrayF): boolean;     //; aZone: integer
//    function FindXY (aXYPoint: TXYPoint; var aRec: TNBestRecArrayF): boolean;

  //  function GetItem(aRow,aCol: integer; var aRec: TNBestRecArrayF): Boolean;
//    procedure SaveToTXT(aFileName: string);

//    property Items [Row,Col: integer]: TNBestRecArrayF read GetItem;  default;

  end;



//===================================================================
implementation
//===================================================================
const
  MAX_BUFFER_SIZE = 1024*1024*2;


type
    //-------------------------------------------
  // file records
  //-------------------------------------------
  TciIndexFileRecord = packed record
  //-------------------------------------------
    Position   : integer; // ������� ����� ������
  end;
{
  //-------------------------------------------
  TciDataFileRecord = packed record
  //-------------------------------------------
    RecordCount: Byte;
    Items: array[0..High(Byte)] of TNBestRec;
  end;
}


//---------------------------------------------
// TCustomMatrixNBest
//---------------------------------------------


destructor TCustomMatrixNBest.Destroy;
begin
  Close;

  inherited;
end;

//--------------------------------------------------------------------
function TCustomMatrixNBest.OpenFile(aFileName: string): Boolean;
//--------------------------------------------------------------------
begin
  Close;

  Active:=False;
 // FOpenMode:=aMode;

 // if FOpenMode=fmOpenRead then
  begin
    if (not FileExists(aFileName + '.idx')) or
       (not FileExists(aFileName + '.dat')) or
       (not FileExists(ChangeFileExt(aFileName,'.nbest')))
    then begin
//      Result := False;
      Exit;
    end;

    if not LoadHeaderFromFile (ChangeFileExt(aFileName,'.nbest')) then
      Exit;
  end;

  FIndexFileStream:=TFileStream.Create (aFileName + '.idx', fmOpenRead);
  FDataFileStream :=TFileStream.Create (aFileName + '.dat', fmOpenRead);

  Active:=true;
  Result := True;
end;


procedure TCustomMatrixNBest.WriteBlankRecord();
var
  rec: TciIndexFileRecord;
begin
  rec.Position:=High(Integer);
  FmemIndex.Write(rec, SizeOf(rec));

  if (FmemIndex.Position >= MAX_BUFFER_SIZE) then
    Commit();
end;

//---------------------------------------------
procedure TCustomMatrixNBest.Commit();
//---------------------------------------------
var iSize1,iSize2: integer;
begin
  iSize1:=FmemIndex.Position;
  FmemIndex.Position:=0;

  iSize2:=FmemData.Position;
  FmemData.Position:=0;

  if iSize1>0 then FIndexFileStream.CopyFrom (FmemIndex,iSize1);
  if iSize2>0 then FDataFileStream.CopyFrom  (FmemData, iSize2);

  FmemIndex.Position:=0;
  FmemData.Position:=0;
end;

//---------------------------------------------
procedure TCustomMatrixNBest.WriteRecord (var aRec; aSize: integer);
//---------------------------------------------
var i,iStartPos,iDataSize: integer;
    rIndexRec : TciIndexFileRecord;
begin
  // ����������, ������� ���� ���� ��������
  iStartPos:=FmemData.Position;

  // save channels if exist
  // - save each channel data
  // - save each channel sectors

  FmemData.Write (aRec, aSize);
//  for i:=0 to aRec.RecordCount-1 do
 //   FmemData.Write (aRec.Items[i], SizeOf(aRec.Items[i]));

  iDataSize:=FmemData.Position-iStartPos;

  // write index record: position(offset), datasize
  rIndexRec.Position   :=FDataFilePosition;
  FmemIndex.Write(rIndexRec, SizeOf(rIndexRec));

  // ������� �������� ������
  Inc(FDataFilePosition, iDataSize);

  if (FmemIndex.Position >= MAX_BUFFER_SIZE) or (FmemData.Position >= MAX_BUFFER_SIZE) then
    Commit();

end;

//------------------------------------------------
function TCustomMatrixNBest.GetItem(aRow,aCol: integer; var aRec: TNBestRecArrayF): Boolean;
//------------------------------------------------
var  i,j,iIndex,iRead,iSectorCount : integer;
     rec: TciDataFileRecord;
     rIndexRec: TciIndexFileRecord;
     iSize: Integer;
begin
  Result:=false;

  FillChar(aRec, SizeOf(aRec), 0);

  if (aRow<0) or (aRow>=RowCount) or (aCol<0) or (aCol>=ColCount) then
    Exit;

  iIndex:=aRow*ColCount + aCol;

  // read position and datasize
  FIndexFileStream.Position := iIndex * SizeOf(TciIndexFileRecord);
  iRead:=FIndexFileStream.Read (rIndexRec, SizeOf(TciIndexFileRecord));
  if iRead <> SizeOf(TciIndexFileRecord) then
    Exit;


  if (rIndexRec.Position <> High(Integer)) then
  begin
    FDataFileStream.Position:=rIndexRec.Position;

    iRead:=FDataFileStream.Read (rec.RecordCount, SizeOf(rec.RecordCount));

    iSize:=SizeOf(rec.Items[0]) * rec.RecordCount;
    iRead:=FDataFileStream.Read (rec.Items, iSize);

  end;

  aRec.RecordCount:=rec.RecordCount;
  for i := 0 to aRec.RecordCount - 1 do
    aRec.Items[i]:=rec.Items[i];

  Result:=true;
end;



//------------------------------------------------------
function TCustomMatrixNBest.FindBL(aPoint: TBLPoint; aZone: integer; var aRec: TNBestRecArrayF): boolean;
//------------------------------------------------------
var xyPoint: TXYPoint;
begin
  if not Active then
  begin
    Result:= False;
    Exit;
  end;

  xyPoint:=geo_BL_to_XY (aPoint,aZone);

  Result:=FindXY (xyPoint, aRec);
  Result:=Result and (aRec.RecordCount>0);
end;


//-----------------------------------------------------------
function TCustomMatrixNBest.FindXY (aXYPoint: TXYPoint; var aRec: TNBestRecArrayF): boolean;
//-----------------------------------------------------------
var r,c:integer;
begin
  Result := FindCellXY(aXYPoint, r,c);

  if result then
    GetItem (r,c, aRec);
end;

//--------------------------------------------------------------------
function TCustomMatrixNBest.CreateFile(aFileName: string): Boolean;
//--------------------------------------------------------------------
begin
  Close;

  FmemData :=TMemoryStream.Create;
  FmemIndex:=TMemoryStream.Create;
  FmemIndex.SetSize (MAX_BUFFER_SIZE);
  FmemData.SetSize (MAX_BUFFER_SIZE);

  FIndexFileStream:=TFileStream.Create (aFileName + '.idx', fmCreate);
  FDataFileStream :=TFileStream.Create (aFileName + '.dat', fmCreate);

  FOpenMode:=fmCreate;
  Active:=True;

end;

//--------------------------------------------------------------------
procedure TCustomMatrixNBest.Close;
//--------------------------------------------------------------------
begin
  if not Active then
    Exit;

  if FOpenMode=fmCreate then
  begin
    Commit();

    FreeAndNil(FmemData);
    FreeAndNil(FmemIndex);
  end;

  FreeAndNil(FIndexFileStream);
  FreeAndNil(FDataFileStream);

  Active:=False;

{
  if Active then
  begin
    if FOpenMode=fmCreate then
      Commit();

    FIndexFileStream.Free;
    FDataFileStream.Free;
    Active:=false;
  end;
}
end;

//------------------------------------------------------
function TCustomMatrixNBest.FindCellXY(aXYPoint: TXYPoint; var aRow, aCol: integer): boolean;
//------------------------------------------------------
// - ����� �������� ������� �� �����������
var r,c:integer;
begin
  // ������������ �������������� ����������; StartX,StartY - left top corner
  if (TopLeft.X >= aXYPoint.x) and (aXYPoint.x >= TopLeft.X-RowCount*(Step)) and
     (TopLeft.Y <= aXYPoint.y) and (aXYPoint.y <= TopLeft.Y+ColCount*(Step)) and
     (Step>0)
  then begin
    aRow:= trunc ((TopLeft.X-aXYPoint.X) / Step) ;
    aCol:= trunc ((aXYPoint.Y - TopLeft.Y) / Step);
    Result:=true; 
  end else
    Result:=false;
end;

//----------------------------------------
procedure TCustomMatrixNBest.SaveHeaderToFile(aFileName: string);
//----------------------------------------
var sSection: string;
    ini: TIniFile;
begin
  ini:=TIniFile.Create (aFileName);

  sSection:='main';
  ini.WriteInteger (sSection, 'StartX',   Round(TopLeft.X));
  ini.WriteInteger (sSection, 'StartY',   Round(TopLeft.Y));
  ini.WriteInteger (sSection, 'RowCount', RowCount);
  ini.WriteInteger (sSection, 'ColCount', ColCount);
  ini.WriteInteger (sSection, 'Step',     Step);
  ini.WriteInteger (sSection, 'ZoneNum',  ZoneNum);
  ini.WriteInteger (sSection, 'Version',  1);


  sSection:='NBest';
  ini.WriteInteger (sSection, 'MinEMP', Summary.MinEMP);
  ini.WriteInteger (sSection, 'MaxEMP', Summary.MaxEMP);
  ini.WriteInteger (sSection, 'MaxRecordCount',  MaxRecordCount);

  ini.Free;
end;

//----------------------------------------
function TCustomMatrixNBest.LoadHeaderFromFile(aFileName: string): Boolean;
//----------------------------------------
var sSection: string;
    ini: TMemIniFile;
begin
  Assert(FileExists(aFileName), aFileName);

  Result := False;

  try
    if not FileExists(aFileName) then
      Exit;

    ini:=TMemIniFile.Create (aFileName);

    sSection:='main';
    TopLeft.X   := ini.ReadInteger (sSection, 'StartX',      0);
    TopLeft.Y   := ini.ReadInteger (sSection, 'StartY',      0);
    RowCount    := ini.ReadInteger (sSection, 'RowCount',    0);
    ColCount    := ini.ReadInteger (sSection, 'ColCount',    0);
    Step        := ini.ReadInteger (sSection, 'Step',        0);
    ZoneNum     := ini.ReadInteger (sSection, 'ZoneNum',     0);
    Version     := ini.ReadInteger (sSection, 'Version',     0);


    sSection:='NBest';
    Summary.MinEMP := ini.ReadInteger (sSection, 'MinEMP', 0);
    Summary.MaxEMP := ini.ReadInteger (sSection, 'MaxEMP', 0);
    MaxRecordCount := ini.ReadInteger (sSection, 'MaxRecordCount', 0);


    ini.Free;
  except end;


  Result := Version>0;
end;


//-------------------------------------------------------------------
procedure TCustomMatrixNBest.SaveToTXT(aFileName: string);
//-------------------------------------------------------------------
var
  i,r,c: integer;
  rec: TNBestRecArrayF;
  S: string;

  F: TextFile;
begin
  if OpenFile(aFileName) then
  begin
    ForceDirectories(ExtractFileDir(aFileName));

    AssignFile(F, aFileName + '.txt');
    ReWrite(F);



    for r := 0 to RowCount - 1 do
    begin
      s:='';

      for c:=0 to ColCount-1 do
      begin
        GetItem(r,c,rec);

 
        if rec.RecordCount>0 then
          s:=s+ Format('%5d', [rec.Items[0].Value ] )
        else
          s:=s+'     ';

      end;

      Writeln(F,s);
//        s:=S + CellToStr (r,c);
    end;

    CloseFile(F);

    Close;
  end;

end;


end.


{


//-------------------------------------------------------------------
procedure TCustomLenMatrix.SaveToTxtFile(aFileName: string);
//-------------------------------------------------------------------
var dir,str: string;
    r,c: integer;
   // list: TStringList;

    F: TextFile;
begin
  dir:=ExtractFileDir (aFileName);
  if not DirectoryExists (dir) then  ForceDirectories(dir);




// list:=TStringList.Create;
  for r:=0 to RowCount-1 do
  begin
    str:='';
    for c:=0 to ColCount-1 do
     str:=Str + CellToStr (r,c);

    Writeln(F,str);

  // list.Add(str);
  end;
// list.SaveToFile(aFileName + '.txt');
// list.Free;

  CloseFile(F);

end;

}


