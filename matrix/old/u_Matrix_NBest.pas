unit u_Matrix_NBest;

interface
uses Classes,IniFiles,SysUtils,FileCtrl,

     u_progress,
     u_Matrix,
     u_matrix_base,
     u_geo,
     u_func;


type
  T6MaxRec = packed record
    Sector_ID : integer;  // ��� ������� (��������)
    EMP_value : byte;     // ������� ���
  end;

  //--------------------------------------------------
  // ������ ������ ��� ����������� � ���� "���������� ���"
  //--------------------------------------------------
  TRegion6MaxFileRec = packed
  record
    Items: array of T6MaxRec;
  end;

  //--------------------------------------------------
  // Object ��� ����������� ����������
  // �� 6 max EMP � ������ ( ID �������, + 6 ��������)
  //--------------------------------------------------
  TMatrixNBest = class(TCustomLenMatrix)
  //--------------------------------------------------
  private
    FRecordRowCount : integer; // ���-�� ��������� � ������ (��������)
    FBuffer         : array[0..50] of T6MaxRec;

    function  GetItem (aRow,aCol:integer): TRegion6MaxFileRec;
    procedure SetItem (aRow,aCol:integer; Value:TRegion6MaxFileRec);

    procedure DoOnSaveToIniFile   (Ini: TMemIniFile);
    procedure DoOnLoadFromIniFile (Ini: TMemIniFile);
    function Find (X,Y: double; var aRec: TRegion6MaxFileRec): boolean;

    procedure SetRecordRowCount (Value: integer);
  public
    Sensitivity  : integer;

    Summary : record
      MinEMP : integer; // min ������� �������
      MaxEMP : integer; // max ������� �������
    end;

    constructor Create;

    // ���������� �������� ����� �� �����������
    function FindBL (aPoint: TBLPoint; aZone:integer; var aRec:TRegion6MaxFileRec): boolean;

    property Items [Row,Col: integer]: TRegion6MaxFileRec read GetItem write SetItem;  default;
    property RecordRowCount: integer read FRecordRowCount write SetRecordRowCount;
  end;


  TMatrixNBestCalc = class(TProgress)
  public
    
     procedure Execute (aMatrix: TMatrixNBest;
                        kupMatrixList: TkupMatrixList;
                        aSensitivity: integer
                        );

  end;


//======================================================
implementation
//======================================================

//-------------------------------------------------
// TRegion6MaxMatrix
//-------------------------------------------------
constructor TMatrixNBest.Create;
begin
  inherited Create;

  OnSaveToIniFile:=DoOnSaveToIniFile;
  OnLoadFromIniFile:=DoOnLoadFromIniFile;
end;

procedure TMatrixNBest.SetRecordRowCount (Value: integer);
begin
  if Value=0 then Value:=6;
  FRecordRowCount:=Value;
  RecordLen:=FRecordRowCount * SizeOf(T6MaxRec);
end;

//------------------------------------------------------
function TMatrixNBest.FindBL (aPoint: TBLPoint; aZone:integer; var aRec:TRegion6MaxFileRec): boolean;
//------------------------------------------------------
var x,y:double;  iZone:integer;  xyPoint: TXYPoint;
begin
  xyPoint:=geo_BL_to_XY (aPoint, aZone);

  Result:=Find (xyPoint.X, xyPoint.Y, aRec);
end;

//------------------------------------------------------
function TMatrixNBest.Find (X,Y: double; var aRec: TRegion6MaxFileRec): boolean;
//------------------------------------------------------
var r,c:integer;  file_rec: TRegion6MaxFileRec;  // ������������ ���������� StartX,StartY - left top corner
begin
  if (TopLeft.X >= x) and (x >= TopLeft.X-RowCount*(Step)) and
     (TopLeft.Y <= y) and (y <= TopLeft.Y+ColCount*(Step)) and (Step>0)
  then begin
    r := trunc ((TopLeft.X-X) / Step) ;
    c := trunc ((y - TopLeft.Y) / Step);
    aRec := Items[r,c];
    Result:=true;
  end else
    Result:=false;
end;

function TMatrixNBest.GetItem (aRow,aCol: integer): TRegion6MaxFileRec;
var i: integer;
begin
  ReadBuffer (aRow,aCol, FBuffer);

  SetLength (Result.Items, FRecordRowCount);

  for i:=0 to FRecordRowCount-1 do
    Result.Items[i]:=FBuffer[i];
end;


procedure TMatrixNBest.SetItem (aRow,aCol:integer; Value:TRegion6MaxFileRec);
var i: integer;
begin
  for i:=0 to FRecordRowCount-1 do
    FBuffer[i]:=Value.Items[i];

  WriteBuffer (aRow,aCol, FBuffer); //FMemStream.Me Value);
end;

//-------------------------------------------------
procedure TMatrixNBest.DoOnSaveToIniFile (Ini: TMemIniFile);
//-------------------------------------------------
var section,dir: string; i: integer;
begin
  ini.WriteInteger ('general', 'RecordRowCount', RecordRowCount);
  ini.WriteInteger ('Summary', 'MinEMP', Summary.MinEMP);
  ini.WriteInteger ('Summary', 'MaxEMP', Summary.MaxEMP);
end;

//-------------------------------------------------
procedure TMatrixNBest.DoOnLoadFromIniFile (Ini: TMemIniFile);
//-------------------------------------------------
begin
  RecordRowCount := ini.ReadInteger ('general', 'RecordRowCount', RecordRowCount);
  Summary.MinEMP := ini.ReadInteger ('Summary', 'MinEMP', 0);
  Summary.MaxEMP := ini.ReadInteger ('Summary', 'MaxEMP', 0);
end;


//--------------------------------------------------
// TMatrix6MaxCalc
//--------------------------------------------------
procedure TMatrixNBestCalc.Execute (aMatrix: TMatrixNBest;
                                              kupMatrixList: TkupMatrixList;
                                              aSensitivity: integer
                                              );
//--------------------------------------------------
type TSortArrRec=record ID, Emp_level: integer; end;
var  arrSort: array of TSortArrRec;
     tmpRec: TSortArrRec;
     min_ind, max_ind: integer;
    // ������� ��� - ��� ����� (80,90,120)
    // � ������� ��������  ������������ �������� ���
    // � ���������� ������� �������

    //---------------------------------------------
    procedure DoPush (ID, EMP_level: integer);
    //---------------------------------------------
    var i: integer;
    begin //
      for i:=0 to High(arrSort) do begin
        if (arrSort[i].Emp_level=0) then begin arrSort[i].Emp_level:=Emp_level; arrSort[i].ID:=ID; Exit; end;

        // ����� MIN � MAX �������� � �������
        // - optimize it
        if (arrSort[i].Emp_level < EMP_level) then begin min_ind:=i end;
        if (arrSort[i].Emp_level > EMP_level) then begin max_ind:=i end;
      end;
      // ���� �������� EMP < max -> �������� �� ����� max
      if (EMP_level < arrSort[max_ind].EMP_level) then begin arrSort[max_ind].Emp_level:=EMP_level; arrSort[max_ind].ID:=ID; end;
    end;

    //---------------------------------------------
    procedure DoSortArr(); // ������������� ������ �� �����������
    //---------------------------------------------
    var i: integer; isChanged: boolean;
    begin
      repeat
        isChanged:=false;
        for i:=0 to High(arrSort)-1 do begin
          if arrSort[i+1].Emp_level=0 then Break; // �������� 2 ������ <> 0
          if (arrSort[i].Emp_level > arrSort[i+1].Emp_level) or
             (arrSort[i].Emp_level=0) // ���� �������� 0, � ������ - �� 0
          then begin tmpRec:=arrSort[i]; arrSort[i]:=arrSort[i+1]; arrSort[i+1]:=tmpRec; isChanged:=true; end;
        end;
      until isChanged=false;
    end;
    //---------------------------------------------

var file_rec: TRegion6MaxFileRec;
    r,c,i,k,sector_id,iValue: integer;
    x,y: double;
begin
  with aMatrix do begin
    SetLength (arrSort, RecordRowCount);
    SetLength (file_rec.Items, RecordRowCount);

    Summary.MinEMP:=0;
    Summary.MaxEMP:=0;

    if kupMatrixList.Count>0 then
      ZoneNum:=kupMatrixList[0].ZoneNum;
  end;



  with aMatrix do
  for r:=0 to RowCount-1 do
  begin
    for c:=0 to ColCount-1 do begin
    //  FillChar (rec.Items, SizeOf(rec), 0);
      x:=TopLeft.X - (r+1/2)*Step;
      y:=TopLeft.Y + (c+1/2)*Step;

      for k:=0 to RecordRowCount-1 do FillChar(arrSort[k], SizeOf(arrSort[k]), 0);
      min_ind:=0; max_ind:=0;

      with kupMatrixList do
        for i:=0 to Count-1 do begin
          sector_id:=Items[i].SectorID;
          if Items[i].FindPointXY (x,y, iValue) and (iValue >= aSensitivity) then
            DoPush (sector_id, Abs(iValue));
        end;

      // sort by EMP level
      DoSortArr(); // ������������� ������

      for i:=0 to High(arrSort) do
      if arrSort[i].EMP_level <> 0 then begin
        file_rec.Items[i].Sector_ID := arrSort[i].ID;
        file_rec.Items[i].EMP_value := arrSort[i].EMP_level;

        with Summary do begin
          if (MinEMP=0) or (-ABS(arrSort[i].EMP_level) < MinEMP) then MinEMP:=-ABS(arrSort[i].EMP_level);
          if (MaxEMP=0) or (-ABS(arrSort[i].EMP_level) > MaxEMP) then MaxEMP:=-ABS(arrSort[i].EMP_level);
        end;
      end else begin
        // ���� �������� ��� ������� - �������� ID ������� = -1
        file_rec.Items[i].Sector_ID := 0;
        file_rec.Items[i].EMP_value := 255;
      end;

      // ��� ������ ������ (��������)
      Items[r,c]:=file_rec;

    end;

    if Assigned(FOnProgress) then
      FOnProgress (r, RowCount, Terminated);

    if Terminated then Break;
  end;
end;



end.


