unit u_nbest_matrix;

interface
uses Classes,SysUtils,IniFiles,

     u_func,
     u_Geo;

type

  TNBestRec = packed record
    ID    : integer;  // ��� ������� (��������)
    Value : byte;     // ������� ���
  end;

  TNBestRecArrayF = record
     RecordCount: Byte;
     Items: array[0..High(Byte)] of TNBestRec;
   end;


  TMatrixNBest = class(TObject)
  private
    FOpenMode : word; //fmCreate,fmOpenRead;

    FIndexFileStream  : TFileStream;
    FDataFileStream   : TFileStream;
    FmemData          : TMemoryStream;
    FmemIndex         : TMemoryStream;

    FDataFilePosition : integer;
  private

    procedure Commit();
    function FindCellXY(aX, aY: double; var aRow, aCol: integer): boolean;
    function LoadHeaderFromFile(aFileName: string): Boolean;


  public
    Active   : boolean; // readonly

    TopLeft  : TXYPoint; // �����. ������ �������� ���� � ������
    ZoneNum  : integer;
    Version  : integer;

    RowCount,ColCount,Step : integer;

    MaxRecordCount : integer;
    Sensitivity    : integer;

    Summary : record
      MinEMP : integer; // min ������� �������
      MaxEMP : integer; // max ������� �������
    end;


    constructor Create;
    destructor Destroy; override;

    function CreateFile(aFileName: string): Boolean;
    procedure SaveHeaderToFile(aFileName: string);
    procedure CloseFile;

    function OpenFile(aFileName: string): Boolean;

    procedure WriteBlankRecord();
    procedure WriteRecord(var aRec: TNBestRecArrayF);

    //array of TNBestRec
    function FindBL (aPoint: TBLPoint; aZone: integer; var aRec: TNBestRecArrayF): boolean;     //; aZone: integer
    function FindXY (aX, aY: double; var aRec: TNBestRecArrayF): boolean;

    function GetItem(aRow,aCol: integer; var aRec: TNBestRecArrayF): Boolean;

//    property Items [Row,Col: integer]: TNBestRecArrayF read GetItem;  default;

  end;



//===================================================================
implementation
//===================================================================
const
  MAX_BUFFER_SIZE = 1024*1024*2;


type
    //-------------------------------------------
  // file records
  //-------------------------------------------
  TciIndexFileRecord = packed record
  //-------------------------------------------
    Position   : integer; // ������� ����� ������
    RecordCount: Byte;
  end;

  //-------------------------------------------
  TciDataFileRecord = packed record
  //-------------------------------------------
    Items: array[0..High(Byte)] of TNBestRec;
  end;



//---------------------------------------------
// TMatrixNBest
//---------------------------------------------
constructor TMatrixNBest.Create;
begin
  inherited Create;
end;


destructor TMatrixNBest.Destroy;
begin
  CloseFile;

  inherited;
end;

//--------------------------------------------------------------------
function TMatrixNBest.OpenFile(aFileName: string): Boolean;
//--------------------------------------------------------------------
begin
  CloseFile;

  Active:=False;
 // FOpenMode:=aMode;

 // if FOpenMode=fmOpenRead then
  begin
    if (not FileExists(aFileName + '.idx')) or
       (not FileExists(aFileName + '.dat')) or
       (not FileExists(ChangeFileExt(aFileName,'.nbest')))
    then begin
//      Result := False;
      Exit;
    end;

    if not LoadHeaderFromFile (ChangeFileExt(aFileName,'.nbest')) then
      Exit;
  end;

  FIndexFileStream:=TFileStream.Create (aFileName + '.idx', fmOpenRead);
  FDataFileStream :=TFileStream.Create (aFileName + '.dat', fmOpenRead);

  Active:=true;
  Result := True;
end;


procedure TMatrixNBest.WriteBlankRecord();
var
  rec: TciIndexFileRecord;
begin
  FillChar (rec, SizeOf(rec), 0);
  FmemIndex.Write(rec, SizeOf(rec));

  if (FmemIndex.Position >= MAX_BUFFER_SIZE) then
    Commit();
end;

//---------------------------------------------
procedure TMatrixNBest.Commit();
//---------------------------------------------
var iSize1,iSize2: integer;
begin
  iSize1:=FmemIndex.Position;
  FmemIndex.Position:=0;

  iSize2:=FmemData.Position;
  FmemData.Position:=0;

  if iSize1>0 then FIndexFileStream.CopyFrom (FmemIndex,iSize1);
  if iSize2>0 then FDataFileStream.CopyFrom  (FmemData, iSize2);

  FmemIndex.Position:=0;
  FmemData.Position:=0;
end;

//---------------------------------------------
procedure TMatrixNBest.WriteRecord (var aRec: TNBestRecArrayF);
//---------------------------------------------
var i,iStartPos,iDataSize: integer;
  rIndexRec : TciIndexFileRecord;
begin
  // ����������, ������� ���� ���� ��������
  iStartPos:=FmemData.Position;

  // save channels if exist
  // - save each channel data
  // - save each channel sectors

  for i:=0 to aRec.RecordCount-1 do
    FmemData.Write (aRec.Items[i], SizeOf(aRec.Items[i]));

  iDataSize:=FmemData.Position-iStartPos;

  // write index record: position(offset), datasize
  rIndexRec.Position   :=FDataFilePosition;
  rIndexRec.RecordCount:=aRec.RecordCount;
//  aIndexRec.DataSize:=iDataSize;
  FmemIndex.Write(rIndexRec, SizeOf(rIndexRec));

  // ������� �������� ������
  Inc(FDataFilePosition, iDataSize);

  if (FmemIndex.Position >= MAX_BUFFER_SIZE) or (FmemData.Position >= MAX_BUFFER_SIZE) then
    Commit();

end;

//------------------------------------------------
//array of TNBestRec
function TMatrixNBest.GetItem(aRow,aCol: integer; var aRec: TNBestRecArrayF): Boolean;
//------------------------------------------------
var i,j,iIndex,iRead,iSectorCount : integer;
     rec: TciDataFileRecord;
     rIndexRec: TciIndexFileRecord;
  iSize: Integer;
begin
  Result:=false;

  FillChar(aRec, SizeOf(aRec), 0);

  if (aRow<0) or (aRow>=RowCount) or (aCol<0) or (aCol>=ColCount) then
    Exit;

  iIndex:=aRow*ColCount + aCol;

  // read position and datasize
  FIndexFileStream.Position := iIndex * SizeOf(TciIndexFileRecord);
  iRead:=FIndexFileStream.Read (rIndexRec, SizeOf(TciIndexFileRecord));
  if iRead <> SizeOf(TciIndexFileRecord) then
    Exit;


  if (rIndexRec.Position > 0) then
  begin
    FDataFileStream.Position:=rIndexRec.Position;


 //   iRead:=FDataFileStream.Read (rec.RecordCount, SizeOf(rec.RecordCount));

  //  SetLength (rec.Items, rec.RecordCount);

    for i:=0 to rIndexRec.RecordCount-1 do
    begin
      iSize:=SizeOf(rec.Items[0]) * rIndexRec.RecordCount;
      iRead:=FDataFileStream.Read (rec.Items, iSize);
    end;


  end;

  aRec.RecordCount:=rIndexRec.RecordCount;
  for i := 0 to aRec.RecordCount - 1 do
    aRec.Items[i]:=rec.Items[i];

 // aItems:=rec.Items;

  Result:=true;
end;



//------------------------------------------------------
function TMatrixNBest.FindBL(aPoint: TBLPoint; aZone: integer; var aRec: TNBestRecArrayF): boolean;
//------------------------------------------------------
var xyPoint: TXYPoint;
begin
  if not Active then
  begin
    Result:= False;
    Exit;
  end;

  xyPoint:=geo_BL_to_XY (aPoint,aZone);

  Result:=FindXY (xyPoint.x, xyPoint.y, aRec);
  Result:=Result and (aRec.RecordCount>0);
end;


//-----------------------------------------------------------
function TMatrixNBest.FindXY(aX, aY: double; var aRec: TNBestRecArrayF): boolean;
//-----------------------------------------------------------
var r,c:integer;
begin
  Result := FindCellXY(aX, aY, r,c);

  if result then
    GetItem (r,c, aRec);
end;

//--------------------------------------------------------------------
function TMatrixNBest.CreateFile(aFileName: string): Boolean;
//--------------------------------------------------------------------
begin
  CloseFile;

  FmemData :=TMemoryStream.Create;
  FmemIndex:=TMemoryStream.Create;
  FmemIndex.SetSize (MAX_BUFFER_SIZE);
  FmemData.SetSize (MAX_BUFFER_SIZE);

  FIndexFileStream:=TFileStream.Create (aFileName + '.idx', fmCreate);
  FDataFileStream :=TFileStream.Create (aFileName + '.dat', fmCreate);

  FOpenMode:=fmCreate;
  Active:=True;

end;

//--------------------------------------------------------------------
procedure TMatrixNBest.CloseFile;
//--------------------------------------------------------------------
begin
  if not Active then
    Exit;

  if FOpenMode=fmCreate then
  begin
    Commit();

    FreeAndNil(FmemData);
    FreeAndNil(FmemIndex);
  end;

  FreeAndNil(FIndexFileStream);
  FreeAndNil(FDataFileStream);

  Active:=False;

{
  if Active then
  begin
    if FOpenMode=fmCreate then
      Commit();

    FIndexFileStream.Free;
    FDataFileStream.Free;
    Active:=false;
  end;
}
end;

//------------------------------------------------------
function TMatrixNBest.FindCellXY(aX, aY: double; var aRow, aCol: integer): boolean;
//------------------------------------------------------
// - ����� �������� ������� �� �����������
var r,c:integer;
begin
  // ������������ �������������� ����������; StartX,StartY - left top corner
  if (TopLeft.X >= ax) and (ax >= TopLeft.X-RowCount*(Step)) and
     (TopLeft.Y <= ay) and (ay <= TopLeft.Y+ColCount*(Step)) and
     (Step>0)
  then begin
    aRow:= trunc ((TopLeft.X-aX) / Step) ;
    aCol:= trunc ((ay - TopLeft.Y) / Step);
    Result:=true; 
  end else
    Result:=false;
end;

//----------------------------------------
procedure TMatrixNBest.SaveHeaderToFile(aFileName: string);
//----------------------------------------
var sSection: string;
    ini: TIniFile;
begin
  ini:=TIniFile.Create (aFileName);

  sSection:='main';
  ini.WriteInteger (sSection, 'StartX',   Round(TopLeft.X));
  ini.WriteInteger (sSection, 'StartY',   Round(TopLeft.Y));
  ini.WriteInteger (sSection, 'RowCount', RowCount);
  ini.WriteInteger (sSection, 'ColCount', ColCount);
  ini.WriteInteger (sSection, 'Step',     Step);
  ini.WriteInteger (sSection, 'ZoneNum',  ZoneNum);
  ini.WriteInteger (sSection, 'Version',  1);


  sSection:='NBest';
  ini.WriteInteger (sSection, 'MinEMP', Summary.MinEMP);
  ini.WriteInteger (sSection, 'MaxEMP', Summary.MaxEMP);
  ini.WriteInteger (sSection, 'MaxRecordCount',  MaxRecordCount);

  ini.Free;
end;

//----------------------------------------
function TMatrixNBest.LoadHeaderFromFile(aFileName: string): Boolean;
//----------------------------------------
var sSection: string;
    ini: TMemIniFile;
begin
  Assert(FileExists(aFileName), aFileName);

  Result := False;

  try
    if not FileExists(aFileName) then
      Exit;

    ini:=TMemIniFile.Create (aFileName);

    sSection:='main';
    TopLeft.X   := ini.ReadInteger (sSection, 'StartX',      0);
    TopLeft.Y   := ini.ReadInteger (sSection, 'StartY',      0);
    RowCount    := ini.ReadInteger (sSection, 'RowCount',    0);
    ColCount    := ini.ReadInteger (sSection, 'ColCount',    0);
    Step        := ini.ReadInteger (sSection, 'Step',        0);
    ZoneNum     := ini.ReadInteger (sSection, 'ZoneNum',     0);
    Version     := ini.ReadInteger (sSection, 'Version',     0);


    sSection:='NBest';
    Summary.MinEMP := ini.ReadInteger (sSection, 'MinEMP', 0);
    Summary.MaxEMP := ini.ReadInteger (sSection, 'MaxEMP', 0);
    MaxRecordCount := ini.ReadInteger (sSection, 'MaxRecordCount', 0);


    ini.Free;
  except end;


  Result := Version>0;
end;


end.


