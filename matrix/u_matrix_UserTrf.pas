unit u_matrix_UserTrf;

interface

uses
  u_Matrix_base;


type
   TTrfCell = packed record
     Sector_ID : integer;
     Coverage  : integer;
     given_traffic : double;
     calc_traffic : double;
   end;

type
   TUserTrfRec = packed record
     Sector_ID : integer;
     Value     : Double;
   end;

type
  //-------------------------------------------------
  TUserTrfMatrix = class(TCustomLenMatrix)
  //-------------------------------------------------

    function  GetItem (aRow,aCol:integer): TUserTrfRec;
    procedure SetItem (aRow,aCol:integer; Value:TUserTrfRec);
    function Find (X,Y: double; var aRec: TUserTrfRec): boolean;

  public

    Cells : array of TTrfCell;

    constructor Create;

    function LoadHeaderFromFile (aFileName: string): boolean; override;
    procedure SaveHeaderToFile(aFileName: string; aRowCount, aColCount, aMaxValue : integer);overload; override;
    procedure SaveHeaderToFile(aFileName: string);overload; override;

    function FindCellIndexByID(aID : integer) : integer;

    function FindBL (aPoint: TBLPoint; aZone:integer; var aRec:TUserTrfRec): boolean;
    property Items [Row,Col: integer]: TUserTrfRec read GetItem write SetItem;  default;

  end;

implementation

//----------------------------------------------------------------
constructor TUserTrfMatrix.Create();
//----------------------------------------------------------------
var oRec : TUserTrfRec;
begin
  inherited ;

  FOpenMode := mtAsVirtFile;
  FRecordLen := SizeOf(oRec);
end;

//------------------------------------------------------
function TUserTrfMatrix.Find (X,Y: double; var aRec: TUserTrfRec): boolean;
//------------------------------------------------------
var r,c:integer;  file_rec: TUserTrfRec;  // использовать координаты StartX,StartY - left top corner
begin
  if (TopLeft.X >= x) and (x >= TopLeft.X-RowCount*(Step)) and
     (TopLeft.Y <= y) and (y <= TopLeft.Y+ColCount*(Step)) and (Step>0)
  then begin
    r := trunc ((TopLeft.X-X) / Step) ;
    c := trunc ((y - TopLeft.Y) / Step);
    aRec := Items[r,c];
    Result:=true;
  end else
    Result:=false;
end;

//------------------------------------------------------
function TUserTrfMatrix.FindBL (aPoint: TBLPoint; aZone:integer; var aRec:TUserTrfRec): boolean;
//------------------------------------------------------
var x,y:double;  iZone:integer;  xyPoint: TXYPoint;
begin
  xyPoint:=geo_BL_to_XY (aPoint, 0, aZone);

  Result:=Find (xyPoint.X, xyPoint.Y, aRec);
end;

//----------------------------------------------
function TUserTrfMatrix.FindCellIndexByID(aID : integer) : integer;
//----------------------------------------------
var i : integer;
begin
  Result := -1;
  for i:=0 to High(Cells) do
    if Cells[i].Sector_ID=aID then
    begin
      Result := i;
      Exit;
    end;

end;

//----------------------------------------------------------------
function TUserTrfMatrix.GetItem (aRow,aCol: integer): TUserTrfRec;
//----------------------------------------------------------------
var i: integer;
begin
  if not ReadBuffer (aRow,aCol, Result)
    then begin
      Result.Sector_ID:=-1;
      Result.Value:=0;
    end;
end;

//------------------------------------------------------
function TUserTrfMatrix.LoadHeaderFromFile (aFileName: string): boolean;
//------------------------------------------------------
var oIni: TMemIniFile;
    iCount, i : integer;
    oCellRec : TTrfCell;
begin
  Result := inherited LoadHeaderFromFile(aFileName);

  if Result then
    with FIniFile do begin

      iCount:=ReadInteger ('CELLS', 'Count', 0);
      SetLength(Cells, iCount);

      for i:=0 to iCount-1 do
      begin
        oCellRec.Sector_ID:=ReadInteger ('CELLS', VarToStr(i)+' ID', 0);
        oCellRec.Coverage:=ReadInteger ('CELLS', VarToStr(i)+' cov', 0);
        oCellRec.given_traffic:= AsFloat( ReadString ('CELLS', VarToStr(i)+' trf', '0'));
//        oCellRec.calc_traffic :=AsFloat(ReadString ('CELLS', VarToStr(i)+' calc_trf', 0);
        Cells[i]:=oCellRec;
      end;
    end;

end;

//-------------------------------------------
procedure TUserTrfMatrix.SaveHeaderToFile(aFileName: string);
//-------------------------------------------
var i : integer;
begin
  inherited;

  FIniFile.WriteInteger('CELLS', 'count', Length(Cells));

  for i:=0 to High(Cells) do
  begin
    FIniFile.WriteInteger('CELLS', VarToStr(i)+' ID', Cells[i].Sector_ID);
    FIniFile.WriteInteger('CELLS', VarToStr(i)+' cov', Cells[i].Coverage);
    FIniFile.WriteFloat('CELLS', VarToStr(i)+' trf', Cells[i].given_traffic);
//    FIniFile.WriteFloat('CELLS', VarToStr(i)+' calc_trf', Cells[i].calc_traffic);
  end;

  FIniFile.UpdateFile;
end;

//-------------------------------------------
procedure TUserTrfMatrix.SaveHeaderToFile(aFileName: string; aRowCount, aColCount, aMaxValue : integer);
//-------------------------------------------
var i : integer;
begin
  inherited;

  FIniFile.WriteInteger('CELLS', 'count', Length(Cells));

  for i:=0 to High(Cells) do
  begin
    FIniFile.WriteInteger('CELLS', VarToStr(i)+' ID', Cells[i].Sector_ID);
    FIniFile.WriteInteger('CELLS', VarToStr(i)+' cov', Cells[i].Coverage);
    FIniFile.WriteFloat('CELLS', VarToStr(i)+' trf', Cells[i].given_traffic);
//    FIniFile.WriteFloat('CELLS', VarToStr(i)+' calc_trf', Cells[i].calc_traffic);
  end;

  FIniFile.UpdateFile;
end;

//-------------------------------------------
procedure TUserTrfMatrix.SetItem (aRow,aCol:integer; Value:TUserTrfRec);
var i: integer;
begin
  WriteBuffer (aRow,aCol, Value); //FMemStream.Me Value);
end;


end.
