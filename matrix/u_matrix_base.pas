unit u_Matrix_base;

interface
uses Classes, SysUtils, IniFiles, FileCtrl,
     u_Geo,
     u_files,
     
     u_VirtFile;

type
  TMatrixRecordType = (dtNone,dtCustomLen,dtByte,dtShortint,dtInteger, dtFloat);

//  TMatrixOpenMode = (omInMemory, omVirtualOpenReadWrite, omVirtualOpenRead);


  TOnSaveToIniFileEvent   = procedure (ini: TMemIniFile) of object;
  TOnLoadFromIniFileEvent = procedure (ini: TMemIniFile) of object;

  //-------------------------------------------------
  // ������� � ������������ �������� ������ (������)
  //-------------------------------------------------
  TCustomLenMatrix = class
  //-------------------------------------------------
  private
    FMemoryStream : TMemoryStream;

    FOnSaveToFile   : TOnSaveToIniFileEvent;
    FOnLoadFromFile : TOnLoadFromIniFileEvent;

    //???
   // NotNullCellCount : integer; //���-�� ��������� ���������

//    FOpenMode: TMatrixOpenMode; //(mtInMemory,mtAsVirtFile);
    FRowCount,FColCount : integer;

    procedure SetRecordType  (Value: TMatrixRecordType);
    procedure SetRecordLen (aValue: word);


  protected
    FOpenMode: (mtInMemory,mtAsVirtFile);
    FVirtualFile  : TVirtualFile;

    FRecordType   : TMatrixRecordType;

    FRecordLen    : word   ; // ����� ������ ������� � ������
    FBlankValue   : integer; // �������� ��� ������� �������

    function CellToStr (ARow,ACol: integer): string; virtual; abstract;

  public
    FileName: string;

    TopLeft  : TXYPoint; // �����. ������ �������� ���� � ������
    StartB   : double;  // �����. ������ �������� ���� � ��������
    StartL   : double;  // �����. ������ �������� ����
    Step     : integer;
    SectorID : integer;
    CalcRadius_m : double;
    ZoneNum  : integer;

 //   HeaderRowCount,HeaderColCount : integer;

    constructor Create;
    destructor Destroy; override;

    function  GetBLBounds (): TBLRect;
    function  GetXYBounds (): TXYRect;

    procedure ClearMemory;
    procedure SetGeoParams (aStartX,aStartY: double; aStep: Integer; aZoneNum: integer);
    procedure Clear;

    function CellCount(): integer;

    procedure SetMatrixSize (aRowCount,aColCount: integer;
                             aRecordLen: integer=0);

    function FindCellXY (aX,aY: double; var aRow,aCol: integer): boolean;

    procedure SetNullCell (aRow,aCol: integer); virtual; abstract;

    function ReadBuffer(aRow, aCol: integer; aBuffer: Pointer): boolean;
    procedure WriteBuffer(aRow, aCol: integer; aBuffer: Pointer);

    function  LoadHeaderFromFile (aFileName: string): boolean; virtual;
    procedure SaveHeaderToFile (aFileName: string); overload; virtual;
    procedure SaveHeaderToFile (aFileName: string; aRowCount,
        aColCount, aMaxValue : integer; aGeoZone: Integer); overload; virtual;


    function CheckBinFile (aFileName: string): boolean;

    function  LoadFromFile (aFileName: string; aOpenMode: word = fmOpenRead): boolean;
//    function  LoadFromFile (aFileName: string; aOpenMode: TMatrixOpenMode = omInMemory): boolean;
    procedure SaveToFile   (aFileName: string);
    procedure SaveToTxtFile (aFileName: string); virtual;

    procedure FillBlankOutOfRadius (aX,aY: double; aRadius: double);

    //cell functions
    function  IsNull (aRow,aCol:integer): boolean; virtual; abstract;// NUKK value ?
    function  GetCellGeoCenter (aRow,aCol: integer): TXYPoint;

    property OnSaveToIniFile  : TOnSaveToIniFileEvent read FOnSaveToFile write FOnSaveToFile;
    property OnLoadFromIniFile: TOnLoadFromIniFileEvent read FOnLoadFromFile write FOnLoadFromFile;

    property BlankValue  : integer read FBlankValue;
    property NullValue  : integer read FBlankValue;

    property RecordLen   : word              read FRecordLen  write SetRecordLen;
    property RecordType  : TMatrixRecordType read FRecordType write SetRecordType;

    property RowCount: integer read FRowCount write FRowCount;
    property ColCount: integer read FColCount write FColCount;
  end;


  procedure CreateIntMatrix(aFileName: string;
                            aStartX,aStartY:double;
                            aRowCount,aColCount:integer;
                            aStep:integer;
                            aGeoZone: Integer);



//=================================================
implementation
//=================================================

const
  INI_GENERAL = 'GENERAL';

type
  TBufferRec= packed record case Byte of
               1:(AsByte     : byte);
               2:(AsShortint : shortint);
               3:(AsInteger  : integer);
               4:(AsFloat    : single);
             end;

//  function GetFileSize_ (aFileName: string): Longint; forward;

//*************************************************
//  TCustomLenMatrix
//*************************************************
constructor TCustomLenMatrix.Create; 
begin
  inherited Create;
  FMemoryStream:=TMemoryStream.Create;
  FVirtualFile:=TVirtualFile.Create;
end;

destructor TCustomLenMatrix.Destroy;
begin
  Clear;

  FVirtualFile.Free;
  FMemoryStream.Free;

  inherited Destroy;
end;


function TCustomLenMatrix.GetBLBounds(): TBLRect;
var xyRect: TxyRect;
begin
{
  xyRect.TopLeft    :=MakeXYPoint(TopLeft.X,TopLeft.Y);
  xyRect.BottomRight:=MakeXYPoint(TopLeft.X-RowCount*Step, TopLeft.Y+ColCount*Step);
}
  raise Exception.Create('Result:=geo_XYRect_to_BLRect (GetXYBounds());');
//  Result:=geo_XYRect_to_BLRect (GetXYBounds());

end;


function TCustomLenMatrix.GetXYBounds(): TXYRect;
begin
  Result.TopLeft    :=MakeXYPoint(TopLeft.X,TopLeft.Y);
  Result.BottomRight:=MakeXYPoint(TopLeft.X-RowCount*Step, TopLeft.Y+ColCount*Step);
end;

//-------------------------------------------------------------------
procedure TCustomLenMatrix.SetMatrixSize;
//-------------------------------------------------------------------
begin
  if ARowCount<0 then ARowCount:=0;
  if AColCount<0 then AColCount:=0;

  FRowCount:=ARowCount;
  FColCount:=AColCount;
  if aRecordLen>0 then RecordLen:=aRecordLen;

  if FOpenMode = mtInMemory then begin
    FMemoryStream.SetSize (RowCount*ColCount*RecordLen);
  end;

end;


procedure TCustomLenMatrix.SetGeoParams;
begin
  TopLeft.X:=aStartX; TopLeft.Y:=aStartY; Step:=aStep; ZoneNum:=aZoneNum;
end;

procedure TCustomLenMatrix.SetRecordType;
begin
  FRecordType:=Value;
  case FRecordType of
    dtByte    : begin RecordLen:=1; FBlankValue:=High(Byte); end;
    dtShortint: begin RecordLen:=1; FBlankValue:=High(Shortint); end;
    dtInteger : begin RecordLen:=4; FBlankValue:=High(Integer); end;
    else FBlankValue:=0;
  end;
end;

//-------------------------------------------------
procedure TCustomLenMatrix.Clear;
//-------------------------------------------------
begin
  TopLeft.X:=0; // TXYPoint; // �����. ������ �������� ���� � ������
  TopLeft.Y:=0; // TXYPoint; // �����. ������ �������� ���� � ������

  StartB   :=0; // double;  // �����. ������ �������� ���� � ��������
  StartL   :=0; //  double;  // �����. ������ �������� ����
  Step     :=0; //  integer;
  SectorID :=0; //  integer;
  ZoneNum  :=0; //  integer;

  ClearMemory;

  SetMatrixSize (0,0);
end;

//-------------------------------------------------
procedure TCustomLenMatrix.ClearMemory;
//-------------------------------------------------
begin
  FRowCount:=0;
  FColCount:=0;

  FMemoryStream.Clear;
  FVirtualFile.Close;
end;


//-------------------------------------------------
function TCustomLenMatrix.ReadBuffer(aRow, aCol: integer; aBuffer: Pointer):
    boolean;
//-------------------------------------------------
var
  src: Pointer;
  iOffs, iTmpOffs : longint;
 // an : TAccessMode;
begin
  Assert(FRecordLen<>0);

  if not ((aRow>=0) and (aCol>=0) and
          (aRow<RowCount) and (aCol<ColCount)) then
  begin
    Result := true;  Exit;
  end;

  if FOpenMode = mtInMemory then
  begin
    src := Pointer(Longint(FMemoryStream.Memory) + (ColCount*aRow + aCol) * FRecordLen);
    Move (src^, aBuffer^, FRecordLen);
    Result:=true;
  end else

  if FOpenMode = mtAsVirtFile then
  begin
    Result := FVirtualFile.ReadRecord (ColCount*aRow + aCol, aBuffer);
  end;


//  Result:=true;
end;


//-------------------------------------------------
procedure TCustomLenMatrix.WriteBuffer(aRow, aCol: integer; aBuffer: Pointer);
//-------------------------------------------------
var src: Pointer;
begin
  if not ((aRow>=0) and (aCol>=0) and
         (aRow<RowCount) and (aCol<ColCount))
  then Exit;


  if FOpenMode = mtInMemory then
  begin
    src :=Pointer(Longint(FMemoryStream.Memory) + (ColCount*aRow + aCol)*FRecordLen);
    Move (aBuffer^, src^, FRecordLen);
  end else


  if FOpenMode = mtAsVirtFile then
  begin
    FVirtualFile.WriteRecord (ColCount*aRow + aCol, aBuffer);
  end else
    raise Exception.Create('');

end;


//------------------------------------------------------
function TCustomLenMatrix.FindCellXY (aX,aY: double; var aRow,aCol: integer): boolean;
//------------------------------------------------------
// - ����� �������� ������� �� �����������
var r,c:integer;
begin
  // ������������ �������������� ����������; StartX,StartY - left top corner
  if (TopLeft.X >= ax) and (ax >= TopLeft.X-RowCount*(Step)) and
     (TopLeft.Y <= ay) and (ay <= TopLeft.Y+ColCount*(Step)) and
     (Step>0)
  then begin
    aRow:= trunc ((TopLeft.X-aX) / Step) ;
    aCol:= trunc ((ay - TopLeft.Y) / Step);

    Result:= (aRow<RowCount) and (aCol<ColCount);
  //  Result:=true;
  end else
    Result:=false;
end;

//------------------------------------------------------
procedure TCustomLenMatrix.SaveHeaderToFile (aFileName: string; aRowCount,
        aColCount, aMaxValue : integer; aGeoZone: Integer);
//------------------------------------------------------
var ini: TMemIniFile;
begin
   ForceDirectories(ExtractFileDir (aFileName));
   RowCount := aRowCount;
   ColCount := aColCount;

   ZoneNum := aGeoZone; // geo_Get6ZoneXY(TopLeft.Y);

   ini:=TMemIniFile.Create (aFileName);
   with ini do
   begin

     WriteInteger (INI_GENERAL, 'StartX',    Round(TopLeft.X));
     WriteInteger (INI_GENERAL, 'StartY',    Round(TopLeft.Y));
     WriteInteger (INI_GENERAL, 'EndY',      Round(TopLeft.Y)+ColCount*Step);
     WriteFloat   (INI_GENERAL, 'StartB',    StartB);
     WriteFloat   (INI_GENERAL, 'StartL',    StartL);
     WriteInteger (INI_GENERAL, 'RowCount',  RowCount);
     WriteInteger (INI_GENERAL, 'ColCount',  ColCount);
     WriteInteger (INI_GENERAL, 'Step',      Step    );
     WriteInteger (INI_GENERAL, 'ZoneNum',   ZoneNum);

     WriteInteger (INI_GENERAL, 'TopLeft_X', Round(TopLeft.X));
     WriteInteger (INI_GENERAL, 'TopLeft_Y', Round(TopLeft.Y));

     WriteInteger ('MATRIX', 'RecordType',   Integer(FRecordType));
     WriteInteger ('MATRIX', 'RecordLen',    FRecordLen);

   end;    // with

   ini.WriteInteger ('Summary', 'MinValue', 1);
   ini.WriteInteger ('Summary', 'MaxValue', aMaxValue);

   ini.UpdateFile;
   ini.Free;
end;

//------------------------------------------------------
procedure TCustomLenMatrix.SaveHeaderToFile (aFileName: string);
//------------------------------------------------------
var ini: TMemIniFile;
begin
   ForceDirectories(ExtractFileDir (aFileName));

   ini:=TMemIniFile.Create (aFileName);
   with ini do
   begin
     WriteInteger (INI_GENERAL, 'StartX',       Round(TopLeft.X));
     WriteInteger (INI_GENERAL, 'StartY',       Round(TopLeft.Y));
     WriteInteger (INI_GENERAL, 'EndY',         Round(TopLeft.Y)+ColCount*Step);
     WriteFloat   (INI_GENERAL, 'StartB',       StartB);
     WriteFloat   (INI_GENERAL, 'StartL',       StartL);
     WriteInteger (INI_GENERAL, 'RowCount',     RowCount);
     WriteInteger (INI_GENERAL, 'ColCount',     ColCount);
     WriteInteger (INI_GENERAL, 'Step',         Step    );
     WriteInteger (INI_GENERAL, 'ZoneNum',      ZoneNum);
  // i.WriteInteger (INI_GENERAL, 'NotNullCellCount',  NotNullCellCount);

     WriteInteger (INI_GENERAL, 'TopLeft_X',    Round(TopLeft.X));
     WriteInteger (INI_GENERAL, 'TopLeft_Y',    Round(TopLeft.Y));
  // i.WriteInteger (INI_GENERAL, 'BottomRight_X',   Round(BottomRight.X));
  // i.WriteInteger (INI_GENERAL, 'BottomRight_Y',   Round(BottomRight.Y));

     WriteInteger ('MATRIX', 'RecordType', Integer(FRecordType));
     WriteInteger ('MATRIX', 'RecordLen',  FRecordLen);

   end;    // with

   if Assigned(FOnSaveToFile) then
     FOnSaveToFile (ini);

   ini.UpdateFile;
   ini.Free;
end;

//---------------------------------------------------------------
procedure CreateIntMatrix(aFileName: string;
                          aStartX,aStartY:double;
                          aRowCount,aColCount:integer;
                          aStep:integer;
                          aGeoZone: Integer);
//---------------------------------------------------------------
var ini: TMemIniFile;
    oFileStream : TFileStream;
begin
   ForceDirectories(ExtractFileDir (aFileName));

   with TMemIniFile.Create (aFileName) do
   begin
     WriteInteger (INI_GENERAL, 'StartX',       Round(aStartX));
     WriteInteger (INI_GENERAL, 'StartY',       Round(aStartY));
     WriteInteger (INI_GENERAL, 'RowCount',     aRowCount);
     WriteInteger (INI_GENERAL, 'ColCount',     aColCount);
     WriteInteger (INI_GENERAL, 'Step',         aStep    );
     WriteInteger (INI_GENERAL, 'ZoneNum',      aGeoZone);

     WriteInteger ('MATRIX', 'RecordType', Integer(dtInteger));
     WriteInteger ('MATRIX', 'RecordLen',  4);

     UpdateFile;
     Free;
  end;


  oFileStream := TFileStream.Create(aFileName+'.bin',fmCreate);
  oFileStream.Size := (ARowCount*AColCount*4);
  oFileStream.Free;

end;



//------------------------------------------------------
procedure TCustomLenMatrix.SaveToFile (aFileName: string);
//------------------------------------------------------
begin
  if aFileName='' then
    Exit;

  SaveHeaderToFile (aFileName);

  if FOpenMode = mtInMemory then
    // ���������� MemoryStream
    FMemoryStream.SaveToFile (aFileName + '.bin') // create BINARY file
end;

//------------------------------------------------------
function TCustomLenMatrix.LoadHeaderFromFile (aFileName: string): boolean;
//------------------------------------------------------
var oIni: TMemIniFile;
begin
  if not FileExists(aFileName) then begin
    //raise Exception.Create('FileName not FileExists');
    Result:=False;
    Exit;
  end;

  try
    oIni:=TMemIniFile.Create (aFileName);
  except
  end;

    with oIni do begin
     TopLeft.X := ReadInteger (INI_GENERAL, 'StartX',   0);
     TopLeft.Y := ReadInteger (INI_GENERAL, 'StartY',   0);
     StartB    := ReadFloat   (INI_GENERAL, 'StartB',   0);
     StartL    := ReadFloat   (INI_GENERAL, 'StartL',   0);
     Step      := ReadInteger (INI_GENERAL, 'Step',     0);
     SectorID  := ReadInteger (INI_GENERAL, 'SectorID', 0);
     ZoneNum   := ReadInteger (INI_GENERAL, 'ZoneNum', 0);

//     NotNullCellCount := ReadInteger (INI_GENERAL, 'NotNullCellCount',  0);

     RowCount  := ReadInteger (INI_GENERAL, 'RowCount', 0);
     ColCount  := ReadInteger (INI_GENERAL, 'ColCount', 0);

     RecordLen:= ReadInteger('MATRIX', 'RecordLen',  0);

    end;

    if Assigned(FOnLoadFromFile) then
      FOnLoadFromFile (oini);

    oIni.Free;

    Result:=True;
    {
  except
    Result:=False;
  end;
     }
end;

//------------------------------------------------------
function TCustomLenMatrix.CheckBinFile (aFileName: string): boolean;
//------------------------------------------------------
var sBinFileName: string;
begin
  // ����������� ��������� �������� ���� (binary file size === mem stream size)
  // ���� �� ������� -> ������ ������ [VALUES]

  sBinFileName:=aFileName + '.bin';
  Result:= (FileExists(sBinFileName)) and
           (GetFileSize(sBinFileName)=
                RowCount * ColCount * FRecordLen);

end;

//------------------------------------------------------
function TCustomLenMatrix.LoadFromFile (aFileName: string; aOpenMode: word = fmOpenRead): boolean;
//------------------------------------------------------
var sBinFileName: string;
begin
   FileName:=aFileName;

   Result:=false;
   Clear;

   if (aFileName='') or (not FileExists(aFileName)) then begin
//////////////////////     ErrorDlg (Format(ERROR_FILE_NOT_EXISTS,[aFileName]));
     Exit;
   end;

 //  FOpenMode:=aOpenMode;

   case aOpenMode of
     fmOpenReadWrite: FOpenMode:=mtInMemory;
     fmOpenRead     : FOpenMode:=mtAsVirtFile;
   else
     raise Exception.Create('aOpenMode  -?');
   end;

   try
   Result:=LoadHeaderFromFile (aFileName);
   except end;

   if not Result then
     Exit;

   if not CheckBinFile (aFileName) then begin
     raise Exception.Create('CheckBinFile ('+aFileName+')');
     Exit;
   end;

   SetMatrixSize (RowCount, ColCount);

   sBinFileName:=aFileName + '.bin';

   case FOpenMode of
     mtInMemory:  begin
                    // load memory stream
                    FMemoryStream.LoadFromFile(sBinFileName);
                  end;

     mtAsVirtFile:begin
                    if not FVirtualFile.Open (sBinFileName, fmOpenReadWrite) then
                      Exit;

                    FVirtualFile.RecordLen1:=FRecordLen;
                 end;

   {  omVirtualOpenReadWrite:begin
                    if not FVirtualFile.Open (sBinFileName, fmOpenReadWrite) then
                      Exit;

                    FVirtualFile.RecordLen:=FRecordLen;
                 end;}

   end;

   Result:=true; // everything's OK
end;



function TCustomLenMatrix.GetCellGeoCenter(aRow, aCol: integer): TXYPoint;
begin
  Result.X:=TopLeft.X - aRow*Step - Step/2;
  Result.Y:=TopLeft.Y + aCol*Step + Step/2;
end;


//-------------------------------------------------------------------
procedure TCustomLenMatrix.SaveToTxtFile(aFileName: string);
//-------------------------------------------------------------------
var dir,str: string;
    r,c: integer;
    F: TextFile;
begin
  dir:=ExtractFileDir (aFileName);
  if not DirectoryExists (dir) then  ForceDirectories(dir);

  AssignFile(F, aFileName + '.txt');
  ReWrite(F);


  for r:=0 to RowCount-1 do
  begin
    str:='';
    for c:=0 to ColCount-1 do
     str:=Str + CellToStr (r,c);

    Writeln(F,str);
  end;

  CloseFile(F);

end;


function TCustomLenMatrix.CellCount(): integer;
begin
  Result:=RowCount * ColCount
end;

//-------------------------------------------------
procedure TCustomLenMatrix.FillBlankOutOfRadius (aX,aY: double; aRadius: double);
// �������� ������� ��� �����
//-------------------------------------------------
var r,c: integer;  x,y,dist: double;
//   iNullCells: integer;
begin
//  iNullCells:=0;
  for r:=0 to RowCount-1 do
  for c:=0 to ColCount-1 do begin
    x:=TopLeft.X - (r+1/2)*Step;
    y:=TopLeft.Y + (c+1/2)*Step;
    dist:=Sqrt(Sqr(x-aX)+Sqr(y-aY));
    if dist > aRadius then begin
      SetNullCell (r,c);
//      Items[r,c]:=FBlankValue;
 //     Inc(iNullCells);
    end;
  end;
end;



procedure TCustomLenMatrix.SetRecordLen(aValue: word);
begin
  if aValue>0 then
    FRecordLen:=aValue;
end;


end.
