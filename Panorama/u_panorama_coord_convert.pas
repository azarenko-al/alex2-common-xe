unit u_panorama_coord_convert;

interface

uses
  Winapi.Windows, SysUtils, math,
  maptype ,
  mapapi
  ;


type
  // ---------------------------------------------------------------
  TPanorama_Coord_convert = class(TObject)
  // ---------------------------------------------------------------
//  private      
//    MapRegisterListFile : ('C:\Program Files\Panorama\GisTool12Free\Dll\x32\��� ��������� ��.xml');

  public    
    class procedure Geo_EPSG_To_EPSG_plane(aEpsg_from, aEpsg_to: Integer; aLat, aLon: double; var aX_out, aY_out: double);
              

    class procedure PlaneEPSGToGeoWGS_plane(aEpsg_from, aEpsg_to: Integer; aX, aY: double; var aX_out, aY_out: double);
//    class procedure PlaneEPSGToGeoWGS84(aEpsg: Integer; aLat, aLon: double; var aLat,
 //       aLon: double);
  
    class procedure PlaneEPSGToGeoWGS_geo(aEpsg_from, aEpsg_to: Integer; aX, aY: double; var aLat_out, aLon_out: double);
        

//function mapGeoWGS84ToGeo42(Map:HMap; var Bx, Ly : double) : integer;

        
//    class procedure PlaneEPSGToGeoWGS84(aEpsg: Integer; aX, aY: double; var aLat,
 //       aLon: double);
   // class procedure GeoToGeo_EPSG(aEpsg_from, aEpsg_to: Integer; var aX, aY:
    //    double);


  end;


  procedure Plane_EPSG_To_EPSG_geo(aEpsg_from, aEpsg_to: Integer; aX, aY: double;
      var aLat_out, aLon_out: double); stdcall;


  procedure Plane_EPSG_To_EPSG_plane(aEpsg_from, aEpsg_to: Integer; aX, aY:
      double; var aX_out, aY_out: double); stdcall;

  

  procedure Geo_EPSG_To_EPSG_plane(aEpsg_from, aEpsg_to: Integer; aLat, aLon:
      double; var aX_out, aY_out: double); stdcall;

 


exports 
  Plane_EPSG_To_EPSG_plane,
  Plane_EPSG_To_EPSG_geo,
  Geo_EPSG_To_EPSG_plane;

  

const
  DEF_WEB_EPSG_3857 = 3857;
  DEF_WGS_MERCATOR_EPSG_3395 = 3395;  //      EPSG:3395<
  
  DEF_WGS_EPSG_4326 = 4326;  //      EPSG:3395<
  

// procedure WGS_to_GSK_2011(aLat, aLon: double; var aLat_out, aLon_out: double);

  

implementation


// ---------------------------------------------------------------
class procedure TPanorama_Coord_convert.Geo_EPSG_To_EPSG_plane(aEpsg_from, aEpsg_to: Integer; aLat, aLon: double; var aX_out, aY_out: double);
// ---------------------------------------------------------------

var
  k: Integer;

  myCS_1:  THANDLE;   
  myCS_2:  THANDLE;  

//  epsg1,epsg2: record
    mapreg1,mapreg2: TMAPREGISTEREX;
    datum1,datum2: TDATUMPARAM;
    ellipsoid1,ellipsoid2: TELLIPSOIDPARAM;  

//  end;
  
  h,x,y : Double;
  s1: string;
  s2: string;
  x_,y_ : Double;

begin
  k:=mapGetParametersForEPSG(aEpsg_from, @mapreg1, @datum1, @ellipsoid1);
  k:=mapGetParametersForEPSG(aEpsg_to,   @mapreg2, @datum2, @ellipsoid2);

 // s1:=TMAPREGISTEREX_to_Str(mapreg1);


                               
  myCS_1 := mapCreateUserSystemParameters(@mapreg1, @datum1, @ellipsoid1);
  myCS_2 := mapCreateUserSystemParameters(@mapreg2, @datum2, @ellipsoid2);// EPSG CK2);

//myCS_1 := mapCreateUserSystemParameters ByEpsg(aEpsg_from);
//  myCS_2 := mapCreateUserSystemParametersByEpsg(aEpsg_to);// EPSG CK2);


  x:=DegToRad(aLat);
  y:=DegToRad(aLon);  

  // ����� ��� ��������� � ������
  // (��� ������������� �������������� �� UserPlane, � UserGeo)
  mapUserGeoToGeoWGS84(myCS_1, &x, &y);

//  mapUser
  
  x_:=RadToDeg(x);
  y_:=RadToDeg(y);  


//  
//  //!!!!!!!!!!
//  mapGeoWGS84ToPlane3d(myCS_1, &x, &y, &h);
//
//
//  x_:=RadToDeg(x);
//  y_:=RadToDeg(y);  
  
  
  mapGeoWGS84ToUserPlane(myCS_2, &x, &y);


  aX_out:=Trunc(x);
  aY_out:=Trunc(y);  

  s1:= Format('%n',[aX_out]);
  s2:= Format('%n',[aY_out]);  

  // ���������� ������� ���������������� ������� ���������
  mapDeleteUserSystemParameters(myCS_1);
  mapDeleteUserSystemParameters(myCS_2);
               
end;


 // DEF_WGS_EPSG_41001 = 41001;  //      EPSG:3395<
  
  
  // 		<CRS>EPSG:3857</CRS>




//--------------------------------------------------------
class procedure TPanorama_Coord_convert.PlaneEPSGToGeoWGS_plane(aEpsg_from,
    aEpsg_to: Integer; aX, aY: double; var aX_out, aY_out: double);
//--------------------------------------------------------
var
  k: Integer;

  myCS_1:  THANDLE;   
  myCS_2:  THANDLE;  

//  epsg1,epsg2: record
    mapreg1,mapreg2: TMAPREGISTEREX;
    datum1,datum2: TDATUMPARAM;
    ellipsoid1,ellipsoid2: TELLIPSOIDPARAM;  
  s1: string;
//  end;             
  
  h,x,y : Double;
  x_,y_ : Double; 
   
begin
  k:=mapGetParametersForEPSG(aEpsg_from, @mapreg1, @datum1, @ellipsoid1);
  k:=mapGetParametersForEPSG(aEpsg_to,   @mapreg2, @datum2, @ellipsoid2);

 // s1:=TMAPREGISTEREX_to_Str(mapreg1);


                               
  myCS_1 := mapCreateUserSystemParameters(@mapreg1, @datum1, @ellipsoid1);
  myCS_2 := mapCreateUserSystemParameters(@mapreg2, @datum2, @ellipsoid2);// EPSG CK2);

//myCS_1 := mapCreateUserSystemParameters ByEpsg(aEpsg_from);
//  myCS_2 := mapCreateUserSystemParametersByEpsg(aEpsg_to);// EPSG CK2);

  x:=aX;
  y:=aY;

  // ����� ��� ��������� � ������
  // (��� ������������� �������������� �� UserPlane, � UserGeo)
  mapUserPlaneToGeoWGS84(myCS_1, x, y);

//  mapUser

  x_:=RadToDeg(x);
  y_:=RadToDeg(y);  


//  
//  //!!!!!!!!!!
//  mapGeoWGS84ToPlane3d(myCS_1, &x, &y, &h);
//
//
//  x_:=RadToDeg(x);
//  y_:=RadToDeg(y);  
  
  
  mapGeoWGS84ToUserPlane(myCS_2, &x, &y);


//  aX_out:=Trunc(x);
//  aY_out:=Trunc(y);  

  aX_out:=x;
  aY_out:=y;  

  
//  aX_out:=x;
//  aY_out:=y;  

  // ���������� ������� ���������������� ������� ���������
  mapDeleteUserSystemParameters(myCS_1);
  mapDeleteUserSystemParameters(myCS_2);
               
end;


//--------------------------------------------------------
class  procedure TPanorama_Coord_convert.PlaneEPSGToGeoWGS_geo(aEpsg_from, aEpsg_to: Integer; 
           aX, aY: double; var aLat_out, aLon_out: double);
//--------------------------------------------------------
var
  k: Integer;

  myCS_1:  THANDLE;   
  myCS_2:  THANDLE;  

//  epsg1,epsg2: record
    mapreg1,mapreg2: TMAPREGISTEREX;
    datum1,datum2: TDATUMPARAM;
    ellipsoid1,ellipsoid2: TELLIPSOIDPARAM;  
  s1: string;
//  end;             
  
  h,x,y : Double;
  x_,y_ : Double; 
   
begin
  k:=mapGetParametersForEPSG(aEpsg_from, @mapreg1, @datum1, @ellipsoid1);
  k:=mapGetParametersForEPSG(aEpsg_to,   @mapreg2, @datum2, @ellipsoid2);

 // s1:=TMAPREGISTEREX_to_Str(mapreg1);


                               
  myCS_1 := mapCreateUserSystemParameters(@mapreg1, @datum1, @ellipsoid1);
  myCS_2 := mapCreateUserSystemParameters(@mapreg2, @datum2, @ellipsoid2);// EPSG CK2);

//myCS_1 := mapCreateUserSystemParameters ByEpsg(aEpsg_from);
//  myCS_2 := mapCreateUserSystemParametersByEpsg(aEpsg_to);// EPSG CK2);

  x:=aX;
  y:=aY;  

  // ����� ��� ��������� � ������
  // (��� ������������� �������������� �� UserPlane, � UserGeo)
  mapUserPlaneToGeoWGS84(myCS_1, x, y);

//  mapUser
  
//  x_:=RadToDeg(x);
//  y_:=RadToDeg(y);  


//  
//  //!!!!!!!!!!
//  mapGeoWGS84ToPlane3d(myCS_1, &x, &y, &h);
//
//
  //test
  x_:=RadToDeg(x);  
  y_:=RadToDeg(y);  
  
  
  mapGeoWGS84ToUserGeo(myCS_2, &x, &y);


  aLat_out:=RadToDeg(x);
  aLon_out:=RadToDeg(y);  

//  aX_out:=x;
//  aY_out:=y;  

  // ���������� ������� ���������������� ������� ���������
  mapDeleteUserSystemParameters(myCS_1);
  mapDeleteUserSystemParameters(myCS_2);
               
end;


// ---------------------------------------------------------------
procedure Test;
// ---------------------------------------------------------------
var
  X1, Y1: double;
  X2, Y2: double;  
  Lat, Lon: double;
begin

// 		<CRS>EPSG:3857</CRS>

  y1:=10; //3 370 000
  x1:=10; //8 410 000  -20 037 508.34279000;
     

  y1:=56; //3 370 000
  x1:=43; //8 410 000  -20037508.34279000;

     //="EPSG:3857
     //EPSG:3395

     //EPSG 3857 or 4326 for GoogleMaps, OpenStreetMap and Leaflet

  TPanorama_Coord_convert.Geo_EPSG_To_EPSG_plane(
       DEF_WGS_EPSG_4326,
//       DEF_WGS_MERCATOR_EPSG_3395,
       DEF_WEB_EPSG_3857,
         -180,-85, x2,y2);


   TPanorama_Coord_convert.Geo_EPSG_To_EPSG_plane(
       DEF_WGS_EPSG_4326,
//       DEF_WGS_MERCATOR_EPSG_3395,
       DEF_WEB_EPSG_3857,
         x1,y1, x2,y2);

     
//  TPanorama_Coord_convert.PlaneEPSGToGeoWGS_plane(3857, 3395, x1,y1, x2,y2);
 // TPanorama_Coord_convert.PlaneEPSGToGeoWGS_plane(DEF_WEB_EPSG_3857, DEF_WGS_EPSG_41001, x1,y1, x2,y2);

  

//  obj:=TPanorama_Coord_convert.Create;;
 // obj.Test;
  
end;

// ---------------------------------------------------------------
procedure Test1;
// ---------------------------------------------------------------
var
  eLat_rad: Double;
  eLat: Double;
  eLat_: Double;
  eLon: Double;
  eLon_: Double;
  eLon_rad: Double;
  k: Integer;
//  X, Y: double;
//  X2, Y2: double;  
 // Lat, Lon: double;
begin


//function mapGeo42ToGeoWGS84(Map:HMap; var Bx, Ly : double) : integer;
//  {$IFNDEF LINUXAPI} stdcall {$ELSE} cdecl {$ENDIF};


// 		<CRS>EPSG:3857</CRS>

  eLat:=37; //3 370 000
  eLon:=50; //8 410 000  -20 037 508.34279000;

//  bl_wgs.B:=55.983285;
//  bl_wgs.L:=37.793224;
  

  eLat:=55.983285; //3 370 000
  eLon:=37.793224; //8 410 000  -20 037 508.34279000;

     
  eLat_rad:=DegToRad(eLat);  
  eLon_rad:=DegToRad(eLon);  


  k:=mapGeoWGS84ToGeo42 (0, eLat_rad, eLon_rad);
  
  eLat_:=RadToDeg(eLat_rad);  
  eLon_:=RadToDeg(eLon_rad);  

     
//  y1:=56; //3 370 000
//  x1:=43; //8 410 000  -20037508.34279000;


//  WGS_to_GSK_2011 (eLat,eLon, x,y);
  
     //="EPSG:3857
     //EPSG:3395

     //EPSG 3857 or 4326 for GoogleMaps, OpenStreetMap and Leaflet


end;         


procedure Geo_EPSG_To_EPSG_plane(aEpsg_from, aEpsg_to: Integer; aLat, aLon: double; var aX_out, aY_out: double);
begin
  TPanorama_Coord_convert.Geo_EPSG_To_EPSG_plane(   aEpsg_from, aEpsg_to,  aLat, aLon,  aX_out, aY_out);

end;

procedure Plane_EPSG_To_EPSG_plane(aEpsg_from, aEpsg_to: Integer; aX, aY:  double; var aX_out, aY_out: double);
begin
  TPanorama_Coord_convert.PlaneEPSGToGeoWGS_plane(  aEpsg_from, aEpsg_to,  aX, aY, aX_out, aY_out);
end;


procedure Plane_EPSG_To_EPSG_geo(aEpsg_from, aEpsg_to: Integer; aX, aY: double;  var aLat_out, aLon_out: double);
begin
  TPanorama_Coord_convert.PlaneEPSGToGeoWGS_geo(aEpsg_from, aEpsg_to,  aX, aY, aLat_out, aLon_out);
                                                           
end;


begin



//  Test;
  Test1;  
  

end.

