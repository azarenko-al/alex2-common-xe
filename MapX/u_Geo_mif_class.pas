unit u_Geo_mif_class;

interface
uses SysUtils, Classes, Graphics, //RxGif,  //jpeg,

   PNGImage,

   XMLDoc, XMLIntf,
   u_xml_document ,

   u_Geo,
  u_geo_convert_new
 // u_Geo
   ;

type

  TMapinfoRasterExport = class(TObject)
  private
    procedure UpdateRaster(aFileName: string);

// TODO: Convert_BmpToGIF11
//  procedure Convert_BmpToGIF11(aBmpFileName: String);
    procedure Convert_BmpToPNG(aBmpFileName: String);
// TODO: Convert_BmpToJPG
//  procedure Convert_BmpToJPG(aBmpFileName: String);

  public
    Raster_FileName: string;

    GK_Zone: integer;

    // ���� ��������������
    Points : array[0..3] of record
                              img_x,img_y: Integer;
                              x,y: Double;
                            end;

  public                                          // 'Temp_bind_layer';
    procedure SaveToTabFile(aFileName: string);
    procedure SaveToKmlFile(aFileName: string);
    procedure SaveToOziFile(aFileName: string);
  end;


//procedure Convert_BmpToGIF(aBmpFileName: String);


//===================================================================
implementation
//===================================================================
const
  CRLF = #13+#10;

function ReplaceStr (Value, OldPattern, NewPattern: string): string;
begin
  Result:=StringReplace (Value, OldPattern, NewPattern, [rfReplaceAll, rfIgnoreCase]);
end;


// TODO: Convert_BmpToGIF11
////------------------------------------------------------
//procedure TMapinfoRasterExport.Convert_BmpToGIF11(aBmpFileName: String);
////------------------------------------------------------
//var
//oBitmap: TBitmap;
//obj : TGIFImage;
//begin
//Assert(FileExists(aBmpFileName));
//
//oBitmap:=TBitmap.Create;
//oBitmap.LoadFromFile(aBmpFileName);
//
//obj:=TGIFImage.Create;
//obj.Assign (oBitmap);
//
//obj.Transparent:=true;
////  obj.BackgroundColor:=clBlack;
//
//obj.SaveToFile (ChangeFileExt(aBmpFileName,'.gif'));
//
//FreeAndNil(obj);
//FreeAndNil(oBitmap);
//end;




//------------------------------------------------------
procedure TMapinfoRasterExport.Convert_BmpToPNG(aBmpFileName: String);
//------------------------------------------------------
var
  oBitmap: TBitmap;
  obj : TPngObject;
begin
  Assert(FileExists(aBmpFileName));

  oBitmap:=TBitmap.Create;
  oBitmap.LoadFromFile(aBmpFileName);

  obj:=TPngObject.Create;
  obj.Assign (oBitmap);

  obj.Transparent:=true;
//  obj.BackgroundColor:=clBlack;

  obj.SaveToFile (ChangeFileExt(aBmpFileName,'.png'));

  FreeAndNil(obj);
  FreeAndNil(oBitmap);
end;



// TODO: Convert_BmpToJPG
////------------------------------------------------------
//procedure TMapinfoRasterExport.Convert_BmpToJPG(aBmpFileName: String);
////------------------------------------------------------
//var
//oBitmap: TBitmap;
//obj : TJPEGImage;
//begin
//Assert(FileExists(aBmpFileName));
//
//oBitmap:=TBitmap.Create;
//oBitmap.LoadFromFile(aBmpFileName);
//
//obj:=TJPEGImage.Create;
//obj.Assign (oBitmap);
//
//obj.Transparent:=true;
////  obj.BackgroundColor:=clBlack;
//
//obj.SaveToFile (ChangeFileExt(aBmpFileName,'.JPG'));
//
//FreeAndNil(obj);
//FreeAndNil(oBitmap);
//end;


// ---------------------------------------------------------------
procedure TMapinfoRasterExport.UpdateRaster(aFileName: string);
// ---------------------------------------------------------------
var
  s,sDir: string;
  sPng: string;

begin
  Assert(Raster_FileName<>'', 'Value <=0');

  sDir:= IncludeTrailingBackslash(extractFileDir(aFileName));

  // -------------------------
  // export raster -> gif
  // -------------------------
  s := LowerCase(ExtractFileExt(Raster_FileName));

  if s='.bmp' then
  begin
    sPng :=sDir + ChangeFileExt (Raster_FileName, '.png');
    if not FileExists(sPng) then
      Convert_BmpToPng(sDir + Raster_FileName);

    Raster_FileName := ChangeFileExt (Raster_FileName, '.png')
  end;

end;

//------------------------------------------------------
procedure TMapinfoRasterExport.SaveToTabFile(aFileName: string);
//------------------------------------------------------
// ������������ ��������� ��� ����� Mapinfo
// - Bounds - ���������� ������� ������� ��������
//------------------------------------------------------
const
  DEF_WOR=
    '!Workspace'    + CRLF +
    '!Version 650'  + CRLF +
    '!Charset WindowsCyrillic'+ CRLF +
    'Open Table ":FILENAME" As _1 Interactive'+ CRLF +
    'Map From _1'   + CRLF +
    'Set Map'       + CRLF +
    '  :CoordSys'+ CRLF + // Earth Projection 8, 1001, "m", 51, 0, 1, 9500000, 0
    'Set Map'       + CRLF +
    '  Layer 1'     + CRLF +
    '    Display Graphic'+ CRLF +
    '    Zoom (0.11, 42) Units "km" Off';

const
  DEF_CoordSys =
   'CoordSys Earth Projection 8, 1001, "m", :Zone ';

const
  MIF_RASTER =
    '!table'            + CRLF +
    '!version 300'      + CRLF +
    '!charset WindowsCyrillic' + CRLF +
    ''                         + CRLF +
    'Definition Table   '+ CRLF +
    '  File ":FileName" '+ CRLF +
    '  Type "RASTER"    '+ CRLF +
    ':Points            '+ CRLF +
    ':CoordSys'+ CRLF + // Earth Projection 8, 1001, "m", :Zone ' + CRLF + //39, 0, 1, 7500000, 0 ' + CRLF +
    'Units "m" ';


    //+ CRLF +
//    'RasterStyle 4 1 '+ CRLF + //transparent setup
 //   'RasterStyle 7 0 '; //16777215'; //bmp: 16776959'; //// JPG

//    'RasterStyle 7 16777215 ';


  function DoMakeCoordSys: string;
  var
    d: integer;
    d1: integer;
    sZone: string;
  begin
    // -------------------------
    d:=GK_Zone*6-3;
    d1:=GK_Zone*1000000 + 500000;
    sZone:= Format('%d, 0, 1, %d, 0', [d, d1]);
    // -------------------------

    Result := ReplaceStr (DEF_CoordSys, ':Zone',   sZone);
  end;


var
  sZone,sXYPoints: string;
  s,sText : string;
  i,iZone: integer;
  d: integer;
  d1: integer;
  oSList: TStringList;

 // sDir: string;
 // sGif: string;

begin
  UpdateRaster(afileName);


  sXYPoints := '';

  for i:=0 to High(Points) do
  begin
    s:=Format('  (%1.10f,%1.10f) (%d,%d) Label "Pt %d"',
      [Points[i].Y, Points[i].X, Points[i].img_x, Points[i].img_y, i+1]);

    if i<>High(Points) then
      s:=s+',';

    sXYPoints:=sXYPoints + s +CRLF;
  end;



//  if aRec.Transparent then
//    sContent:=sContent + CRLF + 'RasterStyle 8 204 ';

(*  // -------------------------
  d:=GK_Zone*6-3;
  d1:=GK_Zone*1000000 + 500000;
  sZone:= Format('%d, 0, 1, %d, 0', [d, d1]);
  // -------------------------
*)

 // sText:=ReplaceStr (sText, ':Zone',   sZone);

  // -------------------------
  oSList:=TStringList.Create;


  sText:=MIF_RASTER;
  sText:=ReplaceStr (sText, ':FileName', ExtractFileName(Raster_FileName));
  sText:=ReplaceStr (sText, ':Points',   sXYPoints);
  sText:=ReplaceStr (sText, ':CoordSys', DoMakeCoordSys());


  oSList.Text := sText;
  oSList.SaveToFile(ChangeFileExt (aFileName, '.tab'));


  sText:=DEF_WOR;
  sText:=ReplaceStr (sText, ':FileName', ExtractFileName(aFileName));
  sText:=ReplaceStr (sText, ':CoordSys', DoMakeCoordSys());

  oSList.Text := sText;
  oSList.SaveToFile(ChangeFileExt (aFileName, '.wor'));



  FreeAndNil(oSList);

end;


// ---------------------------------------------------------------
procedure TMapinfoRasterExport.SaveToKmlFile(aFileName: string);
// ---------------------------------------------------------------

//const
(*<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2"
 xmlns:gx="http://www.google.com/kml/ext/2.2">
<GroundOverlay>
  <name>gx:LatLonQuad Example</name>
  <Icon>
    <href>http://developers.google.com/kml/documentation/images/rectangle.gif</href>
    <viewBoundScale>0.75</viewBoundScale>
  </Icon>
  <gx:LatLonQuad>
    <coordinates>
      81.601884,44.160723 83.529902,43.665148 82.947737,44.248831 81.509322,44.321015
    </coordinates>
  </gx:LatLonQuad>
</GroundOverlay>
</kml>*)


var
  sZone,sCoordinates: string;
  s,sText : string;
  i,iZone: integer;
  d: integer;
  d1: integer;
  oSList: TStringList;
  sDir: string;
  sGif: string;

  oDoc: TXmlDocumentEx;
  vNode,vNode1 : IXMLNode;

  XY: TXYPoint;
  BL: TBLPoint;
  k: Integer;

begin
  UpdateRaster(aFileName);


  sCoordinates := '';

  for i:=0 to 3 do //High(Points)
  begin
    k:=(3-i) mod 4;

    XY.X := Points[k].x;
    XY.Y := Points[k].y;


    bl:= geo_GK_XY_to_WGS84_BL(XY, GK_Zone);

    s:=Format('%1.6f,%1.6f ', [bl.l, bl.b]);

    sCoordinates:=sCoordinates + s
  end;



  // -------------------------
  oDoc:=TXmlDocumentEx.Create;
  oDoc.InitKML;


  vNode:=oDoc.DocumentElement.AddChild('GroundOverlay');
  with vNode.AddChild('Icon') do
  begin
    ChildValues['href'] := Raster_FileName;
    ChildValues['viewBoundScale'] := 0.75;
  end;

  vNode.AddChild('gx:LatLonQuad').AddChild('coordinates','').Text := sCoordinates;


  oDoc.SaveToFile(ChangeFileExt (aFileName, '.kml'));

  FreeAndNil(oDoc);


end;

// ---------------------------------------------------------------
procedure TMapinfoRasterExport.SaveToOziFile(aFileName: string);
// ---------------------------------------------------------------
const
  OZI_HEADER =
    'OziExplorer Map Data File Version 2.2'+ CRLF +
    '[:raster]'+ CRLF +
    '[:raster]'+ CRLF +
    '1 ,Map Code,'+ CRLF +
    'Pulkovo 1942 (2),WGS 84,   0.0000,   0.0000,WGS 84'+ CRLF +
    'Reserved 1'+ CRLF +
    'Reserved 2'+ CRLF +
    'Magnetic Variation,,,E'+ CRLF +
    'Map Projection,Transverse Mercator,PolyCal,No,AutoCalOnly,No,BSBUseWPX,No'+ CRLF +
    '[:points]'+ //CRLF +
    'Point05,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point06,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point07,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point08,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point09,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point10,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point11,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point12,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point13,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point14,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point15,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point16,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point17,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point18,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point19,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point20,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point21,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point22,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point23,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point24,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point25,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point26,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point27,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point28,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point29,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    'Point30,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N'+ CRLF +
    //'Projection Setup,     0.000000000,    39.000000000,     1.000000000,       500000.00,            0.00,,,,,'+ CRLF +
    'Projection Setup,     0.000000000,[:meridian],     1.000000000,       500000.00,            0.00,,,,,'+ CRLF +
    'Map Feature = MF ; Map Comment = MC     These follow if they exist'+ CRLF +
    'Track File = TF      These follow if they exist'+ CRLF +
    'Moving Map Parameters = MM?    These follow if they exist'+ CRLF +
    'MM0,Yes'+ CRLF +
    'MMPNUM,4'+ CRLF +
    '[:MMPXY]'+ //CRLF +
    '[:MMPLL]'+ //CRLF +
    'MM1B,60.972260'+ CRLF +
    'MOP,Map Open Position,0,0'+ CRLF +
    'IWH,Map Image Width/Height,[:Image]'; //3234,3641';


  OZI_Points =
    'Point0%d,xy,%5d,%5d,in, deg,%4d,%8.4f,N,%4d,%8.4f,E, grid,   ,           ,           ,N';

(*  OZI_LINE2 =
    'Point0%d,xy,%5d,%5d,in, deg,%4d,%8.4f,N,%4d,%8.4f,E, grid,   ,           ,           ,N';
*)
  OZI_MMPXY =
    'MMPXY,%d,%d,%d';

  OZI_MMPLL =
    'MMPLL,%d,%11.6f,%11.6f';

  OZI_Image=
    '%d,%d';




//Point01,xy,  141,   86,in, deg,  56,  0.0000,N,  36,  0.0000,E, grid,   ,           ,           ,N


//Point10,xy,     ,     ,in, deg,    ,        ,N,    ,        ,W, grid,   ,           ,           ,N


var
  sZone,sCoordinates: string;
  s,sText : string;
  i,iZone: integer;
 // d: integer;
  //d1: integer;
  oSList: TStringList;
  sDir: string;
  sGif: string;

  sMeridian : string;


  XY: TXYPoint;
  BL: TBLPoint;
  d: Double;
  eMin_x: Double;
  eMin_y: Double;
  iDegree_x: Integer;
  iDegree_y: Integer;
  k: Integer;
  sImage: string;

  sPoints : string;
  sMMPXY : string;
  sMMPLL : string;

begin
  UpdateRaster(aFileName);



  d:=GK_Zone*6-3;
  sMeridian := Format('%16.9f', [d]);

  sPoints:='';
  sMMPXY := '';
  sMMPLL := '';

  for i:=0 to High(Points) do
  begin
    XY.X := Points[i].x;
    XY.Y := Points[i].y;


    bl:= geo_GK_XY_to_Pulkovo42_BL(XY, GK_Zone);

    geo_DecodeDegree_into_Degree_and_Minute(bl.B, iDegree_x,eMin_x);
    geo_DecodeDegree_into_Degree_and_Minute(bl.L, iDegree_y,eMin_y);

    s:=Format(OZI_Points, [i+1,
                          Points[i].img_x, Points[i].img_y,
                          iDegree_x, eMin_x,
                          iDegree_y, eMin_y]);
    sPoints:=sPoints + s + CRLF;

    s:=Format(OZI_MMPXY, [i+1, Points[i].img_x, Points[i].img_y]);
    sMMPXY  := sMMPXY + s + CRLF;

    s:=Format(OZI_MMPLL, [i+1, bl.L, bl.B]);
    sMMPLL  := sMMPLL + s + CRLF;

  end;


  sImage:=Format(OZI_Image, [Points[2].img_x+1, Points[2].img_y+1]);

  sText:=OZI_HEADER;
  sText:=ReplaceStr (sText, '[:raster]', ExtractFileName(Raster_FileName));
  sText:=ReplaceStr (sText, '[:points]', sPoints);
  sText:=ReplaceStr (sText, '[:MMPXY]', sMMPXY);
  sText:=ReplaceStr (sText, '[:MMPLL]', sMMPLL);
  sText:=ReplaceStr (sText, '[:Image]', sImage);
  sText:=ReplaceStr (sText, '[:Meridian]', sMeridian);


  // -------------------------
  oSList:=TStringList.Create;

  oSList.Text := sText;   
  oSList.SaveToFile(ChangeFileExt (aFileName, '.map'));

  FreeAndNil(oSList);



end;



end.
