unit u_MIF_raster;

interface
uses SysUtils, Classes, Graphics, //RxGif,  //jpeg,

 //  PNGImage,

   XMLDoc, XMLIntf,
   
//   u_xml_document ,

   u_Geo
  //u_geo_convert_new
 // u_Geo
   ;

type

  TMapinfoRasterExport = record
  private
//    procedure UpdateRaster(aFileName: string);

// TODO: Convert_BmpToGIF11
//  procedure Convert_BmpToGIF11(aBmpFileName: String);
//    procedure Convert_BmpToPNG(aBmpFileName: String);
// TODO: Convert_BmpToJPG
//  procedure Convert_BmpToJPG(aBmpFileName: String);



  public
    Raster_FileName: string;

  //  GK_Zone: integer;


    // ���� ��������������
    Points : array[0..3] of record
                              img_x,img_y: Integer;
                              x,y: Double;
                            end;
  public    
    procedure Init(aBLRect: TBLRect; aWidth, aHeight: Integer); overload;

    procedure Init1(aBounds: TLatLonBounds; aWidth, aHeight: Integer); overload;

    procedure SaveToTabFile_WGS_degree(aFileName: string);

//    procedure SaveToKmlFile(aFileName: string);
//    procedure SaveToOziFile(aFileName: string);
  end;


  {
   TLatLonBounds = record
 // TXYBounds_MinMax = record
                Lat_Min  : Double;
                Lat_Max  : Double;
                Lon_Min  : Double;
                Lon_Max  : Double;
              end;
                      

  }
  

//procedure Convert_BmpToGIF(aBmpFileName: String);


//===================================================================
implementation
//===================================================================
const
  CRLF = #13+#10;

function ReplaceStr (Value, OldPattern, NewPattern: string): string;
begin
  Result:=StringReplace (Value, OldPattern, NewPattern, [rfReplaceAll, rfIgnoreCase]);
end;

// ---------------------------------------------------------------
procedure TMapinfoRasterExport.Init(aBLRect: TBLRect; aWidth, aHeight: Integer);
// ---------------------------------------------------------------
begin
  Assert(aBLRect.BottomRight.B <> aBLRect.TopLeft.B);
  Assert(aBLRect.BottomRight.L <> aBLRect.TopLeft.L);  




  // bottom right corner
  Points[0].img_x:=aWidth-1;
  Points[0].img_y:=aHeight-1;
  
  Points[0].X:=aBLRect.BottomRight.B;//  Rec.XY.TopLeft.X - (aRec.RowCount)*aRec.XY.StepX;
  Points[0].Y:=aBLRect.BottomRight.L;// aRec.XY.TopLeft.Y + (aRec.ColCount)*aRec.XY.StepY;


  // bottom left corner
  Points[1].img_x:=0;
  Points[1].img_y:=aWidth-1;
  Points[1].X:=aBLRect.BottomRight.B;// aRec.XY.TopLeft.X - (aRec.RowCount)*aRec.XY.StepX;
//  xyPoints[1].X:=aRec.XY.TopLeft.X - (aRec.RowCount-1)*aRec.XY.StepX;
  Points[1].Y:=aBLRect.TopLeft.L;// aRec.XY.TopLeft.Y;

  // top left point
  Points[2].img_x:=0;
  Points[2].img_y:=0;
  Points[2].X:=aBLRect.TopLeft.B;//aRec.XY.TopLeft.X;
  Points[2].Y:=aBLRect.TopLeft.L;//aRec.XY.TopLeft.Y;

  // top right corner
  Points[3].img_x:=aWidth-1;
  Points[3].img_y:=0;
  Points[3].X:=aBLRect.TopLeft.B;//aRec.XY.TopLeft.X;
  Points[3].Y:=aBLRect.BottomRight.L;// aRec.XY.TopLeft.Y + (aRec.ColCount)*aRec.XY.StepY;
//  xyPoints[3].Y:=aRec.XY.TopLeft.Y + (aRec.ColCount-1)*aRec.XY.StepY;




end;

// ---------------------------------------------------------------
procedure TMapinfoRasterExport.Init1(aBounds: TLatLonBounds; aWidth, aHeight:
    Integer);
// ---------------------------------------------------------------    
begin

  // bottom right corner
  Points[0].img_x:=aWidth-1;
  Points[0].img_y:=aHeight-1;
  
  Points[0].X:=aBounds.Lat_Min;//  aBLRect.BottomRight.B;//  Rec.XY.TopLeft.X - (aRec.RowCount)*aRec.XY.StepX;
  Points[0].Y:=aBounds.Lon_Max;// aBLRect.BottomRight.L;// aRec.XY.TopLeft.Y + (aRec.ColCount)*aRec.XY.StepY;


  // bottom left corner
  Points[1].x:=0;
  Points[1].y:=aWidth-1;
  Points[1].X:=aBounds.Lat_Min;//  aBLRect.BottomRight.B;// aRec.XY.TopLeft.X - (aRec.RowCount)*aRec.XY.StepX;
//  xyPoints[1].X:=aRec.XY.TopLeft.X - (aRec.RowCount-1)*aRec.XY.StepX;
  Points[1].Y:=aBounds.Lon_Min;// aBLRect.TopLeft.B;// aRec.XY.TopLeft.Y;

  // top left point
  Points[2].x:=0;
  Points[2].y:=0;
  Points[2].X:=aBounds.Lat_Max;//aBLRect.TopLeft.B;//aRec.XY.TopLeft.X;
  Points[2].Y:=aBounds.Lon_Min;//aBLRect.TopLeft.L;//aRec.XY.TopLeft.Y;

  // top right corner
  Points[3].x:=aWidth-1;
  Points[3].y:=0;
  Points[3].X:=aBounds.Lat_Max;//aBLRect.TopLeft.B;//aRec.XY.TopLeft.X;
  Points[3].Y:=aBounds.Lon_Max;//aBLRect.BottomRight.L;// aRec.XY.TopLeft.Y + (aRec.ColCount)*aRec.XY.StepY;
//  xyPoints[3].Y:=aRec.XY.TopLeft.Y + (aRec.ColCount-1)*aRec.XY.StepY;



end;



// TODO: Convert_BmpToGIF11
////------------------------------------------------------
//procedure TMapinfoRasterExport.Convert_BmpToGIF11(aBmpFileName: String);
////------------------------------------------------------
//var
//oBitmap: TBitmap;
//obj : TGIFImage;
//begin
//Assert(FileExists(aBmpFileName));
//
//oBitmap:=TBitmap.Create;
//oBitmap.LoadFromFile(aBmpFileName);
//
//obj:=TGIFImage.Create;
//obj.Assign (oBitmap);
//
//obj.Transparent:=true;
////  obj.BackgroundColor:=clBlack;
//
//obj.SaveToFile (ChangeFileExt(aBmpFileName,'.gif'));
//
//FreeAndNil(obj);
//FreeAndNil(oBitmap);
//end;


//
//
////------------------------------------------------------
//procedure TMapinfoRasterExport.Convert_BmpToPNG(aBmpFileName: String);
////------------------------------------------------------
//var
//  oBitmap: TBitmap;
//  obj : TPngObject;
//begin
//  Assert(FileExists(aBmpFileName));
//
//  oBitmap:=TBitmap.Create;
//  oBitmap.LoadFromFile(aBmpFileName);
//
//  obj:=TPngObject.Create;
//  obj.Assign (oBitmap);
//
//  obj.Transparent:=true;
////  obj.BackgroundColor:=clBlack;
//
//  obj.SaveToFile (ChangeFileExt(aBmpFileName,'.png'));
//
//  FreeAndNil(obj);
//  FreeAndNil(oBitmap);
//end;



// TODO: Convert_BmpToJPG
////------------------------------------------------------
//procedure TMapinfoRasterExport.Convert_BmpToJPG(aBmpFileName: String);
////------------------------------------------------------
//var
//oBitmap: TBitmap;
//obj : TJPEGImage;
//begin
//Assert(FileExists(aBmpFileName));
//
//oBitmap:=TBitmap.Create;
//oBitmap.LoadFromFile(aBmpFileName);
//
//obj:=TJPEGImage.Create;
//obj.Assign (oBitmap);
//
//obj.Transparent:=true;
////  obj.BackgroundColor:=clBlack;
//
//obj.SaveToFile (ChangeFileExt(aBmpFileName,'.JPG'));
//
//FreeAndNil(obj);
//FreeAndNil(oBitmap);
//end;

//
//// ---------------------------------------------------------------
//procedure TMapinfoRasterExport.UpdateRaster(aFileName: string);
//// ---------------------------------------------------------------
//var
//  s,sDir: string;
//  sPng: string;
//
//begin
//  Assert(Raster_FileName<>'', 'Value <=0');
//
//  sDir:= IncludeTrailingBackslash(extractFileDir(aFileName));
//
//  // -------------------------
//  // export raster -> gif
//  // -------------------------
//  s := LowerCase(ExtractFileExt(Raster_FileName));
//
//  if s='.bmp' then
//  begin
//    sPng :=sDir + ChangeFileExt (Raster_FileName, '.png');
//    if not FileExists(sPng) then
//      Convert_BmpToPng(sDir + Raster_FileName);
//
//    Raster_FileName := ChangeFileExt (Raster_FileName, '.png')
//  end;
//
//end;

//------------------------------------------------------
procedure TMapinfoRasterExport.SaveToTabFile_WGS_degree(aFileName: string);
//------------------------------------------------------
// ������������ ��������� ��� ����� Mapinfo
// - Bounds - ���������� ������� ������� ��������
//------------------------------------------------------
//const
//  DEF_WOR=
//    '!Workspace'    + CRLF +
//    '!Version 650'  + CRLF +
//    '!Charset WindowsCyrillic'+ CRLF +
//    'Open Table ":FILENAME" As _1 Interactive'+ CRLF +
//    'Map From _1'   + CRLF +
//    'Set Map'       + CRLF +
//    '  :CoordSys'+ CRLF + // Earth Projection 8, 1001, "m", 51, 0, 1, 9500000, 0
//    'Set Map'       + CRLF +
//    '  Layer 1'     + CRLF +
//    '    Display Graphic'+ CRLF +
//    '    Zoom (0.11, 42) Units "km" Off';

const
  DEF_CoordSys_WGS11 =
   'CoordSys Earth Projection 8, 1001, "m", :Zone ';

  DEF_CoordSys_WGS =
    'CoordSys Earth Projection 1, 104';
   
const
  MIF_RASTER =
    '!table'            + CRLF +
    '!version 300'      + CRLF +
    '!charset WindowsCyrillic' + CRLF +
    ''                         + CRLF +
    'Definition Table   '+ CRLF +
    '  File ":FileName" '+ CRLF +
    '  Type "RASTER"    '+ CRLF +
    ':Points            '+ CRLF +
    ':CoordSys'  + CRLF + 
    // Earth Projection 8, 1001, "m", :Zone ' + CRLF + //39, 0, 1, 7500000, 0 ' + CRLF +
//    'Units "m" ';
    'Units "degree"' ;

    


    //+ CRLF +
//    'RasterStyle 4 1 '+ CRLF + //transparent setup
 //   'RasterStyle 7 0 '; //16777215'; //bmp: 16776959'; //// JPG

//    'RasterStyle 7 16777215 ';

//
//  function DoMakeCoordSys: string;
//  var
//    d: integer;
//    d1: integer;
//    sZone: string;
//  begin
//    // -------------------------
//    d:=GK_Zone*6-3;
//    d1:=GK_Zone*1000000 + 500000;
//    sZone:= Format('%d, 0, 1, %d, 0', [d, d1]);
//    // -------------------------
//
//    Result := ReplaceStr (DEF_CoordSys, ':Zone',   sZone);
//  end;


var
  sZone,sXYPoints: string;
  s,sText : string;
  i,iZone: integer;
  d: integer;
  d1: integer;
  oSList: TStringList;

 // sDir: string;
 // sGif: string;

begin
//  UpdateRaster(afileName);


  sXYPoints := '';

  for i:=0 to High(Points) do
  begin
    s:=Format('  (%1.10f,%1.10f) (%d,%d) Label "Pt %d"',
      [Points[i].Y, Points[i].X, Points[i].img_x, Points[i].img_y, i+1]);

    if i<>High(Points) then
      s:=s+',';

    sXYPoints:=sXYPoints + s +CRLF;
  end;



//  if aRec.Transparent then
//    sContent:=sContent + CRLF + 'RasterStyle 8 204 ';

(*  // -------------------------
  d:=GK_Zone*6-3;
  d1:=GK_Zone*1000000 + 500000;
  sZone:= Format('%d, 0, 1, %d, 0', [d, d1]);
  // -------------------------
*)

 // sText:=ReplaceStr (sText, ':Zone',   sZone);

  // -------------------------
  oSList:=TStringList.Create;


  sText:=MIF_RASTER;
  sText:=ReplaceStr (sText, ':FileName', ExtractFileName(Raster_FileName));
  sText:=ReplaceStr (sText, ':Points',   sXYPoints);

  sText:=ReplaceStr (sText, ':CoordSys', DEF_CoordSys_WGS);


  oSList.Text := sText;
  oSList.SaveToFile(ChangeFileExt (aFileName, '.tab'));

//
//  sText:=DEF_WOR;
//  sText:=ReplaceStr (sText, ':FileName', ExtractFileName(aFileName));
//  sText:=ReplaceStr (sText, ':CoordSys', DoMakeCoordSys());
//
//  oSList.Text := sText;
//  oSList.SaveToFile(ChangeFileExt (aFileName, '.wor'));
//


  FreeAndNil(oSList);

end;




end.
