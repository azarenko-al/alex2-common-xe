unit u_mapinfo_TAB_file;

interface
uses
  Classes, SysUtils,

  u_func
  ;

type
  TCoordSysType = (csWGS84, csPulkovo42);
  TProjectionType = (ptUTM, ptGaussKruger);

  TRasterHeaderRec_new = packed
  record
    Projection : TProjectionType;
    CoordSys   : TCoordSysType;

    ZoneNum    : integer; //GK

    Points : array[0..2] of record
                              x,y: double;
                              bmp_x,bmp_y: Integer;
                            end;
  end;

  // -------------------------
  TCoordSysRec = record
  // -------------------------
  //  Projection     : TProjectionType;
   // CoordSys       : TCoordSysType;

    Projection_code     : Integer;
    CoordSys_code       : Integer;



    CentralMeridianDegree: Integer;
    UTM_ZoneNum    : Integer;
    GK_ZoneNum     : Integer;

    IsSouth        : Boolean;

    East_offset    : Integer;
    North_offset   : Integer;
  end;


  

  function tab_GetCoordSys1(aCoordSysStr: string; var aRec: TCoordSysRec):
      Boolean;

  function tab_GetCoordSysStr(aRec: TRasterHeaderRec_new): string;

  function tab_CoordSysRecToStr(aRec: TCoordSysRec): string;


  

implementation



function tab_CoordSysRecToStr(aRec: TCoordSysRec): string;
begin
  Result := Format(
             'Projection: %d' + CRLF +
             'CoordSys:   %d' + CRLF +
             'CentralMeridianDegree: %d' + CRLF +
             'UTM_ZoneNum: %d' + CRLF +
             'GK_ZoneNum:  %d' ,
             
             [
              aRec.Projection_code,
              aRec.CoordSys_code,
              aRec.CentralMeridianDegree,
              aRec.UTM_ZoneNum,
              aRec.GK_ZoneNum
             ]);
end;



//-------------------------------------------------------------------
function tab_GetCoordSys1(aCoordSysStr: string; var aRec: TCoordSysRec):
    Boolean;
//-------------------------------------------------------------------
//const
//  DEF='CoordSys Earth Projection 8, 104, "m", ';//51, 0, 1,  9500000, 0';
 // DEF: string='CoordSys Earth Projection 8, 104, "m",';//51, 0, 1,  9500000, 0';

const
  DEF_HEADER = 'CoordSys Earth Projection ';

var
  i: Integer;
  oStrList: TStringList;
begin
  FillChar(aRec, SizeOf(aRec), 0);


  if Pos(DEF_HEADER, aCoordSysStr)=1 then
    aCoordSysStr:=Copy (aCoordSysStr, Length(DEF_HEADER)+1, 100)
  else
    raise Exception.Create('');

 // i :=Pos(#0, aCoordSysStr);
 // aCoordSysStr := Copy(aCoordSysStr, 1, i-1);

  oStrList:=StringToStringList(aCoordSysStr, ',');

  i:=oStrList.Count;

  Result:=False;

  if oStrList.Count=8 then
    with aRec do
  begin
    Projection_code := StrToInt(oStrList[0]);
    CoordSys_code   := StrToInt(oStrList[1]);

    //"m",  oStrList[2]

    CentralMeridianDegree := StrToInt(oStrList[3]);

    East_offset  :=StrToInt(oStrList[6]);
    North_offset :=StrToInt(oStrList[7]);
    IsSouth      :=aRec.North_offset > 0;

    UTM_ZoneNum   :=aRec.CentralMeridianDegree div 6 + 1+ 30;  //51=39 ����

//    if aRec.MeridianDegree>0 then
    GK_ZoneNum := CentralMeridianDegree div 6;
    if GK_ZoneNum<0 then
      GK_ZoneNum:=GK_ZoneNum + 60;

    Result:=True;

  end;

  FreeAndNil(oStrList);

end;



// ---------------------------------------------------------------
function tab_GetCoordSysStr(aRec: TRasterHeaderRec_new): string;
// ---------------------------------------------------------------
var
  d,k: Integer;
begin
 // Result := 'CoordSys Earth Projection ';

  case aRec.CoordSys of
    csWGS84:     Result := 'CoordSys Earth Projection 8, 101, "m", ';
    csPulkovo42: Result := 'CoordSys Earth Projection 1, 1001, "m", ';
  end;

  d:=aRec.ZoneNum*6 + 3;
  k:= 1000000 * aRec.ZoneNum  + 500000;

  Result := Result + Format('%d, 0, 1, %d, 0', [d,k]);

end;

end.
