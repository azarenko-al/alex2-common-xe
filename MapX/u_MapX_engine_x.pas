unit u_MapX_engine_x;

interface


uses

  SysUtils, System.Classes,  Math, Variants, Windows,  StrUtils, Dialogs,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms,

  Xml.XMLIntf,

  u_xml_document,
               

  
  u_MapX,
         
  
  MapXLib_TLB,

//  u_mapX_classes,
  
  u_MapX_func,
  
  u_func,
  
//  u_mapx,
  
  u_Geo
  ;


type
  TOnMapCursorEvent = procedure (aBLPoint: TBLPoint) of object;
  TOnMapPolyEvent   = procedure (var aBLPoints: TBLPointArrayF) of object;
  TOnMapMouseDownEvent = procedure (aBLPoint: TBLPoint) of object;


  TMapEngine_X = class(TObject)
    procedure SetCenter_LatLon(aLat, aLon: double);
  private
    FMoveBitmap: Vcl.TBitmap;
    FOldMousePos: Windows.TPoint;
    CursorPosXY: Windows.TPoint;

 //   FMoveBitmap: TBitmap;

    FCursorPos: TBLPoint;

    FLeftButtonClicked : Boolean;
    FRightButtonPressed: boolean;

  private
  //  FMap: TMap;
    FMapX: CMapX;    
  private
    FOnMapCursor: TOnMapCursorEvent;
    FOnMapMouseDown: TOnMapMouseDownEvent;
    FOnMapPoly: TOnMapPolyEvent;

    function GetCenter: TBLPoint;
//    procedure ExtractTileXYZ(aFileName: string; var aX, aY, aZ: integer);
    function GetLayerIndexByName_from_1(aName: string): integer;
//    procedure BeginUpdate;
//    procedure EndUpdate;
    function GetScaleInCm: double;
  { TODO : remove }
//    procedure DeleteMapsByDir (aCalcRegionDir: string);
//    procedure DeleteMapsByDir(aDir: string);
    procedure Log(aMsg: string);
    procedure Map1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y:
        Integer);
        
    procedure Map1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure Map1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y:
        Integer);
        
    procedure Map1MouseWheel(Sender: TObject; Flags: integer; zDelta: Smallint; var X, Y:
        Single; var EnableDefault: WordBool);
        
    procedure Map1PolyToolUsed(Sender: TObject; ToolNum: Smallint; Flags: Integer; const
        Points: IDispatch; bShift, bCtrl: WordBool; var EnableDefault: WordBool);
    procedure PaintBitmapWhileMoving;
    procedure Init;

    
//    function MapToScreen(aBLPoint: TBLPoint): TPoint;
//    procedure SetDefaultDisplayCoordSys;
    procedure SetOnMapCursor(const Value: TOnMapCursorEvent);
//    procedure Tile_Delete_outside_test;
//    procedure Tile_Del_outside_Rect(aRect: TTileBounds);
  private
    FFirstPointClicked: boolean;
    FFirstPointPos: TBLpoint;
  public         
    procedure Init_CMapX(aMapX: CMapX);


    procedure MouseDown;
    procedure MouseUp;  
//    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);


  
//    MapList: TMapViewList;

    procedure AlignBoundsForBLVector(aBLVector: TBLVector);
    procedure FormKeyDown(var Key: Word; Shift: TShiftState; aCursorPos: TBLPoint);

    function GetBounds: TBLRect;
    function GetPlanBounds: TBLRect;
    function GetScale: double;

//    procedure PageDown;
  //  procedure PageUp;
//    procedure PreviousTool(); virtual; abstract;
    procedure SetBounds(aBLRect: TBLRect);

    function Workspace_Load(aFileName: string): boolean;
    
    procedure ZoomInRect(aRect: TBLRect);

    constructor Create;
    destructor Destroy; override;
    
    procedure Del_Map(aFileName: string);

    function Add_Map(aFileName: string): CMapXLayer;
    
    function Add_Tile(aFileName: string; aLat, aLon: double; ablRect: TblRect):
        CMapXLayer;


    procedure Clear;
    procedure ClearLegend;
    function CreateTempLayerWithFields(aName: string; aParams: array of
        TmiFieldRec): CMapXLayer;
        
    procedure Del_List(aList: TStrings);
    function GetLayerByName(aName: string): CMapXLayer;
    function GetScale_Degree: double;

    function MapExists(aFileName: string): Boolean;
    procedure MouseMove;
    procedure RemoveLayer(aName: string);
//    procedure SetBoundsTest;
//    procedure SetBoundsTest_new;

    procedure SetProj_Mercator;
//    procedure SetScale(aValue: double);
    procedure Set_LongLat_WGS84;
    function ShowLegend(aTitle: string; aMapFileName: string): boolean;


    procedure TempLayer_Clear(aName: string);
    function TempLayer_Create(aLayerName: string): CMapxLayer;

    procedure Test_Unit;
    
//    function GetTileBounds(var aRec: TTileBounds): Boolean;
    procedure TilesClear;
    function Workspace_Save(aFileName: string): boolean;
    procedure ZoomPlan;

    property OnMapMouseDown: TOnMapMouseDownEvent read FOnMapMouseDown write FOnMapMouseDown;
    property OnMapPoly: TOnMapPolyEvent read FOnMapPoly write FOnMapPoly;
    property OnMapCursor: TOnMapCursorEvent read FOnMapCursor write SetOnMapCursor;

  end;


const
   // Custom Tool Constants
   CUSTOM_ARROW_TOOL    = 1;
   CUSTOM_POINT_TOOL    = 2;
   CUSTOM_LINE_TOOL     = 3;
   CUSTOM_POLYGON_TOOL  = 4;
   CUSTOM_POLYLINE_TOOL = 5;
   CUSTOM_RING_TOOL     = 6;
   CUSTOM_RECT_TOOL     = 7;

   CUSTOM_INFO_TOOL     = 8;
   CUSTOM_LINER         = 9;
   CUSTOM_SELECT_TOOL   = 10;

  // CUSTOM_NEIGHBOR_TOOL = 11;
   //CUSTOM_CoChannelFreqs_TOOL = 12;

   RULERTOOLID = 500;


   ELLIPSOID_WGS  = 28;
   ELLIPSOID_KRAS = 3;



implementation



  
constructor TMapEngine_X.Create;
begin
  inherited Create;
       
//  aMap.CurrentTool:=miSelectTool;
  
  
  FMoveBitmap := VCL.Graphics.TBitmap.Create();
//  FMap := aMap;

//  Init;
  
 // MapList := TMapViewList.Create();
end;

destructor TMapEngine_X.Destroy;
begin
//  FreeAndNil(MapList);
  FreeAndNil(FMoveBitmap);

  inherited;
end;


procedure TMapEngine_X.AlignBoundsForBLVector(aBLVector: TBLVector);
var blRect: TBLRect;
begin
  blRect:=geo_BoundBLVector(aBLVector);
  blRect:=geo_ZoomRect (blRect, 0.01);
  SetBounds (blRect);
end;


//--------------------------------------------------------------------
procedure TMapEngine_X.Map1MouseWheel(Sender: TObject; Flags: integer; zDelta: Smallint;
    var X, Y: Single; var EnableDefault: WordBool);
//--------------------------------------------------------------------
var
  dNewZoom: double;
begin
  EnableDefault:= false;

  dNewZoom:= IIF(zDelta < 0, FMapX.Zoom * 2, FMapX.Zoom / 2);
  if dNewZoom > 0 then
    FMapX.ZoomTo(dNewZoom, FCursorPos.L, FCursorPos.B);

end;


//--------------------------------------------------------------------
function TMapEngine_X.GetPlanBounds: TBLRect;
//--------------------------------------------------------------------
begin
//  Result:=mapx_GetPlanBounds (FMap);
  Result:=mapx_GetPlanBounds_CMapX (FMapX);
end;

//--------------------------------------------------------------------
function TMapEngine_X.GetScale_Degree: double;
//--------------------------------------------------------------------
var
  e: Double;
  e1: Double;
begin
  FMapX.MapUnit := miUnitDegree;  
  FMapX.PaperUnit := miUnitDegree;  

  e:=FMapX.Zoom;
 // e:=FMap.MapPaperWidth;  


  e1:= FMapX.Zoom / FMapX.MapPaperWidth;  
 
  Result:=FMapX.Zoom;
  
//  e:=FMap.Mapw;  
  
//  Result:= aMap.Zoom / aMap.MapPaperWidth;  

  
//  Result:=mapx_GetPlanBounds (FMap);
end;




// -------------------------------------------------------------------
function TMapEngine_X.GetScale: double;
// -------------------------------------------------------------------
begin
  Result:=mapx_GetScale_CMapX (FMapX);
   
end;

//
//// -------------------------------------------------------------------
//procedure TMapEngine_X.SetScale(aValue: double);
//// -------------------------------------------------------------------
//begin
//  mapx_SetScale (FMap, aValue);
//   
//end;



//
//procedure TMapEngine_X.PageDown;
//var blRect: TBLRect;
//begin
//  blRect:=GetBounds();
//  blRect:=geo_MoveBLRect (blRect, - Abs(blRect.TopLeft.B - blRect.BottomRight.B), 0);
//  ZoomInRect(blRect);
//end;
//
//procedure TMapEngine_X.PageUp;
//var blRect: TBLRect;
//begin
//  blRect:=GetBounds();
//  blRect:=geo_MoveBLRect (blRect, + Abs(blRect.TopLeft.B - blRect.BottomRight.B), 0);
//  ZoomInRect(blRect);
//end;
//



// -------------------------------------------------------------------
procedure TMapEngine_X.FormKeyDown(var Key: Word; Shift: TShiftState; aCursorPos: TBLPoint);
// -------------------------------------------------------------------
//   function DoGetBounds: TBLRect;
//   begin
//      Result:=mapx_GetBounds (FMap);
//   end;
//
//   procedure DoSetBounds(aBLRect: TBLRect);
//   begin
//     mapx_SetBounds (FMap, aBLRect);
//   end;
//
//   procedure DoSetCenter(aBLPoint: TBLPoint);
//   begin
//      mapx_SetCenter (FMap, aBLPoint);
//   end;
//
//
//var
//  blRect: TBLRect;
  
//const
//  VK_PAGEDOWN_ = 34;
//  VK_PAGEUP_ = 33;

begin
  {
  if Key = VK_ESCAPE then
  begin
    mapx_ClearSelection (FMap);

///////////////    ClearSelection();
//    SetDefaultTool();
  end else
  }

//  if (Key=VK_PAGEDOWN_)  then
//  begin
//    blRect:=DoGetBounds();
//   // blRect:=
//    blRect:=geo_MoveBLRect (blRect, - Abs(blRect.TopLeft.B - blRect.BottomRight.B), 0);
//
////    ZoomInRect(blRect);
//    DoSetBounds(blRect);
//  end else
//
//  // ---------------------------------------------------------------
//  if (Key=VK_PAGEUP_)  then
//  // ---------------------------------------------------------------
//  begin
//    blRect:=DoGetBounds();
//   // blRect:=
//    blRect:=geo_MoveBLRect (blRect, + Abs(blRect.TopLeft.B - blRect.BottomRight.B), 0);
////    ZoomInRect(blRect);
//    DoSetBounds(blRect);
//  end else

//  if Key = VK_SPACE then
//  begin
//    DoSetCenter(aCursorPos);
//  end else

//  if (Key=VK_ADD) or (Key=187) then //btn +
//  begin
//    blRect:=geo_MoveBLRectToCenter (aCursorPos, DoGetBounds());
//    blRect:=geo_ZoomIn(blRect);
//    DoSetBounds(blRect);
////    ZoomInRect(geo_ZoomIn(blRect));
//  end else
//
//  if (Key=VK_SUBTRACT) or (Key=189) then
//  begin
//    blRect:= geo_MoveBLRectToCenter (aCursorPos, DoGetBounds());
//    blRect:=geo_ZoomOut(blRect, 2);
//    DoSetBounds (blRect);
//  end;

end;

//--------------------------------------------------------------------
function TMapEngine_X.GetBounds: TBLRect;
//--------------------------------------------------------------------
begin
//  Result:=mapx_GetBounds (FMap);


//  TLatLonBounds
//  Result:=mapx_XRectangleToBLRect(Map1.Bounds);
end;

procedure TMapEngine_X.Log(aMsg: string);
begin

(*  if Assigned(FOnLog) then
    FOnLog('map: ' + aMsg);
*)
end;



//--------------------------------------------------------------------
// ��������� �������� ����� �� ��������� ������ ������ ����
procedure TMapEngine_X.PaintBitmapWhileMoving;
//--------------------------------------------------------------------
var
  DC: HDC;
  brush: HBRUSH;
  r1, r2: TRect;
  iOffsetX, iOffsetY: Integer;
  k: Integer;
begin
  if FRightButtonPressed then
  begin
    iOffsetX:= CursorPosXY.X - FOldMousePos.X;
    iOffsetY:= CursorPosXY.Y - FOldMousePos.Y;

    if (iOffsetX=0) and (iOffsetY=0) then
      exit;

    Screen.Cursor:=crHandPoint;

//    CursorSet (crHandPoint);

    DC:= GetDC(FMapX.hWnd);
    brush:= CreateSolidBrush(clWhite);



    GetClientRect(FMapX.hWnd, r1); 

    
   // FMapX.Map
    
   // r1:= FMapX.ClientRect;
    r2:= r1;
    if iOffsetY<0 then
      r1.Top:= r1.bottom+iOffsetY
    else
      r1.bottom:= r1.top+iOffsetY;

    if iOffsetX<0 then
      r2.left:= r2.right+iOffsetX
    else
      r2.right:= r2.left+iOffsetX;

    FillRect(DC, r1, brush);
    FillRect(DC, r2, brush);

    DeleteObject(brush);
    BitBlt(DC, iOffsetX, iOffsetY, Round(FMapX.MapScreenWidth), Round(FMapX.MapScreenHeight), 
         FMoveBitmap.Canvas.Handle, 0, 0, SRCCOPY);

    ReleaseDC(FMapX.hWnd, DC);
  end;
end;

//--------------------------------------------------------------------
procedure TMapEngine_X.SetBounds(aBLRect: TBLRect);
//--------------------------------------------------------------------
begin
  Log(geo_FormatBLRect(aBLRect));

  mapx_SetBounds_CMapX (FMapX, aBLRect);
end;

//
//
////--------------------------------------------------------------------
//procedure TMapEngine_X.SetBoundsTest_new;
////--------------------------------------------------------------------
//const
//// epsg 3857
//  DEF_MAX_20037508 =  2 * 20037508.34279000  / 1000;
//
//var
//  blRect: TBLRect;
//
//  oMapFile: TmiMap;
//  sFileName: string;
//    
//begin
//{
//  oMapFile:= TmiMap.Create;
//    
// }
//   
//                     
////  FMap
// // FMap.MapUnit := miUnitKilometer;  //13
//
//
//  blRect.TopLeft.B:=90*2;
//  blRect.TopLeft.L:=-360*2 ;
//
//  blRect.BottomRight.B:=-90*2;
//  blRect.BottomRight.L:=360*2;  
//
//  
////  Log(geo_FormatBLRect(aBLRect));
//
//  SetBounds (BLRect);
//
//// with aBLRect do
////    aMap.Bounds.Set_(TopLeft.L, TopLeft.B,
////                BottomRight.L, BottomRight.B);
//
//
//end;







//--------------------------------------------------------------------
procedure TMapEngine_X.Init;
//--------------------------------------------------------------------
var
  vEmpty: Variant;
begin
  Assert(Assigned(FMapX));

//  FMap := aMap;

  FMapX.Title.X:=0;
  FMapX.Title.Y:=0;
  FMapX.Title.Editable:= False;
  FMapX.Title.Border:= False;
  FMapX.Title.Caption:=' ';
  FMapX.Title.Visible:=False;

  TVarData(vEmpty).vType := varError;
  TVarData(vEmpty).vError := DISP_E_PARAMNOTFOUND;


  FMapX.CreateCustomTool(RULERTOOLID,          miToolTypeLine,    miCrossCursor, miCrossCursor, miCrossCursor, vEmpty);

  FMapX.CreateCustomTool(CUSTOM_LINER,         miToolTypeLine,    miCrossCursor, miCrossCursor, miCrossCursor, vEmpty);
  FMapX.CreateCustomTool(CUSTOM_ARROW_TOOL,    miToolTypePoint,   miArrowToolCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMapX.CreateCustomTool(CUSTOM_POINT_TOOL,    miToolTypePoint,   miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMapX.CreateCustomTool(CUSTOM_INFO_TOOL,     miToolTypePoint,   miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMapX.CreateCustomTool(CUSTOM_LINE_TOOL,     miToolTypeLine,    miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMapX.CreateCustomTool(CUSTOM_POLYGON_TOOL,  miToolTypePoly,    miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMapX.CreateCustomTool(CUSTOM_POLYLINE_TOOL, miToolTypePoly,    miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMapX.CreateCustomTool(CUSTOM_RING_TOOL,     miToolTypeCircle,  miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMapX.CreateCustomTool(CUSTOM_RECT_TOOL,     miToolTypeMarquee, miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMapX.CreateCustomTool(CUSTOM_SELECT_TOOL,   miToolTypeMarquee, miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);


  FMapX.MousewheelSupport:=3; //3 - miFullMousewheelSupport
  FMapX.BackColor    :=clWhite;
  FMapX.CurrentTool  := miZoomInTool;

  FMapX.PaperUnit:= miUnitCentimeter;
  FMapX.MapUnit  := miUnitKilometer;

 // FMapX.ShowHint:= true;
  FMapX.InfotipSupport:= False;

//  
//
//  FMapX.
//
//  FMapX.OnPolyToolUsed :=Map1PolyToolUsed;
//  FMapX.OnMouseMove    :=Map1MouseMove;
//  FMapX.OnMouseWheel   :=Map1MouseWheel;
//  FMapX.OnMouseUp      :=Map1MouseUp;
////  FMap.OnMouseDown    :=Map1Mouse;
//  FMap.OnMouseDown    :=Map1MouseDown;        
//               // miMercator = $0000000A;
//                                   
//  FMap.NumericCoordSys.Set_ (miLongLat, DATUM_WGS84,
////  FMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
//      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
//      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
//      EmptyParam, EmptyParam, EmptyParam, EmptyParam);
//

               
//  CoordSys Earth Projection 10, 104, "m", 0
//
//    procedure Set_(Type_: CoordSysTypeConstants; Datum: OleVariant; Units: OleVariant; 
//                   OriginLongitude: OleVariant; OriginLatitude: OleVariant; 
//                   StandardParallelOne: OleVariant; StandardParallelTwo: OleVariant; 
//                   Azimuth: OleVariant; ScaleFactor: OleVariant; FalseEasting: OleVariant; 
//                   FalseNorthing: OleVariant; Range: OleVariant; Bounds: OleVariant; 
//                   AffineTransform: OleVariant); dispid 15;


               
end;                    


procedure TMapEngine_X.Set_LongLat_WGS84;
begin
  FMapX.NumericCoordSys.Set_ (miLongLat, DATUM_WGS84,
//  FMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam);
  
end;


// ---------------------------------------------------------------
procedure TMapEngine_X.SetProj_Mercator;
// ---------------------------------------------------------------
var
  k: Integer;
  v1: Variant;
  v2: Variant;  
  v3: Variant;  
begin
  //it;
  
//  k:=FMap.NumericCoordSys.type_;  
//  k:=FMap.NumericCoordSys.Datum.Ellipsoid;
//  k:=FMap.NumericCoordSys.Units;

//Exit;


  v1:=FMapX.DisplayCoordSys.type_;  
  v2:=FMapX.DisplayCoordSys.Datum.Ellipsoid;
  v3:=FMapX.DisplayCoordSys.Units;

//  Assert( FMap.DisplayCoordSys.Units = miUnitDegree);

  
//
//type
//  MapUnitConstants = TOleEnum;
//const
//  miUnitMile = $00000000;
//  miUnitKilometer = $00000001;
//  miUnitInch = $00000002;
//  miUnitFoot = $00000003;
//  miUnitYard = $00000004;
//  miUnitMillimeter = $00000005;
//  miUnitCentimeter = $00000006;
//  miUnitMeter = $00000007;
//  miUnitSurveyFoot = $00000008;
//  miUnitNauticalMile = $00000009;
//  miUnitTwip = $0000000A;
//  miUnitPoint = $0000000B;
//  miUnitPica = $0000000C;
//  miUnitDegree = $0000000D;
//  miUnitLink = $0000001E;
//  miUnitChain = $0000001F;
//  miUnitRod = $00000020;
//

  
 // Exit;

  //  miUnitDegree = $0000000D;

  //    procedure Set_(Type_: CoordSysTypeConstants; Datum: OleVariant; Units: OleVariant; 
//                   OriginLongitude: OleVariant; OriginLatitude: OleVariant; 

//  FMap.NumericCoordSys.Datum:=DATUM_WGS84;
//  
//    procedure Set_(Type_: CoordSysTypeConstants; Datum: OleVariant; Units: OleVariant; 
//                   OriginLongitude: OleVariant; OriginLatitude: OleVariant; 
//                   StandardParallelOne: OleVariant; StandardParallelTwo: OleVariant; 
//                   Azimuth: OleVariant; ScaleFactor: OleVariant; FalseEasting: OleVariant; 
//                   FalseNorthing: OleVariant; Range: OleVariant; Bounds: OleVariant; 
//                   AffineTransform: OleVariant); dispid 15;

 
 // Exit;
  
 // FMap.DisplayCoordSys.Set_ (miMercator, DATUM_WGS84, miUnitDegree,
 
 {  FMap.DisplayCoordSys.Set_ (miMercator, DATUM_WGS84, 
  //miUnitDegree,  
   miUnitMeter, 
   0,
//  FMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, //EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam);
 }

 
  FMapX.MapUnit:=miUnitKilometer;

  FMapX.DisplayCoordSys.Set_ (miMercator, DATUM_WGS84, 
  //miUnitDegree,  
   miUnitKilometer,
//   miUnitMeter,   
   0,
//  FMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, //EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam);
      
//  Exit;

  ///////////////////////////////

  FMapX.NumericCoordSys.Set_ (miMercator, DATUM_WGS84, 
  //miUnitDegree,  
   miUnitKilometer,
//   miUnitMeter,   
   0,
//  FMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, //EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam);
            
               
//  CoordSys Earth Projection 10, 104, "m", 0
//
//    procedure Set_(Type_: CoordSysTypeConstants; Datum: OleVariant; Units: OleVariant; 
//                   OriginLongitude: OleVariant; OriginLatitude: OleVariant; 
//                   StandardParallelOne: OleVariant; StandardParallelTwo: OleVariant; 
//                   Azimuth: OleVariant; ScaleFactor: OleVariant; FalseEasting: OleVariant; 
//                   FalseNorthing: OleVariant; Range: OleVariant; Bounds: OleVariant; 
//                   AffineTransform: OleVariant); dispid 15;

  
  //Assert( FMap.DisplayCoordSys.Units = miUnitMeter);

  v1:=FMapX.DisplayCoordSys.type_;  
  v2:=FMapX.DisplayCoordSys.Datum.Ellipsoid;
  v3:=FMapX.DisplayCoordSys.Units;
  

  
  v1:=FMapX.NumericCoordSys.type_;  
  v2:=FMapX.DisplayCoordSys.Datum.Ellipsoid;
  v3:=FMapX.DisplayCoordSys.Units;
  
  
end;



function TMapEngine_X.Add_Map(aFileName: string): CMapXLayer;
begin
  Assert(FileExists(aFileName));

// if MapExists(aFileName)
//  aFileName

  Result:=map_MapAdd_Ex_CMapX(FMapX, aFileName);
end;



//Map1.PropertyPage;

procedure TMapEngine_X.Clear;
begin
  FMapX.Layers.RemoveAll;
  FMapX.DataSets.RemoveAll;
            
end;
                

// ---------------------------------------------------------------
procedure TMapEngine_X.Del_List(aList: TStrings);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to aList.Count-1 do
    Del_Map(aList[i]);

end;




// ---------------------------------------------------------------
procedure TMapEngine_X.Del_Map(aFileName: string);
// ---------------------------------------------------------------
var
  iInd: Integer;
begin
//  GetLayerIndexByName_from_1()
  iInd:=mapx_GetLayerIndexByFileName_CMapX(FMapX, aFileName);

//  iInd:=mapx_GetLayerIndexByFileName_from_1( FMap, aFileName);
 //  v:=mapx_GetLayerByFileName(sFile);

  if iInd>0 then
     FMapX.Layers.Remove(iInd);

 // MapList.Del_Map (aFileName)   ;
     
end;
          

// ---------------------------------------------------------------
function TMapEngine_X.MapExists(aFileName: string): Boolean;
// ---------------------------------------------------------------
var
  iInd: Integer;
begin
//  iInd:=mapx_GetLayerIndexByFileName_from_1( FMap, aFileName);

  Result:=iInd>0
     
end;
          

procedure TMapEngine_X.SetOnMapCursor(const Value: TOnMapCursorEvent);
begin
  FOnMapCursor := Value;
end;

//--------------------------------------------------------------------
function TMapEngine_X.Workspace_Load(aFileName: string): boolean;
//--------------------------------------------------------------------
begin
  Result := mapx_LoadWorkspace_CMapX (FMapX, aFileName);
end;

//--------------------------------------------------------------------
function TMapEngine_X.Workspace_Save(aFileName: string): boolean;
//--------------------------------------------------------------------
begin
 // Result := 
  mapx_SaveWorkspace_CMapX (FMapX, aFileName);
end;



procedure TMapEngine_X.ZoomInRect(aRect: TBLRect);
begin
  SetBounds (aRect);
end;

procedure TMapEngine_X.ZoomPlan;
begin
  SetBounds (GetPlanBounds());

end;




//--------------------------------------------------------------------
procedure TMapEngine_X.Map1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
//--------------------------------------------------------------------
var //iCoord,
  iScale: integer;
  dMapLengthInCm, dMapLengthInKm, dZoom, dMapX, dMapY: Double;
  dX, dY: Single;

  //  S, sHint: string;
begin
  CursorPosXY.X:= X; //fScreenX:= X;
  CursorPosXY.Y:= Y; //fScreenY:= Y;

  PaintBitmapWhileMoving();

  dX:=X;  dY:=Y;
//  Map1.ConvertCoord(fScreenX, fScreenY, dMapX, dMapY, miScreenToMap);
  FMapX.ConvertCoord(dX, dY, dMapX, dMapY, miScreenToMap);

  FCursorPos:= MakeBLPoint(dMapY, dMapX);
//  DoOnCursor (FCursorPos);

  if Assigned(FOnMapCursor)  then
    FOnMapCursor(FCursorPos);


 // RefreshStatusBar();


end;



//--------------------------------------------------------------------
procedure TMapEngine_X.Map1PolyToolUsed(Sender: TObject; ToolNum: Smallint; Flags: Integer;
    const Points: IDispatch; bShift, bCtrl: WordBool; var EnableDefault: WordBool);
//--------------------------------------------------------------------
var
  ps: CMapXPoints;
  vPoint: CMapXPoint;
  i: integer;

  FBLPointArr: TBLPointArrayF;

begin
  case Flags of

    miPolyToolBegin: ;{ Someone's beginning the use of a PolyTool.. }
    miPolyToolEndEscaped: ; { The users hit 'Esc' or backspaced }

    miPolyToolEnd:  { The user finished using a PolyTool by double clicking }

      if ToolNum = CUSTOM_POLYLINE_TOOL then
      begin
        ps:=CMapXPoints(Points);

        FBLPointArr.Count:=ps.Count;

      //  iCount:=ps.Count;

        for i:=1 to FBLPointArr.Count do
        begin

            vPoint:=ps.Item[i];


          FBLPointArr.Items[i-1]:=MakeBLPoint (vPoint.Y, vPoint.X);
        end;

        if Assigned(FOnMapPoly) then
          FOnMapPoly(FBLPointArr);


      //  DoOnPoly (FBLPointArr);
      end;
  end;
end;

//-------------------------------------------------------------------
procedure TMapEngine_X.Map1MouseUp(Sender: TObject; Button: TMouseButton; Shift:
    TShiftState; X, Y: Integer);
//-------------------------------------------------------------------
var
  iOffsetX, iOffsetY: Integer;
begin
  inherited;

  //  TMouseButton = (mbLeft, mbRight, mbMiddle);


  
//  GetKeyState(VK_LBUTTON) 
//  
//  var
//  pt : tPoint;
//begin
//  pt := Mouse.CursorPos; 
  
//  Mouse.

  
  if FRightButtonPressed then
  begin
    Screen.Cursor:=crDefault;

    if (FOldMousePos.X = X) and (FOldMousePos.Y = Y) then
    begin
      FRightButtonPressed:= false;
//      FLeftButtonPressed:= false;
      FMapX.AutoRedraw:= true;
    //////  PopupMenu1.Popup(Mouse.CursorPos.x, Mouse.CursorPos.y);

      exit;
    end;

    iOffsetX:= CursorPosXY.X - FOldMousePos.X;
    iOffsetY:= CursorPosXY.Y - FOldMousePos.Y;

(*    if (iOffsetX=0) and (iOffsetY=0) then begin
      FRightButtonPressed:= false;
      exit;
    end;*)

    //��� ���������� ������ ���� �������� �����
    FMapX.Pan (-iOffsetX, iOffsetY);

    FRightButtonPressed:= false;
    FMapX.AutoRedraw:= true;
    FMapX.Refresh;
  end;

//  FLeftButtonPressed:= False;

end;


  //  TMouseButton = (mbLeft, mbRight, mbMiddle);


  
//  GetKeyState(VK_LBUTTON) 
//  
//  var
//  pt : tPoint;
//begin
//  pt := Mouse.CursorPos; 
  
//  Mouse.



procedure TMapEngine_X.MouseDown;
begin


//Map1MouseDown(Sender: TObject; Button: TMouseButton; Shift:
 //   TShiftState; X, Y: Integer);

//  TShiftState = set of (ssShift, ssAlt, ssCtrl,
//    ssLeft, ssRight, ssMiddle, ssDouble, ssTouch, ssPen, ssCommand, ssHorizontal);

      
end;


procedure TMapEngine_X.MouseUp;
begin


//  TShiftState = set of (ssShift, ssAlt, ssCtrl,
//    ssLeft, ssRight, ssMiddle, ssDouble, ssTouch, ssPen, ssCommand, ssHorizontal);

      
end;


procedure TMapEngine_X.MouseMove;
begin


//  TShiftState = set of (ssShift, ssAlt, ssCtrl,
//    ssLeft, ssRight, ssMiddle, ssDouble, ssTouch, ssPen, ssCommand, ssHorizontal);

      
end;



// -------------------------------------------------------------------
procedure TMapEngine_X.Map1MouseDown(Sender: TObject; Button: TMouseButton; Shift:
    TShiftState; X, Y: Integer);
// -------------------------------------------------------------------
(*
    //------------------------------------------------------------
    procedure DoOnMouseDown ();
    //------------------------------------------------------------
    begin
      FFirstPointClicked:=True;
      FFirstPointPos:=FCursorPos;

      if Assigned(FOnMapMouseDown) then
        FOnMapMouseDown (FCursorPos);
    end;
*)

var
  sObjName: Integer;
  i, iID  : Integer;
  sAction: String;
begin
{if FIsTempPan then begin
  //  SetWindowTool (toolObject);
    FIsTempPan:=False;
 end;  }
  FLeftButtonClicked:= (not (ssRight in Shift)) AND (ssLeft in Shift);

  FRightButtonPressed:= ((ssRight in Shift)) AND (not (ssLeft in Shift));

{
  if (not FIsTempPan) and (ssRight in Shift) then begin
    SetWindowTool (toolPan);
    FIsTempPan:=True;
  end else

  if (FIsTempPan) and (ssRight in Shift) then
  begin
    if Assigned(FindForm( Tfrm_Map_Tool_Liner.ClassName)) then
      SetWindowTool(toolLiner)
    else


    SetWindowTool(toolObject);

    FIsTempPan:=False;
  end;}


  if FRightButtonPressed then
  begin
    //CursorSet (crHandPoint);
    FMapX.AutoRedraw:= false;

    FOldMousePos.X:= X;
    FOldMousePos.Y:= Y;
//    FOldMousePos:= CursorPosXY;

    FMoveBitmap.Width:=  Round(FMapX.MapScreenWidth);
    FMoveBitmap.Height:= Round(FMapX.MapScreenHeight);

  ////////  FMapX.PaintTo (FMoveBitmap.Canvas.Handle, 0, 0);
  end;


  FFirstPointClicked:=True;
  FFirstPointPos:=FCursorPos;

  if Assigned(FOnMapMouseDown) then
    FOnMapMouseDown (FCursorPos);

 // DoOnMouseDown();

end;


//------------------------------------------------------------------------------
function TMapEngine_X.GetScaleInCm: double;
//------------------------------------------------------------------------------
begin
  FMapX.PaperUnit:= miUnitCentimeter;
  FMapX.MapUnit  := miUnitKilometer;

  Result:= Round(FMapX.Zoom * 100000 / FMapX.MapPaperWidth);
end;


procedure TMapEngine_X.SetCenter_LatLon(aLat, aLon: double);
begin
  FMapX.ZoomTo (FMapX.Zoom, aLon, aLat);   // X: Double; Y: Double);

// mapx_SetCenter (FMap, MakeBLPoint(aLat, aLon));

end;


function TMapEngine_X.GetCenter: TBLPoint;
begin
 /////// Result:=mapx_GetCenter(FMap);

end;



//------------------------------------------------------------------------------
function TMapEngine_X.CreateTempLayerWithFields(aName: string; aParams: array of
    TmiFieldRec): CMapXLayer;
//------------------------------------------------------------------------------
var 
  rRec: TmiMapCreateRec;

  vLayer: CMapXLayer;
  
begin
{
  vLayer:= mapx_GetLayerByName(FMap, aName);


//  GetLayerIndexByName_from_1()
 // Result:= 
//  LayerByName(aName);

  if not Assigned(vLayer) then
  begin
    rRec.Fields:= Make_miFieldArray(aParams);
    Result:= mapx_CreateTempLayerWithFeatures(FMap, aName, rRec, nil);//, 1, true);
  end;
  }
  
end;




//--------------------------------------------------------------------
function TMapEngine_X.GetLayerByName(aName: string): CMapXLayer;
//--------------------------------------------------------------------
begin
//  result :=mapx_GetLayerByName (FMap, aName);

end;



//------------------------------------------------------------------------------
procedure TMapEngine_X.Test_Unit;
//------------------------------------------------------------------------------

var
  e: Double;
  vLayer: CMapXLayer;
  sName: string;
  i: Integer;
 // k: Integer;
begin
 
//
//  for i:=1 to FMap.Layers.Count do
//  begin  
//    vLayer:= FMap.Layers.Item[i];
//    
//    sName:= vLayer.Name;
//
//
////    vLayer.
//  
//    
//  end;

//  e:=FMap.Width;
//  e:=FMap.Height;  
//
//
//  FMap.PaperUnit:= miUnitPica;
//  FMap.MapUnit  := miUnitPica;
//
//  e:= FMap.MapPaperWidth;  
//  e:=FMap.Zoom;
//
////  FMap.In
//
//  FMap.PaperUnit:= miUnitPoint;
//  FMap.MapUnit  := miUnitPoint;
//
//  e:= FMap.MapPaperWidth;  
//  e:=FMap.Zoom;
//  
//
//  Round(FMap.Zoom * 100000 / FMap.MapPaperWidth);
  

//
//  type
//  MapUnitConstants = TOleEnum;
//const
//  miUnitMile = $00000000;
//  miUnitKilometer = $00000001;
//  miUnitInch = $00000002;
//  miUnitFoot = $00000003;
//  miUnitYard = $00000004;
//  miUnitMillimeter = $00000005;
//  miUnitCentimeter = $00000006;
//  miUnitMeter = $00000007;
//  miUnitSurveyFoot = $00000008;
//  miUnitNauticalMile = $00000009;
//  miUnitTwip = $0000000A;
//  miUnitPoint = $0000000B;
//  miUnitPica = $0000000C;
//  miUnitDegree = $0000000D;
//  miUnitLink = $0000001E;
//  miUnitChain = $0000001F;
//  miUnitRod = $00000020;
 

end;



//------------------------------------------------------------------------------
procedure TMapEngine_X.ClearLegend;
//------------------------------------------------------------------------------
var
  vLayer: CMapxLayer;
  i: Integer;

const
  DEF_LAYER_LEGEND = 'LEGEND';  
    
begin

  vLayer:= GetLayerByName(DEF_LAYER_LEGEND);
  if not Assigned(vLayer) then 
    exit;

  if vLayer.DataSets.Count>0 then
     for i:=1 to vLayer.DataSets.Count do
        vLayer.DataSets[i].Themes.RemoveAll();

        
  RemoveLayer(DEF_LAYER_LEGEND);
  FMapX.Refresh;
end;


// ---------------------------------------------------------------
function TMapEngine_X.Add_Tile(aFileName: string; aLat, aLon: double; ablRect:
    TblRect): CMapXLayer;
// ---------------------------------------------------------------
//var
//  iHeight: Integer;
//  iWidth: Integer;
//  oIni: TIniFile;
//
// // blRect: TBLRect;
//  e: Double;
//  e1: Double;
//  e2: Double;
//  eLat: Double;
////  eLen_km: Double;
// // eLen_map: double;
//  eLen_raster: Double;
//  eLon: Double;
// // eScale: Double;
//  iMapWidth: Integer;
//  iWidth_m: Integer;
//  k: Integer;
//  k1: Integer;
//  k3: Integer;
//
//  XYBounds_view,
//  XYBounds: TXYBounds_MinMax;

//  r: TBoundsX;

  
// r: TXYBounds_MinMax;

  
begin
//  e1:=FMap.CenterX;
//  e2:=FMap.CenterY;  
              

  TilesClear;
  

  result:=Add_Map (aFileName);
  

  // ---------------------------------------------------------------   
//  FMap.ZoomTo (FMap.Zoom, eLat, eLon);
 ///////// FMap.ZoomTo (FMap.Zoom, aLat, aLon);  
   
 

  SetBounds(ablRect);
  
//  FMap.CenterX:=0;
  
//  FMap.ZoomTo (FMap.Zoom, FMap.CenterX, FMap.CenterY);
  
end;
            

//------------------------------------------------------------------------------
function TMapEngine_X.ShowLegend(aTitle: string; aMapFileName: string): boolean;
//------------------------------------------------------------------------------
type
  //-------------------------------
  TLegendItemRec = record
  //-------------------------------
  //   id,
   //  SchemaID      : integer;
  //   SchemaName    : string;
     Min_value     : double;
     Max_value     : double;
     Color         : integer;
     Min_color     : integer;
     Max_color     : integer;
  //   Transparent   : boolean;
     Type_         : integer;  // 0-normal; 1-gradient
  //   Enabled       : boolean;
     Comment       : string;
   end;


  //-------------------------------
 // TdmColorRangeParamRecArr = array of TdmColorRangeParamRec;
  //-------------------------------
  
  
  TLegendRec = record
    Items : array of TLegendItemRec;
                 
  end;    
  

          //-------------------------------------------------
          function DoLoadColorSchema(aFileName: string): TLegendRec;
          //-------------------------------------------------
          var
            j, iCount: Integer;
            oXMLDoc: TXMLDoc;
            vItem,vNode, vNode1, vNode2: IXMLNode;
            I: Integer;

            r: TLegendItemRec;
          begin
            Assert(FileExists(aFileName));

          
            oXMLDoc:=TXMLDoc.Create;
            oXMLDoc.LoadFromFile(aFileName);

            vItem:=xml_FindNode(oXMLDoc.DocumentElement, 'item');
            
            
            if Assigned(vItem) then 
            begin      
//              aLegendCaption
            
              SetLength(Result.Items, vItem.ChildNodes.Count);
                  
              for I := 0 to vItem.ChildNodes.Count-1 do
              begin
                vNode:= vItem.ChildNodes[i];
           
                r.Color       := xml_GetIntAttr(vNode, ATT_COLOR);
                r.Min_value   := xml_GetIntAttr(vNode, ATT_MIN_VALUE);
                r.Max_value   := xml_GetIntAttr(vNode, ATT_MAX_VALUE);
                r.Min_color   := xml_GetIntAttr(vNode, ATT_MIN_COLOR);
                r.Max_color   := xml_GetIntAttr(vNode, ATT_MAX_COLOR);
                r.Type_       := xml_GetIntAttr(vNode, ATT_TYPE);
              //  Enabled     := AsBoolean(xml_GetAttr (vNode2, ATT_ENABLED));
             //   Transparent := AsBoolean(xml_GetAttr (vNode2, ATT_IS_TRANSPARENT));
                r.Comment     := xml_GetStrAttr (vNode, ATT_COMMENT);
                       
                Result.Items[i]:=r;  
                
              end;
              
            end;              


            FreeAndNil(oXMLDoc);
          end;   



          //-------------------------------------------------
          procedure DoAssignThemeProperties(aRangeCategories: CMapXRangeCategoriesCollection;
                    aLegend: TLegendRec);
          //-------------------------------------------------
          var i: Integer;
          begin
            for i:=0 to High(aLegend.Items) do
            begin
              aRangeCategories[i+1].Style.RegionBorderWidth:= 3;

              with aRangeCategories[i+1].Style do
                if aLegend.Items[i].Type_=1 then
                begin
                  RegionBorderColor := IIF(aLegend.Items[i].Min_Color<0, 0, aLegend.Items[i].Min_Color);
                  RegionColor       := IIF(aLegend.Items[i].Max_Color<0, 0, aLegend.Items[i].Max_Color);
                end else begin
                  RegionColor       := IIF(aLegend.Items[i].Color<0, 0, aLegend.Items[i].Color);
                  RegionBorderColor := IIF(aLegend.Items[i].Color<0, 0, aLegend.Items[i].Color);
                end;

              aRangeCategories[i+1].Min:= aLegend.Items[i].Min_value;
              aRangeCategories[i+1].Max:= aLegend.Items[i].Max_value;
            end;
          end;


//          //-------------------------------------------------
//          function DoExistStr(const aStr: string; aSubStrings: array of string): boolean;
//          //-------------------------------------------------
//          var i: integer;
//          begin
//            Result:=false;
//            for i:=0 to High(aSubStrings) do
//              if pos(aSubStrings[i], aStr) > 0 then
//              begin
//                Result:=true;
//                exit;
//              end;
//          end;


var
  i: Integer;
  s, sFileName: string;
  ds: CMapxDataset;
  th: CMapxTheme;

  vLayer: CMapxLayer;
  vFeature: CMapXFeature;
  vPoints: CMapXPoints;

  rLegendRec: TLegendRec;
  sLegendXmlFileName: string;

const
  DEF_LAYER_LEGEND = 'LEGEND';  
  
begin
  Result:=false;

  ClearLegend();

//  if MapFileIsPart(aMapFileName) then
 //    aMapFileName:= PartFileInitFileName(aMapFileName);

  sLegendXmlFileName:= ChangeFileExt(aMapFileName,'.colors.xml');
//  aMapFileName + '.xml';

  if not FileExists(sLegendXmlFileName) then
  begin
    ShowMessage('����������� ������� ��� ��������� ����� : '+ sLegendXmlFileName);
    exit;
  end;

  rLegendRec:= DoLoadColorSchema (sLegendXmlFileName);
  vLayer:= CreateTempLayerWithFields (DEF_LAYER_LEGEND, [mapx_Field('ID', miTypeInteger) ]);

  //---------------------------------------------------
  // ������� ���� ������������ ������, �����
  // ���� �� ������� ��������� ���������������� (�� ��������� ��������)
  //---------------------------------------------------

  vFeature:= FMapX.FeatureFactory.CreateRegion(EmptyParam, EmptyParam);
  vFeature.Style:= FMapX.DefaultStyle;

  vPoints:= CoPoints.Create;
  vPoints.AddXY (0, 0, EmptyParam);
  vPoints.AddXY (1, 0, EmptyParam);
  vPoints.AddXY (1, 1, EmptyParam);
  vPoints.AddXY (0, 1, EmptyParam);

  vFeature.Parts.Add (vPoints);
  vLayer.AddFeature  (vFeature, EmptyParam);

  //---------------------------------------------------

  ds:= vLayer.DataSets[1];
  th:= ds.Themes.Add(mithemeRanged, EmptyParam, EmptyParam, false);

  th.AutoRecompute:= false;
  th.ComputeTheme:= false;

  th.ThemeProperties.DistMethod := miCustomRanges;
  th.ThemeProperties.NumRanges  := Length(rLegendRec.Items);

  DoAssignThemeProperties (th.ThemeProperties.RangeCategories, rLegendRec);

  //---------------------------------------------------

  th.Legend.LegendTexts.AutoGenerate:= true;
  th.Legend.Compact         := false;
  th.Legend.Title           := '�������';
  th.Legend.Subtitle        := '����� LOS';
  th.Legend.ShowEmptyRanges := true;
  th.Legend.ShowCount       := false;
  th.Legend.Top             := 10;
  th.Legend.Left            := 10;

  for i:=0 to High(rLegendRec.Items) do
    with rLegendRec.Items[i] do
    begin
      if Min_value <> Max_value then
      begin
        if Comment='' then s:= Format('%g .. %g',   [Min_value, Max_value])
                      else s:= Format('%g .. %g%s', [Min_value, Max_value, '  ('+Comment+')' ]);
      end else
        if Comment='' then s:= Format('%g',   [Min_Value])
                      else s:= Format('%g%s', [Min_Value, '  ('+Comment+')' ]);

      th.Legend.LegendTexts[i+1].Text:= s;
    end;

  th.Legend.visible:=true;
  FMapX.Refresh;

  //---------------------------------------------------

 // SetLength(arrColorSchema,0);
  Result:=true;
end;






//------------------------------------------------------------------------------
procedure TMapEngine_X.RemoveLayer(aName: string);
//------------------------------------------------------------------------------
var iIndex: Integer;
begin
  iIndex:= GetLayerIndexByName_from_1(aName);
  if iIndex<>-1 then 
    FMapX.Layers.Remove(iIndex);
end;





//--------------------------------------------------------------------
function TMapEngine_X.GetLayerIndexByName_from_1(aName: string): integer;
//--------------------------------------------------------------------
//begin
//  result :=mapx_GetLayerIndexByName_from_1 (FMap, aName);

var i: integer;
  s: string;
  sName: string;
  vLayer: CMapXLayer;
begin
//  Assert(Assigned(aMap), 'Value not assigned');
//
//
//  Result:=-1;
//
//  for i:=1 to FMapX.Layers.Count do
//  begin     
//    vLayer:=FMapX.Layers.Item[i];
//                    
//    if Eq(vLayer.Name, aName) then
//    begin
//      Result:=i;
//      Exit;
//    end;
//  end;
end;
  


procedure TMapEngine_X.Init_CMapX(aMapX: CMapX);
begin
  FMapX:=aMapX;

  Init;
end;





//--------------------------------------------------------------------
procedure TMapEngine_X.TempLayer_Clear(aName: string);
//--------------------------------------------------------------------
begin
 // mapx_DeleteLayerFeatures(FMap, aName);
end;



//--------------------------------------------------------------------
function TMapEngine_X.TempLayer_Create(aLayerName: string): CMapxLayer;
//--------------------------------------------------------------------
var
  s: string;
//  s: string;
  vLayer: CMapXLayer;
  
begin
  Result:=nil;

  if GetLayerIndexByName_from_1(aLayerName) < 0 then
  begin
    vLayer:=FMapX.Layers.CreateLayer (aLayerName, EmptyParam, 1, EmptyParam, EmptyParam);

    s:=vLayer.Filespec;

    vLayer.Selectable := True;
    vLayer.LabelProperties.Visible := True;

    FMapX.Layers.AnimationLayer := vLayer;

  //  FTempLayerNameList.Add(vLayer.Name);

    Result:=vLayer;

  end;
end;



// ---------------------------------------------------------------
procedure TMapEngine_X.TilesClear;
// ---------------------------------------------------------------
//var
//  I: Integer;
//  s: string;
begin
  mapx_TilesClear (FMapX);

  
//  for i:=FMap.Layers.Count downto 1 do
//  begin     
//    s:=FMap.Layers.Item[i].FileSpec;    
//
//    s:= LowerCase (ExtractFileName(s));
//    
//    if LeftStr(s,4) = 'tile' then
//      FMap.Layers.Remove(i);
//                 
//  end;



//  for I := MapList.Count-1 downto 0 do
//    if MapList[i].IsTile then
//      Del_Map(MapList[i].FileName);

end;

        {
// ---------------------------------------------------------------
procedure TMapEngine_X.ExtractTileXYZ(aFileName: string; var aX, aY, aZ: integer);
// ---------------------------------------------------------------
var
  i: Integer;
  i1: Integer;
  i2: Integer;
  s: string;
  sName: string;
begin
  sName:= LowerCase (ExtractFileName(aFileName));

  
 // if LeftStr() then
  i1:=pos('z', sName);
  i2:=pos('_', sName, i1);
  s:=Copy(sName, i1+1, i2-i1-1);
  aZ:=StrToInt(s);
  
  i1:=pos('x', sName);
  i2:=pos('_', sName, i1);
  s:=Copy(sName, i1+1, i2-i1-1);
  aX:=StrToInt(s);

  i1:=pos('y', sName);
  i2:=pos('.', sName, i1);
  s:=Copy(sName, i1+1, i2-i1-1);
  aY:=StrToInt(s);
  
end;      
         }

         




end.




{

// ---------------------------------------------------------------
function TMapEngine_X.GetTileBounds(var aRec: TTileBounds): Boolean;
// ---------------------------------------------------------------
  

    procedure DoGetTile_XY_blocks(aBLPoint: TBLPoint; aZ: integer; var aX,aY:
        integer);
    var
      iMax: Integer;
      x, y, s: Double;
    begin
      x:=0.5 + aBLPoint.L/360.0;
      s:=sin(DegToRad(aBLPoint.B));
    
      y:=0.5-(1/(4*Pi))*ln((1+s)/(1-s));
    
      iMax:=  Trunc(Math.Power(2, aZ));

      aX:=Floor(iMax*x);
      aY:=Floor(iMax*y);    
    
    end;

  


const
  MAX_ZOOM = 559082264;
//       140 733 037
var
  eLat_step: Double;
  eLon_step: Double;
  eScale: double;
  iMax: Integer;
  iZ: Integer;

  blRect: TBLRect;
  e: Double;
 
  eMax_scale: Double;
  iX: Integer;
  iX1: Integer;
  iX2: Integer;
  iX_min: Integer;
  iY1: Integer;
  iY2: Integer;

var
  k: Integer;
  v: Variant;

  
begin
  ////////////////
 
//  k:=FMap.NumericCoordSys.type_;  
//  k:=FMap.NumericCoordSys.Datum.Ellipsoid;
//  k:=FMap.NumericCoordSys.Units;

  v:=FMap.DisplayCoordSys.type_;  
  v:=FMap.DisplayCoordSys.Datum.Ellipsoid;
  v:=FMap.DisplayCoordSys.Units;

  ////////////////



  FillChar(Result,SizeOf(Result),0);

  blRect:=GetBounds();

  
  eScale:=GetScale();

  for iZ := 0 to 28 do     
  begin
//    e:=+Math.Power(2,iZ+2);
  
//    eMax_scale:=MAX_ZOOM / Math.Power(2,iZ);
//    eMax_scale:=MAX_ZOOM / Math.Power(2,iZ+1);    
    eMax_scale:=MAX_ZOOM / Math.Power(2,iZ+2);    
  
    if eScale > eMax_scale then
    begin
      aRec.Z:=iZ;
      
      aRec.Scale:= Trunc(eMax_scale);
      
      iMax:= Trunc(Math.Power(2,iZ));
  
      eLat_step:=(2*85)/iMax;
      eLon_step:=(2*180)/iMax; 
  

      DoGetTile_XY_blocks(blRect.TopLeft,     iZ, iX1,iY1);
      DoGetTile_XY_blocks(blRect.BottomRight, iZ, iX2,iY2);

      aRec.Block_X_min:=Min(iX1,iX2);
      aRec.Block_X_max:=Max(iX1,iX2);      

      aRec.Block_Y_min:=Min(iY1,iY2);
      aRec.Block_Y_max:=Max(iY1,iY2);      
      
//        
//      aRec.Y_min:= Trunc ((85 - blRect.TopLeft.B) / eLat_step);
//      aRec.Y_max:= Trunc ((85 - blRect.BottomRight.B) / eLat_step);
//        
//  
//      aRec.X_min:= Trunc ((blRect.TopLeft.L     + 180) / eLon_step);
//      aRec.X_max:= Trunc ((blRect.BottomRight.L + 180) / eLon_step);
//
//      if aRec.Y_min<0 then aRec.Y_min:=0;

      
//      if Result._max<0 then Result.Y_min:=0;
  
      Result:=true;
      
      Exit;
    end;
    
  end;    

  Result:=False;
  
end;




// ---------------------------------------------------------------
procedure TMapEngine_X.Tile_Del_outside_Rect(aRect: TTileBounds);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
  sFileName: string;
  X, Y, Z: integer;

  blRect: TBLRect;
   
begin
  Assert(aRect.block_X_min>=0);
  Assert(aRect.block_Y_min>=0);  
  Assert(aRect.block_X_max>=0);
  Assert(aRect.block_Y_max>=0);  

  
 // aList.Clear;
  blRect:=mapx_XRectangleToBLRect (FMap.Bounds);
     

  for i:=FMap.Layers.Count downto 1 do
  begin     
    sFileName:=FMap.Layers.Item[i].FileSpec;    

    blRect:=mapx_XRectangleToBLRect (FMap.Layers.Item[i].Bounds);

    
    s:= LowerCase (ExtractFileName(sFileName));
    
    if LeftStr(s,4) = 'tile' then
    begin
      ExtractTileXYZ(sFileName, X, Y, Z);
          
      if 
        (Z <> aRect.Z ) or

        (X < aRect.block_X_min ) or
        (X > aRect.block_X_max ) or
        
        (Y < aRect.block_Y_min ) or
        (Y > aRect.block_Y_max ) 
      then begin
  //       if iInd>0 then
        FMap.Layers.Remove(i);
      
     //  aList.Add(sFileName);      
      end;
    end;
//      FMap.Layers.Remove(i);
                 
  end;


  //Result:=aList.Count;
       
end;






}





//--------------------------------------------------------------------
procedure TMapEngine_X.SetBoundsTest;
//--------------------------------------------------------------------
const
// epsg 3857
  DEF_MAX_20037508 =  2 * 20037508.34279000  / 1000;

var
  blRect: TBLRect;

  oMapFile: TmiMap;
  sFileName: string;
    
begin
{
  oMapFile:= TmiMap.Create;
                                            

  //  for I := 0 to High(FConfigRec.MapLayers) do
     // FMapLayers[i].
    sFileName:='d\1111.tab';
      
  
    oMapFile.CreateFile1 (sFileName,
                   [
                    mapX_Field ('ID',   miTypeInt)
                 //   mapX_Field (FLD_CODE,   miTypeInt)                    
                 //   mapX_Field (FLD_NAME, miTypeString)
                   ]);             

 }
   
                     
//  FMap
 // FMap.MapUnit := miUnitKilometer;  //13


  blRect.TopLeft.B:=DEF_MAX_20037508;
  blRect.TopLeft.L:=-DEF_MAX_20037508;  

  blRect.BottomRight.B:=-DEF_MAX_20037508;
  blRect.BottomRight.L:=DEF_MAX_20037508;  

  
//  Log(geo_FormatBLRect(aBLRect));

  SetBounds (BLRect);

// with aBLRect do
//    aMap.Bounds.Set_(TopLeft.L, TopLeft.B,
//                BottomRight.L, BottomRight.B);

  
end;



//  GetKeyState(VK_LBUTTON) 
//  
//  var
//  pt : tPoint;
//begin
//  pt := Mouse.CursorPos; 
  
//  Mouse.




