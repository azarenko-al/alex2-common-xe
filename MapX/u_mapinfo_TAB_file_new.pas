unit u_mapinfo_TAB_file_new;

interface

uses classes, SysUtils, StrUtils, Forms,
  XMLDoc, XMLIntf,
   u_xml_document ,

  u_Geo,
  u_geo_convert_new,

  u_func_arr,
  u_func
;


type
  TMapinfoRasterTabFile = class(TObject)
  private
    procedure ExtractCoordSys(aCoordSys: string);
  public
    RasterFileName: string;

    GK_Zone: Integer;
  //  Meridian : Integer;

    Units : (utMeter,utDegree);

    Points: array[0..10] of record
                              //x-lat, y-lon
                              img_x, img_y : Integer;

                              x,y : double;
                              B,L: Double;
                            end;
    PointCount : Integer;


    CoordSys: record
   //   Projection     : TProjectionType;
   //   CoordSys       : TCoordSysType;

      CentralMeridianDegree: Integer;
   //   UTM_ZoneNum    : Integer;
      GK_ZoneNum     : Integer;

      IsSouth        : Boolean;

      East_offset    : Integer;
      North_offset   : Integer;
    end;

  public
    procedure LoadFromFile(aFileName: string);

    procedure SaveToKmlFile(aFileName: string);
    procedure SaveToFile(aFileName: string);

  end;


implementation



// ---------------------------------------------------------------
procedure TMapinfoRasterTabFile.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
var
  I: Integer;
  oSList: TStringList;
  s: string;
  arr,arr1,arr2: TStrArray;
  eMeridian: Integer;
  j: Integer;
  s1: string;
begin
  oSList:=TStringList.Create;

  FillChar(Points,SizeOf(Points),0);
  PointCount :=0;



  oSList.LoadFromFile(aFileName);



  for I := 0 to oSList.Count - 1 do
  begin
//    s := Lowercase(Trim(oSList[i]));
    s := Trim(oSList[i]);

  //  if LeftStr(s,8)='Definition Table' then

    if Eq(s,'Definition Table') then
      for j := i+1 to oSList.Count - 1 do
      begin
      //  s := Lowercase(Trim(oSList[j]));
        s := Trim(oSList[j]);

        if Copy(s,1,4)='File' then
        begin
          s1:=Copy(s,5,100);
          RasterFileName := Trim(ReplaceStr(s1,'"',''));
        end else

        if LeftStr(s,1)='(' then
        begin
//          (9487770.0000000000,5957889.0000000000) (0,0) Label "Pt 1",

          s:=ReplaceStr(s,'(','');
          s:=ReplaceStr(s,')','');

          arr:=StringToStrArray(s, ' ');

          arr1:=StringToStrArray(arr[0], ',');
          arr2:=StringToStrArray(arr[1], ',');


          Points[PointCount].y :=  AsFloat(arr1[0]);
          Points[PointCount].x :=  AsFloat(arr1[1]);

          Points[PointCount].img_y :=  AsInteger(arr2[0]);
          Points[PointCount].img_x :=  AsInteger(arr2[1]);

          Inc(PointCount);

        end else

        if Copy(s,1,8)='CoordSys' then
          ExtractCoordSys(s);

      end;
  end;
//
//
//  arr:=StringToStrArray(oSList[1], ' ');
//
//  s:=replaceStr(arr[3],'.','');
//
//  eMeridian := AsInteger(s);
//  GK_Zone     := (eMeridian+3) div 6;
//
//
//  for I := 0 to 3 do
//  begin
//    s:=oSList[i+2];
//
//    arr:=StringToStrArray(s, ' ');
//
//    Points[i].img_x :=  Round(AsFloat(arr[0]));
//    Points[i].img_y :=  Round(AsFloat(arr[1]));
//
//    Points[i].x :=  Trunc(AsFloat(arr[2]));
//    Points[i].y :=  Trunc(AsFloat(arr[3]));
//
//  end;
  FreeAndNil(oSList);


 // BitmapFileName := ChangeFileExt (ExtractFileName(aFileName), '.bmp');

end;


//-------------------------------------------------------------------
procedure TMapinfoRasterTabFile.ExtractCoordSys(aCoordSys: string);
//-------------------------------------------------------------------
//const
//  DEF='CoordSys Earth Projection 8, 104, "m", ';//51, 0, 1,  9500000, 0';
 // DEF: string='CoordSys Earth Projection 8, 104, "m",';//51, 0, 1,  9500000, 0';

var
  i: Integer;
  oStrList: TStringList;
begin

  oStrList:=StringToStringList(aCoordSys, ',');

  i:=oStrList.Count;

 // Result:=False;

  FillChar(CoordSys,SizeOf(CoordSys),0);


  if oStrList.Count=8 then
    with CoordSys do
  begin
    CentralMeridianDegree := StrToInt(oStrList[3]);

    East_offset  :=StrToInt(oStrList[6]);
    North_offset :=StrToInt(oStrList[7]);
    IsSouth      :=North_offset > 0;

    GK_ZoneNum   :=CentralMeridianDegree div 6 + 1;  //51=39 ����
  //  UTM_ZoneNum   :=CentralMeridianDegree div 6 + 1+ 30;  //51=39 ����

//    if aRec.MeridianDegree>0 then
    GK_ZoneNum := CentralMeridianDegree div 6;
    if GK_ZoneNum<0 then
      GK_ZoneNum:=GK_ZoneNum + 60;

  //  Result:=True;

  end;

  FreeAndNil(oStrList);
end;

// ---------------------------------------------------------------
procedure TMapinfoRasterTabFile.SaveToKmlFile(aFileName: string);
// ---------------------------------------------------------------

//const
(*<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2"
 xmlns:gx="http://www.google.com/kml/ext/2.2">
<GroundOverlay>
  <name>gx:LatLonQuad Example</name>
  <Icon>
    <href>http://developers.google.com/kml/documentation/images/rectangle.gif</href>
    <viewBoundScale>0.75</viewBoundScale>
  </Icon>
  <gx:LatLonQuad>
    <coordinates>
      81.601884,44.160723 83.529902,43.665148 82.947737,44.248831 81.509322,44.321015
    </coordinates>
  </gx:LatLonQuad>
</GroundOverlay>
</kml>*)


var
  sZone,sCoordinates: string;
  s,sText : string;
  i,iZone: integer;
  d: integer;
  d1: integer;
  oSList: TStringList;
  sDir: string;
  sGif: string;

  oDoc: TXmlDocumentEx;
  vNode,vNode1 : IXMLNode;

  XY: TXYPoint;
  BL: TBLPoint;
  k: Integer;

begin
 // UpdateRaster(aFileName);


  sCoordinates := '';

  for i:=0 to PointCount-1 do //High(Points)
  begin
    k:=(3-i) mod 4;

    XY.X := Points[k].x;
    XY.Y := Points[k].y;


    bl:= geo_GK_XY_to_WGS84_BL(XY, GK_Zone);

    s:=Format('%1.6f,%1.6f ', [bl.l, bl.b]);

    sCoordinates:=sCoordinates + s
  end;


  Assert(RasterFileName<>'', 'Value <=0');



  // -------------------------
  oDoc:=TXmlDocumentEx.Create;
  oDoc.InitKML;


  vNode:=oDoc.DocumentElement.AddChild('GroundOverlay');
  with vNode.AddChild('Icon') do
  begin
    ChildValues['href'] := RasterFileName;
    ChildValues['viewBoundScale'] := 0.75;
  end;

  vNode.AddChild('gx:LatLonQuad').AddChild('coordinates','').Text := sCoordinates;


  oDoc.SaveToFile(ChangeFileExt (aFileName, '.kml'));

  FreeAndNil(oDoc);


end;

//------------------------------------------------------
procedure TMapinfoRasterTabFile.SaveToFile(aFileName: string);
//------------------------------------------------------
// ������������ ��������� ��� ����� Mapinfo
// - Bounds - ���������� ������� ������� ��������
//------------------------------------------------------
const
  DEF_WOR=
    '!Workspace'    + CRLF +
    '!Version 650'  + CRLF +
    '!Charset WindowsCyrillic'+ CRLF +
    'Open Table ":FILENAME" As _1 Interactive'+ CRLF +
    'Map From _1'   + CRLF +
    'Set Map'       + CRLF +
    '  :CoordSys'+ CRLF + // Earth Projection 8, 1001, "m", 51, 0, 1, 9500000, 0
    'Set Map'       + CRLF +
    '  Layer 1'     + CRLF +
    '    Display Graphic'+ CRLF +
    '    Zoom (0.11, 42) Units "km" Off';

const
  DEF_CoordSys_DEGREE =
   'CoordSys Earth Projection 8, 1001, "m", :Zone ';

  DEF_CoordSys_M =
   'CoordSys Earth Projection 1, 1001';


const
  MIF_RASTER =
    '!table'            + CRLF +
    '!version 300'      + CRLF +
    '!charset WindowsCyrillic' + CRLF +
    ''                         + CRLF +
    'Definition Table'  + CRLF +
    '  File ":FileName"'+ CRLF +
    '  Type "RASTER"'   + CRLF +
    ':Points'           + // CRLF +
    '  :CoordSys'         + CRLF + // Earth Projection 8, 1001, "m", :Zone ' + CRLF + //39, 0, 1, 7500000, 0 ' + CRLF +
    '  Units ":Units" ';
  //  '  Units "m" ';


    //+ CRLF +
//    'RasterStyle 4 1 '+ CRLF + //transparent setup
 //   'RasterStyle 7 0 '; //16777215'; //bmp: 16776959'; //// JPG

//    'RasterStyle 7 16777215 ';


  function DoMakeCoordSys: string;
  var
    d: integer;
    d1: integer;
    sZone: string;
  begin

    // -------------------------
    d:=GK_Zone*6-3;
    d1:=GK_Zone*1000000 + 500000;
    sZone:= Format('%d, 0, 1, %d, 0', [d, d1]);
    // -------------------------


    if Units=utMeter then
      Result := ReplaceStr (DEF_CoordSys_DEGREE, ':Zone', sZone)
    else
      Result := DEF_CoordSys_M;
  end;


var
  sZone,sXYPoints: string;
  s,sText : string;
  i,iZone: integer;
  d: integer;
  d1: integer;
  oSList: TStringList;
  sUnits: string;
  x: Double;
  y: Double;

 // sDir: string;
 // sGif: string;

begin
 // UpdateRaster(afileName);

  Assert(RasterFileName<>'');


  sXYPoints := '';

  for i:=0 to PointCount-1 do
  //  if (Points[i].Y<>0) and (Points[i].X<>0) then
  begin
    y:=Trunc(Points[i].Y);
    x:=Trunc(Points[i].X);

    if Units=utMeter then
    begin
      y:=Trunc(Points[i].Y);
      x:=Trunc(Points[i].X);
    end else begin
      y:=Points[i].B;
      x:=Points[i].L;
    end;

    s:=Format('  (%1.10f,%1.10f) (%d,%d) Label "%d"',
      [Y, X, Points[i].img_x, Points[i].img_y, i+1]);

    if i<PointCount-1 then
      s:=s+',';

    sXYPoints:=sXYPoints + s +CRLF;
  end;



//  if aRec.Transparent then
//    sContent:=sContent + CRLF + 'RasterStyle 8 204 ';

(*  // -------------------------
  d:=GK_Zone*6-3;
  d1:=GK_Zone*1000000 + 500000;
  sZone:= Format('%d, 0, 1, %d, 0', [d, d1]);
  // -------------------------
*)

 // sText:=ReplaceStr (sText, ':Zone',   sZone);

  // -------------------------
  oSList:=TStringList.Create;


  sText:=MIF_RASTER;
  sText:=ReplaceStr (sText, ':FileName', ExtractFileName(RasterFileName));
  sText:=ReplaceStr (sText, ':Points',   sXYPoints);
  sText:=ReplaceStr (sText, ':CoordSys', DoMakeCoordSys());

  if Units=utMeter then sUnits :='m'
                   else sUnits :='degree';

  sText:=ReplaceStr (sText, ':Units', sUnits);


  oSList.Text := sText;

  aFileName := ChangeFileExt (aFileName, '.tab');
  oSList.SaveToFile(aFileName);


  sText:=DEF_WOR;
  sText:=ReplaceStr (sText, ':FileName', ExtractFileName(aFileName));
  sText:=ReplaceStr (sText, ':CoordSys', DoMakeCoordSys());

  oSList.Text := sText;
  oSList.SaveToFile(ChangeFileExt (aFileName, '.wor'));



  FreeAndNil(oSList);

end;




var

  o: TMapinfoRasterTabFile;

begin
//  Application.Initialize;
//
//  o:=TMapinfoRasterTabFile.Create;
//  o.LoadFromFile('S:\_1\PS_������3.tab');
//  o.SaveToKmlFile('S:\_1\PS_������3_.kml');

end.
//                 PS_������3.tab
//
//!table
//!version 300
//!charset WindowsCyrillic
//
//Definition Table
//  File "2.gif"
//  Type "RASTER"
//  (9487770.0000000000,5957889.0000000000) (0,0) Label "Pt 1",
//  (9507563.0000000000,5957821.0000000000) (4676,0) Label "Pt 2",
//  (9507464.0000000000,5928969.0000000000) (4676,6816) Label "Pt 3",
//  (9487671.0000000000,5929037.0000000000) (0,6816) Label "Pt 4"
//
//CoordSys Earth Projection 8, 1001, "m", 51, 0, 1, 9500000, 0
//Units "m"

