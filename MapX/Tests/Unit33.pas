﻿unit Unit33;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.OleCtrls,

  OSM_TLB,

  x_OSM,
 
  u_geo,

  
  System.Generics.Collections,  


  x_MapView,
  
  MapInfo_MapView_TLB,

  WMS_TLB,

//  x_Tile_Manager,


  u_mapX,
  
 // MapXLib_TLB, 
                    

  CodeSiteLogging,

  Vcl.StdCtrls, Vcl.Mask, RxToolEdit, Vcl.ExtCtrls, MapXLib_TLB

//  x_MapView

  ;
type
  TFeatureItem = class
  private
  public
    FeatureID: integer;
    Name : string;
    
    x, y: double;

    constructor CreateItem(aName : string; aFeatureID: Integer; aX, aY: double);

    function Init(aName : string; aFeatureID: Integer; aX, aY: double):
        TFeatureItem;

  end;


type
  TForm33 = class(TForm)
    Map1: TMap;
    Button1: TButton;
    FilenameEdit1: TFilenameEdit;
    Button2: TButton;
    Button3: TButton;
    ListBox2: TListBox;
    Button4: TButton;
    Timer1: TTimer;
    Button5: TButton;
    FilenameEdit2: TFilenameEdit;
    FilenameEdit3: TFilenameEdit;
    Button6: TButton;
    ComboBox1: TComboBox;
    Button7: TButton;
    Button8: TButton;
    ed_Z: TEdit;
    Button9: TButton;
    Button10: TButton;
    Button11: TButton;
    Button12: TButton;
    Button13: TButton;
    ComboBox_WMS: TComboBox;
    Button14: TButton;
    Button15: TButton;
    Button16: TButton;
    Button17: TButton;
    Button18: TButton;
    Button19: TButton;
    Button20: TButton;
    Button21: TButton;
    Button22: TButton;
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure Button15Click(Sender: TObject);
    procedure Button16Click(Sender: TObject);
    procedure Button17Click(Sender: TObject);
    procedure Button18Click(Sender: TObject);
    procedure Button19Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button20Click(Sender: TObject);
    procedure Button21Click(Sender: TObject);
    procedure Button22Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Map1MapViewChanged(Sender: TObject);
//    procedure Map1AddFeatureToolUsed(ASender: TObject; ToolNum: SmallInt; Flags:
 //       Integer; const Feature: IDispatch; bShift, bCtrl: WordBool; var
  //      EnableDefault: WordBool);
    //procedure Map1DragDrop(Sender, Source: TObject; X, Y: Integer);
  //  procedure Map1EndDrag(Sender, Target: TObject; X, Y: Integer);
//    procedure Map1LabelChanged(ASender: TObject; ChangeType: SmallInt; const
 //       ChangingLabels: CMapXLabels; var EnableDefault: WordBool);
    procedure Map1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure Map1MouseUp(Sender: TObject; Button: TMouseButton; Shift:
        TShiftState; X, Y: Integer);
//    procedure Map1PolyToolUsed(ASender: TObject; ToolNum: SmallInt; Flags: Integer;
 //       const Points: IDispatch; bShift, bCtrl: WordBool; var EnableDefault:
  //      WordBool);
//    procedure Map1RequestData(ASender: TObject; const DataSetName: WideString; Row:
 //       Integer; Field: SmallInt; const Value: OLEVariant; var Done: WordBool);
//    procedure Map1SelectionChanged(Sender: TObject);
//    procedure Map1StartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure Map1ToolUsed(ASender: TObject; ToolNum: SmallInt; X1, Y1, X2, Y2,
        Distance: Double; Shift, Ctrl: WordBool; var EnableDefault: WordBool);
    procedure Timer1Timer(Sender: TObject);
  private
  //   List: TList<Integer>;

     Moved_dict: TDictionary<Integer,TFeatureItem>;

    FIsFirst : boolean;

  
    ITileManager: ITileManagerX;
  
//    IMapView: IMapViewX;
    FMapView: TMapViewX;

    
    procedure Display_Dict;
    function GetSelectedLayer: CMapxLayer;
    procedure Load_CMap;
    procedure Restore;
    procedure SeveSelected;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form33: TForm33;

implementation

{$R *.dfm}



procedure TForm33.Button10Click(Sender: TObject);
begin
  FMapView.Add (FilenameEdit1.FileName);  
end;

procedure TForm33.Button11Click(Sender: TObject);
begin
//     ITileManager.Exec('aaaa');
end;

procedure TForm33.Button12Click(Sender: TObject);
begin
//  FMapView.Add (FilenameEdit1.FileName);  



                                                   
//  ITileManager.Set_Z 56.303090, 44.025859, 15 );
  ITileManager.Set_Center( 56.303090, 44.025859, 19 );

  
  FMapView.Add ('C:\ONEGA\RPLS_DB\bin\WMS\wms_test.TAB');  

  
//Координаты точки в WGS
//56.303090, 44.025859
end;

procedure TForm33.Button13Click(Sender: TObject);
begin
  FMapView.Add (FilenameEdit2.FileName);  


  
end;

procedure TForm33.Button14Click(Sender: TObject);
begin
  ITileManager.Set_WMS_Layer(ComboBox_WMS.Text);
end;

procedure TForm33.Button15Click(Sender: TObject);
begin
  ITileManager.Set_Center( 43.1132639998, 131.8896810105, 19 );


//Владивосток 43.1132639998 131.8896810105
//Екатеринбург 56.8383880037 60.6015040004
//Мурманск 68.974778003 33.0627090095
//Санкт-Петерьург 59.9469290031 30.2315130066
//Одесса 46.4887770029 30.742067005
//Каир 30.075336003 31.2278930057
//Джуба 4.8779410054 31.6020610099

end;

procedure TForm33.Button16Click(Sender: TObject);
begin
  ITileManager.Set_Center( 68.974778003, 33.0627090095, 19 );

end;

procedure TForm33.Button17Click(Sender: TObject);
begin
//Санкт-Петерьург 59.9469290031 30.2315130066

  ITileManager.Set_Center( 59.9469290031, 30.2315130066, 19 );

end;

procedure TForm33.Button18Click(Sender: TObject);
begin
//Екатеринбург 56.8383880037 60.6015040004
  ITileManager.Set_Center( 56.8383880037, 60.6015040004, 19 );

end;

procedure TForm33.Button19Click(Sender: TObject);
begin
//Каир 30.075336003 31.2278930057
//Джуба 4.8779410054 31.6020610099
  ITileManager.Set_Center( 30.075336003, 31.2278930057, 19 );

end;

procedure TForm33.Button1Click(Sender: TObject);
begin
   FMapView.Add_Dlg;
end;

procedure TForm33.Button20Click(Sender: TObject);
begin
 //Каир 30.075336003 31.2278930057
//Джуба 4.8779410054 31.6020610099
  ITileManager.Set_Center( 4.8779410054, 31.6020610099, 19 );

end;

procedure TForm33.Button21Click(Sender: TObject);
begin
//   ITileManager.Dlg_Set_center;
end;

procedure TForm33.Button22Click(Sender: TObject);
begin
//  ITileManager.Dlg_Log;
end;


procedure TForm33.Button2Click(Sender: TObject);
begin

// FilenameEdit3
 
  FMapView.Add (FilenameEdit1.FileName);  
  FMapView.Add (FilenameEdit2.FileName);
  FMapView.Add (FilenameEdit3.FileName);  

end;

procedure TForm33.Button3Click(Sender: TObject);
begin
  Map1.PropertyPage;

//  Map1.Layers[1].Selection.Count;
  
end;

procedure TForm33.Button4Click(Sender: TObject);
begin
  Restore;
end;

procedure TForm33.Button5Click(Sender: TObject);
var
  v: IOSM_X;
  obj: TOSM_X;
  
begin
  obj:=TOSM_X.Create;
  obj.Load(Map1.DefaultInterface);
  
//  v:=CoOSM_X.Create;
//  v.Load(Map1.DefaultInterface);
  
end;


procedure TForm33.Button6Click(Sender: TObject);
begin
//  FMapView.S

  FMapView.Set_Center(59.95, 30.36 );

//FilenameEdit3

end;

procedure TForm33.Button7Click(Sender: TObject);
begin
//  ITileManager.Show_Log;
end;

procedure TForm33.Button8Click(Sender: TObject);
begin



//
//  Ffrm_MapView.FTileManager.Set_Z(oBItem.Tag);
//  Ffrm_MapView.WMS_Update;
////  FTileManager.Load_CMap();
//  Ffrm_MapView.FTileManager.Set_Z(0);
//
// ///////////////////
////  ShowMessage( IntToStr(Ffrm_MapView.WMS.Z));
//
//
//  DoOnMapViewChanged(nil);


end;

procedure TForm33.Button9Click(Sender: TObject);
begin
   ITileManager.Set_Z( StrToInt( ed_z.Text)  );
   ITileManager.Load_CMap();
   ITileManager.Set_Z (0);

   
//  Ffrm_MapView.WMS_Update;
////  FTileManager.Load_CMap();
//  Ffrm_MapView.FTileManager.Set_Z(0);
//
// ///////////////////
////  ShowMessage( IntToStr(Ffrm_MapView.WMS.Z));
//
//
//  DoOnMapViewChanged(nil);
end;

// ---------------------------------------------------------------
procedure TForm33.Restore;
// ---------------------------------------------------------------
var
  vLayer: CMapxLayer;
  I: Integer;
  iLayer: Integer;
  s: string;

  vFeature: CMapXFeature;
  oFeature: TFeatureItem;
begin
 // ListBox1.Clear;

  vLayer:=GetSelectedLayer();
  


  for oFeature in Moved_dict.Values do
  begin
  
    vFeature:=vLayer.GetFeatureByID(oFeature.FeatureID);
  
    vFeature.Point.Set_(oFeature.x, oFeature.y);
    vFeature.Update(EmptyParam,EmptyParam);
    
  end;

end;


procedure TForm33.FormCreate(Sender: TObject);
var
  I: Integer;
  oSList: TstringList;
  s: Widestring;
  s1: string;
begin
  
  //List := TList<integer>.Create;

 
  Moved_dict:=TDictionary<Integer,TFeatureItem>.Create;

  
 //   List.Duplicates := dupIgnore;


//  ITileManager:=  TTileManagerX.Create;
  ITileManager:=  CoTileManagerX.Create;
  ITileManager.Init(Map1.DefaultInterface);
  
  Assert (ITileManager.GetClassGUID = IID_ITileManagerX);
 
  
//  ITileManager.Set_WMS_layer('YandexSat');   //OpenStreetMap
  ITileManager.Set_Dir('c:\_wms');

//  ITileManager.Set_WMS_layer('GoogleHybr');   //OpenStreetMap  
  ITileManager.Set_WMS_layer('YandexSat');   //OpenStreetMap  

//  ITileManager.Set_CMapx(Map1.DefaultInterface);

  
  ITileManager.Load_CMap();


//  ITileManager.Get_Layer_list(s);
  
//  ComboBox1.Items.Text:=s;

//  
//
//  
//  DEF_WMS : array[0..7] of record
//    Caption: string;
//    Name: string;
//  end =
//  (
//  //  (Caption: 'Google - Ñïóòíèê';  Name: 'GoogleSat'),
//    (Caption: 'Google - Êàðòà';    Name: 'GoogleMap'),
//    (Caption: 'Google - Ëàíäøàôò'; Name: 'GoogleLand'),
//    (Caption: 'Google - Ãèáðèä';   Name: 'GoogleHybr'),
//
//    (Caption: 'Yandex - Ñïóòíèê';  Name: 'YandexSat'),
//    (Caption: 'Yandex - Êàðòà';     Name: 'YandexMap') ,
//    (Caption: 'Yandex - Ãèáðèä';    Name: 'YandexText'),
//
//
//    (Caption: '2GIS';   Name: '2GIS'),
//
//    (Caption: 'OpenStreetMap - Êàðòà';   Name: 'OpenStreetMap')
//
//  );
  

  
  FMapView:=TMapViewX.Create;;
  FMapView.Set_CMap(Map1.DefaultInterface);


  { 
  
  

//  IMapView:=CoMapViewX.Create;;
  
  



 }
  
//  Map1.S
  
//  Map1.PropertyPage;
  
end;


procedure TForm33.FormDestroy(Sender: TObject);
begin
  FreeAndNil(Moved_dict);


  FreeAndNil(FMapView);
  

 // IMapView.Set_CMap(nil);

end;



procedure TForm33.Map1MouseMove(Sender: TObject; Shift: TShiftState; X, Y:
    Integer);
begin
  if Map1.CurrentTool = miSelectTool then
    if ssLeft in Shift then
    begin
      if FIsFirst then
        SeveSelected();

      FIsFirst:=False;
    end;
  
end;


// ---------------------------------------------------------------
function TForm33.GetSelectedLayer: CMapxLayer;
// ---------------------------------------------------------------
var
 // vLayer: CMapxLayer;
  I: Integer;
  iLayer: Integer;
  s: string;

 // vFeature: CMapXFeature;
begin
  Result:=nil;


  for iLayer := 1 to Map1.Layers.Count do
    if Map1.Layers[iLayer].Selectable then
    begin
      Result:= Map1.Layers[iLayer];
      Exit;
    end;
end;


// ---------------------------------------------------------------
procedure TForm33.SeveSelected;
// ---------------------------------------------------------------
var
  vLayer: CMapxLayer;
  I: Integer;
  iLayer: Integer;
  oFeatureItem: TFeatureItem;
  s: string;

  vFeature: CMapXFeature;
begin
  //ListBox1.Clear;

  vLayer:=GetSelectedLayer();
  
  oFeatureItem:=TFeatureItem.Create;

  
 // for iLayer := 1 to Map1.Layers.Count do
  //  if Map1.Layers[iLayer].Selectable then
  //  begin
    //  vLayer:= Map1.Layers[iLayer];
               
//   ListBox1.Items.Add('vLayer.Name   '+ vLayer.Name);
//   ListBox1.Items.Add('---');       


               
   for I := 1 to vLayer.Selection.Count do
   begin
     vFeature:=vLayer.Selection.Item[i];
   
     CodeSite.SendMsg( 'FeatureID - ' + IntToStr(vFeature.FeatureID));

     //ListBox1.Items.Add(vFeature.Name);
                                                                           
     
    // if not List.Contains(vFeature.FeatureID) then
     //  List.Add (vFeature.FeatureID);
     
//     if True then
     

     if not Moved_dict.ContainsKey(vFeature.FeatureID) then
       Moved_dict.Add(vFeature.FeatureID,
          oFeatureItem.Init (vFeature.Name, vFeature.FeatureID, vFeature.Point.X, vFeature.Point.Y));


   //  Dict.co
     
   end;


  FreeAndNil(oFeatureItem);
   

  Display_Dict;
   
  //  end;

   
end;
 



// ---------------------------------------------------------------
procedure TForm33.Display_Dict;
// ---------------------------------------------------------------
var
  I: Integer;
  oItem: TFeatureItem;
begin

  ListBox2.Clear;

//   ListBox2.Items.Add('vLayer.Name   '+ vLayer.Name);
//   ListBox2.Items.Add('---');       

                                       

   for  oItem in Moved_dict.Values do
   begin                  
   
     ListBox2.Items.Add(oItem.Name  + intToStr(oItem.FeatureID));
                              

   //  Dict.co
     
   end;
      
end;



procedure TForm33.Map1MapViewChanged(Sender: TObject);
var
  BLRect: TBLRect;
begin
  Timer1.Enabled:=true;


 // CodeSite.SendMsg('Map1MapViewChanged');


  BLRect:=mapx_XRectangleToBLRect(map1.Bounds);


  CodeSite.SendWarning('Map1MapViewChanged - '+ BLRect.ToString);
  

 // CodeSite.SendMsg( 'Map1ToolUsed ' + IntToStr(ToolNum) );
  


end;

procedure TForm33.Map1MouseUp(Sender: TObject; Button: TMouseButton; Shift:
    TShiftState; X, Y: Integer);
begin
 FIsFirst:=True;
end;



procedure TForm33.Map1ToolUsed(ASender: TObject; ToolNum: SmallInt; X1, Y1, X2,
    Y2, Distance: Double; Shift, Ctrl: WordBool; var EnableDefault: WordBool);
begin
///  ShowMessage('Map1ToolUsed - ' + IntToStr(ToolNum) );

  CodeSite.SendMsg( 'Map1ToolUsed ' + IntToStr(ToolNum) );
  
   
  
end;

procedure TForm33.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=False;


//  if Assigned(ITileManager) then
//    ITileManager.Load_CMap();


  Load_CMap();  
    
end;

// ---------------------------------------------------------------
procedure TForm33.Load_CMap;
// ---------------------------------------------------------------
begin
  Map1.OnMapViewChanged:=nil;
                       
  if Assigned(ITileManager) then  
    ITileManager.Load_CMap();

  Map1.OnMapViewChanged:=Map1MapViewChanged;
    
end;


// ---------------------------------------------------------------
constructor TFeatureItem.CreateItem(aName : string; aFeatureID: Integer; aX,
    aY: double);
// ---------------------------------------------------------------
begin
//  Assert(aName<>'');
  Assert(aFeatureID>0);  

  inherited;

  Name:=aName;
  
  FeatureID:=aFeatureID;
  
  X:=aX;  
  Y:=aY;
      
end;



// ---------------------------------------------------------------
function TFeatureItem.Init(aName : string; aFeatureID: Integer; aX, aY:
    double): TFeatureItem;
// ---------------------------------------------------------------
begin
//  Assert(aName<>'');
  Assert(aFeatureID>0);  

//  inherited;

  Name:=aName;
  
  FeatureID:=aFeatureID;
  
  X:=aX;  
  Y:=aY;
      
  Result:=Self;    
end;




end.


