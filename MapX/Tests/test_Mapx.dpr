program test_Mapx;

uses
  Vcl.Forms,
  Unit33 in 'Unit33.pas' {Form33},
  MapInfo_MapView_TLB in 'W:\ActiveX_RPLS_DB\MapInfo_MapView\MapInfo_MapView_TLB.pas',
  WMS_TLB in 'W:\WMS\ActiveX\WMS_TLB.pas',
  x_MapView in 'W:\ActiveX_RPLS_DB\MapInfo_MapView\x_MapView.pas',
  OSM_TLB in 'W:\GIS active\OSM_TLB.pas',
  x_OSM in 'W:\GIS active\x_OSM.pas',
  u_OSM_classes in 'W:\GIS active\OSM\Master\u_OSM_classes.pas',
  u_xml1 in '..\..\XML\u_xml1.pas',
  dm_Main_OSM in 'W:\GIS active\OSM\Master\dm_Main_OSM.pas' {dmMain_OSM: TDataModule},
  u_Mapinfo_WOR_classes in '..\..\Map\u_Mapinfo_WOR_classes.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm33, Form33);
  Application.Run;
end.
