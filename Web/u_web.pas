unit u_web;

interface

uses
  Windows, SysUtils, Dialogs, ExtActns, WinInet;


//  Winapi.Windows, System.SysUtils,   WinInet;


function DownloadFile(aURL, aDestFile: string): Boolean;
function DownloadFile_HTTPS(aURL, aFileName: string): Boolean;

    
implementation



// ---------------------------------------------------------------
function DownloadFile_HTTPS(aURL, aFileName: string): Boolean;
// ---------------------------------------------------------------
const
  BufferSize = 1024;
var
  hSession, hURL: HInternet;
  Buffer: array[1..BufferSize] of Byte;
  BufferLen: DWORD;
  F: File;
begin
   Result := False;
   hSession := InternetOpen('', INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0) ;

   // Establish the secure connection
   InternetConnect (
     hSession,
     PChar(aURL),
     INTERNET_DEFAULT_HTTPS_PORT,
     nil, //PChar(aUser),
     nil, //PChar(aPass),
     INTERNET_SERVICE_HTTP,
     0,
     0
   );

  try
    hURL := InternetOpenURL(hSession, PChar(aURL), nil, 0, 0, 0) ;
    try
      AssignFile(f, aFileName);
      Rewrite(f,1);
      try
        repeat
          InternetReadFile(hURL, @Buffer, SizeOf(Buffer), BufferLen) ;
          BlockWrite(f, Buffer, BufferLen)
        until BufferLen = 0;
      finally
        CloseFile(f) ;
        Result := True;
      end;
    finally
      InternetCloseHandle(hURL)
    end
  finally
    InternetCloseHandle(hSession)
  end;

  Result:=FileExists(aFileName);

//  Assert( FileExists(aFileName), aFileName);
end;



// ---------------------------------------------------------------
function DownloadFile(aURL, aDestFile: string): Boolean;
// ---------------------------------------------------------------
var
//Vcl.ExtActns
  pdfStreamed: TDownloadUrl;
begin
  pdfStreamed:= TDownLoadURL.Create(nil);

  with pdfStreamed do  begin    URL      := aURL;    FileName := aDestFile;    try      ExecuteTarget(nil);    except      on E: Exception do        ShowMessage(E.Message);    end;  end;  FreeAndNil(pdfStreamed);  Result:=FileExists(aDestFile);end;


end.
