unit u_web_func;

interface
uses
  pngimage,  IdCustomHTTPServer, Graphics, Classes, SysUtils,StdCtrls,  Dialogs,
  IdMultipartFormData, 

  Windows, StrUtils,


  DbugIntf,

 // u_json,

  u_func,
  u_func_arr, ///ays,


 // IdDecoder,

  IdHTTPServer;

type
   TLogEvent = procedure(aMsg: string) of object;


  procedure http_Send_Image_PNG_file(aResponse: TIdHTTPResponseInfo; aPngFileName: string);

//  procedure http_Send_Blank_Tile_Image_PNG_file(aResponse: TIdHTTPResponseInfo);

  procedure http_Log_Request(ARequestInfo: TIdHTTPRequestInfo; aLogEvent: TLogEvent);
  procedure http_Log_HTTPServer(AIdHTTPServer: TIdHTTPServer);

  procedure http_Send_JSON(AResponseInfo: TIdHTTPResponseInfo; aJSON: string);

  procedure http_Send_JSON_Result_ok(AResponseInfo: TIdHTTPResponseInfo);

  function http_ExtractStringFromParams(aValue: string): string;

  function ReplaceStrArr(aValue: string; aFromArr: array of string; aTo: string): string;


procedure http_Send_JSON_Wide(AResponseInfo: TIdHTTPResponseInfo; aJSON:
    WideString);


//var
//  g_Disable_log: boolean;



implementation



//var
//  csCriticalSection: TRTLCriticalSection;
//


// ---------------------------------------------------------------
procedure http_Send_JSON_Result_ok(AResponseInfo: TIdHTTPResponseInfo);
// ---------------------------------------------------------------
begin

  http_Send_JSON (AResponseInfo, '{result:"ok")');

//  AResponseInfo.WriteContent;
end;




// ---------------------------------------------------------------
procedure http_Send_JSON(AResponseInfo: TIdHTTPResponseInfo; aJSON: string);
// ---------------------------------------------------------------
var
  s: string;
begin
  if not Assigned(AResponseInfo) then
     Exit;


  s:=AResponseInfo.CustomHeaders.Text;


  AResponseInfo.CustomHeaders.Values['Access-Control-Allow-Origin']:='*';
  AResponseInfo.CustomHeaders.Values['Access-Control-Allow-Headers']:='Origin, X-Requested-With, Content-Type, Accept';

//  AResponseInfo.CustomHeaders.Values['Access-Control-Allow-Methods']:='GET,PUT,POST,DELETE,OPTIONS';


  AResponseInfo.ContentText:=aJSON;
  AResponseInfo.ContentType := 'application/json';

//  AResponseInfo.ResponseNo:=300;

  AResponseInfo.WriteContent;
end;


// ---------------------------------------------------------------
procedure http_Send_JSON_Wide(AResponseInfo: TIdHTTPResponseInfo; aJSON:
    WideString);
// ---------------------------------------------------------------
var
  s: string;
begin
  if not Assigned(AResponseInfo) then
     Exit;


  s:=AResponseInfo.CustomHeaders.Text;


  AResponseInfo.CustomHeaders.Values['Access-Control-Allow-Origin']:='*';
  AResponseInfo.CustomHeaders.Values['Access-Control-Allow-Headers']:='Origin, X-Requested-With, Content-Type, Accept';

//  AResponseInfo.CustomHeaders.Values['Access-Control-Allow-Methods']:='GET,PUT,POST,DELETE,OPTIONS';


  AResponseInfo.ContentText:=aJSON;
  AResponseInfo.ContentType := 'application/json';

//  AResponseInfo.ResponseNo:=300;

  AResponseInfo.WriteContent;
end;


// ---------------------------------------------------------------
procedure http_Log_Request(ARequestInfo: TIdHTTPRequestInfo; aLogEvent: TLogEvent);
// ---------------------------------------------------------------

    //------------------------------------------
    procedure Log(aMsg: string);
    //------------------------------------------
    begin
    //  DbugIntf.SendDebug(aMsg);


    //  if g_Disable_log then
     //   Exit;

      if Assigned(aLogEvent) then
      begin
        aLogEvent(aMsg);
        Exit;
      end;


//      EnterCriticalSection(csCriticalSection);
      try

{        if Assigned(aLogEvent) then
        begin
          aLogEvent(aMsg);
          Exit;
        end;}


         //aMemo.Lines.Add(aMsg);
      finally
     //  LeaveCriticalSection(csCriticalSection);
      end;

    end;


var
  FS: TFileStream;
  i: Integer;
  S: string;
  sParam: string;
  sValue: string;



begin

  s:=ARequestInfo.ContentType;

//  oTIdDecoderMIM:=TIdDecoderMIM.


   Log('--------------------------------------');
   Log(ARequestInfo.Document);

   S:=ARequestInfo.Params.Text;

//   ARequestInfo.Cookies.Count;

//  if ARequestInfo. UnparsedParams.Count>0 then


  //if ARequestInfo.CommandType = hcGET then

  {
  case ARequestInfo.CommandType of
    hcGET     :  Log ('CommandType = hcGET');
    hcPOST    :  Log ('CommandType = hcPOST');

    hcUnknown :  Log ('CommandType = hcUnknown');
    hcHEAD    :  Log ('CommandType = hcHEAD');

    hcDELETE  :  Log ('CommandType = hcDELETE');
    hcPUT     :  Log ('CommandType = hcPUT');
    hcTRACE   :  Log ('CommandType = hcTRACE');
    hcOPTION  :  Log ('CommandType = hcOPTION');
  end;
   }
   
  Log ('Params.Text: ' + ARequestInfo.Params.Text);


  if ARequestInfo.Params.Count>0 then
  begin
     Log ('Params:');

     for i:=0 to ARequestInfo.Params.Count-1 do
     begin
     //  Log ('  QueryField:'+ ARequestInfo.Params[i]);

       sParam:=ARequestInfo.Params.Names[i];
       sValue:=ARequestInfo.Params.ValueFromIndex[i];

       Log ( Format('%s:%s',[sParam,sValue]));
     end;
  end;


  if Assigned(ARequestInfo.PostStream) then
  begin

 //   oPostData.
{

    ARequestInfo.PostStream.Size;


    FS := TFileStream.Create('d:\PostStream.bin', fmCreate);
    ARequestInfo.PostStream.Position := 0;
    FS.Write(ARequestInfo.PostStream, ARequestInfo.PostStream.Size);
    FS.Free;
    }

  end;





//  PostStream


//  if ARequestInfo.pa Cookies.Count>0 then



  if ARequestInfo.Cookies.Count>0 then
  begin
    ShowMessage ('Cookies:');

     Log ('Cookies:');

     for i:=0 to ARequestInfo.Cookies.Count-1 do
     begin
     //  Log ('  QueryField:'+ ARequestInfo.Params[i]);

       sParam:=ARequestInfo.Cookies[i].CookieName;
       sValue:=ARequestInfo.Cookies[i].CookieText;

       Log ( Format('%s:%s',[sParam,sValue]));
     end;
  end;



end;



//-----------------------------------------------------------------------------
procedure http_Send_Image_PNG_file(aResponse: TIdHTTPResponseInfo;
    aPngFileName: string);
//-----------------------------------------------------------------------------
var
  oFileStream: TFileStream;
  oMemoryStream: TMemoryStream;
  sExt: string;
begin
  if not FileExists(aPngFileName) then
  begin
    ShowMessage('not FileExists(aPngFileName)');
    Exit;
  end;

  Assert(FileExists(aPngFileName));

  oFileStream:=TFileStream.Create(aPngFileName, fmShareDenyNone);
  oFileStream.Position:=0;

//  oMemoryStream:=TMemoryStream.Create;


  sExt:=ExtractFileExt(aPngFileName);

  aResponse.ContentStream:=oFileStream;
  aResponse.ContentType := 'image/png';
  aResponse.WriteContent;
 // aResponse.SendResponse;

end;


//------------------------------------------------------------------------
procedure http_Log_HTTPServer(AIdHTTPServer: TIdHTTPServer);
//------------------------------------------------------------------------

    procedure Log(aMsg: string);
    begin
  //    aMemo.Lines.Add(aMsg);
    end;

begin
 Log('--------------------------------------');

 Log(IntToStr(AIdHTTPServer.DefaultPort));



  // TODO -cMM: http_Log_HTTPServer default body inserted
end;
{
//------------------------------------------------------------------------
function http_ExtractStringFromParams(aValue: string): TStrArray;
//------------------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  aValue:=Trim(aValue);


  if (LeftStr(aValue,1)='[') and (RightStr(aValue,1)=']')
  then begin
    aValue:=Copy(aValue, 2, Length(aValue)-2);

    Result :=StringToStrArray(aValue, ',');

    for I := 0 to High(Result) do
    begin
      s:=Result[i];
      if (LeftStr(s,1)='"') and (RightStr(s,1)='"') then
        Result[i]:=Copy(s, 2, Length(s)-2);
    end;


  end;



end;}


//------------------------------------------------------------------------
function http_ExtractStringFromParams(aValue: string): string;
//------------------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  aValue:=Trim(aValue);

  if (LeftStr(aValue,1)='[') and (RightStr(aValue,1)=']')
  then begin
    aValue:=Copy(aValue, 2, Length(aValue)-2);

    Result:=ReplaceStr(aValue, '"', '''');
  end;

end;


function ReplaceStrArr(aValue: string; aFromArr: array of string; aTo: string): string;
var
  I: Integer;
begin
  Result:=aValue;

  for I := 0 to High(aFromArr) do
    Result:=ReplaceStr(Result, aFromArr[i], aTo);
end;





var
  o: TStrArray;

  sIDS : string;

begin
  sIDS:=ReplaceStrArr('["3680"]', ['[',']','"'], '');



//  o:=http_ExtractStringFromParams ('["3694","3695"]');


{
initialization
  InitializeCriticalSection(csCriticalSection);


  //g_Disable_log:=False;

finalization
  DeleteCriticalSection(csCriticalSection);
}

end.

  {

//------------------------------------------------------------------------
procedure TMatrixConverter.createTile(aXBlock, aYBlock, aZoom: Integer; aFolder: string);
//------------------------------------------------------------------------

    procedure PutPixel(aImage: TPngImage; aColor, aX, aY: integer);
    begin
      aImage.Canvas.Pixels[aX, aY]:=aColor;
      aImage.AlphaScanline[aY]^[aX]:=255;
    end;

var
  b: Boolean;
  b1: Boolean;
  image: TPngImage;
  i, j: Integer;
  iColor: Integer;
  oMercator_new: TMercator1;
  oMercator: TMercator;
  iValue, icol, irow: Integer;

   vXYPoint: IXY;

   xy: TXYPoint;

begin
  image:=TPngImage.CreateBlank(COLOR_RGBALPHA, 8, 256, 256);
  oMercator:=TMercator.Create(0, 0);

  oMercator_new:=TMercator1.Create;


  for i:=0 to 255 do
    for j:=0 to 255 do
    begin
      oMercator.SetCoordinates(aXBlock, aYBlock, aZoom, i, j);
      oMercator_new.SetCoordinates(aXBlock, aYBlock, aZoom, i, j);

      vXYPoint:=oMercator.GetXY(FMatrix.ZoneNum);
      b:=FMatrix.FindCellXY (vXYPoint.X, vXYPoint.Y, iRow, iCol);


      xy:=oMercator_new.GetGaussXY(FMatrix.ZoneNum);
      b1:=FMatrix.FindCellXY (xy.X, xy.Y, iRow, iCol);


//  vPoint:=(aPoint as TMercator).GetXY(FMatrix.ZoneNum);
//  Result:=FMatrix.FindCellXY (vPoint.X, vPoint.Y, aRow, aCol);
//  vPoint.Destroy;



//      if FindNearest(oMercator, irow, icol) then
      if b then
      begin
        iValue:=FMatrix[irow, icol];
        if iValue<>FMatrix.BlankValue then
        begin
          iColor:=FColorLegend.GetColor(iValue);

          putPixel(image, iColor, i, j);
        end;

      end;
    end;

  oMercator.Free;

  image.SaveToFile(aFolder+Format('//tile_%d_%d_%d.png', [aZoom, aXBlock, aYBlock]));
//  image.SaveToFile(aFolder+Format('//tile_%d_%d_%d.png', [aZoom, aXBlock, aYBlock ]));
  image.Free;
end;




procedure StringToIP4Addr(const AIP4Str: string): Integer;
var
  S: TStrings;
begin
  S := TStringList.Create;
  try
    S.Delimiter := '.';
    S.DelimitedText := AIP4Str;

    // do preeliminary check. The IP4 Address string must consists of 4 parts. Less or more than that would be invalid values
    if S.Count<>4 then
      raise Exception.Create('Invalid IP4 Address string');

    Result := ToIP4(StrToInt(S[0]), StrToInt(S[1]), StrToInt(S[2]),  StrToInt(S[3]));
  finally
    S.Free;
  end;
end;





(*
//-----------------------------------------------------------------------------
procedure web_SendImage_PNG(aResponse: TWebResponse; aFileName:
    string);
//-----------------------------------------------------------------------------
var
  oFileStream: TFileStream;
begin
  if not FileExists(aFileName) then
  begin
    ShowMessage('not FileExists(aFileName)');
    Exit;
  end;

  Assert(FileExists(aFileName));

  oFileStream:=TFileStream.Create(aFileName, fmOpenRead);
  oFileStream.Position:=0;

  aResponse.ContentStream:=oFileStream;
  aResponse.ContentType := 'image/png';
  aResponse.SendResponse;
end;


*)




//------------------------------------------------------------------------
procedure http_Send_Blank_Tile_Image_PNG_file(aResponse: TIdHTTPResponseInfo);
//------------------------------------------------------------------------

    procedure PutPixel(aImage: TPngImage; aColor, aX, aY: integer);
    begin
      aImage.Canvas.Pixels[aX, aY]:=aColor;
      aImage.AlphaScanline[aY]^[aX]:=255;
    end;


var
  i: Integer;
  j: Integer;
  oImage: TPngImage;
  oMemoryStream: TMemoryStream;

begin
  oImage:=TPngImage.CreateBlank(COLOR_RGBALPHA, 8, 256, 256);
//  oImage:=TPngImage.CreateBlank(COLOR_RGBALPHA, 8, 256, 256);

  //draw frame
  for i:=0 to oImage.Width-1 do
    for j:=0 to oImage.Height-1 do
    begin
      if (i=0) or (i=oImage.Width -1) or
         (j=0) or (j=oImage.Height-1)
      then
        putPixel(oImage, clRed, i, j);

    end;



  oMemoryStream:=TMemoryStream.Create;

  oImage.SaveToStream(oMemoryStream);

  aResponse.ContentStream:=oMemoryStream;
  aResponse.ContentType := 'image/png';
  aResponse.WriteContent;

  FreeAndNil(oImage);

 // oImage.SaveToStream();


  // TODO -cMM: http_Send_Blank_Tile_Image_PNG_file default body inserted
end;
