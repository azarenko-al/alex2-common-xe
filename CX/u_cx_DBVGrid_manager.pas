unit u_cx_DBVGrid_manager;

interface
uses
  cxDBVGrid,  cxDropDownEdit, cxVGrid,


   cxControls, cxInplaceContainer, cxCheckBox,  cxSpinEdit,
  GSCtrls, cxPropertiesStore,   cxButtonEdit, cxTextEdit,
  cxDBLookupComboBox,   DBGrids, cxPC, cxEdit    ,

  u_func,

   Classes;

type

//-------------------------------------------------
  TRowData = class
  //-------------------------------------------------
    FieldName  : string;
    Comment    : string; // �������� ����
  end;




type
  TcxDBVGridManager = class(TObject)
  private
    FcxDBVerticalGrid1: TcxDBVerticalGrid;
   // function GetRowData(aRow: TcxCustomRow): TRowData;
//    procedure Init(acxDBVerticalGrid: TcxDBVerticalGrid);
  //  function GetInspectorRowByFieldName111111(aFieldName: string): TcxCustomRow;
  public

    constructor Create(aDBVerticalGrid: TcxDBVerticalGrid);


//    function GetRowData(aRow: TcxCustomRow): TRowData;


    function AddCategory(aParentRow: TcxCustomRow; aCaption: string): TcxCustomRow;

//    function AddCategory(aParentRow: TcxCustomRow; aCaption: string): TcxDBEditorRow;
    function AddRow(aParentRow: TcxCustomRow; aCaption, aFieldName: string): TcxDBEditorRow;

    function AddComboBox(aParentRow: TcxCustomRow; aCaption, aFieldName: string):
        TcxDBEditorRow;

    procedure AssignComboBoxItems(aRow: TcxDBEditorRow; aItems: tstrings);

    procedure ClearRows;

  end;

type
  TcxVerticalGridManager = class(TObject)
  private
    FcxVerticalGrid1: TcxVerticalGrid;
    function FindRowByFieldName(aFieldName: string): TcxEditorRow;
  public
    constructor Create(aGrid: TcxVerticalGrid);

    function AddCategory(aParentRow: TcxCustomRow; aCaption: string): TcxCustomRow;

    function AddComboBox(aParentRow: TcxCustomRow; aCaption: string; aItems:
        tstrings): TcxEditorRow;
//    function AddCategory(aParentRow: TcxCustomRow; aCaption: string): TcxDBEditorRow;
    function AddRow(aParentRow: TcxCustomRow; aCaption: string): TcxEditorRow;

    function AddRow_Float(aParentRow: TcxCustomRow; aCaption: string): TcxEditorRow;

//    procedure AssignComboBoxItems(aRow: TcxEditorRow; aItems: tstrings);

    procedure ClearRows;
    procedure GetFieldNames(aList: TStrings);

    function GetRowData(aRow: TcxCustomRow): TRowData;

    function GetFieldValue(aFieldName: string): Variant;
    procedure SetFieldValue(aFieldName: string; aValue: Variant);

    procedure SetReadOnly;

  end;



implementation




constructor TcxDBVGridManager.Create(aDBVerticalGrid: TcxDBVerticalGrid);
begin
  inherited Create;

  FcxDBVerticalGrid1:=aDBVerticalGrid;
  FcxDBVerticalGrid1.ClearRows;

end;

// ---------------------------------------------------------------
function TcxDBVGridManager.AddCategory(aParentRow: TcxCustomRow; aCaption: string): TcxCustomRow;
// ---------------------------------------------------------------
begin
(*
  if Assigned(aParentRow) then
    Result:=cxDBVerticalGrid1.AddChild(aParentRow, TcxCategoryRow)
  else
    Result:=cxDBVerticalGrid1.Add(TcxCategoryRow);
*)

  Result:=FcxDBVerticalGrid1.Add(TcxCategoryRow);

  if Assigned(aParentRow) then
    Result.Parent := aParentRow;


  (Result as TcxCategoryRow).Properties.Caption:=aCaption;

end;


// ---------------------------------------------------------------
function TcxDBVGridManager.AddRow(aParentRow: TcxCustomRow; aCaption, aFieldName:
    string): TcxDBEditorRow;
// ---------------------------------------------------------------
var
  oEditRow: TcxDBEditorRow;
begin
  if Assigned(aParentRow) then
     Result:=FcxDBVerticalGrid1.AddChild(aParentRow, TcxDBEditorRow)  as TcxDBEditorRow
  else
     Result:=FcxDBVerticalGrid1.Add(TcxDBEditorRow) as TcxDBEditorRow;



  oEditRow:=Result as TcxDBEditorRow;

  oEditRow.Properties.Caption               :=aCaption;// + ' / '+ aFieldInfo.FieldName;
  oEditRow.Properties.DataBinding.FieldName :=aFieldName;

 // oEditRow.Tag := Integer (TRowData.Create);

end;


// ---------------------------------------------------------------
function TcxDBVGridManager.AddComboBox(aParentRow: TcxCustomRow; aCaption, aFieldName:
    string): TcxDBEditorRow;
// ---------------------------------------------------------------
var
  oEditRow: TcxDBEditorRow;
begin
  Result := AddRow(aParentRow, aCaption, aFieldName);

  oEditRow:=Result as TcxDBEditorRow;

  oEditRow.Properties.EditPropertiesClass:=TcxComboBoxProperties;

  with TcxComboBoxProperties(oEditRow.Properties.EditProperties) do
  begin
  //  OnCloseUp     := DoOnPickCloseUp_CX;
    UseMouseWheel := False;
    DropDownRows  :=20;
    DropDownListStyle:=lsFixedList;
  end;

end;

procedure TcxDBVGridManager.AssignComboBoxItems(aRow: TcxDBEditorRow; aItems: tstrings);
begin
  TcxComboBoxProperties(aRow.Properties.EditProperties).Items.Assign (aItems);

 // cxVerticalGrid1.ClearRows;

//  cxVerticalGrid1: TcxDBVerticalGrid;
end;



procedure TcxDBVGridManager.ClearRows;
begin
  FcxDBVerticalGrid1.ClearRows;

end;

//
//procedure TcxDBVGridManager.Init(acxDBVerticalGrid: TcxDBVerticalGrid);
//begin
//  FcxDBVerticalGrid1 :=acxDBVerticalGrid;
//end;

constructor TcxVerticalGridManager.Create(aGrid: TcxVerticalGrid);
begin
  inherited Create;

  FcxVerticalGrid1:=aGrid;
  FcxVerticalGrid1.ClearRows;

end;

// ---------------------------------------------------------------
function TcxVerticalGridManager.AddCategory(aParentRow: TcxCustomRow; aCaption: string): TcxCustomRow;
// ---------------------------------------------------------------
begin
(*
  if Assigned(aParentRow) then
    Result:=cxDBVerticalGrid1.AddChild(aParentRow, TcxCategoryRow)
  else
    Result:=cxDBVerticalGrid1.Add(TcxCategoryRow);
*)

  Result:=FcxVerticalGrid1.Add(TcxCategoryRow);

  if Assigned(aParentRow) then
    Result.Parent := aParentRow;


  (Result as TcxCategoryRow).Properties.Caption:=aCaption;

end;


// ---------------------------------------------------------------
function TcxVerticalGridManager.AddRow_Float(aParentRow: TcxCustomRow;
    aCaption: string): TcxEditorRow;
// ---------------------------------------------------------------
var
  oSpinEditProperties: TcxSpinEditProperties;

begin
  Result := AddRow(aParentRow, aCaption);


  Result.Properties.EditPropertiesClass:=TcxSpinEditProperties;

  oSpinEditProperties:=TcxSpinEditProperties(Result.Properties.EditProperties);

  oSpinEditProperties.Alignment.Horz:=taLeftJustify;
  oSpinEditProperties.UseMouseWheel:=False;
  oSpinEditProperties.UseCtrlIncrement:=True;


  oSpinEditProperties.ValueType := vtFloat;

  Result.Properties.Options.ShowEditButtons :=eisbNever;

(*  case aFieldInfo.FieldType of
                    vftFloat: oSpinEditProperties.ValueType := vtFloat;
                    vftInt  : oSpinEditProperties.ValueType := vtInt;
                 end;
*)

end;


// ---------------------------------------------------------------
function TcxVerticalGridManager.AddRow(aParentRow: TcxCustomRow; aCaption:
    string): TcxEditorRow;
// ---------------------------------------------------------------
var
  oEditRow: TcxEditorRow;
begin
  if Assigned(aParentRow) then
     Result:=FcxVerticalGrid1.AddChild(aParentRow, TcxEditorRow)  as TcxEditorRow
  else
     Result:=FcxVerticalGrid1.Add(TcxEditorRow) as TcxEditorRow;



  oEditRow:=Result as TcxEditorRow;

  oEditRow.Properties.Caption :=aCaption;// + ' / '+ aFieldInfo.FieldName;

  oEditRow.Tag := Integer (TRowData.Create);

 // oEditRow.Properties.DataBinding.FieldName :=aFieldName;

end;

// ---------------------------------------------------------------
function TcxVerticalGridManager.AddComboBox(aParentRow: TcxCustomRow; aCaption:
    string; aItems: tstrings): TcxEditorRow;
// ---------------------------------------------------------------
var
  oEditRow: TcxEditorRow;

  oProperties: TcxComboBoxProperties;
begin
  Result := AddRow(aParentRow, aCaption );

  oEditRow:=Result ;// as TcxEditorRow;

  oEditRow.Properties.EditPropertiesClass:=TcxComboBoxProperties;

   oProperties:=TcxComboBoxProperties(oEditRow.Properties.EditProperties);

 // with TcxComboBoxProperties(oEditRow.Properties.EditProperties) do
 // begin
  //  OnCloseUp     := DoOnPickCloseUp_CX;
    oProperties.UseMouseWheel := False;
    oProperties.DropDownRows  :=20;
    oProperties.DropDownListStyle:=lsFixedList;
 // end;

  // oProperties:=TcxComboBoxProperties(aRow.Properties.EditProperties);

   oProperties.Items.Assign (aItems);


end;
//
//procedure TcxVerticalGridManager.AssignComboBoxItems(aRow: TcxEditorRow;
//    aItems: tstrings);
//begin
//  TcxComboBoxProperties(aRow.Properties.EditProperties).Items.Assign (aItems);
//
// // cxVerticalGrid1.ClearRows;
//
////  cxVerticalGrid1: TcxDBVerticalGrid;
//end;

procedure TcxVerticalGridManager.ClearRows;
begin
  FcxVerticalGrid1.ClearRows;

end;

function TcxVerticalGridManager.GetRowData(aRow: TcxCustomRow): TRowData;
begin
  Result := TRowData(aRow.Tag);

  Assert(Assigned(Result), 'Value not assigned');

end;


procedure TcxVerticalGridManager.SetReadOnly;
begin
 ///// with FcxVerticalGrid1 do
   /////// Options:=Options - [ioEditing];
end;

// ---------------------------------------------------------------
procedure TcxVerticalGridManager.SetFieldValue(aFieldName: string; aValue:
    Variant);
// ---------------------------------------------------------------
var
  iInd: Integer;
  iValue: Integer;
  k: Integer;
  oProperties: TcxComboBoxProperties;
  oRow: TcxEditorRow;
  s: string;
begin
  oRow:=FindRowByFieldName(aFieldName);

  Assert(Assigned(oRow), 'Value not assigned');

  if oRow.Properties.EditPropertiesClass=TcxComboBoxProperties then
  begin
    oProperties:=TcxComboBoxProperties(oRow.Properties.EditProperties);

    iValue:=aValue;

    iInd := oProperties.Items.IndexOfObject(Pointer(iValue));

    if iInd>=0 then

//    if oProperties.Items.Count>aValue then
    begin     

//      s:=oProperties.Items[iInd];
//      k:=oProperties.Items.IndexOf [iInd]


      oRow.Properties.Value:=oProperties.Items[iInd];
    end;


//    s:=oRow.Properties.Values[0];
   // s:=oRow.Properties.Value[0];

//    oProperties.Va


  end else
     oRow.Properties.Value := aValue;


  if oRow.Properties.EditPropertiesClass=TcxSpinEditProperties then
  ;




 // oRow.Properties.Value := aValue;




(*

oProperties: TcxComboBoxProperties;
begin
  Result := AddRow(aParentRow, aCaption );

  oEditRow:=Result as TcxEditorRow;

  oEditRow.Properties.EditPropertiesClass:=TcxComboBoxProperties;

   oProperties:=TcxComboBoxProperties(oEditRow.Properties.EditProperties);

 // with TcxComboBoxProperties(oEditRow.Properties.EditProperties) do
 // begin
  //  OnCloseUp     := DoOnPickCloseUp_CX;
    oProperties.UseMouseWheel := False;
    oProperties.DropDownRows  :=20;
    oProperties.DropDownListStyle:=lsFixedList;
    
*)

end;

// ---------------------------------------------------------------
function TcxVerticalGridManager.GetFieldValue(aFieldName: string): Variant;
// ---------------------------------------------------------------
var
  I: Integer;
  iInd: Integer;
  oProperties: TcxComboBoxProperties;
  oRow: TcxEditorRow;
  s: string;
  s1: string;
  sValue: string;
begin
  oRow:=FindRowByFieldName(aFieldName);

   Result :=0;

 // Result := oRow.Properties.Value ;

  if oRow.Properties.EditPropertiesClass=TcxComboBoxProperties then
  begin
    oProperties:=TcxComboBoxProperties(oRow.Properties.EditProperties);

    s1:=oProperties.Items.Text;

    for I := 0 to oProperties.Items.Count - 1 do
    begin
      s:=oProperties.Items[i];


    end;

    sValue:=oRow.Properties.Value;

    iInd := oProperties.Items.IndexOf(sValue );

    if iInd>=0 then

//    if oProperties.Items.Count>aValue then
    begin


//      s:=oProperties.Items[iInd];

      result:= Integer (oProperties.Items.Objects[iInd]);
    end else
      Result :=0;
  //    ShowMessage('');
  
 end

 else
    Result := oRow.Properties.Value ;


end;


//--------------------------------------------------------
function TcxVerticalGridManager.FindRowByFieldName(aFieldName: string):
    TcxEditorRow;
//--------------------------------------------------------
var
  i: integer;
  oData: TRowData;

  oRow: TcxEditorRow;
 // oCategory: TcxCategoryRow;

begin
  Result:=nil;

 // FcxVerticalGrid1.Rows.Count;

//  with FcxVerticalGrid1 do
//    for i:=0 to Rows.Count-1 do
//  begin
//    oData:=GetRowData(Rows[i]);
//
//    Assert(Assigned(oData), 'Value not assigned');
//
//
////    if Assigned(oData) then
//    if Eq (oData.FieldName, AFieldName) then
//    begin
//     FcxVerticalGrid1.Rows[i].;
//
//      Result:=Rows[i];
//      Exit;
//    end;
//
//  end;

 for I := 0 to FcxVerticalGrid1.Rows.Count - 1 do
  begin
    //---------------------------------------------------------
    if FcxVerticalGrid1.Rows[i] is TcxEditorRow then
    begin
      oRow:=FcxVerticalGrid1.Rows[i] as TcxEditorRow;
      oData:=TRowData(oRow.Tag);

      Result := oRow;

       Assert( Assigned(oData));

       if Eq (oData.FieldName, AFieldName) then
       begin
       // FcxVerticalGrid1.Rows[i].;

         Result:=oRow;
         Exit;
       end;


//      sFieldName:=oRow.Properties.DataBinding.FieldName;

    end;// else

  //  ---------------------------------------------------------
//    if FcxVerticalGrid1.Rows[i] is TcxCategoryRow then
//    begin
//      oCategory:=FcxVerticalGrid1.Rows[i] as TcxCategoryRow;
//      oItem:=TRowData(oCategory.Tag);
//
//    end else
//      raise Exception.Create('');
//
//
//    if Eq(oItem.FieldName,aFieldName) then
//    begin
//      Result:=cxDBVerticalGrid1.Rows[i];
//      Exit;
//    end;
  end;


end;



//--------------------------------------------------------
procedure TcxVerticalGridManager.GetFieldNames(aList: TStrings);
//--------------------------------------------------------
var
  i: integer;
  oData: TRowData;

  oRow: TcxEditorRow;
 // oCategory: TcxCategoryRow;

begin
  aList.Clear;


  for I := 0 to FcxVerticalGrid1.Rows.Count - 1 do
    if FcxVerticalGrid1.Rows[i] is TcxEditorRow then
    begin
      oRow:=FcxVerticalGrid1.Rows[i] as TcxEditorRow;
      oData:=TRowData(oRow.Tag);

     // Result := oRow;

       Assert( Assigned(oData));

     //  if Eq (oData.FieldName, AFieldName) then
         aList.Add(oData.FieldName);

    end;



end;




end.


 //
//procedure TcxVerticalGridManager.Init(acxDBVerticalGrid: TcxDBVerticalGrid);
//begin
//  FcxVerticalGrid1 :=acxDBVerticalGrid;
//end;


