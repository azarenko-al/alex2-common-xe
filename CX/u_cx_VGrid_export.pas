unit u_cx_VGrid_export;

interface
uses
  SysUtils, Classes,  Registry,  cxVGrid, Variants, cxCheckBox,  cxTextEdit,
  cxDBLookupComboBox,cxButtonEdit, IniFiles,


  Dialogs,

  u_Assert,

  u_func

  , cxDBVGrid;

  

procedure cx_VerticalGrid_SaveToReg(aVerticalGrid: TcxVerticalGrid; aRegPath:
    string);


procedure cx_VerticalGrid_SaveStateToReg(aVerticalGrid: TcxDBVerticalGrid;
    aRegPath: string);



procedure cx_VerticalGrid_LoadFromReg(aVerticalGrid: TcxVerticalGrid; aRegPath:
    string);

procedure cx_VerticalGrid_SaveToIni(aVerticalGrid: TcxVerticalGrid; aIniFile:
    string);

procedure cx_VerticalGrid_LoadFromIni(aVerticalGrid: TcxVerticalGrid; aIniFile:
    string);



implementation

//------------------------------------------------------
procedure cx_VerticalGrid_SaveToReg(aVerticalGrid: TcxVerticalGrid; aRegPath:
    string);
//------------------------------------------------------
var
  I: Integer;
  oReg: TRegIniFile;
  v: Variant;
  oEditRow: TcxEditorRow;
begin
  Assert(aRegPath<>'');

  aRegPath:=IncludeTrailingBackslash (aRegPath) + aVerticalGrid.Name;


  oReg:= TRegIniFile.Create(aRegPath);

  for I := 0 to aVerticalGrid.Rows.Count - 1 do
  begin
    if aVerticalGrid.Rows[i] is TcxEditorRow then
    begin
      try
        oEditRow:=TcxEditorRow(aVerticalGrid.Rows[i]);
        v:=oEditRow.Properties.Value;

        if oEditRow.Expanded then
           oReg.WriteBool('', oEditRow.Name+'_Expanded', oEditRow.Expanded);

//        oEditRow.Properties.EditPropertiesClass.ClassName;

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxLookupComboBoxProperties then
        begin
          oReg.WriteInteger('', oEditRow.Name, IIF(VarIsNull(v), False, v));
        end else

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxCheckBoxProperties then
        begin
          oReg.WriteBool('', oEditRow.Name, IIF(VarIsNull(v), False, v));
        end else

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxTextEditProperties then
        begin
          oReg.WriteString('', oEditRow.Name, VarToStr(v));
        end else

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxButtonEditProperties then
        begin
          oReg.WriteString ('', oEditRow.Name,        VarToStr(v));
          oReg.WriteInteger('', oEditRow.Name+'_TAG', oEditRow.Tag);
        end else

          oReg.WriteString('', oEditRow.Name, VarToStr(v));

      except
        raise Exception.Create('');
      end;
    end;
  end;

  oReg.Free;

end;


//------------------------------------------------------
procedure cx_VerticalGrid_SaveToIni(aVerticalGrid: TcxVerticalGrid; aIniFile:
    string);
//------------------------------------------------------
var
  I: Integer;
  oIni: TIniFile;
  v: Variant;
  oEditRow: TcxEditorRow;
  sSection: string;
begin
  Assert(aIniFile<>'');

//  aRegPath:=IncludeTrailingBackslash (aRegPath) + aVerticalGrid.Name;

//  aFileName:= ChangeFileExt(aFileName, '.ini');

//  ForceDirByFileName(aFileName);

//  ForceDirectories (ExtractFileDir (aFileName));

  ForceDirectories( ExtractFileDir (aIniFile) );

  sSection:=aVerticalGrid.Name;


  oIni:= TIniFile.Create(aIniFile);

  for I := 0 to aVerticalGrid.Rows.Count - 1 do
  begin
    if aVerticalGrid.Rows[i] is TcxEditorRow then
    begin
      try
        oEditRow:=TcxEditorRow(aVerticalGrid.Rows[i]);
        v:=oEditRow.Properties.Value;

        if oEditRow.Expanded then
           oIni.WriteBool(sSection, oEditRow.Name+'_Expanded', oEditRow.Expanded);

//        oEditRow.Properties.EditPropertiesClass.ClassName;

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxLookupComboBoxProperties then
        begin
          oIni.WriteInteger(sSection, oEditRow.Name, IIF(VarIsNull(v), False, v));
        end else

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxCheckBoxProperties then
        begin
          oIni.WriteBool(sSection, oEditRow.Name, IIF(VarIsNull(v), False, v));
        end else

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxTextEditProperties then
        begin
          oIni.WriteString(sSection, oEditRow.Name, VarToStr(v));
        end else

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxButtonEditProperties then
        begin
          oIni.WriteString (sSection, oEditRow.Name,        VarToStr(v));
          oIni.WriteInteger(sSection, oEditRow.Name+'_TAG', oEditRow.Tag);
        end else

          oIni.WriteString(sSection, oEditRow.Name, VarToStr(v));

      except
        raise Exception.Create('');
      end;
    end;
  end;

  oIni.Free;

end;





//------------------------------------------------------
procedure cx_VerticalGrid_LoadFromReg(aVerticalGrid: TcxVerticalGrid; aRegPath:
    string);
//------------------------------------------------------
var
  bExpanded: Boolean;
  I: Integer;
  oReg: TRegIniFile;
  v: Variant;
  oEditRow: TcxEditorRow;
  s: string;
  s1: string;
begin
  Assert(aRegPath<>'');

  aRegPath:=IncludeTrailingBackslash (aRegPath) + aVerticalGrid.Name;

  oReg:= TRegIniFile.Create(aRegPath);

  for I := 0 to aVerticalGrid.Rows.Count - 1 do
  begin
    if aVerticalGrid.Rows[i] is TcxEditorRow then
    begin
      oEditRow:=TcxEditorRow(aVerticalGrid.Rows[i]);

      v:=oEditRow.Properties.Value;

      bExpanded :=oReg.ReadBool('', oEditRow.Name+'_Expanded', False);

      if bExpanded then
        oEditRow.Expanded := True;


      s:=oReg.ReadString('', oEditRow.Name, '');
      if s='' then
        Continue;


      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxLookupComboBoxProperties then
      begin
//        v:=oReg.ReadInteger('', oEditRow.Name, 0);
        v:=oReg.ReadString('', oEditRow.Name, '');
      end else

      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxCheckBoxProperties then
      begin
        v:=oReg.ReadBool('', oEditRow.Name, False);

       // if (v=False) then
        //  Continue;
         //
        oEditRow.Properties.Value:=v;
        Continue;

      end else

      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxTextEditProperties then
      begin
        v:=oReg.ReadString('', oEditRow.Name, VarToStr(v));
     //   if (v<>'') then
      //    oEditRow.Properties.Value:=v;

      end else

      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxButtonEditProperties then
      begin
        oEditRow.Tag:=oReg.ReadInteger('', oEditRow.Name+'_TAG', oEditRow.Tag);
        v :=oReg.ReadString ('', oEditRow.Name, VarToStr(v));

//         oReg.WriteString('', oEditRow.Name, VarToStr(v));
//        oReg.WriteInteger('', oEditRow.Name+'_TAG', oEditRow.Tag);
      end else
 //       raise Exception.Create('cx_VerticalGrid_SaveToReg(: '+ oEditRow.Properties.EditPropertiesClass.ClassName);

        v:=oReg.ReadString('', oEditRow.Name, VarToStr(v));


     try
       if (v<>'') //and (v<>False)
       then
         oEditRow.Properties.Value:=v;

     except
       s:=oEditRow.Name;

    ///////   s1:=oEditRow.Properties.EditPropertiesClass.ClassName;

    ///////   ShowMessage( varToStr (v) );
     end;



    end;

  end;

  oReg.Free;

end;



//------------------------------------------------------
procedure cx_VerticalGrid_LoadFromIni(aVerticalGrid: TcxVerticalGrid; aIniFile:
    string);
//------------------------------------------------------
var
  bExpanded: Boolean;
  I: Integer;
//  oReg: TRegIniFile;
  v: Variant;
  oEditRow: TcxEditorRow;
  oIni: TIniFile;
  s: string;
  s1: string;
  sSection: string;
begin
//  Assert(aRegPath<>'');

  sSection:=aVerticalGrid.Name;

  oIni:= TIniFile.Create(aIniFile);


//  aRegPath:=IncludeTrailingBackslash (aRegPath) + aVerticalGrid.Name;

//  oReg:= TRegIniFile.Create(aRegPath);

  for I := 0 to aVerticalGrid.Rows.Count - 1 do
  begin
    if aVerticalGrid.Rows[i] is TcxEditorRow then
    begin
      oEditRow:=TcxEditorRow(aVerticalGrid.Rows[i]);

      v:=oEditRow.Properties.Value;

      bExpanded :=oIni.ReadBool(sSection, oEditRow.Name+'_Expanded', False);

      if bExpanded then
        oEditRow.Expanded := True;


      s:=oIni.ReadString(sSection, oEditRow.Name, '');
      if s='' then
        Continue;


      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxLookupComboBoxProperties then
      begin
//        v:=oReg.ReadInteger('', oEditRow.Name, 0);
        v:=oIni.ReadString(sSection, oEditRow.Name, '');
      end else

      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxCheckBoxProperties then
      begin
        v:=oIni.ReadBool(sSection, oEditRow.Name, False);

       // if (v=False) then
        //  Continue;
         //
        oEditRow.Properties.Value:=v;
        Continue;

      end else

      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxTextEditProperties then
      begin
        v:=oIni.ReadString(sSection, oEditRow.Name, VarToStr(v));
     //   if (v<>'') then
      //    oEditRow.Properties.Value:=v;

      end else

      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxButtonEditProperties then
      begin
        oEditRow.Tag:=oIni.ReadInteger(sSection, oEditRow.Name+'_TAG', oEditRow.Tag);
        v :=oIni.ReadString (sSection, oEditRow.Name, VarToStr(v));

//         oReg.WriteString('', oEditRow.Name, VarToStr(v));
//        oReg.WriteInteger('', oEditRow.Name+'_TAG', oEditRow.Tag);
      end else
 //       raise Exception.Create('cx_VerticalGrid_SaveToReg(: '+ oEditRow.Properties.EditPropertiesClass.ClassName);

        v:=oIni.ReadString(sSection, oEditRow.Name, VarToStr(v));


     try
       if (v<>'') //and (v<>False)
       then
         oEditRow.Properties.Value:=v;

     except
       s:=oEditRow.Name;

    ///////   s1:=oEditRow.Properties.EditPropertiesClass.ClassName;

    ///////   ShowMessage( varToStr (v) );
     end;



    end;

  end;

  oIni.Free;

end;



//---------------------------------------------------------------------------
procedure cx_VerticalGrid_SaveStateToReg(aVerticalGrid: TcxDBVerticalGrid;
    aRegPath: string);
//---------------------------------------------------------------------------
var
  I: Integer;
  oReg: TRegIniFile;
//  v: Variant;
  oEditRow: TcxEditorRow;
  s: string;
begin
  Assert(aRegPath<>'');

  aRegPath:=IncludeTrailingBackslash (aRegPath) + aVerticalGrid.Name;


  oReg:= TRegIniFile.Create(aRegPath);

  for I := 0 to aVerticalGrid.Rows.Count - 1 do
  begin
    s:=aVerticalGrid.Rows[i].ClassNAme;


    if aVerticalGrid.Rows[i] is TcxEditorRow then
    begin
      try
        oEditRow:=TcxEditorRow(aVerticalGrid.Rows[i]);
      //  v:=oEditRow.Properties.Value;

        if oEditRow.Expanded then
           oReg.WriteBool('', oEditRow.Name+'_Expanded', oEditRow.Expanded);

//        oEditRow.Properties.EditPropertiesClass.ClassName;

      except
        raise Exception.Create('');
      end;
    end;
  end;

  oReg.Free;

end;




end.
