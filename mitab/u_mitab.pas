unit u_mitab;

interface
uses SysUtils,Classes,Forms,Math,Graphics,Dialogs, Variants,

    // u_mapx_lib,

     u_func,
     u_files,
     u_Geo,
     u_MapX,
//     u_MapX_lib,

     mitab;

type


  Tmitab_feature = mitab_feature;
  TmiFieldArray = array of  TmiFieldRec;

  //-----------------------------------------------------
  TmitabMap = class
  //-----------------------------------------------------
  private
    mitab_handle: integer;

    Fields: TmiFieldArray; //array of  TmiFieldRec;

    
//    Fields: array of record
//              Name: string;  
//              Index: integer;
//            end;
//


    function Create_feature(feature_type: integer): Tmitab_feature;
    function GetFieldIndexByName(aName: string): integer;
    
//    function get_field_as_double(aFeature: Tmitab_feature; field_index: integer):  double;
//    function get_field_as_string(aFeature: Tmitab_feature; field_index: integer):  string;

    procedure WriteFeatureFieldValues(aFeature: Tmitab_feature; aFieldValues: array of Variant);
    procedure WriteSymbol(aBLPoint: TBLPoint; aStyle: TmiStyleRec; aFieldValues:
        array of Variant);
    function Write_feature(aFeature: mitab_feature): integer;

//    procedure LoadFieldNames;

  public
  

    destructor Destroy; override;

//    function CreateFile(aFileName: string; aFields: array of TmiFieldRec): Boolean;
//        overload;

    function CreateFile(aFileName: string; aFields: array of TmiFieldRec): Boolean;
      //  overload;

    function  OpenFile (aFileName: string): boolean;
    procedure CloseFile ();


//    function  GetFieldValue (aFeature: Tmitab_feature; aFieldName: string): Variant;

//    function  Read_feature (feature_id: integer): Tmitab_feature;

//    function  Next_feature_id (last_feature_id: integer): integer;


//    function get_feature_count: Integer;
//    function get_field_as_integer(aFeature: Tmitab_feature; field_name: string): integer;

//    procedure WriteBezier(var aBLPoints: TBLPointArrayF; aStyle: TmiStyle; aParams:
//        array of TmiParam);


//    procedure WriteSymbol(aBLPoint: TBLPoint; aStyle: TmiStyleRec; aFieldValues:
//        array of Variant);

//    procedure WriteLine(aBLPoints: TBLPointArray; aStyle: TmiStyleRec;  aFieldValues: array of Variant);

    procedure WriteLineF(var aBLPoints: TBLPointArrayF; aStyle: TmiStyleRec;  aFieldValues: array of Variant);

    procedure WriteVector(aBLVector: TBLVector; aStyle: TmiStyleRec; aFieldValues:  array of Variant);

    procedure WriteRegionRect(aBLRect: TBLRect; aStyle: TmiStyleRec; aFieldValues:  array of Variant);

//
//    procedure WriteRegionRect(aBLRect: TBLRect; aStyle: TmiStyleRec; aFieldValues:
//        array of Variant);
//
    procedure WriteRegionF(var aBLPoints: TBLPointArrayF; aStyle: TmiStyleRec;
        aFieldValues: array of Variant);
//        
//    procedure WriteRegion(var aBLPoints: TBLPointArray; aStyle: TmiStyleRec;
//        aFieldValues: array of Variant);

//    procedure WriteRegionParts (var aParts: TmiPartsArr; aStyle: TmiStyle; aFieldValues:  array of TmiParam); overload;
 //   procedure WriteRegionParts (aParts: TmiRegion; aStyle: TmiStyle; aFieldValues:  array of TmiParam); overload;

  end;

//  procedure mitab_CreateFile (aFileName: string;  aFields: array of TmiField);

  function mitab_MakeColor (aColor: integer): integer; forward;

procedure mitab_test;

const

  DEF_COORD_PULKOVO_SYS = 'CoordSys Earth Projection 1, 9999, 3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0';

  
//  DEF_COORD_SYS = 'CoordSys Earth Projection 1, 1001';


//=====================================================
implementation
//=====================================================





const
  MITAB_MAX_POINT_COUNT = 5000;


const

  // feature type values
  TABFC_NoGeom      = 0;
  TABFC_Point       = 1;
  TABFC_FontPoint   = 2;
  TABFC_CustomPoint = 3;
  TABFC_Text        = 4;
  TABFC_Polyline    = 5;
  TABFC_Arc         = 6;
  TABFC_Region      = 7;
  TABFC_Rectangle   = 8;
  TABFC_Ellipse     = 9;
  TABFC_MultiPoint  = 10;


  MI_FONT_MAPINFO_SYMBOLS= 'MapInfo symbols';


//--------------------------------------------------------
procedure ColorTo_R_G_B(Value: TColor; var R,G,B: byte);
//--------------------------------------------------------
var cl: packed record case b:byte of
          0: (int: TColor);   1: (RGB: array[0..4] of byte);
        end;
begin
  cl.int:=Value;
  R:=cl.rgb[0];  G:=cl.rgb[1];  B:=cl.rgb[2];
end;


function mitab_MakeColor (aColor: integer): integer;
var r,g,b: byte;
begin
//  Result := aColor;
//  Exit;

  ColorTo_R_G_B (aColor, r,g,b);
  Result:=(r*256*256) + (g * 256) + b;
//  Result:=(b*65536) + (g * 256) + r;

//  Result := rgb(blue, green, red);

{
   iPixelColor := ColorToRGB(clRed); //for some reason mitab apparently uses BGR not RGB as MapX (?)

   red  := getrvalue(iPixelColor);
   green := getgvalue(iPixelColor);
   blue := getbvalue(iPixelColor);

   iInvColor := rgb(blue, green, red);
}
end;


destructor TmitabMap.Destroy;
begin
  CloseFile();
  inherited;
end;



// ---------------------------------------------------------------
function TmitabMap.CreateFile(aFileName: string; aFields: array of
    TmiFieldRec): Boolean;
// ---------------------------------------------------------------
var
  pch: array[0..255] of AnsiChar;
  i,iWidth,iType: integer;

//  pChr: PChar;

begin
  aFileName:=ChangeFileExt(aFileName, '.Tab');


//  mapx_DeleteFile (aFileName);

  ForceDirByFileName (aFileName);

//function  mitab_c_create          (filename, mif_or_tab, mif_projectiondef: pchar; north, south, east, west: double): mitab_handle;                      stdcall; external DLL Name '_mitab_c_create@44'               ;

//  if aBLRect.TopLeft.B=0 then
//  oMap.CreateFile ('d:\1\temp.tab',
//CoordSys Earth Projection 1, 9999, 3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0


  StrPCopy( pch, aFileName);

  mitab_handle:=mitab_c_create (pch, 'tab',

//    mitab_handle:=mitab_c_create (PAnsiChar(aFileName), 'tab',
//                                  'CoordSys Earth Projection 1, 1001',
                   //               'CoordSys Earth Projection 1, 9999, 3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0',
                                  DEF_COORD_PULKOVO_SYS,

//  DEF_COORD_PULKOVO_SYS = 'CoordSys Earth Projection 1, 9999, 3, 23.57, -140.95, -79.8, 0, -0.35, -0.79, -0.22, 0';

                                  0, 0, 0, 0);
  Result := mitab_handle<>0;

  Assert (mitab_handle<>0, 'procedure TmitabMap.CreateFile: '+aFileName);

  iWidth:=0;

  for i:=0 to High(aFields) do
  begin
    StrPCopy(pch,aFields[i].Name);

//function  mitab_c_add_field
//(handle: mitab_handle; field_name: PAnsiChar;field_type, width, precision, indexed, unique: longint): longint;

    case aFields[i].Type_ of
      miTypeString : begin
                     //  iType:=TABFT_Char;
                      // iWidth:=aFields[i].Size;
                       mitab_c_add_field (mitab_handle, pch, TABFT_Char, aFields[i].Size, 0,0,0);

                     end;
      miTypeFloat    : //iType:=TABFT_Float;
                       mitab_c_add_field (mitab_handle, pch, TABFT_Float, 12, 2,0,0);


      miTypeInt      : //iType:=TABFT_Integer;
                       mitab_c_add_field (mitab_handle, pch, TABFT_SmallInt, 8, 0,0,0);

      miTypeSmallInt : //iType:=TABFT_SmallInt;
                       mitab_c_add_field (mitab_handle, pch, TABFT_SmallInt, 0, 0,0,0);

      miTypeLogical  : //iType:=TABFT_Logical;
                       mitab_c_add_field (mitab_handle, pch, TABFT_Logical, 0, 0,0,0);

    else
      raise Exception.Create('');

    end;

//    StrPCopy(pch,aFields[i].Name);
//    mitab_c_add_field (mitab_handle, pch, iType, iWidth, 0,0,0);
  end;

//    field_index = mitab_c_add_field( dataset, "TestInt",
//                                     TABFT_Integer, 8, 0,
//                                     1, 0 ); /* Indexed */
//    assert( field_index == 0 );
//
//    field_index = mitab_c_add_field( dataset, "TestFloat", TABFT_Float,
//                                     12, 2, 0, 0 );
//    assert( field_index == 1 );
//
//    field_index = mitab_c_add_field( dataset, "TestString", TABFT_Char,
//                                     10, 0, 0, 0 );
//    assert( field_index == 2 );


//
//  TABFT_Char        = 1;
//  TABFT_Integer     = 2;
//  TABFT_SmallInt    = 3;
//  TABFT_Decimal     = 4;
//  TABFT_Float       = 5;
//  TABFT_Date        = 6;
//  TABFT_Logical     = 7;


  SetLength(Fields, Length(aFields));
  for I := 0 to High(aFields) do
    Fields[i]:= aFields[i];

end;


// ---------------------------------------------------------------
function TmitabMap.OpenFile(aFileName: string): boolean;
// ---------------------------------------------------------------
begin
  mitab_handle:=mitab_c_open (PAnsiChar(aFileName));

end;


procedure TmitabMap.CloseFile;
begin
  if mitab_handle>0 then begin
    mitab_c_close (mitab_handle);
    mitab_handle:=0;
  end;
end;


function TmitabMap.Create_feature(feature_type: integer): Tmitab_feature;
begin
  result := mitab_c_Create_feature (mitab_handle, feature_type);
end;


function TmitabMap.Write_feature(aFeature: mitab_feature): integer;
begin
  result := mitab_c_Write_feature (mitab_handle, aFeature);
end;


function TmitabMap.GetFieldIndexByName(aName: string): integer;
var
  i: Integer;
begin
  Result := -1;

  Assert (Length(Fields)>0);

  for I := 0 to High(Fields) do
    if Eq(Fields[i].Name, aName) then
    begin
      result := i;
      Exit;
    end;

end;


//------------------------------------------------------
procedure TmitabMap.WriteFeatureFieldValues(aFeature: Tmitab_feature;  aFieldValues: array of Variant);
//------------------------------------------------------
var
  iIndex,i: integer;
  s: ShortString;
  iFeature: Tmitab_feature;
 // p: PAnsiChar;

  pch: array[0..255] of AnsiChar;

begin
  Assert (Length(Fields)>0);

//  pChr: PChar;




  for i := 0 to (Length(aFieldValues) div 2)-1 do
  begin
    iIndex:=GetFieldIndexByName (aFieldValues[i*2]);
    if iIndex<0 then
      Continue;

    s:=VarToStr(aFieldValues[i*2+1]);

//    s:=s.Replace('.',',');

    StrPCopy( pch, s);

//    s:=s.Replace(',','.');

//procedure mitab_c_set_field (feature: mitab_feature; field_index: longint; value: PAnsiChar);                                                          stdcall; external DLL Name '_mitab_c_set_field@12'            ;

//    mitab_c_set_field (aFeature, iIndex, PAnsiChar(s) );
    mitab_c_set_field (aFeature, iIndex, pch );

//    p:=mitab_c_get_field_as_string (aFeature, iIndex);

//    function  mitab_c_get_field_as_string    (feature: mitab_feature; field: longint): PAnsiChar;                                                                       stdcall; external DLL Name '_mitab_c_get_field_as_string@8'   ;
  end;

end;


// ---------------------------------------------------------------
procedure TmitabMap.WriteVector(aBLVector: TBLVector; aStyle: TmiStyleRec;
    aFieldValues: array of Variant);
// ---------------------------------------------------------------
var
  BLPoints: TBLPointArrayF;
begin
  BLPoints.Count:=2;
  BLPoints.Items[0]:=aBLVector.Point1;
  BLPoints.Items[1]:=aBLVector.Point2;

  WriteLineF (BLPoints, aStyle, aFieldValues);
end;



// ---------------------------------------------------------------
procedure TmitabMap.WriteLineF(var aBLPoints: TBLPointArrayF; aStyle:
    TmiStyleRec; aFieldValues: array of Variant);
// ---------------------------------------------------------------
var r,i: integer;
    iFeature: Tmitab_feature;
    x,y: array[0..MITAB_MAX_POINT_COUNT-1] of double;
begin
  iFeature:=mitab_c_create_feature (mitab_handle, TABFC_Polyline);
  Assert (iFeature<>0);

  Assert(aBLPoints.Count<MITAB_MAX_POINT_COUNT);


  for I := 0 to aBLPoints.Count-1 do
  begin
    y[i]:=aBLPoints.Items[i].B;
    x[i]:=aBLPoints.Items[i].L;
  end;

  mitab_c_set_points (iFeature, 0, aBLPoints.Count, x[0], y[0]);

  mitab_c_set_pen (iFeature, aStyle.LineWidth, aStyle.LineStyle, mitab_MakeColor(aStyle.LineColor));

  WriteFeatureFieldValues (iFeature, aFieldValues);


  r:=mitab_c_write_feature (mitab_handle, iFeature);
  Assert (r=1);

  mitab_c_destroy_feature (iFeature);


end;


//------------------------------------------------------
procedure TmitabMap.WriteSymbol(aBLPoint: TBLPoint; aStyle: TmiStyleRec;  aFieldValues: array of Variant);
//------------------------------------------------------
//void MITAB_STDCALL mitab_c_set_symbol  (  mitab_feature  feature,
//int  symbol_no,
//int  symbol_size,
//int  symbol_color

var
  i2: Integer;
  i1: Integer;
  r,i: integer;
  iFeature: Tmitab_feature;
  x,y: array[0..1] of double;

begin
  iFeature:=mitab_c_create_feature (mitab_handle, TABFC_FontPoint);
  Assert (iFeature>0);

 // StrPCopy(pch, aStyle.FontName);

//  with aStyle do
  mitab_c_set_font  (iFeature, PAnsiChar(aStyle.SymbolFontName));

//  mitab_c_set_font  (iFeature, pch);// PChar(aStyle.FontName));
 //// mitab_c_set_symbol (iFeature, aStyle.SymbolNumber, aStyle.FontSize,  aStyle.FontColor);

  if aStyle.SymbolFontSize=0 then  aStyle.SymbolFontSize:=10;
  if aStyle.SymbolCharacter=0 then  aStyle.SymbolCharacter:=MI_SYMBOL_STAR;


//  i1 := 256*256;
 // i2 := clRed;
  //i1 := i1 * i2;
  //with aStyle do
  mitab_c_set_symbol(iFeature,
                     aStyle.SymbolCharacter,
                     aStyle.SymbolFontSize,
                     mitab_MakeColor(aStyle.SymbolFontColor));


 // with aStyle do
  //  mitab_c_set_pen  (iFeature, RegionBorderWidth, RegionBorderStyle, RegionBorderColor);


//  mitab_c_set_symbol_angle (iFeature, aStyle.SymbolFontRotation );
//  mitab_c_set_pen  (iFeature, aStyle.LineWidth, aStyle.LineStyle, aStyle.LineColor);

{  with aStyle do
    mitab_c_set_pen  (iFeature, RegionBorderWidth, RegionBorderStyle, RegionBorderColor);

  with aStyle do
    mitab_c_set_brush  (iFeature, RegionBackColor, RegionForegroundColor, RegionPattern, IIF(RegionIsTransparent,1,0));
}

  y[0]:=aBLPoint.B;
  x[0]:=aBLPoint.L;

  mitab_c_set_points (iFeature, 0, 1, x[0], y[0]);

  WriteFeatureFieldValues (iFeature, aFieldValues);

  r:=mitab_c_write_feature (mitab_handle, iFeature);
  Assert (r=1);
  mitab_c_destroy_feature (iFeature);

end;



// ---------------------------------------------------------------
procedure TmitabMap.WriteRegionRect(aBLRect: TBLRect; aStyle: TmiStyleRec;
    aFieldValues: array of Variant);
// ---------------------------------------------------------------
var
  arPoints: TBLPointArrayF;
begin
  geo_BLRectToBLPointsF(aBLRect, arPoints);

  WriteRegionF (arPoints, aStyle, aFieldValues);
end;



// ---------------------------------------------------------------
procedure mitab_test;
// ---------------------------------------------------------------
var
  dOffset: double;   
  i: Integer;
  oMap: TmitabMap;

  blPointF: TBLPointArrayF;
  rStyle: TmiStyleRec;
  blRect: TblRect;
begin
  oMap:=TmitabMap.Create;

//  blRect.TopLeft:=

  oMap.CreateFile ('d:\1\temp.tab',
                    [mapx_Field ('name', miTypeString, 100)]
                  );

// oMap.CloseFile;

{
  oMap.CreateFile ('u:\test.tab',
                    [mif_Field ('name', miTypeString, 100)],
                    blRect
                  );

}  //oMap.CloseFile;
{
  blPointF.Count:=2;
  blPointF.Items[0].B:=50;
  blPointF.Items[0].L:=30;

  blPointF.Items[1].B:=50.1;
  blPointF.Items[1].L:=30.1;

  aStyle.LineColor:=clRed;
  aStyle.LineStyle:=MI_PEN_PATTERN_SOLId;
  aStyle.LineWidth:=1;

}

  for i := 0 to 5000 do
  begin
    fillChar (rStyle, SizeOf(rStyle), 0);

    dOffset:=(1/10000) * i;


    blPointF.Count:=2;
    blPointF.Items[0].B:=50+dOffset;
    blPointF.Items[0].L:=40+dOffset;

    blPointF.Items[1].B:=53.1+dOffset;
    blPointF.Items[1].L:=35.122+dOffset;

//      blPointF.Items[2].B:=50.3+dOffset;
//      blPointF.Items[2].L:=35.3+dOffset;
//
//
//      blPointF.Items[3].B:=50+dOffset;
//      blPointF.Items[3].L:=40+dOffset;
//


    rStyle.LineStyle:=MI_PEN_PATTERN_SOLId;
    rStyle.LineColor:=clRed;


    oMap.WriteLineF (blPointF, rStyle, ['name', 'sds'] );


//      rStyle.RegionBorderStyle:=MI_PEN_PATTERN_SOLId;
//      rStyle.RegionBorderColor:=clRed;
//      rStyle.RegionBorderWidth:=1;
//
//      rStyle.RegionPattern:=miPatternNoFill;  // Solid; //miPatternNoFill;


    //  rStyle.RegionPattern  :=MI_BRUSH_PATTERN_DOTS;
    //  rStyle.RegionForegroundColor:=clYellow;
    //  rStyle.RegionForegroundColor:=clYellow;
    //  rStyle.RegionBackgroundColor    :=clRed;
    //  rStyle.RegionForegroundColor:=clRed;

//      rStyle.RegionIsTransparent:=False;
//
//
//      try
//        oMap.WriteRegionF (blPointF, rStyle, [mapx_Par ('name', 'sds')]);
//      finally
//      	// free resources
//      end;  // try/finally
//


{
      rStyle.Character:=MI_SYMBOL_STAR;
      rStyle.FontColor:=clNavy;
      rStyle.FontSize:=14;
      rStyle.FontName:=MI_FONT_MAPINFO_SYMBOLS;
      //rStyle.RegionBackColor    :=clRed;


      oMap.WriteSymbol (blPointF.Items[0], rStyle, [mapx_Par ('name', 'sds')]);
}
  end;

 //// oMap.WriteRegion (blPointF, rStyle, [mif_Par ('name', 'sds')]);

{
  aStyle.LineColor:=clRed;
  aStyle.LineStyle:=MI_PEN_PATTERN_SOLId;
  aStyle.LineWidth:=1;}


//   oMap.WriteLine(blPointF, aStyle);

  oMap.CloseFile;
end;


// ---------------------------------------------------------------
procedure TmitabMap.WriteRegionF(var aBLPoints: TBLPointArrayF; aStyle:
    TmiStyleRec; aFieldValues: array of Variant);
// ---------------------------------------------------------------
var
  r,i,j: integer;
  iFeature: Tmitab_feature;

  x,y: array[0..MITAB_MAX_POINT_COUNT-1] of double;
begin
  Assert (Length(Fields)>0);


  iFeature:=mitab_c_create_feature (mitab_handle, TABFC_Region );
  Assert (iFeature<>0);

  with aStyle do
    mitab_c_set_pen  (iFeature,
                      RegionBorderWidth,
                      RegionBorderStyle,
                      mitab_MakeColor(RegionBorderColor));

//procedure mitab_c_set_brush
// (feature: mitab_feature; fg_color, bg_color, pattern, transparent: longint);                                           stdcall; external DLL Name '_mitab_c_set_brush@20'            ;

  with aStyle do
    mitab_c_set_brush(iFeature,
                      mitab_MakeColor(RegionColor),
                      mitab_MakeColor(RegionBackColor),
                      RegionPattern,
                      0);
                      //IIF(Region IsTransparent,1,0));

  Assert(aBLPoints.Count<MITAB_MAX_POINT_COUNT);

  for i:=0 to aBLPoints.Count-1 do
  begin
    y[i]:=aBLPoints.Items[i].B;
    x[i]:=aBLPoints.Items[i].L;

  end;

  y[aBLPoints.Count]:=y[0];
  x[aBLPoints.Count]:=x[0];

  mitab_c_set_points (iFeature, j, aBLPoints.Count+1, x[0], y[0]);


  WriteFeatureFieldValues (iFeature, aFieldValues);

  r:=mitab_c_write_feature (mitab_handle, iFeature);
  Assert (r=1);

  mitab_c_destroy_feature (iFeature);

end;



begin

 // mitab_test;


end.

(*







{
//------------------------------------------------------
procedure TmitabMap.WriteRegionParts (aParts: TmiRegion; aStyle: TmiStyle;
    aFieldValues: array of TmiParam);
//------------------------------------------------------
var
  iPointCount: integer;
  r,i,iPart: integer;
  iFeature: Tmitab_feature;

  x,y: array[0..MITAB_MAX_POINT_COUNT-1] of double;
begin
  if aParts.Count=0 then
    Exit;


  iFeature:=mitab_c_create_feature (mitab_handle, TABFC_Region );
  Assert (iFeature<>0);

  with aParts.Style do
    mitab_c_set_pen  (iFeature,
                      RegionBorderWidth,
                      RegionBorderStyle,
                      MakeMapinfoColor(RegionBorderColor));

  with aStyle do
    mitab_c_set_brush(iFeature,
                      MakeMapinfoColor(RegionForegroundColor),
                      MakeMapinfoColor(RegionBackgroundColor),
                      RegionPattern,
                      IIF(RegionIsTransparent,1,0));


  for iPart:=0 to aParts.Count-1 do
  begin
    iPointCount:=aParts[iPart].Points.Count;

    Assert(iPointCount<>0);
    Assert(iPointCount<MITAB_MAX_POINT_COUNT);


    for I := 0 to aParts[iPart].Points.Count-1 do
    begin
      y[i]:=aParts[iPart].Points.Items[i].B;
      x[i]:=aParts[iPart].Points.Items[i].L;
    end;

    y[iPointCount]:=y[0];
    x[iPointCount]:=x[0];

    mitab_c_set_points (iFeature, -iPart, iPointCount+1, x[0], y[0]);

  end;


  WriteFeatureFieldValues (iFeature, aFieldValues);

  r:=mitab_c_write_feature (mitab_handle, iFeature);
  Assert (r=1);

  mitab_c_destroy_feature (iFeature);

end;
}





// ---------------------------------------------------------------
procedure TmitabMap.WriteRegion(var aBLPoints: TBLPointArray; aStyle:
    TmiStyleRec; aFieldValues: array of Variant);
// ---------------------------------------------------------------
var blPoints: TBLPointArrayF;
  I: Integer;
begin
  blPoints.Count:=Length(aBLPoints);

  for I := 0 to High(aBLPoints) do
    blPoints.Items[i]:=aBLPoints[i];

  WriteRegionF(BLPoints, aStyle, aFieldValues);

end;



// ---------------------------------------------------------------
procedure TmitabMap.WriteRegionF(var aBLPoints: TBLPointArrayF; aStyle:
    TmiStyleRec; aFieldValues: array of Variant);
// ---------------------------------------------------------------
var
  r,i,j: integer;
  iFeature: Tmitab_feature;

  x,y: array[0..MITAB_MAX_POINT_COUNT-1] of double;
begin
  iFeature:=mitab_c_create_feature (mitab_handle, TABFC_Region );
  Assert (iFeature<>0);

  with aStyle do
    mitab_c_set_pen  (iFeature,
                      RegionBorderWidth,
                      RegionBorderStyle,
                      mitab_MakeColor(RegionBorderColor));

  with aStyle do
    mitab_c_set_brush(iFeature,
                      mitab_MakeColor(RegionForegroundColor),
                      mitab_MakeColor(RegionBackgroundColor),
                      RegionPattern,
                      IIF(RegionIsTransparent,1,0));

  Assert(aBLPoints.Count<MITAB_MAX_POINT_COUNT);

  for i:=0 to aBLPoints.Count-1 do
  begin
    y[i]:=aBLPoints.Items[i].B;
    x[i]:=aBLPoints.Items[i].L;

  end;

  y[aBLPoints.Count]:=y[0];
  x[aBLPoints.Count]:=x[0];

  mitab_c_set_points (iFeature, j, aBLPoints.Count+1, x[0], y[0]);


  WriteFeatureFieldValues (iFeature, aFieldValues);

  r:=mitab_c_write_feature (mitab_handle, iFeature);
  Assert (r=1);

  mitab_c_destroy_feature (iFeature);

end;


// ---------------------------------------------------------------
procedure TmitabMap.WriteRegionRect(aBLRect: TBLRect; aStyle: TmiStyleRec;
    aFieldValues: array of Variant);
// ---------------------------------------------------------------
var
  arPoints: TBLPointArrayF;
begin
  geo_BLRectToBLPointsF(aBLRect, arPoints);

  WriteRegionF (arPoints, aStyle, aFieldValues);
end;





//------------------------------------------------------
procedure TmitabMap.WriteSymbol(aBLPoint: TBLPoint; aStyle: TmiStyleRec;  aFieldValues: array of Variant);
//------------------------------------------------------
//void MITAB_STDCALL mitab_c_set_symbol  (  mitab_feature  feature,
//int  symbol_no,
//int  symbol_size,
//int  symbol_color

var
  i2: Integer;
  i1: Integer;
  r,i: integer;
  iFeature: Tmitab_feature;
  x,y: array[0..1] of double;

begin
  iFeature:=mitab_c_create_feature (mitab_handle, TABFC_FontPoint);
  Assert (iFeature>0);

 // StrPCopy(pch, aStyle.FontName);

//  with aStyle do
  mitab_c_set_font  (iFeature, PChar(aStyle.FontName));

//  mitab_c_set_font  (iFeature, pch);// PChar(aStyle.FontName));
 //// mitab_c_set_symbol (iFeature, aStyle.SymbolNumber, aStyle.FontSize,  aStyle.FontColor);

  if aStyle.FontSize=0 then  aStyle.FontSize:=10;
  if aStyle.Character=0 then  aStyle.Character:=MI_SYMBOL_STAR;
    

//  i1 := 256*256;
 // i2 := clRed;
  //i1 := i1 * i2;
  //with aStyle do
  mitab_c_set_symbol(iFeature,
                     aStyle.Character,
                     aStyle.FontSize,
                     mitab_MakeColor(aStyle.FontColor));


 // with aStyle do
  //  mitab_c_set_pen  (iFeature, RegionBorderWidth, RegionBorderStyle, RegionBorderColor);


//  mitab_c_set_symbol_angle (iFeature, aStyle.SymbolFontRotation );
//  mitab_c_set_pen  (iFeature, aStyle.LineWidth, aStyle.LineStyle, aStyle.LineColor);

{  with aStyle do
    mitab_c_set_pen  (iFeature, RegionBorderWidth, RegionBorderStyle, RegionBorderColor);

  with aStyle do
    mitab_c_set_brush  (iFeature, RegionBackColor, RegionForegroundColor, RegionPattern, IIF(RegionIsTransparent,1,0));
}

  y[0]:=aBLPoint.B;
  x[0]:=aBLPoint.L;

  mitab_c_set_points (iFeature, 0, 1, x[0], y[0]);

  WriteFeatureFieldValues (iFeature, aFieldValues);

  r:=mitab_c_write_feature (mitab_handle, iFeature);
  Assert (r=1);
  mitab_c_destroy_feature (iFeature);

end;


//
//procedure mitab_CreateFile(aFileName: string; aFields: array of TmiField);
//var
//  obj: TmitabMap;
//begin
//  obj := TmitabMap.Create();
//
//  obj.CreateFile (aFileName, aFields);
//  obj.Free;
//end;
//
//
//procedure TmitabMap.WriteLine(aBLPoints: TBLPointArray; aStyle: TmiStyleRec;
//    aFieldValues: array of Variant);
//begin
//
//end;
//
//
//
//function TmitabMap.CreateFile(aFileName: string; aFields: array of
//    TmiFieldRec): Boolean;
//begin
//  Result :=CreateFile(aFileName, aFields, NULL_BLRECT);
//end;



// ---------------------------------------------------------------
procedure TmitabMap.LoadFieldNames();
// ---------------------------------------------------------------
var
  I: Integer;
  iCount: integer;
  pCh: PChar;
begin
  iCount:=mitab_c_get_field_count (mitab_handle);

  SetLength (Fields, iCount);
  for I := 0 to iCount-1 do
  begin
    pCh:=mitab_c_get_field_name (mitab_handle, i);

    Fields[i].Name:=string(pCh);
    Fields[i].Index:=i;
  end;
end;





//function TmitabMap.get_feature_count: Integer;
//begin
//  Result := mitab_c_get_feature_count(mitab_handle);
//end;



//function TmitabMap.Next_feature_id(last_feature_id: integer): integer;
//begin
//  result := mitab_c_Next_feature_id (mitab_handle, last_feature_id);
//end;
//
//
//function TmitabMap.Read_feature(feature_id: integer): Tmitab_feature;
//begin
//  result := mitab_c_Read_feature (mitab_handle, feature_id);
//end;

//function TmitabMap.get_field_as_double(aFeature: Tmitab_feature; field_index:
//    integer): double;
//begin
//  result := mitab_c_get_field_as_double (aFeature, field_index);
//end;
//
//
//function TmitabMap.get_field_as_string(aFeature: Tmitab_feature; field_index:
//    integer): string;
//var
//  Pch: PChar;
//begin
//  Pch := mitab_c_get_field_as_string (aFeature, field_index);
//  result := String(Pch);
//end;


//function TmitabMap.get_field_as_integer(aFeature: Tmitab_feature; field_name:
//    string): integer;
//var
//  iIndex: integer;
//begin
//  iIndex:=GetFieldIndexByName(field_name);
//  result := AsInteger( get_field_as_string (aFeature, iIndex));
//
//end;



//
////------------------------------------------------------
//procedure TmitabMap.WriteBezier(var aBLPoints: TBLPointArrayF; aStyle: TmiStyle;
//    aParams: array of TmiParam);
////------------------------------------------------------
//const
//  ARROW_LEN = 300;
//var
//  arrBLPointsOut: TBLPointArrayF;
//  iCenter: integer;
//  dAzimuth: double;
//  blVector: TBLVector;
//begin
//  geo_MakeBezier (aBLPoints, arrBLPointsOut);
//
//  WriteLineF(arrBLPointsOut, aStyle, aParams);
//
//  iCenter:=arrBLPointsOut.Count div 2;
//
//  //draw arrow on the centre
//
//  dAzimuth:=geo_Azimuth(arrBLPointsOut.Items[iCenter], arrBLPointsOut.Items[iCenter-1]);
//  blVector.Point1:= arrBLPointsOut.Items[iCenter];
//
//  blVector.Point2:= geo_RotateByAzimuth (blVector.Point1, ARROW_LEN, geo_Azimuth_Plus_Azimuth(dAzimuth,30));
//  WriteVector(blVector, aStyle, [] );
////  WriteVector(blVector, aStyle, [mapx_Par('objname','')] );
//
//  blVector.Point2:=geo_RotateByAzimuth (blVector.Point1, ARROW_LEN, geo_Azimuth_Plus_Azimuth(dAzimuth,-30));
//  WriteVector(blVector, aStyle, []);
////  WriteVector(blVector, aStyle, [mapx_Par('objname','')]);
//end;



//
////------------------------------------------------------
//procedure mapx_DeleteFile (aFileName: string);
////------------------------------------------------------
//begin
//  SysUtils.DeleteFile (ChangeFileExt (aFileName, '.dat'));
//  SysUtils.DeleteFile (ChangeFileExt (aFileName, '.id'));
//  SysUtils.DeleteFile (ChangeFileExt (aFileName, '.map'));
//  SysUtils.DeleteFile (ChangeFileExt (aFileName, '.tab'));
//end;



//function TmitabMap.GetFieldValue(aFeature: Tmitab_feature; aFieldName: string): Variant;
//begin
//
//end;


