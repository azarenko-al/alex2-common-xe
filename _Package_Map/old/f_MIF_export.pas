unit f_MIF_export;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls,  StdCtrls, ToolWin, ComCtrls, MapXLib_TLB,

  u_MapX,
  u_GEO
  ;

  {$DEFINE CLOSE}

type
  TExportMapRec = packed
  record
    GeosetFileName: string;

    ImgFileName: string;
    Scale: integer;

    Mode: (mtNone,mtVEctor,mtPoint,mtRect);

    BLPoint1: TblPoint;
    BLVector: TblVector;
    BLRect: TBLRect;

    Width,Height: double;
  end;



  Tfrm_MIF_export = class(TForm)
    Map1: TMap;
    ToolBar1: TToolBar;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  protected

    procedure ExportMap;

  public

    Params: TExportMapRec;
  end;


  procedure MIF_ExportMap (aRec: TExportMapRec);

  

implementation {$R *.dfm}


// -------------------------------------------------------------------
procedure MIF_ExportMap (aRec: TExportMapRec);
// -------------------------------------------------------------------
var obj: Tfrm_MIF_export;
begin
  obj:=Tfrm_MIF_export.Create(Application);
  obj.ShowModal;


//  obj.Mode:=aRec;

  obj.Params:=aRec;

  obj.ExportMap;

  obj.free;
end;


procedure Tfrm_MIF_export.FormCreate(Sender: TObject);
begin 
  Caption:='�������� �����...';

 // Top:=3000;
end;




// -------------------------------------------------------------------
procedure Tfrm_MIF_export.ExportMap;
// -------------------------------------------------------------------
begin
  Map1.Geoset:=Params.GeosetFileName;

  if Params.Scale=0 then
    Params.Scale:=100000;


  Map1.Title.X:=0;
  Map1.Title.Y:=0;
  Map1.Title.Editable:= False;
  Map1.Title.Border:= False;
  Map1.Title.Caption:='-';
  Map1.Title.Visible:=False;

  Map1.PaperUnit:= miUnitCentimeter;
  Map1.MapUnit  := miUnitCentimeter;

  // Width,Height - in cm
  Map1.Width :=Round (Params.Width  * (Screen.PixelsPerInch )/2.54)  ;
  Map1.Height:=Round (Params.Height * (Screen.PixelsPerInch )/2.54)  ;


  {$IFDEF CLOSE}
   PostMessage(Handle, WM_CLOSE, 0,0);
  {$ENDIF}

  ShowModal;

  Map1.ExportMap (Params.ImgFileName, miFormatBmp);

end;



procedure Tfrm_MIF_export.FormShow(Sender: TObject);
begin

  mapx_SetScale (Map1, Params.Scale);


  case Params.Mode of
    mtPoint:  begin
         //       mapx_SetScale (Map1, Params.Scale);
                if Params.blPoint1.B<>0 then
                  Map1.ZoomTo (Map1.Zoom, Params.blPoint1.L, Params.blPoint1.B);
              end;

    mtVector: mapx_SetBoundsForBLVector (Map1, Params.BLVector);
  end;

end;


end.
