unit u_proxy_1111111111__;

interface

uses
  Dialogs, JPEG, PNGImage, IdIcmpClient,

 Forms,

 IdGlobal,
 
  ExtCtrls, Graphics,  SysUtils, Classes,  StrUtils, Windows, WinSock,
                   
  
  IniFiles,
  StdVcl, 

  Registry;


implementation




function Ping_(aClient: TIdIcmpClient; const AHost : string): Boolean;
var
 // MyIdIcmpClient: TIdIcmpClient;
  sBuffer: Shortstring;
begin
  Result := True;

 // MyIdIcmpClient := TIdIcmpClient.Create(Application);
  aClient.ReceiveTimeout := 200;
  aClient.Host := AHost;
  aClient.PacketSize := 24;
  aClient.Protocol := 1;
  aClient.IPVersion := Id_IPv4;
                         
//  sBuffer := MyIdIcmpClient.Host;// + StringOfChar(' ', 255);
                    
 
  try
   aClient.Ping(aClient.Host);
 //   MyIdIcmpClient.Ping11111111;
    Application.ProcessMessages; // There's no need to call this!
  except
    on E: Exception do 
    begin
      ShowMessage(e.Message);
    
      Result := False;
      Exit;
    end;
  end;  
  
  if aClient.ReplyStatus.ReplyStatusType <> rsEcho Then 
     result := False;

 // MyIdIcmpClient.Free;
end;


// ---------------------------------------------------------------
function DetectIEProxyServer(var aProxyHost: string; var aProxyPort: integer):
    Boolean;
// ---------------------------------------------------------------
var
  bProxyEnable: Boolean;
  k: Integer;
//  oReg: TRegIniFile;
  s: string;
  sProxyServer: string;
begin
//    
//   oReg:=TRegIniFile.Create('\Software\Microsoft\Windows\CurrentVersion\Internet Settings');
//
//      Result :=oReg.ReadString('','ProxyServer','');
//
//      
//      s:=oReg.ReadString('','ProxyEnable','0');
//
//      
//      bProxyEnable :=oReg.ReadInteger('','ProxyEnable',0) = 1;
//
//      
//      FreeAndNil(oReg);
//
//      
//      Exit;
//
    
    with TRegistry.Create do
    try
        RootKey := HKEY_CURRENT_USER;
            
        if OpenKey('\Software\Microsoft\Windows\CurrentVersion\Internet Settings', False) then 
        begin
          sProxyServer := ReadString('ProxyServer');
 
          k:=LastDelimiter(':',sProxyServer);
          if k>0 then
          begin
            aProxyHost:=LeftStr(sProxyServer,k-1);

            s:= Copy(sProxyServer,k+1, 100);
                
            aProxyPort:=StrToIntDef(s,0);
          end;
 
          k :=ReadInteger('ProxyEnable');
          Result:=k=1;
                                       
          //ProxyEnable

              
          CloseKey;
        end;
       // else
        //  Result := '';
    finally
      Free;
    end;


  if Result then
    aProxyHost:=GetIPAddress (aProxyHost);

//        IdHTTP1.ProxyParams.ProxyServer:=GetIPAddress (sProxyHost);


end;

// ---------------------------------------------------------------
function GetIPAddress(NetworkName: String): String;
// ---------------------------------------------------------------
var
  Error: DWORD;
  HostEntry: PHostEnt;
  Data: WSAData;
  Address: In_Addr;
begin
     Error:=WSAStartup(MakeWord(1, 1), Data);
     if Error = 0 then 
     begin
//          HostEntry:=gethostbyname(PansiChar(NetworkName));
          HostEntry:=gethostbyname(PansiChar(AnsiString(NetworkName)));

          
          Error:=GetLastError();
          if Error = 0 then 
          begin
               Address:=PInAddr(HostEntry^.h_addr_list^)^;
               Result:=inet_ntoa(Address);
          end
          else 
          begin
               Result:='Unknown';
          end;
     end
     else
     begin
          Result:='Error';
     end;
     WSACleanup();
end;


end.


function DetectIEProxyServer(var aProxyHost: string; var aProxyPort: integer):
    Boolean;

function GetIPAddress(NetworkName: String): String;

function Ping_(aClient: TIdIcmpClient; const AHost : string): Boolean;


