unit u_XML_new;

interface

uses

  System.Classes,   
   Xml.XMLIntf,  Forms, SysUtils, Variants, XMLDoc,  Dialogs,

   u_func
   ;
                         
                         

function XMLAttribute(aNode: IXMLNODE; aName: string): string;
function XMLAttribute_Float(aNode: IXMLNODE; aName: string): double;
function XMLAttribute_Int(aNode: IXMLNODE; aName: string): integer;

function XMLGetNodeText(aNode: IXMLNODE; aName: string): string;
                        
function XMLFindNode(aNode: IXMLNODE; aName: string): IXMLNODE;

function XMLFindNode_ByPath(aNode: IXMLNODE; aPath: array of string): IXMLNODE;
    

                        

                        
implementation

//--------------------------------------------------------
function XMLAttribute(aNode: IXMLNODE; aName: string): string;
//--------------------------------------------------------
var
  v: OleVariant;
begin
  Assert(Assigned(aNode));

  try
    v:=aNode.Attributes[aName];
    if not VarIsNull(v) then
      Result:=v
    else
      Result:='';

  except on E: Exception do
  end;


 

end;

//--------------------------------------------------------
function XMLFindNode(aNode: IXMLNODE; aName: string): IXMLNODE;
//--------------------------------------------------------
var
  m: Integer;
  s: string;
  vNode: IXMLNODE;
begin
//  if aNode then

  Assert(Assigned(aNode));


  vNode:=aNode.ChildNodes.FindNode(aName);


  for m := 0 to aNode.ChildNodes.Count - 1 do
  begin
    s:=aNode.ChildNodes[m].LocalName;
    s:=aNode.ChildNodes[m].Prefix;


    if Eq(aNode.ChildNodes[m].LocalName, aName) then
    begin
      Result:=aNode.ChildNodes[m];
      Exit;
    end;

  end;


  Result:=nil;
end;


//--------------------------------------------------------
function XMLGetNodeText(aNode: IXMLNODE; aName: string): string;
//--------------------------------------------------------
var
 // s: string;
  vNode: IXMLNODE;
begin
  Assert(Assigned(aNode));

  vNode:=aNode.ChildNodes.FindNode(aName);

  if not Assigned(vNode) then
     vNode:= XMLFindNode(aNode,aName);



//  aNode.Prefix

  //s:=aNode.NamespaceURI;

  if Assigned(vnode) then
    Result:=vnode.Text
  else
    Result:='';

end;


function XMLAttribute_Float(aNode: IXMLNODE; aName: string): double;
var
  s: string;
begin
  s:=XMLAttribute(aNode,aName);
  Result := AsFloat(s);
end;


function XMLAttribute_Int(aNode: IXMLNODE; aName: string): integer;
var
  s: string;
begin
  s:=XMLAttribute(aNode,aName);
  Result := StrToIntDef(s,0);
end;


// ---------------------------------------------------------------
function XMLFindNode_ByPath(aNode: IXMLNODE; aPath: array of string): IXMLNODE;
// ---------------------------------------------------------------
var
  I: Integer;
  vNode: IXMLNODE;
begin
  vNode:= aNode;
   
  for I := 0 to High(aPath) do
  begin
    vNode := XMLFindNode(vNode, aPath[i]);
    if not Assigned(vNode) then
      Break;
  end;

  Result:=vNode;
end;




end.
