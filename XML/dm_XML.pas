unit dm_XML;

interface

uses
  VCL.CheckLst, DB,
  System.SysUtils, System.Classes, Xml.xmldom, Xml.XMLIntf, Xml.XMLDoc, Forms,
  Xml.Win.msxmldom
;

type
  TdmXML = class(TDataModule)
    XMLDocument1: TXMLDocument;
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    class function Check_Dataset_to_XML(aDataset: TDataset): string;
    class function GetXMLDocumentRef: TXMLDocument;


  end;

var
  dmXML: TdmXML;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

//--------------------------------------------------------
class function TdmXML.Check_Dataset_to_XML(aDataset: TDataset): string;
//--------------------------------------------------------
var
  I: Integer;
  oDoc: TXMLDocument;
  vNode: IXMLNode;
begin
  oDoc:=TdmXML.GetXMLDocumentRef;

  aDataset.First;
  aDataset.DisableControls;

  with aDataset do
   while not EOF do
   begin
      if FieldByName('checked').AsBoolean then
      begin
        vNode:=oDoc.DocumentElement.AddChild('item');

        vNode.Attributes['id']  := FieldValues['id'];
        vNode.Attributes['name']:= FieldValues['name'];

        vNode.Attributes['checked']  := True;

  //      if FieldByName('enabled').AsBoolean then

    //      vNode.Attributes['checked']:=aCheckListBox.Checked[i];

      end;

      Next;

   end;

  aDataset.First;
  aDataset.EnableControls;


  oDoc.SaveToXML(Result);
end;

////--------------------------------------------------------
//class procedure TdmXML.XML_to_CheckListBox(aCheckListBox: TCheckListBox; aXML:
//    string);
////--------------------------------------------------------
//var
//  bChecked: Boolean;
//  I: Integer;
//  iID: Integer;
//  oDoc: TXMLDocument;
//  sName: string;
//  vNode: IXMLNode;
//begin
//  aCheckListBox.Items.Clear;
//
//  if aXML='' then
//    Exit;
//
//  oDoc:=TdmXML.GetXMLDocumentRef;
//
//  try
//    oDoc.LoadFromXML(aXML);
//
//  except on E: Exception do
//    Exit;
//  end;
//
//
//  for I := 0 to oDoc.DocumentElement.ChildNodes.Count-1 do
//  begin
//    vNode:=oDoc.DocumentElement.ChildNodes[i];
//
//    iID  :=StrToIntDef(vNode.Attributes['id'], 0);
//    sName:=vNode.Attributes['name'];
//    bChecked:=vNode.Attributes['checked']='true';
//
//    aCheckListBox.Items.AddObject(sName, Pointer(iID));
//    aCheckListBox.Checked[i]:=true; //bChecked;
//
//  end;
//
//end;
//


class function TdmXML.GetXMLDocumentRef: TXMLDocument;
begin
  if not Assigned(dmXML) then
    Application.CreateForm(TdmXML, dmXML);

  dmXML.XMLDocument1.DocumentElement.ChildNodes.Clear;

  Result:= dmXML.XMLDocument1;

end;


procedure TdmXML.DataModuleCreate(Sender: TObject);
begin
  XMLDocument1.AddChild('Document');
end;

end.
