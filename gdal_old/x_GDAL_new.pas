unit x_GDAL_new;

interface
uses

// System.SysUtils, //.TEncoding.UTF8

 Vcl.Forms, System.IniFiles,System.SysUtils,  Windows,  Classes, Vcl.Dialogs, 

 codeSiteLogging,
  
 u_GDAL_classes,

 u_run;
 
//  u_dll_GDAL;
  
 // u_GDAL;

type

   TGDAL_XYRect = packed record
                        Lat_min: Double;
                        Lat_max: double;

                        Lon_min: Double;
                        Lon_max: double;

                        Zone: byte;


                        BmpFileName : string;
                        TabFileName : string;

                        //    rec.Transparence_0_255 := g_Params_bin_to_map.Transparence;

                     end;



  procedure gdal_BMP_to_TAB(aBmpFileName: ShortString; aParams: TGDAL_XYRect);
      stdcall;


  procedure TGDAL_XYRect_SaveToFile  (aFileName: String; aParams: TGDAL_XYRect);   
  procedure TGDAL_XYRect_LoadFromFile(aFileName: String; var aParams: TGDAL_XYRect);


//procedure Test_GDAL;


exports     
  gdal_BMP_to_TAB;   


implementation



//------------------------------------------------------
procedure Test_GDAL;
//------------------------------------------------------
var
  sParam: string;
  rec: TGDAL_XYRect;
  sBitmapFile: string;
 
begin

 // xyRect_42:= FValuesMatrix.GetXYBounds;

//  rec.Lat_max:=6262379;
//  rec.Lat_min:=6222479;
//
//  rec.Lon_min:=8410180;
//  rec.Lon_max:=8450080;
//
//  rec.Zone:=8;

                                 
//  sBitmapFile:='C:\Users\Alexey\AppData\Local\RPLS_DB_LINK\SERVER.onega_link.1568\Predictions\LOS\los_142784876.bmp';

  sBitmapFile:='C:\Users\m.garbar\AppData\Local\RPLS_DB_LINK\(local).onega_link_2019.1\Predictions\region.1\500\cell_layer.112\region.kup.bmp';
  
  TGDAL_XYRect_LoadFromFile(sBitmapFile + '.ini', rec);
  
//  Assert (rec.Zone<100);

  gdal_BMP_to_TAB (sBitmapFile, rec);

end;


 {
// ---------------------------------------------------------------
function RunAndWait(const aFileName: String): DWORD;
// ---------------------------------------------------------------
var 
  StartupInfo: TStartupInfo; 
  ProcessInfo: TProcessInformation; 
//  sPath: string;
begin
  Result := STILL_ACTIVE; 

//  sPath:=ExtractFileDir(aFileName);
  
  GetStartupInfo(StartupInfo); 
  
// CreateProcess(NULL, lpszCommandLine, NULL, NULL, FALSE, 
//              CREATE_NO_WINDOW, NULL, NULL, &si, &pi);

  
  if not CreateProcess(nil, PChar(aFileName), nil, nil, IsConsole, 
//    CREATE_NO_WINDOW, nil, PChar(sPath), StartupInfo, ProcessInfo) 
    NORMAL_PRIORITY_CLASS, nil, nil, StartupInfo, ProcessInfo) 
//    NORMAL_PRIORITY_CLASS, nil, PChar(sPath), StartupInfo, ProcessInfo) 
   then
    RaiseLastWin32Error; 
    
  try   
    if WaitForSingleObject(ProcessInfo.hProcess, INFINITE) = WAIT_OBJECT_0  then 
      GetExitCodeProcess(ProcessInfo.hProcess, Result); 
      
  finally 
    CloseHandle(ProcessInfo.hThread); 
    CloseHandle(ProcessInfo.hProcess); 
  end; 

  
//
//  BOOL WINAPI CreateProcess(
//  LPCTSTR lpApplicationName,
//  LPTSTR lpCommandLine,
//  LPSECURITY_ATTRIBUTES lpProcessAttributes,
//  LPSECURITY_ATTRIBUTES lpThreadAttributes,
//  BOOL bInheritHandles,
//  DWORD dwCreationFlags,
//  LPVOID lpEnvironment,
//  LPCTSTR lpCurrentDirectory,
//  LPSTARTUPINFO lpStartupInfo,
//  LPPROCESS_INFORMATION lpProcessInformation
//);
//


  
end; 

 }

  
// ---------------------------------------------------------------
procedure gdal_BMP_to_TAB(aBmpFileName: ShortString; aParams: TGDAL_XYRect);
// ---------------------------------------------------------------
var
  k: Integer;
  oBAT: TStringList;
  oIni: TIniFile;

  rGDAL_json: TGDAL_json;
  s: string;
  sFile: string;
  sFile_no_ext: string;
  sFile_bat: string;
  sParam: string;
  sPath: string;
  sPath_bin: string;
  
  sProj4: string;
  
begin




  Assert (aParams.Lat_min <> aParams.Lat_max);
  Assert (aParams.Lon_min <> aParams.Lon_max);

  // -------------------------------------------------
  
  sFile_bat:=ChangeFileExt(aBmpFileName, '.bat');


  oBAT:=TStringList.Create;


  sFile:=ExtractFilePath(Application.ExeName) + 'gdal.ini';
  if not FileExists(sFile) then
    ShowMessage('FileExists: ' + sFile);
    
  Assert(FileExists(sFile), sFile);

  // ---------------------------------------------------------------
  
  oIni:=TIniFile.Create(sFile);
  sPath:=oIni.ReadString('main','path','');
  FreeAndNil(oIni);
  
  // ---------------------------------------------------------------
  sPath:=ExtractFilePath(Application.ExeName) + IncludeTrailingBackslash(sPath);
  sPath_bin:=sPath + 'bin\';


 // ShowMessage(sPath_bin);
  
  



//  sPath_bin:='D:\OSGeo4W\bin\';
//
//set path=W:\GIS\GeoConverter_XE\bin\OSGeo4W\bin\;P:\_megafon\test_Altai\Clutter\
//set GDAL_DATA=W:\GIS\GeoConverter_XE\bin\OSGeo4W\share\epsg_csv
//set GDAL_FILENAME_IS_UTF8=NO
//del all_*
//del *.envi
//del *.tif
//del *.json


  oBAT.Add('set path='+ sPath_bin);

 
  s:=Format('set GDAL_DATA=%s',[sPath])+ 'share\epsg_csv' ;      
  oBAT.Add(s);

  oBAT.Add('set GDAL_FILENAME_IS_UTF8=NO');

  
//  oBAT.Add('set GDAL_DATA=D:\OSGeo4W\share\epsg_csv');

//  oBAT.Add('del *.tif');
//  oBAT.Add('del *.json');
//  oBAT.Add('del *.tab');


 // aBmpFileName:=ExtractFileName(aBmpFileName);
                            
 // ShowMessage(aBmpFileName);     sPath_bin 
  
  
//  sFile:=ChangeFileExt(ExtractFileName(aBmpFileName), '');
  sFile_no_ext:=ChangeFileExt(aBmpFileName, '');


  s:= ExtractFileName(aBmpFileName);
  s:= ChangeFileExt(ExtractFileName(aBmpFileName),'.tif');



 Assert(aParams.Zone<100 );
  
  //     -projwin_srs

//
//const
//  ZONE_INFO_ARR : array[4..29] of string =
//    (
//      '21, 0, 1,  4500000, 0',  //5
//      '27, 0, 1,  5500000, 0',  //5
//      '33, 0, 1,  6500000, 0',  //6
//      '39, 0, 1,  7500000, 0',  //7
//      '45, 0, 1,  8500000, 0',  //8
//      '51, 0, 1,  9500000, 0',  //9
//      '57, 0, 1, 10500000, 0',  //10
//      '63, 0, 1, 11500000, 0',  //11
//
  
//EPSG:28420
//Pulkovo 1942 / Gauss-Kruger zone 20  

//  +proj=tmerc +lat_0=0 +lon_0=117 +k=1 +x_0=20500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs 
  
//  oBAT.Add(Format('gdalwarp.exe -overwrite -t_srs  "%s"  all.tif all_proj_GK_.tif',[sProj4]));
//  oBAT.Add (      'gdalinfo.exe -json all_proj_GK_.tif > all_proj_GK_.json');
  
  
  sProj4 := Format('+proj=tmerc +lat_0=0 +lon_0=%d +k=1 +x_0=%d +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs',
             [ aParams.Zone * 6 -3,  aParams.Zone * 1000000 + 500000]);
//             [ 20 * 6 -3,  20 * 1000000 + 500000]);
  
  oBAT.Add('rem zone : ' + IntToStr( aParams.Zone));
                          
//  s:= Format('gdal_translate  -projwin_srs "%s"  -of GTiff  -co "INTERLEAVE=PIXEL" -a_ullr  %d  %d %d %d  "%s" "%s" ',
  sParam:= Format('gdal_translate  -a_srs "%s"  -of GTiff  -co "INTERLEAVE=PIXEL" -a_ullr  %d  %d %d %d  "%s" "%s" ',
//  sParam:= Format('gdal_translate  -a_srs EPSG:%d  -of GTiff  -co "INTERLEAVE=PIXEL" -a_ullr  %d  %d %d %d  "%s" "%s" ',
                               
                           [  sProj4,

                          //    28400 + aParams.Zone,
                                                          
                              Round(aParams.Lon_min), 
                              Round(aParams.Lat_max), 

                              Round(aParams.Lon_max),
                              Round(aParams.Lat_min),
                               
                              ExtractFileName( aBmpFileName),
                              ExtractFileName( ChangeFileExt(aBmpFileName,'.tif') )  
                             ]);
  
                                  
//  codeSite.SendMsg(sParam);
                
  oBAT.Add(sParam);

  s:=  Format('gdalinfo -json "%s.tif" >  "%s.json" ',[ExtractFileName(sFile_no_ext), ExtractFileName(sFile_no_ext)]);
//  s:=  Format('gdalinfo -json "%s.3395.tif" >  "%s.3395.json" ',[sFile_no_ext,sFile_no_ext]);
  oBAT.Add(s);
  
  
  //gdal_translate -a_srs EPSG:28408    nnovg_pl_50_2D.envi nnovg_pl_50_2D.tif
//  s:= sPath_bin+ Format('gdal_translate.exe -a_srs EPSG:%d  "%s.envi" "%s.tif"',[28400+ FRelMatrix.ZoneNum,sFile,sFile]);
//  oBAT.Add(s);

  s:= Format('gdalwarp -t_srs EPSG:3395 "%s.tif"  "%s.3395.tif" ', [ExtractFileName(sFile_no_ext), ExtractFileName( sFile_no_ext) ]);
//  s:= Format('gdalwarp -t_srs EPSG:3395 "%s.tif"  "%s.3395.tif" ', [sFile_no_ext, sFile_no_ext]);

  
//  s:= '"' + sPath_bin + Format('gdalwarp" -t_srs EPSG:3395 "%s.tif"  "%s.3395.tif" ',[sFile_no_ext,sFile_no_ext]);
  oBAT.Add(s);
                   
  

  s:=  Format('gdalinfo -json "%s.3395.tif" >  "%s.3395.json" ',[ExtractFileName(sFile_no_ext), ExtractFileName(sFile_no_ext)]);
//  s:=  Format('gdalinfo -json "%s.3395.tif" >  "%s.3395.json" ',[sFile_no_ext,sFile_no_ext]);
  oBAT.Add(s);

  
  ForceDirectories ( ExtractFilePath(sFile_bat ));
  
  
  //TEncoding.GetEncoding(CP_866));
  
//  oBAT.SaveToFile(sFile_bat, TEncoding.UTF8);
  oBAT.SaveToFile(sFile_bat, TEncoding.GetEncoding(866));
  
  FreeAndNil(oBAT);
  
  // ---------------------------------------------------------------

 // ShowMessage('3');  



// function RunApp(const aFileName: String; const aParamStr: String = ''): DWORD;

 
//  RunApp(sFile_bat, '');
 
 
  SetCurrentDir( ExtractFileDir ( sFile_bat ));
 
  
  ShellExecute_AndWait (sFile_bat, '');
//  RunAndWait_2 (sFile_bat);
  
  
 // k:=RunApp(sFile_bat);
                  

  s:=ChangeFileExt(aBmpFileName, '.3395.json');  
  Assert (FileExists (s));
                                               

  // ---------------------------------------------------------------
  rGDAL_json.LoadFromFile(ChangeFileExt(aBmpFileName, '.3395.json'));
  rGDAL_json.SaveToFile_TAB_3395(ChangeFileExt(aBmpFileName, '.tab'),  ChangeFileExt(aBmpFileName, '.3395.tif'));  
     
  

  

  
end;

// ---------------------------------------------------------------
procedure TGDAL_XYRect_SaveToFile(aFileName: String; aParams: TGDAL_XYRect);
// --------------------------------------------------------------- 

var
  oIni: TIniFile;
begin
 // aFileName:=ChangeFileExt(aFileName, '.ini');


  oIni:=TIniFile.Create (aFileName);
                 
  oIni.WriteFloat('main', 'Lat_min', aParams.Lat_min);
  oIni.WriteFloat('main', 'Lat_max', aParams.Lat_max);
 
  oIni.WriteFloat('main', 'Lon_min', aParams.Lon_min);
  oIni.WriteFloat('main', 'Lon_max', aParams.Lon_max);

  oIni.WriteInteger('main', 'Zone', aParams.Zone);

  oIni.WriteString('main', 'BmpFileName', aParams.BmpFileName);
  oIni.WriteString('main', 'TabFileName', aParams.TabFileName);

  
  
  FreeAndNil(oIni);
   
end;



// --------------------------------------------------------------- 
procedure TGDAL_XYRect_LoadFromFile(aFileName: String; var aParams:  TGDAL_XYRect);
// ---------------------------------------------------------------

var
  oIni: TIniFile;
begin
//  aFileName:=ChangeFileExt(aFileName, '.ini');

  Assert (FileExists(aFileName));

  oIni:=TIniFile.Create (aFileName);
                 
  aParams.Lat_min:=oIni.ReadFloat('main', 'Lat_min',0);
  aParams.Lat_max:=oIni.ReadFloat('main', 'Lat_max',0);
 
  aParams.Lon_min:=oIni.ReadFloat('main', 'Lon_min',0);
  aParams.Lon_max:=oIni.ReadFloat('main', 'Lon_max',0);

  aParams.Zone:=oIni.ReadInteger('main', 'Zone', 0);

  aParams.BmpFileName:=oIni.ReadString('main', 'BmpFileName', '');
  aParams.TabFileName:=oIni.ReadString('main', 'TabFileName', '');

  
  
  FreeAndNil(oIni);
   
end;



end.
