
unit u_neva_Classes;

interface
uses Classes,Graphics,SysUtils,
     oTypes,
     dmw_use,
     u_neva_Func,
     u_Geo,
     u_progress,
     u_func;


type

  //--------------------------------------------
  //����� (��������) ���� �������� �����
  //--------------------------------------------
  neva_TStyle = record
  //--------------------------------------------
    PenType   : (ptSingle,ptDouble); // Single,  // single, double
    WidthType : (wtMM,wtPixel); // in mm or pixels

    Width,
    PenColor,PenPattern,BrushColor, BrushStyle: integer;
    Caption: string; // text label
  end;

  //--------------------------------------------
  Tneva_ObjectRec = record
  //--------------------------------------------
     Offset,Code,Local: integer;
     ParentOffset: integer;

     Name     : string; // �������� ������� �� ��������������
     Caption  : string; // ����������� �������� �������
     Height   : double; // ������ ������� � ������

     Address: record
              House: string;
              SubHouse: string;
              Korpus: string;
            end;

     Visible  : boolean;
     Expanded : boolean;

     NevaStyle: neva_TStyle;
     BLPoints : TBLPointArray;
     BLBounds : TBLRect;

     XYBounds : TXYRect;

     IsRootArea: boolean; //��� ��������� �������� - ������� �������� ��������
  end;

  neva_TOnNodeFoundEvent = procedure
            (var aObjectRec: Tneva_ObjectRec) of object;

  //--------------------------------------------
  // �������� ���� ����� ����� DM
  //--------------------------------------------
  Tneva_NodesReview = class (TProgress)
  //--------------------------------------------
  private
    FOnNodeFound  : neva_TOnNodeFoundEvent;
    FOnRegionStart: TNotifyEvent;
    FOnRegionEnd  : TNotifyEvent;

    FState        : (stNone,stRegion);
    FMainRegionOffset : integer;

    FTotal   : integer; //���-�� ��������
    FProgress: integer;

    FNevaObjectRec: Tneva_ObjectRec;

    procedure DoProcessNodes;
    procedure DoProcessLayers ();

  public
    Params: record
      BLBounds  : TBLRect;
      DmFileName: string;

      //flSimplePoly - �� ��������� ������ �������� (������ � ����)
      Flags     : set of (flSimplePoly);
    end;

    procedure ExecuteProc; override;
    function ExtractNode(var aObjectRec: Tneva_ObjectRec): Boolean;

    property OnNodeFound  : neva_TOnNodeFoundEvent read FOnNodeFound write FOnNodeFound;
    property OnRegionStart: TNotifyEvent read FOnRegionStart write FOnRegionStart;
    property OnRegionEnd  : TNotifyEvent read FOnRegionEnd write FOnRegionEnd;
  end;


  //--------------------------------------------
  // ���������� ������ ����� ����� DM
  //--------------------------------------------
  neva_TDmTree = class (TProgress)
  //--------------------------------------------
  private
    FOnNodeFound  : neva_TOnNodeFoundEvent;
  public
    MaxLevel: integer; //���-�� ��������

    Total,Progress: integer; //���-�� ��������

    procedure Execute (aMapFileName: string);
    procedure ProcessNodes (aParentOffset: integer; aLevel: integer=0);

    property OnNodeFound: neva_TOnNodeFoundEvent read FOnNodeFound write FOnNodeFound;
  end;


//====================================================
implementation
//====================================================

//-------------------------------------------------
function neva_Extract_Brush_Style (aColor: integer): integer;
//-------------------------------------------------
var br: integer;
begin
  br:=(aColor shr 10 );
  Result:=(63 and br);
end;

//-------------------------------------------------
function neva_DmColorIndToColor (aColorIndex: integer): integer;
//-------------------------------------------------
var cl:integer;
begin
  case aColorIndex of
     0: cl:=clBlack;    1: cl:=clNavy;     2: cl:=clGreen;   3: cl:=clTeal ;
     4: cl:=clMaroon;   5: cl:=clPurple;   6: cl:=clOlive;   7: cl:=clSilver;
     8: cl:=clGray;     9: cl:=clBlue;    10: cl:=clLime;   11: cl:=clAqua;
    12: cl:=clRed;     13: cl:=clFuchsia; 14: cl:=clYellow; 15: cl:=clWhite;
    16: cl:=$004080FF; 17: cl:=clYellow;  20: cl:=clAqua;

    23: cl:=clGray;
    29: cl:=clGreen;
  else
    cl:=clWhite;
  end;
  Result:=cl;
end;


//-------------------------------------------------
// neva_TNodesReview
//-------------------------------------------------


// ---------------------------------------------------------------
function Tneva_NodesReview.ExtractNode(var aObjectRec: Tneva_ObjectRec):  Boolean;
// ---------------------------------------------------------------

  function IsNodeHasAreaChildren (): boolean;
   begin
     Result:=False;
     if dm_Goto_down then
     begin
       while dm_Goto_right do
         if (neva_LOCAL_AREA = dm_Get_Local) then begin
           Result:=True;
           Break;
         end; //iLocal;

       dm_Goto_upper;
     end;

   end;

var
  k,iCode,iOffset,iOldOffset,iLocal,iColorInd,iColor: integer;
  str: string;
  blPoints: TBLPointArray;
  mm,tag,width,style: byte;
  fc,pc: byte;
  iByte: byte;

  obj_name: TShortStr;
  pName : PChar;
  sName: string;
  vValue: Variant;
  rNevaStyle: neva_TStyle;
  xyBounds: TXYRect;

  //cl,flags,i: integer;
  cl,flags : integer;

  bChildren,bIsRootArea: boolean;
  iChildren: integer;
begin
  FillChar (aObjectRec, SizeOf(aObjectRec), 0);


  iCode  :=dm_Get_Code;
  iLocal :=dm_Get_Local;
  iOffset:=dm_Object;


  aObjectRec.Offset  :=iOffset;
  aObjectRec.Code    :=iCode;
  aObjectRec.Local   :=iLocal;
  aObjectRec.IsRootArea:=False;

  flags:=dm_Get_Flags;


  if (iCode>0) then
  begin


    iColorInd :=dm_Get_Color;

    pName:=obj_Get_Name (iCode, iLocal, obj_name);
    sName:=pName;

    if neva_Get_Sem_Value (1, vValue, varSingle) then aObjectRec.Height:=vValue;
    if neva_Get_Sem_Value (9, vValue, varString) then aObjectRec.Caption:=vValue;

    if neva_Get_Sem_Value (100, vValue, varString) then begin
      aObjectRec.Address.House:=vValue;
      if neva_Get_Sem_Value (106, vValue, varString) then aObjectRec.Address.SubHouse:=vValue;
      if neva_Get_Sem_Value (107, vValue, varString) then aObjectRec.Address.Korpus:=vValue;
    end;


    case iLocal of

      neva_LOCAL_SIGN: ;

      //----------------------------------------------------
      neva_LOCAL_TEXT:
      //----------------------------------------------------
      begin
        // get text label
        if aObjectRec.Caption <> '' then begin
          rNevaStyle.Caption:=vValue;
          rNevaStyle.PenColor:=neva_DmColorIndToColor (iColorInd);
        end;
      end;

      //----------------------------------------------------
      neva_LOCAL_LINE:
      //----------------------------------------------------
      begin
        dm_Get_Pen  (mm,tag,width,style);
        dm_Get_Brush (fc,pc);

        rNevaStyle.PenColor  :=neva_DmColorIndToColor (fc);
        rNevaStyle.BrushColor:=neva_DmColorIndToColor (fc);
        rNevaStyle.Width     :=width;
        rNevaStyle.WidthType :=wtMM;

        rNevaStyle.PenType   :=IIF(tag=0, ptSingle, ptDouble);

        rNevaStyle.PenPattern:=style;
        FillChar (xyBounds, SizeOF(xyBounds), 0);
      end;

      //----------------------------------------------------
      neva_LOCAL_AREA:
      //----------------------------------------------------
      begin
        dm_Get_Brush(fc,pc);
        rNevaStyle.BrushColor:=neva_DmColorIndToColor (fc);
        rNevaStyle.PenColor  :=neva_DmColorIndToColor (pc);
        rNevaStyle.Width     :=width;
        rNevaStyle.BrushStyle:=style;
        cl:=dmw_Get_Color();
        rNevaStyle.BrushStyle:=neva_Extract_Brush_Style(cl);

        neva_Get_Object_Bounds (xyBounds);
      end;
    end;

    neva_GetObjectBLPoints (blPoints);

    iChildren:=dm_Is_Childs (iOffset);
    bChildren:=iChildren > 0;

    bIsRootArea:=(bChildren) and (iLocal=neva_LOCAL_AREA);
    if bIsRootArea then
      bIsRootArea:=IsNodeHasAreaChildren();

  //  if Assigned(FOnNodeFound) then
   // begin
     {aObjectRec.Offset  :=iOffset;
     aObjectRec.Code    :=iCode;
     aObjectRec.Local   :=iLocal;}
    aObjectRec.Name    :=sName;
    aObjectRec.NevaStyle:=rNevaStyle;
    aObjectRec.BLPoints:=blPoints;

    aObjectRec.XYBounds:=xyBounds;

    aObjectRec.BLBounds:=geo_DefineRoundBLRect (blPoints);

    aObjectRec.Height:=Round(aObjectRec.Height);

     //���������, ���� �� ������� ���� �������
    aObjectRec.IsRootArea:=bIsRootArea; //(bChildren) and (iLocal=neva_LOCAL_AREA);

    //  FOnNodeFound (rObj);
  //  end;

    result := True;
  end else
    result := False;

end;


//-------------------------------------------------
procedure Tneva_NodesReview.DoProcessNodes(); // (aParentOffset: integer);
//-------------------------------------------------

var
 // rObj: Tneva_ObjectRec;

  bIsRootArea: boolean;
  iOffset: integer;

begin
   repeat
     Inc(FProgress);

     DoProgress (FProgress,FTotal);

//     if Assigned(FOnProgress) then  FOnProgress (FProgress,FTotal, Terminated);
     if Terminated then Break;


     if ExtractNode (FNevaObjectRec {rObj}) then
     begin
       if Assigned(FOnNodeFound) then
         FOnNodeFound (FNevaObjectRec {rObj});
     end;

  {   bIsRootArea:=rObj.IsRootArea;
     iOffset:=rObj.Offset;
}
     if dm_Goto_down then
     begin
       bIsRootArea  :=FNevaObjectRec.IsRootArea;// rObj.IsRootArea;
       iOffset      :=FNevaObjectRec.Offset; // rObj.Offset;


       if bIsRootArea and Assigned(FOnRegionStart) then
         FOnRegionStart(nil);

       DoProcessNodes ();

       if bIsRootArea and Assigned(FOnRegionEnd) then
         FOnRegionEnd(nil);


        dm_Goto_node (iOffset); // goto old parent node
     end;



{

     iCode  :=dm_Get_Code;
     iLocal :=dm_Get_Local;
     iOffset:=dm_Object;

     flags:=dm_Get_Flags;

     if (iCode>0) then
     begin
       FillChar (rObj, SizeOf(rObj), 0);

       iColorInd :=dm_Get_Color;

       pName:=obj_Get_Name (iCode, iLocal, obj_name);
       sName:=pName;

       if neva_Get_Sem_Value (1, vValue, varSingle) then rObj.Height:=vValue;
       if neva_Get_Sem_Value (9, vValue, varString) then rObj.Caption:=vValue;

       if neva_Get_Sem_Value (100, vValue, varString) then begin
         rObj.Address.House:=vValue;
         if neva_Get_Sem_Value (106, vValue, varString) then rObj.Address.SubHouse:=vValue;
         if neva_Get_Sem_Value (107, vValue, varString) then rObj.Address.Korpus:=vValue;
       end;


       case iLocal of

         neva_LOCAL_SIGN: ;

         //----------------------------------------------------
         neva_LOCAL_TEXT:
         //----------------------------------------------------
         begin
           // get text label
           if rObj.Caption <> '' then begin
             rNevaStyle.Caption:=vValue;
             rNevaStyle.PenColor:=neva_DmColorIndToColor (iColorInd);
           end;
         end;

         //----------------------------------------------------
         neva_LOCAL_LINE:
         //----------------------------------------------------
         begin
           dm_Get_Pen  (mm,tag,width,style);
           dm_Get_Brush (fc,pc);

           rNevaStyle.PenColor  :=neva_DmColorIndToColor (fc);
           rNevaStyle.BrushColor:=neva_DmColorIndToColor (fc);
           rNevaStyle.Width     :=width;
           rNevaStyle.WidthType :=wtMM;

           rNevaStyle.PenType   :=IIF(tag=0, ptSingle, ptDouble);

           rNevaStyle.PenPattern:=style;
           FillChar (xyBounds, SizeOF(xyBounds), 0);
         end;

         //----------------------------------------------------
         neva_LOCAL_AREA:
         //----------------------------------------------------
         begin
           dm_Get_Brush(fc,pc);
           rNevaStyle.BrushColor:=neva_DmColorIndToColor (fc);
           rNevaStyle.PenColor  :=neva_DmColorIndToColor (pc);
           rNevaStyle.Width     :=width;
           rNevaStyle.BrushStyle:=style;
           cl:=dmw_Get_Color();
           rNevaStyle.BrushStyle:=neva_Extract_Brush_Style(cl);

           neva_Get_Object_Bounds (xyBounds);
         end;
       end;

       neva_GetObjectBLPoints (blPoints);

       iChildren:=dm_Is_Childs (iOffset);
       bChildren:=iChildren > 0;

       bIsRootArea:=(bChildren) and (iLocal=neva_LOCAL_AREA);
       if bIsRootArea then
         bIsRootArea:=IsNodeHasAreaChildren();

       if Assigned(FOnNodeFound) then
       begin
         rObj.Offset  :=iOffset;
         rObj.Code    :=iCode;
         rObj.Local   :=iLocal;
         rObj.Name    :=sName;
         rObj.NevaStyle:=rNevaStyle;
         rObj.BLPoints:=blPoints;

         rObj.XYBounds:=xyBounds;

         rObj.BLBounds:=geo_DefineRoundBLRect (blPoints);

         rObj.Height:=Round(rObj.Height);

         //���������, ���� �� ������� ���� �������
         rObj.IsRootArea:=bIsRootArea; //(bChildren) and (iLocal=neva_LOCAL_AREA);

         FOnNodeFound (rObj);
       end;

     end;

}
    { if dm_Goto_down then
     begin
        if bIsRootArea and Assigned(FOnRegionStart) then
          FOnRegionStart(nil);

        DoProcessNodes ();

        if bIsRootArea and Assigned(FOnRegionEnd) then
          FOnRegionEnd(nil);

         iOffset:=rObj.Offset;
         dm_Goto_node (iOffset); // goto old parent node
     end;
}
   until (not dm_Goto_Right);
end;

//-------------------------------------------------
procedure Tneva_NodesReview.DoProcessLayers ();
//-------------------------------------------------
var i,iCode,iOffset,iLocal: integer;   str: string;
// bFound: boolean;
begin
   dm_Goto_Root;
   if not dm_Goto_down then Exit;

   // ������ ��� �������� 2 ������
   repeat
     if Terminated then Exit;

     iCode  :=dm_Get_Code;
     iLocal :=dm_Get_Local;
     iOffset:=dm_Object;

    // flags:=dm_Get_Flags;

     if (iLocal = neva_LOCAL_MENU) then
     begin
       {
       bFound:=false;
       for i:=0 to High(Params.MaskArr) do
         if (Params.MaskArr[i]=0) or
          ((Params.MaskArr[i]>0) and (iCode div 10000000 = Params.MaskArr[i]))
         then begin bFound:=true; Break; end;
       }

       if dm_Goto_down then begin
          DoProcessNodes();

          dm_Goto_node (iOffset);
       end;
     end;

   until (not dm_Goto_Right);

end;

//----------------------------------------------------
procedure Tneva_NodesReview.ExecuteProc;
//----------------------------------------------------
begin
  if neva_Open_Map(Params.DmFileName, fmOpenRead) then
  begin
    FTotal:=dm_Objects_Count;
    FProgress:=0;

    DoProcessLayers ();
    neva_Close_Map();
  end;
end;

//-------------------------------------------------
procedure neva_TDmTree.ProcessNodes (aParentOffset: integer; aLevel: integer=0);
//-------------------------------------------------
  procedure DoProgress();
  begin
//     Inc(Progress);
     Inc(Total);
    { if Assigned(FOnProgress) then  FOnProgress (Progress,Total);
    }
    // if Progress > Total then
      //  Inc(Progress);
  end;

var k,iCode,iOffset,iLocal: integer;  vValue: Variant;
//  flags: integer;

  objRec: Tneva_ObjectRec;
begin
   if (MaxLevel > 0) and (aLevel > MaxLevel) then Exit;

   if aParentOffset = 0 then begin
     dm_Goto_Root;
     if not dm_Goto_down then Exit;
   end;

   repeat
     iCode  :=dm_Get_Code;
     iOffset:=dm_Object;

           {
     if iOffset = 364 then
       iOffset:=iOffset;
      }
     if (iCode>0) then
     begin
       if Assigned(FOnNodeFound) then
       begin
         objRec.Code  :=dm_Get_Code;
         objRec.Offset:=dm_Object; //iOffset;
         objRec.Local :=dm_Get_Local; //iLocal;
         objRec.ParentOffset:=aParentOffset;

         neva_Get_Sem_Value (SEM_NAME, vValue, varString);
         objRec.Caption:=vValue;

         neva_Get_Sem_Value (SEM_EXPANDED, vValue, varBoolean);
         objRec.Expanded:=vValue;

         objRec.Visible:=not (dm_Get_Flags in [16]);

         FOnNodeFound (objRec);

         DoProgress();
       end;
     end;

   until (not dm_Goto_Right) or (Terminated);


   repeat
     if Terminated then Exit;

     iOffset:=dm_Object;
     if dm_Goto_down then
        ProcessNodes (iOffset,aLevel+1);

   until (not dm_Goto_Left);


   if aParentOffset > 0 then
     neva_Goto_node (aParentOffset);
end;

//----------------------------------------------------
procedure neva_TDmTree.Execute (aMapFileName: string);
//----------------------------------------------------
begin

  if neva_Open_Map(aMapFileName, fmOpenRead) then
  begin
  //  Total:=dm_Objects_Count;
    MaxLevel:=0; //4;

    ProcessNodes (0);

    neva_Close_Map();
  end;
end;

{

procedure Proc (aObjectInfo: Tneva_ObjectRec);
begin
  aObjectInfo.Code:=1;
  //
end;}

procedure Test;
var
  obj: Tneva_NodesReview;

begin
  obj:=Tneva_NodesReview.Create;
  obj.Params.DMFileName:='u:\1.dm';

{  obj.Params.LabelsFileName:='t:\belarus_labels.dm';
  obj.Params.ObjFileName:='m:\neva\obj\100rpls.obj';
  obj.Params.LabelCode:=90100010;
}
  obj.ExecuteProc();
  obj.Free;

end;



begin
  {Test;
  Test;
  Test;
  Test;
  Test;
  Test;
  Test;}


{
  obj:=TdmLabelMaker.Create;
  obj.Params.DMFileName:='t:\belarus_Ok.dm';
  obj.Params.LabelsFileName:='t:\belarus_labels.dm';
  obj.Params.ObjFileName:='m:\neva\obj\100rpls.obj';
  obj.Params.LabelCode:=90100010;

  obj.Execute();
  obj.Free;
  }
//  obj:=Tneva_DmTree.Create;
//  obj.Execute ('t:\project.dm');

end.
