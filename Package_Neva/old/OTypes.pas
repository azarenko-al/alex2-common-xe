unit OTypes; interface

uses
  Windows,Classes,SysUtils,Math;

const
  rus_interface: Boolean = true;
  dos_interface: Boolean = true;
  drag_Enabled: Boolean = true;

  ClsId_Null: TGUID = '{00000000-0000-0000-0000-000000000000}';

  sPlus: array[Boolean] of char = '-+';

  ClientCount: Integer = 0;

  LPoly_Max = 127;
  fsNumStr = 16;

  Small = 1E-6;

  ORient_Max = 6;

type
  float = single;

  alfa = array[1..8] of char;
  alpha = array[1..16] of char;
  char2 = array[1..2] of char;
  char3 = array[1..3] of char;
  char4 = array[1..4] of char;
  char5 = array[1..5] of char;
  char7 = array[1..7] of char;

  PBools = ^TBools;
  TBools = array[0..63] of Boolean;

  PBytes = ^TBytes;
  TBytes = array[0..1023] of Byte;

  TByte8 = array[0..7] of byte;

  PWords = ^TWords;
  TWords = array[0..1023] of word;

  PIntegers = ^TIntegers;
  TIntegers = array[0..1023] of Integer;

  PInt64s = ^TInt64s;
  TInt64s = array[0..1023] of Int64;

  PHeights = ^THeights;
  THeights = array[0..LPoly_Max] of Integer;

  PSmallInts = ^TSmallInts;
  TSmallInts = array[0..1023] of SmallInt;

  PLongBuf = ^TLongBuf;
  TLongBuf = array[0..4096] of byte;

  TShortStr = array[0..255] of Char;
  TWideStr = array[0..255] of WideChar;
  TCmdStr = array[0..1023] of Char;

  PLongStr = ^TLongStr;
  TLongStr = array[0..8191] of Char;

  PPointers = ^TPointers;
  TPointers = array[0..511] of PBytes;

  TMapView = record
    h: THandle; p: PBytes;
    size: Int64;
  end;

  PFloats = ^TFloats;
  TFloats = array[0..1023] of float;

  PDoubles = ^TDoubles;
  TDoubles = array[0..1023] of double;

  LPoint = TPoint;

  wpoint = record x,y: Word end;
  ipoint = record x,y: SmallInt end;
  tgauss = record x,y: Double end;
  tgeoid = record b,l: Double end;
  vpoint = record x,y,z: Integer end;

  PDatum = ^TDatum; TDatum = vpoint;

  txyz = record x,y,z: Double end;

  xpoint = record
    p: lpoint; g: tgauss
  end;

  tsys = record
    pps,prj,elp: Integer;
    b1,b2,lc: Double;
    dat: TDatum;
  end;

  xgeoid = record
    x,y: Double; s: tsys
  end;

  lxyz = record case integer of
0: (p: LPoint); 1: (v: VPoint)
  end;

  LRect = record case integer of
0: (Left,Top,Right,Bottom: Integer);
1: (lt,rb: LPoint); 
  end;

  TVector = array[0..1] of tpoint;
  LVector = array[0..1] of lpoint;
  GVector = array[0..1] of tgauss;
  VVector = array[0..1] of vpoint;

  tdouble = record case integer of
0: (d: double);
1: (f: single);
2: (l: longint);
3: (i: SmallInt);
4: (x: Int64);
5: (w: word);
6: (b: byte);
  end;

  tint64 = record case integer of
0: (x: int64);
1: (c: array[0..1] of Cardinal);
2: (i: array[0..1] of Integer);
3: (w: array[0..3] of word);
4: (s: array[0..3] of SmallInt);
5: (b: array[0..7] of byte);
6: (w1: word; c1: Cardinal; w2: word);
7: (id,cn: Cardinal);
  end;

  tlong = record case integer of
0: (i: longint);
1: (p: Pointer);
2: (c: Cardinal);
3: (w: array[0..1] of word);
4: (b: array[0..3] of byte);
5: (r: single)
  end;

  tword = record case integer of
0:  (b: array[0..1] of byte);
1:  (w: word)
  end;

  TString = record case integer of
0: (x: TWideStr);
1: (s: ShortString);
2: (ci: TShortStr);
3: (d: double);
4: (f: single);
5: (l: longint);
6: (i: SmallInt);
7: (w: word);
8: (b: byte);
  end;

  TOrient = array[0..Orient_Max-1] of tpoint;
  WOrient = array[0..Orient_Max-1] of wpoint;
  LOrient = array[0..Orient_Max-1] of lpoint;
  GOrient = array[0..Orient_Max-1] of tgauss;
  VOrient = array[0..Orient_Max-1] of vpoint;

  PPoly = ^TPoly;
  TPoly = array[0..LPoly_Max] of TPoint;

  PWPoly = ^TWPoly;
  TWPoly = array[0..LPoly_Max] of wpoint;

  PIPoly = ^TIPoly;
  TIPoly = array[0..LPoly_Max] of IPoint;

  PLPoly = PPoly;
  TLPoly = array[0..LPoly_Max] of LPoint;

  PGPoly = ^TGPoly;
  TGPoly = array[0..LPoly_Max] of TGauss;

  PVPoly = ^TVPoly;
  TVPoly = array[0..LPoly_Max] of VPoint;

  PLine = ^TLine;
  TLine = record N: Integer; Pol: TPoly end;

  PWLine = ^TWLine;
  TWLine = record N: Integer; Pol: TWPoly end;

  PLLine = ^TLLine;
  TLLine = packed record N: SmallInt; Pol: TLPoly end;
  VLLine = packed record N: SmallInt; Pol: LOrient end;

  PPolyLine = ^TPolyLine;
  TPolyLine = record N: Integer; Pol: TLPoly end;

  PVLine = ^TVLine;
  TVLine = record N: Integer; Pol: TVPoly end;
  TVBound = record N: Integer; Pol: VOrient end;

  PRealVec = ^TRealVec;
  TRealVec = array[0..1023] of float;

  TNumStr = array[0..fsNumStr-1] of char;
  PNumStr = ^TNumStr;

  pChannel = ^tChannel;
  tChannel = array[0..255] of byte;

  phChannel = ^thChannel;
  thChannel = array[0..15] of byte;

  pIndex = ^tIndex;
  tIndex = array[0..4095] of longint;

  tRGBQuads = array[0..255] of tRGBQuad;
  pRGBQuads = ^tRGBQuads;

  tRGB_Rec = record r,g,b: byte end;
  tRGB_Tbl = array[0..255] of tRGB_Rec;
  pRGB_Tbl = ^tRGB_Tbl;

  TColors = array[0..255] of TColorRef;
  PColors = ^TColors;

  THexColors = array[0..15] of TColorRef;
  PHexColors = ^THexColors;

  tDmwColors = array[0..31] of tColorRef;
  pDmwColors = ^tDmwColors;

  pPaletteChanel = ^tPaletteChanel;
  tPaletteChanel = array[0..255] of word;

  PFillMask = ^TFillMask;
  TFillMask = array[0..15] of Word;

  PFillMasks = ^TFillMasks;
  TFillMasks = array[0..63] of TFillMask;

  TRGB_hist = array[0..255] of Longint;

  TMixedColor = record cl,lo,hi: TColorRef end;

  PMixedColors = ^TMixedColors;
  TMixedColors = array[0..63] of TMixedColor;

  TEventProc = procedure of object;

  TLLineProc = procedure(lp: PLLine) of object;
  TLLineFunc = function(lp: PLLine): Integer of object;

  TPixelProc = procedure(x,y: Integer) of object;

  TTriWord = array[0..2] of word;
  TTriWords = array[0..1023] of TTriWord;
  PTriWords = ^TTriWords;

  TCmyk = record c,m,y,k: Double end;

const
  gsSolid = 0;
  gsHorz  = 1;
  gsVert  = 2;
  gsBack  = 3;
  gsForw  = 4;
  gsGrid  = 5;
  gsCross = 6;

  HexBit: array[0..15] of Word =
  ($8000,$4000,$2000,$1000,$0800,$0400,$0200,$0100,
   $0080,$0040,$0020,$0010,$0008,$0004,$0002,$0001);

  cFillMask: array[0..15] of TFillMask =
    (($FFFF,$FFFF,$FFFF,$FFFF, {0}
      $FFFF,$FFFF,$FFFF,$FFFF,
      $FFFF,$FFFF,$FFFF,$FFFF,
      $FFFF,$FFFF,$FFFF,$FFFF),

     ($FFFF,$0000,$0000,$0000, {1}
      $0000,$0000,$0000,$0000,
      $FFFF,$0000,$0000,$0000,
      $0000,$0000,$0000,$0000),

     ($8080,$8080,$8080,$8080, {2}
      $8080,$8080,$8080,$8080,
      $8080,$8080,$8080,$8080,
      $8080,$8080,$8080,$8080),

     ($8080,$4040,$2020,$1010, {3}
      $0808,$0404,$0202,$0101,
      $8080,$4040,$2020,$1010,
      $0808,$0404,$0202,$0101),

     ($0101,$0202,$0404,$0808, {4}
      $1010,$2020,$4040,$8080,
      $0101,$0202,$0404,$0808,
      $1010,$2020,$4040,$8080),

     ($FFFF,$8080,$8080,$8080, {5}
      $8080,$8080,$8080,$8080,
      $FFFF,$8080,$8080,$8080,
      $8080,$8080,$8080,$8080),

     ($8181,$4242,$2424,$1818, {6}
      $1818,$2424,$4242,$8181,
      $8181,$4242,$2424,$1818,
      $1818,$2424,$4242,$8181),

     ($0000,$0000,$0000,$0000, {7}
      $0000,$0000,$0000,$0000,
      $0000,$0000,$0000,$0000,
      $0000,$0000,$0000,$0000),

     ($AAAA,$0000,$5555,$0000, {8}
      $AAAA,$0000,$5555,$0000,
      $AAAA,$0000,$5555,$0000,
      $AAAA,$0000,$5555,$0000),

     ($FFFF,$0000,$0000,$0000, {9}
      $FFFF,$0000,$0000,$0000,
      $FFFF,$0000,$0000,$0000,
      $FFFF,$0000,$0000,$0000),

     ($8888,$8888,$8888,$8888, {A}
      $8888,$8888,$8888,$8888,
      $8888,$8888,$8888,$8888,
      $8888,$8888,$8888,$8888),

     ($8888,$4444,$2222,$1111, {B}
      $8888,$4444,$2222,$1111,
      $8888,$4444,$2222,$1111,
      $8888,$4444,$2222,$1111),

     ($1111,$2222,$4444,$8888, {C}
      $1111,$2222,$4444,$8888,
      $1111,$2222,$4444,$8888,
      $1111,$2222,$4444,$8888),

     ($FFFF,$8888,$8888,$8888, {D}
      $FFFF,$8888,$8888,$8888,
      $FFFF,$8888,$8888,$8888,
      $FFFF,$8888,$8888,$8888),

     ($8888,$0000,$2222,$0000, {E}
      $8888,$0000,$2222,$0000,
      $8888,$0000,$2222,$0000,
      $8888,$0000,$2222,$0000),

     ($8080,$0000,$0000,$0000, {F}
      $0808,$0000,$0000,$0000,
      $8080,$0000,$0000,$0000,
      $0808,$0000,$0000,$0000));

  EGA_Colors: tHexColors =
    ($00000000,
     $007F0000,
     $00007F00,
     $007F7F00,
     $0000007F,
     $007F007F,
     $00007F7F,
     $00C0C0C0,
     $007F7F7F,
     $00FF0000,
     $0000FF00,
     $00FFFF00,
     $000000FF,
     $00FF00FF,
     $0000FFFF,
     $00FFFFFF);

  EGA_Quads: array[0..15] of longint =
    ($00000000,
     $0000007F,
     $00007F00,
     $00007F7F,
     $007F0000,
     $007F007F,
     $007F7F00,
     $00C0C0C0,
     $007F7F7F,
     $000000FF,
     $0000FF00,
     $0000FFFF,
     $00FF0000,
     $00FF00FF,
     $00FFFF00,
     $00FFFFFF);

var
  sys_CYCaption: Integer;
  sys_CXVScroll: Integer;

function xEqual(f1,f2: Double): Boolean;
function eps_Equal(f1,f2,eps: Double): Boolean;

function _int64(i0,i1: Integer): Int64;

function Long_words(w1,w2: Integer): Integer;
function Long_bytes(b0,b1,b2,b3: Integer): Integer;

function _Geoid(b,l: Double): TGeoid;
function _Gauss(x,y: Double): TGauss;

function _XPoint(x,y: Integer): XPoint;
function _LPoint(x,y: Integer): LPoint;
function _VPoint(x,y,z: Integer): VPoint;

function _LGauss(x,y: Double): LPoint;

function _cmyk(c,m,y,k: Double): TCmyk;

function _sys(pps,prj,elp: Integer; b1,b2,lc: Double): tsys;

function _XGeoid(x,y: Double; elp,prj,pps: Integer): XGeoid;

function _xyz(x,y,z: Double): txyz;

function xy_Point(const v: vpoint): LPoint;

function _Locator(X,Y,R: Integer): TRect;

procedure _GOrient(Dst,Src: PGPoly);

function ru_Datum: TDatum;
function nil_Datum: TDatum;

function ru_elp: tsys;

function elp_Equal(const s1,s2: tsys): Boolean;
function sys_Equal(const s1,s2: tsys): Boolean;

function xGetMem(len: longint): pointer;
function zGetMem(len: longint): pointer;
function xFreeMem(p: pointer; len: longint): pointer;

function xAllocPtr(len: Integer): pointer;
function xReAllocPtr(P: Pointer; len: Integer): Pointer;
function xFreePtr(p: pointer): pointer;

function xAllocInt(cnt: Integer): PIntegers;
function xAllocInt64(cnt: Integer): PInt64s;
function xAllocDoubles(cnt: Integer): PDoubles;
function xAllocLPoints(cnt: Integer): PLPoly;
function xAllocGauss(cnt: Integer): PGPoly;

function Alloc_LLine(cnt: Integer): PLLine;
function Alloc_VLLine(cnt: Integer): PVLine;

function Alloc_PolyLine(cnt: Integer): PPolyLine;

function xAllocFile(len: Integer; out vm: TMapView): pointer;
function xReadOnlyFile(h: Integer; out vm: TMapView): pointer;
function xUpdateFile(h: Integer; out vm: TMapView): Pointer;
procedure xFreeFile(var vm: TMapView);

function xResolution(w,h: double): double;

function xDeviceRatio(Dev: HDC): Double;

function MetersPerPixel(DC: hDC): double;
function xMetersPerPixel: double;

function BitsPerPixel(DC: hDC): Integer;
function xBitsPerPixel: Integer;

function xCreateCompatibleBitMap(Width,Height: integer): hBitMap;

function xPixels(dist: Double; scale: longint): longint;

function Param_Option(opt: ShortString): Boolean;
function xParamCount: Integer;

function xRange(i, a,b: double): double;
function iRange(i, a,b: Integer): Integer;

function iIndex(ind,max: Integer): Integer;

procedure iSwap(var a,b: Integer);
procedure cSwap(var a,b: Cardinal);
procedure wSwap(var a,b: Word);
procedure xSwap(var a,b: Double);

function Long(a,b: Integer): longint;

function CountersCount(cp: pIntegers; N: Integer): Integer;
function CountersMax(cp: pIntegers; N: Integer): Integer;

function Int_Contains(Items: PIntegers; N: Integer;
                      item: Integer): Integer;

function Index_Contains(Items: pInt64s; Count: Integer;
                        Id: Integer): Integer;

function x_Index_Contains(Items: PInt64s; Count: Integer;
                          Id: Integer): Integer;

procedure Get_MinMax(cp: PIntegers; N: Integer;
                     min,max: PInteger);

function z_axe_ptr(hp: PIntegers; N: Integer): PIntegers;

function int_Compare(p1: PIntegers; Count1: Integer;
                     p2: PIntegers; Count2: Integer): Boolean;

function SwapInt(i: Integer): Integer;
function SwapWord(w: Word): Word;

function int_Time(T: TDateTime): Integer;

implementation

function xEqual(f1,f2: Double): Boolean;
begin
  Result:=Abs(f1-f2) <= Small
end;

function eps_Equal(f1,f2,eps: Double): Boolean;
begin
  Result:=Abs(f1-f2) <= eps
end;

function _int64(i0,i1: Integer): Int64;
//var
//  v: TInt64;
begin
//  v.i[0]:=i0; v.i[1]:=i1;
  Result:=0;
end;

function Long_words(w1,w2: Integer): Integer;
asm
  shl  EDx,16
  add  EAx,EDx
end;

function Long_bytes(b0,b1,b2,b3: Integer): Integer;
var
  ax: tlong;
begin
  ax.b[0]:=b0; ax.b[1]:=b1;
  ax.b[2]:=b2; ax.b[3]:=b3;
  Result:=ax.i
end;

function _Geoid(b,l: Double): TGeoid;
begin
  Result.b:=b; Result.l:=l
end;

function _Gauss(x,y: Double): TGauss;
begin
  Result.x:=x; Result.y:=y
end;

function _LPoint(x,y: Integer): LPoint;
begin
  Result.x:=x; Result.y:=y
end;

function _XPoint(x,y: Integer): XPoint;
begin
  Result.p.x:=x; Result.p.y:=y;
  Result.g.x:=x; Result.g.y:=y;
end;

function _VPoint(x,y,z: Integer): VPoint;
begin
  Result.x:=x; Result.y:=y; Result.z:=z;
end;

function _LGauss(x,y: Double): LPoint;
begin
  Result.x:=Round(x);
  Result.y:=Round(y);
end;

function _cmyk(c,m,y,k: Double): TCmyk;
begin
  Result.c:=c;
  Result.m:=m;
  Result.y:=y;
  Result.k:=k;
end;

function _xyz(x,y,z: Double): txyz;
begin
  Result.x:=x; Result.y:=y; Result.z:=z;
end;

function _sys(pps,prj,elp: Integer; b1,b2,lc: Double): tsys;
var
  s: tsys;
begin
  FillChar(s,SizeOf(s),0);
  s.pps:=pps; s.prj:=prj; s.elp:=elp;
  s.b1:=b1; s.b2:=b2; s.lc:=lc;

  s.dat:=nil_Datum; if pps = 1 then
  if prj <= 2 then s.dat:=ru_Datum;

  Result:=s
end;

function _XGeoid(x,y: Double; elp,prj,pps: Integer): XGeoid;
begin
  Fillchar(Result,SizeOf(XGeoid),0);
  Result.s.elp:=elp; Result.s.prj:=prj;
  Result.s.pps:=pps; Result.x:=x; Result.y:=y;
end;

function ru_Datum: TDatum;
begin
  Result.x:=28; Result.y:=-130; Result.z:=-95
end;

function is_nil_Datum(const dat: TDatum): Boolean;
begin
  Result:=(dat.x = 0) and (dat.y = 0) and (dat.z = 0)
end;

function nil_Datum: TDatum;
begin
  Result.x:=0; Result.y:=0; Result.z:=0
end;

function ru_elp: tsys;
var
  s: tsys;
begin
  FillChar(s,Sizeof(s),0);
  s.elp:=1; s.dat:=ru_Datum;
  Result:=s
end;

function elp_Equal(const s1,s2: tsys): Boolean;
var
  elp1,elp2: Integer; dat1,dat2: TDatum;
begin
  elp1:=s1.elp; dat1:=s1.dat;
  elp2:=s2.elp; dat2:=s2.dat;

  if elp1 = 0 then begin elp1:=1; dat1:=ru_Datum end;
  if elp2 = 0 then begin elp2:=1; dat2:=ru_Datum end;

  if elp1 = 1 then
  if is_nil_Datum(dat1) then
  dat1:=ru_Datum;

  if elp2 = 1 then
  if is_nil_Datum(dat2) then
  dat2:=ru_Datum;

  Result:=(elp1 = elp2) and
          (dat1.x = dat2.x) and
          (dat1.y = dat2.y) and
          (dat1.z = dat2.z)
end;

function sys_Equal(const s1,s2: tsys): Boolean;
begin
  Result:=(s1.pps = s2.pps) and
          (s1.elp = s2.elp) and
          (s1.prj = s2.prj) and
          xEqual(s1.lc,s2.lc) and
          xEqual(s1.b1,s2.b1) and
          xEqual(s1.b2,s2.b2)
end;

function xy_Point(const v: vpoint): LPoint;
begin
  Result.x:=v.x; Result.y:=v.y;
end;

function _Locator(X,Y,R: Integer): TRect;
begin
  Result:=Rect(X-R,Y-R,X+R,Y+R)
end;

procedure _GOrient(Dst,Src: PGPoly);
var
  I: Integer;
begin
  for I:=0 to Orient_Max-1 do Dst[I]:=Src[I]
end;

function xGetMem(len: Integer): pointer;
begin
  Result:=nil;
  try
    GetMem(Result,len);
  except
    on EOutOfMemory do Result:=nil
  end
end;

function zGetMem(len: Integer): pointer;
begin
  Result:=xGetMem(len); if Result <> nil then
  FillChar(Result^,len,0)
end;

function xFreeMem(p: pointer; len: Integer): pointer;
begin
  if p <> nil then FreeMem(p,len); Result:=nil
end;

function xAllocPtr(len: Integer): Pointer;
begin
  Result:=nil; if len > 0 then begin
    Result:=GlobalAllocPtr(0,len);
    if Assigned(Result) then
    FillChar(Result^,len,0)
  end
end;

function xReAllocPtr(P: Pointer; len: Integer): Pointer;
var
  old: Integer;
begin
  Result:=GlobalReAllocPtr(P,len,0);
  if Result = nil then begin
    Result:=xAllocPtr(len);

    old:=GlobalSize(GlobalHandle(P));
    if (old > 0) and (old < len) then
    if Assigned(Result) then
    Move(P^,Result^,old);

    xFreePtr(P);
  end
end;

function xFreePtr(p: pointer): pointer;
begin
  if p <> nil then GlobalFreePtr(p);
  Result:=nil
end;

function xAllocInt(cnt: Integer): PIntegers;
begin
  Result:=xAllocPtr(cnt * SizeOf(Integer))
end;

function xAllocInt64(cnt: Integer): PInt64s;
begin
  Result:=xAllocPtr(cnt * SizeOf(Int64))
end;

function xAllocDoubles(cnt: Integer): PDoubles;
begin
  Result:=xAllocPtr(cnt * SizeOf(Double))
end;

function xAllocLPoints(cnt: Integer): PLPoly;
begin
  Result:=xAllocPtr(cnt * SizeOf(LPoint))
end;

function xAllocGauss(cnt: Integer): PGPoly;
begin
  Result:=xAllocPtr(cnt * SizeOf(TGauss))
end;

function Alloc_LLine(cnt: Integer): PLLine;
begin
  Result:=xAllocPtr((cnt+1)*SizeOf(LPoint));
  if Result <> nil then Result.N:=cnt-1
end;

function Alloc_VLLine(cnt: Integer): PVLine;
begin
  Result:=xAllocPtr((cnt+1) * SizeOf(VPoint));
  if Result <> nil then Result.N:=cnt-1
end;

function Alloc_PolyLine(cnt: Integer): PPolyLine;
begin
  Result:=xAllocPtr((cnt+1)*SizeOf(LPoint));
  if Result <> nil then Result.N:=cnt-1
end;

function xAllocFile(len: Integer; out vm: TMapView): Pointer;
begin
  Result:=nil; with vm do begin
    h:=CreateFileMapping($FFFFFFFF,nil,PAGE_READWRITE,0,len,nil);
    if h <> 0 then Result:=MapViewOfFile(h,FILE_MAP_WRITE,0,0,0);
    p:=Result; size:=0; if Result <> nil then size:=len
  end
end;

function xReadOnlyFile(h: Integer; out vm: TMapView): Pointer;
begin
  Result:=nil; vm.p:=nil; vm.size:=FileSeek(h,0,2);
  vm.h:=CreateFileMapping(h,nil,PAGE_READONLY,0,0,nil);
  if vm.h <> 0 then Result:=MapViewOfFile(vm.h,FILE_MAP_READ,0,0,0);
  vm.p:=Result
end;

function xUpdateFile(h: Integer; out vm: TMapView): Pointer;
begin
  Result:=nil; vm.p:=nil; vm.size:=FileSeek(h,0,2);
  vm.h:=CreateFileMapping(h,nil,PAGE_READWRITE,0,0,nil);
  if vm.h <> 0 then Result:=MapViewOfFile(vm.h,FILE_MAP_WRITE,0,0,0);
  vm.p:=Result
end;

procedure xFreeFile(var vm: TMapView);
begin
  with vm do begin
    if h <> 0 then begin
      if p <> nil then UnmapViewOfFile(p);
      CloseHandle(h); h:=0; p:=nil;
    end; p:=xFreePtr(p); size:=0
  end
end;

function xResolution(w,h: double): double;
var
  DC: hDC; kx,ky: double;
begin
  DC:=GetDC(0);
  kx:=w/GetDeviceCaps(DC,HorzRes);
  ky:=h/GetDeviceCaps(DC,VertRes);
  Result:=Math.Max(kx,ky);
  ReleaseDC(0,DC);
end;

function xDeviceRatio(Dev: HDC): Double;
var
  DC: HDC; dev_ppi,dc_ppi: Integer;
begin
  Result:=1;

  DC:=GetDC(0);
  if DC <> 0 then begin
    dev_ppi:=GetDeviceCaps(DC,LogPixelSx);
    dc_ppi:=GetDeviceCaps(DC,LogPixelSx);
    if (dev_ppi > 0) and (dc_ppi > 0) then
    Result:=dev_ppi / dc_ppi
  end
end;

function MetersPerPixel(DC: HDC): double;
var
  ppi: Integer;
begin
  ppi:=GetDeviceCaps(DC,LogPixelSx);
  if ppi > 0 then Result:=0.0254 / ppi else
  Result:=xMetersPerPixel
end;

function xMetersPerPixel: double;
var
  DC: hDC;
begin
  DC:=GetDC(0);
  Result:=MetersPerPixel(DC);
  ReleaseDC(0,DC)
end;

function BitsPerPixel(DC: hDC): Integer;
begin
  Result:=GetDeviceCaps(DC, BITSPIXEL) *
          GetDeviceCaps(DC, PLANES);
end;

function xBitsPerPixel: Integer;
var
  DC: hDC;
begin
  DC:=GetDC(0);
  Result:=BitsPerPixel(DC);
  ReleaseDC(0,DC)
end;

function xCreateCompatibleBitMap(Width,Height: Integer): hBitMap;
var
  DC: hDC;
begin
  DC:=GetDC(0);
  Result:=CreateCompatibleBitMap(DC,Width,Height);
  ReleaseDC(0,DC)
end;

function xPixels(dist: Double; scale: longint): longint;
begin
  Result:=0; if scale > 0 then
  Result:=Round(dist / scale / xMetersPerPixel + 0.5)
end;

function Param_Option(opt: ShortString): Boolean;
var
  i: Integer;
begin
  Result:=false;

  for i:=1 to ParamCount do
  if ParamStr(i) = opt then begin
    Result:=true; Break
  end

end;

function xParamCount: Integer;
var
  i: Integer;
begin
  Result:=ParamCount;

  for i:=1 to ParamCount do
  if ParamStr(i)[1] = '/' then Dec(Result)
  else Break
end;

function xRange(i, a,b: Double): Double;
begin
  Result:=Min(Max(i,a),b)
end;

function iRange(i, a,b: Integer): Integer;
begin
  if i < a then i:=a;
  if i > b then i:=a;
  Result:=i;
end;

function iIndex(ind,max: Integer): Integer;
asm
  cmp  EAx,0
  jl   @err
  cmp  EAx,EDx
  jl   @exit

@err:
  mov  EAx,0
@exit:
end;

procedure iSwap(var a,b: Integer);
asm
  push ESi
  mov  ESi,EAx
  mov  EAx,DWORD PTR [ESi]
  mov  ECx,DWORD PTR [EDx]
  mov  DWORD PTR [ESi],ECx
  mov  DWORD PTR [EDx],EAx
  pop  ESi
end;

procedure cSwap(var a,b: Cardinal);
asm
  push ESi
  mov  ESi,EAx
  mov  EAx,DWORD PTR [ESi]
  mov  ECx,DWORD PTR [EDx]
  mov  DWORD PTR [ESi],ECx
  mov  DWORD PTR [EDx],EAx
  pop  ESi
end;

procedure wSwap(var a,b: Word);
asm
  push ESi
  mov  ESi,EAx
  mov  Ax,WORD PTR [ESi]
  mov  Cx,WORD PTR [EDx]
  mov  WORD PTR [ESi],Cx
  mov  WORD PTR [EDx],Ax
  pop  ESi
end;

procedure xSwap(var a,b: Double); var t: Double;
begin t:=a; a:=b; b:=t end;

function Long(a,b: Integer): longint;
begin
  tlong(Result).w[0]:=a;
  tlong(Result).w[1]:=b;
end;

function CountersCount(cp: PIntegers; N: Integer): Integer;
asm
  push EBx
  push ESi

  mov  ESi,EAx
  mov  ECx,EDx

  cld
  mov  EBx,0
@loop:
  lodsd
  add  EBx,EAx
  loop @loop

  mov  EAx,EBx

  pop  ESi
  pop  EBx
end;

function CountersMax(cp: pIntegers; N: Integer): Integer;
asm
  push EBx
  push ESi

  mov  ESi,EAx
  mov  ECx,EDx

  cld
  mov  EBx,0
@loop:
  lodsd
  cmp  EBx,EAx
  jg   @next
  mov  EBx,EAx
@next:
  loop @loop

  mov  EAx,EBx

  pop  ESi
  pop  EBx
end;

procedure Get_MinMax(cp: PIntegers; N: Integer;
                     min,max: PInteger);
begin
  asm
    push EAx
    push EBx
    push ECx
    push EDx
    push ESi
    push EDi

    cld
    mov  ESi,[cp]
    mov  ECx,[N]

    mov  EBx,DWORD PTR [ESi]
    mov  EDx,DWORD PTR [ESi]

    cmp  ECx,0
    jle  @skip
  @loop:
    lodsd
    cmp  EAx,EBx
    jl   @less
    cmp  EAx,EDx
    jg   @larger
    jmp  @next
@less:
    mov  EBx,EAx
    jmp  @next
@larger:
    mov  EDx,EAx
  @next:
    loop @loop

@skip:
    mov  EDi,[min]
    mov  DWORD PTR [EDi],EBx
    mov  EDi,[max]
    mov  DWORD PTR [EDi],EDx

    pop  EDi
    pop  ESi
    pop  EDx
    pop  ECx
    pop  EBx
    pop  EAx
  end
end;

function Int_Contains(Items: PIntegers; N: Integer;
                      item: Integer): Integer;
asm
  push EBx
  push ESi

  mov  ESi,EAx
  mov  EBx,ECx
  mov  ECx,EDx

  mov  EDx,0

  cld
@loop:
  lodsd
  cmp  EBx,EAx
  je   @exit
  inc  EDx
  loop @loop

  mov  EDx,-1
@exit:
  mov  EAx,EDx

  pop  ESi
  pop  EBx
end;

function Index_Contains(Items: PInt64s; Count: Integer;
                        Id: Integer): Integer;
asm
  push EBx
  push ESi

  mov  ESi,EAx
  mov  EBx,ECx
  mov  ECx,EDx

  mov  EDx,0

  cld
@loop:
  lodsd
  cmp  EBx,EAx
  je   @exit
  add  ESi,4
  inc  EDx
  loop @loop

  mov  EDx,-1
@exit:
  mov  EAx,EDx

  pop  ESi
  pop  EBx
end;

function x_Index_Contains(Items: PInt64s; Count: Integer;
                          Id: Integer): Integer;
asm
  push EBx
  push ESi
  push EDi

  mov  ESi,EAx
  mov  EBx,ECx

  mov  ECx,0
  dec  EDx

@loop:
  cmp  ECx,EDx
  jg   @false

  mov  EAx,ECx
  add  EAx,EDx
  shr  EAx,1

  mov  EDi,EAx
  shl  EDi,3
  add  EDi,ESi

  cmp  EBx,DWORD PTR [EDi]
  jb   @less
  je   @exit

@more:
  mov  ECx,EAx
  inc  ECx
  jmp  @loop

@less:
  mov  EDx,EAx
  dec  EDx
  jmp  @loop

@false:
  mov  EAx,-1

@exit:
  pop  EDi
  pop  ESi
  pop  EBx
end;

function z_axe_ptr(hp: PIntegers; N: Integer): PIntegers;
var
  zmin,zmax: Integer;
begin
  Result:=nil; if Assigned(hp) then begin
    Get_MinMax(hp,N, @zmin,@zmax);
    if (zmin <> 0) or (zmax <> 0) then
    Result:=hp
  end;
end;

function int_Compare(p1: PIntegers; Count1: Integer;
                     p2: PIntegers; Count2: Integer): Boolean;
var
  i: Integer;
begin
  Result:=Count1 = Count2;

  if Result then
  for i:=0 to Count1-1 do
  if p1[i] <> p2[i] then begin
    Result:=false; Break
  end
end;

function SwapInt(i: Integer): Integer;
asm
  BSWAP EAx
end;

function SwapWord(w: Word): Word;
asm
  BSWAP EAx
  shr   EAx,16
end;

function int_Time(T: TDateTime): Integer;
var
  Hour, Min, Sec, MSec: Word;
begin
  DecodeTime(T, Hour,Min,Sec,MSec);
  Result:=Hour; Result:=Result * 3600;
  Inc(Result, Min*60 + Sec)
end;

begin
  sys_CYCaption:=GetSystemMetrics(sm_CYCaption);
  sys_CXVScroll:=GetSystemMetrics(sm_CXVScroll);
//  rus_interface:=not Param_Option('/t')
end.


