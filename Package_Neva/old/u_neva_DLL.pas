unit u_neva_DLL;

interface
uses SysUtils,Classes,Dialogs,FileCtrl,
     dm_utils,
     dm_sxf,

     u_func,
     u_Geo,
     u_neva_func
     ;

 //   procedure neva_DM_to_SXF (aDM,aSXF: string);

    function  neva_SXF_to_DM (aSXF,aDM,aObj: string; ed,win: integer): Boolean;


    function neva_DM_to_REL  (aMaps           : TStrings;
                              aIFrame         : TXYiRect;
                              aStep           : integer;
                              aReliefFileName : string
                             ): boolean; overload;


    function neva_DM_to_REL  (aMapFileName    : string;
                              aMaps           : TStrings;
                              aStep           : integer;
                              aReliefFileName : string
                             ): boolean; overload;

//    procedure neva_Recolor  (aDMFileName: string);


//==================================================================
implementation
//==================================================================

{
procedure neva_DM_to_SXF (aDM,aSXF: string);
begin
  ForceDirectories (ExtractFileDir( aSXF));
  dm_to_sxf (PChar(aDM), PChar(aSXF));
end;
 }

function  neva_SXF_to_DM (aSXF,aDM,aObj: string; ed,win: integer): Boolean;
begin
 ForceDirectories (ExtractFileDir( aDM));
 Result:=sxf_to_dm (PChar(aSXF),PChar(aDM),PChar(aObj), ed,win);

end;


//----------------------------------------------------------
function neva_DM_to_REL  (aMaps    : TStrings;
                          aIFrame   : TXYiRect;
                          aStep    : integer;
                          aReliefFileName : string
                         ): boolean; overload;
//----------------------------------------------------------
var
  list: TStringList;
  sMainMapFileName,sMapsFileName: string;

begin
  Result:=False;
  ForceDirectories (ExtractFileDir (aReliefFileName));

  aReliefFileName :=ChangeFileExt(aReliefFileName,  '.rel');
  sMapsFileName   :=ChangeFileExt(aReliefFileName,  '_files.txt');
  sMainMapFileName:=aMaps[0];

  // save all except first strings
  list:=TStringList.Create;
  list.Assign (aMaps);
  if list.Count > 0 then list.Delete(0);
//  list.Add(sMainMapFileName);
  list.SaveToFile (sMapsFileName);
  list.Free;

  dm_Relief (StrNew(PChar(sMainMapFileName)),
             StrNew(PChar(sMapsFileName)),
             StrNew(PChar(aReliefFileName)),

             aIFrame.TopLeft.X,     aIFrame.TopLeft.Y,
             aIFrame.BottomRight.X, aIFrame.BottomRight.Y,

             aStep*10,
             false
             );

  // delete temp files
  DeleteFile (ChangeFileExt(aReliefFileName,  '.bl'));
  DeleteFile (ChangeFileExt(aReliefFileName,  '.lg'));
  DeleteFile (ChangeFileExt(aReliefFileName,  '.ms'));
  DeleteFile (ChangeFileExt(aReliefFileName,  '_.fot'));
  DeleteFile (ChangeFileExt(aReliefFileName,  '_.xy'));
  DeleteFile (ChangeFileExt(aReliefFileName,  '_files.txt'));

  if (not FileExists(aReliefFileName)) 
  then begin
    ShowMessage ('������� ����� �� ������������: '+ aReliefFileName);
    Exit;
  end;

  Result:=True;
end;


//----------------------------------------------------------
function neva_DM_to_REL  (aMapFileName    : string;
                          aMaps           : TStrings;
                          aStep           : integer;
                          aReliefFileName : string
                         ): boolean; overload;
//----------------------------------------------------------
var xyFrame: TXYiRect;
    oList: TStringList;
    ind: integer;
begin
  Result:=False;

  oList:=TStringList.Create;
  oList.Assign (aMaps);

  // move to top

  ind:=oList.IndexOf(aMapFileName);
  if ind>=0 then oList.Move (ind,0)
            else oList.Insert(0, aMapFileName);


  if neva_Map_LocalXYFrame (aMapFileName, xyFrame) then
  begin
    Result:=neva_DM_to_REL (oList, xyFrame, aStep,  aReliefFileName);
  end;

  oList.Free;
end;


(*
procedure neva_Recolor (aDMFileName: string);
var i: integer;
begin
  i:=dm_Recolor (StrNew(PChar(aDMFileName)));
  i:=0;
end;*)



begin
//  neva_Recolor ('t:\aaa.dm');


//  DM_to_REL ('t:\test.dm', 300, 't:\rel\test.rel');

end.




