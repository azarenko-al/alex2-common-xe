unit u_neva_LabelMaker;

interface
uses Classes,Graphics,SysUtils,
     u_neva_Func,
     u_Geo,
     u_classes,
     u_neva_Classes;

type
  TdmLabelInfo = class
    RegionBounds: TBLRect;
    Name: string;
  end;

  //--------------------------------------------
  // �������� �������� �� ���������� �������
  //--------------------------------------------
  TdmLabelMaker = class (TBaseCalc)
  private
    FLabels: TList;
    procedure DoOnNode (aObjectInfo: neva_TObjectInfo);
  public
    Params: record
      DMFileName    : string;
      LabelsFileName: string; // ����������� ����
      ObjFileName   : string;
      LabelCode     : integer;
    end;

    procedure Execute ();
  end;


implementation

              
//----------------------------------------------------
procedure TdmLabelMaker.DoOnNode (aObjectInfo: neva_TObjectInfo);
//----------------------------------------------------
var
  sName: string;
  v: Variant;
  oLabel: TdmLabelInfo;
begin
  if not neva_Get_Sem_Value  (9, v, varString) then
    Exit;

  sName:=v;
  oLabel:=TdmLabelInfo.Create;
  oLabel.Name:=sName;
  oLabel.RegionBounds:=aObjectInfo.BLBounds;

  FLabels.Add (oLabel);
end;

//----------------------------------------------------
procedure TdmLabelMaker.Execute();
//----------------------------------------------------
var
  obj: neva_TNodesReview;
  blRect: TBLRect;

  i,id: integer;
  blPoint: TBLPoint;
  oLabelInfo: TdmLabelInfo;
begin
  FLabels:=TList.Create;

  obj:=neva_TNodesReview.Create;
  obj.OnNodeFound:=DoOnNode;

  obj.Execute (Params.DMFileName);
  obj.Free;

  if neva_Map_BLFrame (Params.DMFileName, blRect) then
  begin
    neva_Create_Map (Params.LabelsFileName, Params.ObjFileName, blRect);

    neva_Open_Map (Params.LabelsFileName, False);

    for i:=0 to FLabels.Count-1 do
    begin
      oLabelInfo:=TdmLabelInfo (FLabels.Items[i]) ;
      blPoint:=geo_GetCenter (oLabelInfo.RegionBounds);

      id:=neva_Insert_Text (Params.LabelCode, oLabelInfo.Name, blPoint, false);
    end;

    neva_Close_Map();
  end;

  FLabels.Free;
end;
//----------------------------------------------------




var
  obj: TdmLabelMaker;
begin

end.
 