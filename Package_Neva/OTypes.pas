unit OTypes;

interface

uses
  Windows,Classes,SysUtils,Math;

const
  rus_interface: Boolean = true;
  dos_interface: Boolean = true;
  debug_enabled: Boolean = false;
  drag_Enabled: Boolean = true;
  ver_english: Boolean = false;
  txt_english: Boolean = false;
  log_enabled: Boolean = false;
  view_interface: Boolean = false;

  IsWin32: Boolean = true;
  IsOcx: Boolean = false;

  ErrCode: Integer = 0;

  ansi_draw: Boolean = false;
  ansi_pack: Boolean = false;

  InterfaceIndex: Integer = 0;

  ClsId_Null: TGUID = '{00000000-0000-0000-0000-000000000000}';

  sPlus: array[Boolean] of Char = '-+';

  ClientCount: Integer = 0;

  LPoly_Max = 255;
  fsNumStr = 16;

  Small = 1E-6;

  Orient_Max = 6;
  Value_Max  = 32;

  Unicode_Max = 1023;
  Vector_Max  = 1023;

  Int_NAN = -999999999;

  WinOS: Integer = 0;

type
  b11 = Byte;
  b12 = Word;
  b14 = Cardinal;
  long = Longint;

  Int16 = SmallInt;

  Float = single;
  PFloat = ^Float;

  tbyte_set = set of byte;
  tchar_set = set of char;

  alfa = array[1..8] of Char;
  alpha = array[1..16] of Char;
  Char2 = array[1..2] of Char;
  Char3 = array[1..3] of Char;
  Char4 = array[1..4] of Char;
  Char5 = array[1..5] of Char;
  Char7 = array[1..7] of Char;
  Char8 = array[1..8] of Char;

  char10 = array[1..10] of char;
  char16 = array[1..16] of char;
  char32 = array[1..32] of char;

  TRational = record c1,c2: longint; end;

  PBools = ^TBools;
  TBools = array[0..63] of Boolean;

  PBytes = ^TBytes;
  TBytes = array[0..1023] of Byte;

  TByte8 = array[0..7] of Byte;

  PWords = ^TWords;
  TWords = array[0..1023] of Word;

  PSmallints = ^TSmallints;
  TSmallints = array[0..1023] of Smallint;

  PIntegers = ^TIntegers;
  TIntegers = array[0..1023] of Integer;

  PDWords = ^TDWords;
  TDWords = array[0..1023] of DWord;

  PPointers = ^TPointers;
  TPointers = array[0..1023] of PBytes;

  PPointerArray = ^TPointerArray;
  TPointerArray = array[0..1023] of Pointer;

  PInt64s = ^TInt64s;
  TInt64s = array[0..1023] of Int64;

  PPool = ^TPool;
  TPool = record Len: word; Buf: tbytes end;

  PHeights = ^THeights;
  THeights = array[0..LPoly_Max] of Integer;

  PLongBuf = ^TLongBuf;
  TLongBuf = array[0..4096] of Byte;

  TAlfaStr = array[0..31] of Char;
  TNameStr = array[0..31] of Char;
  TShortStr = array[0..255] of Char;
  TPathStr = array[0..511] of Char;
  TWideStr = array[0..255] of WideChar;
  TUnicode = array[0..1023] of WideChar;
  TCmdStr = array[0..1023] of Char;
  THexStr = array[0..1023] of Char;

  PLongStr = ^TLongStr;
  TLongStr = array[0..8191] of Char;

  TMapView = record
    h: THandle; p: PBytes;
    size: Int64;
  end;

  PFloats = ^TFloats;
  TFloats = array[0..1023] of Float;

  PSingles = ^TSingles;
  TSingles = array[0..1023] of Single;

  PDoubles = ^TDoubles;
  TDoubles = array[0..1023] of Double;

  XRect = record x,y,w,h: Integer end;

  LPoint = TPoint;

  PGauss = ^TGauss;
  PVPoint = ^VPoint;

  WPoint = record x,y: Word end;
  IPoint = record x,y: SmallInt end;
  TGauss = record x,y: Double end;
  TRange = record min,max: Double end;
  TGeoid = record b,l: Double end;
  VPoint = record x,y,z: Integer end;
  TPoint64 = record x,y: Int64 end;

  PPort2d = ^TPort2d;
  TPort2d = record case Integer of
0:(x,y,w,h: Double); 1:(o,s: TGauss);  end;

  PDatum = ^TDatum; TDatum = VPoint;

  PDatum7 = ^TDatum7;
  TDatum7 = record
    dX,dY,dZ,wX,wY,wZ,m: Double;
  end;

  PDatums = ^TDatums;
  TDatums = Array[0..255] of TDatum7;

  PXyz = ^TXyz;
  TXyz = record x,y,z: Double; end;

  pxyz_array = ^txyz_array;
  txyz_array = array[0..1023] of txyz;

  txyz_orient = array[0..7] of txyz;

  XPoint = record
    p: LPoint; g: TGauss
  end;

  psys3 = ^tsys3;
  tsys3 = record
    pps,prj,elp: Integer;
    b1,b2,lc: Double;
    dat: TDatum; rn: Double;
  end;

  psys = ^tsys; tsys = record
    pps,prj,elp: Integer;
    lc,b1,b2,b3,k0,rn,x0,y0: Double;
    dat: TDatum7;
  end;

  xgeoid = record
    x,y: Double; s: tsys;
  end;

  PGeoPoint = ^TGeoPoint;
  TGeoPoint = xgeoid;

  lxyz = record case integer of
0: (p: LPoint); 1: (v: VPoint)
  end;

  LRect = record case integer of
0: (Left,Top,Right,Bottom: Integer);
1: (lt,rb: LPoint);
  end;

  TVector = array[0..1] of TPoint;
  LVector = array[0..1] of LPoint;
  GVector = array[0..1] of TGauss;
  VVector = array[0..1] of VPoint;

  TDouble = record case integer of
0: (d: Double);
1: (f: single);
2: (l: Longint);
3: (i: SmallInt);
4: (x: Int64);
5: (w: Word);
6: (b: Byte);
  end;

  TInt64 = record case integer of
0: (x: Int64);
1: (c: array[0..1] of Cardinal);
2: (i: array[0..1] of Integer);
3: (p: array[0..1] of Pointer);
4: (w: array[0..3] of Word);
5: (s: array[0..3] of SmallInt);
6: (b: array[0..7] of Byte);
7: (w1: Word; c1: Cardinal; w2: Word);
8: (dx,dy,dz,dw: SmallInt);
9: (id,cn: Cardinal);
  end;

  tlong = record case integer of
0: (i: Longint);
1: (p: PBytes);
2: (c: Cardinal);
3: (w: array[0..1] of Word);
4: (s: array[0..1] of SmallInt);
5: (b: array[0..3] of Byte);
6: (f: single)
  end;

  TWord = record case integer of
0:  (b: array[0..1] of Byte);
1:  (w: Word)
  end;

  PString = ^TString;
  TString = record case integer of
0: (x: TWideStr);
1: (s: ShortString);
2: (ci: TShortStr);
3: (d: Double);
4: (f: single);
5: (l: Longint);
6: (i: SmallInt);
7: (w: Word);
8: (b: Byte);
  end;

  TOrient = array[0..Orient_Max-1] of TPoint;
  WOrient = array[0..Orient_Max-1] of WPoint;
  LOrient = array[0..Orient_Max-1] of LPoint;
  GOrient = array[0..Orient_Max-1] of TGauss;
  VOrient = array[0..Orient_Max-1] of VPoint;
  XOrient = array[0..Orient_Max-1] of XGeoid;
  ZOrient = array[0..Orient_Max-1] of Longint;
  ZValues = array[0..Orient_Max-1] of Double;
  RValues = array[0..Orient_Max-1] of Float;

  DValues = array[0..Value_Max-1] of Double;

  TPoint3 = Array[0..2] of TPoint;

  TGauss3 = Array[0..2] of TGauss;
  TGauss4 = Array[0..3] of TGauss;

  TMoments = Array[0..2] of Double;

  XYZOrient = array[0..Orient_Max-1] of txyz;

  GCross = array[0..8] of TGauss;
  TCross = array[0..8] of TPoint;

  PXPoly = ^XOrient;

  PPoly = ^TPoly;
  TPoly = array[0..LPoly_Max] of TPoint;

  PWPoly = ^TWPoly;
  TWPoly = array[0..LPoly_Max] of WPoint;

  PIPoly = ^TIPoly;
  TIPoly = array[0..LPoly_Max] of IPoint;

  PLPoly = PPoly;
  TLPoly = array[0..LPoly_Max] of LPoint;

  PGPoly = ^TGPoly;
  TGPoly = array[0..LPoly_Max] of TGauss;

  PVPoly = ^TVPoly;
  TVPoly = array[0..LPoly_Max] of VPoint;

  PXyzPoly = ^TXyzPoly;
  TXyzPoly = array[0..LPoly_Max] of txyz;

  PLine = ^TLine;
  TLine = record N: Integer; Pol: TPoly end;

  PWLine = ^TWLine;
  TWLine = record N: Integer; Pol: TWPoly end;

  PLLine = ^TLLine;
  TLLine = packed record N: SmallInt; Pol: TLPoly end;
  VLLine = packed record N: SmallInt; Pol: LOrient end;
  CLLine = packed record N: SmallInt; Pol: TCross end;

  PPolyLine = ^TPolyLine;
  TPolyLine = record N: Integer; Pol: TLPoly end;

  PVLine = ^TVLine;
  TVLine = record N: Integer; Pol: TVPoly end;

  PGLine = ^TGLine;
  TGLine = record N: Integer; Pol: GOrient end;

  PXLine = ^TXLine;
  TXLine = record N: Integer; Pol: TGPoly end;

  TVBound = record N: Integer; Pol: VOrient end;

  PRealVec = ^TRealVec;
  TRealVec = array[0..1023] of Float;

  TNumStr = array[0..fsNumStr-1] of Char;
  PNumStr = ^TNumStr;

  TChannel = array[0..255] of Byte;
  thChannel = array[0..15] of Byte;

  PIndex = ^TIndex;
  TIndex = array[0..4095] of Longint;

  TRGBQuads = array[0..255] of TRGBQuad;
  PRGBQuads = ^TRGBQuads;

  TRGB_Rec = packed record r,g,b: Byte end;
  TRGB_Tbl = packed array[0..255] of TRGB_Rec;
  PRGB_Tbl = ^TRGB_Tbl;

  TColors = array[0..255] of TColorRef;
  PColors = ^TColors;

  THexColors = array[0..15] of TColorRef;
  PHexColors = ^THexColors;

  TDmwColors = array[0..31] of TColorRef;
  PDmwColors = ^TDmwColors;

  PPaletteChanel = ^TPaletteChanel;
  TPaletteChanel = array[0..255] of Word;

  PFillMask = ^TFillMask;
  TFillMask = array[0..15] of Word;

  PFillMasks = ^TFillMasks;
  TFillMasks = array[0..63] of TFillMask;

  TRGB_hist = array[0..255] of Integer;

  TMixedColor = record cl,lo,hi: TColorRef end;

  PMixedColors = ^TMixedColors;
  TMixedColors = array[0..63] of TMixedColor;

  TEventProc = procedure of object;
  TEventFunc = function(Sender: TObject): HResult of object;
  TPointerProc = procedure(p: Pointer) of object;
  TPointerNext = function(p: Pointer): Pointer of object;
  TPointerFunc = function(p: Pointer): Boolean of object;

  TBoolProc = procedure(Sender: TObject; Fl: Boolean) of object;

  TCharProc = procedure(Sender: TObject; Str: PChar) of object;
  TDblProc = procedure(Sender: TObject; Dbl: Boolean) of object;

  TCharFunc = function(Str: PChar): PChar of object;

  TLPolyProc = procedure(lp: PLPoly; lp_n: Integer) of object;
  TLPolyFunc = function(lp: PLPoly; lp_n: Integer): Integer of object;

  TLPolyProc1 = procedure(lp: PLPoly; hp: PIntegers;
                          lp_n: Integer) of object;

  TLLineProc = procedure(lp: PLLine) of object;
  TLLineFunc = function(lp: PLLine): Integer of object;

  TLLineProc1 = procedure(lp: PLLine; hp: PIntegers) of object;

  TGetInteger = function(Sender: TObject): Integer of object;
  TSetInteger = procedure(Sender: TObject; V: Integer) of object;
  TValueProcI = procedure(Sender: TObject; V: PIntegers) of object;

  TValueProc = procedure(Sender: TObject; V: Double) of object;
  TValueProc2 = procedure(Sender: TObject; V1,V2: Double) of object;
  TValueFunc = function(Sender: TObject; var V: Double): Boolean of object;

  TValueProcN = procedure(Sender: TObject; V: PDoubles) of object;

  THandleProc = procedure(H: THandle) of object;

  TFloatProc = procedure(V: Float) of object;
  TFloatProcN = procedure(VP: PFloats) of object;

  TIntegerProc = procedure(V: Integer) of object;
  TIntegerFunc = function(V: Integer): Boolean of object;
  TIntegerProc2 = procedure(V1,V2: Integer) of object;
  TIntegerProc3 = procedure(V1,V2,V3: Integer) of object;
  TIntegerProcN = procedure(ip: PIntegers) of object;
  TPixelProc = procedure(X,Y: Integer) of object;

  TRectProc = procedure(Sender: TObject; const Rect: TRect) of object;

  VPointProc = function(vp: PVPoly; N: Integer): Integer of object;

  TGaussProc = function(gp: PGPoly; N: Integer): Integer of object;

  TXyzProc = function(vp: pxyz_array; N: Integer): Integer of object;

  TProjectFunc = function(x,y: Double; pps: Integer): TPoint of object;

  TIntfFunc = function(out Obj): HResult of object; 

  TGeoProc = procedure(const P: TGeoPoint) of object;

  TTriWord = array[0..2] of Word;
  TTriWords = array[0..1023] of TTriWord;
  PTriWords = ^TTriWords;

  TCmyk = record c,m,y,k: Double end;
  thsv = record h,s,v: Double end;

const
  gsSolid = 0;
  gsHorz  = 1;
  gsVert  = 2;
  gsBack  = 3;
  gsForw  = 4;
  gsGrid  = 5;
  gsCross = 6;

  HexBit: array[0..15] of Word =
  ($8000,$4000,$2000,$1000,$0800,$0400,$0200,$0100,
   $0080,$0040,$0020,$0010,$0008,$0004,$0002,$0001);

  cFillMask: array[0..15] of TFillMask =
    (($FFFF,$FFFF,$FFFF,$FFFF, {0}
      $FFFF,$FFFF,$FFFF,$FFFF,
      $FFFF,$FFFF,$FFFF,$FFFF,
      $FFFF,$FFFF,$FFFF,$FFFF),

     ($FFFF,$0000,$0000,$0000, {1}
      $0000,$0000,$0000,$0000,
      $FFFF,$0000,$0000,$0000,
      $0000,$0000,$0000,$0000),

     ($8080,$8080,$8080,$8080, {2}
      $8080,$8080,$8080,$8080,
      $8080,$8080,$8080,$8080,
      $8080,$8080,$8080,$8080),

     ($8080,$4040,$2020,$1010, {3}
      $0808,$0404,$0202,$0101,
      $8080,$4040,$2020,$1010,
      $0808,$0404,$0202,$0101),

     ($0101,$0202,$0404,$0808, {4}
      $1010,$2020,$4040,$8080,
      $0101,$0202,$0404,$0808,
      $1010,$2020,$4040,$8080),

     ($FFFF,$8080,$8080,$8080, {5}
      $8080,$8080,$8080,$8080,
      $FFFF,$8080,$8080,$8080,
      $8080,$8080,$8080,$8080),

     ($8181,$4242,$2424,$1818, {6}
      $1818,$2424,$4242,$8181,
      $8181,$4242,$2424,$1818,
      $1818,$2424,$4242,$8181),

     ($0000,$0000,$0000,$0000, {7}
      $0000,$0000,$0000,$0000,
      $0000,$0000,$0000,$0000,
      $0000,$0000,$0000,$0000),

     ($AAAA,$0000,$5555,$0000, {8}
      $AAAA,$0000,$5555,$0000,
      $AAAA,$0000,$5555,$0000,
      $AAAA,$0000,$5555,$0000),

     ($FFFF,$0000,$0000,$0000, {9}
      $FFFF,$0000,$0000,$0000,
      $FFFF,$0000,$0000,$0000,
      $FFFF,$0000,$0000,$0000),

     ($8888,$8888,$8888,$8888, {A}
      $8888,$8888,$8888,$8888,
      $8888,$8888,$8888,$8888,
      $8888,$8888,$8888,$8888),

     ($8888,$4444,$2222,$1111, {B}
      $8888,$4444,$2222,$1111,
      $8888,$4444,$2222,$1111,
      $8888,$4444,$2222,$1111),

     ($1111,$2222,$4444,$8888, {C}
      $1111,$2222,$4444,$8888,
      $1111,$2222,$4444,$8888,
      $1111,$2222,$4444,$8888),

     ($FFFF,$8888,$8888,$8888, {D}
      $FFFF,$8888,$8888,$8888,
      $FFFF,$8888,$8888,$8888,
      $FFFF,$8888,$8888,$8888),

     ($8888,$0000,$2222,$0000, {E}
      $8888,$0000,$2222,$0000,
      $8888,$0000,$2222,$0000,
      $8888,$0000,$2222,$0000),

     ($8080,$0000,$0000,$0000, {F}
      $0808,$0000,$0000,$0000,
      $8080,$0000,$0000,$0000,
      $0808,$0000,$0000,$0000));

  EGA_Colors: THexColors =
    ($00000000,
     $007F0000,
     $00007F00,
     $007F7F00,
     $0000007F,
     $007F007F,
     $00007F7F,
     $00C0C0C0,
     $007F7F7F,
     $00FF0000,
     $0000FF00,
     $00FFFF00,
     $000000FF,
     $00FF00FF,
     $0000FFFF,
     $00FFFFFF);

  EGA_Quads: array[0..15] of Longint =
    ($00000000,
     $0000007F,
     $00007F00,
     $00007F7F,
     $007F0000,
     $007F007F,
     $007F7F00,
     $00C0C0C0,
     $007F7F7F,
     $000000FF,
     $0000FF00,
     $0000FFFF,
     $00FF0000,
     $00FF00FF,
     $00FFFF00,
     $00FFFFFF);

var
  sys_CYCaption: Integer;
  sys_CXVScroll: Integer;
  sys_CYHScroll: Integer;

//procedure Reset_ansi;

function xEqual(f1,f2: Double): Boolean;
function eps_Equal(f1,f2,eps: Double): Boolean;

function _mult(i1,i2: Integer): Int64;
{function _int64(i0,i1: Integer): Int64;}

function Long_Words(w1,w2: Integer): Integer;
function Long_Bytes(b0,b1,b2,b3: Integer): Integer;

function _Range(min,max: Double): TRange;
procedure Range_inc(var r: TRange; v: Double);

function Gauss_nil: TGauss;

function _Geoid(b,l: Double): TGeoid;
function _Gauss(x,y: Double): TGauss;

function _XPoint(x,y: Integer): XPoint;
function _LPoint(x,y: Integer): LPoint;
function _VPoint(x,y,z: Integer): VPoint;
function _lxyz(x,y,z: Double): VPoint;

function _LGauss(x,y: Double): LPoint;
function _IGauss(const g: TGauss): LPoint;

function _XRect(x,y,w,h: Integer): XRect;

function _cmyk(c,m,y,k: Double): TCmyk;

function dat7_dat3(const dat: TDatum7): TDatum;
function dat3_dat7(const dat: TDatum): TDatum7;

function sys3_sys7(const s: tsys3): tsys;

function sys7(pps,prj,elp: Integer; b1,b2,lc: Double): tsys;

function sys_nil: tsys;
function sys3_nil: tsys3;

function sys_wgs84: tsys;
function sys_ru42: tsys;
function sys_ru95: tsys;

function _XGeoid(x,y: Double; elp,prj,pps: Integer): XGeoid;
function XGeoid_nil: XGeoid;

function _xyz(x,y,z: Double): txyz;

function xy_Point(const v: vpoint): LPoint;

function _Locator(X,Y,R: Integer): TRect;

function Range_radius(const r: trange): Double;

procedure _GOrient(Dst,Src: PGPoly);

procedure Datum_norm(var v: TDatum7);
procedure Datum_disp(var v: TDatum7);

function Datum7(dX,dY,dZ,wx,wy,wz,m: Double): TDatum7;
function IsDatum7(Dat: PDatum7): Boolean;

function nil_Datum7: TDatum7;

function cu_Datum: TDatum7;
function ru42_Datum: TDatum7;
function ru95_Datum: TDatum7;

function ru_elp: tsys;

function Is_ru42_Datum(const dat: TDatum7): Boolean;
function is_nil_Datum7(const dat: TDatum7): Boolean;

function dat_Equal(const s1,s2: tsys): Boolean;
function elp_Equal(elp1,elp2: Integer): Boolean;
function sys_plan(const s: tsys): Boolean;

function xGetMem(len: Longint): pointer;
function zGetMem(len: Longint): pointer;
function xFreeMem(p: pointer; len: Longint): pointer;

function xAllocPtr(len: Integer): pointer;
function xReAllocPtr(P: Pointer; len: Integer): Pointer;
function xFreePtr(p: pointer): pointer;

function xAllocInt(n: Integer): PIntegers;
function xReallocInt(P: PIntegers; n: Integer): PIntegers;

function xAllocWords(n: Integer): PWords;
function xAllocInt64(n: Integer): PInt64s;
function xAllocDoubles(n: Integer): PDoubles;
function xAllocFloats(n: Integer): PFloats;

function xAllocLPoints(n: Integer): PLPoly;
function xReallocLPoints(P: PLPoly; n: Integer): PLPoly;

function xAllocVPoints(n: Integer): PVPoly;
function xReallocVPoints(P: PVPoly; n: Integer): PVPoly;

function xAllocGauss(n: Integer): PGPoly;
function xReallocGauss(P: PGPoly; n: Integer): PGPoly;

function Alloc_LLine(n: Integer): PLLine;
function Alloc_VLLine(n: Integer): PVLine;

function Realloc_LLine(lp: PLLine; n: Integer): PLLine;

function Alloc_PolyLine(n: Integer): PPolyLine;

function Alloc_ZLine(out lp_z: PIntegers;
                     lp_max: Integer): PLLine;

function xResizeBuf(Buf: Pointer; NewSize: Integer;
                    var BufSize: Integer): Pointer;

function xResizeInt(Buf: Pointer; NewCount: Integer;
                    var BufCount: Integer): Pointer;

function xAllocFile(len: Integer; out vm: TMapView): pointer;
function xReadOnlyFile(h: Integer; out vm: TMapView): pointer;
function xUpdateFile(h: Integer; out vm: TMapView): Pointer;
procedure xFreeFile(var vm: TMapView);

function xCreateFileMap(len: Integer; fn: PChar;
                        out vm: TMapView): Pointer;

function xOpenFileMap(fn: PChar; out vm: TMapView): Pointer;

function GetTotalMemorySize: DWord;
function GetAvailMemorySize: DWord;

function xResolution(w,h: Double): Double;

function xDeviceRatio(Dev: HDC): Double;
function xDevice_dpi(DC: HDC): Integer;
function xDeviceAspect(DC: HDC): Double;

function MetersPerPixel(DC: HDC): Double;
function xMetersPerPixel: Double;

function PixelsPerMeter(DC: HDC): TGauss;
function xPixelsPerMeter: TGauss;
function xDeviceDpi: Integer;

function xDeviceStep(Dev: HDC; st: Integer): Integer;

function BitsPerPixel(DC: hDC): Integer;
function xBitsPerPixel: Integer;

function xCreateCompatibleBitMap(Width,Height: integer): hBitMap;

function xPixels(dist,scale: Double): Longint;
function xPenThick(DC: HDC; mm: Float): Integer;

function Param_key(key: PChar): Boolean;

function Param_Option(opt: PChar): Boolean;
function xParamCount: Integer;

function xRange(i, a,b: Double): Double;
function iRange(i, a,b: Integer): Integer;

function iIndex(ind,max: Integer): Integer;

procedure xmove(si,di: PBytes; len: Integer);

function Int3(P: Pointer): Integer;

procedure pSwap(var p1,p2: Pointer);
procedure iSwap(var a,b: Integer);
procedure cSwap(var a,b: Cardinal);
procedure wSwap(var a,b: Word);
procedure xSwap(var a,b: Double);
procedure fSwap(var a,b: Float);
procedure bSwap(var a,b: Byte);

function Long2i(a,b: Integer): Longint;

function xBytesCount(bp: PBytes; N: Integer): Integer;

function xCountersCount(cp: PIntegers; N: Integer): Int64;

function CountersCount(cp: PIntegers; N: Integer): Integer;
function CountersMax(cp: PIntegers; N: Integer): Integer;

function IntegersAve(lp: PIntegers; N: Integer): Integer;

function Channel_Indexof(Chan: PBytes; Count: Integer;
                         Item: Integer): Integer;

function Int_Contains(Items: PIntegers; Count: Integer;
                      Item: Integer): Integer;

function Int_Delete(Items: PIntegers; Count: Integer;
                    Index: Integer): Integer;

function lp_Contains_Index(lp: PLLine;
                           Id: Integer): Integer;

function Index_Contains(Items: PInt64s; Count: Integer;
                        Id: Integer): Integer;

function x_Index_Contains(Items: PInt64s; Count: Integer;
                          Id: Integer): Integer;

procedure Get_MinMax(vp: PIntegers; N: Integer;
                     min,max: PInteger);

procedure words_MinMax(vp: PWords; N: Integer;
                       out vmin,vmax: Word);

procedure small_MinMax(vp: PSmallInts; N: Integer;
                       out vmin,vmax: Integer);

function z_axe_ptr(hp: PIntegers; N: Integer): PIntegers;

function int_Compare(p1: PIntegers; Count1: Integer;
                     p2: PIntegers; Count2: Integer): Boolean;

function x_Index_Sort(Items: PInt64s; Count: Integer): Integer;

procedure Inc_vector(X: PIntegers; Count,dX: Integer);
procedure Inc_array(X,dX: PIntegers; Count: Integer);

function SwapInt(i: Integer): Integer;
function SwapSingle(f: Single): Single;
function SwapWord(w: Word): Word;

function int_Time(T: TDateTime): Integer;

procedure byte_set_Checked(var s: tbyte_set;
                           v: Integer; chk: Boolean);

function ByteInt(b: Byte): Integer;

procedure Max_Gauss_Bound(G: PGPoly; N: Integer;
                          out lt,rb: tgauss);

procedure time_beg(var dt: Integer);
procedure time_end(var dt: Integer);



implementation


{procedure Reset_ansi;
begin
  ansi_draw:=false;
  ansi_pack:=false;
end;
}

function xEqual(f1,f2: Double): Boolean;
begin
  Result:=Abs(f1-f2) <= Small
end;

function eps_Equal(f1,f2,eps: Double): Boolean;
begin
  Result:=Abs(f1-f2) <= eps
end;

function _mult(i1,i2: Integer): Int64;
begin
  Result:=i1; Result:=Result * i2
end;

{function _int64(i0,i1: Integer): Int64;
var
  v: TInt64;
begin
  v.i[0]:=i0; v.i[1]:=i1;
  Result:=Int64(v)
end;
}
function Long_Words(w1,w2: Integer): Integer;
asm
  shl  EDx,16
  add  EAx,EDx
end;

function Long_Bytes(b0,b1,b2,b3: Integer): Integer;
var
  ax: tlong;
begin
  ax.b[0]:=b0; ax.b[1]:=b1;
  ax.b[2]:=b2; ax.b[3]:=b3;
  Result:=ax.i
end;

function _Geoid(b,l: Double): TGeoid;
begin
  Result.b:=b; Result.l:=l
end;

function _Range(min,max: Double): TRange;
begin
  Result.min:=min; Result.max:=max;
end;

procedure Range_inc(var r: TRange; v: Double);
begin
  if v < r.min then r.min:=v;
  if v > r.max then r.max:=v;
end;

function Gauss_nil: TGauss;
begin
  Result.x:=0; Result.y:=0
end;

function _Gauss(x,y: Double): TGauss;
begin
  Result.x:=x; Result.y:=y
end;

function _LPoint(x,y: Integer): LPoint;
begin
  Result.x:=x; Result.y:=y
end;

function _XPoint(x,y: Integer): XPoint;
begin
  Result.p.x:=x; Result.p.y:=y;
  Result.g.x:=x; Result.g.y:=y;
end;

function _VPoint(x,y,z: Integer): VPoint;
begin
  Result.x:=x; Result.y:=y; Result.z:=z;
end;

function _lxyz(x,y,z: Double): VPoint;
begin
  Result.x:=Round(x);
  Result.y:=Round(y);
  Result.z:=Round(z);
end;

function _LGauss(x,y: Double): LPoint;
begin
  Result.x:=Round(x);
  Result.y:=Round(y);
end;

function _IGauss(const g: TGauss): LPoint;
begin
  Result.x:=Round(g.x);
  Result.y:=Round(g.y);
end;

function _XRect(x,y,w,h: Integer): XRect;
begin
  Result.x:=x; Result.y:=y;
  Result.w:=w; Result.h:=h;
end;

function _cmyk(c,m,y,k: Double): TCmyk;
begin
  Result.c:=c;
  Result.m:=m;
  Result.y:=y;
  Result.k:=k;
end;

function _xyz(x,y,z: Double): txyz;
begin
  Result.x:=x; Result.y:=y; Result.z:=z;
end;

function Datum7(dX,dY,dZ,wX,wY,wZ,m: Double): TDatum7;
begin
  Result.dX:=dX; Result.dY:=dY; Result.dZ:=dZ;
  Result.wX:=wX; Result.wY:=wY; Result.wZ:=wZ;
  Result.m:=m
end;

function dat7_dat3(const dat: TDatum7): TDatum;
begin
  Result.x:=Round(dat.dX);
  Result.y:=Round(dat.dY);
  Result.z:=Round(dat.dZ);
end;

function dat3_dat7(const dat: TDatum): TDatum7;
begin
  with dat do
  Result:=Datum7(x,y,z, 0,0,0,0)
end;

function sys3_sys7(const s: tsys3): tsys;
var
  sys: tsys;
begin
  Fillchar(sys,Sizeof(sys),0);
  sys.pps:=s.pps;
  sys.prj:=s.prj;
  sys.elp:=s.elp;
  sys.b1:=s.b1;
  sys.b2:=s.b2;
  sys.lc:=s.lc;
  sys.rn:=s.rn;
  sys.dat:=dat3_dat7(s.dat);
  Result:=sys
end;

function sys7(pps,prj,elp: Integer; b1,b2,lc: Double): tsys;
var
  s: tsys;
begin
  FillChar(s,SizeOf(s),0);
  s.pps:=pps; s.prj:=prj; s.elp:=elp;
  s.b1:=b1; s.b2:=b2; s.lc:=lc;

  if (pps = 1) and (elp = 1) then
  s.dat:=ru42_Datum; Result:=s
end;

function sys_nil: tsys;
begin
  Fillchar(Result,Sizeof(tsys),0);
end;

function sys3_nil: tsys3;
begin
  Fillchar(Result,Sizeof(tsys),0);
end;

function sys_wgs84: tsys;
begin
  Fillchar(Result,Sizeof(tsys),0);
  Result.elp:=9
end;

function sys_ru42: tsys;
begin
  Result:=sys_nil; Result.elp:=1;
  Result.dat:=ru42_Datum
end;

function sys_ru95: tsys;
begin
  Result:=sys_nil; Result.elp:=1;
  Result.dat:=ru95_Datum
end;

function _XGeoid(x,y: Double; elp,prj,pps: Integer): XGeoid;
begin
  FillChar(Result,SizeOf(XGeoid),0);
  Result.s.elp:=elp; Result.s.prj:=prj;
  Result.s.pps:=pps; Result.x:=x; Result.y:=y;
end;

function XGeoid_nil: XGeoid;
begin
  FillChar(Result,SizeOf(XGeoid),0);
end;

procedure Datum_norm(var v: TDatum7);
begin
  v.wx:=v.wx / 60 / 60 / 180 * Pi;
  v.wy:=v.wy / 60 / 60 / 180 * Pi;
  v.wz:=v.wz / 60 / 60 / 180 * Pi;
  v.m:=v.m * 1E-6;
end;

procedure Datum_disp(var v: TDatum7);
begin
  v.wx:=v.wx * 60 * 60 * 180 / Pi;
  v.wy:=v.wy * 60 * 60 * 180 / Pi;
  v.wz:=v.wz * 60 * 60 * 180 / Pi;
  v.m:=v.m / 1E-6;
end;

function IsDatum7(Dat: PDatum7): Boolean;
begin
  Result:=false;
  if Assigned(Dat) then
  if (Abs(Dat.dX) >= 0.01)
  or (Abs(Dat.dY) >= 0.01)
  or (Abs(Dat.dZ) >= 0.01)
  or (Abs(Dat.wx) >= 0.01)
  or (Abs(Dat.wy) >= 0.01)
  or (Abs(Dat.wz) >= 0.01) then
  Result:=true
end;

function nil_Datum7: TDatum7;
var
  dat: TDatum7;
begin
  Fillchar(dat,Sizeof(dat),0);
  Result:=dat
end;

function cu_Datum: TDatum7;
var
  s: TDatum7;
begin
  s.dX:=2.478;
  s.dY:=149.752;
  s.dZ:=197.726;
  s.wX:=-0.526356;
  s.wY:=-0.497970;
  s.wZ:=0.500831;
  s.m:=0.685238;

  Datum_norm(s); Result:=s
end;

function ru42_Datum: TDatum7;
begin
  Result:=Datum7(28,-130,-95, 0,0,0,0)
end;

function ru95_Datum: TDatum7;
var
  s: TDatum7;
begin
  s.dX:=26.3;
  s.dY:=-132.6;
  s.dZ:=-76.3;
  s.wX:=-0.22;
  s.wY:=-0.40;
  s.wZ:=-0.90;
  s.m:=-0.12;

  Datum_norm(s); Result:=s
end;

function Is_ru42_Datum(const dat: TDatum7): Boolean;
begin
  Result:=is_nil_Datum7(dat);
  if not Result then with dat do
  if Round(dX) = 28 then
  if Round(dY) = -130 then
  if Round(dZ) = -95 then
  Result:=true
end;

function is_nil_Datum7(const dat: TDatum7): Boolean;
begin
  Result:=false; with dat do
  if Round(dX) = 0 then
  if Round(dY) = 0 then
  if Round(dZ) = 0 then
  Result:=true
end;

function ru_elp: tsys;
var
  s: tsys;
begin
  FillChar(s,Sizeof(s),0);
  s.elp:=1; s.dat:=ru42_Datum;
  Result:=s
end;

function dat_Equal(const s1,s2: tsys): Boolean;
begin
  Result:=false;
  if s1.elp = s2.elp then
  if Abs(s1.dat.dX - s2.dat.dX) < 1 then
  if Abs(s1.dat.dY - s2.dat.dY) < 1 then
  if Abs(s1.dat.dZ - s2.dat.dZ) < 1 then
  Result:=true
end;

function elp_Equal(elp1,elp2: Integer): Boolean;
begin
  if elp1 = 0 then elp1:=elp2;
  if elp2 = 0 then elp2:=elp1;
  Result:=elp1 = elp2
end;

function sys_plan(const s: tsys): Boolean;
begin
  with s do
  Result:=(pps = 0) and
          (elp = 0) and
          (prj = 0)
end;

function xy_Point(const v: vpoint): LPoint;
begin
  Result.x:=v.x; Result.y:=v.y;
end;

function _Locator(X,Y,R: Integer): TRect;
begin
  Result:=Rect(X-R,Y-R,X+R,Y+R)
end;

function Range_radius(const r: trange): Double;
begin
  Result:=(r.max - r.min) / 2
end;

procedure _GOrient(Dst,Src: PGPoly);
var
  I: Integer;
begin
  for I:=0 to Orient_Max-1 do Dst[I]:=Src[I]
end;

function xGetMem(len: Integer): pointer;
begin
  Result:=nil;
  try
    GetMem(Result,len);
  except
    on EOutOfMemory do Result:=nil
  end
end;

function zGetMem(len: Integer): pointer;
begin
  Result:=xGetMem(len); if Result <> nil then
  FillChar(Result^,len,0)
end;

function xFreeMem(p: pointer; len: Integer): pointer;
begin
  if p <> nil then FreeMem(p,len); Result:=nil
end;

function xAllocPtr(len: Integer): Pointer;
begin
  Result:=nil; if len > 0 then begin
    Result:=GlobalAllocPtr(0,len);
    if Assigned(Result) then
    FillChar(Result^,len,0)
  end
end;

function xReAllocPtr(P: Pointer; len: Integer): Pointer;
var
  old: Integer;
begin
  Result:=nil;

  if Assigned(P) then
  Result:=GlobalReAllocPtr(P,len,0);

  if len > 0 then
  if Result = nil then begin
    Result:=xAllocPtr(len);

    old:=0; if Assigned(P) then
    old:=GlobalSize(GlobalHandle(P));
    if (old > 0) and (old < len) then
    if Assigned(Result) then
    Move(P^,Result^,old);

    xFreePtr(P)
  end
end;

function xFreePtr(p: pointer): pointer;
begin
  if p <> nil then GlobalFreePtr(p);
  Result:=nil
end;

function xAllocWords(n: Integer): PWords;
begin
  Result:=xAllocPtr(n * SizeOf(Word))
end;

function xAllocInt(n: Integer): PIntegers;
begin
  Result:=xAllocPtr(n * SizeOf(Integer))
end;

function xReallocInt(P: PIntegers; n: Integer): PIntegers;
begin
  Result:=xReallocPtr(P,n * SizeOf(Integer))
end;

function xAllocInt64(n: Integer): PInt64s;
begin
  Result:=xAllocPtr(n * SizeOf(Int64))
end;

function xAllocDoubles(n: Integer): PDoubles;
begin
  Result:=xAllocPtr(n * SizeOf(Double))
end;

function xAllocFloats(n: Integer): PFloats;
begin
  Result:=xAllocPtr(n * SizeOf(Float))
end;

function xAllocLPoints(n: Integer): PLPoly;
begin
  Result:=xAllocPtr(n * SizeOf(LPoint))
end;

function xReallocLPoints(P: PLPoly; n: Integer): PLPoly;
begin
  Result:=xReAllocPtr(P,n * SizeOf(LPoint))
end;

function xAllocVPoints(n: Integer): PVPoly;
begin
  Result:=xAllocPtr(n * SizeOf(VPoint))
end;

function xReallocVPoints(P: PVPoly; n: Integer): PVPoly;
begin
  Result:=xReAllocPtr(P,n * SizeOf(VPoint))
end;

function xAllocGauss(n: Integer): PGPoly;
begin
  Result:=xAllocPtr(n * SizeOf(TGauss))
end;

function xReallocGauss(P: PGPoly; n: Integer): PGPoly;
begin
  Result:=xReallocPtr(P,n * SizeOf(TGauss))
end;

function Alloc_LLine(n: Integer): PLLine;
begin
  Result:=xAllocPtr((n+1)*SizeOf(LPoint));
  if Result <> nil then Result.N:=n-1
end;

function Realloc_LLine(lp: PLLine; n: Integer): PLLine;
var
  cx: Integer;
begin
  cx:=(n+1)*SizeOf(LPoint);
  Result:=xReallocPtr(lp,cx);
end;

function Alloc_VLLine(n: Integer): PVLine;
begin
  Result:=xAllocPtr((n+1) * SizeOf(VPoint));
  if Result <> nil then Result.N:=n-1
end;

function Alloc_PolyLine(n: Integer): PPolyLine;
begin
  Result:=xAllocPtr((n+1)*SizeOf(LPoint));
  if Result <> nil then Result.N:=n-1
end;

function Alloc_ZLine(out lp_z: PIntegers;
                     lp_max: Integer): PLLine;
var
  cx: Integer;
begin
  cx:=lp_max+1; lp_z:=nil;
  Result:=Alloc_LLine(cx + (cx div 2));
  if Assigned(Result) then begin
    lp_z:=@Result.Pol[cx];
    Result.N:=-1;
  end
end;

function xResizeBuf(Buf: Pointer; NewSize: Integer;
                    var BufSize: Integer): Pointer;
begin
  if NewSize > 0 then

  if Buf = nil then begin
    Buf:=xAllocPtr(NewSize);
    BufSize:=NewSize
  end else
  if NewSize > BufSize then begin
    Buf:=xReallocPtr(Buf,NewSize);
    BufSize:=NewSize
  end;

  Result:=Buf
end;

function xResizeInt(Buf: Pointer; NewCount: Integer;
                    var BufCount: Integer): Pointer;
var
  sz: Integer;
begin
  sz:=BufCount*4;
  Result:=xResizeBuf(Buf,NewCount*4,sz);
  BufCount:=sz div 4
end;

function xAllocFile(len: Integer; out vm: TMapView): Pointer;
begin
  Result:=nil; with vm do begin
    h:=CreateFileMapping($FFFFFFFF,nil,PAGE_READWRITE,0,len,nil);
    if h <> 0 then Result:=MapViewOfFile(h,FILE_MAP_WRITE,0,0,0);
    p:=Result; size:=0; if Result <> nil then size:=len
  end
end;

function xReadOnlyFile(h: Integer; out vm: TMapView): Pointer;
begin
  Result:=nil; vm.p:=nil; vm.size:=FileSeek(h,0,2);
  vm.h:=CreateFileMapping(h,nil,PAGE_READONLY,0,0,nil);

  if vm.h <> 0 then begin
    Result:=MapViewOfFile(vm.h,FILE_MAP_READ,0,0,0);
    if Result = nil then begin
      CloseHandle(vm.h); vm.h:=0
    end
  end;

  vm.p:=Result;
end;

function xUpdateFile(h: Integer; out vm: TMapView): Pointer;
begin
  Result:=nil; vm.p:=nil; vm.size:=FileSeek(h,0,2);
  vm.h:=CreateFileMapping(h,nil,PAGE_READWRITE,0,0,nil);
  if vm.h <> 0 then Result:=MapViewOfFile(vm.h,FILE_MAP_WRITE,0,0,0);
  vm.p:=Result
end;

procedure xFreeFile(var vm: TMapView);
begin
  with vm do if h <> 0 then begin
    if p <> nil then UnmapViewOfFile(p);
    CloseHandle(h);
  end;

  vm.h:=0; vm.p:=nil; vm.size:=0;
end;

function xCreateFileMap(len: Integer; fn: PChar;
                        out vm: TMapView): Pointer;
begin
  Result:=nil; with vm do begin
    h:=CreateFileMapping($FFFFFFFF,nil,PAGE_READWRITE,0,len,fn);
    if h <> 0 then Result:=MapViewOfFile(h,FILE_MAP_WRITE,0,0,0);
    p:=Result; size:=0; if Result <> nil then size:=len
  end
end;

function xOpenFileMap(fn: PChar; out vm: TMapView): Pointer;
begin
  Result:=nil; with vm do begin
    h:=OpenFileMapping(FILE_MAP_WRITE, False, fn);
    if h <> 0 then Result:=MapViewOfFile(h,FILE_MAP_WRITE,0,0,0);
    p:=Result; size:=0;
  end
end;

function GetTotalMemorySize: DWord;
var
  mst: TMEMORYSTATUS;
begin
  GlobalMemoryStatus(mst);
  Result:=mst.dwTotalPhys div 1024 div 1024
end;

function GetAvailMemorySize: DWord;
var
  mst: TMEMORYSTATUS;
begin
  GlobalMemoryStatus(mst);
  Result:=mst.dwAvailPhys div 1024 div 1024
end;

function xResolution(w,h: Double): Double;
var
  DC: hDC; kx,ky: Double;
begin
  DC:=GetDC(0);
  kx:=w/GetDeviceCaps(DC,HorzRes);
  ky:=h/GetDeviceCaps(DC,VertRes);
  Result:=Math.Max(kx,ky);
  ReleaseDC(0,DC);
end;

function xDeviceRatio(Dev: HDC): Double;
var
  DC: HDC; dev_ppi,dc_ppi: Integer;
begin
  Result:=1;

  DC:=GetDC(0);
  if DC <> 0 then begin
    dev_ppi:=GetDeviceCaps(DC,LogPixelSx);
    dc_ppi:=GetDeviceCaps(DC,LogPixelSx);
    if (dev_ppi > 0) and (dc_ppi > 0) then
    Result:=dev_ppi / dc_ppi
  end
end;

function xDevice_dpi(DC: HDC): Integer;
begin
  Result:=GetDeviceCaps(DC,LogPixelSx)
end;

function xDeviceAspect(DC: HDC): Double;
var
  mpp,mpp0: Double;
begin
  Result:=1;
  if DC <> 0 then begin
    mpp:=MetersPerPixel(DC);
    mpp0:=xMetersPerPixel;
    if mpp > 1E-8 then
    Result:=mpp0 / mpp
  end
end;

function MetersPerPixel(DC: HDC): Double;
var
  ppi: Integer;
begin
  Result:=0;
  ppi:=GetDeviceCaps(DC,LogPixelSx);
  if ppi > 0 then Result:=0.0254 / ppi
end;

function xMetersPerPixel: Double;
var
  DC: hDC;
begin
  DC:=GetDC(0);
  Result:=MetersPerPixel(DC);
  ReleaseDC(0,DC)
end;

function xDeviceDpi: Integer;
var
  DC: hDC;
begin
  DC:=GetDC(0);
  Result:=GetDeviceCaps(DC,LogPixelSx);
  ReleaseDC(0,DC)
end;

function PixelsPerMeter(DC: HDC): TGauss;
begin
  Result.x:=GetDeviceCaps(DC,LogPixelSx) / 0.0254;
  Result.y:=GetDeviceCaps(DC,LogPixelSy) / 0.0254;
end;

function xPixelsPerMeter: TGauss;
var
  DC: hDC;
begin
  DC:=GetDC(0);
  Result:=PixelsPerMeter(DC);
  ReleaseDC(0,DC)
end;

function xDeviceStep(Dev: HDC; st: Integer): Integer;
var
  ppm1,ppm2: TGauss;
begin
  Result:=st;

  ppm1:=xPixelsPerMeter;
  ppm2:=PixelsPerMeter(Dev);

  if ppm1.x >= 1 then
  if ppm2.x >= 1 then
  Result:=Round(st / ppm1.x * ppm2.x);
end;

function BitsPerPixel(DC: hDC): Integer;
begin
  Result:=GetDeviceCaps(DC, BITSPIXEL) *
          GetDeviceCaps(DC, PLANES);
end;

function xBitsPerPixel: Integer;
var
  DC: hDC;
begin
  DC:=GetDC(0);
  Result:=BitsPerPixel(DC);
  ReleaseDC(0,DC)
end;

function xCreateCompatibleBitMap(Width,Height: Integer): hBitMap;
var
  DC: hDC;
begin
  DC:=GetDC(0);
  Result:=CreateCompatibleBitMap(DC,Width,Height);
  ReleaseDC(0,DC)
end;

function xPixels(dist,scale: Double): Longint;
begin
  Result:=0; if scale > 0.1 then
  Result:=Round(dist / scale / xMetersPerPixel + 0.5)
end;

function xPenThick(DC: HDC; mm: Float): Integer;
var
  dpi: Integer;
begin
  dpi:=GetDeviceCaps(DC,LogPixelSx);
  Result:=Round(dpi / 25.4 * mm);
end;

function Param_key(key: PChar): Boolean;
var
  i: Integer; s: TShortstr;
begin
  Result:=false;

  for i:=1 to ParamCount do
  if StrPCopy(s,ParamStr(i)) <> nil then
  if StrIComp(s,key) = 0 then begin
    Result:=true; Break
  end
end;

function Param_Option(opt: PChar): Boolean;
var
  i: Integer;
begin
  Result:=false;

  for i:=1 to ParamCount do
  if ParamStr(i) = StrPas(opt) then begin
    Result:=true; Break
  end

end;

function xParamCount: Integer;
var
  i: Integer;
begin
  Result:=ParamCount;

  for i:=1 to ParamCount do
  if ParamStr(i)[1] = '/' then Dec(Result)
  else Break
end;

function xRange(i, a,b: Double): Double;
begin
  Result:=Min(Max(i,a),b)
end;

function iRange(i, a,b: Integer): Integer;
begin
  if i < a then i:=a;
  if i > b then i:=a;
  Result:=i;
end;

function iIndex(ind,max: Integer): Integer;
asm
  cmp  EAx,0
  jl   @err
  cmp  EAx,EDx
  jl   @exit

@err:
  mov  EAx,0
@exit:
end;

procedure xmove(si,di: PBytes; len: Integer);
var
  cx: Integer;
begin
  si:=@si[len]; di:=@di[len];

  while len > 0 do begin cx:=Min(4096,len);
    Dec(Longint(si),cx); Dec(Longint(di),cx);
    Move(si^,di^,cx); Dec(len,cx)
  end
end;

procedure pSwap(var p1,p2: Pointer);
asm
  push ESi
  mov  ESi,EAx
  mov  EAx,DWord PTR [ESi]
  mov  ECx,DWord PTR [EDx]
  mov  DWord PTR [ESi],ECx
  mov  DWord PTR [EDx],EAx
  pop  ESi
end;

procedure iSwap(var a,b: Integer);
asm
  push ESi
  mov  ESi,EAx
  mov  EAx,DWord PTR [ESi]
  mov  ECx,DWord PTR [EDx]
  mov  DWord PTR [ESi],ECx
  mov  DWord PTR [EDx],EAx
  pop  ESi
end;

procedure cSwap(var a,b: Cardinal);
asm
  push ESi
  mov  ESi,EAx
  mov  EAx,DWord PTR [ESi]
  mov  ECx,DWord PTR [EDx]
  mov  DWord PTR [ESi],ECx
  mov  DWord PTR [EDx],EAx
  pop  ESi
end;

procedure wSwap(var a,b: Word);
asm
  push ESi
  mov  ESi,EAx
  mov  Ax,Word PTR [ESi]
  mov  Cx,Word PTR [EDx]
  mov  Word PTR [ESi],Cx
  mov  Word PTR [EDx],Ax
  pop  ESi
end;

procedure xSwap(var a,b: Double); var ax: Double;
begin ax:=a; a:=b; b:=ax end;

procedure fSwap(var a,b: Float); var ax: FLoat;
begin ax:=a; a:=b; b:=ax end;

procedure bSwap(var a,b: Byte); var al: Byte;
begin al:=a; a:=b; b:=al end;

function Long2i(a,b: Integer): Longint;
begin
  tlong(Result).w[0]:=a;
  tlong(Result).w[1]:=b;
end;

function xBytesCount(bp: PBytes; N: Integer): Integer;
var
  i: Integer;
begin
  Result:=0;
  for i:=1 to N do begin
    Inc(Result,bp[0]); bp:=@bp[1]
  end
end;

function xCountersCount(cp: PIntegers; N: Integer): Int64;
var
  i: Integer;
begin
  Result:=0;
  for i:=1 to N do begin
    Inc(Result,cp[0]); cp:=@cp[1]
  end
end;

function CountersCount(cp: PIntegers; N: Integer): Integer;
asm
  push EBx
  push ESi

  mov  ESi,EAx
  mov  ECx,EDx

  cld
  mov  EBx,0
@loop:
  lodsd
  add  EBx,EAx
  loop @loop

  mov  EAx,EBx

  pop  ESi
  pop  EBx
end;

function CountersMax(cp: PIntegers; N: Integer): Integer;
asm
  push EBx
  push ESi

  mov  ESi,EAx
  mov  ECx,EDx

  cld
  mov  EBx,0
@loop:
  lodsd
  cmp  EBx,EAx
  jg   @next
  mov  EBx,EAx
@next:
  loop @loop

  mov  EAx,EBx

  pop  ESi
  pop  EBx
end;

function IntegersAve(lp: PIntegers; N: Integer): Integer;
var
  ax: Int64; i: Integer;
begin
  ax:=0; for i:=1 to N do begin
    Inc(ax,lp[0]); lp:=@lp[1];
  end; Result:=ax div N
end;

procedure Get_MinMax(vp: PIntegers; N: Integer;
                     min,max: PInteger);
begin
  asm
    push EAx
    push EBx
    push ECx
    push EDx
    push ESi
    push EDi

    cld
    mov  ESi,[vp]
    mov  ECx,[N]

    mov  EBx,DWord PTR [ESi]
    mov  EDx,DWord PTR [ESi]

    cmp  ECx,0
    jle  @skip
  @loop:
    lodsd
    cmp  EAx,EBx
    jl   @less
    cmp  EAx,EDx
    jg   @larger
    jmp  @next
@less:
    mov  EBx,EAx
    jmp  @next
@larger:
    mov  EDx,EAx
  @next:
    loop @loop

@skip:
    mov  EDi,[min]
    mov  DWord PTR [EDi],EBx
    mov  EDi,[max]
    mov  DWord PTR [EDi],EDx

    pop  EDi
    pop  ESi
    pop  EDx
    pop  ECx
    pop  EBx
    pop  EAx
  end
end;

procedure words_MinMax(vp: PWords; N: Integer;
                       out vmin,vmax: Word);
var
  v1,v2,v: Word;
begin
  vmin:=0; vmax:=0;
  if N > 0 then begin
    v1:=vp[0]; v2:=v1; Dec(N);

    while N > 0 do begin
      vp:=@vp[1]; Dec(N); v:=vp[0];
      if v < v1 then v1:=v;
      if v > v2 then v2:=v;
    end;

    vmin:=v1; vmax:=v2;
  end
end;

procedure small_MinMax(vp: PSmallInts; N: Integer;
                       out vmin,vmax: Integer);
var
  v1,v2,v: Integer;
begin
  vmin:=0; vmax:=0;
  if N > 0 then begin
    v1:=vp[0]; v2:=v1; Dec(N);

    while N > 0 do begin
      vp:=@vp[1]; Dec(N); v:=vp[0];
      if v < v1 then v1:=v;
      if v > v2 then v2:=v;
    end;

    vmin:=v1; vmax:=v2;
  end
end;

function Int3(P: Pointer): Integer;
asm
  push EBx
  push ESi

  mov  ESi,EAx
  mov  EBx,0

  lodsw
  mov  Bx,Ax

  mov  EAx,0
  lodsb
  shl  EAx,16
  or   EAx,EBx

  pop  ESi
  pop  EBx
end;

function Channel_Indexof(Chan: PBytes; Count: Integer;
                         Item: Integer): Integer;
asm
  push EBx
  push ESi

  mov  ESi,EAx
  mov  EBx,ECx
  mov  ECx,EDx

  cmp  ECx,0
  je   @not_found

  mov  EDx,0

  cld
@loop:
  lodsb
  cmp  Bl,Al
  je   @exit
  inc  EDx
  loop @loop

@not_found:
  mov  EDx,-1
@exit:
  mov  EAx,EDx

  pop  ESi
  pop  EBx
end;

function Int_Contains(Items: PIntegers; Count: Integer;
                      item: Integer): Integer;
asm
  push EBx
  push ESi

  mov  ESi,EAx
  mov  EBx,ECx
  mov  ECx,EDx

  cmp  ECx,0
  je   @not_found

  mov  EDx,0

  cld
@loop:
  lodsd
  cmp  EBx,EAx
  je   @exit
  inc  EDx
  loop @loop

@not_found:
  mov  EDx,-1
@exit:
  mov  EAx,EDx

  pop  ESi
  pop  EBx
end;

function Int_Delete(Items: PIntegers; Count: Integer;
                    Index: Integer): Integer;
begin
  if Index >= 0 then
  if Index < Count then begin
    Dec(Count); while Index < Count do begin
      Items[Index]:=Items[Index+1]; Inc(Index)
    end;
  end; Result:=Count
end;

function lp_Contains_Index(lp: PLLine;
                           Id: Integer): Integer;
begin
  Result:=-1; with lp^ do if N >= 0 then
  Result:=Index_Contains(@Pol,N+1, Id)
end;

function Index_Contains(Items: PInt64s; Count: Integer;
                        Id: Integer): Integer;
asm
  push EBx
  push ESi

  mov  ESi,EAx
  mov  EBx,ECx
  mov  ECx,EDx

  mov  EDx,-1

  cmp  ECx,0
  jle  @exit

  mov  EDx,0

  cld
@loop:
  lodsd
  cmp  EBx,EAx
  je   @exit
  add  ESi,4
  inc  EDx
  loop @loop


  mov  EDx,-1
@exit:
  mov  EAx,EDx

  pop  ESi
  pop  EBx
end;

function x_Index_Contains(Items: PInt64s; Count: Integer;
                          Id: Integer): Integer;
asm
  push EBx
  push ESi
  push EDi

  mov  ESi,EAx
  mov  EBx,ECx

  mov  ECx,0
  dec  EDx

@loop:
  cmp  ECx,EDx
  jg   @false

  mov  EAx,ECx
  add  EAx,EDx
  shr  EAx,1

  mov  EDi,EAx
  shl  EDi,3
  add  EDi,ESi

  cmp  EBx,DWord PTR [EDi]
  jb   @less
  je   @exit

@more:
  mov  ECx,EAx
  inc  ECx
  jmp  @loop

@less:
  mov  EDx,EAx
  dec  EDx
  jmp  @loop

@false:
  mov  EAx,-1

@exit:
  pop  EDi
  pop  ESi
  pop  EBx
end;

function z_axe_ptr(hp: PIntegers; N: Integer): PIntegers;
var
  zmin,zmax: Integer;
begin
  Result:=nil; if Assigned(hp) then begin
    Get_MinMax(hp,N, @zmin,@zmax);
    if (zmin <> 0) or (zmax <> 0) then
    Result:=hp
  end;
end;

function int_Compare(p1: PIntegers; Count1: Integer;
                     p2: PIntegers; Count2: Integer): Boolean;
var
  i: Integer;
begin
  Result:=Count1 = Count2;

  if Result then
  for i:=0 to Count1-1 do
  if p1[i] <> p2[i] then begin
    Result:=false; Break
  end
end;

function x_Index_Sort(Items: PInt64s; Count: Integer): Integer;
var
  i,j: Integer; p1,p2: PInt64s; ax,bx: TInt64;
begin
  p1:=Items;
  for i:=0 to Count-2 do begin

    ax.x:=p1[0]; p2:=@p1[1];

    for j:=i+1 to Count-1 do begin
      bx.x:=p2[0];
      if bx.c[0] < ax.c[0] then begin
        p2[0]:=ax.x; ax:=bx;
      end; p2:=@p2[1]
    end;

    p1[0]:=ax.x; p1:=@p1[1]
  end;

  Result:=Count
end;

function SwapInt(i: Integer): Integer;
asm
  BSWAP EAx
end;

function SwapSingle(f: Single): Single;
var
  v: Longint;
begin
  v:=PLongint(@f)^;
  
  asm
    mov   EAx,[v]
    BSWAP EAx;
    mov   [v],EAx
  end;

  Result:=PSingle(@v)^
end;

function SwapWord(w: Word): Word;
asm
  BSWAP EAx
  shr   EAx,16
end;

procedure Inc_vector(X: PIntegers; Count,dX: Integer);
var
  i: Integer;
begin
  for i:=1 to Count do begin
    Inc(X[0],dX); X:=@X[1]
  end
end;

procedure Inc_array(X,dX: PIntegers; Count: Integer);
var
  i: Integer;
begin
  for i:=1 to Count do begin
    Inc(X[0],dX[0]); X:=@X[1]; dX:=@dX[1]
  end
end;

function int_Time(T: TDateTime): Integer;
var
  Hour, Min, Sec, MSec: Word;
begin
  DecodeTime(T, Hour,Min,Sec,MSec);
  Result:=Hour; Result:=Result * 3600;
  Inc(Result, Min*60 + Sec)
end;

procedure byte_set_Checked(var s: tbyte_set;
                           v: Integer; chk: Boolean);
begin
  if chk then s:=s + [v]
  else        s:=s - [v]
end;

function ByteInt(b: Byte): Integer;
begin
  Result:=b; if b and $80 <> 0 then
  Result:=Result or $FFFFFF00;
end;

procedure Max_Gauss_Bound(G: PGPoly; N: Integer;
                          out lt,rb: tgauss);
var
  i: Integer; x1,y1,x2,y2: Double;
begin
  x1:=G[0].x; y1:=G[0].y; x2:=x1; y2:=y1;

  for i:=1 to N-1 do with G[i] do begin
    if x < x1 then x1:=x; if x > x2 then x2:=x;
    if y < y1 then y1:=y; if y > y2 then y2:=y;
  end;

  lt.x:=x1; lt.y:=y1; rb.x:=x2; rb.y:=y2;
end;

procedure time_beg(var dt: Integer);
begin
  Dec(dt,GetTickCount)
end;

procedure time_end(var dt: Integer);
begin
  Inc(dt,GetTickCount)
end;

// 5=WinXP; 6=Vista
function GetWinOS: Integer;
var
  ver: TOSVERSIONINFO;
begin
  Result:=0;

  Fillchar(ver,Sizeof(ver),0);
  ver.dwOSVersionInfoSize:=Sizeof(ver);
  if GetVersionEx(ver) then
  Result:=ver.dwMajorVersion ;
end;

 {
begin     

  sys_CYCaption:=GetSystemMetrics(sm_CYCaption);
  sys_CXVScroll:=GetSystemMetrics(sm_CXVScroll);
  sys_CYHScroll:=GetSystemMetrics(sm_CYHScroll);

  IsWin32:=Win32Platform = Ver_Platform_Win32_NT;

  WinOS:=GetWinOS;

  if rus_interface then
  rus_interface:=not Param_Option('/t');

  debug_enabled:=Param_Option('/d');
  log_enabled:=Param_Option('/log');
  view_interface:=Param_Option('/v');
  }

end.
