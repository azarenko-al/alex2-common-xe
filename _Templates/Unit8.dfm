object Form8: TForm8
  Left = 511
  Top = 542
  Width = 775
  Height = 446
  Caption = 'Form8'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Buttons: TPanel
    Left = 0
    Top = 349
    Width = 767
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 0
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 767
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 384
    Width = 767
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 1
    DesignSize = (
      767
      35)
    object Bevel2: TBevel
      Left = 0
      Top = 0
      Width = 767
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object Button3: TButton
      Left = 604
      Top = 8
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object Button4: TButton
      Left = 688
      Top = 8
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Cancel = True
      ModalResult = 2
      TabOrder = 1
    end
    object Button5: TButton
      Left = 4
      Top = 9
      Width = 153
      Height = 23
      TabOrder = 2
    end
    object Button6: TButton
      Left = 166
      Top = 9
      Width = 75
      Height = 23
      TabOrder = 3
    end
  end
  object PageControl1: TPageControl
    Left = 15
    Top = 61
    Width = 525
    Height = 271
    ActivePage = TabSheet1
    TabOrder = 6
    object TabSheet1: TTabSheet
      Caption = 'Splitters'
      object cxSplitter1: TcxSplitter
        Left = 0
        Top = 0
        Width = 517
        Height = 8
        HotZoneClassName = 'TcxSimpleStyle'
        AlignSplitter = salTop
      end
    end
  end
  object dxBarManager: TdxBarManager
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Bars = <
      item
        AllowQuickCustomizing = False
        Caption = 'Menu Bar'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 453
        FloatTop = 236
        FloatClientWidth = 319
        FloatClientHeight = 19
        ItemLinks = <
          item
            Item = siEdit
            Visible = True
          end
          item
            Item = siView
            Visible = True
          end
          item
            Item = siInsert
            Visible = True
          end
          item
            Item = siFormat
            Visible = True
          end
          item
            Item = siTools
            Visible = True
          end
          item
            Item = siTable
            Visible = True
          end
          item
            Item = siWindow
            Visible = True
          end
          item
            Item = siHelp
            Visible = True
          end>
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = False
      end
      item
        Caption = 'Standard'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 23
        DockingStyle = dsTop
        FloatLeft = 272
        FloatTop = 237
        FloatClientWidth = 312
        FloatClientHeight = 50
        ItemLinks = <
          item
            Item = btnNew
            Visible = True
          end
          item
            Item = btnOpen
            Visible = True
          end
          item
            Item = btnSave
            Visible = True
          end
          item
            BeginGroup = True
            Item = btnPrint
            Visible = True
          end
          item
            Item = btnPrintPreview
            Visible = True
          end
          item
            Item = btnSpellingandGrammar
            Visible = True
          end
          item
            BeginGroup = True
            Item = btnCut
            Visible = True
          end
          item
            Item = btnCopy
            Visible = True
          end
          item
            Item = btnPaste
            Visible = True
          end
          item
            Item = btnFormatPainter
            Visible = True
          end
          item
            BeginGroup = True
            Item = btnUndo
            Visible = True
          end
          item
            Item = btnRedo
            Visible = True
          end
          item
            BeginGroup = True
            Item = btnHyperlink
            Visible = True
          end
          item
            Item = btnTablesandBordersToolbar
            Visible = True
          end
          item
            Item = btnInsertTable
            Visible = True
          end
          item
            Item = dxBarButton4
            Visible = True
          end
          item
            Item = btnColumns
            Visible = True
          end
          item
            Item = btnWordArt
            Visible = True
          end
          item
            Item = btnDocumentMap
            Visible = True
          end
          item
            Item = btnShowAll
            Visible = True
          end
          item
            Item = cbZoom
            UserDefine = [udWidth]
            UserWidth = 51
            Visible = True
          end
          item
            Item = dxBarButton8
            Visible = True
          end>
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = True
        WholeRow = False
      end>
    Categories.Strings = (
      'File'
      'Edit'
      'View'
      'Insert'
      'Format'
      'Tools'
      'Table'
      'Window and Help'
      'Built-in Menus'
      'Internet')
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True)
    HelpButtonGlyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000010000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDD0
      DDDDDDDDDDDDDD00DDDDDDD7777770B07DDDDD0000000FF007DDD0FFFBFBFBFF
      F07DD0FBFFF44FFBF07DD0FFFBFFFBFFF07DD0FBFFF47FFBF07DD0FFFBF748FF
      F07DD0FBFFFB747BF07DD0FFF47FF44FF07DD0FBF44B844BF07DD0FFF844448F
      F07DD0FBFFFBFFFBF07DD0FFFBFFFBFFF0DDDD00000000000DDD}
    PopupMenuLinks = <>
    ShowHelpButton = True
    Style = bmsFlat
    UseSystemFont = False
    Left = 629
    Top = 65
    DockControlHeights = (
      0
      0
      49
      0)
    object siFile: TdxBarSubItem
      Caption = '&File'
      Category = 8
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnNew
          Visible = True
        end
        item
          Item = btnOpen
          Visible = True
        end
        item
          Item = btnClose
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnSave
          Visible = True
        end
        item
          Item = btnSaveAs
          Visible = True
        end
        item
          Item = btnNewWebPage
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnVersion
          MostRecentlyUsed = False
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnWebPagePreview
          MostRecentlyUsed = False
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnPageSetup
          Visible = True
        end
        item
          Item = btnPrintPreview
          Visible = True
        end
        item
          Item = btnPrint
          Visible = True
        end
        item
          Item = siSendTo
          Visible = True
        end
        item
          Item = btnProperties
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnExit
          Visible = True
        end>
    end
    object siEdit: TdxBarSubItem
      Caption = '&Edit'
      Category = 8
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnUndo
          Visible = True
        end
        item
          Item = btnRepeat
          MostRecentlyUsed = False
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnCut
          Visible = True
        end
        item
          Item = btnCopy
          Visible = True
        end
        item
          Item = btnPaste
          Visible = True
        end
        item
          Item = btnPasteSpecial
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnPasetAsHyperlink
          MostRecentlyUsed = False
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnClear
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = dxBarButton1
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnFind
          Visible = True
        end
        item
          Item = btnReplace
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnGoTo
          MostRecentlyUsed = False
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnLinks
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnObject
          MostRecentlyUsed = False
          Visible = True
        end>
    end
    object siView: TdxBarSubItem
      Caption = '&View'
      Category = 8
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnNormal
          Visible = True
        end
        item
          Item = btnWebLayout
          Visible = True
        end
        item
          Item = btnPrintLayout
          Visible = True
        end
        item
          Item = btnOutLine
          MostRecentlyUsed = False
          Visible = True
        end
        item
          BeginGroup = True
          Item = siToolBars
          Visible = True
        end
        item
          Item = btnRuler
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnDocumentMap
          MostRecentlyUsed = False
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnComments
          MostRecentlyUsed = False
          Visible = True
        end>
    end
    object siInsert: TdxBarSubItem
      Caption = '&Insert'
      Category = 8
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnPageBreak
          Visible = True
        end
        item
          Item = btnPageNumber
          Visible = True
        end
        item
          Item = btnDateAndTime
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnSumbol
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnAutoText
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnDate
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnTime
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnComment
          MostRecentlyUsed = False
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnFootnote
          MostRecentlyUsed = False
          Visible = True
        end
        item
          BeginGroup = True
          Item = siPicture
          Visible = True
        end
        item
          Item = btnTextBox
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnFile
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnObject1
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnAddressBook
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnHyperlink
          Visible = True
        end>
    end
    object siFormat: TdxBarSubItem
      Caption = 'F&ormat'
      Category = 8
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnFont
          Visible = True
        end
        item
          Item = btnParagraph
          Visible = True
        end
        item
          Item = btnBulletsAndNumbering1
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnSmallCaps
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnAllCaps
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnGropCap
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnTextDirection
          MostRecentlyUsed = False
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnTheme
          Visible = True
        end
        item
          Item = btnFontColor
          Visible = True
        end
        item
          Item = btnAutoFormat
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnStyle
          MostRecentlyUsed = False
          Visible = True
        end>
    end
    object siTools: TdxBarSubItem
      Caption = '&Tools'
      Category = 8
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnSpellingandGrammar
          Visible = True
        end
        item
          Item = siLanguage
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnWordCount
          Visible = True
        end
        item
          Item = btnAutoSummarize
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnAutoCorrect
          MostRecentlyUsed = False
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnTrackChanges
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnMergeDocuments
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnProtectDocument
          MostRecentlyUsed = False
          Visible = True
        end
        item
          BeginGroup = True
          Item = siMacro
          MostRecentlyUsed = False
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnCustomize
          Visible = True
        end>
    end
    object siTable: TdxBarSubItem
      Caption = 'T&able'
      Category = 8
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnDrawTable
          Visible = True
        end
        item
          BeginGroup = True
          Item = siInsertTable
          Visible = True
        end
        item
          Item = siDeleteTable
          Visible = True
        end
        item
          Item = btnMergeCells
          Visible = True
        end
        item
          Item = btnAplitCells
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnTableAutoFormat
          Visible = True
        end
        item
          Item = siAutoFit
          Visible = True
        end
        item
          Item = btnSortAscepring
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnSortDescending
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnAutoSum
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnGridLines
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnTableProperties
          Visible = True
        end>
    end
    object siWindow: TdxBarSubItem
      Caption = '&Window'
      Category = 8
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnNewWindow
          Visible = True
        end
        item
          Item = btnArrangeAll
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnSplit
          MostRecentlyUsed = False
          Visible = True
        end>
    end
    object siHelp: TdxBarSubItem
      Caption = '&Help'
      Category = 8
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButton9
          Visible = True
        end
        item
          Item = dxBarButton10
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButton11
          Visible = True
        end
        item
          Item = dxBarButton12
          Visible = True
        end
        item
          Item = dxBarButton13
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = dxBarButton14
          MostRecentlyUsed = False
          Visible = True
        end>
    end
    object btnNew: TdxBarButton
      Caption = '&New'
      Category = 0
      Hint = 'New'
      Visible = ivAlways
      ImageIndex = 0
      ShortCut = 16462
    end
    object btnNewWebPage: TdxBarButton
      Caption = 'New Web Pa&ge'
      Category = 0
      Hint = 'New Web Page'
      Visible = ivAlways
      ImageIndex = 1
    end
    object btnNewEmailMessage: TdxBarButton
      Caption = 'New E-mail Message'
      Category = 0
      Hint = 'New E-mail Message'
      Visible = ivAlways
      ImageIndex = 2
    end
    object btnNewBlankDocument: TdxBarButton
      Caption = 'New Blank Document'
      Category = 0
      Hint = 'New Blank Document'
      Visible = ivAlways
      ImageIndex = 0
    end
    object btnOpen: TdxBarButton
      Caption = '&Open...'
      Category = 0
      Hint = 'Open'
      Visible = ivAlways
      ImageIndex = 3
      ShortCut = 16463
    end
    object btnClose: TdxBarButton
      Caption = '&Close'
      Category = 0
      Hint = 'Close'
      Visible = ivAlways
      ImageIndex = 4
    end
    object btnCloseAll: TdxBarButton
      Caption = 'Close All'
      Category = 0
      Hint = 'Close All'
      Visible = ivAlways
      ImageIndex = 4
    end
    object btnSave: TdxBarButton
      Caption = '&Save'
      Category = 0
      Hint = 'Save'
      Visible = ivAlways
      ImageIndex = 5
      ShortCut = 16467
    end
    object btnSaveAs: TdxBarButton
      Caption = 'Save &As...'
      Category = 0
      Hint = 'Save As'
      Visible = ivAlways
    end
    object btnSaveAsWebPage: TdxBarButton
      Caption = 'Save as Web Page...'
      Category = 0
      Hint = 'Save as Web Page'
      Visible = ivAlways
      ImageIndex = 6
    end
    object btnSaveAll: TdxBarButton
      Caption = 'Save All'
      Category = 0
      Hint = 'Save All'
      Visible = ivAlways
      ImageIndex = 7
    end
    object btnSaveVersion: TdxBarButton
      Caption = 'Save Version'
      Category = 0
      Hint = 'Save Version'
      Visible = ivAlways
      ImageIndex = 8
    end
    object btnWebPagePreview: TdxBarButton
      Caption = 'We&b Page Preview'
      Category = 0
      Hint = 'Web Page Preview'
      Visible = ivAlways
      ImageIndex = 9
    end
    object btnPageSetup: TdxBarButton
      Caption = 'Page Set&up...'
      Category = 0
      Hint = 'Page Setup'
      Visible = ivAlways
      ImageIndex = 10
    end
    object btnPrintPreview: TdxBarButton
      Caption = 'Print Pre&view'
      Category = 0
      Hint = 'Print Preview'
      Visible = ivAlways
      ImageIndex = 11
    end
    object btnPrint: TdxBarButton
      Caption = '&Print'
      Category = 0
      Hint = 'Print'
      Visible = ivAlways
      ImageIndex = 12
      ShortCut = 16464
    end
    object btnPrintSetup: TdxBarButton
      Caption = 'Print Setup...'
      Category = 0
      Hint = 'Print Setup'
      Visible = ivAlways
      ImageIndex = 13
    end
    object btnMailRecipient: TdxBarButton
      Caption = 'Ma&il Recipient'
      Category = 0
      Hint = 'Mail Recipient'
      Visible = ivAlways
      ImageIndex = 14
    end
    object btnSendNow: TdxBarButton
      Caption = 'Send Now'
      Category = 0
      Hint = 'Send Now'
      Visible = ivAlways
    end
    object btnRoundRecipient: TdxBarButton
      Caption = '&Round Recipient...'
      Category = 0
      Hint = 'Round Recipient'
      Visible = ivAlways
      ImageIndex = 16
    end
    object btnExchengeFolder: TdxBarButton
      Caption = '&Exchange Folder...'
      Category = 0
      Hint = 'Exchange Folder'
      Visible = ivAlways
      ImageIndex = 17
    end
    object btnFaxRecipient: TdxBarButton
      Caption = '&Fax Recipient'
      Category = 0
      Hint = 'Fax Recipient'
      Visible = ivAlways
      ImageIndex = 18
    end
    object btnSendToMicrosoftPowerPoint: TdxBarButton
      Caption = 'Send to Microsoft &PowerPoint'
      Category = 0
      Hint = 'Send to Microsoft PowerPoint'
      Visible = ivAlways
      ImageIndex = 19
    end
    object btnVersion: TdxBarButton
      Caption = '&Version'
      Category = 0
      Hint = 'Version'
      Visible = ivAlways
      ImageIndex = 20
    end
    object btnProperties: TdxBarButton
      Caption = 'Properties'
      Category = 0
      Hint = 'Properties'
      Visible = ivAlways
    end
    object btnExit: TdxBarButton
      Caption = 'Exit'
      Category = 0
      Hint = 'Exit'
      Visible = ivAlways
    end
    object btnUndo: TdxBarButton
      Caption = '&Undo'
      Category = 1
      Hint = 'Undo'
      Visible = ivAlways
      ButtonStyle = bsDropDown
      ImageIndex = 21
      ShortCut = 16474
    end
    object btnRedo: TdxBarButton
      Caption = 'Redo'
      Category = 1
      Enabled = False
      Hint = 'Redo'
      Visible = ivAlways
      ButtonStyle = bsDropDown
      ImageIndex = 22
    end
    object btnRepeat: TdxBarButton
      Caption = '&Repeat'
      Category = 1
      Hint = 'Repeat'
      Visible = ivAlways
      ImageIndex = 23
      ShortCut = 16473
    end
    object btnCut: TdxBarButton
      Caption = 'Cu&t'
      Category = 1
      Hint = 'Cut'
      Visible = ivAlways
      ImageIndex = 24
      ShortCut = 16472
    end
    object btnCopy: TdxBarButton
      Caption = '&Copy'
      Category = 1
      Hint = 'Copy'
      Visible = ivAlways
      ImageIndex = 25
      ShortCut = 16451
    end
    object btnPaste: TdxBarButton
      Caption = '&Paste'
      Category = 1
      Enabled = False
      Hint = 'Paste'
      Visible = ivAlways
      ImageIndex = 26
      ShortCut = 16470
    end
    object btnPasteTable: TdxBarButton
      Caption = 'Paste Table'
      Category = 1
      Hint = 'Paste Table'
      Visible = ivAlways
      ImageIndex = 26
    end
    object btnPasteSpecial: TdxBarButton
      Caption = 'Paste &Special...'
      Category = 1
      Hint = 'Paste Special'
      Visible = ivAlways
    end
    object btnPasetAsHyperlink: TdxBarButton
      Caption = 'Paste as &Hyperlink'
      Category = 1
      Hint = 'Paste as Hyperlink'
      Visible = ivAlways
    end
    object btnClear: TdxBarButton
      Caption = 'Cle&ar'
      Category = 1
      Hint = 'Clear'
      Visible = ivAlways
      ImageIndex = 27
      ShortCut = 46
    end
    object dxBarButton1: TdxBarButton
      Caption = 'Select All'
      Category = 1
      Hint = 'Select A&ll'
      Visible = ivAlways
      ShortCut = 16449
    end
    object btnFind: TdxBarButton
      Caption = '&Find...'
      Category = 1
      Hint = 'Find'
      Visible = ivAlways
      ImageIndex = 28
      ShortCut = 16454
    end
    object btnFindNext: TdxBarButton
      Caption = 'Find Next'
      Category = 1
      Hint = 'Find Next'
      Visible = ivAlways
      ImageIndex = 29
    end
    object btnReplace: TdxBarButton
      Caption = 'R&eplace...'
      Category = 1
      Hint = 'Replace'
      Visible = ivAlways
      ImageIndex = 30
      ShortCut = 16456
    end
    object btnGoTo: TdxBarButton
      Caption = '&Go To...'
      Category = 1
      Hint = 'Go To'
      Visible = ivAlways
      ImageIndex = 31
      ShortCut = 16455
    end
    object btnLinks: TdxBarButton
      Caption = 'Lin&ks...'
      Category = 1
      Hint = 'Links'
      Visible = ivAlways
    end
    object btnObject: TdxBarButton
      Caption = '&Object...'
      Category = 1
      Hint = 'Object'
      Visible = ivAlways
    end
    object btnPicture: TdxBarButton
      Caption = 'Picture'
      Category = 1
      Hint = 'Picture'
      Visible = ivAlways
    end
    object btnCreateTextBoxLink: TdxBarButton
      Caption = 'Create Text Box Link'
      Category = 1
      Hint = 'Create Text Box Link'
      Visible = ivAlways
      ImageIndex = 32
    end
    object btnBreakForwardLink: TdxBarButton
      Caption = 'Break Forward Link'
      Category = 1
      Hint = 'Break Forward Link'
      Visible = ivAlways
      ImageIndex = 33
    end
    object btnNextTextBox: TdxBarButton
      Caption = 'Next Text Box'
      Category = 1
      Hint = 'Next Text Box'
      Visible = ivAlways
      ImageIndex = 34
    end
    object btnPreviousTextBox: TdxBarButton
      Caption = 'Previous Text Box'
      Category = 1
      Hint = 'Previous Text Box'
      Visible = ivAlways
      ImageIndex = 35
    end
    object btnNextComment: TdxBarButton
      Caption = 'Next Comment'
      Category = 1
      Hint = 'Next Comment'
      Visible = ivAlways
      ImageIndex = 36
    end
    object btnPreviousComment: TdxBarButton
      Caption = 'Previous Comment'
      Category = 1
      Hint = 'Previous Comment'
      Visible = ivAlways
      ImageIndex = 37
    end
    object cbZoom: TdxBarCombo
      Caption = 'Zoom:'
      Category = 2
      Hint = 'Zoom:'
      Visible = ivAlways
      Text = '200 %'
      Width = 100
      Items.Strings = (
        '500 %'
        '200 %'
        '150 %'
        '100 %'
        '75 %'
        '50 %'
        '25 %'
        '10 %'
        'Page Width')
      ItemIndex = 1
    end
    object btnRuler: TdxBarButton
      Caption = '&Ruler'
      Category = 2
      Hint = 'Ruler'
      Visible = ivAlways
      AllowAllUp = True
      ButtonStyle = bsChecked
      ImageIndex = 38
    end
    object btnShowAll: TdxBarButton
      Caption = 'Show All'
      Category = 2
      Hint = 'Show All'
      Visible = ivAlways
      AllowAllUp = True
      ButtonStyle = bsChecked
      Down = True
      ImageIndex = 39
    end
    object btnDocumentMap: TdxBarButton
      Caption = '&Document Map'
      Category = 2
      Hint = 'Document Map'
      Visible = ivAlways
      AllowAllUp = True
      ImageIndex = 40
    end
    object btnViewFieldCodes: TdxBarButton
      Caption = 'View Field Codes'
      Category = 2
      Hint = 'View Field Codes'
      Visible = ivAlways
      ImageIndex = 41
    end
    object btnShowFieldShading: TdxBarButton
      Caption = 'Show Field Shading'
      Category = 2
      Hint = 'Show Field Shading'
      Visible = ivAlways
      ImageIndex = 42
    end
    object btnFullScreen: TdxBarButton
      Caption = 'Full Screen'
      Category = 2
      Hint = 'Full Screen'
      Visible = ivAlways
      ImageIndex = 43
    end
    object btnMagnifier: TdxBarButton
      Caption = 'Magnifier'
      Category = 2
      Hint = 'Magnifier'
      Visible = ivAlways
      ImageIndex = 44
    end
    object btnZoom100: TdxBarButton
      Caption = 'Zoom 100 %'
      Category = 2
      Hint = 'Zoom 100 %'
      Visible = ivAlways
      ImageIndex = 45
    end
    object btnFitToWindow: TdxBarButton
      Caption = 'Fit To Window'
      Category = 2
      Hint = 'Fit To Window'
      Visible = ivAlways
      ImageIndex = 46
    end
    object btnOnePage: TdxBarButton
      Caption = 'One Page'
      Category = 2
      Hint = 'One Page'
      Visible = ivAlways
      ImageIndex = 47
    end
    object btnNormal: TdxBarButton
      Caption = '&Normal'
      Category = 2
      Hint = 'Normal'
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 1
      Down = True
      ImageIndex = 48
    end
    object btnWebLayout: TdxBarButton
      Caption = '&Web Layout'
      Category = 2
      Hint = 'Web Layout'
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 1
      ImageIndex = 49
    end
    object btnPrintLayout: TdxBarButton
      Caption = '&Print Layout'
      Category = 2
      Hint = 'Print Layout'
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 1
      ImageIndex = 50
    end
    object btnOutLine: TdxBarButton
      Caption = '&OutLine'
      Category = 2
      Hint = 'OutLine'
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 1
      ImageIndex = 51
    end
    object siToolBars: TdxBarSubItem
      Caption = '&Toolbars'
      Category = 2
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnStandard
          Visible = True
        end
        item
          Item = btnFormatting
          Visible = True
        end
        item
          Item = btnInternet
          Visible = True
        end>
    end
    object btnComments: TdxBarButton
      Caption = '&Comments'
      Category = 2
      Hint = 'Comments'
      Visible = ivAlways
      ImageIndex = 52
    end
    object btnHTMLSource: TdxBarButton
      Caption = 'HTML Source'
      Category = 2
      Hint = 'HTML Source'
      Visible = ivAlways
    end
    object dxBarButton2: TdxBarButton
      Caption = 'Next Comment'
      Category = 2
      Hint = 'Next Comment'
      Visible = ivAlways
      ImageIndex = 36
    end
    object dxBarButton3: TdxBarButton
      Caption = 'Previous Comment'
      Category = 2
      Hint = 'Previous Comment'
      Visible = ivAlways
      ImageIndex = 37
    end
    object btnPromote: TdxBarButton
      Caption = 'Primote'
      Category = 2
      Hint = 'Primote'
      Visible = ivAlways
      ImageIndex = 53
    end
    object btnDemote: TdxBarButton
      Caption = 'Demote'
      Category = 2
      Hint = 'Demote'
      Visible = ivAlways
      ImageIndex = 54
    end
    object btnDemoteToBodyText: TdxBarButton
      Caption = 'Demote to Body Text'
      Category = 2
      Hint = 'Demote to Body Text'
      Visible = ivAlways
      ImageIndex = 55
    end
    object btnMoveUp: TdxBarButton
      Caption = 'Move Up'
      Category = 2
      Hint = 'Move Up'
      Visible = ivAlways
      ImageIndex = 56
    end
    object btnMoveDown: TdxBarButton
      Caption = 'Move Down'
      Category = 2
      Hint = 'Move Down'
      Visible = ivAlways
      ImageIndex = 57
    end
    object btnExpand: TdxBarButton
      Caption = 'Expand'
      Category = 2
      Hint = 'Expand'
      Visible = ivAlways
      ImageIndex = 58
    end
    object btnCollapse: TdxBarButton
      Caption = 'Collapse'
      Category = 2
      Hint = 'Collapse'
      Visible = ivAlways
      ImageIndex = 59
    end
    object btnAll: TdxBarButton
      Caption = 'All'
      Category = 2
      Hint = 'All'
      Visible = ivAlways
    end
    object btnShowHeading1: TdxBarButton
      Caption = 'Show Heading 1'
      Category = 2
      Hint = 'Show Heading 1'
      Visible = ivAlways
      ImageIndex = 60
    end
    object btnShowHeading2: TdxBarButton
      Caption = 'Show Heading 2'
      Category = 2
      Hint = 'Show Heading 2'
      Visible = ivAlways
      ImageIndex = 61
    end
    object btnShowHeading3: TdxBarButton
      Caption = 'Show Heading 3'
      Category = 2
      Hint = 'Show Heading 3'
      Visible = ivAlways
      ImageIndex = 62
    end
    object btnShowHeading4: TdxBarButton
      Caption = 'Show Heading 4'
      Category = 2
      Hint = 'Show Heading 4'
      Visible = ivAlways
      ImageIndex = 64
    end
    object btnShowHeading5: TdxBarButton
      Caption = 'Show Heading 5'
      Category = 2
      Hint = 'Show Heading 5'
      Visible = ivAlways
      ImageIndex = 64
    end
    object btnFirstLineOnlyView: TdxBarButton
      Caption = 'First Line Only View'
      Category = 2
      Hint = 'First Line Only View'
      Visible = ivAlways
      ImageIndex = 65
    end
    object btnShowFormatting: TdxBarButton
      Caption = 'Show Formatting'
      Category = 2
      Hint = 'Show Formatting'
      Visible = ivAlways
      ImageIndex = 66
    end
    object btnMasterDocumentView: TdxBarButton
      Caption = 'Master Document View'
      Category = 2
      Hint = 'Master Document View'
      Visible = ivAlways
      ImageIndex = 67
    end
    object btnNextHeader: TdxBarButton
      Caption = 'Next Header'
      Category = 2
      Hint = 'Next Header'
      Visible = ivAlways
      ImageIndex = 68
    end
    object btnPreviousHeader1: TdxBarButton
      Caption = 'Previous Header'
      Category = 2
      Hint = 'Previous Header'
      Visible = ivAlways
      ImageIndex = 69
    end
    object btnHideBodyText: TdxBarButton
      Caption = 'Hide Body Text'
      Category = 2
      Hint = 'Hide Body Text'
      Visible = ivAlways
      ImageIndex = 70
    end
    object btnPageBreak: TdxBarButton
      Caption = 'Page &Break'
      Category = 3
      Hint = 'Page Break'
      Visible = ivAlways
      ImageIndex = 71
    end
    object btnInsertColumnBreak: TdxBarButton
      Caption = 'Insert Column Break'
      Category = 3
      Hint = 'Insert Column Break'
      Visible = ivAlways
    end
    object btnInsertSectionBreak: TdxBarButton
      Caption = 'Insert Section Break'
      Category = 3
      Hint = 'Insert Section Break'
      Visible = ivAlways
    end
    object btnPageNumber: TdxBarButton
      Caption = 'Page &Number'
      Category = 3
      Hint = 'Page Number'
      Visible = ivAlways
      ImageIndex = 72
    end
    object btnNumberOfPages: TdxBarButton
      Caption = 'Number of Pages'
      Category = 3
      Hint = 'Number of Pages'
      Visible = ivAlways
      ImageIndex = 73
    end
    object btnDate: TdxBarButton
      Caption = '&Date'
      Category = 3
      Hint = 'Date'
      Visible = ivAlways
      ImageIndex = 74
    end
    object btnTime: TdxBarButton
      Caption = 'T&ime'
      Category = 3
      Hint = 'Time'
      Visible = ivAlways
      ImageIndex = 75
    end
    object btnPageNumbers: TdxBarButton
      Caption = 'Page Numbers...'
      Category = 3
      Hint = 'Page Numbers'
      Visible = ivAlways
      ImageIndex = 72
    end
    object btnDateAndTime: TdxBarButton
      Caption = 'Date and &Time'
      Category = 3
      Hint = 'Date and Time'
      Visible = ivAlways
    end
    object btnAutoText: TdxBarButton
      Caption = '&AutoText...'
      Category = 3
      Hint = 'AutoText'
      Visible = ivAlways
      ImageIndex = 76
    end
    object btnSumbol: TdxBarButton
      Caption = '&Symbol...'
      Category = 3
      Hint = 'Symbol'
      Visible = ivAlways
      ImageIndex = 77
    end
    object btnComment: TdxBarButton
      Caption = '&Comment'
      Category = 3
      Hint = 'Comment'
      Visible = ivAlways
      ImageIndex = 78
    end
    object btnDeleteComment: TdxBarButton
      Caption = 'Delete Comment'
      Category = 3
      Hint = 'Delete Comment'
      Visible = ivAlways
      ImageIndex = 79
    end
    object btnFootnote: TdxBarButton
      Caption = '&Footnote...'
      Category = 3
      Hint = 'Footnote'
      Visible = ivAlways
      ImageIndex = 80
    end
    object InsertListNumField: TdxBarButton
      Caption = 'Insert ListNum Field'
      Category = 3
      Hint = 'Insert ListNum Field'
      Visible = ivAlways
      ImageIndex = 81
    end
    object btnHyperlink: TdxBarButton
      Caption = '&Hyperlink...'
      Category = 3
      Hint = 'Hyperlink'
      Visible = ivAlways
      ImageIndex = 82
      ShortCut = 16459
    end
    object btnFromFile: TdxBarButton
      Caption = '&From File...'
      Category = 3
      Hint = 'From File'
      Visible = ivAlways
      ImageIndex = 83
    end
    object btnTextBox: TdxBarButton
      Caption = 'Text &Box'
      Category = 3
      Hint = 'Text Box'
      Visible = ivAlways
      ImageIndex = 84
    end
    object btnHorizontal: TdxBarButton
      Caption = 'Horizontal'
      Category = 3
      Hint = 'Horizontal'
      Visible = ivAlways
      ImageIndex = 85
    end
    object btnFile: TdxBarButton
      Caption = 'F&ile...'
      Category = 3
      Hint = 'File'
      Visible = ivAlways
      ImageIndex = 86
    end
    object btnInsertExcelSpreadsheet: TdxBarButton
      Caption = 'Inset Excel Spreadsheet'
      Category = 3
      Hint = 'Inset Excel Spreadsheet'
      Visible = ivAlways
      ImageIndex = 87
    end
    object btnChart: TdxBarButton
      Caption = '&Chart'
      Category = 3
      Hint = 'Chart'
      Visible = ivAlways
      ImageIndex = 88
    end
    object btnWordArt: TdxBarButton
      Caption = '&WordArt...'
      Category = 3
      Hint = 'WordArt'
      Visible = ivAlways
      ImageIndex = 89
    end
    object btnEquationEditor: TdxBarButton
      Caption = 'Equation Editor'
      Category = 3
      Hint = 'Equation Editor'
      Visible = ivAlways
      ImageIndex = 90
    end
    object btnVoiceComment: TdxBarButton
      Caption = 'Voice Comment'
      Category = 3
      Hint = 'Voice Comment'
      Visible = ivAlways
      ImageIndex = 91
    end
    object btnPenComment: TdxBarButton
      Caption = 'Pen Comment'
      Category = 3
      Hint = 'Pen Comment'
      Visible = ivAlways
      ImageIndex = 92
    end
    object btnFromScannerOrCamear: TdxBarButton
      Caption = 'From &Scanner or Camera...'
      Category = 3
      Hint = 'From Scanner or Camera'
      Visible = ivAlways
      ImageIndex = 93
    end
    object btnClipArt: TdxBarButton
      Caption = '&Clip Art...'
      Category = 3
      Hint = 'Clip Art'
      Visible = ivAlways
      ImageIndex = 94
    end
    object btnDataBase: TdxBarButton
      Caption = 'Database...'
      Category = 3
      Hint = 'Database'
      Visible = ivAlways
      ImageIndex = 95
    end
    object btnAddressBook: TdxBarButton
      Caption = 'Add&ress Book...'
      Category = 3
      Hint = 'Address Book'
      Visible = ivAlways
      ImageIndex = 96
    end
    object btnObject1: TdxBarButton
      Caption = '&Object...'
      Category = 3
      Hint = 'Object'
      Visible = ivAlways
      ImageIndex = 97
    end
    object cbFontName: TdxBarFontNameCombo
      Caption = 'Font:'
      Category = 4
      Hint = 'Font'
      Visible = ivAlways
      Text = 'Times New Roman'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888444488844444488887488888744
        7888888448888744888888874888844788888888444444488888888874884478
        8888888884484488888888888744478888888888884448888888888888747888
        8888888888848888888888888888888888888888888888888888}
      Width = 160
      DropDownCount = 12
    end
    object cnFontSize: TdxBarCombo
      Caption = 'Font Size:'
      Category = 4
      Hint = 'Font Size'
      Visible = ivAlways
      Text = '10'
      Width = 100
      Items.Strings = (
        '8'
        '9'
        '10'
        '11'
        '12'
        '14'
        '16'
        '18'
        '20'
        '22'
        '24'
        '26'
        '28'
        '36'
        '48'
        '72')
      ItemIndex = 2
    end
    object cbStyle: TdxBarCombo
      Caption = 'Style:'
      Category = 4
      Hint = 'Style'
      Visible = ivAlways
      Text = 'Normal'
      Width = 100
      DropDownWidth = 190
      Items.Strings = (
        'Default Paragraph Font'
        'Heading 1'
        'Heading 2'
        'Heading 3'
        'Normal')
      ItemIndex = 4
    end
    object btnBold: TdxBarButton
      Caption = 'Bold'
      Category = 4
      Hint = 'Bold'
      Visible = ivAlways
      AllowAllUp = True
      ButtonStyle = bsChecked
      ImageIndex = 98
    end
    object btnItalic: TdxBarButton
      Caption = 'Italic'
      Category = 4
      Hint = 'Italic'
      Visible = ivAlways
      AllowAllUp = True
      ButtonStyle = bsChecked
      ImageIndex = 99
    end
    object btnUnderline: TdxBarButton
      Caption = 'Underline'
      Category = 4
      Hint = 'Underline'
      Visible = ivAlways
      AllowAllUp = True
      ButtonStyle = bsChecked
      ImageIndex = 100
    end
    object btnFormatPainter: TdxBarButton
      Caption = 'Format Painter'
      Category = 4
      Hint = 'Format Painter'
      Visible = ivAlways
      ImageIndex = 101
    end
    object btnFontColor: TdxBarButton
      Caption = 'Font Color'
      Category = 4
      Hint = 'Font Color'
      Visible = ivAlways
      ButtonStyle = bsDropDown
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCDDDDDDDDDDDD
        DDDD4444DDD444444DDDD74DDDDD7447DDDDDD44DDDD744DDDDDDD74DDDD447D
        DDDDDDD4444444DDDDDDDDD74DD447DDDDDDDDDD44D44DDDDDDDDDDD74447DDD
        DDDDDDDDD444DDDDDDDDDDDDD747DDDDDDDDDDDDDD4DDDDDDDDD}
    end
    object btnHighlight: TdxBarButton
      Caption = 'Hightlight'
      Category = 4
      Hint = 'Hightlight'
      Visible = ivAlways
      ButtonStyle = bsDropDown
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBDDDDDDDDDDDD
        DDDDDD00000DDDDDDDDDDD08FFF0DDDDDDDDDDD08FC70DDDDDDDDDDD0CC770DD
        DDDDDDDDD0C7270DDDDDDDDDDD0FF270DDDDDDDDDDD0FF270DDDDDDDDDDD0FF0
        C0DDDDDDDDDDD00FC0DDDDDDDDDDDD000DDDDDDDDDDDDDDDDDDD}
    end
    object btnGrowFont: TdxBarButton
      Caption = 'Grow Font'
      Category = 4
      Hint = 'Grow Font'
      Visible = ivAlways
      ImageIndex = 104
    end
    object btnShrinkFont: TdxBarButton
      Caption = 'Shink Font'
      Category = 4
      Hint = 'Shink Font'
      Visible = ivAlways
      ImageIndex = 105
    end
    object btnGrowFont1Pt: TdxBarButton
      Caption = 'Grow Font 1 Pt'
      Category = 4
      Hint = 'Grow Font 1 Pt'
      Visible = ivAlways
      ImageIndex = 106
    end
    object btnShinkFont1Pt: TdxBarButton
      Caption = 'Shink Font 1 Pt'
      Category = 4
      Hint = 'Shink Font 1 Pt'
      Visible = ivAlways
      ImageIndex = 107
    end
    object btnSmallCaps: TdxBarButton
      Caption = '&Small Caps'
      Category = 4
      Hint = 'Small Caps'
      Visible = ivAlways
      ImageIndex = 115
    end
    object btnAllCaps: TdxBarButton
      Caption = '&All Caps'
      Category = 4
      Hint = 'All Caps'
      Visible = ivAlways
      ImageIndex = 116
    end
    object btnSuperscript: TdxBarButton
      Caption = 'Superscript'
      Category = 4
      Hint = 'Superscript'
      Visible = ivAlways
      ImageIndex = 117
    end
    object btnsubscript: TdxBarButton
      Caption = 'Subscript'
      Category = 4
      Hint = 'Subscript'
      Visible = ivAlways
      ImageIndex = 118
    end
    object btnDecreaseIndent: TdxBarButton
      Caption = 'Decrease Indent'
      Category = 4
      Hint = 'Decrease Indent'
      Visible = ivAlways
      AllowAllUp = True
      ButtonStyle = bsChecked
      ImageIndex = 119
    end
    object btnIncreaseIndent: TdxBarButton
      Caption = 'Increase Indent'
      Category = 4
      Hint = 'Increase Indent'
      Visible = ivAlways
      AllowAllUp = True
      ImageIndex = 120
    end
    object btnNumbering: TdxBarButton
      Caption = 'Numbering'
      Category = 4
      Hint = 'Numbering'
      Visible = ivAlways
      AllowAllUp = True
      ButtonStyle = bsChecked
      GroupIndex = 4
      ImageIndex = 121
    end
    object btnBullets: TdxBarButton
      Caption = 'Bullets'
      Category = 4
      Hint = 'Bullets'
      Visible = ivAlways
      AllowAllUp = True
      ButtonStyle = bsChecked
      GroupIndex = 4
      ImageIndex = 122
    end
    object btnAlignLeft: TdxBarButton
      Caption = 'Align Left'
      Category = 4
      Hint = 'Align Left'
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 3
      Down = True
      ImageIndex = 123
    end
    object btnCenter: TdxBarButton
      Caption = 'Center'
      Category = 4
      Hint = 'Center'
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 3
      ImageIndex = 124
    end
    object btnAlignRight: TdxBarButton
      Caption = 'Align Right'
      Category = 4
      Hint = 'Align Right'
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 3
      ImageIndex = 125
    end
    object btnJustify: TdxBarButton
      Caption = 'Justify'
      Category = 4
      Hint = 'Justify'
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 3
      ImageIndex = 126
    end
    object btnColumns: TdxBarButton
      Caption = 'Columns'
      Category = 4
      Hint = 'Columns'
      Visible = ivAlways
      ImageIndex = 130
    end
    object btnAutoFormat: TdxBarButton
      Caption = 'A&utoFormat...'
      Category = 4
      Hint = 'AutoFormat'
      Visible = ivAlways
      ImageIndex = 131
    end
    object btnInsertListNumField: TdxBarButton
      Caption = 'Insert ListNum Field'
      Category = 4
      Hint = 'Insert ListNum Field'
      Visible = ivAlways
      ImageIndex = 81
    end
    object btnChangeTextDirection: TdxBarButton
      Caption = 'Change Text Direction'
      Category = 4
      Hint = 'Change Text Direction'
      Visible = ivAlways
      ImageIndex = 132
    end
    object btnFont: TdxBarButton
      Caption = '&Font...'
      Category = 4
      Hint = 'Font'
      Visible = ivAlways
      ImageIndex = 133
    end
    object btnParagraph: TdxBarButton
      Caption = '&Paragraph...'
      Category = 4
      Hint = 'Paragraph'
      Visible = ivAlways
      ImageIndex = 134
    end
    object btnBulletsAndNumbering1: TdxBarButton
      Caption = '&Bullets and Numbering...'
      Category = 4
      Hint = 'Bullets and Numbering'
      Visible = ivAlways
      ImageIndex = 135
    end
    object btnGropCap: TdxBarButton
      Caption = '&Drop Cap...'
      Category = 4
      Hint = 'Drop Cap'
      Visible = ivAlways
      ImageIndex = 136
    end
    object btnStyle: TdxBarButton
      Caption = '&Style...'
      Category = 4
      Hint = 'Style'
      Visible = ivAlways
      ImageIndex = 137
    end
    object btnTextDirection: TdxBarButton
      Caption = '&Text Direction...'
      Category = 4
      Hint = 'Text Direction'
      Visible = ivAlways
      ImageIndex = 138
    end
    object btnChangeCase: TdxBarButton
      Caption = 'Change Case...'
      Category = 4
      Hint = 'Change Case'
      Visible = ivAlways
      ImageIndex = 139
    end
    object btnTheme: TdxBarButton
      Caption = 'T&heme...'
      Category = 4
      Hint = 'Theme'
      Visible = ivAlways
      ImageIndex = 140
    end
    object btnCOMAddIns: TdxBarButton
      Caption = 'COM Add-Ins...'
      Category = 5
      Hint = 'COM Add-Ins'
      Visible = ivAlways
    end
    object btnSpellingandGrammar: TdxBarButton
      Caption = 'Spelling and Grammar...'
      Category = 5
      Hint = 'Spelling and Grammar'
      Visible = ivAlways
      ImageIndex = 145
      ShortCut = 16502
    end
    object btnNextMisspelling: TdxBarButton
      Caption = 'Next Misspelling'
      Category = 5
      Hint = 'Next Misspelling'
      Visible = ivAlways
    end
    object btnHideSpellingErrors: TdxBarButton
      Caption = 'Hide Spelling Errors'
      Category = 5
      Hint = 'Hide Spelling Errors'
      Visible = ivAlways
    end
    object btnHideGrammarErrors: TdxBarButton
      Caption = 'Hide Grammar Errors'
      Category = 5
      Hint = 'Hide Grammar Errors'
      Visible = ivAlways
    end
    object btnSpelling: TdxBarButton
      Caption = 'Spelling...'
      Category = 5
      Hint = 'Spelling'
      Visible = ivAlways
      ImageIndex = 145
    end
    object btnDictionary: TdxBarButton
      Caption = 'Dictionary...'
      Category = 5
      Hint = 'Dictionary'
      Visible = ivAlways
    end
    object btnGrammar: TdxBarButton
      Caption = 'Grammar...'
      Category = 5
      Hint = 'Grammar'
      Visible = ivAlways
      ImageIndex = 146
    end
    object btnSetLanguage: TdxBarButton
      Caption = 'Set Language...'
      Category = 5
      Hint = 'Set Language'
      Visible = ivAlways
      ImageIndex = 147
    end
    object btnThesaurus: TdxBarButton
      Caption = 'Thesaurus...'
      Category = 5
      Hint = 'Thesaurus'
      Visible = ivAlways
      ShortCut = 8310
    end
    object btnWordCount: TdxBarButton
      Caption = 'Word Count...'
      Category = 5
      Hint = 'Word Count'
      Visible = ivAlways
    end
    object btnAutoSummarize: TdxBarButton
      Caption = 'AutoSummarize...'
      Category = 5
      Hint = 'AutoSummarize'
      Visible = ivAlways
      ImageIndex = 148
    end
    object btnResumarize: TdxBarButton
      Caption = 'Resumarize'
      Category = 5
      Hint = 'Resumarize'
      Visible = ivAlways
      ImageIndex = 148
    end
    object btnHighlightShowOnlySummary: TdxBarButton
      Caption = 'Highlight/Show Only Summary'
      Category = 5
      Hint = 'Highlight/Show Only Summary'
      Visible = ivAlways
      ImageIndex = 149
    end
    object btnAutoCorrect: TdxBarButton
      Caption = 'AutoCorrect...'
      Category = 5
      Hint = 'AutoCorrect'
      Visible = ivAlways
    end
    object btnToolsAutoCorrectExceptions: TdxBarButton
      Caption = 'Tools AutoCorrect Exceptions'
      Category = 5
      Hint = 'Tools AutoCorrect Exceptions'
      Visible = ivAlways
    end
    object btnHighlightChanges: TdxBarButton
      Caption = 'Highlight Changes...'
      Category = 5
      Hint = 'Highlight Changes'
      Visible = ivAlways
      ImageIndex = 150
    end
    object btnTrackChanges: TdxBarButton
      Caption = 'Track Changes'
      Category = 5
      Hint = 'Track Changes'
      Visible = ivAlways
      ImageIndex = 151
    end
    object btnAcceptOrRejectChanges: TdxBarButton
      Caption = 'Accept or Reject Changes...'
      Category = 5
      Hint = 'Accept or Reject Changes'
      Visible = ivAlways
    end
    object btnAcceptChange: TdxBarButton
      Caption = 'Accept Change'
      Category = 5
      Hint = 'Accept Change'
      Visible = ivAlways
      ImageIndex = 152
    end
    object btnRejectChange: TdxBarButton
      Caption = 'Reject Change'
      Category = 5
      Hint = 'Reject Change'
      Visible = ivAlways
      ImageIndex = 153
    end
    object btnNextChange: TdxBarButton
      Caption = 'Next Change'
      Category = 5
      Hint = 'Next Change'
      Visible = ivAlways
      ImageIndex = 154
    end
    object btnPreviousChange: TdxBarButton
      Caption = 'Previous Change'
      Category = 5
      Hint = 'Previous Change'
      Visible = ivAlways
      ImageIndex = 155
    end
    object btnCompareDocuments: TdxBarButton
      Caption = 'Compare Documents...'
      Category = 5
      Hint = 'Compare Documents'
      Visible = ivAlways
      ImageIndex = 156
    end
    object btnMergeDocuments: TdxBarButton
      Caption = 'Merge Documents...'
      Category = 5
      Hint = 'Merge Documents'
      Visible = ivAlways
    end
    object btnProtectDocument: TdxBarButton
      Caption = 'Protect Document'
      Category = 5
      Hint = 'Protect Document'
      Visible = ivAlways
    end
    object btnEnvelopesAndLabels: TdxBarButton
      Caption = 'Envelopes and Labels...'
      Category = 5
      Hint = 'Envelopes and Labels'
      Visible = ivAlways
      ImageIndex = 157
    end
    object btnLetterWizard: TdxBarButton
      Caption = 'Letter Wizard...'
      Category = 5
      Hint = 'Letter Wizard'
      Visible = ivAlways
    end
    object btnMacros: TdxBarButton
      Caption = 'Macros...'
      Category = 5
      Hint = 'Macros'
      Visible = ivAlways
      ImageIndex = 158
    end
    object btnRecordMacroStopRecorder: TdxBarButton
      Caption = 'Record Macro / Stop Recorder'
      Category = 5
      Hint = 'Record Macro / Stop Recorder'
      Visible = ivAlways
    end
    object btnSecurity: TdxBarButton
      Caption = 'Security...'
      Category = 5
      Hint = 'Security'
      Visible = ivAlways
    end
    object btnVisualBasicEditor: TdxBarButton
      Caption = 'Visual Basic Editor'
      Category = 5
      Hint = 'Visual Basic Editor'
      Visible = ivAlways
      ImageIndex = 159
    end
    object btnMicrosoftScriptEditor: TdxBarButton
      Caption = 'Microsoft Script Editor'
      Category = 5
      Hint = 'Microsoft Script Editor'
      Visible = ivAlways
      ImageIndex = 160
    end
    object btnInsertScript: TdxBarButton
      Caption = 'Insert Script'
      Category = 5
      Hint = 'Insert Script'
      Visible = ivAlways
    end
    object btnRemoveAllScripts: TdxBarButton
      Caption = 'Remove All Scripts'
      Category = 5
      Hint = 'Remove All Scripts'
      Visible = ivAlways
    end
    object btnShowAllScripts: TdxBarButton
      Caption = 'Show All Scripts'
      Category = 5
      Hint = 'Show All Scripts'
      Visible = ivAlways
    end
    object btnStopRecordind: TdxBarButton
      Caption = 'Stop Recording'
      Category = 5
      Hint = 'Stop Recording'
      Visible = ivAlways
      ImageIndex = 161
    end
    object btnPauseRecording: TdxBarButton
      Caption = 'Pause Recording'
      Category = 5
      Hint = 'Pause Recording'
      Visible = ivAlways
      ImageIndex = 162
    end
    object btnUpdateField: TdxBarButton
      Caption = 'Update Field'
      Category = 5
      Hint = 'Update Field'
      Visible = ivAlways
      ImageIndex = 163
    end
    object btnRepaginate: TdxBarButton
      Caption = 'Repaginate'
      Category = 5
      Hint = 'Repaginate'
      Visible = ivAlways
      ImageIndex = 164
    end
    object btnShrinkOnePage: TdxBarButton
      Caption = 'Shrink One Page'
      Category = 5
      Hint = 'Shrink One Page'
      Visible = ivAlways
      ImageIndex = 165
    end
    object btnInsertTable: TdxBarButton
      Caption = 'Insert Table...'
      Category = 6
      Hint = 'Insert Table'
      Visible = ivAlways
      ImageIndex = 166
    end
    object dxBarButton4: TdxBarButton
      Caption = 'Insert Excel Spreadsheet'
      Category = 6
      Hint = 'Insert Excel Spreadsheet'
      Visible = ivAlways
      ImageIndex = 167
    end
    object btnCells: TdxBarButton
      Caption = 'Insert Cells'
      Category = 6
      Hint = 'Insert Cells'
      Visible = ivAlways
      ImageIndex = 168
    end
    object btnRows: TdxBarButton
      Caption = 'Insert Rows'
      Category = 6
      Hint = 'Insert Rows'
      Visible = ivAlways
      ImageIndex = 169
    end
    object btnColumnsToTheLeft: TdxBarButton
      Caption = 'Columns to the Left'
      Category = 6
      Hint = 'Columns to the Left'
      Visible = ivAlways
      ImageIndex = 170
    end
    object btnDeleteCells: TdxBarButton
      Caption = 'Delete Cells'
      Category = 6
      Hint = 'Delete Cells'
      Visible = ivAlways
      ImageIndex = 171
    end
    object btnDeleteRows: TdxBarButton
      Caption = 'Delete Rows'
      Category = 6
      Hint = 'Delete Rows'
      Visible = ivAlways
      ImageIndex = 172
    end
    object btnDeleteColumns: TdxBarButton
      Caption = 'Delete Columns'
      Category = 6
      Hint = 'Delete Columns'
      Visible = ivAlways
      ImageIndex = 173
    end
    object btnGridLines: TdxBarButton
      Caption = 'Hide Gridlines'
      Category = 6
      Hint = 'Hide Gridlines'
      Visible = ivAlways
      AllowAllUp = True
      ButtonStyle = bsChecked
      Down = True
      ImageIndex = 174
    end
    object btnTableAutoFormat: TdxBarButton
      Caption = 'Table AutoFormat...'
      Category = 6
      Hint = 'Table AutoFormat'
      Visible = ivAlways
      ImageIndex = 175
    end
    object btnAutoSum: TdxBarButton
      Caption = 'AutoSum'
      Category = 6
      Hint = 'AutoSum'
      Visible = ivAlways
      ImageIndex = 176
    end
    object btnSortAscepring: TdxBarButton
      Caption = 'Sort Ascending'
      Category = 6
      Hint = 'Sort Ascending'
      Visible = ivAlways
      ImageIndex = 177
    end
    object btnSortDescending: TdxBarButton
      Caption = 'Sort Descending'
      Category = 6
      Hint = 'Sort Descending'
      Visible = ivAlways
      ImageIndex = 178
    end
    object btnFindInField: TdxBarButton
      Caption = 'Find in Field'
      Category = 6
      Hint = 'Find in Field'
      Visible = ivAlways
      ImageIndex = 179
    end
    object btnTableInsert: TdxBarSubItem
      Caption = 'Table Insert'
      Category = 6
      Visible = ivAlways
      ImageIndex = 180
      ItemLinks = <>
    end
    object btnTablesandBordersToolbar: TdxBarButton
      Caption = 'Tables and Borders Toolbar'
      Category = 6
      Hint = 'Tables and Borders Toolbar'
      Visible = ivAlways
      ImageIndex = 181
    end
    object btnDrawTable: TdxBarButton
      Caption = 'Draw Table'
      Category = 6
      Hint = 'Draw Table'
      Visible = ivAlways
      ImageIndex = 182
    end
    object btnEraser: TdxBarButton
      Caption = 'Eraser'
      Category = 6
      Hint = 'Eraser'
      Visible = ivAlways
      ImageIndex = 183
    end
    object btnMergeCells: TdxBarButton
      Caption = 'Merge Cells'
      Category = 6
      Hint = 'Merge Cells'
      Visible = ivAlways
      ImageIndex = 184
    end
    object btnAplitCells: TdxBarButton
      Caption = 'Split Cells...'
      Category = 6
      Hint = 'Split Cells'
      Visible = ivAlways
      ImageIndex = 185
    end
    object btnAlignTop: TdxBarButton
      Caption = 'Align Top'
      Category = 6
      Hint = 'Align Top'
      Visible = ivAlways
      ImageIndex = 186
    end
    object btnCenterVerticaly: TdxBarButton
      Caption = 'Center Verticaly'
      Category = 6
      Hint = 'Center Verticaly'
      Visible = ivAlways
      ImageIndex = 187
    end
    object btnAlignBottom: TdxBarButton
      Caption = 'Align Bottom'
      Category = 6
      Hint = 'Align Bottom'
      Visible = ivAlways
      ImageIndex = 188
    end
    object siCellAlignment: TdxBarSubItem
      Caption = 'Cell Alignment'
      Category = 6
      Visible = ivAlways
      ImageIndex = 189
      ItemLinks = <>
    end
    object btnDistributeRowsEvenly: TdxBarButton
      Caption = 'Distribute Rows Evenly'
      Category = 6
      Hint = 'Distribute Rows Evenly'
      Visible = ivAlways
      ImageIndex = 190
    end
    object btnDistributeColumnsEvenly: TdxBarButton
      Caption = 'Distribute Columns Evenly'
      Category = 6
      Hint = 'Distribute Columns Evenly'
      Visible = ivAlways
      ImageIndex = 191
    end
    object btnNewWindow: TdxBarButton
      Caption = 'New Window'
      Category = 7
      Hint = 'New Window'
      Visible = ivAlways
      ImageIndex = 192
    end
    object btnArrangeAll: TdxBarButton
      Caption = 'Arrange All'
      Category = 7
      Hint = 'Arrange All'
      Visible = ivAlways
      ImageIndex = 193
    end
    object btnSplit: TdxBarButton
      Caption = 'Split'
      Category = 7
      Hint = 'Split'
      Visible = ivAlways
      ImageIndex = 194
    end
    object btnNextWindow: TdxBarButton
      Caption = 'Next Window'
      Category = 7
      Hint = 'Next Window'
      Visible = ivAlways
    end
    object btnPreviousWindow: TdxBarButton
      Caption = 'Previous Window'
      Category = 7
      Hint = 'Previous Window'
      Visible = ivAlways
    end
    object dxBarButton7: TdxBarButton
      Caption = 'Close'
      Category = 7
      Hint = 'Close'
      Visible = ivAlways
    end
    object dxBarButton8: TdxBarButton
      Caption = 'Microsoft Word Help'
      Category = 7
      Hint = 'Microsoft Word Help'
      Visible = ivAlways
      ImageIndex = 195
    end
    object dxBarButton9: TdxBarButton
      Caption = 'Show the Office Assistant'
      Category = 7
      Hint = 'Show the Office Assistant'
      Visible = ivAlways
    end
    object dxBarButton10: TdxBarButton
      Caption = 'What'#39's This?'
      Category = 7
      Hint = 'What'#39's This?'
      Visible = ivAlways
      ImageIndex = 196
    end
    object dxBarButton11: TdxBarButton
      Caption = 'Contents and Index'
      Category = 7
      Hint = 'Contents and Index'
      Visible = ivAlways
      ImageIndex = 197
    end
    object dxBarButton12: TdxBarButton
      Caption = 'About'
      Category = 7
      Hint = 'About'
      Visible = ivAlways
    end
    object dxBarButton13: TdxBarButton
      Caption = 'WordPerfect Help...'
      Category = 7
      Hint = 'WordPerfect Help'
      Visible = ivAlways
    end
    object dxBarButton14: TdxBarButton
      Caption = 'WordPerfect Help Options...'
      Category = 7
      Hint = 'WordPerfect Help Options'
      Visible = ivAlways
    end
    object siSendTo: TdxBarSubItem
      Caption = 'Sen&d To'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnMailRecipient
          Visible = True
        end
        item
          Item = btnRoundRecipient
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnExchengeFolder
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnFaxRecipient
          MostRecentlyUsed = False
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnSendToMicrosoftPowerPoint
          MostRecentlyUsed = False
          Visible = True
        end>
    end
    object siPicture: TdxBarSubItem
      Caption = '&Picture'
      Category = 3
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnClipArt
          Visible = True
        end
        item
          Item = btnFromFile
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnWordArt
          Visible = True
        end
        item
          Item = btnFromScannerOrCamear
          MostRecentlyUsed = False
          Visible = True
        end
        item
          Item = btnChart
          Visible = True
        end>
    end
    object siLanguage: TdxBarSubItem
      Caption = 'Language'
      Category = 5
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnSetLanguage
          Visible = True
        end
        item
          Item = btnThesaurus
          Visible = True
        end>
    end
    object btnCustomize: TdxBarButton
      Caption = 'Customize...'
      Category = 5
      Hint = 'Customize'
      Visible = ivAlways
    end
    object siMacro: TdxBarSubItem
      Caption = 'Macro'
      Category = 5
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnMacros
          Visible = True
        end
        item
          Item = btnRecordMacroStopRecorder
          Visible = True
        end
        item
          Item = btnSecurity
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnVisualBasicEditor
          Visible = True
        end
        item
          Item = btnMicrosoftScriptEditor
          Visible = True
        end>
    end
    object siInsertTable: TdxBarSubItem
      Caption = 'Insert Table'
      Category = 6
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnInsertTable
          Visible = True
        end
        item
          Item = dxBarButton4
          Visible = True
        end
        item
          BeginGroup = True
          Item = btnCells
          Visible = True
        end
        item
          Item = btnRows
          Visible = True
        end>
    end
    object siDeleteTable: TdxBarSubItem
      Caption = 'Delete Table'
      Category = 6
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnDeleteColumns
          Visible = True
        end
        item
          Item = btnDeleteRows
          Visible = True
        end
        item
          Item = btnDeleteCells
          Visible = True
        end>
    end
    object siAutoFit: TdxBarSubItem
      Caption = 'Auto Fit'
      Category = 6
      Visible = ivAlways
      ItemLinks = <
        item
          Item = btnDistributeRowsEvenly
          Visible = True
        end
        item
          Item = btnDistributeColumnsEvenly
          Visible = True
        end>
    end
    object btnTableProperties: TdxBarButton
      Caption = 'Table Properties'
      Category = 6
      Enabled = False
      Hint = 'Table Properties'
      Visible = ivAlways
    end
    object btnFontColorItem: TdxBarButton
      Caption = 'FontColorItem'
      Category = 4
      Visible = ivAlways
    end
    object btnDownFontColorItem: TdxBarButton
      Caption = 'DownFontColorItem'
      Category = 4
      Hint = 'DownFontColorItem'
      Visible = ivAlways
      ButtonStyle = bsChecked
    end
    object btnCustomFontColor: TdxBarButton
      Caption = 'CustomFontColor'
      Category = 4
      Hint = 'CustomFontColor'
      Visible = ivAlways
    end
    object btnColorItem: TdxBarButton
      Caption = 'ColorItem'
      Category = 4
      Hint = 'ColorItem'
      Visible = ivAlways
    end
    object btnDownColorItem: TdxBarButton
      Caption = 'DownColorItem'
      Category = 4
      Hint = 'DownColorItem'
      Visible = ivAlways
      ButtonStyle = bsChecked
    end
    object btnCustomColor: TdxBarButton
      Caption = 'CustomColor'
      Category = 4
      Hint = 'CustomColor'
      Visible = ivAlways
    end
    object btnStandard: TdxBarButton
      Tag = 1
      Caption = 'Standard'
      Category = 2
      Hint = 'Standard'
      Visible = ivAlways
      AllowAllUp = True
      ButtonStyle = bsChecked
      Down = True
    end
    object btnFormatting: TdxBarButton
      Tag = 2
      Caption = 'Formatting'
      Category = 2
      Hint = 'Formatting'
      Visible = ivAlways
      AllowAllUp = True
      ButtonStyle = bsChecked
      Down = True
    end
    object btnInternet: TdxBarButton
      Tag = 3
      Caption = 'Internet'
      Category = 2
      Hint = 'Internet'
      Visible = ivAlways
      AllowAllUp = True
      ButtonStyle = bsChecked
      Down = True
    end
    object cbAddress: TdxBarCombo
      Caption = 'Address'
      Category = 9
      Hint = 'Address'
      Visible = ivAlways
      ImageIndex = 76
      Width = 100
      Items.Strings = (
        'http://www.devexpress.com/'
        'http://www.devexpress.com/products/eforumlib.htm')
      ItemIndex = -1
    end
    object btnBack: TdxBarLargeButton
      Caption = 'Back'
      Category = 9
      Hint = 'Back'
      Visible = ivAlways
      ImageIndex = 198
      HotGlyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000000000000FFFFFF00FF00
        FF00FFFFCC00FFFF9900FFCC3300FF993300FF66330004040400000000000000
        0000000000000000000000000000000000000000000000000000111111111111
        1111111100001111111111111111111100001111111111111111111100001111
        1111111111111111000011111111711111111111000011111117711111111111
        0000111111757111111111110000111117557111111111110000111175457777
        7777711100001117544555555555711100001162344444444445711100001116
        2343232332357111000011116233766666667111000011111623711111111111
        0000111111627111111111110000111111167111111111110000111111117111
        1111111100001111111111111111111100001111111111111111111100001111
        11111111111111110000}
      Width = 50
    end
    object btnForward: TdxBarLargeButton
      Caption = 'Forward'
      Category = 9
      Hint = 'Forward'
      Visible = ivAlways
      ImageIndex = 199
      HotGlyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000000000000FFFFFF00FF00
        FF00FFFFCC00FFFF9900FFCC3300FF993300FF66330004040400000000000000
        0000000000000000000000000000000000000000000000000000111111111111
        1111111100001111111111111111111100001111111111111111111100001111
        1111111111111111000011111111111711111111000011111111111771111111
        0000111111111117671111110000111111111117567111110000111777777777
        5467111100001116555555555446711100001116244444444444671100001116
        2222222224347111000011166666666623471111000011111111111624711111
        0000111111111116471111110000111111111116711111110000111111111116
        1111111100001111111111111111111100001111111111111111111100001111
        11111111111111110000}
      Width = 50
    end
    object btnStop: TdxBarLargeButton
      Caption = 'Stop'
      Category = 9
      Hint = 'Stop'
      Visible = ivAlways
      ImageIndex = 200
      HotGlyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000000000000FFFFFF003399
        FF006666FF000066FF00FF00FF003300FF006666CC000000CC00000099000404
        0400000000000000000000000000000000000000000000000000444444444444
        4444444400004444449999999444444400004444998888888994444400004449
        7777777788894444000044875555557777889444000048735555555577788944
        0000483355555555557789440000823550005550007788940000821555000500
        0557789400008215555000005557789400008215555500055555789400008213
        5550000055557894000082135500050005557894000086113000555000557894
        0000482133555555555579440000486113355555555589440000448211333555
        5557944400004448621113355579444400004444886222227884444400004444
        44888888844444440000}
      Width = 50
    end
    object btnRefresh: TdxBarLargeButton
      Caption = 'Refresh'
      Category = 9
      Hint = 'Refresh'
      Visible = ivAlways
      ImageIndex = 201
      HotGlyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000000000000FFFFFF0099FF
        FF00FF00FF00CCCCCC0080808000009933000404040066990000666600000000
        0000000000000000000000000000000000000000000000000000222222222222
        2222222200002666666666666666662200002433333333333333362200002400
        1010101010103622000024010101050101013622000024001010550010103622
        0000240101055555010136220000240010105507801036220000240001010500
        8101362200002400001010108010362200002401018101010101362200002400
        1080050010103622000024010187055001013622000024001005555510103622
        0000240000000550013336220000240000001500166666220000240000000101
        0400622200002400000000100406222200002400000000000462222200002444
        44444444442222220000}
      Width = 50
    end
    object btnHome: TdxBarLargeButton
      Caption = 'Home'
      Category = 9
      Hint = 'Home'
      Visible = ivAlways
      ImageIndex = 202
      HotGlyph.Data = {
        E6040000424DE604000000000000360000002800000014000000140000000100
        180000000000B004000000000000000000000000000000000000FF00FF040404
        0404040404040404040404040404040404040404040404040404040404040404
        04040404040404040404040404040404FF00FFFF00FFFF00FF808080CCCCCCCC
        CCCCCCCCCCCCCCCCCCCCCC0066990099CC006699006699040404CCCCCCCCCCCC
        CCCCCCCCCCCCCCCCCC040404FF00FFFF00FFFF00FF808080FFFFFF99FFFFFFFF
        FF99FFFFFFFFFF0066990099FF0099CC006699040404FFFFFF99FFFFFFFFFF99
        FFFFCCCCCC040404FF00FFFF00FFFF00FF808080FFFFFFFFFFFF99FFFFFFFFFF
        99FFFF00669900CCFF0099FF0099CC04040499FFFFFFFFFF99FFFFFFFFFFCCCC
        CC040404FF00FFFF00FFFF00FF808080FFFFFF99FFFFFFFFFF99FFFFFFFFFF00
        669900CCFF00CCFF0099FF040404FFFFFF99FFFFFFFFFF99FFFFCCCCCC040404
        FF00FFFF00FFFF00FF808080FFFFFFFFFFFF99FFFFFFFFFF99FFFF00669999FF
        FF00CCFF00CCFF04040499FFFFFFFFFF99FFFFFFFFFFCCCCCC040404FF00FFFF
        00FFFF00FF808080FFFFFFFFFFFFFFFFFF99FFFFFFFFFF006699006699006699
        006699040404FFFFFF99FFFFFFFFFF99FFFFCCCCCC040404FF00FFFF00FFFF00
        FF808080FFFFFFFFFFFF99FFFFFFFFFF99FFFFFFFFFF99FFFFFFFFFF99FFFFFF
        FFFF99FFFFFFFFFF99FFFFFFFFFFCCCCCC040404FF00FFFF00FFFF00FF808080
        669999FFFFFFFFFFFF99FFFFFFFFFF99FFFFFFFFFF99FFFFFFFFFF99FFFFFFFF
        FF99FFFFFFFFFF99FFFF669999040404FF00FFFF00FF04040404040404040466
        9999FFFFFFFFFFFF99FFFFFFFFFFFFFFFFFFFFFF99FFFFFFFFFF99FFFFFFFFFF
        99FFFFFFFFFF040404040404040404FF00FF00669999FFFF0099CC0404046699
        99FFFFFFFFFFFFFFFFFFFFFFFF99FFFFFFFFFF99FFFFFFFFFF99FFFFFFFFFF04
        040499FFFF00CCFF040404FF00FFFF00FF00669999FFFF0099CC040404669999
        FFFFFFFFFFFFFFFFFFFFFFFF99FFFFFFFFFF99FFFFFFFFFF04040499FFFF00CC
        FF040404FF00FFFF00FFFF00FFFF00FF00669999FFFF0099CC040404669999FF
        FFFFFFFFFFFFFFFFFFFFFF99FFFFFFFFFF04040499FFFF00CCFF040404FF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FF00669999FFFF0099CC040404669999FFFF
        FFFFFFFF99FFFFFFFFFF04040499FFFF00CCFF040404FF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FF00669999FFFF0099CC040404669999FFFFFF
        FFFFFF04040499FFFF00CCFF040404040404FF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00669999FFFF0099CC04040466999904040499
        FFFF00CCFF0404046600FF040404FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF00669999FFFF0099CC04040499FFFF00CCFF0404
        040000996600FF040404FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF00669999FFFF0099CC00CCFF040404FF00FF000099
        6600FF040404FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FF00669999FFFF040404FF00FFFF00FF0000996600FF04
        0404FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF006699FF00FFFF00FFFF00FF000099000099040404FF00
        FFFF00FFFF00FFFF00FF}
      Width = 50
    end
  end
end
