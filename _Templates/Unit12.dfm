object Form12: TForm12
  Left = 574
  Top = 397
  Width = 779
  Height = 417
  Caption = 'Form12'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object LabeledEdit1: TLabeledEdit
    Left = 13
    Top = 255
    Width = 217
    Height = 21
    EditLabel.Width = 65
    EditLabel.Height = 13
    EditLabel.Caption = 'sdffffffffffffffffff'
    TabOrder = 4
  end
  object ControlBar1: TControlBar
    Left = -1
    Top = 35
    Width = 296
    Height = 27
    AutoDock = False
    AutoDrag = False
    AutoSize = True
    BevelInner = bvNone
    RowSnap = False
    TabOrder = 5
    object MaskEdit2: TMaskEdit
      Left = 13
      Top = 2
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'MaskEdit2'
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Bars = <
      item
        Caption = 'Custom 1'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 604
        FloatTop = 357
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButton1
            Visible = True
          end>
        Name = 'Custom 1'
        OneOnRow = True
        Row = 0
        ShowMark = False
        UseOwnFont = False
        Visible = True
        WholeRow = False
      end>
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 177
    Top = 81
    DockControlHeights = (
      0
      0
      28
      0)
    object dxBarButton1: TdxBarButton
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
    end
  end
  object ActionManager1: TActionManager
    ActionBars = <
      item
        Items = <
          item
            Action = Action2
            Caption = '&Action2'
          end
          item
            Action = Action3
            Caption = 'A&ction3'
          end>
      end
      item
        Items = <
          item
            Action = Action3
            Caption = '&Action3'
          end
          item
            Action = Action2
            Caption = 'A&ction2'
          end>
      end
      item
        Items = <
          item
            Action = Action2
            Caption = '&Action2'
          end>
      end>
    Left = 252
    Top = 80
    StyleName = 'XP Style'
    object Action1: TAction
      Caption = 'Action1'
    end
    object Action2: TAction
      Caption = 'Action2'
    end
    object Action3: TAction
      Caption = 'Action3'
    end
  end
end
