unit u_img_PNG;

interface

uses Graphics,SysUtils, Windows, Dialogs, Forms, FileCtrl, extctrls,

//     PNGImage_;
     PNGImage;


  procedure img_BMP_to_PNG(aBmpFileName, aPNGFileName: ShortString;
      aIsTransparent: boolean = false; aTransparentColor: TColor = 0); stdcall;



      
exports
  img_BMP_to_PNG;



implementation

//------------------------------------------------------
procedure img_BMP_to_PNG(aBmpFileName, aPNGFileName: ShortString;
    aIsTransparent: boolean = false; aTransparentColor: TColor = 0);
//------------------------------------------------------
var
    oBMP: Graphics.TBitmap;

    oPNG:TPNGObject;
    i,j: Integer;
    pLine: pRGBLine;
    pAlphaLine: pByteArray;
begin
  Assert(aPNGFileName<>'');


  aBmpFileName:= ChangeFileExt(aBmpFileName, '.bmp');
  aPNGFileName:= ChangeFileExt(aPNGFileName, '.png');

  Assert( FileExists( aBmpFileName));

//  DeleteFile(aPNGFileName);


  oBMP:= Graphics.TBitmap.Create;
 // oBMP.PixelFormat := pf16bit;
  try
    oBMP.LoadFromFile(aBmpFileName);
  except
    ShowMessage('�� ������� ��������� ����: '+aBmpFileName);

    raise Exception.Create('�� ������� ��������� ����: '+aBmpFileName);
    oBMP.Free;
    Exit;
  end;

  oPNG:= TPNGObject.Create;
  try
    oPNG.Assign(oBMP);
  except
    raise Exception.Create('�� ������� ��������� ����: '+aPNGFileName);
    oBMP.Free;
    oPNG.Free;
    Exit;
  end;

  oPNG.Transparent:= True;

          {
  oPNG.CreateAlpha;

  if aIsTransparent then
    for i := 0 to oPNG.Height - 1 do
    begin
      pLine := oPNG.Scanline[i];
      pAlphaLine := oPNG.AlphaScanline[i];
      for j := 0 to oPNG.Width - 1 do
        try
          with pLine[j] do
          if RGB(rgbtRed, rgbtGreen, rgbtBlue) = aTransparentColor then
            pAlphaLine[j]:= 0; //��������� ������������
        except
        end;
    end;

    }

  oPNG.SaveToFile(aPNGFileName);

  oBMP.Free;
  oPNG.Free;
end;    //




end.
