unit u_calc_points;

//===================================================================
interface
//===================================================================
uses Sysutils, Math, Messages, Windows, Classes, DB,
     dxmdaset,

     u_GEO,
     u_func,
     u_db,
     u_files,

     u_const_db,

     u_profile_types,

     u_xml;


type
  Tdrive_BLPointList = class;
  Tdrive_BLPointItem = class;

  //-------------------------------------------------------------------
  TPointsSummaryRec = record
  //-------------------------------------------------------------------
    AveError,CKO,CCKO: double;
    Dispersion: double;
    PointCount: integer;
    Open_K0, Closed_K0: double;

  end;

  //-------------------------------------------------------------------
  Tdrive_Summary = record
  //-------------------------------------------------------------------
    AveError    : double;  // ��� ��������
    CKO,CCKO    : double;
    coeff_corr  : double;
    Dispersion  : double;
    PointCount  : integer;
    CalcPointCount: integer;  //����� � ��������

    MinDistance : double;
    MaxDistance : double;

    Types: array[TrelProfileType] of record
      Type_ : TrelProfileType;
      //Stat  : TPointsStatRec;
      AveError    : double;  // ��� ��������
      CKO,CCKO    : double;
      PointCount  : integer;
    end;
  end;


  //-------------------------------------------------------------------
  Tdrive_BLPointItemRec = record
  //-------------------------------------------------------------------
    ID     : integer;
    BLPoint: TBLPoint;

    Type_  : TrelProfileType;
    RxLev_calc : integer;    // KUP calculated
    RxLev_drive: integer; // KUP drive
    Distance: double;
  end;

  //-------------------------------------------------------------------
  Tdrive_BLPointList = class(TCollection)
  //-------------------------------------------------------------------
  private
    MemData: TdxMemData;

    function GetItem (Index: integer): Tdrive_BLPointItem;
    procedure CalcSummary (var aSummary: Tdrive_Summary);

  public
    constructor Create;
    destructor Destroy; override;

    procedure AddItem (aRec: Tdrive_BLPointItemRec);
    procedure Assign  (aSource: Tdrive_BLPointList);
    procedure Append (aSource: Tdrive_BLPointList);

    procedure CalcCalibrateSummary (var aRec: TPointsSummaryRec);

    //��� ������� ������: k0_open, k0_closed
    procedure CalcOpenAndClosed_K0 (var aOpen_K0, aClosed_K0: double);

    procedure CalcSummaryFull (var aSummary: Tdrive_Summary);

    procedure CalcAveAndCKO (aProfileType: TrelProfileType;
                             var aRec: TPointsSummaryRec); overload;

    procedure CalcAveAndCKO (var aRec: TPointsSummaryRec); overload;
    procedure CalcAveAndCKO (var aAveErr,aCKO: double); overload;

    procedure SaveToFile   (aFileName: string);
    procedure LoadFromFile (aFileName: string);
    procedure LoadFromDataset(aDataset: TDataset);

    property  Items [Index: integer]: Tdrive_BLPointItem read GetItem; default;
  end;

  //-------------------------------------------------------------------
  Tdrive_BLPointItem = class (TCollectionItem)
  //-------------------------------------------------------------------
  public
    Data   : Tdrive_BLPointItemRec;
    XYPoint: TXYPoint;

    function RxLev_error: integer;
    function IsCalculated: boolean;
    function TypeStr: string;
  end;


  //-------------------------------------------------------------------
  TRegionBLPointList = class(TList)
  //-------------------------------------------------------------------
  private
    function GetItem (Index: integer): Tdrive_BLPointList;
    procedure Clear; override;
  public
    function NewItem: Tdrive_BLPointList;
    property Items [Index: integer]: Tdrive_BLPointList read GetItem; default;
  end;


//===================================================================
implementation
//===================================================================

//const
//  AVE_MAX = 0.001; // ���� ������� ������ ���� �������� -> AVE = 0


//-------------------------------------------------------------------
procedure Tdrive_BLPointList.LoadFromFile(aFileName: string);
//-------------------------------------------------------------------
var rec: Tdrive_BLPointItemRec;
begin
{
  if not FileExists(aFileName) then begin
//////////    raise Exception.Create('File not exists: '+ aFileName);
    Exit;
  end;

  MemData.LoadFromBinaryFile (aFileName);

  Clear;

  with MemData do
    while not EOF do
  begin
    rec.ID         :=FieldValues[FLD_ID];
    rec.BLPoint    :=db_ExtractBLPoint (MemData);
    rec.RxLev_calc :=FieldValues[FLD_RXLEV_CALC];
    rec.RxLev_drive:=FieldValues[FLD_RXLEV_DRIVE];
    rec.Type_      :=FieldValues[FLD_TYPE];

    AddItem (rec);
    Next;
  end;
}
end;

//-------------------------------------------------------------------
procedure Tdrive_BLPointList.LoadFromDataset(aDataset: TDataset);
//-------------------------------------------------------------------
var rec: Tdrive_BLPointItemRec;
begin{
  Clear;

  with aDataset do
    while not EOF do
  begin
    rec.ID         :=FieldValues[FLD_ID];
    rec.BLPoint    :=db_ExtractBLPoint (aDataset);
    rec.RxLev_calc :=FieldByName(FLD_RXLEV_CALC).AsInteger;
    rec.RxLev_drive:=FieldValues[FLD_RXLEV_DRIVE];
    rec.Type_      :=TrelProfileType(FieldByName(FLD_TYPE).AsInteger);
    rec.Distance   :=FieldByName(FLD_DISTANCE).AsFloat;

    AddItem (rec);
    Next;
  end;}
end;

//--------------------------------------------------------------
procedure Tdrive_BLPointList.SaveToFile(aFileName: string);
//--------------------------------------------------------------
var i: integer;
begin
  MemData.Close;
  MemData.Open;

  for i:=0 to Count-1 do
    db_AddRecord (MemData,
        [db_Par(FLD_ID,  Items[i].Data.ID),
         db_Par(FLD_LAT, Items[i].Data.BLPoint.B),
         db_Par(FLD_LON, Items[i].Data.BLPoint.L),

         db_Par(FLD_TYPE,        Items[i].Data.Type_),
         db_Par(FLD_RXLEV_CALC,  Items[i].Data.RxLev_calc),
         db_Par(FLD_RXLEV_DRIVE, Items[i].Data.RxLev_drive)
        ]);

  ForceDirByFileName (aFileName);
//  MemData.SaveToTextFile (aFileName);
  MemData.SaveToBinaryFile (aFileName);
end;

//--------------------------------------------------------------
procedure Tdrive_BLPointList.Assign (aSource: Tdrive_BLPointList);
//--------------------------------------------------------------
begin
  Clear;
  Append (aSource);
end;

//--------------------------------------------------------------
procedure Tdrive_BLPointList.Append (aSource: Tdrive_BLPointList);
//--------------------------------------------------------------
var i: integer;  obj: Tdrive_BLPointItem;
begin
  for i:=0 to aSource.Count-1 do
  begin
    obj:=Tdrive_BLPointItem.Create(Self);
    obj.Data:=aSource.Items[i].Data;
  end;
end;

//-------------------------------------------------------------------
constructor Tdrive_BLPointList.Create;
//-------------------------------------------------------------------
begin
  inherited Create(Tdrive_BLPointItem);

  MemData:=TdxMemData.Create(nil);
  db_CreateFieldArr (MemData,
    [ db_Field(FLD_ID,  ftInteger),
      db_Field(FLD_LAT, ftFloat),
      db_Field(FLD_LON, ftFloat),
      db_Field(FLD_TYPE, ftInteger),
      db_Field(FLD_RXLEV_CALC,  ftInteger),
      db_Field(FLD_RXLEV_DRIVE, ftInteger)
     ]);
end;


destructor Tdrive_BLPointList.Destroy;
begin
  MemData.Free;
  inherited;
end;


procedure Tdrive_BLPointList.AddItem (aRec: Tdrive_BLPointItemRec);
var obj: Tdrive_BLPointItem;
begin
  obj:=Tdrive_BLPointItem.Create(Self);
  obj.Data:=aRec;
end;


function Tdrive_BLPointList.GetItem(Index: integer): Tdrive_BLPointItem;
begin
  Result:=Tdrive_BLPointItem(inherited Items[Index]);
end;

//-------------------------------------------------------------------
procedure Tdrive_BLPointList.CalcSummary (var aSummary: Tdrive_Summary);
//-------------------------------------------------------------------
var x,y1,y2: double;
    ave_error,
    cko_error: double;

    sum_err: double; // �����
    sum2: double; // �����
    iCalculatedCount,i: integer;
    K_XY, // �������������� ������
    coeff_corr,
    S_calc, S_drive,
    M_calc, M_drive: double; // ������� ��������
    sum,sum_calc,sum_drive: double;

begin
  FillChar(aSummary, SizeOf(aSummary), 0);

  aSummary.PointCount:=Count;

  if Count=0 then
    Exit;

  sum_err:=0;  ave_error:=0;
  //- ������� �������� ------------------
  sum_calc:=0; sum_drive:=0;
  iCalculatedCount:=0;

  for i:=0 to Count-1 do
  begin
    with aSummary do begin
      if (Items[i].Data.Distance > MaxDistance) then
        MaxDistance:=Items[i].Data.Distance;
      if (MinDistance=0) or
        (Items[i].Data.Distance < MinDistance)
      then
        MinDistance:=Items[i].Data.Distance;
    end;

    if not Items[i].IsCalculated then
      Continue;

    Inc(iCalculatedCount);
    sum_err  :=sum_err   + Items[i].RxLev_error;    // '������� �������� ������';
    sum_calc :=sum_calc  + Items[i].Data.RxLev_calc;
    sum_drive:=sum_drive + Items[i].Data.RxLev_drive;

  end;


  if iCalculatedCount=0 then
    Exit;

  M_calc:=sum_calc / iCalculatedCount;
  M_drive:=sum_drive / iCalculatedCount;

  // ���������� ���� ������� ------------
  sum:=0; //K_XY:=0;
  for i:=0 to Count-1 do
    if Items[i].IsCalculated then
  begin
    sum:=sum + (Items[i].Data.RxLev_calc - M_calc) *
               (Items[i].Data.RxLev_drive - M_drive);
  end;
  K_XY:=(1 / iCalculatedCount) * sum;


  // ���������� ����� -------------------
  sum_calc:=0; sum_drive:=0;
  for i:=0 to Count-1 do
    if Items[i].IsCalculated then
  begin
    sum_calc:=sum_calc   + Sqr(Items[i].Data.RxLev_calc);
    sum_drive:=sum_drive + Sqr(Items[i].Data.RxLev_drive);
  end;
  S_calc :=Sqrt((sum_calc/iCalculatedCount) - Sqr(M_calc));
  S_drive:=Sqrt((sum_drive/iCalculatedCount) - Sqr(M_drive));


  if (S_calc>0) and (S_calc * S_drive<>0)
    then coeff_corr:=K_XY / (S_calc * S_drive)
    else coeff_corr:=0;

  if iCalculatedCount>0 then
  begin
    aSummary.AveError:=TruncFloat(sum_err / iCalculatedCount, 2);
//    if Abs(aSummary.AveError) < AVE_MAX then
//      aSummary.AveError:=0;
  end;

  aSummary.coeff_corr := coeff_corr;


  //------------------------
  //������ CKO, ���������
  //------------------------

  //������ ����� ���������
  sum2:=0;
  for i:=0 to Count-1 do
    if Items[i].IsCalculated then
      sum2 := sum2 + Sqr(Items[i].RxLev_error);


  with aSummary do
    if iCalculatedCount>1 then begin
      Dispersion:=sum2/iCalculatedCount - Sqr(aSummary.AveError);
      CCKO      :=Sqrt(sum2/iCalculatedCount);
      CKO       :=Sqrt(Dispersion);

    end else begin
      Dispersion:=0;  CKO:=0;
    end;

end;

//-------------------------------------------------------------------
procedure Tdrive_BLPointList.CalcSummaryFull (var aSummary: Tdrive_Summary);
//-------------------------------------------------------------------
const
  TYPES : array[0..8] of TrelProfileType =
     (
      ptOpen,

      ptHalfOpened_relief,
      ptHalfOpened_forest,
      ptHalfOpened_city,
      ptHalfOpened_country,

      ptClosed_relief,
      ptClosed_forest,
      ptClosed_city,
      ptClosed_country
     );

var
  i: integer;
  rStat: TPointsSummaryRec;
begin
  CalcSummary (aSummary);

  for i:=0 to High(TYPES) do
    with aSummary.Types[TYPES[i]] do
    begin
      Type_:=TYPES[i]; //test

      CalcAveAndCKO (TYPES[i], rStat);

      CKO :=rStat.CKO;
      CCKO:=rStat.CCKO;
      AveError:=rStat.AveError;
      PointCount:=rStat.PointCount;
    end;

  aSummary.Types[ptClosed].PointCount:=
    aSummary.Types[ptClosed_relief].PointCount +
    aSummary.Types[ptClosed_forest].PointCount +
    aSummary.Types[ptClosed_city].PointCount +
    aSummary.Types[ptClosed_country].PointCount;

end;



//-------------------------------------------------------------------
procedure Tdrive_BLPointList.CalcAveAndCKO (aProfileType: TrelProfileType;
                                            var aRec: TPointsSummaryRec);
//-------------------------------------------------------------------
  function DoCheckPoint (aIndex: integer): boolean ;
  begin
    Result:=Items[aIndex].IsCalculated;
    if Result then
      Result:=(aProfileType = ptAny) or
              ((aProfileType <> ptAny) and
               (Items[aIndex].Data.Type_=aProfileType));
  end;


var sum2,sum_err: double; // �����
    i,iCalculatedCount: integer;

begin
  FillChar(aRec, SizeOf(aRec), 0);

  if Count=0 then
    Exit;

  sum_err:=0;
  iCalculatedCount:=0;

  for i:=0 to Count-1 do
    if DoCheckPoint(i) then begin
      sum_err:=sum_err + Items[i].RxLev_error;    // '������� �������� ������';
      Inc(iCalculatedCount);
    end;

  if iCalculatedCount=0 then
    Exit;

  aRec.PointCount:=iCalculatedCount;
  aRec.AveError:=sum_err / iCalculatedCount;

  sum2:=0;
  for i:=0 to Count-1 do
    if DoCheckPoint(i) then
      sum2 := sum2 + Sqr(Items[i].RxLev_error);

  aRec.Dispersion:=(sum2 / iCalculatedCount) - Sqr(aRec.AveError);
  aRec.CKO       :=Sqrt(aRec.Dispersion);
  aRec.CCKO      :=Sqrt(sum2 / iCalculatedCount);


  TruncFloatVar (aRec.CKO, 2);
  TruncFloatVar (aRec.AveError, 2);

end;

//-------------------------------------------------------------------
procedure Tdrive_BLPointList.CalcAveAndCKO (var aRec: TPointsSummaryRec);
//-------------------------------------------------------------------
begin
  CalcAveAndCKO (ptAny, aRec);
end;

//-------------------------------------------------------------------
procedure Tdrive_BLPointList.CalcAveAndCKO (var aAveErr,aCKO: double);
//-------------------------------------------------------------------
var rec: TPointsSummaryRec;
begin
  CalcAveAndCKO (ptAny, rec);
  aCKO:=rec.CKO;
  aAveErr:=rec.AveError;
end;


//-------------------------------------------------------------------
procedure Tdrive_BLPointList.CalcOpenAndClosed_K0 (var aOpen_K0, aClosed_K0: double);
//-------------------------------------------------------------------
const
  CLOSED_TYPES : array[0..7] of TrelProfileType =
     (
      ptHalfOpened_relief,
      ptHalfOpened_forest,
      ptHalfOpened_city,
      ptHalfOpened_country,

      ptClosed_relief,
      ptClosed_forest,
      ptClosed_city,
      ptClosed_country
     );

var recSummary: Tdrive_Summary;
    i: integer;
    dSumTop,dSumBottom: double;
begin
  CalcSummaryFull (recSummary);

  aOpen_K0:= recSummary.Types[ptOpen].AveError;

  dSumTop:=0;
  dSumBottom:=0;

  for i:=0 to High(CLOSED_TYPES) do
    with recSummary.Types[CLOSED_TYPES[i]] do
  begin
    dSumTop   :=dSumTop    + PointCount*AveError;
    dSumBottom:=dSumBottom + PointCount;
  end;

  if dSumBottom<>0 then
    aClosed_K0:=dSumTop / dSumBottom
  else
    aClosed_K0:=0;

  TruncFloatVar(aClosed_K0, 2);
end;


//-------------------------------------------------------------------
// Tdrive_BLPointItem
//-------------------------------------------------------------------

function Tdrive_BLPointItem.IsCalculated: boolean;
begin
  Result:=Data.Type_<>ptUnknown;
end;

function Tdrive_BLPointItem.RxLev_error: integer;
begin
  Result:=Data.RxLev_drive - Data.RxLev_calc;
end;



function Tdrive_BLPointItem.TypeStr: string;
begin
  Result:= profile_TypeToStr (TrelProfileType(Data.Type_));
end;


procedure Tdrive_BLPointList.CalcCalibrateSummary(var aRec: TPointsSummaryRec);
begin
  CalcAveAndCKO (aRec);
  CalcOpenAndClosed_K0 (aRec.Open_K0, aRec.Closed_K0);
end;

//-------------------------------------------------------------------
// TRegionBLPointList 
//-------------------------------------------------------------------

procedure TRegionBLPointList.Clear;
var i: integer;
begin
  for i:=0 to Count-1 do Items[i].Free;
  inherited;
end;

function TRegionBLPointList.GetItem(Index: integer): Tdrive_BLPointList;
begin
  Result:=Tdrive_BLPointList(inherited Items[Index]);
end;

function TRegionBLPointList.NewItem: Tdrive_BLPointList;
var
  obj: Tdrive_BLPointList;
begin
  obj:=Tdrive_BLPointList.Create;
  Add (obj);
end;


end.


