unit d_MIF_export;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Forms,
  OleCtrls,

  MapXLib_TLB,

  u_MapX,
  u_GEO,

  ComCtrls, ToolWin;

 {$DEFINE CLOSE}

type
  TExportMapRec = record
    GeosetFileName: string;

    ImgFileName: string;
    Scale: integer;

    Mode: (mtNone,mtVEctor,mtPoint,mtRect);

    BLPoint1: TblPoint;
    BLVector: TblVector;
    BLRect: TBLRect;

    Width,Height: double;

    IsDebug : Boolean;

    MapFileList : TStringList;
  end;

type

  Tdlg_MIF_export = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);

  protected

    procedure ExportMap;

  private
    FMap: TMap;
    FParams: TExportMapRec;

  public
    class procedure Execute(aRec: TExportMapRec);
  end;

//procedure MIF_ExportMap (aRec: TExportMapRec);

(*
var
  dlg_MIF_export: Tdlg_MIF_export;
*)
implementation
{$R *.dfm}

class procedure Tdlg_MIF_export.Execute(aRec: TExportMapRec);
begin
  with Tdlg_MIF_export.Create(Application) do
  begin
   // obj.ShowModal;

  //  obj.Mode:=aRec;

    FParams:=aRec;

    ExportMap;

  //  ShowModal;
    Free;
  end;
end;


procedure Tdlg_MIF_export.FormCreate(Sender: TObject);
begin
  Caption:='�������� �����...';

  {$IFDEF CLOSE}
  Top:=3000;
  {$ENDIF}

  FMap := TMap.Create(Self);
  InsertControl(FMap);

//  FMap.Parent := Panel1;
end;


procedure Tdlg_MIF_export.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FMap);
  inherited;
end;


// -------------------------------------------------------------------
procedure Tdlg_MIF_export.ExportMap;
// -------------------------------------------------------------------
begin
  FMap.Geoset:=FParams.GeosetFileName;

  if FParams.Scale=0 then
    FParams.Scale:=100000;

  FMap.Top:=50;


  FMap.Title.X:=0;
  FMap.Title.Y:=0;
  FMap.Title.Editable:= False;
  FMap.Title.Border:= False;
  FMap.Title.Caption:='-';
  FMap.Title.Visible:=False;

  FMap.PaperUnit:= miUnitCentimeter;
  FMap.MapUnit  := miUnitCentimeter;

  // Width,Height - in cm
  FMap.Width :=Round (FParams.Width  * (Screen.PixelsPerInch )/2.54)  ;
  FMap.Height:=Round (FParams.Height * (Screen.PixelsPerInch )/2.54)  ;


  {$IFDEF CLOSE}
   PostMessage(Handle, WM_CLOSE, 0,0);
  {$ENDIF}

  ShowModal;

  FMap.ExportMap (FParams.ImgFileName, miFormatBmp);
end;



//--------------------------------------------------------------
procedure Tdlg_MIF_export.FormShow(Sender: TObject);
//--------------------------------------------------------------
begin
  case FParams.Mode of
    mtPoint:  begin
         //       mapx_SetScale (Map1, FParams.Scale);
                if FParams.blPoint1.B<>0 then
                  FMap.ZoomTo (FMap.Zoom, FParams.blPoint1.L, FParams.blPoint1.B);

              end;

    mtVector: mapx_SetBoundsForBLVector (FMap, FParams.BLVector);
  end;

  mapx_SetScale (FMap, FParams.Scale);

end;

procedure Tdlg_MIF_export.ToolButton1Click(Sender: TObject);
begin
  FMap.Layers.LayersDlg(0,0);
end;

end.


