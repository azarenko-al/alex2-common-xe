unit u_Mapinfo_WOR_classes;

interface

uses
  System.Classes, SySutils, Graphics,   StrUtils, System.Generics.Collections, 

//  MapXLib_TLB,

  u_func,
  u_files,

  u_mapX; 


//  System.Contnrs;


type
//  TworMap = class;
 // TworTable = class;
 // TworTableList = class;
 // TMifWorkspace = class;

  // -------------------------------------------------------------------
  TworTable = class //(System.Classes.TCollectionItem)
  // -------------------------------------------------------------------
  public
    Name : string;
    FileName : string;
//    Alias : string;

    Type_ : Integer;
    
//    type
//  LayerTypeConstants = TOleEnum;
//const
//  miLayerTypeNormal = $00000000;
//  miLayerTypeRaster = $00000002;
//  miLayerTypeSeamless = $00000004;
//  miLayerTypeUnknown = $00000005;
//  miLayerTypeUserDraw = $00000006;
//  miLayerTypeDrilldown = $00000007;


    LabelProperties_Style:
      record
        TextFontName : string;
        TextFontSize : Integer;
        TextFontBackColor : Integer;

        Position : Integer;

        IsAutoLabel : boolean;
        KeyField : string;

      end;

(*
    sName     :=OleVariant(vLayer.LabelProperties.Style.TextFont).Name;
    iSize     :=OleVariant(vLayer.LabelProperties.Style.TextFont).Size;
    iColorBack:=vLayer.LabelProperties.Style.TextFontBackColor;
    sPos      :=PositionToStr(vLayer.LabelProperties.Position);
*)
  end;


  TworTableList = class(TList<TworTable>)
  private
  //  function GetItems(Index: Integer): TworTable;
  public
//    constructor Create;
    function AddItem: TworTable;

    procedure AddFile(aFileName: string);

 //   property Items[Index: Integer]: TworTable read GetItems; default;
  end;

  
  

 // TworTableList11_ = class(TObjectList);

  
(*
  // -------------------------------------------------------------------
  TworMap = class
  // -------------------------------------------------------------------
  public
    CenterX : double;
    CenterY : double;
    Zoom    : double;

//    FileName : string;
  //  Alias : string;
  end;*)

  TMifWorkspace = class(TworTableList)
  private
  public
    CenterX : double;
    CenterY : double;
    Zoom    : double;

  public
    procedure LoadFromFile(aFileName: string);
  //  procedure LoadFromMap(aMap: TMap);
    procedure SaveToFile(aFileName: string);
  end;



implementation

const
  miLayerTypeNormal = $00000000;
  miLayerTypeRaster = $00000002;
  miLayerTypeSeamless = $00000004;
  miLayerTypeUnknown = $00000005;
  miLayerTypeUserDraw = $00000006;
  miLayerTypeDrilldown = $00000007;
  




  //--------------------------------------------------------
  procedure ColorTo_R_G_B(Value: TColor; var R,G,B: byte);
  //--------------------------------------------------------
  var cl: packed record case b:byte of
            0: (int: TColor);   1: (RGB: array[0..4] of byte);
          end;
  begin
    cl.int:=Value;
    R:=cl.rgb[0];  G:=cl.rgb[1];  B:=cl.rgb[2];
  end;


  function MakeMapinfoColor (aColor: integer): integer;
  var r,g,b: byte;
  begin
    ColorTo_R_G_B (aColor, r,g,b);
    Result:=(r*65536) + (g * 256) + b;
  end;



//
//
//constructor TworTableList.Create;
//begin
//  inherited Create(TworTable);
//end;


function TworTableList.AddItem: TworTable;
begin
  Result := TworTable.Create;
  Add (Result);
end;

// ---------------------------------------------------------------
procedure TworTableList.AddFile(aFileName: string);
// ---------------------------------------------------------------
begin
  with AddItem() do
  begin
    Name    := ExtractFileName(aFileName);
    FileName:=aFileName;
//    Type_   := miLayerTypeRaster;
  end;    
  
  // TODO -cMM: TworTableList.AddFile default body inserted
end;
//
//
//function TworTableList.GetItems(Index: Integer): TworTable;
//begin
//  Result := TworTable(Inherited Items[index]);
//end;


//LoadFromFile(aFileName: string);




procedure TMifWorkspace.LoadFromFile(aFileName: string);
//---------------------------------------------------------
(*var oStrList: TStringList;
    strArr: TStrArray;
    str,str1,str2,str3, sFileName: string;
    dAngle,eLoss: double;
    iAngle,i,total: integer;
    k,m,n,p: integer;
  sCommand: string;

    state: (HEADER_PART,HORIZONTAL_PART_H,VERTICAL_PART_H,
                        HORIZONTAL_PART_V,VERTICAL_PART_V);
*)//    DiagramHorz_H, DiagramVert_H,
//    DiagramHorz_V, DiagramVert_V: TMask;
begin
(*  H.Clear;
  V.Clear;


  Result:=false;

  if not FileExists(aFileName) then
    Exit;

  oStrList:=TStringList.Create;
  Result:=false;

  try
    oStrList.LoadFromFile (AFileName);
  except
  end;


  k:=0; m:=0; n:=0; p:=0;

  i := 0;

  while i<oStrList.Count do
  begin
    s:=oStrList[i];
    i:=i+1;

    if Eq(s,'!Workspace:') then Continue;
    if Eq(s,'!Version 600:') then Continue;
    if Eq(s,'!Charset WindowsCyrillic') then Continue;


    sCommand := 'Open Table';
    if Copy(s, 1, SizeOf(sCommand)) = sCommand then
    ;

    sCommand := 'Map From';
    if Copy(s, 1, SizeOf(sCommand)) = sCommand then
    ;

    sCommand := 'Set Map';
    if Copy(s, 1, SizeOf(sCommand)) = sCommand then
    ;

    sCommand := 'Set Window';
    if Copy(s, 1, SizeOf(sCommand)) = sCommand then
    ;

*)


(*
Set Window FrontWindow() ScrollBars Off Autoscroll On
Set Map
  CoordSys Earth Projection 1, 0
  Center (37.10639,57.25866831)
  Zoom 1013.767682 Units "km"
  Preserve Zoom Display Zoom
  Distance Units "km" Area Units "sq km" XY Units "degree"
Set Map
  Layer 1
    Display Graphic
    Global   Symbol (35,0,12)
    Label Line Arrow Position Above Font ("Arial Cyr",0,9,0) Pen (1,2,0)
      With bts_1
      Parallel On Auto Off Overlap Off Duplicates Off Offset 2
      Visibility On
Set Window FrontWindow() Printer
 Name "Microsoft Office Document Image Writer" Orientation Portrait Copies 1
 Papersize 9
Browse * From line_rrl12_sdh
  Position (0.958333,0.46875) Units "in"
  Width 13.1042 Units "in" Height 6.98958 Units "in"
  Row 100 Column 3
Set Window Frontwindow() Font ("Arial CYR",0,8,0)
Set Window FrontWindow() Printer
 Name "Microsoft Office Document Image Writer" Orientation Portrait Copies 1
 Papersize 9
*)


//    Open Table "E:\1. Customers\mts moscow\line_rrl12_sdh" As line_rrl12_sdh Interactive

(*

  end;



  for i:=0 to oStrList.Count-1 do
  begin
    // �������� ���������� �� �������
    oStrList[i]:=ReplaceStr (oStrList[i], #9, ' ');
    oStrList[i]:=ReplaceStr (oStrList[i], ',', ' ');

    strArr:=StringToStrArray (oStrList[i],' ');
    total:=High(strArr)+1; str1:=''; str2:=''; str3:='';
    if total>=1 then str1:=strArr[0];
    if total>=2 then str2:=strArr[1];
    if total>=3 then str3:=strArr[2];

    if Eq(str1,'MODNUM:')     then ModelName := str2 + str3     else
    if Eq(str1,'LOWFRQ:')     then Freq_MHz := AsFloat(str2) else
    if Eq(str1,'HGHFRQ:')     then Freq_MHz := (AsFloat(str2)+Freq_MHz)/2 else
    if Eq(str1,'MDGAIN:')     then Gain_dBi := AsFloat(str2) else
    if Eq(str1,'ELTILT:')     then TiltElectrical  :=str2    else
    if Eq(str1,'FRTOBA:')     then FrontToBackRatio:= AsFloat(str2) else

    if Eq(str1,'AZWIDT:')     then DNAHorzWidth := AsFloat(str2) else
    if Eq(str1,'ELWIDT:')     then DNAVertWidth := AsFloat(str2) else

    if (Eq(str1,'POLARI:')) and (Eq(str2,'H/H'))
                             then state:=HORIZONTAL_PART_H   else
    if (Eq(str1,'POLARI:')) and (Eq(str2,'H/V'))
                             then state:=VERTICAL_PART_H     else
    if (Eq(str1,'POLARI:')) and (Eq(str2,'V/H'))
                             then state:=HORIZONTAL_PART_V   else
    if (Eq(str1,'POLARI:')) and (Eq(str2,'V/V'))
                             then state:=VERTICAL_PART_V     else

    if Eq(str1,'PATCUT:')     then Continue  else
    if Eq(str1,'NUPOIN:')     then Continue  else
    if Eq(str1,'ENDFIL:')     then Break  else

    if state in [HORIZONTAL_PART_H,VERTICAL_PART_H,
                 HORIZONTAL_PART_V,VERTICAL_PART_V] then
    begin
      dAngle:=AsFloat(str1);
      if (dAngle<0) and (dAngle>=-180) then   dAngle:= 360 + dAngle;

      eLoss :=Abs(AsFloat(str2));

      if (dAngle>=0) and (dAngle<360) then
        if state=HORIZONTAL_PART_H then
          if not Find_Angle_InMask(dAngle, H.Horz) then
          begin
            SetLength(H.Horz, Length(H.Horz)+1);
            H.Horz[k].Loss:=eLoss;
            H.Horz[k].Angle:=dAngle;
            k:=k+1;
          end else

        if state=VERTICAL_PART_H then
          if not Find_Angle_InMask(dAngle, H.Vert) then
          begin
            SetLength(H.Vert, Length(H.Vert)+1);
            H.Vert[m].Loss:=eLoss;
            H.Vert[m].Angle:=dAngle;
            m:=m+1;
          end;

        if state=HORIZONTAL_PART_V then
          if not Find_Angle_InMask(dAngle, V.Horz) then
          begin
            SetLength(V.Horz, Length(V.Horz)+1);
            V.Horz[n].Loss:=eLoss;
            V.Horz[n].Angle:=dAngle;
            n:=n+1;
          end else

        if state=VERTICAL_PART_V then
          if not Find_Angle_InMask(dAngle, V.Vert) then
          begin
            SetLength(V.Vert, Length(V.Vert)+1);
            V.Vert[p].Loss:=eLoss;
            V.Vert[p].Angle:=dAngle;
            p:=p+1;
          end;
    end;
  end;

  Result:=true; // Everything's OK

  oStrList.Free;*)
end;

procedure TMifWorkspace.SaveToFile(aFileName: string);

     //-------------------------------------------------------------------
     function GetMapinfoShortFileName(aFileName: string): string;
     //-------------------------------------------------------------------
     var
       s: string;
     begin
      aFileName:=ExtractFileNameNoExt(aFileName) ;
      aFileName:=ReplaceStr(aFileName,' ','_');
      aFileName:=ReplaceStr(aFileName,'.','_');
      aFileName:=ReplaceStr(aFileName,'-','_');
      aFileName:=ReplaceStr(aFileName,',','_');

      s:=Copy(aFileName, 1,1);
      if Copy(aFileName, 1,1)[1] in ['0'..'9'] then
        aFileName:='_'+ aFileName;

      Result := aFileName;
    end;


    function MiPositionToStr(aValue: Integer): string;
    begin
      case aValue of
        miPositionCC : Result := 'Center';
        miPositionTL : Result := 'Above RIGHT';
        miPositionTC : Result := 'Above Left';
        miPositionTR : Result := 'Above Left';
        miPositionCL : Result := 'LEFT';
        miPositionCR : Result := 'RIGHT';
        miPositionBL : Result := 'Below Left';
        miPositionBC : Result := 'Below';
        miPositionBR : Result := 'Below RIGHT';
      end;

    end;


    function StrToMiPosition(aValue: string): Integer;
    begin
     (* case aValue of
        miPositionCC : Result := 'Center';
        miPositionTL : Result := 'Above RIGHT';
        miPositionTC : Result := 'Above Left';
        miPositionTR : Result := 'Above Left';
        miPositionCL : Result := 'LEFT';
        miPositionCR : Result := 'RIGHT';
        miPositionBL : Result := 'Below Left';
        miPositionBC : Result := 'Below';
        miPositionBR : Result := 'Below RIGHT';
      end;
*)
    end;



const
  CRLF = #13+#10;

var
  v: Variant;
 // vField : CMapXField;

  sKeyField: string;
  st: string;
  bAuto: Boolean;
  sPos: string;
  iColorBack: integer;
  iSize: integer;
  sFont: string;
  sName: string;
  sFilename: string;
  i: integer;
  S,sNames: string;
 // v: Variant;

 // vLayer: CMapxLayer;
   oTable: TworTable;


  oSList: TStringList;
  s1: string;
  sDir: string;
begin
  oSList:=TStringList.Create;


(*  oSList.Add('!Workspace');
  oSList.Add('!Version 600');
  oSList.Add('!Charset WindowsCyrillic');

*)
  s1:= '!Workspace' + CRLF +
      '!Version 600' + CRLF +
      '!Charset WindowsCyrillic';// + CRLF;


  oSList.Add(s1);

  sNames := '';


  sDir:=ExtractFilePath(aFileName);

  
  for i := 0 to Count-1 do
  begin
    oTable :=Items[i];

    //u_mapX_tools

//    sFilename:=aMap.Layers.Item[i].FileSpec;
    sFilename:=oTable.FileName;

    if UpperCase(LeftStr(sFilename, Length(sDir))) = UpperCase(sDir) then
       sFilename:=Copy(sFilename, Length(sDir)+1, 100 );
    

    
  //  st:=aMap.Layers.Item[i].KeyField;


    sName:=GetMapinfoShortFileName(sFilename) ;
    sName:=sName + Format('_%d',[i]);
    
  //  map1.Layers.Item[i].Name;

    s1:=Format('Open Table "%s" As %s Interactive', [sFilename,sName]);
    oSList.Add(s1);

    s:=s+ Format('Open Table "%s" As %s Interactive', [sFilename,sName]) + CRLF;

    sNames:=sNames+ sName + IIF(i<Count-1,',','');

  end;

  //Map From pmp_site,property,aqua,pmp_sector_ant
  s:=s+ Format('Map From %s', [sNames]) + CRLF ;

  s1:=Format('Map From %s', [sNames]);
  oSList.Add(s1);


  s:=s+ 'Set Map ' + CRLF;
  oSList.Add('Set Map ');

//  oSList.Add('  CoordSys Earth Projection 1, 1001 ');
  oSList.Add('  CoordSys Earth Projection 1, 104 ');

  s1:=Format('  Center (%1.8f,%1.8f)  ',  [CenterX, CenterY]);
  oSList.Add(s1);

 // s1:=Format('  Zoom %1.6f Units "km" Off ', [Zoom]);
 // oSList.Add(s1);

     // s+
  s1:= '  CoordSys Earth Projection 1, 1001 '+ CRLF +
       Format('  Center (%1.8f,%1.8f)  ',  [CenterX, CenterY]); //+ CRLF +
     //  Format('  Zoom %1.6f Units "km" Off', [Zoom]); // + CRLF ;

  oSList.Add(s1);

  s:=s+ 'Set Map '+ CRLF;

  oSList.Add('Set Map ');

  for i := 0 to Count-1   do
  begin
    oTable :=Items[i];


//    vLayer:=aMap.Layers.Item[i];

  //  vLayer.LabelProperties.Visible

    s:=s+ Format('  Layer %d      ', [i+1])+ CRLF;

    s1:=Format('  Layer %d      ', [i+1]);
    oSList.Add(s1);


    case oTable.Type_ of

        miLayerTypeRaster:
        begin
          s1:= '  Display Graphic Global ' + CRLF +
               '  Selectable Off   ' + CRLF ;
          s:=s+ s1;

          oSList.Add(s1);
        end;


        miLayerTypeNormal:
        begin
           // v := vLayer.LabelProperties.Visible;
           // sName:=AsString(v);
            sName     :=oTable.LabelProperties_Style.TextFontName;// OleVariant(vLayer.LabelProperties.Style.TextFont).Name;
            iSize     :=oTable.LabelProperties_Style.TextFontSize;// OleVariant(vLayer.LabelProperties.Style.TextFont).Size;
            iColorBack:=oTable.LabelProperties_Style.TextFontBackColor;//  vLayer.LabelProperties.Style.TextFontBackColor;
            sPos      :=MiPositionToStr(oTable.LabelProperties_Style.Position);

        //    v:=vLayer.LabelProperties.DataField;

            if Pos('cyr',sName)=0 then
              sName:=sName + ' cyr';


            sFont:= Format('("%s",0,%d,0,%d)',
                      [sName,
                       iSize,
                       MakeMapinfoColor(iColorBack)
                      ]);

(*
           if (vLayer.Datasets.Count = 0) then
             aMap.Datasets.Add (miDataSetLayer, vLayer, EmptyParam, EmptyParam,
                                EmptyParam, EmptyParam, EmptyParam, EmptyParam) ;

*)(*
            vField:=vLayer.LabelProperties.DataField as CMapXField;
            if Assigned(vField) then
              sKeyField:=vField.Name;*)

            bAuto:=oTable.LabelProperties_Style.IsAutoLabel;//  vLayer.AutoLabel;
            sKeyField:=oTable.LabelProperties_Style.KeyField;// vLayer.KeyField;

            s1:= Format('    Label Line Arrow Position %s Font %s Pen (1,2,0)', [sPos,sFont])+ CRLF +
       IIF(sKeyField<>'','    With '+ sKeyField+ CRLF,'') +
//                  Format('    With %s       ', [sName])+ CRLF +
                  Format('    Parallel On Auto %s ', [IIF(bAuto,'On','Off') ])+
                             'Overlap Off Duplicates On Offset 2'+ CRLF +
                         '    Visibility On '+ CRLF + 
                         '    Zoom (1.8, 740) Units "km" Off ';

                         //    Zoom (1.8, 740) Units "km" Off 


            s:=s+ s1;
            oSList.Add(s1);


{            Format('    Label Line Arrow Position %s Font %s Pen (1,2,0)', [sPos,sFont])+ CRLF +
       IIF(sKeyField<>'','    With '+ sKeyField+ CRLF,'') +
//                  Format('    With %s       ', [sName])+ CRLF +
                  Format('    Parallel On Auto %s ', [IIF(bAuto,'On','Off') ])+
                             'Overlap Off Duplicates On Offset 2'+ CRLF +
                         '    Visibility On ' + CRLF;
}

        end;
    //  end;
    end;
  end;

//  StrToTxtFile (s, aWorFileName);

  ForceDirByFileName(aFileName);

  oSList.SaveToFile (aFileName);
  FreeAndNil(oSList);

end;



end.





{



constructor TmtsProperty.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  Cells := TmtsCellList.Create();
end;

destructor TmtsProperty.Destroy;
begin
  FreeAndNil(Cells);
  inherited Destroy;
end;

procedure TmtsProperty.LoadFromDataset(aDataset: TDataset);
var
  iMaxH: INTeger;
  s: string;
begin
  with aDataset do
  begin

    BLPoint_Pulkovo.B:=FieldByName('������__����').AsFloat;
    BLPoint_Pulkovo.L:=FieldByName('�������_����').AsFloat;

    BLPoint_WGS:=geo_BL_to_BL(BLPoint_Pulkovo, EK_KRASOVSKY42, EK_WGS84);


    GROUND_HEIGHT:=   FieldByName('h_�����').AsInteger;
    ADDRESS:=         FieldByName('�����').AsString;

    H1_m:=FieldByName('h1_m').AsInteger;
    H2_m:=FieldByName('h2_m').AsInteger;
    H3_m:=FieldByName('h3_m').AsInteger;

    Name_:=Trim(FieldByName('���_�').AsString);

    s:=GetLastChar(Name);
    if (s='a') or (s='b') or (s='c') then
      Name_:=Copy(Name_, 1, Length(Name_)-1 );


   iMaxH:=Max( Max(h1_m, h2_m), h3_m);

 //  sName:=sName + Format(' (H: %d)', [iMaxH]);


   Standard:=FieldByName('��������').AsString;


   RegionName := FieldByName(DEF_REGION_).AsString;

   Comment:= Format('max H: %d', [iMaxH]);

  end;

 ////////// Cells.Clear;
(*
  if h1_m > 0 then LoadFromDataset(aDataset, Cells.AddItem, 1);
  if h2_m > 0 then LoadFromDataset(aDataset, Cells.AddItem, 2);
  if h3_m > 0 then LoadFromDataset(aDataset, Cells.AddItem, 3);
*)
end;





//-------------------------------------------------------------------
procedure TMifWorkspace.LoadFromMap(aMap: TMap);
//-------------------------------------------------------------------

 (*    //-------------------------------------------------------------------
     function GetMapinfoShortFileName(aFileName: string): string;
     //-------------------------------------------------------------------
     var
       s: string;
     begin
      aFileName:=ExtractFileNameNoExt(aFileName) ;
      aFileName:=ReplaceStr(aFileName,' ','_');
      aFileName:=ReplaceStr(aFileName,'.','_');
      aFileName:=ReplaceStr(aFileName,'-','_');
      aFileName:=ReplaceStr(aFileName,',','_');

      s:=Copy(aFileName, 1,1);
      if Copy(aFileName, 1,1)[1] in ['0'..'9'] then
        aFileName:='_'+ aFileName;

      Result := aFileName;
    end;
*)

  (*  function MiPositionToStr(aValue: Integer): string;
    begin
      case aValue of
        miPositionCC : Result := 'Center';
        miPositionTL : Result := 'Above RIGHT';
        miPositionTC : Result := 'Above Left';
        miPositionTR : Result := 'Above Left';
        miPositionCL : Result := 'LEFT';
        miPositionCR : Result := 'RIGHT';
        miPositionBL : Result := 'Below Left';
        miPositionBC : Result := 'Below';
        miPositionBR : Result := 'Below RIGHT';
      end;

    end;*)

//
//    function StrToMiPosition(aValue: string): Integer;
//    begin
//     (* case aValue of
//        miPositionCC : Result := 'Center';
//        miPositionTL : Result := 'Above RIGHT';
//        miPositionTC : Result := 'Above Left';
//        miPositionTR : Result := 'Above Left';
//        miPositionCL : Result := 'LEFT';
//        miPositionCR : Result := 'RIGHT';
//        miPositionBL : Result := 'Below Left';
//        miPositionBC : Result := 'Below';
//        miPositionBR : Result := 'Below RIGHT';
//      end;
//*)
//    end;
//

//
//const
//  CRLF = #13+#10;

var
  v: Variant;
  vField : CMapXField;

(*  sKeyField: string;
  st: string;
  bAuto: Boolean;
  sPos: string;
  iColorBack: integer;
  iSize: integer;
  sFont: string;
  sName: string;
  sFilename: string;*)
  i: integer;
 // S,sNames: string;
 // v: Variant;

  vLayer: CMapxLayer;

  oTable: TworTable;


//  oSList: TStringList;
  s1: string;
begin
//  oSList:=TStringList.Create;


(*  oSList.Add('!Workspace');
  oSList.Add('!Version 600');
  oSList.Add('!Charset WindowsCyrillic');

*)
(*
  s1:= '!Workspace' + CRLF +
      '!Version 600' + CRLF +
      '!Charset WindowsCyrillic';// + CRLF;
*)

 // oSList.Add(s1);

  CenterX:=aMap.CenterX;
  CenterY:=aMap.CenterY;
  Zoom:=aMap.Zoom;


//  sNames := '';

  for i := 1 to aMap.Layers.Count do
  begin
    oTable:=AddItem;


//    vLayer:=

    //u_mapX_tools
//    vLayer:=mapx_GetLayerByIndex_from_0(aMap,i);
    vLayer:=mapx_GetLayerByIndex_from_1(aMap,i);

//    sFilename:=aMap.Layers.Item[i].FileSpec;

    oTable.FileName:=vLayer.FileSpec;
//    oTable.Name:=GetMapinfoShortFileName(sFilename) ;


    oTable.LabelProperties_Style.TextFontName := OleVariant(vLayer.LabelProperties.Style.TextFont).Name;
    oTable.LabelProperties_Style.TextFontSize := OleVariant(vLayer.LabelProperties.Style.TextFont).Size;
    oTable.LabelProperties_Style.TextFontBackColor := vLayer.LabelProperties.Style.TextFontBackColor;
    oTable.LabelProperties_Style.Position := vLayer.LabelProperties.Position;

(*

     if (vLayer.Datasets.Count = 0) then
       aMap.Datasets.Add (miDataSetLayer, vLayer, EmptyParam, EmptyParam,
                          EmptyParam, EmptyParam, EmptyParam, EmptyParam) ;


    vField:=vLayer.LabelProperties.DataField as CMapXField;
    if Assigned(vField) then
      sKeyField:=vField.Name;*)


    oTable.LabelProperties_Style.IsAutoLabel:=vLayer.AutoLabel;
    oTable.LabelProperties_Style.KeyField:=vLayer.KeyField;


(*
        miLayerTypeNormal:
        begin
           // v := vLayer.LabelProperties.Visible;
           // sName:=AsString(v);
            sName     :=OleVariant(vLayer.LabelProperties.Style.TextFont).Name;
            iSize     :=OleVariant(vLayer.LabelProperties.Style.TextFont).Size;
            iColorBack:=vLayer.LabelProperties.Style.TextFontBackColor;
            sPos      :=MiPositionToStr(vLayer.LabelProperties.Position);

*)


  //  st:=aMap.Layers.Item[i].KeyField;


(*    sName:=GetMapinfoShortFileName(sFilename) ;
  //  map1.Layers.Item[i].Name;

    s1:=Format('Open Table "%s" As %s Interactive', [sFilename,sName]);
    oSList.Add(s1);

    s:=s+ Format('Open Table "%s" As %s Interactive', [sFilename,sName]) + CRLF;

    sNames:=sNames+ sName + IIF(i<aMap.Layers.Count-1,',','');
*)
  end;
(*
  //Map From pmp_site,property,aqua,pmp_sector_ant
  s:=s+ Format('Map From %s', [sNames]) + CRLF ;

  s1:=Format('Map From %s', [sNames]);
  oSList.Add(s1);


  s:=s+ 'Set Map ' + CRLF;
  oSList.Add('Set Map ');

  oSList.Add('  CoordSys Earth Projection 1, 1001 ');
*)

  oTable.Type_ := vLayer.Type_;


(*
  s1:=Format('  Center (%1.8f,%1.8f)  ',  [aMap.CenterX, aMap.CenterY]);
  oSList.Add(s1);

  s1:=Format('  Zoom %1.6f Units "km" ', [aMap.Zoom]);
  oSList.Add(s1);*)

(*     // s+
  s1:= '  CoordSys Earth Projection 1, 1001 '+ CRLF +
       Format('  Center (%1.8f,%1.8f)  ',  [aMap.CenterX, aMap.CenterY]) + CRLF +
       Format('  Zoom %1.6f Units "km" ', [aMap.Zoom]); // + CRLF ;

  oSList.Add(s1);

  s:=s+ 'Set Map '+ CRLF;

  oSList.Add('Set Map ');
*)

//
//  for i := 0 to aMap.Layers.Count-1   do
//  begin
//
////    vLayer:=aMap.Layers.Item[i];
//    vLayer:=mapx_GetLayerByIndex_from_0(aMap,i);
//
//  //  vLayer.LabelProperties.Visible
//
//    s:=s+ Format('  Layer %d      ', [i+1])+ CRLF;
//
//    s1:=Format('  Layer %d      ', [i+1]);
//    oSList.Add(s1);
//
//
//    case vLayer.Type_ of
//
//        miLayerTypeRaster:
//        begin
//          s1:= '  Display Graphic  ' + CRLF +
//               '  Selectable Off   ' + CRLF ;
//          s:=s+ s1;
//
//          oSList.Add(s1);
//        end;
//
//
//        miLayerTypeNormal:
//        begin
//           // v := vLayer.LabelProperties.Visible;
//           // sName:=AsString(v);
//            sName     :=OleVariant(vLayer.LabelProperties.Style.TextFont).Name;
//            iSize     :=OleVariant(vLayer.LabelProperties.Style.TextFont).Size;
//            iColorBack:=vLayer.LabelProperties.Style.TextFontBackColor;
//            sPos      :=MiPositionToStr(vLayer.LabelProperties.Position);
//
//        //    v:=vLayer.LabelProperties.DataField;
//
//            if Pos('cyr',sName)=0 then
//              sName:=sName + ' cyr';
//
//
//            sFont:= Format('("%s",0,%d,0,%d)',
//                      [sName,
//                       iSize,
//                       MakeMapinfoColor(iColorBack)
//                      ]);
//
//
//           if (vLayer.Datasets.Count = 0) then
//             aMap.Datasets.Add (miDataSetLayer, vLayer, EmptyParam, EmptyParam,
//                                EmptyParam, EmptyParam, EmptyParam, EmptyParam) ;
//
//
//            vField:=vLayer.LabelProperties.DataField as CMapXField;
//            if Assigned(vField) then
//              sKeyField:=vField.Name;
//
//            bAuto:=vLayer.AutoLabel;
//            sKeyField:=vLayer.KeyField;
//
//            s1:= Format('    Label Line Arrow Position %s Font %s Pen (1,2,0)', [sPos,sFont])+ CRLF +
//       IIF(sKeyField<>'','    With '+ sKeyField+ CRLF,'') +
////                  Format('    With %s       ', [sName])+ CRLF +
//                  Format('    Parallel On Auto %s ', [IIF(bAuto,'On','Off') ])+
//                             'Overlap Off Duplicates On Offset 2'+ CRLF +
//                         '    Visibility On '; // + CRLF;
//
//
//
//            s:=s+ s1;
//            oSList.Add(s1);
//
//
//{            Format('    Label Line Arrow Position %s Font %s Pen (1,2,0)', [sPos,sFont])+ CRLF +
//       IIF(sKeyField<>'','    With '+ sKeyField+ CRLF,'') +
////                  Format('    With %s       ', [sName])+ CRLF +
//                  Format('    Parallel On Auto %s ', [IIF(bAuto,'On','Off') ])+
//                             'Overlap Off Duplicates On Offset 2'+ CRLF +
//                         '    Visibility On ' + CRLF;
//}
//
//        end;
//    //  end;
//    end;
//  end;

//  StrToTxtFile (s, aWorFileName);
(*
  ForceDirByFileName(aWorFileName);

  oSList.SaveToFile (aWorFileName);
  FreeAndNil(oSList);*)

end;


