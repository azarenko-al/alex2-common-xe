object dlg_Mapinfo_Data: Tdlg_Mapinfo_Data
  Left = 298
  Top = 195
  Width = 867
  Height = 523
  Caption = 'Mapinfo Data'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 878
    Top = 129
    Width = 507
    Height = 348
    Align = alRight
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cx_Data: TcxGridDBTableView
      DataController.DataSource = ds_Data
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cx_Data
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 129
    Width = 289
    Height = 348
    Align = alLeft
    BevelOuter = bvNone
    Caption = 'Panel1'
    TabOrder = 1
    object cxGrid2: TcxGrid
      Left = 0
      Top = 248
      Width = 289
      Height = 100
      Align = alBottom
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      object cxFields: TcxGridDBTableView
        DataController.DataSource = ds_Fields
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        OptionsCustomize.ColumnFiltering = False
        OptionsData.Editing = False
        OptionsView.GroupByBox = False
        object cxGridDBColumn1: TcxGridDBColumn
          DataBinding.FieldName = 'name'
          Width = 188
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxFields
      end
    end
    object cxSplitter2: TcxSplitter
      Left = 0
      Top = 169
      Width = 289
      Height = 8
      HotZoneClassName = 'TcxSimpleStyle'
      AlignSplitter = salTop
      Control = cxVerticalGrid1
    end
    object cxVerticalGrid1: TcxVerticalGrid
      Left = 0
      Top = 0
      Width = 289
      Height = 169
      Align = alTop
      LookAndFeel.Kind = lfFlat
      OptionsView.RowHeaderWidth = 161
      TabOrder = 2
      object cxVerticalGrid1CategoryRow1: TcxCategoryRow
        Properties.Caption = #1042#1099#1089#1086#1090#1099' '#1076#1083#1103' '#1084#1072#1090#1088#1080#1094#1099
        object row_Abs_H: TcxEditorRow
          Properties.Caption = #1072#1073#1089#1086#1083' '#1074#1099#1089#1086#1090#1072
          Properties.EditPropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.EditProperties.KeyFieldNames = 'name'
          Properties.EditProperties.ListColumns = <
            item
              FieldName = 'name'
            end>
          Properties.EditProperties.ListOptions.ShowHeader = False
          Properties.EditProperties.ListSource = ds_Fields
          Properties.EditProperties.OnValidate = row_Abs_HEditPropertiesValidate
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
        end
        object row_Relative_H: TcxEditorRow
          Properties.Caption = #1086#1090#1085#1086#1089#1080#1090#1077#1083#1100#1085#1072#1103' '#1074#1099#1089#1086#1090#1072
          Properties.EditPropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.EditProperties.KeyFieldNames = 'name'
          Properties.EditProperties.ListColumns = <
            item
              FieldName = 'name'
            end>
          Properties.EditProperties.ListOptions.ShowHeader = False
          Properties.EditProperties.ListSource = ds_Fields
          Properties.EditProperties.OnValidate = row_Abs_HEditPropertiesValidate
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
        end
      end
      object cxVerticalGrid1CategoryRow2: TcxCategoryRow
        Properties.Caption = #1055#1086#1083#1103' '#1089' '#1082#1086#1076#1072#1084#1080' '#1086#1073#1098#1077#1082#1090#1086#1074
        object row_Code: TcxEditorRow
          Properties.Caption = #1050#1086#1076
          Properties.EditPropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.EditProperties.DropDownListStyle = lsFixedList
          Properties.EditProperties.KeyFieldNames = 'name'
          Properties.EditProperties.ListColumns = <
            item
              FieldName = 'name'
            end>
          Properties.EditProperties.ListOptions.ShowHeader = False
          Properties.EditProperties.ListSource = ds_Fields
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
        end
        object row_Descr: TcxEditorRow
          Properties.Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          Properties.EditPropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.EditProperties.KeyFieldNames = 'name'
          Properties.EditProperties.ListColumns = <
            item
              FieldName = 'name'
            end>
          Properties.EditProperties.ListOptions.ShowHeader = False
          Properties.EditProperties.ListSource = ds_Fields
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
        end
      end
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 289
    Top = 129
    Width = 8
    Height = 348
    HotZoneClassName = 'TcxSimpleStyle'
    Control = Panel1
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 477
    Width = 1385
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object Panel2: TPanel
    Left = 0
    Top = 26
    Width = 1385
    Height = 103
    Align = alTop
    Caption = 'Panel2'
    Color = clMedGray
    TabOrder = 8
    Visible = False
  end
  object mem_Data: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 640
    Top = 24
    object mem_Dataname: TStringField
      FieldName = 'name'
    end
  end
  object ds_Fields: TDataSource
    DataSet = mem_Fields
    Left = 720
    Top = 72
  end
  object mem_Fields: TdxMemData
    Active = True
    Indexes = <>
    SortOptions = []
    Left = 720
    Top = 24
    object mem_Fieldsname: TStringField
      FieldName = 'name'
    end
  end
  object ds_Data: TDataSource
    DataSet = mem_Data
    Left = 640
    Top = 72
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 552
    Top = 24
    object act_ColumnAutoWidth: TAction
      Caption = 'Auto Width'
      Checked = True
      OnExecute = act_ColumnAutoWidthExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Bars = <
      item
        Caption = 'Custom 1'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = -347
        FloatTop = 326
        FloatClientWidth = 94
        FloatClientHeight = 47
        IsMainMenu = True
        ItemLinks = <
          item
            Item = btn_ColumnAutoWidth
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarSubItem1
            Visible = True
          end>
        MultiLine = True
        Name = 'Custom 1'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 320
    Top = 24
    DockControlHeights = (
      0
      0
      26
      0)
    object btn_ColumnAutoWidth: TdxBarButton
      Action = act_ColumnAutoWidth
      Category = 0
      Hint = 'Auto Width'
      ButtonStyle = bsChecked
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1058#1072#1073#1083#1080#1094#1072
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButton1
          Visible = True
        end>
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    Options = [fpPosition]
    StoredProps.Strings = (
      'Panel1.Width')
    StoredValues = <>
    Left = 472
    Top = 24
  end
end
