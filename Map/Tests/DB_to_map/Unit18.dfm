object Form18: TForm18
  Left = 1260
  Top = 468
  Caption = 'Form18'
  ClientHeight = 428
  ClientWidth = 867
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 32
    Top = 192
    Width = 377
    Height = 217
    DataSource = ds_Objects
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Button1: TButton
    Left = 152
    Top = 136
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 3
    OnClick = Button1Click
  end
  object Map1: TMap
    Left = 432
    Top = 192
    Width = 264
    Height = 228
    ParentColor = False
    TabOrder = 6
    ControlData = {
      2CA10700491B000091170000010000000F0000FFFFFEFF0D470065006F004400
      69006300740069006F006E0061007200790000FFFEFF3745006D007000740079
      002000470065006F007300650074004E0061006D00650020007B003900410039
      00410043003200460034002D0038003300370035002D0034003400640031002D
      0042004300450042002D00340037003600410045003900380036004600310039
      0030007D00FFFEFF001E00FFFEFF000600010A000000000000FFFEFF00500001
      0100000001F4010000050000800C0000000014000000000002000E00E8030005
      00000100000000E9E7E2000000000000000001370000000000E9E7E200000000
      0000000352E30B918FCE119DE300AA004BB85101CC00009001DC7C0100054172
      69616C000352E30B918FCE119DE300AA004BB851010200009001A42C02000B4D
      61702053796D626F6C730000000000000001000100FFFFFF000200E9E7E20000
      0000000001000000010000FFFEFF3245006D0070007400790020005400690074
      006C00650020007B00300031004100390035003000340042002D004300450031
      0033002D0034003400310035002D0041003500410030002D0035003100440038
      00430032004600310035003200300034007D0000000000FFFFFF000100000000
      000000000000000000000000000000000000000352E30B918FCE119DE300AA00
      4BB85101CC00009001348C030005417269616C000352E30B918FCE119DE300AA
      004BB85101CC00009001348C030005417269616C000000000000000000000000
      000000000000000000000000000000000000000000000000009C944000000000
      00606C400100000100001801000001000000FFFFFFFF1C000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000020000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008076C000000000008056C0000000000080764000000000008056400000
      00009CCBC0171801000001000000FFFFFFFF1C00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000200
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000080
      76C000000000008056C000000000008076400000000000805640000000009CCB
      C017000000000000000000000000000000000000000000000000000000000000
      00000000000000000000}
  end
  object Button2: TButton
    Left = 688
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 7
    OnClick = Button2Click
  end
  object PopupMenu1: TPopupMenu
    Left = 48
    Top = 136
    object asdfasdf1: TMenuItem
      Caption = 'asdfasdf'
    end
    object asdfasd1: TMenuItem
      Caption = 'asdfasd'
      object wertwe1: TMenuItem
        Caption = 'wertwe'
      end
      object wertwer1: TMenuItem
        Caption = '-'
      end
      object wertwer2: TMenuItem
        Caption = 'wertwer'
      end
    end
    object asdfasd2: TMenuItem
      Caption = 'asdfasd'
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object wertwe2: TMenuItem
      Caption = 'wertwe'
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 48
    Top = 56
    DockControlHeights = (
      0
      0
      23
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 1006
      FloatTop = 433
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem2'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem4'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem3'
        end>
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link_old;Data Source=SERVER1'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 232
    Top = 56
  end
  object ds_Objects: TDataSource
    DataSet = ADOQuery1
    Left = 369
    Top = 112
  end
  object ADOQuery1: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select top 100   id, name , lat, lon   from property')
    Left = 368
    Top = 56
  end
  object FDQuery1: TFDQuery
    Left = 560
    Top = 56
  end
end
