unit u_MapX_func;

interface
uses SysUtils, Variants, Dialogs, StrUtils,

    MapXLib_TLB
    ;


type
  TMapViewAddRec = record
    LayerName    : string;
    FileName     : string;
//    ObjectName   : string;

    AutoLabel    : boolean;

    IsZoomAllowed: boolean;
    ZoomMin      : integer;
    ZoomMax      : integer;

//    CalcRegionID : integer;

    IsSelectable : boolean;
    IsTransparent: boolean;
    IsEditable   : boolean;

 //   IsFiltered   : boolean;

  //  LabelProperties_RegPath: string;

  //  DisplayIndex : Integer;
    DisplayIndex_from_1 : Integer;

    MapAlignment: (//malNone,
                   malTop, malBottom);//, malMiddle); /malAuto,

  end;


  function map_MapAdd(aMap: TMap; aRec: TMapViewAddRec): CMapXLayer; overload;
  function map_MapAdd(aMap: TMap; aFileName: string): CMapXLayer; overload;

  procedure mapx_Dlg_Ver(aMap: TMap);

  procedure mapx_SetLayerAutoLabel(aLayer: CMapXLayer; aValue: boolean);

function map_MapAdd_Ex(aMap: TMap; aFileName: string): CMapXLayer;

procedure map_MapAdd_test(aMap: TMap);

function map_MapAdd_Ex_CMapX(aMap: CMapX; aFileName: string): CMapXLayer;

procedure mapx_TilesClear(aMap: CMapX);

implementation


//--------------------------------------------------------------------
function map_MapAdd(aMap: TMap; aRec: TMapViewAddRec): CMapXLayer;
//--------------------------------------------------------------------
var
  vLayer: CMapXLayer;
  sRegPath: string;
    i: Integer;
begin
  Result := nil;

  aRec.FileName:= ChangeFileExt(aRec.FileName, '.tab');

  if not FileExists (aRec.FileName) then
  begin
  //  Log('file not found: '+ aRec.FileName);
    exit;
  end;

//    vLayer:=map1.OleObject.layers.add (aFileName);
  try
    case aRec.MapAlignment of
  //    malAuto:    vLayer:= aMap.Layers.Add (aRec.FileName, EmptyParam);
      malTop:     vLayer:= aMap.Layers.Add (aRec.FileName, 1); //0
//      malMiddle:  vLayer:= aMap.Layers.Add (aRec.FileName, 500);
    //  malBottom:  vLayer:= aMap.Layers.Add (aRec.FileName, aMap.Layers.Count); //1000
      malBottom:  vLayer:= aMap.Layers.Add (aRec.FileName, EmptyParam); //1000
    else
      vLayer:= aMap.Layers.Add (aRec.FileName, 1); //1000
    end;

  except
    ShowMessage('file not found: '+ aRec.FileName);
    Exit;
  end;

  if aRec.LayerName<>'' then
    vLayer.Name:=aRec.LayerName;

  if aRec.ZoomMax>0 then
  begin
    vLayer.ZoomLayer:=aRec.IsZoomAllowed;
    vLayer.ZoomMin:=aRec.ZoomMin;
    vLayer.ZoomMax:=aRec.ZoomMax;
  end;


  Assert(Assigned(vLayer), 'vLayer not assigned');


  case vLayer.Type_ of
    miLayerTypeRaster: begin
      vLayer.ZoomLayer:= False;
    end;

    miLayerTypeNormal: begin
      vLayer.AutoLabel := aRec.AutoLabel;
      vLayer.Selectable:= aRec.IsSelectable;
      vLayer.Editable  := aRec.IsEditable;


{      if aRec.ObjectName<>'' then
        sRegPath:=REGISTRY_MapObjectsLabels +  aRec.ObjectName
      else
        sRegPath:=REGISTRY_MapObjectsLabels + ExtractFileName(aRec.FileName);
}

  //    Assert(aRec.LabelProperties_RegPath<>'');

(*      if aRec.LabelProperties_RegPath<>'' then
        mapx_LabelProperties_LoadFromReg (vLayer.LabelProperties, aRec.LabelProperties_RegPath);
*)

//      dmMap_Desktop.ApplyLabelProperties (vLayer, aRec.FileName);

//      vLayer.Refresh;
    end;
  end;

  Result := vLayer;
end;
        

// ---------------------------------------------------------------
procedure mapx_TilesClear(aMap: CMapX);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  for i:=aMap.Layers.Count downto 1 do
  begin     
    s:=aMap.Layers.Item[i].FileSpec;    

    s:= LowerCase (ExtractFileName(s));
    
    if LeftStr(s,Length('wms_tile')) = 'wms_tile' then
      aMap.Layers.Remove(i);
                 
  end;               
end;



//--------------------------------------------------------------------
function map_MapAdd_Ex_CMapX(aMap: CMapX; aFileName: string): CMapXLayer;
//--------------------------------------------------------------------
var
  bTile: Boolean;
  vLayer: CMapXLayer;
  sRegPath: string;
    i: Integer;

    iTileInd: Integer;
    
  s: string;
  sFileName: string;
  sName: string;
begin
  Result := nil;

  sName:= ExtractFileName(aFileName);

  bTile:=pos('wms_tile',LowerCase(sName))=1;
                          
  iTileInd :=0;
//  iRasterInd :=0;  
                          
  
//  for i:=aMap.Layers.Count downto 1  do
  for i:=1 to aMap.Layers.Count do
  begin     
    vLayer:=aMap.Layers.Item[i];

    sFileName:=vLayer.FileSpec;    
                   
    case vLayer.Type_ of
        miLayerTypeRaster: ;                   
        miLayerTypeNormal: ;
    end;                       
    
    s:= LowerCase (ExtractFileName(sFileName));
    
    if LeftStr(s,4) = 'wms_tile' then 
      if iTileInd=0 then
        iTileInd:=i;

  end;


  Assert(FileExists(aFileName));
  
  // add TILE to end
  if bTile then
    vLayer:= aMap.Layers.Add (aFileName, aMap.Layers.Count+1)
    
  else begin      
  //  if bTile then
   //   vLayer:= aMap.Layers.Add (aFileName, 1)
   // else

    vLayer:= aMap.Layers.Add (aFileName, 1);
      

    if aMap.Layers.Count>1 then
      if not bTile then
        case vLayer.Type_ of
           miLayerTypeRaster: if iTileInd>0 then
                               aMap.Layers.Move(1, iTileInd) ;
       
//           miLayerTypeNormal:  aMap.Layers.Move(aMap.Layers.Count, 1) ;

        end;    


    
//    vLayer.
//    aMap.Layers.M
  end;
   

  Result := vLayer;
end;




//--------------------------------------------------------------------
function map_MapAdd_Ex(aMap: TMap; aFileName: string): CMapXLayer;
//--------------------------------------------------------------------

begin
  Result:=map_MapAdd_Ex_CMapX (aMap.DefaultInterface, aFileName);


  
end;



//--------------------------------------------------------------------
procedure map_MapAdd_test(aMap: TMap);
//--------------------------------------------------------------------
var
  bTile: Boolean;
  vLayer: CMapXLayer;
  sRegPath: string;
    i: Integer;

    iTileInd: Integer;
    
  s: string;
  sFileName: string;
  sName: string;
begin
  //Result := nil;
//
//  sName:= ExtractFileName(aFileName);
//
//  bTile:=pos('tile',LowerCase(sName))=1;
//                          
//  iTileInd :=0;
////  iRasterInd :=0;  
//                          
//  
////  for i:=aMap.Layers.Count downto 1  do
//  for i:=1 to aMap.Layers.Count do
//  begin     
//    vLayer:=aMap.Layers.Item[i];
//
//    sFileName:=vLayer.FileSpec;    
//                   
//    case vLayer.Type_ of
//        miLayerTypeRaster: ;                   
//        miLayerTypeNormal: ;
//    end;                       
//    
//    s:= LowerCase (ExtractFileName(sFileName));
//    
//    if LeftStr(s,4) = 'tile' then 
//      if iTileInd=0 then
//        iTileInd:=i;
//
//  end;


end;



procedure mapx_SetLayerAutoLabel(aLayer: CMapXLayer; aValue: boolean);
begin
  if aLayer.Type_ in [miLayerTypeNormal] then
    aLayer.AutoLabel := aValue;

//  aLayer.AutoLabel:=aValue;
end;


procedure mapx_Dlg_Ver(aMap: TMap);
begin
  ShowMessage(Format('������ MapX: %s', [aMap.Version]));
end;


function map_MapAdd(aMap: TMap; aFileName: string): CMapXLayer;
var
  rec: TMapViewAddRec;
begin
//  if Mapx_In then
  


  FillChar(rec, SizeOf(rec), 0);

  rec.FileName:=aFileName;
  rec.MapAlignment :=malTop;
//  rec.AutoLabel:=map_rec.AutoLabel;
//   rec.LabelProperties_RegPath:=REGISTRY_MapObjectsLabels + ExtractFileName(rec.FileName);

  Result := map_MapAdd(aMap, rec);
end;


end.
