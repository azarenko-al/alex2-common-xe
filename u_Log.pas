  unit u_Log;

interface

uses
  CodeSiteLogging;

  

procedure Log(aMsg: string);
procedure LogError(aMsg: string);


implementation


procedure Log(aMsg: string);
begin
  CodeSite.Send(aMsg);
end;


procedure LogError(aMsg: string);
begin
  CodeSite.SendError(aMsg);
end;




end.
