unit u_MapInfo_MIF_MID;

interface
uses SysUtils,Classes,dxmdaset,DB,

     u_Geo,
//     u_db,
     u_func,
     u_files,
     u_img
     ;

  //   u_FileFormats;


type
  TMapInfoRegion = class; //����������� ��������� � �����


  //---------------------------------------------------------
  TMapInfoFile = class
  //---------------------------------------------------------
  private
    FMifFile,FMidFile: TextFile;
    FDataStr    : string;

    FPen   : record width, pattern, color: integer; end;
    FBrush : record pattern, forecolor, backcolor: integer; end;

    procedure LoadHeaderFromFile (aFileName: string);
    procedure WritePen();
    procedure WriteBrush();
    procedure WriteDataStr(); //  (Value: string);
    procedure WriteHeader();

  public

//    TempRegion : TMapInfoRegion;

    Header : record
      Version   : string;
      CharSet   : string;
      Delimiter : string;
      FCoordSys  : string;
    end;

    constructor Create;

    function  Open (aFileName: string): boolean;
    procedure Close();
    procedure WriteSymbol   (b1,l1: double; aCode,aColor:integer);
    procedure WriteLine     (b1,l1, b2,l2: double);
    procedure WritePolyLine (aBLPoints: TBLPointArray);
    procedure WriteRect     (b1,l1, b2,l2: double);
    procedure WriteText     (b1,l1: double; aCaption,aAlign: string; aBackColor: integer);

    procedure WriteRegion (aRegion: TMapInfoRegion);
    procedure SetPen      (width, pattern, color: integer);
    procedure SetBrush    (pattern, forecolor, backcolor: integer);
    procedure SetDataStr  (Value: string);

    function LoadFromFile(aFileName: string; aDataStrIndex: Integer=0): boolean; 

//    property RowCount; //: integer read GetRowCount;
//    property Cells; // [aRow: integer; aColumnName: string]: Variant read GetCellValue;

  end;


  TMapInfoFeature = class //����������� ��������� � �����
  public

  end;

  TMapInfoFeatureList = class //����������� ��������� � �����
  public

  end;



  TMapInfoPolygon = class
    Points: TBLPointArray;
  end;

  //-----------------------------------------------------
  TMapInfoRegion = class(TList) //����������� ��������� � �����
  //-----------------------------------------------------
  private
    function  GetItem (Index: integer): TMapInfoPolygon;
    function  GetPolygonCount(): integer;
  public
    Color  : integer;
    DataStr: string;

    constructor Create (aColor: integer);
    procedure Clear; override;
    procedure AddPolygon (aBLPoints: TBLPointArray);

    property  Items    [Index:integer]: TMapInfoPolygon read GetItem; default;
    property  Polygons [Index:integer]: TMapInfoPolygon read GetItem;
    property  PolygonCount: integer read GetPolygonCount;
  end;

  //-----------------------------------------------------
  TMapInfoRegionList = class(TList) //����������� �������
  //-----------------------------------------------------
    function  GetItem (Index: integer): TMapInfoRegion;
    procedure Clear; override;
    property  Items [Index:integer]: TMapInfoRegion read GetItem;  default;
  end;


  function MakeMapInfoColor (aColor: integer): integer;




const
  //������� ���������
  MIF_CoordSys_KRASOVSKY = 'Earth Projection 1, 1001';

  MIF_SYMBOL_STAR = 58;

  MIF_BRUSH_PATTERN_BLANK = 1;
  MIF_BRUSH_PATTERN_SOLID = 2;
  MIF_BRUSH_PATTERN_DOTS  = 16;

  // pen pattern styles
  MIF_PEN_PATTERN_BLANK   = 1;
  MIF_PEN_PATTERN_SOLId   = 2;
  MIF_PEN_PATTERN_DASH    = 8;
  MIF_PEN_PATTERN_ARROW   = 59;


//=====================================================
implementation
//=====================================================
const
  // MapInfo records ----------------------------------
  MIF_HEADER = 'Version 300'               + CRLF +
               'Charset "WindowsCyrillic"' + CRLF +
               'Delimiter ":Delimiter"'    + CRLF +
               'CoordSys   :CoordSys';


//------------------------------------------------
constructor TMapInfoFile.Create;
//------------------------------------------------
begin
  inherited;
  Header.FCoordSys:=MIF_CoordSys_KRASOVSKY;
  Header.Delimiter:=';';

//  TempRegion:=TMapInfoRegion.Create(0);

end;

//------------------------------------------------
procedure TMapInfoFile.WriteDataStr(); // (Value: string);
//------------------------------------------------
begin
  Writeln (FMidFile, FDataStr);
end;

//------------------------------------------------
procedure TMapInfoFile.WriteHeader();
//------------------------------------------------
var i:integer; str:string;
begin

(*  str:=MIF_HEADER;
  str:=ReplaceStr (str, ':Delimiter', Header.Delimiter);
  str:=ReplaceStr (str, ':CoordSys',  Header.FCoordSys);

  Writeln (FMifFile, str);
  Writeln (FMifFile, Format('Columns %d',[Columns.Count]));

  for i:=0 to Columns.Count-1 do
  begin
    case Columns[i].Type_ of
      ctString  : str:='Char(50)';
      ctFloat   : str:='Float';
      ctInteger : str:='Smallint';
      ctSmallint: str:='Smallint';
      ctDecimal : str:=Format('Decimal (%d,0)', [Columns[i].Precision]);
    end;

    Writeln (FMifFile, Format('  %s %s', [Columns[i].Name,str]));
  end;

  Writeln (FMifFile, 'Data');
*)
end;

//------------------------------------------------
function TMapInfoFile.Open (aFileName: string): boolean;
//------------------------------------------------
begin
  ForceDirByFileName (aFileName);

  AssignFile (FMifFile, ChangeFileExt(aFileName,'.mif'));
  AssignFile (FMidFile, ChangeFileExt(aFileName,'.mid'));

  ReWrite (FMifFile);
  ReWrite (FMidFile);

  WriteHeader();
  Result:=true;
end;

procedure TMapInfoFile.Close();
begin
  CloseFile(FMifFile);
  CloseFile(FMidFile);
end;


procedure TMapInfoFile.WriteSymbol (b1,l1: double; aCode,aColor:integer);
begin
  Writeln (FMifFile, Format('Point %1.6f %1.6f' + CRLF + '    Symbol (%d,%d,10,"MapInfo Cartographic",256,0)',
     [l1,b1,aCode,MakeMapinfoColor(aColor)]));
end;


procedure TMapInfoFile.WriteLine (b1,l1, b2,l2: double);
begin
  Writeln (FMifFile, Format('Line %1.6f %1.6f %1.6f %1.6f', [l1,b1, l2,b2]));
  WritePen();
  WriteDataStr();
end;


procedure TMapInfoFile.WritePolyLine (aBLPoints: TBLPointArray);
var i,iCount: integer;
  str: string;
begin
  Writeln (FMifFile, 'PLINE');
  iCount:=High(aBLPoints)+1;
  Writeln (FMifFile, Format('  %d', [iCount]));
  for i:=0 to iCount-1 do begin
    str:=Format('%1.6f %1.6f', [aBLPoints[i].B, aBLPoints[i].L]);
    Writeln (FMifFile, Format('%1.6f %1.6f', [aBLPoints[i].L, aBLPoints[i].B]));
  end;
  WriteDataStr();
end;


procedure TMapInfoFile.WriteRect (b1,l1, b2,l2: double);
begin
  Writeln (FMifFile, Format('Rect %1.2f %1.2f %1.2f %1.2f', [l1,b1, l2,b2]));
  WritePen();
  WriteDataStr();
end;


procedure TMapInfoFile.WriteText (b1,l1: double; aCaption,aAlign: string; aBackColor: integer );
begin
  Writeln (FMifFile, Format('Text', []) );
  Writeln (FMifFile, Format('    "%s"', [aCaption]));
  Writeln (FMifFile, Format('    %1.6f %1.6f %1.6f %1.6f', [l1,b1,l1+0.01,b1+0.001]));
  Writeln (FMifFile, Format('    Justify %s',  [aAlign]));
  Writeln (FMifFile, Format('    Font ("Arial",0,10,0,%d)',[MakeMapinfoColor(aBackColor)]));

//  WritePen();
  WriteDataStr();

end;

procedure TMapInfoFile.SetDataStr (Value: string);
begin
  FDataStr:=Value;
end;

procedure TMapInfoFile.SetPen (width, pattern, color: integer);
begin
  FPen.width:=width;  FPen.pattern:=pattern;  FPen.color:=color;
end;

procedure TMapInfoFile.SetBrush (pattern, forecolor, backcolor: integer);
begin
  FBrush.pattern:=pattern;  FBrush.forecolor:=forecolor;  FBrush.backcolor:=backcolor;
end;

procedure TMapInfoFile.WritePen();
begin
  with FPen do
    Writeln (FMifFile, Format('    Pen (%d,%d,%d)', [width, pattern, MakeMapinfoColor(color)]));
end;

procedure TMapInfoFile.WriteBrush();
begin
  with FBrush do
  if pattern>1
    then Writeln (FMifFile, Format('    Brush (%d,%d)', [pattern, MakeMapinfoColor(forecolor), MakeMapinfoColor(backcolor)]))
    else Writeln (FMifFile, Format('    Brush (%d,%d,%d)', [pattern, MakeMapinfoColor(forecolor), MakeMapinfoColor(backcolor)]))

end;


procedure TMapInfoFile.WriteRegion (aRegion: TMapInfoRegion);
var i,j: integer;
begin
  Writeln (FMifFile, Format('Region %d', [aRegion.PolygonCount]));
  for i:=0 to aRegion.PolygonCount-1 do
   with aRegion.Polygons[i] do begin
      Writeln (FMifFile, Format('   %d', [High(Points)+1]));

      for j:=0 to High(Points) do
        Writeln (FMifFile, Format('%1.6f %1.6f', [Points[j].l, Points[j].b]));
   end;

  WritePen(); WriteBrush(); WriteDataStr();
end;


//-----------------------------------------------------
procedure TMapInfoFile.LoadHeaderFromFile (aFileName: string); // MIF
//-----------------------------------------------------
var list: TStringList;
    i,j: integer;
    strArr: TStrArray;
   // colType: TGridColumnType;
    colName: string;
    iColCount: integer;
begin

(*  MemData.Close;
  db_ClearFields (MemData);

  if not FileExists (aFileName) then
    Exit;

  list:=TStringList.Create;
  try
    list.LoadFromFile (aFileName);

    // parse header ------------------------
    i:=-1;
    while i < list.Count-1 do
    begin
      Inc(i);
      strArr:=StringToStrArray (list[i],' ');

      if High(strArr)>=1 then
      begin
        if Eq(strArr[0],'Version')   then Header.Version:=strArr[1] else
        if Eq(strArr[0],'Charset')   then Header.Charset:=strArr[1] else
        if Eq(strArr[0],'Delimiter') then Header.Delimiter:=ReplaceStr(strArr[1],'"','') else
        if Eq(strArr[0],'CoordSys')  then Header.FCoordSys:=strArr[1] else

        if Eq(strArr[0],'Columns')   then
        begin
          //SetLength(Columns, AsInteger(strArr[1]));
          iColCount:=AsInteger(strArr[1]);

          for j:=0 to iColCount-1 do
          begin
             Inc(i);
             strArr:=StringToStrArray (list[i],' ');

             colName:=strArr[0];

             if Eq(strArr[1],'Float')    then colType:=ctFloat else
             if Eq(strArr[1],'Integer')  then colType:=ctInteger else
             if Eq(strArr[1],'Smallint') then colType:=ctSmallint
                                         else colType:=ctString;
             Columns.AddItem (colName,colType);

             if Eq(strArr[1],'Float')    then db_CreateField (MemData, colName, ftFloat) else
             if Eq(strArr[1],'Integer')  then db_CreateField (MemData, colName, ftInteger) else
             if Eq(strArr[1],'Smallint') then db_CreateField (MemData, colName, ftSmallint)
                                         else db_CreateField (MemData, colName, ftString);

          end;
        end;

        if Eq(strArr[0],'Data') then
          Break;

      end else
        Break;
    end;

    Delimiter:=Header.Delimiter;
  finally
    list.Free;
  end;

  MemData.Open;
  *)
end;

//-------------------------------------------
// TMapInfoRegionGroup
//-------------------------------------------
constructor TMapInfoRegion.Create (aColor: integer);
begin
  inherited Create;
  Color:=aColor;
end;

function TMapInfoRegion.GetItem (Index: integer): TMapInfoPolygon;
begin
  Result:=TMapInfoPolygon(inherited Items[Index]);
end;

//procedure TMapInfoRegion.AddPolygon (aPointArr: TXYPointArray);
procedure TMapInfoRegion.AddPolygon (aBLPoints: TBLPointArray);
var obj: TMapInfoPolygon;
begin
  obj:=TMapInfoPolygon.Create;  Add(obj);
  obj.Points:=aBLPoints;
end;

procedure TMapInfoRegion.Clear;
var i:integer;
begin
  for i:=0 to Count-1 do Items[i].Destroy; inherited;
end;


function TMapInfoRegion.GetPolygonCount: integer;
begin
  Result:=Count;
end;

//-------------------------------------------
// TMapInfoRegionList
//-------------------------------------------
function TMapInfoRegionList.GetItem (Index: integer): TMapInfoRegion;
begin
  Result:=TMapInfoRegion(inherited Items[Index]);
end;

procedure TMapInfoRegionList.Clear;
var i:integer;
begin
  for i:=0 to Count-1 do Items[i].Destroy; inherited;
end;


function MakeMapinfoColor (aColor: integer): integer;
var r,g,b: byte;
begin

(*  ColorToRGB (aColor, r,g,b);
  Result:=(r*65536) + (g * 256) + b;
*)
end;


//-----------------------------------------------------
function TMapInfoFile.LoadFromFile(aFileName: string; aDataStrIndex: Integer=0): boolean;
//-----------------------------------------------------
begin

(*
Result:=False;

  if not FileExists (aFileName) then
    Exit;

  LoadHeaderFromFile (aFileName);
  LoadDataFile (ChangeFileExt(aFileName,'.mid'));
  Result:=True;
*)
end;


//var
//  o: TMapInfoFile;

//begin
{
  o:=TMapInfoFile.Create;
  o.LoadFromFile ('t:\!\exp\������ ���.mif');
  o.MemData.SaveToTextFile ('t:\!\exp\aassssssa.txt');
 }
end.

