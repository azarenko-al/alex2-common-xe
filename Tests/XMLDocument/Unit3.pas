unit Unit3;

interface

const
//  CRLF = #13+#10;
  CRLF = #10;


const
  MODEL_DET_TEMPLATE_XML =
   '<?xml version="1.0" encoding="windows-1251"?>'+ CRLF+
   //'<?xml:stylesheet type="text/xsl" ?>'+   //href=""
   '<Document>'+ CRLF+
   '<GROUP name="�������� ���������">'+   CRLF+

   '</GROUP>'+  CRLF+
   '</Document>';
implementation
   
end.


   '      <PARAM id="02" value="1" name="HefBS=" >'+CRLF+
   '        <ITEM value="1" name="HefBS=H_BS;"/>'+CRLF+
   '        <ITEM value="2" name="HefBS=(H_BS+H�����BS)-Hcp"/>'+CRLF+
   '      </PARAM> '+CRLF+

   '      <PARAM id="01" value="1" name="����������� ����������� ������ ������� ��"> '+CRLF+
   '        <ITEM value="1" name="����������� HefBS �� ������ ��������� ������� 1..10 �� �� BS"/> '+CRLF+
   '        <ITEM value="2" name="����������� HefBS � ����� �� ���������� �� ������ ����� ������"/> '+CRLF+
   '      </PARAM> '+CRLF+
   '      <PARAM id="11" value="2" name="��������� ������ ��� �� ��" comment="��� ��������� ������ ��� �� �������� ����������, �������������� ��������� ������ ��� � ����������� �� �������� �������� �� �� �� ��������� ������ �� ������� �., �� "> '+CRLF+
   '        <ITEM value="1" name="�������"/> '+CRLF+
   '        <ITEM value="2" name="�� ���()"/> '+CRLF+
   '      </PARAM> '+CRLF+
   '      <PARAM id="12" value="2" name="����������� ��������� ������ ��� �� ��"/> '+CRLF+
   '      <PARAM/> '+CRLF+
   '  </GROUP> '+CRLF+
   '  <GROUP name="���������� �������� ��������� � �� [dB]"> '+CRLF+
   '      <PARAM id="13_1" value="67" name="�����"/> '+CRLF+
   '      <PARAM id="13_2" value="27" name="� ����"/> '+CRLF+
   '      <PARAM id="13_3" value="37" name="� ������"/> '+CRLF+
   '      <PARAM id="13_4" value="27" name="� ��"/> '+CRLF+
   '  </GROUP> '+CRLF+
   '  <GROUP name="��� �������� ����������"> '+CRLF+
   '      <PARAM id="03" value="3" name="������ ��������� ��� ������� ������ W��=W��" > '+CRLF+
   '        <ITEM value="1" name="W�� �� ���� ��� �������� ���������" /> '+CRLF+
   '        <ITEM value="2" name="W��=(122+k0) + (20+k1)Lg(R[��]) - 20Lg(Dv[c�])" /> '+CRLF+
   '        <ITEM value="3" name="W��=W�� �� ����������� )" /> '+CRLF+
   '      </PARAM> '+CRLF+
   '      <GROUP name="���: W��=(122+k0) + (20+k1)Lg(R[��]) - 20Lg(Dv[c�])"> '+CRLF+
      '      <PARAM id="closed_k0" value="0" name="k0=-40...40" /> '+CRLF+
      '      <PARAM id="closed_k1" value="0" name="k1=-10...10" /> '+CRLF+
   '      </GROUP> '+CRLF+
   '      <PARAM id="14" value="2" name="��� �������� ���������� ��� ������� W�� �� ���� ��� �������� ��������� �������������� �������� Wp ���������������������� R" '+CRLF+
   '                              comment="��� �������� ���������� ��� ������� W�� �� ���� ��� �������� ��������� �������������� �������� Wp ���������������������� R"> '+CRLF+
   '        <ITEM value="1" name="������� 1/R"/> '+CRLF+
   '        <ITEM value="2" name="�� ���(1/R)"/> '+CRLF+
   '      </PARAM> '+CRLF+
   '      <PARAM id="15" value="2" name="����������� �������� Wp ���������������������� R" '+CRLF+
   '                               comment="��� �������� ���������� ��� ������� W�� �� ���� ��� �������� ��������� �������������� �������� Wp ���������������������� R:"/> '+CRLF+
   '  </GROUP> '+CRLF+
   '  <GROUP name="��� �������� ����������"> '+CRLF+
   '      <PARAM id="04" value="1" name="������ ��������� ��� ������� ������ W��=W��" > '+CRLF+
   '        <ITEM value="1" name="W�� �� ���� ��� �������� ���������"/> '+CRLF+
   '        <ITEM value="2" name="W��=(122+k0) + (20+k1)Lg(R[��]) - 20Lg(Dv[c�])"/> '+CRLF+
   '        <ITEM value="3" name="W��=W�� �� ����������� "/> '+CRLF+
   '      </PARAM> '+CRLF+
   '      <GROUP name="���: W��=(122+k0) + (20+k1)Lg(R[��]) - 20Lg(Dv[c�])"> '+CRLF+
      '      <PARAM id="open_k0" value="0" name="k0=-40...40" /> '+CRLF+
      '      <PARAM id="open_k1" value="0" name="k1=-10...10" /> '+CRLF+
   '      </GROUP> '+CRLF+

   '      <PARAM id="06"  value="69.55" name="������ ����������� ���� ��� ���������� ����������"/> '+CRLF+
   '      <PARAM id="07"  value="2"     name="����� ���� �������"/> '+CRLF+
   '      <PARAM id="05"  value="1" name="������ Wp"> '+CRLF+
   '        <ITEM value="1" name="������ Wp �� ���� �����������, �������� � ��"/> '+CRLF+
   '        <ITEM value="2" name="������ Wp �� ���������� ������� ����� ��"/> '+CRLF+
   '      </PARAM> '+CRLF+
   '  </GROUP> '+CRLF+
   '  <GROUP name="��� ���� ����������"> '+CRLF+
   '      <PARAM id="08"  value="2" name="������ ������ � ����"> '+CRLF+
   '        <ITEM value="1" name="��������� ������ ���� ��� ����"/> '+CRLF+
   '        <ITEM value="2" name="�� ��������� ������ ����, � ��������� �������� ��������� � ����"/> '+CRLF+
   '        <ITEM value="3" name="�� ��������� ������ ����, � ������ ������ � ���� �� ���������"/> '+CRLF+
   '      </PARAM> '+CRLF+
   '      <PARAM id="09"  value="1" name="������ ������ � ������"> '+CRLF+
   '        <ITEM value="1" name="��������� ������ ������ ��� ����"/> '+CRLF+
   '        <ITEM value="2" name="�� ��������� ������ ������, � ��������� �������� ��������� � ������"/> '+CRLF+
   '        <ITEM value="3" name="�� ��������� ������ ������, � ������ ������ � ������ �� ���������"/> '+CRLF+
   '      </PARAM> '+CRLF+
   '      <PARAM id="10" value="3" name="������ ������ � �� - ������� ���������� ����" > '+CRLF+
   '        <ITEM value="1" name="��������� ������ �� ��� ����"/> '+CRLF+
   '        <ITEM value="2" name="�� ��������� ������ ��, � ��������� �������� ��������� � ��"/> '+CRLF+
   '        <ITEM value="3" name="�� ��������� ������ ��, � ������ ������ � �� �� ���������"/> '+CRLF+
   '      </PARAM> '+CRLF+
   '  </GROUP> '+CRLF+
   '  <GROUP name="��� ������ ���������"> '+CRLF+
   '      <PARAM id="16" value="50"  name="������� ������ ����� � ������ [m]"/> '+CRLF+
   '      <PARAM id="19" value="50"  name="������� ������ ����� � �� [m]"/> '+CRLF+
   '      <PARAM id="22" value="25"  name="������� ���������� ����� ��������� � ���� [m]"/> '+CRLF+
   '      <GROUP name="�������� ����� [dB]"> '+CRLF+
   '          <PARAM id="18" value="23"  name="� ������=0...40"' +CRLF+
   '          comment="[0�40] �������� ����� ��� ������� ������ � ������ �� ������� ���������, ��. "/> '+CRLF+
   '          <PARAM id="24" value="0"   name="� ����=0...20" '+CRLF+
   '          comment="[0�20] �������� ����� ��� ������� ������ � ���� �� ������� ���������, ��. "/>'+CRLF+

   '          <PARAM id="21" value="5"   name="� ��=0...20"' +CRLF+
   '          comment="[0�20] �������� ����� ��� ������� ������ � �� �� ������� ���������, ��. "/> '+CRLF+
   '      </GROUP> '+CRLF+
   '      <GROUP name="����������, �� ������� ������������... [km]"> '+CRLF+
   '         <PARAM id="17" value="35" name="������� ������ ����� � ������ "  comment="0-������ ������ ����� ���������� �� ����� ��������� ���� � �����"/> '+CRLF+
   '         <PARAM id="20" value="1"  name="������� ������ � ��"            comment="0-������ ������ ����� ���������� �� ����� ��������� ���� � ��"/> '+CRLF+
   '         <PARAM id="23" value="35" name="������� ���������� ����� ��������� � ����"   comment="0- ������ ���������� ����� ��������� ���������� �� ����� ��������� ���� � ���"/> '+CRLF+
   '      </GROUP> '+CRLF+
   '  </GROUP> '+CRLF+
   '  <GROUP name="��� �����"> '+CRLF+
   '      <PARAM id="25"  value="8"  name="����� ����������� �������"  type="int" /> '+CRLF+
   '      <PARAM id="26"  value="2"  name="���������� ���������� ���������� �� �������, ����� ������� [dB]"/> '+CRLF+
   '      <PARAM id="Kwp" value="1"  name="�����.������������� ����������� ���������� (0..1)"/> '+CRLF+

   '      <PARAM id="27" value="0.2" name="������������������ ���������� [km]"/> '+CRLF+
   '      <PARAM id="28" value="0" name="����� ������� �������� ������"> '+CRLF+
   '        <ITEM value="0" name="������� �����"/> '+CRLF+
   '        <ITEM value="1" name="������������ �����"/> '+CRLF+
   '      </PARAM> '+CRLF+
   '      <PARAM id="281" value="45" name="���������� �������� W� [db] = 0...75"/> '+ CRLF+   //MaxWp
   '      <GROUP name="���������� r2 ��� �����"> '+CRLF+
   '          <PARAM id="29" value="0.05"  name="���� ������� � ���� [km]"/> '+CRLF+
   '          <PARAM id="30" value="0.025" name="���� ������� � ������ [km]"/> '+CRLF+
   '          <PARAM id="31" value="0.05"  name="���� ������� � �� [km]"/> '+CRLF+
   '      </GROUP>'+CRLF+

   '</GROUP>'+CRLF+
   '</Document>';


implementation

end.


