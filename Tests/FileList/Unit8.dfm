object Form8: TForm8
  Left = 564
  Top = 326
  Width = 623
  Height = 427
  Caption = 'Form8'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 48
    Top = 216
    Width = 173
    Height = 53
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object ed_Src: TDirectoryEdit
    Left = 36
    Top = 88
    Width = 329
    Height = 21
    DialogKind = dkWin32
    NumGlyphs = 1
    TabOrder = 1
    Text = 'u:\!\'
  end
  object ed_Dest: TDirectoryEdit
    Left = 28
    Top = 152
    Width = 329
    Height = 21
    DialogKind = dkWin32
    NumGlyphs = 1
    TabOrder = 2
    Text = 'u:\!\dest'
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredValues = <>
    Left = 396
    Top = 28
  end
end
