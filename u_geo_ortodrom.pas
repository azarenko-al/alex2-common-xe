﻿unit u_geo_ortodrom;

interface

uses
  SysUtils, Dialogs, Math;



procedure GetOrtodromPoint_LatLon(aLat1, aLon1, aAzimuth_deg, aDist_km: double;
    var aLat2, aLon2: double);





implementation


type
  TLatLon = record Lat,Lon : Double end;

  TLatLon1 = array[0..1] of Double;
  TXYZ1    = array[0..2] of Double;

  TXYZ    = record X,Y,Z : Double end;


//
///*
// * Преобразование сферических координат в вектор
// *
// * Аргументы исходные:
// *     y - {широта, долгота}
// *
// * Аргументы определяемые:
// *     x - вектор {x, y, z}
// */
//--------------------------------------------------------
procedure SpherToCart(y: TLatLon; var x: TXYZ);
//--------------------------------------------------------
var
  p : double;
begin
  p := cos(y.Lat);

  x.z:= sin(y.Lat);
  x.y:= P * sin(y.Lon);
  x.x:= P * cos(y.Lon);


end;
//
///*
// * Преобразование вектора в сферические координаты
// *
// * Аргументы исходные:
// *     x - {x, y, z}
// *
// * Аргументы определяемые:
// *     y - {широта, долгота}
// *
// * Возвращает:
// *     длину вектора
// */
//double CartToSpher(double x[], double y[])
{
  double p;

  p = hypot(x[0], x[1]);
  y[1] = atan2(x[1], x[0]);
  y[0] = atan2(x[2], p);

  return hypot(p, x[2]);
}


//--------------------------------------------------------
procedure CartToSpher(x: TXYZ; var y: TLatLon);
//--------------------------------------------------------
var
  p : double;
begin

  p := Hypot(x.X, x.Y);

  y.Lon:=ArcTan2(x.Y, x.X);
  y.Lat:=ArcTan2(x.Z, p);


end;



//
///*
// * Вращение вокруг координатной оси
// *
// * Аргументы:
// *     x - входной/выходной 3-вектор
// *     a - угол вращения
// *     i - номер координатной оси (0..2)
// */


//--------------------------------------------------------
procedure Rotate(var x: TXYZ;  a : double; i: integer );
//--------------------------------------------------------
var
  c, s, xj: double;
  j, k: integer;

  x_: array[0..2] of double;

begin
  x_[0]:=x.X;
  x_[1]:=x.Y;
  x_[2]:=x.Z;


  j := (i + 1) mod 3;
  k := (i - 1) mod 3;
  c := cos(a);
  s := sin(a);
  xj := x_[j] * c + x_[k] * s;
  x_[k] := -x_[j] * s + x_[k] * c;
  x_[j] := xj;

  x.X:=x_[0];
  x.Y:=x_[1];
  x.Z:=x_[2];


end;



//--------------------------------------------------------
//
///*
// * Решение прямой геодезической задачи
// *
// * Аргументы исходные:
// *     pt1  - {широта, долгота} //точки Q1
// *     azi  - азимут начального направления
// *     dist - расстояние (сферическое)
// *
// * Аргументы определяемые:
// *     pt2  - {широта, долгота} точки Q2
// */
//--------------------------------------------------------
procedure SphereDirect(pt1: TLatLon; azi, dist: double; var pt2: TLatLon);
//--------------------------------------------------------
var
  pt: TLatLon; //array[0..1] of double;
  x: TXYZ;


begin
//  double pt[2], x[3];

  pt.Lat := PI/2 - dist;
  pt.Lon := PI - azi;
  SpherToCart(pt, x);			// сферические -> декартовы
  Rotate(x, pt1.Lat - PI/2, 1);	// первое вращение
  Rotate(x, -pt1.Lon, 2);		// второе вращение
  CartToSpher(x, pt2);	     		// декартовы -> сферические

end;


// ---------------------------------------------------------------
procedure GetOrtodromPoint_LatLon(aLat1, aLon1, aAzimuth_deg, aDist_km: double;
    var aLat2, aLon2: double);
// ---------------------------------------------------------------
const
  A_EATRH  = 6371.0;
var

  pt1_,pt2_: TLatLon;

 // azi : double;
  dist: Double;
  s: string;

begin

  pt1_.Lat:=DegToRad(aLat1);
  pt1_.Lon:=DegToRad(aLon1);


  dist:=  aDist_km / ( 2*Pi*A_EATRH);
  dist:=  aDist_km / A_EATRH;

  SphereDirect (pt1_, DegToRad(aAzimuth_deg), dist, pt2_);


  aLat2:=RadToDeg(pt2_.Lat);
  aLon2:=RadToDeg(pt2_.Lon);


//  if aLat2>180 then
//    aLat2:=aLat2 - 90;


//  Assert(aLat2<90);


//  s:=Format('lat: %1.6f  lon: %1.6f   azi: %1.6f',[Lat1, Lon1, azi]);
//  s:=s+ '-----'+  Format('lat: %1.6f  lon: %1.6f',[Lat2, Lon2]);

//  ShowMessage( s );



end;




//--------------------------------------------------------
procedure Test;
//--------------------------------------------------------
const
  A_EATRH  = 6371.0;
var
  pt1,pt2: TLatLon;
  pt1_,pt2_: TLatLon;


  lat,lon : double;
  azi : double;
  dist: Double;
  s: string;

begin
  GetOrtodromPoint_LatLon (10,10, 0, 1000, lat,lon );


//  s:=Format('lat: %1.6f  lon: %1.6f   azi: %1.6f',[pt1.Lat, pt1.Lon, azi]);
  s:=s+ '-----'+  Format('lat: %1.6f  lon: %1.6f',[Lat, Lon]);

  ShowMessage( s );





Exit;

  pt1.Lat:=0;
  pt1.Lon:=0;

  pt1_.Lat:=DegToRad(pt1.Lat);
  pt1_.Lon:=DegToRad(pt1.Lon);


  azi:=-90;
  azi:=0;

  dist:= Pi/2;

  SphereDirect (pt1_, DegToRad(azi), Pi/2, pt2_);


  pt2.Lat:=RadToDeg(pt2_.Lat);
  pt2.Lon:=RadToDeg(pt2_.Lon);


  s:=Format('lat: %1.6f  lon: %1.6f   azi: %1.6f',[pt1.Lat, pt1.Lon, azi]);
  s:=s+ '-----'+  Format('lat: %1.6f  lon: %1.6f',[pt2.Lat, pt2.Lon]);

  ShowMessage( s );



end;


begin
// .. Test;


end.

{

void SpherToCart(double y[], double x[])
{
  double p;

  p = cos(y[0]);
  x[2] = sin(y[0]);
  x[1] = p * sin(y[1]);
  x[0] = p * cos(y[1]);

  return;
}






{
//
///*
// * Решение прямой геодезической задачи
// *
// * Аргументы исходные:
// *     pt1  - {широта, долгота} //точки Q1
// *     azi  - азимут начального направления
// *     dist - расстояние (сферическое)
// *
// * Аргументы определяемые:
// *     pt2  - {широта, долгота} точки Q2
// */
void SphereDirect(double pt1[], double azi, double dist, double pt2[])
{
  double pt[2], x[3];

  pt[0] = M_PI_2 - dist;
  pt[1] = M_PI - azi;
  SpherToCart(pt, x);			// сферические -> декартовы
  Rotate(x, pt1[0] - M_PI_2, 1);	// первое вращение
  Rotate(x, -pt1[1], 2);		// второе вращение
  CartToSpher(x, pt2);	     		// декартовы -> сферические

  return;
}

//
///*
// * Преобразование сферических координат в вектор
// *
// * Аргументы исходные:
// *     y - {широта, долгота}
// *
// * Аргументы определяемые:
// *     x - вектор {x, y, z}
// */
void SpherToCart(double y[], double x[])
{
  double p;

  p = cos(y[0]);
  x[2] = sin(y[0]);
  x[1] = p * sin(y[1]);
  x[0] = p * cos(y[1]);

  return;
}

