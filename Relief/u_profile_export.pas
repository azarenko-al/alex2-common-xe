unit u_profile_export;

interface
uses
  SysUtils, Classes, HTTPApp, Forms, StrUtils,

  u_JSON,

 // u_JSON,

  u_db,

  u_XML,

  u_rel_Profile;


type
  TProfile_export = class(TObject)
  protected
  public
    class function Export_JSON_str(aProfile: TrelProfile): string;
  end;

implementation

class function TProfile_export.Export_JSON_str(aProfile: TrelProfile): string;

var
  eDistKM: Double;
  i: Integer;
  oRootJS: TJSONobject;
  js: TJSONobject;
  oList: TJSONlist;

begin   

  oRootJS := TJSONobject.Create;

  oRootJS.Add('count', aProfile.Count);

  oList:=oRootJS.AddList('items');


  for i:=0 to aProfile.Count-1 do
  begin
    eDistKM:=aProfile.Data.Items[i].Distance_km;

    js:=oList.AddObjectEx(
         [ 'dist',       eDistKM, //TruncFloat(Data.Items[i].Distance,3) ),
       //   json_ATT ('dist_km',    eDistKM), //TruncFloat(Data.Items[i].Distance,3) ),
           'rel_h',      aProfile.Data.Items[i].Rel_H,
           'earth_h',    aProfile.Data.Items[i].earth_H,
           'local_h',    aProfile.Data.Items[i].Clutter_H,
           'local_code', aProfile.Data.Items[i].Clutter_Code
         ]);

    end;


  result := TJSON.GenerateText(oRootJS);

  FreeAndNil(oRootJS);

end;



end.
