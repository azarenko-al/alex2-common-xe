unit u_bee_Matrix;

interface
uses Math,Windows,SysUtils,Classes,IniFiles,Dialogs,

    I_rel_Matrix1,

//     I_rel_Matrix,
   u_rel_Matrix_base,

   u_func,
   u_files,
   u_VirtFile,
   u_geo_MIF,
   u_Geo_convert,
   u_Geo
   ;

type

  TBeeCluMatrixFile = class;


  //----------------------------------------
  TBeeMatrix = class(TrelMatrixBase)
  //----------------------------------------
  private
    FBeeCluMatrixFile: TBeeCluMatrixFile;


    function GetItem (aRow,aCol: integer): Trel_Record; override;
    function GetBLRectByMapName (aMapName: ShortString): TBLRect;
  protected

  public
//    BLBounds: TBLRect;

    constructor Create;
    destructor Destroy; override;

    function OpenHeader (aFileName: string): Boolean; override;
    function OpenFile (aFileName: string): boolean; override;

    function  FindPointBL  (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean; override;
    function  FindPointXY  (aXYPoint: TXYPoint; aZoneNum: integer; var aRec: Trel_Record): boolean; override;

    function  FindCellBL   (aBLPoint: TBLPoint; var aRow,aCol: integer): boolean; override;
    function  FindCellXY   (aXYPoint: TXYPoint; var aRow,aCol: integer): boolean; override;

//    procedure SaveHeaderToMIF (aFileName,aBitmapFileName: string); override;
//                               aImgRowCount,aImgColCount: integer); override;

    class function  GetInfo (aFileName: string; var aRec: TrelMatrixInfoRec): boolean; override;
  end;

  TBeeCluMatrixFile = class(TVirtualFile)
    constructor Create;
    function GetItem (aIndex:integer; var aValue: byte): boolean;
  end;



//===============================================================
implementation
//===============================================================

//---------------------------------------------------------------
constructor TBeeMatrix.Create;
//---------------------------------------------------------------
begin
  inherited;

  RowCount:=1853;
  ColCount:=1573;

  //scale = 200 !!!!!

  StepB:=geo_EncodeDegree (0, 40, 0) / RowCount;
  StepL:=geo_EncodeDegree (1, 0, 0) / ColCount;

  HeaderLen:=0;
  RecordLen:=2;

  FBeeCluMatrixFile:=TBeeCluMatrixFile.Create;


  MatrixType:=mtLonLat_;

end;


destructor TBeeMatrix.Destroy;
begin
  FBeeCluMatrixFile.Free;
  inherited;
end;



//---------------------------------------------------------
function TBeeMatrix.GetBLRectByMapName (aMapName: ShortString): TBLRect;
////aMainLetter: char; aMainNumber, aSmallNumber: integer): TBLRect;
//---------------------------------------------------------
var
  iRow, iCol: integer;
  bDelta,lDelta: double;
  iMainNumber, iSmallNumber: integer;
  cMainLetter: AnsiChar;
  blTopLeft: TBLPoint;
begin
  aMapName:= UpperCase(aMapName);

  cMainLetter:=  aMapName[1];
  iMainNumber:=  AsInteger(Copy(aMapName, 2, 2));
  iSmallNumber:= AsInteger(Copy(aMapName, 4, 2));

  blTopLeft.B:= (Integer(Ord(cMainLetter) - Ord('A')))*4 + 4;
  blTopLeft.L:= (iMainNumber - 31)*6;

  iCol:= ((iSmallNumber-1) mod 6);// - 1;
  iRow:= ((iSmallNumber-1) div 6);

  //scale = 200
{
  if (iSmallNumber mod 6) = 0 then
  begin
    iXindex:= 5;
    Dec(iYindex);
  end;
 }
  bDelta:=(geo_EncodeDegree(4,0,0)/ 6);
  lDelta:=(geo_EncodeDegree(6,0,0)/ 6);

  Result.TopLeft.B:= blTopLeft.B - iRow*bDelta;
  Result.TopLeft.L:= blTopLeft.L + iCol*lDelta;

//  Result.BottomRight.B:= Result.TopLeft.B + 1/6;
  Result.BottomRight.B:= Result.TopLeft.B - bDelta;
  Result.BottomRight.L:= Result.TopLeft.L + lDelta;

  if Pos('_2', aMapName)>0 then
    // ����� �� ������� �������
    geo_MoveBLRect (Result, 0, lDelta);

end;

// ---------------------------------------------------------------
function TBeeMatrix.OpenHeader(aFileName: string): boolean;
// ---------------------------------------------------------------
var
  sShortMapName: string;
begin
  sShortMapName:=Trim(ExtractFileNameNoExt (aFileName));

  BLBounds:=GetBLRectByMapName (sShortMapName);
end;

//---------------------------------------------------------------
function TBeeMatrix.OpenFile (aFileName: string): Boolean;
//---------------------------------------------------------------
var
  sExt,sShortMapName: string;
begin
  sShortMapName:=Trim(ExtractFileNameNoExt (aFileName));


  BLBounds:=GetBLRectByMapName (sShortMapName);


  Result := inherited OpenFile(aFileName);

  {
  FVirtualFile.FileName:=Trim(aFileName);

//
//  if Active then
//  begin
//    Result:=true; Exit;
//  end;

  Result:=FVirtualFile.Open (aFileName);

 }
  if not Result then
    Exit;


  sExt := ChangeFileExt (aFileName, '.clt');
  
  if not FBeeCluMatrixFile.Open (sExt) then
    ShowMessage('File not found: '+ sExt);


   //!!!!!!!!!!!!!!!!!!!!!  :\C
 ///  SaveToDebugFiles('s:\111111111');

end;

//--------------------------------------------------------------
// STUFF
//--------------------------------------------------------------


//--------------------------------------------------------------
class function TBeeMatrix.GetInfo (aFileName: string; var aRec: TrelMatrixInfoRec): boolean;
//--------------------------------------------------------------
var obj: TBeeMatrix;
begin
  obj:=TBeeMatrix.Create;

  if obj.OpenFile(aFileName) then  //LoadHeaderFromFile(aFileName) then
  begin
    FillChar (aRec, SizeOf(aRec), 0);

    aRec.FileSize:=obj.FileSize;
    aRec.RowCount:=obj.RowCount;
    aRec.ColCount:=obj.ColCount;

    aRec.StepB:=obj.StepB;
    aRec.StepL:=obj.StepL;

    aRec.BLBounds:=obj.BLBounds;

{    aRec.GroundMinH:=obj.GroundMinH;
    aRec.GroundMaxH:=obj.GroundMaxH;
}
    Result:=True;
  end else
    Result:=False;

  obj.Free;
end;


function TBeeMatrix.FindCellXY(aXYPoint: TXYPoint; var aRow,aCol: integer):  boolean;
var blPoint: TBLPoint;
begin
  blPoint:=geo_XY_to_BL (aXYPoint);
  Result:=FindCellBL (blPoint, aRow,aCol);
end;


function TBeeMatrix.FindPointXY (aXYPoint: TXYPoint; aZoneNum: integer; var aRec: Trel_Record): boolean;
var blPoint: TBLPoint;
begin
  blPoint:=geo_XY_to_BL (aXYPoint);
  Result:=FindPointBL (blPoint, aRec);
end;



//---------------------------------------------------------------
function TBeeMatrix.FindCellBL(aBLPoint: TBLPoint; var aRow,aCol: integer):
    boolean;
//--------------------------------------------------------------
var r: integer;
begin

  if  (Active) and
      ((BLBounds.TopLeft.B > aBLPoint.B) and (aBLPoint.B > BLBounds.BottomRight.B) and
       (BLBounds.TopLeft.L < aBLPoint.L) and (aBLPoint.L < BLBounds.BottomRight.L)) then
  begin
    aRow := Trunc((BLBounds.TopLeft.B - aBLPoint.B) / StepB);
   // Row := RowCount -1 - r;
    aCol := Trunc((aBLPoint.L - BLBounds.TopLeft.L) / StepL);

    Result:=(aRow>=0) and (aCol>=0) and (aRow<=RowCount-1) and (aCol<=ColCount-1);
  end else
    Result:=false;

end;


//---------------------------------------------------------------
function TBeeMatrix.FindPointBL (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean;
//--------------------------------------------------------------
var iRow,iCol: integer;
begin
  Result:=FindCellBL (aBLPoint, iRow,iCol);
  if Result then begin
    aRec:=Items[iRow,iCol];

    if aRec.rel_h = EMPTY_HEIGHT then
      Result:=False;
  end;
end;
  

// -------------------------------------------------------------------
constructor TBeeCluMatrixFile.Create;
// -------------------------------------------------------------------
begin
  inherited;

  HeaderLen1:=0;
  RecordLen1:=1;
end;


function TBeeCluMatrixFile.GetItem (aIndex: integer; var aValue: byte): boolean;
begin
  Result:=ReadRecord (aIndex, @aValue);
end;


// -------------------------------------------------------------------
function TBeeMatrix.GetItem(aRow, aCol: integer): Trel_Record;
// -------------------------------------------------------------------

(*CLUTTERTYPE	CLUTTERNAME	ALTERNAME
0	�������� ���������	Open
1	����	Water
2	���	Forest
3	��������	SubUrban
4	������	Road
5	�������� ������	RailRoad
6	������� �����	HighUrban
7	������������ ���������	Industry
8	����������	Bush
9	�����	Urban
18	������	Bush_Water
*)

const
  BEE_OPEN_AREA = 0;  //0	�������� ���������	Open
  BEE_WATER     = 1;  //1	����	Water
  BEE_FOREST    = 2;  //2	���	Forest
  BEE_SubUrban  = 3;  //3	��������	SubUrban
  BEE_ROAD      = 4;  //4	������	Road
  BEE_RAILROAD  = 5;  //5	�������� ������	RailRoad
  BEE_HighUrban = 6;  //6	������� �����	HighUrban
  BEE_Industry  = 7;  //7	������������ ���������	Industry
  BEE_Bush      = 8;  //8	����������	Bush
  BEE_Urban     = 9;  //9	�����	Urban
  BEE_Bush_Water= 18; //18	������	Bush_Water

  ARR: array[0..10] of record src,dest : Integer; end =
  (
   (src: BEE_OPEN_AREA  ;  dest: DEF_CLU_OPEN_AREA),
   (src: BEE_WATER      ;  dest: DEF_CLU_WATER),
   (src: BEE_FOREST     ;  dest: DEF_CLU_FOREST),
   (src: BEE_SubUrban   ;  dest: DEF_CLU_COUNTRY),
   (src: BEE_ROAD       ;  dest: DEF_CLU_ROAD),
   (src: BEE_RAILROAD   ;  dest: DEF_CLU_RailROAD),
   (src: BEE_HighUrban  ;  dest: DEF_CLU_CITY),
   (src: BEE_Industry   ;  dest: DEF_CLU_COUNTRY),
   (src: BEE_Bush       ;  dest: DEF_CLU_BOLOTO), //DEF_CLU_OPEN_AREA),
   (src: BEE_Urban      ;  dest: DEF_CLU_CITY),
   (src: BEE_Bush_Water ;  dest: DEF_CLU_BOLOTO)
  );



(*const

  DEF_CLU_COUNTRY   = 3;  // 3- ���
  DEF_CLU_ROAD      = 4;
  DEF_CLU_CITY      = 7;  // 7- �����
  DEF_CLU_BOLOTO    = 8; // 31- ���
  DEF_CLU_ONE_BUILD = 73; // 73- ���.����
  DEF_CLU_ONE_HOUSE = 31; // 31- ���
*)


var
  I: Integer;
  iClu: byte;
  iH: integer;
begin
  Result:=inherited GetItem(aRow, aCol);
                     
  i:=aRow*ColCount+ aCol;

  Result.Clutter_H:=0;
  Result.Clutter_Code:=DEF_CLU_OPEN_AREA;

//  iClu := FBeeCluMatrixFile.GetItem(i, iClu) then

  if not FBeeCluMatrixFile.GetItem(i, iClu) then
    Exit;

(*
  Result.Clutter_Code:=iClu;

  Exit; //!!!!!!!!!!!!!!!!!!!!!!!!!!
*)

  for I := 0 to High(ARR) do
    if iClu=ARR[i].src then
      Result.Clutter_Code:=ARR[i].Dest;

end;


var
  o: TBeeMatrix;
{ TBeeCluMatrixFile }


begin

(*  o:=TBeeMatrix.Create;
  o.OpenFile ('S:\33\N3604     .Ter');
*)

end.




  {
//------------------------------------------------------
procedure TBeeMatrix.SaveHeaderToMIF (aFileName,aBitmapFileName: string);
                      //                 aImgRowCount,aImgColCount: integer);
//------------------------------------------------------
var rec: TMifHeaderInfoRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.FileName      :=ChangeFileExt (aFileName, '.tab');
  rec.BitmapFileName:=aBitmapFileName;
  rec.BL.BLBounds   :=BLBounds;
//  rec.ImgRowCount   :=aImgRowCount;
//  rec.ImgColCount   :=aImgColCount;
  rec.ImgRowCount   :=RowCount;
  rec.ImgColCount   :=ColCount;
  rec.BL.StepB      :=StepB;
  rec.BL.StepL      :=StepL;

  geo_SaveBLHeaderForMIF (rec);
end;
 }

