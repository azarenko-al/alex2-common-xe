unit u_RLF_to_ENVI_export;

interface

uses Graphics, SysUtils, IOUtils,    iniFiles, Windows,  Classes, Forms,  Dialogs,

//  u_GDal_bat,
  u_GDal_classes,
  u_rel_engine,

//  u_run,

//  u_rel_engine

//  u_geo

//  u_Progress,

  I_rel_Matrix1,

  u_rel_matrix_base,
  u_rlf_Matrix;


type
  TRLF_to_ENVI_export = class(TObject)
    class procedure SaveCluttersToEnvi_ByFile(aFileName_rel, aFileName_clu: string);
    class procedure SaveCluttersToEnvi(aRelMatrix: TrelMatrixBase; aFileName_clu:  string);

    class procedure SaveToEnvi(aRelMatrix: TRlfMatrix; aDir: string);
    class procedure SaveToHDR(aRelMatrix: TRlfMatrix; aFileName_clu: string);

  end;

implementation


// ---------------------------------------------------------------
class procedure TRLF_to_ENVI_export.SaveCluttersToEnvi_ByFile(aFileName_rel,
    aFileName_clu: string);
// ---------------------------------------------------------------
var
  oRelMatrix: TrelMatrixBase;
begin
  oRelMatrix:=rel_CreateRelMatrixByFileName (aFileName_rel);
  oRelMatrix.OpenFile(aFileName_rel);

  TRLF_to_ENVI_export.SaveCluttersToEnvi(oRelMatrix, aFileName_clu);

  FreeAndNil(oRelMatrix);
end;

// ---------------------------------------------------------------
class procedure TRLF_to_ENVI_export.SaveCluttersToEnvi(aRelMatrix: TrelMatrixBase; aFileName_clu: string);
// ---------------------------------------------------------------
var
  bool: Boolean;
  iRow: integer;
  iCol: integer;
  rRel: Trel_Record;

  oFileStream: TFileStream;
  bt_value:  byte;
  hdr: TGDAL_Envi_HDR;
  oMemStream_clu: TMemoryStream;
  sFile: string;
begin


//  oFileStream:=TFileStream.create(ChangeFileExt(aFileName, '.envi'), fmCreate);
  oMemStream_clu:=TMemoryStream.create;
  oMemStream_clu.SetSize(aRelMatrix.RowCount * aRelMatrix.ColCount);


  for iRow := 0 to aRelMatrix.RowCount - 1 do
//    if not Terminated then
  begin
//    if iRow mod 200 = 0 then
//     DoProgress (iRow, FRelMatrix.RowCount);


    for iCol := 0 to aRelMatrix.ColCount - 1 do
    begin
        //   result:= FRelMatrix.GetItemByRowCol (ARow, aCol, aRec);

      bool:= aRelMatrix.GetItemByRowCol(iRow, iCol, rRel);
//      if (not b) then

      bt_value:=255;

      if bool then
        if (rRel.Rel_H <> EMPTY_HEIGHT) then
          bt_value:=rRel.Clutter_Code;


      oMemStream_clu.Write(bt_value, 1);
//      oMemStream.Write(@bt_value, 1);

    end;


  end;

//  oFileStream.CopyFrom(oMemStream, 0);

 // FreeAndNil(oFileStream);

  oMemStream_clu.SaveToFile(aFileName_clu);
  FreeAndNil(oMemStream_clu);

  // ---------------------------------------------------------------

//  FillChar (hdr, SIZEOF(hdr), 0);
  hdr.Clear;

  hdr.ColCount:=aRelMatrix.ColCount;
  hdr.RowCount:=aRelMatrix.RowCount;
  hdr.CellSize:=aRelMatrix.StepX;
  hdr.DataType:=1;
//  hdr.DataType:=2;

  hdr.Lon_min :=aRelMatrix.XYBounds.TopLeft.Y;
  hdr.Lat_max :=aRelMatrix.XYBounds.TopLeft.X;

  hdr.Zone_GK :=aRelMatrix.ZoneNum_GK;

  hdr.SaveToFile(ChangeFileExt(aFileName_clu, '.hdr'));
//  hdr.SaveToFile_HDR_Pulkovo(ChangeFileExt(sFile, '.hdr'));

end;

// ---------------------------------------------------------------
class procedure TRLF_to_ENVI_export.SaveToEnvi(aRelMatrix: TRlfMatrix; aDir:
    string);
// ---------------------------------------------------------------
var
  bool: Boolean;
  iRow: integer;
  iCol: integer;
  rRel: Trel_Record;

  oFileStream: TFileStream;
  bt_value:  byte;
  hdr: TGDAL_Envi_HDR;
  k: Integer;
  oFileStream_clu: TMemoryStream;
  oFileStream_rel: TFileStream;
  oMemStream: TMemoryStream;
  oMemStream_clu: TMemoryStream;
  oMemStream_clu_h: TMemoryStream;
  oMemStream_rel: TMemoryStream;
  sFile: string;
  sFileName: string;
begin

  sFileName:= IncludeTrailingBackslash(aDir) +ExtractFileName(aRelMatrix.FileName);

//  oFileStream_rel:=TFileStream.create(ChangeFileExt(aDir, '_REL.envi'), fmCreate);
  oMemStream_rel :=TMemoryStream.create;
  oMemStream_clu   :=TMemoryStream.create;
  oMemStream_clu_h :=TMemoryStream.create;


  oMemStream_rel.SetSize(aRelMatrix.RowCount * aRelMatrix.ColCount *2);
  oMemStream_clu.SetSize(aRelMatrix.RowCount * aRelMatrix.ColCount);
  oMemStream_clu_h.SetSize(aRelMatrix.RowCount * aRelMatrix.ColCount);


  k:=oMemStream_rel.Position;


  for iRow := 0 to aRelMatrix.RowCount - 1 do
//    if not Terminated then
  begin
//    if iRow mod 200 = 0 then
//     DoProgress (iRow, FRelMatrix.RowCount);


    for iCol := 0 to aRelMatrix.ColCount - 1 do
    begin
        //   result:= FRelMatrix.GetItemByRowCol (ARow, aCol, aRec);

      bool:= aRelMatrix.GetItemByRowCol(iRow, iCol, rRel);
//      if (not b) then


      if (rRel.Rel_H <> EMPTY_HEIGHT) then
      begin
        oMemStream_rel.Write(rRel.Rel_H, 2);
        oMemStream_clu.Write(rRel.Clutter_Code, 1);
        oMemStream_clu_h.Write(rRel.Clutter_h, 1);
      end else
      begin
        oMemStream_rel.Position:=oMemStream_rel.Position+2;
        oMemStream_clu.Position:=oMemStream_clu.Position+1;
        oMemStream_clu_h.Position:=oMemStream_clu_h.Position+1;
      end;

//      oMemStream.Write(@bt_value, 1);

    end;


  end;



  oMemStream_rel.SaveToFile(ChangeFileExt(sFile, '_REL.envi'));
  oMemStream_clu.SaveToFile(ChangeFileExt(sFile, '_clu.envi'));
  oMemStream_clu_h.SaveToFile(ChangeFileExt(sFile, '_clu_h.envi'));

  FreeAndNil(oMemStream_rel);
  FreeAndNil(oMemStream_clu);
  FreeAndNil(oMemStream_clu_h);


  // ---------------------------------------------------------------

//  FillChar (hdr, SIZEOF(hdr), 0);
  hdr.Clear;

  hdr.ColCount:=aRelMatrix.ColCount;
  hdr.RowCount:=aRelMatrix.RowCount;
  hdr.CellSize:=aRelMatrix.StepX;
  hdr.DataType:=2;
//  hdr.DataType:=2;

  hdr.Lon_min :=aRelMatrix.XYBounds.TopLeft.Y;
  hdr.Lat_max :=aRelMatrix.XYBounds.TopLeft.X;
  hdr.Zone_GK :=aRelMatrix.ZoneNum_GK;

  hdr.SaveToFile( ChangeFileExt(sFile, '_REL.hdr'));

  hdr.DataType:=1;
  hdr.SaveToFile( ChangeFileExt(sFile, '_clu.hdr'));
  hdr.SaveToFile( ChangeFileExt(sFile, '_clu_h.hdr'));



//  hdr.SaveToFile_HDR_Pulkovo(ChangeFileExt(sFile, '.hdr'));

end;

//-------------------------------------------------------------------
class procedure TRLF_to_ENVI_export.SaveToHDR(aRelMatrix: TRlfMatrix; aFileName_clu: string);
//-------------------------------------------------------------------
var
  hdr: TGDAL_Envi_HDR;

begin
 //  FillChar (hdr, SIZEOF(hdr), 0);
  hdr.Clear;

  hdr.ColCount:=aRelMatrix.ColCount;
  hdr.RowCount:=aRelMatrix.RowCount;
  hdr.CellSize:=aRelMatrix.StepX;
  hdr.DataType:=1;
//  hdr.DataType:=2;

  hdr.Lon_min :=aRelMatrix.XYBounds.TopLeft.Y;
  hdr.Lat_max :=aRelMatrix.XYBounds.TopLeft.X;
  hdr.Zone_GK :=aRelMatrix.ZoneNum_GK;

  hdr.SaveToFile( ChangeFileExt(aFileName_clu, '.hdr'));


end;


end.
