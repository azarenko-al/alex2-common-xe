unit u_rel_NASA;

interface
uses  SysUtils, Classes,

  I_rel_Matrix1,

  u_rel_Matrix_base,

  u_func,
//  u_geo_MIF,

  u_Geo_convert_new,
  u_Geo_convert,
  u_GEO;

type

  //----------------------------------------------------
  TrelNASA = class(TrelMatrixBase)
  //----------------------------------------------------
  private
 //   FBLBounds : TBLRect; //���������� ������ ������� ����

  protected
     function GetItem (aRow,aCol: integer): Trel_Record; override;

  public
    constructor Create;

    function OpenHeader(aFileName: string): Boolean;override;
    function OpenFile (aFileName: string): boolean; override;

    function  FindPointBL  (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean; override;
    function  FindPointXY  (aXYPoint: TXYPoint; aZoneNum: integer; var aRec: Trel_Record): boolean; override;

//    function  FindCellBL   (aBLPoint: TBLPoint; var Row,Col: integer): boolean; override;
    function  FindCellXY   (aXYPoint: TXYPoint; var Row,Col: integer): boolean; override;


//    procedure SaveHeaderToMIF(aFileName,aBitmapFileName: string); override;
                          //     aImgRowCount,aImgColCount: integer); override;

    class function GetInfoStr (aFileName: string): string; //override;

    class function GetInfo (aFileName: string; var aRec: TrelMatrixInfoRec): boolean; override;

   end;




//====================================================================
implementation
//====================================================================

function NASA_FileName_to_BLBounds(aFileName: string): TBLRect; forward;



// -------------------------------------------------------------------
constructor TrelNASA.Create;
// -------------------------------------------------------------------
begin
  inherited;

  HeaderLen:=0;
  RecordLen:=2;

  RowCount:=1201;
  ColCount:=1201;

 ///// FileSize:=RowCount * ColCount * RecordLen;

  StepB:=0.000833333333333;
  StepL:=0.000833333333333;

  Width :=geo_EncodeDegree (1, 0, 0);
  Height:=geo_EncodeDegree (1, 0, 0);

  StepB:=geo_EncodeDegree (1, 0, 0) / RowCount;
  StepL:=geo_EncodeDegree (1, 0, 0) / ColCount;

  MatrixType:=mtLonLat_;
end;


// ---------------------------------------------------------------
function TrelNASA.OpenHeader(aFileName: string): boolean;
// ---------------------------------------------------------------
begin
  BLBounds := NASA_FileName_to_BLBounds(aFileName);
end;

//---------------------------------------------------------------
function TrelNASA.OpenFile (aFileName: string): Boolean;
//---------------------------------------------------------------
(*var
  n,e: integer;
  S: string;
  *)
begin
  Result := inherited OpenFile(aFileName);
                                         
//  Result:= FVirtualFile.Open (aFileName);

  BLBounds := NASA_FileName_to_BLBounds(aFileName);

end;


//-----------------------------------------------------------
function TrelNASA.GetItem (aRow,aCol: integer): Trel_Record;
//-----------------------------------------------------------
    function MacWordToPC(Value : word): word;
    asm xchg Al,Ah
    end;

var si: Smallint;
    w: word;

var
  iOffset: Int64;
begin
 // FillChar(Result,SizeOf(Result),0);

  iOffset := HeaderLen + RecordLen*( (aRow * ColCount) + aCol );

  FVirtualFile.Position:=iOffset;

//  if ReadRecord (iOffset, @Result) then
  iLen:=FVirtualFile.Read (w, SizeOf(w));


//  Result:=Inherited GetItem (aRow,aCol);

//  if FVirtualFile.ReadRecord ((aRow * ColCount) + aCol, @w) then
  if iLen=SizeOf(w) then
  begin
    w:=MacWordToPC(w);
    si:=Smallint(w);

    try
       if w=$8000 then
         Result.Rel_H:=EMPTY_HEIGHT
       else
         Result.Rel_H:=si;

    finally
    end;  // try/finally


  end else
    Result.Rel_H:=EMPTY_HEIGHT;


  Result.Clutter_Code:=0;
  Result.Clutter_H:=0;

end;


function TrelNASA.FindPointXY (aXYPoint: TXYPoint; aZoneNum: integer; var aRec: Trel_Record): boolean;
var
  blPoint: TBLPoint;
begin
  blPoint:=geo_XY_to_BL (aXYPoint, aZoneNum);

  //KRAS->WGS
  blPoint:=geo_BL_to_BL_new (blPoint, EK_CK_42, EK_WGS84 );

  Result:=FindPointBL (blPoint, aRec);
end;


function TrelNASA.FindCellXY (aXYPoint: TXYPoint; var Row,Col: integer): boolean;
var
  blPoint: TBLPoint;
begin
  blPoint:=geo_XY_to_BL (aXYPoint);
  Result:=FindCellBL (blPoint, Row,Col);
end;


//---------------------------------------------------------------
function TrelNASA.FindPointBL (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean;
//--------------------------------------------------------------
var iRow,iCol: integer;
begin
  Result:=FindCellBL (aBLPoint, iRow,iCol);
  if Result then begin
    aRec:=Items[iRow,iCol];

    if aRec.rel_h = EMPTY_HEIGHT then
      Result:=False;
  end;
end;


//--------------------------------------------------------------
class function TrelNASA.GetInfo (aFileName: string; var aRec: TrelMatrixInfoRec): boolean;
//--------------------------------------------------------------
var
  obj: TrelNASA;
begin
  obj:=TrelNASA.Create;

  aRec.FileSize:=obj.FileSize;
  aRec.RowCount:=obj.RowCount;
  aRec.ColCount:=obj.ColCount;

  aRec.StepB:=obj.StepB;
  aRec.StepL:=obj.StepL;

  aRec.BLBounds:=NASA_FileName_to_BLBounds(aFileName);

  obj.Free;

  Result:=True;


(*

  if obj.OpenFile(aFileName) then
  begin
    FillChar (aRec, SizeOf(aRec), 0);

    aRec.FileSize:=obj.FileSize;
    aRec.RowCount:=obj.RowCount;
    aRec.ColCount:=obj.ColCount;

    aRec.StepB:=obj.StepB;
    aRec.StepL:=obj.StepL;

    aRec.BLBounds:=obj.FBLBounds;

    Result:=True;
  end else
    Result:=False;
    *)


end;


//-------------------------------------------------
class function TrelNASA.GetInfoStr (aFileName: string): string;
//-------------------------------------------------
const
  MSG_INFO = '��� B [m]: %0.7f'  + CRLF +
             '��� L [m]: %0.7f'  + CRLF +
             '������: %d x %d'   + CRLF +
             '������� (BL): %0.0f; %0.0f  --  %0.0f; %0.0f' ;


var obj: TrelNASA;
begin
  obj:=TrelNASA.Create;

  if obj.OpenFile(aFileName) then
   with obj do
    Result:=Format(MSG_INFO,
       [StepB, StepL,
        RowCount, ColCount,
        BLBounds.TopLeft.B,     BLBounds.TopLeft.L,
        BLBounds.BottomRight.B, BLBounds.BottomRight.L
       ]);

  obj.Free;

end;




//  TLatLonBounds = record
 // TXYBounds_MinMax = record
//                Lat_Min  : Double;
//                Lat_Max  : Double;
//                Lon_Min  : Double;
//                Lon_Max  : Double;
//              end;
//                     

//---------------------------------------------------------------
function NASA_FileName_to_BLBounds(aFileName: string): TBLRect;
//---------------------------------------------------------------
var
  b_E: Boolean;
  b_N: Boolean;
  n,e: integer;
  S: string;
begin
  Result :=NULL_BLRECT;

  // Result:=Open (aFileName);

   // ������ 2 ����� - ������
   // ������ 3 ����� - �������

   s:=UpperCase(ExtractFileName(aFileName));

   b_N:=Copy(s,1,1)='N';
   b_E:=Copy(s,4,1)='E';   
   
 //  if (Copy(s,1,1)='N') and
  //    (Copy(s,4,1)='E')
  // then begin
     n:=AsInteger(Copy(s,2,2));
     e:=AsInteger(Copy(s,5,3));

     // -----------------------------------
     if b_N then
     // -----------------------------------
     begin
       Result.TopLeft.B    :=n+1;
       Result.BottomRight.B:=n;
       
     end else
     begin
       Result.TopLeft.B    :=-n+1;
       Result.BottomRight.B:=-n;
       
     end;
     
     // -----------------------------------
     if b_E then
     // -----------------------------------
     begin 
       Result.TopLeft.L    :=e;
       Result.BottomRight.L:=e+1;
       
     end else
     begin
       Result.TopLeft.L    :=-e;
       Result.BottomRight.L:=-e+1;
       
     end;     
     

     
  // end ;
 //    else
  //     ;

//
// Result.TopLeft.B:=Result.TopLeft.B * IIF(b_N,1,-1);
// Result.BottomRight.B:=Result.BottomRight.B * IIF(b_N,1,-1);
//
// Result.TopLeft.L    :=Result.TopLeft.L     * IIF(b_E,1,-1);
// Result.BottomRight.L:=Result.BottomRight.L * IIF(b_E,1,-1);

 
//  Assert(Result.BLRect.BottomRight.B <> Result.TopLeft.B);
 // Assert(Result.BottomRight.L        <> Result.TopLeft.L);  
   
end;





end.

{
//---------------------------------------------------------------
function TrelNASA.FindCellBL (aBLPoint: TBLPoint; var Row,Col: integer): boolean;
//--------------------------------------------------------------
begin
  if  (Active) and
      ((FBLBounds.TopLeft.B > aBLPoint.B) and (aBLPoint.B > FBLBounds.BottomRight.B) and
       (FBLBounds.TopLeft.L < aBLPoint.L) and (aBLPoint.L < FBLBounds.BottomRight.L)) then
  begin
    Row := Abs(Trunc((aBLPoint.B - FBLBounds.TopLeft.B) / StepB));
    Col := Abs(Trunc((aBLPoint.L - FBLBounds.TopLeft.L) / StepL));

    Result:=(Row>=0) and (Col>=0) and (Row<=RowCount-1) and (Col<=ColCount-1);
  end else
    Result:=false;
end;}






{
//------------------------------------------------------
procedure TrelNASA.SaveHeaderToMIF(aFileName,aBitmapFileName: string);
//------------------------------------------------------
var rec: TMifHeaderInfoRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.FileName:=ChangeFileExt (aFileName, '.tab');
  rec.BitmapFileName:=aBitmapFileName;
  rec.BL.BLBounds:=FBLBounds;
  rec.ImgRowCount:=RowCount;
  rec.ImgColCount:=ColCount;
//  rec.ImgRowCount:=aImgRowCount;
//  rec.ImgColCount:=aImgColCount;
  rec.BL.StepB:=StepB;
  rec.BL.StepL:=StepL;

  geo_SaveBLHeaderForMIF (rec);
end;
}
