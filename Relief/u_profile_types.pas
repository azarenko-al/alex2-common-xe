unit u_profile_types;

interface

type
  TrelProfileType =
     (prtUnknown,

      prtOpen,

      prtHalfOpened,
      prtHalfOpened_relief,
      prtHalfOpened_forest,
      prtHalfOpened_city,
      prtHalfOpened_country,
      prtClosed,
      prtClosed_relief,
      prtClosed_forest,
      prtClosed_city,        //10
      prtClosed_country,

      prtAny
      );

  function profile_TypeToStr (aValue: TrelProfileType): string;


implementation


//-------------------------------------------------------------------
function profile_TypeToStr (aValue: TrelProfileType): string;
//-------------------------------------------------------------------
var i: integer;
begin
  i:=Ord(aValue);
  if i=10 then
   i:=10;

  case aValue of
    prtUnknown  : Result:='?';
    prtOpen: Result:='������';

    prtClosed: Result:='������';
    prtClosed_relief:  Result:='������-������';
    prtClosed_forest:  Result:='������-���';
    prtClosed_city:    Result:='������-�����';
    prtClosed_country: Result:='������-��';

 //   ptHalfOpened: Result:='����������';
    prtHalfOpened_relief:  Result:='����������-������';
    prtHalfOpened_forest:  Result:='����������-���';
    prtHalfOpened_city:    Result:='����������-�����';
    prtHalfOpened_country: Result:='����������-��';

  else
    Result:='';

  end;
end;


end.
