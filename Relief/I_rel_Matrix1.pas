unit I_rel_Matrix1;

interface
{$DEFINE test}

uses
  Graphics,

  u_Geo;

const
  DEF_CLU_OPEN_AREA = 0;  // 1- ��� (1)
  DEF_CLU_FOREST    = 1;  // 1- ��� (1)
  DEF_CLU_WATER     = 2;  // 3- ���
  DEF_CLU_COUNTRY   = 3;  // 3- ���
  DEF_CLU_ROAD      = 4;
  DEF_CLU_RailROAD  = 5;
  DEF_CLU_CITY      = 7;  // 7- �����
  DEF_CLU_BOLOTO    = 8; // 31- ���
  DEF_CLU_ONE_BUILD = 73; // 73- ���.����
  DEF_CLU_ONE_HOUSE = 31; // 31- ���

  FILTER_RLF_MATRIX      = '������� ����� (*.rlf;*.hgt)|*.rlf;*.hgt';
  FILTER_RLF_MATRIX_EXT  = '*.rlf;*.hgt';


  MAX_REL_POINTS = 30000;
  EMPTY_HEIGHT  : smallint = High(smallint);

type

  //-------------------------------------------------
  TrelProfilePointRec = packed record  // ������ �� ������� ���������
  //-------------------------------------------------
    Rel_H         : smallint; // ������ ������� (��� ������� ����)

    Clutter_H     : single;   // ������ �������� - �� ���
                              //�������- ��� ���������������� ��������������
    Clutter_Code  : smallint; // ��� �������� ��������

    Distance_KM   : double;   // ���������� �� ��������� ����� (0)
    Earth_H       : double;   // ������ ����� (��� ����������� �������)
    Full_H        : double;
  end;


  TBuildProfileParamRec = packed record
    XYVector       : TXYVector;
    BLVector       : TBLVector;
    Step_m         : integer;
    Zone           : integer;
    Refraction     : double;

 //   MaxDistance_KM : Double; //����������� �����

    BL_Length_KM   : Double; // ����� ��� ������� BL
  end;

  //-------------------------------------------------------------------
  TrelProfileRec = packed record
  //-------------------------------------------------------------------
    Count       : integer;
    XYVector    : TXYVector;
    BLVector    : TBLVector;

    StepX,StepY : double; //km
    Step_R_km   : double; //km

    Distance_KM : double; //km

    Items: array[0..MAX_REL_POINTS-1] of TrelProfilePointRec;
  end;

  // ������ ������� �������
  Trel_Record = packed record
    Rel_H        : smallint;  // ������ ������� � ����� ��
    Clutter_Code : byte;
    Clutter_H    : byte;
  end;

  //-------------------------------------------------------------------
  TrelMatrixInfoRec = packed record
  //-------------------------------------------------------------------
    RowCount,ColCount : integer;

    MatrixType: (mtLonLat,mtXY); //mtNone,
//    MatrixType: (mtNone,mtXY);

    // BL matrix
    BLBounds    : TBLRect;
    StepB,StepL : double;

    // XY matrix
    XYBounds: TXYRect;
    ZoneNum_GK : word; //integer;
    StepX,StepY: double;

    FileSize: Int64;

    GroundMinH,GroundMaxH: double;

    NullValue : Integer;

   // ZoneNum : Byte;
//    Projection: string; //array [0..2] of Char;  // 'GK','UTM' - ������������� �����

  end;




{$IFNDEF test}
//  function GetInterface_IReliefEngineX(var Obj): HResult; stdcall;
{$ENDIF}

const
  DEF_CLUTTER_ARR : array[0..7-1] of record
                                     Code   : Integer;
                                     Color  : Integer;
                                     Height : Integer;
                                     Name   : string;
                                   end
  =
  (
   (Code: DEF_CLU_OPEN_AREA; Color: clWhite),

   (Code: DEF_CLU_FOREST;    Color: clGreen; Height:20),
   (Code: DEF_CLU_WATER;     Color: clBlue),
   (Code: DEF_CLU_COUNTRY;   Color: clGray),
//   (Code: DEF_CLU_ROAD;      Color: clRed)
   (Code: DEF_CLU_CITY;      Color: clRed),
   (Code: DEF_CLU_ONE_BUILD; Color: clMaroon),
   (Code: DEF_CLU_ONE_HOUSE; Color: clOlive)
  );

//
//  DEF_CLU_OPEN_AREA = 0;  // 1- ��� (1)
//  DEF_CLU_FOREST    = 1;  // 1- ��� (1)
//  DEF_CLU_WATER     = 2;  // 3- ���
//  DEF_CLU_COUNTRY   = 3;  // 3- ���
//  DEF_CLU_ROAD      = 4;
//  DEF_CLU_RailROAD  = 5;
//  DEF_CLU_CITY      = 7;  // 7- �����
//  DEF_CLU_BOLOTO    = 8; // 31- ���
//  DEF_CLU_ONE_BUILD = 73; // 73- ���.����
//  DEF_CLU_ONE_HOUSE = 31; // 31- ���
//
//

implementation


end.

