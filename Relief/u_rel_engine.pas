unit u_rel_engine;

interface

uses SysUtils,Classes,Dialogs,Math,ComObj,ActiveX,
     System.Generics.Collections,
     CodeSiteLogging,

   I_rel_matrix1,

   u_Log,
   u_func,
   u_Geo,
   u_Geo_convert,
//   u_VirtFile,

   u_rel_Matrix_base,

//   u_bee_Matrix,
//   u_rel_NASA,
   u_rlf_Matrix
   ;

type
  TPointRelInfo = packed record
                    Rel: Trel_Record;

                    Files: array[0..10] of ShortString;
                //    Files: array[0..0] of ShortString;
                  end;



  //---------------------------------------
  // ������ ������ �������
  //---------------------------------------
  TRelEngine = class(TObjectList<TrelMatrixBase>)
  //(TInterfacedObject, IReliefEngineX)
//  TMapItemList = class(TList<TMapItem>)

  //---------------------------------------
//  type
//    //-------------------------------------------------------------------
//    TrelProfileRec11111111 = record
//    //-------------------------------------------------------------------
//      Count       : integer;
//      XYVector    : TXYVector;
//      BLVector    : TBLVector;
//
//      StepX,StepY : double; //km
//      Step_R_km   : double; //km
//
//      Distance_KM : double; //km
//
//      Items: array[0..MAX_REL_POINTS-1] of TrelProfilePointRec;
//    end;
//

  private
//    function GetMatrixInfo(aFileName: String; var aRec: TrelMatrixInfoRec): boolean;

//    procedure Dlg_ShowMatrixList_11111111111;
//    function Count: Integer;    stdcall;

//    function FileNameExists_111111111(aFileName: String): boolean;
    //-------------------------------------------------
    //������� ��� ������ �������� �������
    //-------------------------------------------------
    //��������� ������ ������ ��� ������ �������


    function FindPointBLInFile(aFileName: String; aBLPoint: TBLPoint; var aRec:
        Trel_Record; var aZone: Integer): boolean; stdcall;
  //  function FindPointBL_Ex_11111111(aBLPoint: TBLPoint; var aRec: TPointRelInfo):
    //    boolean;
//    function FindPointBL_with_FileName(aBLPoint: TBLPoint; var aRec: Trel_Record;
//        var aFileName: String): boolean; stdcall;
//    procedure Clear; stdcall;  //override;


    //��������� ������ 6�� ����
    function GetBest6Zone(aDefaultZone: integer; aXYPoint: TXYPoint): integer;
//    function GetMatrixInfoStr__1111111111(aFileName: String): String;
//    FList: TList;

//    function GetItem (Index:integer): TrelMatrixBase;

//     procedure LoadFromStrings (aStrings: TStrings);
 //   function Open: boolean;
//    procedure CalcSummary (var aSummary: TrelSummary);

  public

//    constructor Create;
//    destructor Destroy; override;

//    function AddFile (aFileName: string): integer;TrelMatrixBase;
    function AddFile(aFileName: String): Boolean;
//    procedure RemoveFile(aFileName: String);stdcall;

//    function FileExists (aFileName: string): boBoolean;
    function IndexOf(aFileName: String): integer;

    function  FindPointXY  (aPoint: TXYPoint; aZoneNum: integer; var aRec: Trel_Record): boolean;    //overload;
//    function  FindPointXY_ (aX,aY: double; var aRec: Trel_Record): boolean; //overload;
    function  FindPointBL  (aBLPoint: TBLPoint; var aRec: Trel_Record): Boolean;  stdcall;


//    procedure Close();


    function BuildProfileXY (var aProfileRec: TrelProfileRec;
                             var aRec: TBuildProfileParamRec;

                             aXYVector: TXYVector;
                             aStep_m: integer;
                             aZone: integer;
                             aRefraction: double
                            // aMaxDistance_km: double=0
                             ): boolean; stdcall;

    function BuildProfileBL (var aProfileRec: TrelProfileRec;
                             var aRec: TBuildProfileParamRec;

                             aBLVector: TBLVector;
                             aStep_m: integer;
                             aRefraction: double
                            // aMaxDistance_km: double=0
                             ): boolean;  stdcall;

    function GetStepMin: Integer;

//    property Items [Index:integer]: TrelMatrixBase read GetItem; default;
  end;



//  function GetDllVer: WideString; stdcall;


//  function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall;

//  function GetInterface_IReliefEngineX(var Obj): HResult; stdcall;


//function GetReliefEngine: TRelEngine;
//var
//  g_RelEngine: TRelEngine;


  function rel_CreateRelMatrixByFileName(aFileName: string): TrelMatrixBase;



(*
exports
  GetInterface_IReliefEngineX,
  DllGetInterface;
*)

//  function Get_IReliefEngineX: IReliefEngineX; stdcall;

(*
exports
  Get_IReliefEngineX;
*)

//===========================================================
// implementation
//===========================================================
implementation

uses
  System.DateUtils;
//var
//  FRelEngine: TRelEngine;


(*
procedure TRelEngine.Initialize;
begin
  inherited;
  FList:=TList.Create();


//  ShowMessage('TRelEngine.Initialize');
end;
*)
//
//constructor TRelEngine.Create;
//begin
//  inherited;
////  FList:=TList.Create();
//
////  ShowMessage('TRelEngine.Create;');
//end;
//
//
//destructor TRelEngine.Destroy;
//begin
////  ShowMessage('TRelEngine.Destroy;');
//
////  FreeAndNil(FList);
//
////  FList.free;
//  inherited;
//
////  ShowMessage('destructor TRelEngine.Destroy;');
//end;
//


//-------------------------------------------------------
//
// TRelEngine : ������ ������ �������
//

//-------------------------------------------------------
function TRelEngine.FindPointBL (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean;
//-------------------------------------------------------
var i:integer;
begin
  for i:=0 to Count-1 do
    if Items[i].Active then
      if Items[i].FindPointBL (aBLPoint, aRec) then
      begin
        {
        if Clutters.Count>0 then
         if (aRec.Loc_Code > 0) and (aRec.Loc_H = 0) then
           aRec.Loc_H := Clutters.ItemsByID[aRec.Loc_Code].Height;
        }
        Result:=true;
        Exit;
      end;

  Result:=false;
end;





// ---------------------------------------------------------------
function TRelEngine.FindPointXY (aPoint: TXYPoint; aZoneNum: integer; var aRec: Trel_Record): boolean;
// ---------------------------------------------------------------
var i:integer;
begin
//  if FList.Count=0 then
 //   g_Log.AddError('TRelEngine', '���-�� ������ = 0');

  for i:=0 to Count-1 do
    if Items[i].Active then
      if Items[i].FindPointXY (aPoint, aZoneNum, aRec) then
      begin
        {
        if Clutters.Count>0 then
         if (aRec.Loc_Code > 0) and (aRec.Loc_H = 0) then
           aRec.Loc_H := Clutters.ItemsByID[aRec.Loc_Code].Height;
        }
        Result:=true;
        Exit;
      end;

  Result:=false;    
end;



  function CompareByStep (Item1,Item2: Pointer): integer;
  var obj1,obj2: TrelMatrixBase;
  begin
    obj1:=TrelMatrixBase(Item1);  obj2:=TrelMatrixBase(Item2);

    if obj1.StepX > obj2.StepX then Result:=+1 else
    if obj1.StepX < obj2.StepX then Result:=-1 else Result:=0;
  end;




//-------------------------------------------------------------------
function TRelEngine.AddFile(aFileName: String): Boolean;
//-------------------------------------------------------------------
var obj: TrelMatrixBase;
    sExt: string;
    ind: integer;
begin
  Result:=False;
  if not FileExists (aFileName) then
    Exit;

  ind:=IndexOf(aFileName);
  if ind>=0 then
  begin
    Result:=True;
  //  Items[ind];
    Exit;
  end;

  obj:=rel_CreateRelMatrixByFileName (aFileName);
  if not Assigned(obj) then
    Exit;

  Add (obj);
  obj.OpenFile (obj.FileName);

  Result:=True;

//  FList.Sort(CompareByStep);
//  Result:=obj;
end;



function TRelEngine.IndexOf(aFileName: String): integer;
var i:integer;
begin
  for i:=0 to Count-1 do
    if Eq(aFileName, Items[i].FileName) then begin Result:=i; Exit; end;
  Result:=-1;
end;



//--------------------------------------------------------------
function TRelEngine.BuildProfileXY (
                                    var aProfileRec: TrelProfileRec;
                                    var aRec: TBuildProfileParamRec;

                                    aXYVector: TXYVector;
                                    aStep_m: integer;
                                    aZone: integer;
                                    aRefraction: double
                                   // aMaxDistance_km: double=0
                                     ): boolean;
//--------------------------------------------------------------
var
  I: Integer;
  j,iTest,iCount: integer;
  fDistance_km: double;
  xyPos: TXYPoint;
  distance_x, distance_y,
  dStepX, dStepY : double;
  rel_Point: Trel_Record;

  d,test_d,test_r2: double;
  ind: Integer;

  dt_Start: TDateTime;

begin
  dt_start:=Now;
  CodeSite.Send( 'function TRelEngine.BuildProfileXY...' );


  Assert(aRec.XYVector.Point1.X = aXYVector.Point1.X);
  Assert(aRec.XYVector.Point1.y = aXYVector.Point1.y);
  Assert(aRec.XYVector.Point2.X = aXYVector.Point2.X);
  Assert(aRec.XYVector.Point2.y = aXYVector.Point2.y);

  Assert(aRec.Step_m         = aStep_m);
  Assert(aRec.Zone           = aZone);
//  Assert(aRec.MaxDistance_km = aMaxDistance_km);
  Assert(aRec.Refraction     = aRefraction);


   aProfileRec.XYVector:=aXYVector;

   //notes:
   // - ���� ��� ���������� ������� ���� ��������� ����� �� ������� �������
   //   �� ����� ������� �� ���� �������
   Result:=true;

   iCount:=0;
//   iDisplayCount:=0;

   distance_x:=aXYVector.Point2.X-aXYVector.Point1.X;  //��������� ���������� ���Y=��������Y- �� ����������
   distance_y:=aXYVector.Point2.Y-aXYVector.Point1.Y;  //��������� ���������� ���X=��������X- �� ����������
   fDistance_km:=0.001*Sqrt(Sqr(distance_x)+Sqr(distance_y)); //m

//   d:=geo_GetDist

   //���� ����������� ����� ��������� ��� ���������� �������
   if (fDistance_km< 0.001)
//    --or
//      ((aMaxDistance_km<>0) and (fDistance_km > aMaxDistance_km))
   then begin
      Result := False;
      Exit;
   end;

   if aStep_m < 1 then
     aStep_m:=1;

   iCount:=Ceil(fDistance_km/(aStep_m*0.001))+1;

   if iCount > 1000 then
     iCount:=1000;

CodeSite.Send( 'iCount : ', iCount );

   aProfileRec.Step_R_km:=fDistance_km / (iCount-1);

//   0.001*Sqrt(Sqr(aProfileRec.StepX) + Sqr(aProfileRec.StepY)); //km

{   if Abs(distance_x)>Abs(distance_y)
       then iCount:=Round(Abs(distance_x)/aStep)+1
       else iCount:=Round(Abs(distance_y)/aStep)+1;
}
 //  if Abs(distance_x)>Abs(distance_y)
  //     then
       aProfileRec.StepX:= (distance_x) / (iCount-1);
      // else
       aProfileRec.StepY:= (distance_y) / (iCount-1);


   //����������� ��� �������� ����� � "������"

   // ����������� � ��������� ����� �� �������� ����������� ������, �.�.
   // ���������� �������� ��������� ����� ��-�� �������� ������� ��������

   iTest:=Length(aProfileRec.Items);

   if iCount >= Length(aProfileRec.Items) then begin
    /////////////// raise Exception.Create('iCount > max');
    // FCount:=0;
     Exit;
   end;


//   aProfile.DisplayCount:=iCount;
   aProfileRec.Count:=iCount;

   //BS-PO
   xyPos:=aXYVector.Point1;
   //PO-BS

//   aProfileRec.Step_R:=0.001*Sqrt(Sqr(aProfileRec.StepX) + Sqr(aProfileRec.StepY)); //km

//   aProfileRec.Distance_km:=aProfileRec.Step_R * (aProfileRec.Count-1);
   aProfileRec.Distance_km:=fDistance_km;

   if aProfileRec.Distance_km=0 then
     aProfileRec.Distance_km:=aProfileRec.Step_R_km;


   for i:=0 to iCount -1 do
//     for i:=iCount -1 to iCount -1 do
   begin
   //  if i=344 then
    //  j:=1;

     xyPos.X:=xyPos.X + aProfileRec.StepX;
     xyPos.Y:=xyPos.Y + aProfileRec.StepY;
    // xyPos.Zone:=aZone;

//     aProfileRec.Items[i].Distance:=aProfileRec.Step_R * i; // * (i-1);
     aProfileRec.Items[i].Distance_km:=aProfileRec.Step_R_km * i; // * (i-1);
//     aProfileRec.Items[i].Distance_km:=TruncFloat(aProfileRec.Step_R_km * i, 4); // * (i-1);
   //   aProfileRec.Items[i].Earth_H := geo_GetEarthHeight (aProfileRec.Items[i].Distance, aProfileRec.Distance, aRefraction);
     aProfileRec.Items[i].Earth_H := geo_GetEarthHeight (aProfileRec.Items[i].Distance_km, aProfileRec.Distance_km, aRefraction);

     if i=iCount-1 then
       aProfileRec.Items[i].Earth_H:=0;

     aProfileRec.Items[i].Clutter_code:=0;
     aProfileRec.Items[i].Clutter_h   :=0;
     aProfileRec.Items[i].Rel_H   :=0;
     aProfileRec.Items[i].Full_H  :=0; // Items[i].Rel_H + Items[i].Clu_h + Items[i].Earth_H;


//     test_d:=Items[i].Distance; //:=Step_R * i; // * (i-1);
//     test_d:=geo_GetEarthHeight (Items[i].Distance, Distance, Refraction);

 //    if Result then
      if FindPointXY (xyPos, aZone, rel_Point) then
//      if FindPointXY_ (xyPos.X, xyPos.Y, rel_Point) then
     begin
     //  if rel_Point.rel_h > 10000 then
      //   raise Exception.Create('rel_Point.rel_h > 10000');

       aProfileRec.Items[i].Clutter_code:=rel_Point.Clutter_Code;
       aProfileRec.Items[i].Clutter_h   :=rel_Point.Clutter_h;
       aProfileRec.Items[i].Rel_H       :=rel_Point.rel_h;

     //  Rel_H + Clu_h + Earth_H;

       aProfileRec.Items[i].Full_H  := aProfileRec.Items[i].Rel_H +
                                       aProfileRec.Items[i].Clutter_h +
                                       aProfileRec.Items[i].Earth_H;
                                //aProfileRec.ItemFullHeight (i); // Items[i].Rel_H + Items[i].Clu_h + Items[i].Earth_H;

//       Inc(iDisplayCount);
     end else begin
       aProfileRec.Items[i].Rel_H:=0;//EMPTY_HEIGHT;
       aProfileRec.Items[i].Full_H:=aProfileRec.Items[i].Earth_H;
      // aProfileRec.Items[i].Clu_h:=0;

       Result:=False;
     end;

   end;

 //     aProfileRec.Count:=iCount;
 {
   // ----------------------------------
   if aRec.BL_Length_KM>0 then
   // ----------------------------------
   begin
     for I := aProfileRec.Count-1 downto 1 do
       if aProfileRec.Items[i].Distance_KM = aRec.BL_Length_KM then
         Exit
       else
         if aProfileRec.Items[i].Distance_KM > aRec.BL_Length_KM then
         begin
           Dec(aProfileRec.Count);
         end else
           Break;

//     if aProfileRec.Count>0 then

     Inc(aProfileRec.Count);

     ind := aProfileRec.Count-1;
     xyPos:=aXYVector.Point2;

     if FindPointXY (xyPos, aZone, rel_Point) then
     begin
       aProfileRec.Items[ind].Distance_KM:=aRec.BL_Length_KM;

       aProfileRec.Items[ind].Clutter_code:=rel_Point.Clutter_Code;
       aProfileRec.Items[ind].Clutter_h   :=rel_Point.Clutter_h;
       aProfileRec.Items[ind].Rel_H       :=rel_Point.rel_h;
       aProfileRec.Items[ind].Earth_H     :=0;

       aProfileRec.Items[ind].Full_H  := rel_Point.Rel_H +
                                         rel_Point.Clutter_h +
                                         0;
     end;


   end;
  }


  CodeSite.Send( 'function TRelEngine.BuildProfileXY done ' +
                 intToStr( MilliSecondsBetween(dt_start, Now)) + ' msec' );


   {
   test_r2 := aProfileRec.Distance - aProfileRec.Items[aProfileRec.Count-1-1].Distance;
   if test_r2<0 then  raise Exception.Create('r2<0');
   }

 ///////////// aProfile.ApplyDefaultHeights();

end;



//--------------------------------------------------------------------
function TRelEngine.GetBest6Zone(aDefaultZone: integer; aXYPoint: TXYPoint):
    integer;
//--------------------------------------------------------------------
//��������� ������ 6�� ���� �� ������ (���������)
var i,iMin,iDelta:integer;
  XYBounds: TXYRect;
begin
//  Reload();
  iMin:=100;
  for i:=0 to Count-1 do
   // if {(Items[i].Active)} then
    begin
      //only matrixes around point
      if Items[i].Info.MatrixType = mtXY then
      begin
        XYBounds:=Items[i].Info.XYBounds;

        if (aXYPoint.X < XYBounds.TopLeft.x)     and
           (aXYPoint.Y > XYBounds.TopLeft.y)     and
           (aXYPoint.X > XYBounds.BottomRight.x) and
           (aXYPoint.Y < XYBounds.BottomRight.y)
        then begin
          iDelta:=Abs(Items[i].ZoneNum_GK-aDefaultZone);
          if iDelta < iMin then
            begin
               iMin:=iDelta;
               aDefaultZone:=Items[i].ZoneNum_GK;
            end;
        end;
      end;
    end;
  Result:=aDefaultZone;
end;


//--------------------------------------------------------------
function TRelEngine.BuildProfileBL(

    var aProfileRec: TrelProfileRec;
    var aRec: TBuildProfileParamRec;

    aBLVector: TBLVector;
    aStep_m: integer;
    aRefraction: double
  //  aMaxDistance_km: double=0
    ):  boolean;

//--------------------------------------------------------------
// ��������� ����� ������� �� 2 �����������
//--------------------------------------------------------------
var
  i,{iStep,}iZone,iZone1,iZone2: integer;
  xyVector: TXYVector;
  xyPoint: TxyPoint;

  rec: TBuildProfileParamRec;
begin
//  Assert(aRec.XYVector.Point1.X = aBLVector.Point1.X);
//  Assert(aRec.XYVector.Point1.y = aBLVector.Point1.y);
//  Assert(aRec.XYVector.Point2.X = aBLVector.Point2.X);
//  Assert(aRec.XYVector.Point2.y = aBLVector.Point2.y);

  Assert(aRec.Step_m         = aStep_m);
//  Assert(aRec.Zone           = aZone);
//  Assert(aRec.MaxDistance_km = aMaxDistance_km);
  Assert(aRec.Refraction     = aRefraction);


  //���������� ����� ���� ��� �������
  iZone1:= geo_Get6ZoneL (aBLVector.Point1.L);

  //for RLF matrixes
  xyPoint:=geo_BL_to_XY (aBLVector.Point1);
  iZone2:= GetBest6Zone(iZone1, xyPoint);


  if iZone2>0 then  iZone:=iZone2
              else  iZone:=iZone1;

  aProfileRec.BLVector:=aBLVector;

  xyVector.Point1:=geo_BL_to_XY (aBLVector.Point1,iZone);
  xyVector.Point2:=geo_BL_to_XY (aBLVector.Point2, iZone);

//  Open();

  //define min step
  // 26.02.07
  if aStep_m=0 then
    aStep_m:=50;

{  begin
    iStep:=0;
   // with aMatrixList do
      for i:=0 to Count-1 do
       if (Items[i].Active) and
          ((iStep=0) or (Items[i].Step < iStep))
       then iStep:=Round(Items[i].Step);
  end else
    iStep:=aStep;
  if iStep=0 then
    Result:=False
  else
}

  FillChar(rec,SizeOf(rec),0);

  rec.XYVector       := xyVector;
  rec.Step_m         := aStep_m;
  rec.Refraction     := aRefraction;
//  rec.MaxDistance_KM := aMaxDistance_km;
  rec.Zone           := iZone;
  rec.BL_Length_KM   := geo_Distance_km(aBLVector);

  Result:=BuildProfileXY (aProfileRec, rec,
         xyVector, aStep_m, iZone, aRefraction); //, aMaxDistance_km);

end;




// ---------------------------------------------------------------
function TRelEngine.FindPointBLInFile(aFileName: String; aBLPoint: TBLPoint;
    var aRec: Trel_Record; var aZone: Integer): boolean;
// ---------------------------------------------------------------
var
  iIndex: Integer;
begin
  Result := False;
  iIndex := IndexOf (aFileName);

 // iIndex:= gl_RelMatrixList.IndexOf(aFileName);

  if iIndex>=0 then
  begin
    Result:= Items[iIndex].FindPointBL(aBLPoint, aRec);
    if Result then
    begin
      aZone :=Items[iIndex].ZoneNum_GK;


      Result:= Items[iIndex].FindPointBL(aBLPoint, aRec);
    end;
  end;
end;



function TRelEngine.GetStepMin: Integer;
var
  i: Integer;
  oRel: TrelMatrixBase;
begin
  Result:=0;


  for i:=0 to Count-1 do
  begin
    oRel:=Items[i];

    if (Result =0) or (oRel.StepX<Result) then
      Result := round (oRel.StepX);
  end;


  // TODO -cMM: TRelEngine.GetStepMin default body inserted
end;



//-------------------------------------------------------------------
function rel_CreateRelMatrixByFileName(aFileName: string): TrelMatrixBase;
//-------------------------------------------------------------------
var sExt: string;
begin
  Result:=nil;
  sExt:=LowerCase(ExtractFileExt (Trim(aFileName)));

//  if sExt='.hdr'  then Result:=TrelIntergraph.Create else

  if sExt='.rlf'  then Result:=TrlfMatrix.Create
//  if sExt='.ter'  then Result:=TBeeMatrix.Create else
//  if sExt='.hgt'  then Result:=TrelNASA.Create
 // if sExt='.vm'  then Result:=Trel_VerticalMapper.Create
  else begin
    Result:=nil;
    Exit;

    raise Exception.Create(aFileName);
  end;

  Result.FileName:=aFileName;
end;


initialization
 // g_RelEngine:=TRelEngine.Create;

finalization
//  FreeAndNil(g_RelEngine);

//  if Assigned(FRelEngine) then
//    FreeAndNil(FRelEngine);
   // := TRelEngine.Create;



end.



{

procedure TRelEngine.Clear;
var i:integer;
begin
  for i:=0 to FList.Count-1 do Items[i].Free;
  FList.Clear;
end;


function TRelEngine.GetItem (Index: integer): TrelMatrixBase;
begin
  Result:=TrelMatrixBase(FList[Index]);
end;



function TRelEngine.Count: Integer;
begin
  Result := FList.Count;
end;



{
//-------------------------------------------------------------------
function TRelEngine.GetMatrixInfo(aFileName: String; var aRec: TrelMatrixInfoRec):
    boolean;
//-------------------------------------------------------------------
var obj: TrelMatrixBase;
    sExt: string;
begin
  Result:=False;
//  if not FileExists (aFileName) then
//    Exit;

  sExt:=LowerCase(ExtractFileExt (aFileName));

//  if sExt='.rel'  then obj:=TrelNevaMatrix.GetInfo (aFileName, aRec) else
  if sExt='.rlf'  then
    Result:=TrlfMatrix.GetInfo  (aFileName, aRec) else


//  if sExt='.ter'  then
//    Result:=TBeeMatrix.GetInfo  (aFileName, aRec) else

//  if sExt='.zip'  then
//    Result:=TrelNASA.GetInfo (aFileName, aRec) else

  if sExt='.hgt'  then
    Result:=TrelNASA.GetInfo (aFileName, aRec)

//  if sExt='.vm'  then Result:=Trel_VerticalMapper.GetInfo    (aFileName, aRec)

  else begin
   // Result:=nil;
    raise Exception.Create(sExt);
  end;

end;
}


(*
// ---------------------------------------------------------------
function DllGetInterface(const IID: TGUID; var Obj): HResult;
// ---------------------------------------------------------------
begin
  Result:=S_FALSE; Integer(Obj):=0;

  if IsEqualGUID(IID, IReliefEngineX) then
  begin
    with TRelEngine.Create do
      if GetInterface(IID,OBJ) then
        Result:=S_OK
  end;
end;


// ---------------------------------------------------------------
function GetInterface_IReliefEngineX(var Obj): HResult;
// ---------------------------------------------------------------
begin
  Result:=S_FALSE; Integer(Obj):=0;

  with TRelEngine.Create do
    if GetInterface(IReliefEngineX,OBJ) then Result:=S_OK
end;

*)


//function GetDllVer: WideString;
//begin
//  Result := GetAppVersionStr();
//end;


{
function GetReliefEngine: TRelEngine;
begin
  if not Assigned(FRelEngine) then
    FRelEngine:= TRelEngine.Create;

  Result := FRelEngine;

end;
}
     {

//-------------------------------------------------------
function TRelEngine.FindPointBL_Ex_11111111(aBLPoint: TBLPoint; var aRec:
    TPointRelInfo): boolean;
//-------------------------------------------------------
var i,iCount:integer;
begin
  iCount := 0;

  FillChar(aRec,SizeOf(aRec),0);


  for i:=0 to Count-1 do
    if Items[i].Active then
      if Items[i].FindPointBL (aBLPoint, aRec.Rel) then
      begin
       // aFileName:=Items[i].FileName;

        Inc(iCount);

        if iCount <=  Length(aRec.Files) then
          aRec.Files[iCount-1] := Items[i].FileName;



        {
        if Clutters.Count>0 then
         if (aRec.Loc_Code > 0) and (aRec.Loc_H = 0) then
           aRec.Loc_H := Clutters.ItemsByID[aRec.Loc_Code].Height;
        }
        Result:=true;
        Exit;
      end;

  Result:=false;
end;

      {
procedure TRelEngine.Dlg_ShowMatrixList_11111111111;
var
  i: integer;
  oList: TStringList;
  S: string;
begin
  oList:=TStringList.Create;
  s:='';

  for i := 0 to Count - 1 do
    s:=s + Items[i].FileName + CRLF;

  oList.Free;


  ShowMessage(s);
end;


function TRelEngine.FileNameExists_111111111(aFileName: String): boolean;
begin
  Result := IndexOf (aFileName) >=0;

end;


//-------------------------------------------------------------------
function TRelEngine.GetMatrixInfoStr__1111111111(aFileName: String): String;
//-------------------------------------------------------------------
var obj: TrelMatrixBase;
    sExt: string;
begin
//  Result:=False;
//  if not FileExists (aFileName) then
//    Exit;

  sExt:=LowerCase(ExtractFileExt (aFileName));

//  if sExt='.rel'  then obj:=TrelNevaMatrix.GetInfo (aFileName, aRec) else
  if sExt='.rlf'  then Result:=TrlfMatrix.GetInfoStr  (aFileName) else
  if sExt='.hgt'  then Result:=TrelNASA.GetInfoStr (aFileName) else
//////  if sExt='.ter'  then Result:=TBeeMatrix.GetInfoStr (aFileName)
   begin
   // Result:=nil;
    raise Exception.Create('');
  end;

end;




{
//-------------------------------------------------------------------
procedure TRelEngine.RemoveFile(aFileName: String);
//-------------------------------------------------------------------
var ind: integer;
begin
  ind:=IndexOf(aFileName);
  if ind>=0 then
  begin
    Items[ind].Free;
    FList.Delete (ind);
  end;

end;
}


//-------------------------------------------------------
function TRelEngine.FindPointBL_with_FileName(aBLPoint: TBLPoint; var aRec:
    Trel_Record; var aFileName: String): boolean;
//-------------------------------------------------------
var i:integer;
begin
  for i:=0 to Count-1 do
    if Items[i].Active then
      if Items[i].FindPointBL (aBLPoint, aRec) then
      begin
        aFileName:=Items[i].FileName;

        {
        if Clutters.Count>0 then
         if (aRec.Loc_Code > 0) and (aRec.Loc_H = 0) then
           aRec.Loc_H := Clutters.ItemsByID[aRec.Loc_Code].Height;
        }
        Result:=true;
        Exit;
      end;

  Result:=false;
end;

