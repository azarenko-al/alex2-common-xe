unit u_rlf_Matrix;

interface
uses Math,SysUtils,Classes, IniFiles,

//  CodeSiteLogging,

  // u_VirtFile,

   I_rel_Matrix1,

   u_rel_Matrix_base,

   u_files,
 //  u_geo_MIF,

   u_Geo_convert_new,
  
   u_Geo
   ;


type
//  Trlf_Record = Trel_Record;

  //----------------------------------------
  TrlfMatrix = class(TrelMatrixBase)
  //----------------------------------------
  private
    FFileStream: TFileStream;
//    FXYBounds: TXYRect; //���������� ������ : ����� ������� ����, ������ ������ ����

    procedure WriteMemStream (aRow,aCol: integer; aMemStream: TMemoryStream);
    procedure WriteItem (aRow,aCol: integer; aRec: Trel_Record);

  protected
//  procedure SaveHeaderToXYFile(aFileName: string); override;

  public

    //Info: TrelMatrixInfoRec; //inherited

  //  ZoneNum: integer;
//    function ZoneNum(): integer; override;

    constructor Create;

    function OpenFile (aFileName: string): boolean;  override;
    function OpenHeader(aFileName: string): Boolean; override;

    function LoadHeaderFromFile (aFileName: string): boolean;

    function FindPointBL  (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean; override;
    function FindPointBL_WGS(aBLPoint: TBLPoint; var aRec: Trel_Record): boolean;

    function  FindPointXY  (aXYPoint: TXYPoint; aZoneNum: integer; var aRec: Trel_Record): boolean; override;

//    function  FindCellXY   (aXYPoint: TXYPoint; var aRow,aCol: integer): boolean; override;
    function  FindCellBL   (aBLPoint: TBLPoint; var aRow,aCol: integer): boolean; override;

//    procedure SaveHeaderToMIF(aFileName,aBitmapFileName: string); override;
//                               aImgRowCount,aImgColCount: integer); override;

    procedure UpdateMinMaxH(aFileName: string; aGroundMinH, aGroundMaxH: integer);

    class function CreateFileHeader (aFileName: string; aRec: TrelMatrixInfoRec): boolean;

//    class procedure ExportToENVI__11111111111(aFileName, aDestFileName: string);

  //  class function CreateFileHeader (aFileName: string; aRec: TrlfHeaderAddRec): boolean;

    class function GetInfo (aFileName: string; var aRec: TrelMatrixInfoRec): boolean;
    class function GetInfoStr (aFileName: string): string;


    class procedure SaveFileHeaderToFileStream(aFileStream: TFileStream; aRec: TrelMatrixInfoRec);
    class procedure SaveToIni(aFileName: string);

    class procedure Update_Earth_0_to_NULL(aFileName, aFileName_dest: string);

  end;

  
//procedure TrlfMatrix_Test;



//===============================================================
implementation




type

  //---- ��������� ������� ������� ----------------
  Trlf_HeaderRec = packed record
  //-----------------------------------------------
     Mark: array [0..5] of AnsiChar;  // 'RLF0  ' - ������������� �����

     ColCount,RowCount: integer;  // ������ ������� (���-�� �������� �� ��������� � ����������� )

     GroundMinH,GroundMaxH : double;          // ���-���� ������

     TopLeft,NotUsed1,
     BottomRight: packed record X,Y: double; end;

     //,NotUsed2
     ZoneNum : Byte;

     NotUsed3: array [0..2] of Byte;//AnsiChar;  // 'GK','UTM' - ������������� �����

     NotUsed2: array[0..8+8-1-1-3] of Byte; //7-3 bytes
  end;




//===============================================================
const
  CRLF = #13+#10;

{

const
  EMPTY_HEIGHT: smallint = High(smallint);
}


//---------------------------------------------------------------
constructor TrlfMatrix.Create;
//---------------------------------------------------------------
begin
  inherited;

  HeaderLen:=SizeOf(Trlf_HeaderRec);
  RecordLen:=SizeOf(Trel_Record);

  MatrixType:=mtXY_;

  NullValue:=EMPTY_HEIGHT;

end;




//---------------------------------------------------------------
function TrlfMatrix.OpenFile (aFileName: string): Boolean;
//---------------------------------------------------------------
begin

 // aFileName:=Trim(aFileName);
  {
  if Active then
  begin
    Result:=true;
    Exit;
  end;
  }
//   if not Enabled then begin Result:=false; Exit; end;

  Result:=LoadHeaderFromFile(aFileName);
  if not Result then
    Exit;


  Result := inherited OpenFile(aFileName);

//   FFileMode:=aMode;

//  Result:=FVirtualFile.Open (aFileName);

 // Active:=FVirtualFile.Active1;

  //if not Result then
//    Exit;
end;


// ---------------------------------------------------------------
function TrlfMatrix.OpenHeader(aFileName: string): boolean;
// ---------------------------------------------------------------
begin
  LoadHeaderFromFile(aFileName);
end;


//--------------------------------------------------------------
function TrlfMatrix.LoadHeaderFromFile (aFileName: string): boolean;
//--------------------------------------------------------------
var oFileStream: TFileStream;
    iRead: integer;
    rFileHeader  : Trlf_HeaderRec;
    iDataSize  : int64;
    bl: TBLPoint;
begin
  Result:=false;

  if not FileExists(aFileName) then
    Exit;

//  FileSize:=GetFil oFileStream.Size;


  try
    oFileStream:=TFileStream.Create (aFileName, fmShareDenyNone);
    iRead:=oFileStream.Read (rFileHeader, SizeOf(rFileHeader));

    FileSize:=oFileStream.Size;

    if (iRead=SizeOf(rFileHeader)) then
      if (StrComp(rFileHeader.Mark, 'rlf1') = 0) then
    begin
      RowCount  := rFileHeader.RowCount;
      ColCount  := rFileHeader.ColCount;
//      Step      :=    (rFileHeader.TopLeft.x - rFileHeader.BottomRight.x) / rFileHeader.RowCount ;

      StepX     :=Abs(rFileHeader.TopLeft.x - rFileHeader.BottomRight.x) / rFileHeader.RowCount ;
      StepY     :=Abs(rFileHeader.TopLeft.Y - rFileHeader.BottomRight.Y) / rFileHeader.ColCount ;

//      StepY     :=(FFileHeader.BottomRight.y - FFileHeader.TopLeft.y) / FFileHeader.ColCount ;
      XYBounds.TopLeft.X := rFileHeader.TopLeft.x;
      XYBounds.TopLeft.Y := rFileHeader.TopLeft.y;

      XYBounds.BottomRight.x:=XYBounds.TopLeft.X - RowCount*StepX;
      XYBounds.BottomRight.y:=XYBounds.TopLeft.Y + ColCount*StepY;

//      GroundMinH:=Round(rFileHeader.GroundMinH);
//      GroundMaxH:=Round(rFileHeader.GroundMaxH);

      GroundMinH:= Round( rFileHeader.GroundMinH);
      GroundMaxH:= Round( rFileHeader.GroundMaxH);

      ZoneNum_GK:=rFileHeader.ZoneNum;

      //check filesize
      iDataSize:=Int64(ColCount) * Int64(RowCount) * SizeOf(Trel_Record);


//CodeSite.Send('HeaderLen '+ IntToStr(HeaderLen) );
//CodeSite.Send('iDataSize '+ IntToStr(iDataSize) );
//CodeSite.Send('iDataSize + HeaderLen '+ IntToStr(iDataSize + HeaderLen) );
//CodeSite.Send('FileSize '+ IntToStr(FileSize) );

      Assert((iDataSize + HeaderLen) = FileSize,
       'function TrlfMatrix.LoadHeaderFromFile (aFileName: string): boolean; - (iDataSize + HeaderLen) = FileSize - '+ aFileName);

      Result:=((iDataSize + HeaderLen) = FileSize);
    end;

  finally
    FreeAndNil(oFileStream);
//    oFileStream.Free;
  end;

  if ZoneNum_GK=0 then
    ZoneNum_GK:=geo_Get6ZoneY (XYBounds.TopLeft.Y);

//  FXYBounds.TopLeft.Zone:=ZoneNum;
 // FXYBounds.BottomRight.Zone:=ZoneNum;

{
  ZoneNum:=ZoneNum-1;

  bl:=geo_XY_to_BL(FXYBounds.TopLeft);
  FXYBounds.TopLeft:=geo_BL_to_XY(bl, 0,  ZoneNum);

  bl:=geo_XY_to_BL(FXYBounds.BottomRight);
  FXYBounds.BottomRight:=geo_BL_to_XY(bl, 0,  ZoneNum);

}


  Info.FileSize:=FileSize;

  Info.RowCount:=RowCount;
  Info.ColCount:=ColCount;

  Info.XYBounds:=XYBounds;
  Info.StepX:=StepX;
  Info.StepY:=StepY;
  Info.ZoneNum_GK:=ZoneNum_GK;

  Info.GroundMinH:=GroundMinH;
  Info.GroundMaxH:=GroundMaxH;

  Info.MatrixType:=mtXY;

end;


//--------------------------------------------------------------
// STUFF
//--------------------------------------------------------------

//------------------------------------------------------
procedure TrlfMatrix.UpdateMinMaxH(aFileName: string; aGroundMinH, aGroundMaxH:
    integer);
//------------------------------------------------------
var header_rec: Trlf_HeaderRec;
    oFileStream: TFileStream;
begin
  try
    oFileStream:=TFileStream.Create (aFileName, fmShareDenyNone);
    oFileStream.Read (header_rec, SizeOf(header_rec));

    header_rec.GroundMinH:=aGroundMinH;
    header_rec.GroundMaxH:=aGroundMaxH;

    oFileStream.Seek (0, soFromBeginning);
    oFileStream.Write (header_rec, SizeOf(header_rec));
  finally
    oFileStream.Free;
  end;
end;


//------------------------------------------------------
procedure TrlfMatrix.WriteMemStream (aRow,aCol: integer; aMemStream: TMemoryStream);
//------------------------------------------------------
var iPos: integer;
begin
  iPos:=SizeOf(Trlf_HeaderRec) + SizeOf(Trel_Record)*(aRow*ColCount + aCol);
  FFileStream.Seek (iPos, soFromBeginning);
  aMemStream.SaveToStream (FFileStream);
end;

//------------------------------------------------------
procedure TrlfMatrix.WriteItem (aRow,aCol: integer; aRec: Trel_Record);
//------------------------------------------------------
var iPos: integer;
begin
  iPos:=SizeOf(Trlf_HeaderRec) + SizeOf(Trel_Record)*(aRow*ColCount + aCol);
  FFileStream.Seek (iPos, soFromBeginning);
  FFileStream.Write (aRec, SizeOf(aRec));
end;

//------------------------------------------------------
class function TrlfMatrix.CreateFileHeader(aFileName: string; aRec: TrelMatrixInfoRec): boolean;
//------------------------------------------------------
var
  header_rec: Trlf_HeaderRec;
//  r,j: integer;
// // cell_rec: Trel_Record;
  oFileStream: TFileStream;
begin

  assert (aRec.XYBounds.TopLeft.X > 0);
  assert (aRec.XYBounds.TopLeft.Y > 0);
  assert (aRec.XYBounds.BottomRight.X > 0);
  assert (aRec.XYBounds.BottomRight.Y > 0);

  assert (aRec.ColCount > 0);
  assert (aRec.RowCount > 0);


  Result:=False;

  FillChar (header_rec, SizeOf(header_rec), 0);
//  FillChar (cell_rec, SizeOf(cell_rec), 0);

  header_rec.Mark:='rlf1'; // 'RLF1' - ������������� �����
  header_rec.ColCount:=aRec.ColCount;
  header_rec.RowCount:=aRec.RowCount;


  header_rec.TopLeft.X    :=aRec.XYBounds.TopLeft.X;
  header_rec.TopLeft.Y    :=aRec.XYBounds.TopLeft.Y;
  header_rec.BottomRight.X:=aRec.XYBounds.BottomRight.X;
  header_rec.BottomRight.Y:=aRec.XYBounds.BottomRight.Y;

{  header_rec.TopLeft.X    :=aRec.XYFrame.TopLeft.X;
  header_rec.TopLeft.Y    :=aRec.XYFrame.TopLeft.Y;
  header_rec.BottomRight.X:=aRec.XYFrame.BottomRight.X;
  header_rec.BottomRight.Y:=aRec.XYFrame.BottomRight.Y;
}

  header_rec.GroundMinH:=aRec.GroundMinH;
  header_rec.GroundMaxH:=aRec.GroundMaxH;

  ForceDirByFileName (aFileName);


  try
    oFileStream:=TFileStream.Create (aFileName, fmCreate);
    oFileStream.Write (header_rec, SizeOf(header_rec));
    Result:=True;
  finally
    oFileStream.Free;
  end;

end;


//------------------------------------------------------
class procedure TrlfMatrix.SaveFileHeaderToFileStream(aFileStream: TFileStream; aRec:
    TrelMatrixInfoRec);
//------------------------------------------------------
var
  header_rec: Trlf_HeaderRec;

  xyBottomRight: TXYPoint;
begin
  Assert(Assigned(aFileStream), 'Value not assigned');

  assert (aRec.ColCount > 0);
  assert (aRec.RowCount > 0);

  
  assert (aRec.XYBounds.TopLeft.X <> 0);
  assert (aRec.XYBounds.TopLeft.Y <> 0);
  assert (aRec.XYBounds.BottomRight.X <> 0);
  assert (aRec.XYBounds.BottomRight.Y <> 0);



  assert (aRec.StepX> 0);
  assert (aRec.StepY> 0);


  xyBottomRight.X:= aRec.XYBounds.TopLeft.X - aRec.StepX * aRec.RowCount;
  xyBottomRight.Y:= aRec.XYBounds.TopLeft.Y + aRec.StepY * aRec.ColCount;


 // Result:=False;

  FillChar (header_rec, SizeOf(header_rec), 0);
//  FillChar (cell_rec, SizeOf(cell_rec), 0);

  header_rec.Mark:='rlf1'; // 'RLF1' - ������������� �����
  header_rec.ColCount:=aRec.ColCount;
  header_rec.RowCount:=aRec.RowCount;


  header_rec.TopLeft.X    :=aRec.XYBounds.TopLeft.X;
  header_rec.TopLeft.Y    :=aRec.XYBounds.TopLeft.Y;
  header_rec.BottomRight.X:=aRec.XYBounds.BottomRight.X;
  header_rec.BottomRight.Y:=aRec.XYBounds.BottomRight.Y;

{  header_rec.TopLeft.X    :=aRec.XYFrame.TopLeft.X;
  header_rec.TopLeft.Y    :=aRec.XYFrame.TopLeft.Y;
  header_rec.BottomRight.X:=aRec.XYFrame.BottomRight.X;
  header_rec.BottomRight.Y:=aRec.XYFrame.BottomRight.Y;
}

  header_rec.GroundMinH:=aRec.GroundMinH;
  header_rec.GroundMaxH:=aRec.GroundMaxH;

  header_rec.ZoneNum   := aRec.ZoneNum_GK;

//  if aRec.Projection='UTM' then
//    header_rec.Projection:='UTM'
//  else
  //  header_rec.Projection:='GK';

//    header_Rec.ZoneNum :=iGK_ZoneNum + 30;
  //header_Rec.Projection :='UTM';
                  
  try
    aFileStream.Write (header_rec, SizeOf(header_rec));
  finally
  end;

end;


//--------------------------------------------------------------
class function TrlfMatrix.GetInfo (aFileName: string; var aRec: TrelMatrixInfoRec): boolean;
//--------------------------------------------------------------
var obj: TrlfMatrix;
begin
  obj:=TrlfMatrix.Create;
  if obj.LoadHeaderFromFile(aFileName) then
  begin
    aRec:=obj.Info;

    Result:=True;
  end else
    Result:=False;

  FreeAndNil(obj);

end;


//-------------------------------------------------
class function TrlfMatrix.GetInfoStr (aFileName: string): string;
//-------------------------------------------------
const
  MSG_INFO = '��� [m]: %d'     + CRLF +
             '6�� ����: %d'    + CRLF +
             '������: %d x %d' + CRLF +
             '������ min,max [m]: %d, %d' + CRLF +
             '������� (xy): %0.0f; %0.0f  --  %0.0f; %0.0f' ;


var obj: TrlfMatrix;
begin
  obj:=TrlfMatrix.Create;
  obj.LoadHeaderFromFile (aFilenAME);

  with obj do
    Result:=Format(MSG_INFO,
       [Round(StepX), ZoneNum_GK,
        RowCount, ColCount,
        Round(GroundMinH), Round(GroundMaxH),
        Info.XYBounds.TopLeft.x,     Info.XYBounds.TopLeft.y,
        Info.XYBounds.BottomRight.x, Info.XYBounds.BottomRight.y
       ]);

  FreeAndNil(obj);

end;


// ---------------------------------------------------------------
class procedure TrlfMatrix.SaveToIni(aFileName: string);
// ---------------------------------------------------------------
var 
  oMatrix: TrlfMatrix;

  oIni: TIniFile;
begin
  oMatrix:=TrlfMatrix.Create;
  oMatrix.LoadHeaderFromFile (aFileName);

  oIni:=TIniFile.Create(ChangeFileExt(aFileName, '.ini'));

  oIni.WriteInteger('main','ColCount',oMatrix.ColCount);
  oIni.WriteInteger('main','RowCount',oMatrix.RowCount);

  oIni.WriteFloat('main','XYBounds.TopLeft.Y',oMatrix.XYBounds.TopLeft.Y);
  oIni.WriteFloat('main','XYBounds.TopLeft.X',oMatrix.XYBounds.TopLeft.X);

  oIni.WriteFloat('main','XYBounds.BottomRight.Y',oMatrix.XYBounds.BottomRight.Y);
  oIni.WriteFloat('main','XYBounds.BottomRight.X',oMatrix.XYBounds.BottomRight.X);

  
  oIni.WriteFloat('main','StepX',oMatrix.StepX);
  oIni.WriteFloat('main','StepY',oMatrix.StepY);

//  oIni.WriteInteger('main','NODATA_value',NODATA_value);

//  oIni.WriteInteger('main','MinValue',MinValue);
//  oIni.WriteInteger('main','MaxValue',MaxValue);


  FreeAndNil(oIni);

  FreeAndNil(oMatrix);
  
end;



function TrlfMatrix.FindCellBL(aBLPoint: TBLPoint; var aRow,aCol: integer):
    boolean;
var xyPoint: TXYPoint;
begin
  xyPoint:=geo_Pulkovo42_BL_to_XY(aBLPoint, ZoneNum_GK);

//  xyPoint:=geo_BL_to_XY (aBLPoint);
  Result:=FindCellXY (xyPoint, aRow,aCol);
end;

// ---------------------------------------------------------------
function TrlfMatrix.FindPointBL (aBLPoint: TBLPoint; var aRec: Trel_Record): boolean;
// ---------------------------------------------------------------
var
  iNewZone: Integer;
  xy: TXYPoint;
begin
//  iNewZone := geo_Get6ZoneBL(aBLPoint);
//  iNewZone := -1;

  xy:=geo_Pulkovo42_BL_to_XY(aBLPoint, ZoneNum_GK);

//  xy:=geo_BL_to_XY (aBLPoint, ZoneNum_GK);

(*
  //�������� � ����� ����
//  if aXYPoint.Zone<>ZoneNum then
  if aZoneNum <> ZoneNum then
  begin
    bl:=geo_XY_to_BL (aXYPoint, aZoneNum);
    aXYPoint:=geo_BL_to_XY (bl,  ZoneNum);
  end;

*)


//  zon

//  Result:=FindPointXY (xy, geo_Get6ZoneBL(aBLPoint), aRec);

 // Result:=FindPointXY (xy, iNewZone, aRec);

  Result:=FindPointXY (xy, ZoneNum_GK, aRec);
end;


//---------------------------------------------------------------
function TrlfMatrix.FindPointXY (aXYPoint: TXYPoint; aZoneNum: integer; var aRec: Trel_Record): boolean;
//--------------------------------------------------------------
var
  iXofs,iYofs : integer;
  //S: string;
  iZone : integer;
  bl: TBLPoint;
  e1: Double;
  e2: Double;
  e3: Double;
  e4: Double;
  i1: Integer;
begin
  Assert(ZoneNum_GK>0, 'TrlfMatrix ZoneNum =0');

//elp: byte=EK_KRASOVSKY42; aZone: integer=0


{  if geo_Get6ZoneXY( aXYPoint) <> ZoneNum then
  begin
    bl:=geo_XY_to_BL (aXYPoint);
    aXYPoint:=geo_BL_to_XY (bl, ZoneNum);
  end;


}
  //�������� � ����� ����
//  if aXYPoint.Zone<>ZoneNum then
  if aZoneNum>0 then
    if aZoneNum <> ZoneNum_GK then
    begin
 
      bl      :=geo_Pulkovo42_XY_to_BL (aXYPoint, aZoneNum);
      aXYPoint:=geo_Pulkovo42_BL_to_XY (bl, ZoneNum_GK);
    end;

{

  iZone:=ZoneNum;

  s:= FileName;}

 {  bl: TBLPoint;
   S: string;
}
  e1:=Abs(aXYPoint.X - XYBounds.TopLeft.x);
  e2:=Abs(aXYPoint.X - XYBounds.BottomRight.x);

  e3:=Abs(aXYPoint.y - XYBounds.TopLeft.y);
  e4:=Abs(aXYPoint.y - XYBounds.BottomRight.y);
  
  
  if  (Active) and
      (aXYPoint.X < XYBounds.TopLeft.x)     and
      (aXYPoint.X > XYBounds.BottomRight.x) and

      (aXYPoint.Y > XYBounds.TopLeft.y)     and
      (aXYPoint.Y < XYBounds.BottomRight.y)

  then begin
//     iXofs  := Integer(Round((XYBounds.TopLeft.x - aXYPoint.X) / Step));
//     iYofs  := Integer(Round((aXYPoint.Y - XYBounds.TopLeft.y) / Step));
     iXofs  := Trunc((XYBounds.TopLeft.x - aXYPoint.X) / StepX);
     iYofs  := Trunc((aXYPoint.Y - XYBounds.TopLeft.y) / StepY);

     aRec:=GetItem (iXofs, iYofs);

     Result:=(aRec.Rel_H <> EMPTY_HEIGHT);
  end else
    Result:=false;

end;



// ---------------------------------------------------------------
function TrlfMatrix.FindPointBL_WGS(aBLPoint: TBLPoint; var aRec: Trel_Record):  boolean;
// ---------------------------------------------------------------
var
  iNewZone: Integer;
  xy: TXYPoint;
  bl_gk: TBLPoint;
begin
  Assert (ZoneNum_GK>0);

  bl_gk:=geo_WGS84_to_Pulkovo42(aBLPoint);

                   
  
  
//  CodeSite.Send('WGS84 - '+ aBLPoint.ToString);
//  CodeSite.Send('Pulkovo42 - '+ bl_gk.ToString);

  
  
//  iNewZone := geo_Get6ZoneBL(aBLPoint);
//  iNewZone := -1;

  xy:=geo_Pulkovo42_BL_to_XY(bl_gk, ZoneNum_GK);

//  xy:=geo_BL_to_XY (bl_gk, ZoneNum_GK);

(*
  //�������� � ����� ����
//  if aXYPoint.Zone<>ZoneNum then
  if aZoneNum <> ZoneNum then
  begin
    bl:=geo_XY_to_BL (aXYPoint, aZoneNum);
    aXYPoint:=geo_BL_to_XY (bl,  ZoneNum);
  end;

*)


//  zon

//  Result:=FindPointXY (xy, geo_Get6ZoneBL(aBLPoint), aRec);

 // Result:=FindPointXY (xy, iNewZone, aRec);

  Result:=FindPointXY (xy, ZoneNum_GK, aRec);
end;       

 

// ---------------------------------------------------------------
class procedure TrlfMatrix.Update_Earth_0_to_NULL(aFileName, aFileName_dest: string);
// ---------------------------------------------------------------
var
  k: Integer;

  oFS: TFileStream;
  oFS_dest: TFileStream;

  rH: Trlf_HeaderRec;
  rRec: Trel_Record;
  
  
begin
  oFS:=TFileStream.Create(aFileName, fmOpenRead);
  oFS_dest:=TFileStream.Create(aFileName_dest, fmCreate);

  oFS.Read (rH,  SizeOf(Trlf_HeaderRec) );
  oFS_dest.Write (rH,  SizeOf(Trlf_HeaderRec) );
  
  while oFS.Read(rRec, SizeOf(rRec)) >0 do
  begin
    if rRec.Rel_H=0 then
      rRec.Rel_H:=EMPTY_HEIGHT;
  
    oFS_dest.Write(rRec, SizeOf(rRec));
     
    k:=oFS.Position;
     
  end;  

  
  //Trel_Record

  
  FreeAndNil(oFS);
  FreeAndNil(oFS_dest);
  
  //ShowMessage('������� ������������: '+ aFileName_dest);
  
//

//  obj:=TrlfMatrix.Create;
//  obj.OpenFile('c:\GIS\nnovg_pl.rlf');
//  obj.Free;

//  TrlfMatrix
  
end;
   

//   
//procedure TrlfMatrix_Test; 
//var
//  obj: TrlfMatrix;
//
//  xy: TXYPoint;
//  bl: TBLPoint;
//  bl_wgs_new, bl_wgs: TBLPoint;
//  r: Trel_Record;
//  
//begin
////  bl_wgs.B:=55.983285;
////  bl_wgs.L:=37.793224;
////  
////  
////  bl_wgs.B:=55.983276;
////  bl_wgs.L:=37.794912;
//
//  
//  bl_wgs.B:=55.98323736;
//  bl_wgs.L:=37.79511275;
//
//  
//  
//  bl:=geo_Pulkovo42_to_WGS84(bl_wgs);
//
//  bl_wgs_new:=geo_WGS84_to_Pulkovo42(bl);
//  
//  
////  bl_wgs.L:=37.794911;
////  bl_wgs.B:=55.983277;
//  
//    
//  obj:=TrlfMatrix.Create;
//  obj.OpenFile('d:\andr\Code_01.rlf');
//  obj.FindPointBL_WGS(bl_wgs, r);
//
////  xy:=geo_Pulkovo42_BL_to_XY(bl_wgs, obj.ZoneNum_GK);
//  
//  
//  
//  
//  
// // TrlfMatrix.Update_Earth_0_to_NULL('P:\__mts\Berdsk_5.rlf', 'P:\__mts\Berdsk_5_new.rlf');
//
////  TrlfMatrix.Update_Earth_0_to_NULL('P:\__mts\nnovg_pl.rlf', 'P:\__mts\nnovg_pl.rlf_new.rlf');
//  
//  
//end;
//
//



begin
  /////////// TrlfMatrix_Test





 // test;

 {
  obj:=TrlfMatrix.Create;
  obj.Test();

  obj.Free;

  }
end.


(*
// ---------------------------------------------------------------
procedure TRLF_to_UTM.Run;
// ---------------------------------------------------------------
var
  b: Boolean;
  I: Integer;

  header_Rec: TrelMatrixInfoRec;

  rXYRect_GK: TXYRect;
  xyRect_UTM: TXYRect;

  xy : TXYPoint;

  bl_CK42: TBLPoint;

  bl,bl1,bl2: TBLPoint;
  bTerminated: Boolean;

//  iGK_zone: integer;
  iColCount: integer;
  iRowCount: integer;

  iRow: integer;
  iCol: integer;
  iGK_ZoneNum: Integer;
  iStep: Integer;
  iUTM_ZoneNum: Integer;

  rlf_rec: Trel_Record;
//  iValue: integer;

  oRlfFileStream: TFileStream;
  oSrcRlfMatrix: TrlfMatrix;
  s: string;

  xy_GK: TXYPoint;
  xy_UTM: TXYPoint;

  xyXYPoints_GK: TXYPointArrayF;
  xyPoints_UTM: TXYPointArrayF;

 // wValue: Word;
  
begin
  oSrcRlfMatrix:=TrlfMatrix.Create;

  oSrcRlfMatrix.OpenFile(Params.FileName);

 // OpenFiles();

 // FFileName_DEM:=ed_DEM.FileName;

  Terminated := False;

 // btn_Run.Action := act_Stop;

//   Info.PixelSize



  if Terminated then
    Exit;


  //iStep :=round(FDEM.Cellsize);

//  Assert(iStep>0, 'Value <=0');

  iRowCount := oSrcRlfMatrix.RowCount;
  iColCount := oSrcRlfMatrix.ColCount;
  rXYRect_GK:= oSrcRlfMatrix.XYBounds;


  // ---------------------------------------------------------------

  iGK_ZoneNum:=oSrcRlfMatrix.ZoneNum;
  iUTM_ZoneNum :=iGK_ZoneNum + 30;


  geo_XYRectToXYPointsF (oSrcRlfMatrix.XYBounds, xyXYPoints_GK);


  xyPoints_UTM.Count :=4;


  // GK xy -> UTM xy
  for i:=0 to xyXYPoints_GK.Count-1 do
  begin
    xy_GK := xyXYPoints_GK.Items[i];
    xy_UTM := geo_GK_to_UTM(xy_GK, iGK_ZoneNum);

    xyPoints_UTM.Items[i] := xy_UTM;
  end;

  xyRect_UTM := geo_RoundXYPointsToXYRect(xyPoints_UTM);



  iStep := Round(oSrcRlfMatrix.StepX);

  iRowCount := Round(Abs(xyRect_UTM.TopLeft.X - xyRect_UTM.BottomRight.X) / iStep);
  iColCount := Round(Abs(xyRect_UTM.TopLeft.Y - xyRect_UTM.BottomRight.Y) / iStep);


  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=iColCount;
  header_Rec.RowCount:=iRowCount;

  header_Rec.StepX:=iStep;
  header_Rec.StepY:=iStep;

  header_Rec.XYBounds:=xyRect_UTM;

  header_Rec.ZoneNum := iUTM_ZoneNum;// iGK_ZoneNum + 30;
  header_Rec.Projection :='UTM';

////  iGK_zone := oSrcRlfMatrix.ZoneNum;

  // ---------------------------------------------------------------

  oRlfFileStream:=TFileStream.Create(Params.UTM_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  // ---------------------------------------------------------------


  for iRow := 0 to iRowCount - 1 do
  begin
    if Terminated then
      Break;

    DoOnProgress(iRow, iRowCount, bTerminated);


    for iCol := 0 to iColCount - 1 do
    begin
      if Terminated then  Break;

      xy_UTM.x := xyRect_UTM.TopLeft.X - iRow*iStep - iStep/2;
      xy_UTM.y := xyRect_UTM.TopLeft.Y + iCol*iStep + iStep/2;

      xy_GK := geo_UTM_to_GK(xy_UTM, iUTM_ZoneNum);


      if not oSrcRlfMatrix.FindPointXY(xy_GK, iGK_ZoneNum, rlf_rec) then
      begin
        FillChar(rlf_rec, SizeOf(rlf_rec), 0);
        rlf_rec.Rel_H := EMPTY_HEIGHT;
      end;

      FMemStream.Write(rlf_rec, SizeOf(rlf_rec));

    end;

    oRlfFileStream.CopyFrom(FMemStream, 0);
    FMemStream.Clear;

  end;

  FreeAndNil(oRlfFileStream);
end;







// ---------------------------------------------------------------
function TrelMatrixBase.GetPoints_WGS: TBLPointArray;
// ---------------------------------------------------------------
var
  I: Integer;
  xy_arr: TXYPointArray;
  bl_arr: TBLPointArray;
  bl: TBLPoint;
begin
  SetLength (Result, 4);

  case MatrixType of
    mtXY_: begin
              xy_arr := geo_XYRectToXYPoints_(XYBounds);

              for I := 0 to High(Result) do
                Result[i] := geo_GK_XY_to_WGS84_BL(xy_arr[i], ZoneNum_GK);

           end;

    mtLonLat_: begin
              assert(BLBounds.TopLeft.B<>0);

              bl_arr := geo_BLRectToBLPoints_polygone(BLBounds);

             // Result := bl_arr;

              for I := 0 to High(Result) do
                Result[i] := geo_Pulkovo42_to_WGS84(bl_arr[i]);

           end;

  end;


end;




// ---------------------------------------------------------------
class procedure TrlfMatrix.ExportToENVI__11111111111(aFileName, aDestFileName:
    string);
// ---------------------------------------------------------------
var
  b: Boolean;
  I: Integer;

//  header_Rec: TrelMatrixInfoRec;

//  rXYRect_GK: TXYRect;
//  xyRect_UTM: TXYRect;

//  xy : TXYPoint;

//  bl_CK42: TBLPoint;

//  bl,bl1,bl2: TBLPoint;
  //bTerminated: Boolean;

//  iGK_zone: integer;
  iColCount: integer;
  iRowCount: integer;

  iRow: integer;
  iCol: integer;
  iGK_ZoneNum: Integer;
  iStep: Integer;
  iUTM_ZoneNum: Integer;
  oFileStream: TFileStream;

  rlf_rec: Trel_Record;
//  iValue: integer;

  oRlfFileStream: TFileStream;
  oSrcRlfMatrix: TrlfMatrix;
  s: string;

  xy_GK: TXYPoint;
  xy_UTM: TXYPoint;

  xyXYPoints_GK: TXYPointArrayF;
  xyPoints_UTM: TXYPointArrayF;

 // wValue: Word;
  
begin
  oSrcRlfMatrix:=TrlfMatrix.Create;

  oSrcRlfMatrix.OpenFile(aFileName);

 // OpenFiles();

 // FFileName_DEM:=ed_DEM.FileName;


  //iStep :=round(FDEM.Cellsize);

//  Assert(iStep>0, 'Value <=0');

  iRowCount := oSrcRlfMatrix.RowCount;
  iColCount := oSrcRlfMatrix.ColCount;
  //rXYRect_GK:= oSrcRlfMatrix.XYBounds;


  // ---------------------------------------------------------------
//
//  iGK_ZoneNum:=oSrcRlfMatrix.ZoneNum;
////  iUTM_ZoneNum :=iGK_ZoneNum + 30;
//
//
//  geo_XYRectToXYPointsF (oSrcRlfMatrix.XYBounds, xyXYPoints_GK);
//
//
//  xyPoints_UTM.Count :=4;
//
//
//  // GK xy -> UTM xy
//  for i:=0 to xyXYPoints_GK.Count-1 do
//  begin
//    xy_GK := xyXYPoints_GK.Items[i];
//    xy_UTM := geo_GK_to_UTM(xy_GK, iGK_ZoneNum);
//
//    xyPoints_UTM.Items[i] := xy_UTM;
//  end;
//
//  xyRect_UTM := geo_RoundXYPointsToXYRect(xyPoints_UTM);
//


//  iStep := Round(oSrcRlfMatrix.StepX);

//  iRowCount := Round(Abs(xyRect_UTM.TopLeft.X - xyRect_UTM.BottomRight.X) / iStep);
//  iColCount := Round(Abs(xyRect_UTM.TopLeft.Y - xyRect_UTM.BottomRight.Y) / iStep);

//
//  // ---------------------------------------------------------------
//  FillChar (header_Rec, SizeOf(header_Rec), 0);
//
//  header_Rec.ColCount:=iColCount;
//  header_Rec.RowCount:=iRowCount;
//
//  header_Rec.StepX:=iStep;
//  header_Rec.StepY:=iStep;
//
//  header_Rec.XYBounds:=xyRect_UTM;
//
//  header_Rec.ZoneNum := iUTM_ZoneNum;// iGK_ZoneNum + 30;
//  header_Rec.Projection :='UTM';

////  iGK_zone := oSrcRlfMatrix.ZoneNum;

  // ---------------------------------------------------------------

  oFileStream:=TFileStream.Create(aFileName, fmCreate);

//  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  // ---------------------------------------------------------------


  for iRow := 0 to iRowCount - 1 do
  begin
//    if Terminated then
//      Break;

//    DoOnProgress(iRow, iRowCount, bTerminated);


    for iCol := 0 to iColCount - 1 do
    begin
//      Items
 //     if Terminated then  Break;

//      xy_UTM.x := xyRect_UTM.TopLeft.X - iRow*iStep - iStep/2;
//      xy_UTM.y := xyRect_UTM.TopLeft.Y + iCol*iStep + iStep/2;
//
//      xy_GK := geo_UTM_to_GK(xy_UTM, iUTM_ZoneNum);
//
//
//      if not oSrcRlfMatrix.FindPointXY(xy_GK, iGK_ZoneNum, rlf_rec) then
//      begin
//        FillChar(rlf_rec, SizeOf(rlf_rec), 0);
//        rlf_rec.Rel_H := EMPTY_HEIGHT;
//      end;

  //    oFileStream.Write(rlf_rec, SizeOf(rlf_rec));

    end;

//    oRlfFileStream.CopyFrom(FMemStream, 0);
//    FMemStream.Clear;

  end;

  FreeAndNil(oRlfFileStream);
end;
