unit u_rel_matrix_list;

interface

uses
  Classes,

  u_rel_Matrix_base;

type
  TRelMatrixList = class(TList)
  private
    function GetItem(Index:integer): TrelMatrixBase;
  public
    property Items[Index:integer]: TrelMatrixBase read GetItem; default;
  end;


implementation

function TRelMatrixList.GetItem(Index: integer): TrelMatrixBase;
begin
  Result:=TrelMatrixBase(Items[Index]);
end;

end.
