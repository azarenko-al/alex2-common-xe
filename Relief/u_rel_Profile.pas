unit u_rel_Profile;

interface
uses
  SysUtils, Variants, XMLIntf, DB, Graphics,  XMLDoc, Classes, IOUtils,

   u_xml_document,

   u_func_arrays,

   u_func,
   u_db,
   u_files,
   u_GEO,

   u_img,

   u_Log,

   u_clutters,
//   u_profile_types,

   I_rel_Matrix1
   ;


const
  MAX_REL_POINTS = 10000;


type

  //-------------------------------------------------
  TrelProfile = class
  //-------------------------------------------------
  private
(*    function GetType1(aAntennaH1, aAntennaH2, aFreq_GHz: double): TrelProfileType;
    function GetType_with_Los1(aAntennaH1, aAntennaH2, aFreq_GHz: double; var
        aLosValue: double): TrelProfileType;
*)
  private

//    procedure CalcSummary1;

    function GetCount: Integer;
    function GetItem(Index: Integer): TrelProfilePointRec;
    function GetItemEx(Index: Integer): TrelProfilePointRec;

    function GetItemEarthHeight(aIndex: integer): double;
    procedure DoOnCalcFields(DataSet: TDataSet);
    function GetDisplayItem(Index: Integer): TrelProfilePointRec;
    procedure SetItems(Index: Integer; const Value: TrelProfilePointRec);
    procedure UpdateItemFullHeight(aIndex: integer);
  protected

   // FDirection_Point2ToPoint1: boolean;
  public
    Refraction : double;     //1.33

   {
    Property1_id : Integer;
    Property2_id : Integer;
    Property1_name : string;
    Property2_name : string;
    }

//    IsDirectionReversed : Boolean; //Direction

//    IsRotated : Boolean;

    Clutters: TrelClutterModel;

    Summary:  record
                MaxClutterH,
                MinRelH, MaxRelH, MaxEarthH: integer;
              end;

    Data: TrelProfileRec;


    constructor Create;
    destructor Destroy; override;

    function LastItem: TrelProfilePointRec;
    function FirstItem: TrelProfilePointRec;

//    function ItemDistance_KM(aIndex: integer): Double;
    function ItemFullHeight(aIndex: word): double;

    function GetFullHeightByDistance (aDistance: double): double;
    function GetAveFullHeight(): double;

    procedure SetRefraction (aValue: double);

    procedure Clear;

    function GetItemIndexByDistance_KM(aDistance_KM: double): integer;

    procedure ApplyDefaultClutterHeights;

    procedure Assign (aSource: TrelProfile);

    function GeoDistance_KM: Double;

    procedure SaveToCSV(aFileName: String);
    function LoadFromCSV(aFileName: String): Boolean;

    procedure LoadFromXmlFile(aFileName: string);
    function LoadFromXml(aValue: string; aLength_km: double): Boolean;
    procedure SaveToXmlFile(aFileName: string);

    procedure SaveToDataset(aDataset: TDataset);
    procedure LoadFromDataset(aDataset: TDataset);

    function GetXML: string;

    function ItemEarthHeightByDistance(aDistanceKM: double): double;

    procedure Join(aSource: TrelProfile);

    property Count: Integer read GetCount;

//    function Export_to_JSON(aProfile: TrelProfile): string;
    function GetStep_km: double;
    function GetWaterAreaPercent: Double;

//    procedure ShowXmlFile;

    procedure Validate;

    property Items[Index: Integer]: TrelProfilePointRec read GetItem write SetItems;
    property ItemsEx [Index: Integer]: TrelProfilePointRec read GetItemEx;

    property DisplayItems[Index: Integer]: TrelProfilePointRec read GetDisplayItem;

  end;



function GetClutterColorByCode(aCode : Integer): Integer;
function GetClutterNameByCode(aCode : Integer): string;


const
  DEF_FOREST    = 1;  // 1- ��� (1)
  DEF_COUNTRY   = 3;  // 3- ���
  DEF_CITY      = 7;  // 7- �����
  DEF_ONE_BUILD = 73; // 73- ���.����
  DEF_ONE_HOUSE = 31; // 31- ���
  DEF_BOLOTO    = 8; // 31- ���


//==============================================================================
implementation
//==============================================================================
          


constructor TrelProfile.Create;
begin
  inherited;
  Clutters:=TrelClutterModel.Create;
end;

destructor TrelProfile.Destroy;
begin
  Clutters.Free;
  inherited;
end;


function TrelProfile.ItemFullHeight(aIndex: word): double;
begin
  with Data.Items[aIndex] do
    Result:=Rel_H + Clutter_h + Earth_H;
end;


procedure TrelProfile.UpdateItemFullHeight(aIndex: integer);
begin
  with Data.Items[aIndex] do
    Full_H:=Rel_H + Clutter_h + Earth_H;
end;


//-------------------------------------------------------------------
procedure TrelProfile.Join(aSource: TrelProfile);
//-------------------------------------------------------------------
// ��������� ����� !!!!
//-------------------------------------------------------------------

var
  iCount: integer;
  I: integer;
  eOffset_KM : Double;
begin
  iCount:=Data.Count;

  eOffset_KM := Data.Distance_KM;

//  Assert(eOffset_KM>0);

//  if Data.Items[Data.Count-1]
  Data.Distance_KM:=Data.Distance_KM + aSource.Data.Distance_KM;

  // ��������� �� 1 - �.�. ��������� ����� ���� � �� ��
  Data.Count :=Data.Count+ aSource.Data.Count -1;

  for I := 0 to aSource.Data.Count - 1-1 do
  begin
    // ������� aSource ����� ���� �������� !!!!
    // aSource.Data.Items[   ->>>> aSource.Items[
    Data.Items[iCount+i]:=aSource.Items[i+1];

    Data.Items[iCount+i].Distance_km:=Data.Items[iCount+i].Distance_km + eOffset_KM;
  end;


  Validate();
end;


//--------------------------------------------------------
procedure TrelProfile.Assign (aSource: TrelProfile);
//--------------------------------------------------------
begin
  Assert(Assigned(aSource), 'Value not assigned');

  Refraction:=aSource.Refraction; //  : double read FDistance; // [m]

  Data:=aSource.Data;

//  IsDirectionReversed:=aSource.IsDirectionReversed;

//  FDirection_Point2ToPoint1:=aSource.FDirection_Point2ToPoint1;

  Clutters.Assign (aSource.Clutters);
end;

//--------------------------------------------------------
function TrelProfile.GetFullHeightByDistance (aDistance: double): double;
//--------------------------------------------------------
var ind: integer;
begin
  ind:=GetItemIndexByDistance_KM (aDistance);
  
  if ind >= 0 then
    Result:=Data.Items[ind].Full_H
  else
    Result:=-1;

end;

//--------------------------------------------------------
function TrelProfile.GetItemIndexByDistance_KM(aDistance_KM: double): integer;
//--------------------------------------------------------
var i: integer;
begin
  for i:=0 to Count-1-1 do
    if (Data.Items[i].Distance_km<=aDistance_KM) and
       (aDistance_KM<=Data.Items[i+1].Distance_km)
    then begin
      Result:=i;
      Exit;
    end;

  Result:=-1;
end;

//--------------------------------------------------------------
procedure TrelProfile.SetRefraction (aValue: double);
//--------------------------------------------------------------
var i: integer;
begin
   Refraction:=aValue;

   for i:=0 to Count-1 do
   begin
     Data.Items[i].Earth_H := geo_GetEarthHeight (Data.Items[i].Distance_km, Data.Distance_km, Refraction);
     UpdateItemFullHeight(i);
   end;
end;

//--------------------------------------------------------------
procedure TrelProfile.ApplyDefaultClutterHeights;
//--------------------------------------------------------------
var i, iCode: integer;
begin
  for i:=0 to Count-1 do
    if (Data.Items[i].Clutter_Code > 0) and
       (Data.Items[i].Clutter_H = 0) then
    begin
      iCode:=Data.Items[i].Clutter_Code;

      Data.Items[i].Clutter_H:=Clutters.GetHeightByCode(iCode);

      UpdateItemFullHeight (i);
    end;
end;


// --------------------------------------------------------
function TrelProfile.GetAveFullHeight(): double;
// --------------------------------------------------------
// ����������� ������� ������ �������
var i: integer;  h: double;
begin
   h:= 0;
   for i:=0 to Count-1 do
     h:=h + Data.Items[i].Full_H ;

   if Count>0 then Result:=h/Count
              else Result:=0
end;


function TrelProfile.GetItemEarthHeight(aIndex: integer): double;
begin
  Result:=geo_GetEarthHeight
            (Data.Items[aIndex].Distance_KM, Data.Distance_KM, Refraction);
end;


function TrelProfile.LastItem: TrelProfilePointRec;
begin
  if Count>0 then
    Result:=Items[Count-1]
//    Result:=Data.Items[Count-1]
  else
    FillChar(Result, SizeOf(Result), 0);
end;


function TrelProfile.FirstItem: TrelProfilePointRec;
begin
  if Count>0 then
//    Result:=Data.Items[0]
    Result:=Items[0]
  else
    FillChar(Result, SizeOf(Result), 0);
end;



procedure TrelProfile.Clear;
begin
  Data.Count:=0;
  Data.Distance_KM :=0;
//  FillChar(Data,SizeOf(Data),0);
end;


function TrelProfile.GeoDistance_KM: Double;
begin
 // Result := geo_DistanceBLV(Data.BLVector) / 1000;
  Result := Data.Distance_KM;
  Result:=TruncFloat(Data.Distance_KM, 4);
end;


function TrelProfile.GetCount: Integer;
begin
  Result := Data.Count;
end;


//----------------------------------------------------------
procedure TrelProfile.LoadFromXmlFile(aFileName: string);
//----------------------------------------------------------
var s: string;
begin
//  IOUtils.TFile.

  s:=TxtFileToStr(aFileName);
  LoadFromXml(s,0);
end;



//----------------------------------------------------------
function TrelProfile.LoadFromXml(aValue: string; aLength_km: double): Boolean;
//----------------------------------------------------------
var i: integer;
  eItem_Distance_km: Double;
  vClutters,vGroup,vNode: IXMLNode;
  eStep,fDistance: double;
  iCount: Integer;

   oXMLDoc: TXmlDocumentEx;
begin
  Result:=False;

  if aValue='' then
    Exit;


  oXMLDoc:=TXmlDocumentEx.Create;

  oXMLDoc.LoadFromText (aValue);
  
//  oXMLDoc.XmlDocument.XML.Text := aValue;

//  oXMLDoc.LoadFromText(aValue);


//  StrToTxtFile(aValue, 'd:\profile.xml');
  // ---------------------------------------------------------------
  vClutters:=xml_FindNode (oXMLDoc.DocumentElement, 'Clutters');
  if Assigned(vClutters) then
  begin
    iCount :=vClutters.ChildNodes.Count;
    SetLength(Clutters.Items_new, iCount);

    for i:=0 to iCount-1 do
    begin
      vNode:=vClutters.ChildNodes[i];

      Clutters.Items_new[i].Code  := xml_GetIntAttr  (vNode, 'code');
      Clutters.Items_new[i].Height:= xml_GetIntAttr(vNode, 'Height');
      Clutters.Items_new[i].Name  := xml_GetStrAttr  (vNode, 'Name');
      Clutters.Items_new[i].Color := xml_GetIntAttr  (vNode, 'Color');

    end;
  end;         

  // ---------------------------------------------------------------
  vGroup:=xml_FindNode (oXMLDoc.DocumentElement, 'Items');
  if not Assigned(vGroup) then
    Exit;

  Refraction:= xml_GetFloatAttr(vGroup, 'Refraction');

  Data.Step_R_km   :=xml_GetFloatAttr (vGroup, 'step_km');

  Data.Distance_km :=xml_GetFloatAttr (vGroup, 'Distance_km');

  if aLength_km=0 then
    aLength_km:=Data.Distance_km;


(*  Data.BLVector.Point1.B:=xml_GetFloatAttr (vGroup, 'B1');
  Data.BLVector.Point1.L:=xml_GetFloatAttr (vGroup, 'L1');
  Data.BLVector.Point2.B:=xml_GetFloatAttr (vGroup, 'B2');
  Data.BLVector.Point2.L:=xml_GetFloatAttr (vGroup, 'L2');
*)

//  Property1_id:=xml_GetIntAttr (vGroup, 'Property1_id');
//  Property2_id:=xml_GetIntAttr (vGroup, 'Property2_id');
                     
  Data.Count:=0;

//  for i:=0 to vGroup.ChildNodes.Count-1 do
  for i:=0 to vGroup.ChildNodes.Count-1 do
  begin
//    vNode:=vGroup.ChildNodes[i];
    vNode:=vGroup.ChildNodes[i];

    eItem_Distance_km:=xml_GetFloatAttr (vNode, 'dist');

    if eItem_Distance_km<0 then
      eItem_Distance_km:=0;

    if (Data.Distance_km>0) and (eItem_Distance_km>Data.Distance_km) then
      eItem_Distance_km:=Data.Distance_km;

//       (Data.Items[i].Distance_km>Data.Distance_km) then

    if eItem_Distance_km > aLength_km  then
    begin
      Break;
      {
      FreeAndNil(oXMLDoc);
      Data.Count:=0;
      Result:=False;
      Exit;
      }
    end;


    Data.Items[i].Distance_km:=eItem_Distance_km;


    Data.Items[i].Rel_H        :=xml_GetIntAttr   (vNode, 'Rel_H');
    Data.Items[i].Clutter_H    :=xml_GetFloatAttr (vNode, 'Local_H');
    Data.Items[i].Clutter_Code :=xml_GetIntAttr   (vNode, 'Local_Code');

    Data.Items[i].Earth_H := GetItemEarthHeight(i);

    UpdateItemFullHeight(i);



    Inc(Data.Count);
  end;

  FreeAndNil(oXMLDoc);

//  if not Result then
//    Exit;


  if (Data.Distance_km=0) then
    Data.Distance_km:=eItem_Distance_km;

  Result:=(Data.Count>0)

end;

//--------------------------------------------------------------------
procedure TrelProfile.SaveToXmlFile(aFileName: string);
//--------------------------------------------------------------------
var
  s: string;
begin
  s:=GetXML();
  StrToTxtFile(s, ChangeFileExt(aFileName, '.xml') );
end;

//
////--------------------------------------------------------------------
//procedure TrelProfile.ShowXmlFile;
////--------------------------------------------------------------------
//var
//  s: string;
//begin
//  s:=GetXML();
//  ShellExec_Notepad_temp(s, '.xml');
//end;
//


//--------------------------------------------------------------------
function TrelProfile.GetXML: string;
//--------------------------------------------------------------------
var
  i: integer;
  vClutters,vGroup: IXMLNode;
  oXMLDoc: TXmlDocumentEx;
  eDistKM: double;
  iClutter_Code: Integer;
begin
  oXMLDoc:=TXmlDocumentEx.Create;


  vClutters:=oXMLDoc.DocumentElement.AddChild('clutters');

 // i := Clutters.Count;

  for i:=0 to High(Clutters.Items_new) do
  begin
   // Assert(Clutters.Items_new[i].Name <> '');
 

    xml_AddNode (vClutters, 'item',
         [
          'code',    Clutters.Items_new[i].Code,
          'height',  Clutters.Items_new[i].Height,
          'name',    Clutters.Items_new[i].Name,
          'color',   Clutters.Items_new[i].Color
         ]);
  end;

  vGroup:=xml_AddNode (oXMLDoc.DocumentElement, 'items',
           ['count', Count,

//            xml_Att('Distance',    GeoDistance_KM()),
            'distance_km', GeoDistance_KM(),

            'refraction', Refraction
      //      xml_Att('step_km',    Data.Step_R_km),
(*
            xml_Att('Property1_id', Property1_id),
            xml_Att('Property2_id', Property2_id),
            xml_Att('Property1_name', Property1_name),
            xml_Att('Property2_name', Property2_name)
*)


            ]);

  if Data.BLVector.Point1.B<>0 then
    xml_UpdateNode (vGroup,
           [
      //      xml_Att('step_km',    Data.Step_R_km),

//            xml_Att('Property1_id', Property1_id),
 //           xml_Att('Property2_id', Property2_id),

            xml_Att('lat1', Data.BLVector.Point1.B),
            xml_Att('lon1', Data.BLVector.Point1.L),
            xml_Att('lat2', Data.BLVector.Point2.B),
            xml_Att('lon2', Data.BLVector.Point2.L),

            xml_Att('x1', Data.XYVector.Point1.X),
            xml_Att('y1', Data.XYVector.Point1.Y),
            xml_Att('x2', Data.XYVector.Point2.X),
            xml_Att('y2', Data.XYVector.Point2.Y),


            xml_Att('b1', Data.BLVector.Point1.B),
            xml_Att('l1', Data.BLVector.Point1.L),
            xml_Att('b2', Data.BLVector.Point2.B),
            xml_Att('l2', Data.BLVector.Point2.L)

            ]);


  for i:=0 to Count-1 do
  begin
    eDistKM:=Round(Data.Items[i].Distance_km*1000) / 1000 ;
//    eDistKM:=eDistKM ;

    eDistKM:=TruncFloat(Data.Items[i].Distance_km,3);
    eDistKM:=TruncFloat(Data.Items[i].Distance_km,4);

    iClutter_Code:=Items[i].Clutter_Code;

    xml_AddNode (vGroup, 'item',
         [
          'dist',       eDistKM, //TruncFloat(Data.Items[i].Distance,3) ),
       //   xml_ATT ('dist_km',    eDistKM), //TruncFloat(Data.Items[i].Distance,3) ),
          'rel_h',      Data.Items[i].Rel_H,
          'earth_h',    Data.Items[i].earth_H,
          'local_h',    Data.Items[i].Clutter_H,
          'local_code', Data.Items[i].Clutter_Code,

          'clutter_h',    Data.Items[i].Clutter_H,
          'clutter_code', Data.Items[i].Clutter_Code,


          'distance_km', eDistKM,
          'ground_h',     Items[i].Rel_H ,

          'clutter_hex_color', ColorToHEX(GetClutterColorByCode(iClutter_Code)),
      //           xml_ATT ('clutter_color_HEX',GetClutterColorByCode(Data.Items[i].Clutter_Code)),
          'clutter_name', GetClutterNameByCode(iClutter_Code)
            
         ]);

(*
  with dmLink_report.relProfile do
     for i:=0 to Count-1 do
       xml_AddNode (vNode, TAG_ITEM,
          [//xml_ATT ('R',          TruncFloat(Data.Items[i].Distance,3) ),
           xml_ATT ('distance_km',   TruncFloat(Items[i].Distance_KM,3) ),
           xml_ATT ('ground_h',     Items[i].Rel_H ),
           xml_ATT ('clutter_h',    Items[i].Clutter_H ),
           xml_ATT ('clutter_code', Items[i].Clutter_Code),
           xml_ATT ('earth_h',      TruncFloat(Items[i].Earth_H)),

           xml_ATT ('clutter_color',ColorToHEX(GetClutterColorByCode(Items[i].Clutter_Code))),
//           xml_ATT ('clutter_color_HEX',GetClutterColorByCode(Data.Items[i].Clutter_Code)),
           xml_ATT ('clutter_name', GetClutterNameByCode(Items[i].Clutter_Code))
      ])*);  

  end;


//  Result := oXMLDoc.XMLText;

  Result := oXMLDoc.XmlDocument.XML.Text ;

//  oXMLDoc.Free;

  FreeAndNil(oXMLDoc);

end;

//----------------------------------------------------------
function TrelProfile.LoadFromCSV(aFileName: String): Boolean;
//----------------------------------------------------------
var
  oStrList: TStringList;
  s: string;

  arr: TStrArray;
  i: Integer;
  ind: Integer;
begin
  oStrList:=TStringList.Create;
  oStrList.LoadFromFile(aFileName);

//  oStrList.Add('dist_m;rel_h;clutter_h;clutter_code') ;

  Clear;

//  Data.

  for i:=1 to oStrList.Count-1 do
  begin
    arr := StringToStrArray(oStrList[i], ';');

    ind := Data.Count;

    Data.Items[ind].Distance_KM  := AsFloat(arr[0]) / 1000;
    Data.Items[ind].Rel_H        := AsInteger(arr[1]);
    Data.Items[ind].Clutter_H    := AsInteger(arr[2]);
    Data.Items[ind].Clutter_Code := AsInteger(arr[3]);

    Inc(Data.Count);

  end;

  if Data.Count>0 then
    Data.Distance_KM := Data.Items[Data.Count-1].Distance_KM;


  FreeAndNil(oStrList);

  Refraction  := 1.33;

end;


//--------------------------------------------------------------------
procedure TrelProfile.SaveToCSV(aFileName: String);
//--------------------------------------------------------------------
var
  i: integer;
  eDistKM: double;

  oStrList: TStringList;
  s: string;
begin
  oStrList:=TStringList.Create;

  oStrList.Add('dist_m;rel_h;clutter_h;clutter_code') ;

  for i:=0 to Count-1 do
  begin
    eDistKM:=Round(Data.Items[i].Distance_km*1000);

    s:= Format('%1.0f;%d;%1.1f;%d', [
          eDistKM,
          Data.Items[i].Rel_H,
          Data.Items[i].Clutter_H,
          Data.Items[i].Clutter_Code
          ]);

    oStrList.Add(s);
  end;


  oStrList.SaveToFile( ChangeFileExt(aFileName, '.csv') );

  FreeAndNil(oStrList);
end;


//-------------------------------------------------------------------
function TrelProfile.GetItem(Index: Integer): TrelProfilePointRec;
//-------------------------------------------------------------------
var
  r: TrelProfilePointRec;
begin
//  Result := Data.Items[Index];

//  if not IsDirectionReversed then
//  if not FDirection_Point2ToPoint1 then
    Result := Data.Items[Index];

//  else begin
//    r := Data.Items[Count-1-Index];
//    r.Distance_km:=Data.Distance_KM - r.Distance_km;
//
//    if r.Distance_km<0 then
//      r.Distance_km :=0;
//
//    Result:=r;
//  end;

  Assert(Result.Distance_km>=0, Format('Result.Distance_km <0: %1.5f', [Result.Distance_km]));

end;


//-------------------------------------------------------------------
function TrelProfile.GetItemEx(Index: Integer): TrelProfilePointRec;
//-------------------------------------------------------------------
begin
  Result:=Items[Index];

  if (Result.Clutter_Code > 0) and (Result.Clutter_H = 0) then
  begin
  //  iCode:=Result.Clutter_Code;

    Result.Clutter_H:=Clutters.GetHeightByCode(Result.Clutter_Code);
             
 //   UpdateItemFullHeight (i);
  end;

  Result.Earth_H := geo_GetEarthHeight (Result.Distance_km,
                       Data.Distance_km, Refraction);

  with Result do
    Full_H := Rel_H + Clutter_h + Earth_H;


//  Result := Data.Items[Index];


end;




//-------------------------------------------------------------------
function TrelProfile.GetDisplayItem(Index: Integer): TrelProfilePointRec;
//-------------------------------------------------------------------
begin
//  if not IsDirectionReversed then
//  if not FDirection_Point2ToPoint1 then
    Result := Data.Items[Index];

//  else begin
//    Result := Data.Items[Count-1-Index];
//    Result.Distance_km:=Data.Distance_KM - Result.Distance_km;
//  end;

  Assert(Result.Distance_km>=0, Format('Result.Distance_km <0: %1.5f', [Result.Distance_km]));
  
end;


// ---------------------------------------------------------------
procedure TrelProfile.SetItems(Index: Integer; const Value: TrelProfilePointRec);
// ---------------------------------------------------------------
var ind: Integer;
begin
//  if not IsDirectionReversed then
//  if not FDirection_Point2ToPoint1 then
    ind:=Index;
//  else
//    ind:=Count-1-Index;

  if ind>=0 then
  begin
    Data.Items[ind].Clutter_H:=Value.Clutter_H;
    Data.Items[ind].Clutter_Code:=Value.Clutter_Code;
    Data.Items[ind].Rel_H:=Value.Rel_H;
  end;

end;





function TrelProfile.ItemEarthHeightByDistance(aDistanceKM: double): double;
begin
  if aDistanceKM <= GeoDistance_KM() then
    Result:=geo_GetEarthHeight (aDistanceKM, GeoDistance_KM(), Refraction)
  else
    Result:=0;
end;


const
  FLD_INDEX     = 'INDEX';

  FLD_dist_M    = 'dist_M';
  FLD_earth_h   = 'earth_h';
  FLD_rel_h     = 'rel_h';
  FLD_clu_h     = 'clu_h';
  FLD_clu_code  = 'clu_code';
  FLD_CLU_color = 'clu_color';

//---------------------------------------------------
procedure TrelProfile.LoadFromDataset(aDataset: TDataset);
//---------------------------------------------------
var
  i,iCluCode:integer;
  bm: TBookmark;
  rec:  TrelProfilePointRec;
begin
  Assert(Data.Count=aDataset.RecordCount);

  with aDataset do
  begin
    bm:=GetBookmark;
    DisableControls;

    First;


    while not EOF do
    begin
      i:=FieldByName(FLD_INDEX).AsInteger;

      FillChar(rec, SizeOf(rec), 0);

      rec.Clutter_H   := FieldByName(FLD_clu_h).AsInteger;
      rec.rel_h       := FieldByName(FLD_rel_h).AsInteger;
      rec.Clutter_code:= FieldByName(FLD_clu_code).AsInteger;

      Items[i]:=rec;

//      Item

{//      Data.Items[i].Distance_KM :=(FieldByName(FLD_DIST_M).AsFloat / 1000);
      Items[i].Clutter_H   := FieldByName(FLD_clu_h).AsInteger;
      Items[i].rel_h       := FieldByName(FLD_rel_h).AsInteger;
      Items[i].Clutter_code:= FieldByName(FLD_clu_code).AsInteger;
}
      Next;
    end;

    GotoBookmark(bm);
    EnableControls;
  end;

end;


//--------------------------------------------------------------
function TrelProfile.GetWaterAreaPercent: Double;
//--------------------------------------------------------------
var i,iWaterCount: integer;
begin
  iWaterCount :=0;

  for i:=0 to Count-1 do
    if Data.Items[i].Clutter_Code = DEF_CLU_WATER then
      Inc(iWaterCount);

  if Count>0 then
    Result := (iWaterCount / Count) * 100
  else
    Result := 0 ;


  Result := TruncFloat(Result,1);

end;



//--------------------------------------------------------------
procedure TrelProfile.SaveToDataset(aDataset: TDataset);
//--------------------------------------------------------------
var
  i: Integer;
begin
 with aDataset do
  begin
    DisableControls;

    Close;
    Open;

    for i:=0 to Count-1 do
    begin
      db_AddRecord_ (aDataset,
        [
          FLD_INDEX,    i ,
          FLD_RECNO,    i , //+1
          FLD_dist_M,   Items[i].Distance_KM * 1000  ,
          FLD_earth_h,  Items[i].Earth_H,

          FLD_rel_h,    Items[i].Rel_H,
          FLD_clu_h,    Items[i].Clutter_H,
          FLD_clu_code, Items[i].Clutter_Code
        ]);
    end;

    First;
    EnableControls;

    OnCalcFields:=DoOnCalcFields ;
  end;
end;

//-------------------------------------------------------------------
procedure TrelProfile.DoOnCalcFields(DataSet: TDataSet);
//-------------------------------------------------------------------
var
  eDistKM: double;
  iCode: integer;
  iColor : Integer;
begin
  iCode  :=DataSet.FieldByName(FLD_Clu_code).AsInteger;
  eDistKM:=DataSet.FieldByName(FLD_DIST_M).AsFloat / 1000;

  if Clutters.GetColorByCode(iCode, iColor) then
    DataSet[FLD_clu_color]:=iColor;

//  DataSet[FLD_clu_color]:= Clutters.GetColorByCode(iCode);

  DataSet[FLD_EARTH_H]  := ItemEarthHeightByDistance (eDistKM);

//  Clutters.GetColorByCode(iCode);

 // DataSet[FLD_N]:= DataSet.recno;

end;


// ---------------------------------------------------------------
function TrelProfile.GetStep_km: double;
// ---------------------------------------------------------------
begin
  Result := Data.Step_R_km;

  if Result=0 then
    if (Count>1) then
      Result :=Items[1].Distance_KM- Items[0].Distance_KM
    else
      Result :=0;
end;

//-------------------------------------------------------------------
procedure TrelProfile.Validate;
//-------------------------------------------------------------------
var
  I: Integer;
begin
(*  if Count>10 then
    Assert(Data.Items[Count div 2].Earth_H>0);
*)

  for I := 0 to Data.Count - 2 do    // Iterate
    Assert(Data.Items[i].Distance_KM < Data.Items[i+1].Distance_KM);
end;

// ---------------------------------------------------------------
function GetClutterNameByCode(aCode : Integer): string;
// ---------------------------------------------------------------
begin
  case aCode of    //
    DEF_CLU_OPEN_AREA : Result := '�������� ���������';
    DEF_CLU_FOREST    : Result := '���';  // 1- ��� (1)
    DEF_CLU_WATER     : Result := '����';  // 3- ���
    DEF_CLU_COUNTRY   : Result := '���.�����';  // 3- ���  -DEF_COUNTRY
    DEF_CLU_CITY      : Result := '�����'; // 7- �����
    DEF_CLU_ONE_BUILD : Result := '�������� ������� ������'; // 73- ���.����
    DEF_CLU_ONE_HOUSE : Result := '��������� ���'; // 31- ���

    DEF_CLU_ROAD      : Result :='������';  //4
  else
    Result := '';
  end;    // case

end;


// ---------------------------------------------------------------
function GetClutterColorByCode(aCode : Integer): integer;
// ---------------------------------------------------------------
begin
  case aCode of    //
    DEF_CLU_OPEN_AREA : Result := clWhite;
    DEF_CLU_FOREST    : Result := clGreen;  // 1- ��� (1)
    DEF_CLU_WATER     : Result := clBlue;  // 2- ���
    DEF_CLU_COUNTRY   : Result := 10789024;  // 3- ���  -DEF_COUNTRY
    DEF_CLU_ROAD      : Result :=clYellow;  //4
    DEF_CLU_CITY      : Result := clRed; // 7- �����
    DEF_CLU_ONE_BUILD : Result := 128; // 73- ���.����
    DEF_CLU_ONE_HOUSE : Result := 4210816; // 31- ���

  else
    Result := 0;
  end;    // case

end;



end.

