unit u_class_manager;

interface
uses
  Classes,Dialogs;


type
  TClassManager = class(TObject)
  private
    FDataModuleList: TList;
  public
    constructor Create;
    procedure FreeDatamodules;
    procedure RegisterDataModule(aDatamodule: TDataModule);
    procedure ShowDatamodules;
    procedure UnRegisterDatamodule(aDataModule: TDataModule);
  end;

(*var
  g_ClassManager: TClassManager;
*)
implementation


constructor TClassManager.Create;
begin
  inherited;
  FDataModuleList:=TList.Create;
end;

procedure TClassManager.FreeDatamodules;
var
  I: Integer;
begin
  while FDataModuleList.Count>0 do
    TDataModule(FDataModuleList[0]).Free;

  FDataModuleList.Clear;

  //  TDataModule(FDataModuleList[i]).Free;
  //FDataModuleList.Free;
end;

procedure TClassManager.RegisterDataModule(aDatamodule: TDataModule);
begin
 // ShowMessage(aDatamodule.ClassName);

  if FDataModuleList.IndexOf(aDatamodule)<0 then
    FDataModuleList.Add(aDatamodule);
end;

procedure TClassManager.ShowDatamodules;
var
  I: Integer;
  oSList: TStringList;
begin
  oSList := TStringList.Create();

  for I := 0 to FDataModuleList.Count - 1 do
    oSList.Add(TDataModule(FDataModuleList[i]).Name + '  -  ');

  ShowMessage(oSList.Text);

  oSList.Free;
end;

procedure TClassManager.UnRegisterDatamodule(aDataModule: TDataModule);
var
  ind: Integer;
begin
//  ind:=FDataModuleList.Count;

  ind:=FDataModuleList.IndexOf(aDatamodule);
  if ind>=0 then
    FDataModuleList.Remove(aDatamodule);

 // ind:=FDataModuleList.Count;


 // if LClassList.IndexOf(aDataModule)<0 then
  //  LClassList.Add(aDataModule);
end;



//begin
 // g_ClassManager:=TClassManager.Create;

(*initialization
//  g_ClassManager:=TClassManager.Create;
finalization*)

{  if LDataModuleList.Count>0 then
    ShowMessage('u_object_manager - LDataModuleList.Count>0');
}

  //FreeAndNil(g_ClassManager);

end.
