unit u_storage;

interface

uses
  SysUtils, cxGridCustomView, cxGridDBTableView,
           
  cxTL, cxDBTL,

  //cxGridDBTreeList,

  cxGridDBBandedTableView

    ;

type
  TStorage = class(TObject)
  private
   FRegPath: string;
   procedure SetRegPath(const Value: string);
  public

    function GetPathByClass(aClassName: string): string;

    procedure RestoreFromRegistry(aObject: TcxGridDBBandedTableView; aClassName:
        string); overload;

    procedure RestoreFromRegistry(aObject: TcxGridDBTableView; aClassName: string);
        overload;
    procedure RestoreFromRegistry(aObject: TcxDBTreeList; aClassName: string);
       overload;

    // ---------------------------------------------------------------
    procedure StoreToRegistry(aObject: TcxGridDBBandedTableView; aClassName:
        string; aIsFullInfo: boolean = False); overload;

    procedure StoreToRegistry(aObject: TcxDBTreeList; aClassName: string;
        aIsFullInfo: boolean = False); overload;


    procedure StoreToRegistry(aObject: TcxGridDBTableView; aClassName: string);
        overload;

    property RegPath: string read FRegPath write SetRegPath;

//  cxGrid1DBBandedTableView1.RestoreFromRegistry(FRegPath + cxGrid1DBBandedTableView1.Name);

 //   const
//  REGISTRY_COMMON_FORMS = 'Software\Onega\Common\Forms\';

  end;

var
  g_Storage: TStorage;

implementation

function TStorage.GetPathByClass(aClassName: string): string;
begin
  Result := FRegPath + aClassName +'\';
end;

// ---------------------------------------------------------------
procedure TStorage.RestoreFromRegistry(aObject: TcxGridDBBandedTableView;   aClassName: string);
// ---------------------------------------------------------------
var
  s: string;
begin
//  Assert(FRegPath<>'');
  s:=FRegPath + aClassName +'\'+ aObject.Name;

  aObject.RestoreFromRegistry(s);
end;



// ---------------------------------------------------------------
procedure TStorage.RestoreFromRegistry(aObject: TcxDBTreeList; aClassName:    string);
// ---------------------------------------------------------------
var
  s: string;
begin
//  Assert(FRegPath<>'');
  s:=FRegPath + aClassName +'\'+ aObject.Name;

  aObject.RestoreFromRegistry(s);
end;



// ---------------------------------------------------------------
procedure TStorage.RestoreFromRegistry(aObject: TcxGridDBTableView; aClassName:  string);
// ---------------------------------------------------------------
var
  s: string;
begin
 // Assert(FRegPath<>'');
  s:=FRegPath + aClassName +'\'+ aObject.Name;
  aObject.RestoreFromRegistry(s);
end;

procedure TStorage.SetRegPath(const Value: string);
begin
  FRegPath := IncludeTrailingBackslash(Value);
end;

// ---------------------------------------------------------------
procedure TStorage.StoreToRegistry(aObject: TcxGridDBBandedTableView;
    aClassName: string; aIsFullInfo: boolean = False);
// ---------------------------------------------------------------
var
  s: string;
begin
//  Assert(FRegPath<>'');

  s:=FRegPath + aClassName +'\' + aObject.Name;

  if aIsFullInfo then
    aObject.StoreToRegistry(s, True,  [gsoUseFilter])
  else
    aObject.StoreToRegistry(s);

end;

// ---------------------------------------------------------------
procedure TStorage.StoreToRegistry(aObject: TcxGridDBTableView; aClassName:

    string);
// ---------------------------------------------------------------
var
  s: string;
begin
 // Assert(FRegPath<>'');
  s:=FRegPath + aClassName +'\' + aObject.Name;
  aObject.StoreToRegistry(s);
end;

procedure TStorage.StoreToRegistry(aObject: TcxDBTreeList; aClassName: string;
    aIsFullInfo: boolean = False);
var
  s: string;
begin
  s:=FRegPath + aClassName +'\' + aObject.Name;
  aObject.StoreToRegistry(s);
end;



initialization
  g_Storage:=TStorage.Create;
  g_Storage.RegPath := 'Software\Onega\Common\Forms';

finalization
  FreeAndNil(g_Storage);
  
end.
