unit u_run;

interface
uses
  Windows,SysUtils,  vcl.Forms, vcl.Dialogs,
CodeSiteLogging,

 // u_func,

  Winapi.ShellAPI
  ;

procedure RunAppEx(aFileName: string; aParams: array of string);

//  function RunApp (aFileName, aParamStr: string): boolean; // export;
//  function RunAppEx(aFileName: string; aParams: array of string): boolean;

//function RunApp11111111111111(aFileName, aParamStr: string): boolean;

function RunApp(const aFileName: String; const aParamStr: String = ''): DWORD;

function RunAndWait_2(aExeNameAndParams: string; ncmdShow: Integer = SW_SHOWNORMAL): Integer;

function ShellExecute_AndWait(aFileName, aParams: string): bool;

procedure ShellExec(aFileName: string; aFileParams: string = '');

function RunApp_hidden(const aFileName: String; const aParamStr: String = ''):
    DWORD;


//procedure ExecuteAndWait(const aCommando: string);


implementation

//---------------------------------------------------------------------------
procedure ShellExec(aFileName: string; aFileParams: string = '');
//---------------------------------------------------------------------------
var
  r: Integer;
begin
  Assert(aFileName<>'');
 // Assert(FileExists( aFileName));

//  if aFileName<>'' then

//function ShellExecute(hWnd: HWnd; Operation, FileName, Parameters, Directory: PChar; ShowCmd: Integer): HINST; stdcall;

  r := ShellExecute(0,nil, pChar(string(aFileName)), pChar(string(aFileParams)), nil, SW_NORMAL);
  r:=0;

//  ShellExecute(0,nil, pChar(aFileName), pChar(aFileParams), nil, SW_NORMAL);
end;


// ---------------------------------------------------------------
function ShellExecute_AndWait(aFileName, aParams: string): bool;
// ---------------------------------------------------------------
var
  exInfo: TShellExecuteInfo;
  Ph: DWORD;
begin

  FillChar(exInfo, SizeOf(exInfo), 0);
  
  exInfo.fMask := SEE_MASK_NOCLOSEPROCESS or SEE_MASK_FLAG_DDEWAIT;
//  exInfo.fMask := SEE_MASK_FLAG_DDEWAIT;
  
  with exInfo do
  begin
    cbSize := SizeOf(exInfo);
//    fMask := SEE_MASK_NOCLOSEPROCESS or SEE_MASK_FLAG_DDEWAIT;
    


    Wnd := GetActiveWindow();
    exInfo.lpVerb := 'open';
    exInfo.lpParameters := PChar(aParams);
    lpFile := PChar(aFileName);
    nShow := SW_SHOWNORMAL;
  end;
  
  if ShellExecuteEx(@exInfo) then
    Ph := exInfo.hProcess
  else
  begin
    ShowMessage(SysErrorMessage(GetLastError));
    Result := true;
    exit;
  end;
  
  while WaitForSingleObject(exInfo.hProcess, 50) <> WAIT_OBJECT_0 do
    Application.ProcessMessages;
    
  CloseHandle(Ph);

  Result := true;

end;



// ---------------------------------------------------------------
function RunApp(const aFileName: String; const aParamStr: String = ''): DWORD;
// ---------------------------------------------------------------
var 
  StartupInfo: TStartupInfo; 
  ProcessInfo: TProcessInformation;    
  sParams: string;     
begin 
  CodeSite.Send('function RunApp(  '+ aFileName);

  Assert (FileExists(aFileName), aFileName);

  sParams:=aFileName+ ' ' + aParamStr;
                             
  GetStartupInfo(StartupInfo); 

   
//  if not CreateProcess(nil, PChar(sParams), nil, nil,  False, 
//                       CREATE_NO_WINDOW, nil, nil, StartupInfo, ProcessInfo) then

  if not CreateProcess(nil, PChar(sParams), nil, nil,  IsConsole, 
                       NORMAL_PRIORITY_CLASS, nil, nil, StartupInfo, ProcessInfo) 
  then
      RaiseLastWin32Error;

  try 
    if WaitForSingleObject(ProcessInfo.hProcess, INFINITE) = WAIT_OBJECT_0 
  then 
      GetExitCodeProcess(ProcessInfo.hProcess, Result); 
  finally 
    CloseHandle(ProcessInfo.hThread); 
    CloseHandle(ProcessInfo.hProcess); 
  end; 
  
end; 


// ---------------------------------------------------------------
function RunApp_hidden(const aFileName: String; const aParamStr: String = ''):  DWORD;
// ---------------------------------------------------------------
var 
  StartupInfo: TStartupInfo; 
  ProcessInfo: TProcessInformation;    
  sParams: string;     
begin 
  CodeSite.Send('function RunApp(  '+ aFileName);

  Assert (FileExists(aFileName), aFileName);

  sParams:=aFileName+ ' ' + aParamStr;
                             
  GetStartupInfo(StartupInfo); 

   
  if not CreateProcess(nil, PChar(sParams), nil, nil,  False, 
                       CREATE_NO_WINDOW, nil, nil, StartupInfo, ProcessInfo) 

//  if not CreateProcess(nil, PChar(sParams), nil, nil,  IsConsole, 
//                       NORMAL_PRIORITY_CLASS, nil, nil, StartupInfo, ProcessInfo) 
  then
      RaiseLastWin32Error; 

  try 
    if WaitForSingleObject(ProcessInfo.hProcess, INFINITE) = WAIT_OBJECT_0 
  then 
      GetExitCodeProcess(ProcessInfo.hProcess, Result); 
  finally 
    CloseHandle(ProcessInfo.hThread); 
    CloseHandle(ProcessInfo.hProcess); 
  end; 
  
end; 


// ---------------------------------------------------------------
function RunAndWait_2(aExeNameAndParams: string; ncmdShow: Integer =  SW_SHOWNORMAL): Integer;
// ---------------------------------------------------------------
var
  b: Boolean;
  StartupInfo: TStartupInfo;
  ProcessInformation: TProcessInformation;
  Res: Bool;
  lpExitCode: DWORD;
  
begin
  with StartupInfo do //you can play with this structure
  begin
    cb := SizeOf(TStartupInfo);
    lpReserved := nil;
    lpDesktop := nil;
    lpTitle := nil;
    dwFlags := STARTF_USESHOWWINDOW;
    wShowWindow := ncmdShow;
    cbReserved2 := 0;
    lpReserved2 := nil;
  end;
  
  Res := CreateProcess(nil, PChar(aExeNameAndParams), nil, nil, True,
      CREATE_DEFAULT_ERROR_MODE
      or NORMAL_PRIORITY_CLASS, nil, nil, StartupInfo, ProcessInformation);
      
  while True do
  begin
      b:=GetExitCodeProcess(ProcessInformation.hProcess, lpExitCode);

      if lpExitCode = 1 then
      begin
        Sleep (1000);
        Application.ProcessMessages;
        Continue;
        
      end;

      if lpExitCode <> STILL_ACTIVE then
          Break;

//

      Application.ProcessMessages;
  end;

  Result := Integer(lpExitCode);

end;


function DoubleQuotedStr(aValue: String): String;
Begin
  Result:='"'+aValue+'"';
end;


// ---------------------------------------------------------------
procedure RunAppEx(aFileName: string; aParams: array of string);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  s:='';

  for I := 0 to High(aParams) do
    s:=s+ DoubleQuotedStr(aParams[i]) + ' ';

  RunApp (aFileName, s);

end;



end.

