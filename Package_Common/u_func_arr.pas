unit u_func_arr;

interface
uses
  SysUtils, Classes, Variants,

  u_func;
 

  // ������ ����� - ���������, ��������� - ���������
//  function FirstUpperCase (aValue: string): string;


  //---------------------------------------------------
  // Array functions
  //---------------------------------------------------
  function  FindInIntArray  (Value: integer; Arr: TIntArray): integer; // index of integer

  function  FindInStrArray  (Value: string;  Arr: TStrArray): integer; // index of integer

// TODO: FindInDoubleArray
 function  FindInDoubleArray    (Value: double;  Arr: TDoubleArray): integer; // index of integer

  procedure IntArrayJoinValues   (var Arr: TIntArray; Values: TIntArray);

  function  IntArrayMinRaznos    (aArr: TIntArray): integer;

  function  IntArrayMaxRaznos    (aArr: TIntArray): integer;

  procedure IntArrayJoinValuesLimit (var Arr: TIntArray; Values: TIntArray; ArrLimit : TIntArray);

  procedure IntArrayRemoveValue  (var Arr: TIntArray; Value: integer);

  procedure IntArrayRemoveValues (var Arr: TIntArray; Values: TIntArray);

  procedure IntArraySort         (var aArr: TIntArray);

  function  IntArrayToString     (Arr: TIntArray; Delimiter: string='; '): string;

// TODO: IntArrayIncrease
//procedure IntArrayIncrease     (var Arr: TIntArray; Value: integer);

  procedure IntArrayAddValues    (var Arr: TIntArray; Values: array of integer);

  procedure IntArrayAddValue     (var Arr: TIntArray; Value: integer);

//   function  IntArrayToStringSimple (aArr: TIntArray; aDelimiter: string=','): string;
  function IntArrayToStringSimple (aArr: TIntArray; aDelimiter: string=',';
                                   aEndDelimiter: boolean=False): string;
  procedure AddToIntArray        (Value: integer; var IntArr: TIntArray);
  function  DoubleArray_MaxValue (aArr: TDoubleArray): double;
  function  DoubleArray_MinValue (aArr: TDoubleArray): double;
  function  CompareIntArrays     (Values1,Values2: TIntArray): boolean;

  //---------------------------------------------------
  // StrArray
  //---------------------------------------------------
  procedure StrArrayAddValue    (var Arr: TStrArray; Value: string);

  function StrArrayIndexOf      (Arr: TStrArray; Value: string): integer;

  function StrArrayToString     (Arr: TStrArray; Delimiter: string='; '): string;

  procedure AddValueToStrArray  (aValue: string; var Arr: TStrArray);

  function StringToDoubleArray(Value: string; Delimiter: Char = ';'):
      TDoubleArray;

  function StringToIntArray(Value: string; Delimiter: Char = ';'): TIntArray;

  function StringToIntArraySimple(Value: string; Delimiter: Char=';'): TIntArray;

  function StringToStrArray(aValue: string; aDelimiter: Char = ';'): TStrArray;

  procedure DoubleArraySort      (var aArr: TDoubleArray);

  function  DoubleArrayToString  (Arr: TDoubleArray; Delimiter: string=';'): string;

  function  GetMaxFromIntArray   (IntArr: TintArray): integer;

  function  MakeIntArray          (aValues: array of integer): TIntArray;

  //---------------------------------------------------
  // IntArray
  //---------------------------------------------------
  function  ValueInIntArray      (Value: integer; IntArr: TIntArray): boolean;

  procedure AddToDoubleArray     (aValue: double; var DoubleArr: TDoubleArray);

  //---------------------------------------------------
  // DoubleArray
  //---------------------------------------------------

  procedure DoubleArrayAddValue  (var aArr: TDoubleArray; Value: double);


implementation

//--------------------------------------------------------
// VAR array routines ------------------------------------
//--------------------------------------------------------

//--------------------------------------------------------
function FindInIntArray (Value: integer; Arr: TIntArray): integer; // index of integer
//--------------------------------------------------------
var i:integer;
begin
  Result:=-1;
  for i:=0 to High(Arr) do
    if Arr[i]=Value then begin Result:=i; Exit; end;
end;

function FindInStrArray (Value: string; Arr: TStrArray): integer; // index of integer
var i:integer;
begin
  Result:=-1;
  for i:=0 to High(Arr) do if Eq(Arr[i],Value) then begin Result:=i; Exit; end;
end;

// TODO: IntArrayIncrease
//// TODO: FindInDoubleArray
//--------------------------------------------------------
function FindInDoubleArray (Value: double; Arr: TDoubleArray): integer; // index of integer
////--------------------------------------------------------
var i:integer;
begin
  Result:=-1;
  for i:=0 to High(Arr) do if Arr[i]=Value then begin Result:=i; Exit; end;
end;
//
//procedure IntArrayIncrease (var Arr: TIntArray; Value: integer);
//var i:integer;
//begin
//for i:=0 to High(Arr) do Arr[i]:=Arr[i]+Value;
//end;

//--------------------------------------------------------
function IntArrayToString (Arr: TIntArray; Delimiter: string='; '): string;
//--------------------------------------------------------
var i,Len,iValue,iCount: integer;

    procedure AppendRange (MinValue,MaxValue: integer);
    begin
      if MinValue = MaxValue-1 then begin AppendRange(MinValue,MinValue); AppendRange(MaxValue,MaxValue); end else
      if MinValue < MaxValue   then Result:=Result + Format('%d-%d', [MinValue, MaxValue]) + Delimiter
                               else Result:=Result + Format('%d',    [MinValue]) + Delimiter;
      iCount:=0;
    end;
begin // ������ ����������� �������� �������� : 1-23;33
 // IntArraySort(Arr);
  Result:=''; Len:=High(Arr);

  for i:=0 to Len do
  begin
    if (i=0) then begin iValue:=Arr[i]; iCount:=1; end
      else
    if (i>0) then begin
      if (Arr[i] = Arr[i-1]+1) then Inc(iCount) else
      if (Arr[i]<> Arr[i-1]+1) then begin
                                    AppendRange (iValue, iValue+iCount-1);
                                    iValue:=Arr[i]; iCount:=1;
                                 end;
    end;

    if (i=Len) then AppendRange (iValue, iValue+iCount-1);
  end;

  // remove last ';'
  if Length(Result) > Length(Delimiter)
    then Result:=Copy(Result,1,Length(Result)-Length(Delimiter));
end;

procedure IntArrayJoinValues (var Arr: TIntArray; Values: TIntArray);
var i:integer;
begin
  for i:=0 to High(Values) do
    AddToIntArray (Values[i], Arr);
end;

//-------------------------------------------------------------------
procedure IntArraySort (var aArr: TIntArray);
//-------------------------------------------------------------------
var i,v: integer; modified: boolean;// ���������� �� �����������
begin
  repeat
    modified:=false;
    for i:=0 to High(aArr)-1 do
      if aArr[i] > aArr[i+1] then
        begin v:=aArr[i+1]; aArr[i+1]:=aArr[i]; aArr[i]:=v; modified:=true; end;
  until
    (modified=false);
end;

procedure IntArrayRemoveValue (var Arr: TIntArray; Value: integer);
var ind,i:integer;
begin
  repeat
    ind:=FindInIntArray (Value,Arr);
    if (ind>=0) then begin // �������� �������� �����
      for i:=ind+1 to High(Arr) do Arr[i-1]:=Arr[i];
      SetLength (Arr,High(Arr));
    end;
  until (ind<0);
end;

procedure IntArrayRemoveValues (var Arr: TIntArray; Values: TIntArray);
var i:integer;
begin
  for i:=0 to High(Values) do  IntArrayRemoveValue (Arr, Values[i]);
end;

function IntArrayMinRaznos (aArr: TIntArray): integer;
var i,d:integer;
   intArr: TIntArray;
begin
  if High(aArr) <=0 then begin Result:=-1; Exit; end;

  SetLength (intArr, High(aArr)+1);
  for i:=0 to High(intArr) do  intArr[i]:=aArr[i];

  IntArraySort (intArr);

  Result:=0;
  for i:=0 to High(intArr)-1 do begin
    d:=intArr[i+1]-intArr[i];
    if (Result=0) or ((d>0) and (d<Result)) then Result:=d;
  end;

  if Result>0 then Dec(Result);
end;

procedure IntArrayJoinValuesLimit (var Arr: TIntArray; Values: TIntArray; ArrLimit : TIntArray); //lina-function
var i:integer;
begin
  for i:=0 to High(Values) do
    if ValueInIntArray(Values[i], arrLimit) then AddToIntArray (Values[i], Arr);
end;

//--------------------------------------------------------
function IntArrayMaxRaznos (aArr: TIntArray): integer;
//--------------------------------------------------------
var i,d:integer;
   intArr: TIntArray;
begin
  if High(aArr) <=0 then begin Result:=-1; Exit; end;

  SetLength (intArr, High(aArr)+1);
  for i:=0 to High(intArr) do  intArr[i]:=aArr[i];

  IntArraySort (intArr);

  Result:=0;
  for i:=0 to High(intArr)-1 do begin
    d:=intArr[i+1]-intArr[i];
    if (Result=0) or ((d>0) and (d>Result)) then Result:=d;
  end;

  if Result>0 then Dec(Result);
end;

//--------------------------------------------------------
function IntArrayToStringSimple (aArr: TIntArray; aDelimiter: string=',';
                                          aEndDelimiter: boolean=False): string;
//--------------------------------------------------------
var i: integer;
begin
  Result:='';

  for i:=0 to High(aArr) do
  begin
    Result:=Result + IntToStr(aArr[i]);

    if i<>High(aArr) then
      Result:=Result + aDelimiter
    else
    if aEndDelimiter then
      Result:=Result + aDelimiter;
  end;
end;

procedure IntArrayAddValues (var Arr: TIntArray; Values: array of integer);
var i:integer;
begin
  for i:=0 to High(Values) do
    AddToIntArray (Values[i], Arr);
end;

procedure IntArrayAddValue (var Arr: TIntArray; Value: integer);
begin
  AddToIntArray (Value, Arr);
end;

function StrArrayToString (Arr: TStrArray; Delimiter: string='; '): string;
var i:integer;
begin
  Result:='';
  for i:=0 to High(Arr) do begin
    Result:=Result + Arr[i];
    if i<High(Arr) then Result:=Result + Delimiter;
  end;
end;

procedure StrArrayAddValue (var Arr: TStrArray; Value: string);
begin
   SetLength (Arr, High(Arr)+1+1);
   Arr [High(Arr)]:=Value;
end;

function StrArrayIndexOf (Arr: TStrArray; Value: string): integer;
var i:integer;
begin
  for i:=0 to High(Arr) do
    if Eq(Arr[i],Value) then begin Result:=i; Exit; end;
  Result:=-1;
end;

function DoubleArray_MaxValue (aArr: TDoubleArray): double;
var i: integer;
begin
  if Length(aArr)=0 then begin Result:=0; Exit; end;

  Result:=aArr[0];
  for i:=1 to High(aArr) do
    if aArr[i]>Result then Result:=aArr[i];
end;

function DoubleArray_MinValue (aArr: TDoubleArray): double;
var i: integer;
begin
  if Length(aArr)=0 then begin Result:=0; Exit; end;

  Result:=aArr[0];
  for i:=1 to High(aArr) do
    if aArr[i]<Result then Result:=aArr[i];
end;

//--------------------------------------------------------
function CompareIntArrays (Values1,Values2: TIntArray): boolean;
//--------------------------------------------------------
var i: integer;
begin
  if High(Values1) <> High(Values2) then begin Result:=false; Exit; end;

  IntArraySort (Values1);
  IntArraySort (Values2);

  for i:=0 to High(Values1) do
     if Values1[i] <> Values2[i] then begin Result:=false; Exit; end;
  Result:=true;
end;

procedure AddValueToStrArray(aValue: string; var Arr: TStrArray);
begin
  SetLength (Arr, High(Arr)+2);
  Arr [High(Arr)]:=aValue;
end;

//--------------------------------------------------------
procedure AddToIntArray (Value: integer; var IntArr: TIntArray);
//--------------------------------------------------------
begin
  if (FindInIntArray (Value,IntArr)<0) then begin
    SetLength (IntArr, High(IntArr)+2);
    IntArr [High(IntArr)]:=Value;
  end;
end;

//--------------------------------------------------------
function StringToIntArray(Value: string; Delimiter: Char = ';'): TIntArray;
//--------------------------------------------------------
var i,iValue:integer; arr,arr_:TStrArray;
begin
  try
  arr:=StringToStrArray (Value,Delimiter);

  SetLength(Result,0);
  for i:=0 to High(arr) do
    if Pos('-', arr[i]) <= 1 then
      AddToIntArray (AsInteger(arr[i]), Result)
    else begin
      arr_:=StringToStrArray (arr[i],'-');
      if High(arr_)+1=2 then begin
        for iValue:=AsInteger(arr_[0]) to AsInteger(arr_[1]) do
          AddToIntArray (iValue, Result);
      end;
    end;
  finally
  end;

  IntArrayRemoveValue (Result, 0);

end;

//--------------------------------------------------------
function StringToDoubleArray(Value: string; Delimiter: Char = ';'):
    TDoubleArray;
//--------------------------------------------------------
var i:integer; strArr:TStrArray;
begin
  strArr:=StringToStrArray (Value,Delimiter);
  SetLength(Result,High(strArr)+1);
  for i:=0 to High(strArr) do  Result[i]:=AsFloat(strArr[i]);
end;

//--------------------------------------------------------
function StringToIntArraySimple (Value: string; Delimiter: Char=';'): TIntArray;
//--------------------------------------------------------
var i:integer; arr:TStrArray;
begin
  Assert(Length(Delimiter)>0);

  arr:=StringToStrArray (Value,Delimiter);
  SetLength(Result,0);
  for i:=0 to High(arr) do begin
    SetLength (Result, High(Result)+2);
    Result [High(Result)]:=AsInteger(arr[i]);
  end;
end;

//--------------------------------------------------------
function StringToStrArray(aValue: string; aDelimiter: Char = ';'): TStrArray;
//--------------------------------------------------------
var i:integer;
  iPos: Integer;
    oList: TStringList;
  s: string;
begin
  //Assert(length(aDelimiter)>0);

  //TAB -> space

  aValue:=ReplaceStr(aValue, Chr(9), ' ');




  oList:=StringToStringList(aValue, aDelimiter);


//  oList:=TStringList.Create;
 // Result.Delimiter:=aDelimiter[1];
  //Result.DelimitedText:=aValue;

(*
  repeat
    aValue :=Trim(aValue);

    iPos :=Pos(aDelimiter, aValue);
    if iPos>0 then
    begin
      s:=Copy(aValue,1,iPos-1);
     // oList.Add(s);

      aValue := Copy(aValue,iPos+1, Length(aValue));
    end else begin
      s:=aValue;
      aValue :='';
    end;

    if s<>'' then oList.Add(s);

  until aValue='';
*)

//  oList:=CreateStringListFromString(aValue, aDelimiter);

  SetLength (Result, oList.Count);
  for i := 0 to oList.Count-1 do
    Result[i]:=oList[i];

  oList.Free;

end;

function DoubleArrayToString (Arr: TDoubleArray; Delimiter: string=';'): string;
var
  i,Len:integer;
begin
  Result:=''; Len:=High(Arr);
  for i:=0 to Len do
    Result:=Result + FloatToStr(Arr[i]) + IIF(i<Len, Delimiter,'');
end;

//-------------------------------------------------------------------
procedure DoubleArraySort (var aArr: TDoubleArray);
//-------------------------------------------------------------------
var i: integer; modified: boolean;// ���������� �� �����������
    v: double;
begin
  repeat
    modified:=false;
    for i:=0 to High(aArr)-1 do
      if aArr[i] > aArr[i+1] then
        begin v:=aArr[i+1]; aArr[i+1]:=aArr[i]; aArr[i]:=v; modified:=true; end;
  until
    (modified=false);
end;

function GetMaxFromIntArray (IntArr: TintArray): integer;
var i:integer;
begin
  if High(IntArr)=-1 then Result:=0
  else begin
    Result:=IntArr[0];
    for i:=1 to High(IntArr) do if IntArr[i]>Result then Result:=IntArr[i];
  end;
end;

function MakeIntArray(aValues: array of integer): TIntArray;
var i:integer;
begin
  SetLength (Result, High(aValues)+1);
  for i:=0 to High(aValues) do Result[i]:=aValues[i];
end;

(*
begin
  Result:=MakeStrArray (Values);
end;
*)


function MakeVariantArray (Values: array of Variant): TVariantArray;
var i: integer;
begin
  SetLength (Result, High(Values));
  for i:=0 to High(Values) do  Result[i]:=Values[i];
end;

function ValueInIntArray (Value: integer; IntArr: TIntArray): boolean;
begin
  Result:=(FindInIntArray (Value,IntArr)>=0);
end;

//--------------------------------------------------------
procedure AddToDoubleArray(aValue: double; var DoubleArr: TDoubleArray);
//--------------------------------------------------------
begin
  if (FindInDoubleArray (aValue,DoubleArr)<0) then begin
    SetLength (DoubleArr, High(DoubleArr)+2);
    DoubleArr [High(DoubleArr)]:=aValue;
  end;
end;

procedure DoubleArrayAddValue(var aArr: TDoubleArray; Value: double);
begin
  AddToDoubleArray (Value, aArr);
end;

end.
