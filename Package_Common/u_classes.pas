unit u_classes;

interface
uses Classes, SysUtils, DB, IniFiles, Dialogs,

     u_func
     ;

type


  TSortedStringList1 = class(TStringList)
    constructor Create;
  end;

  TIDListItem = class
    ID         : integer;
    TableName  : string;
    ObjName    : string;
    Name       : string;
    Tag        : integer;
    Tag1       : integer;
    GUID       : string;

    Comment    : string;
  end;

  //--------------------------------------------------
  TIDList = class(TList)
  //--------------------------------------------------
  private
    function CreateUsedObjectNameList: TStringList;
    function  GetItem (Index: integer): TIDListItem;
    function  FText (): string;
    function ValuesToString111(aDelimiter: string='; '): string;

 //   procedure ValuesToStrings1111111111(var aOutList: TStringList; aDelimiter:
  //      string=','; aBlockSize: integer=100);
  public
    function AddID    (aID: integer): TIDListItem;
    function AddTag   (aID: integer; aTag: integer): TIDListItem;
    function AddName  (aID: integer; aName: string): TIDListItem;
    function AddNameAndComment (aID: integer; aName, aComment: string): TIDListItem;
    function AddObjNameAndGUID1(aID: integer; aObjName,aGUID: string): TIDListItem;

  //          aIDList.AddNameAndComment(sID, sHop, AsString(bAdded));
    procedure AddIntArray (aArr: TIntArray);

    function AddGUID (aID: integer; aGUID: string): TIDListItem;
    function AddObjName (aID: integer; aObjName: string): TIDListItem;
    function Add_NameAndGUID(aName,aGUID: string): TIDListItem;

    procedure LoadFromDataSet (aDataSet: TDataSet);
    procedure LoadIDsFromDataSet(aDataSet: TDataSet; aFieldName: string = 'ID');
    procedure SaveToDataSet (aDataSet: TDataSet);
    procedure LoadFromStrings(aStrings: TStrings);

    procedure SaveToFile (aFileName: string);

    procedure LoadIDsFromIniFile(aINIFileName, aSection: String);

    procedure SaveIDsToIniFile(aINIFileName, aSection: String);

 //   procedure LoadNamesFromIniFile(aINIFileName, aSection: String);
 //   procedure SaveNamesToIniFile(aINIFileName, aSection: String);


    function  IndexByID  (aID: integer): integer;
    function  IDFound    (aID: integer): boolean;
    function  Compare (Source: TIDList): boolean; // true - equal lists
    procedure DeleteByID (aID: integer);

    procedure SortByID;
    procedure SortByTag1;
    procedure SortByObjName;

    procedure Assign (aSource: TIDList);
    procedure Clear; override;

    procedure DlgShow;

    function FindByObjName(aID: integer; aObjName: string): TIDListItem;

    procedure LoadIDsFromIniSection(aIniFile: TIniFile; aSection: string);
    function MakeDelimitedText_ID: string;

    function MakeIntArray (): TIntArray;
    function MakeWhereString: string;

    function ToString: string;


    procedure SaveIDsToIniFile1(aIni: TIniFile; aSection: String);

    property  Text: string read FText;
    property  Items   [Index: integer]: TIDListItem read GetItem; default;
  end;


  //--------------------------------------------------
  TStrIDListItem = class
  //--------------------------------------------------
    SID: string;
    Name: string;
    Comment: string;

  end;

  //--------------------------------------------------
  TStrIDList = class(TList)
  //--------------------------------------------------
  private
    function  GetItem (Index: integer): TStrIDListItem;
  public
    function AddSID(aSID: string): TStrIDListItem;
    function  AddName  (aID: string; aName: string): TStrIDListItem;
    function IndexBySID(aSID: string): integer;
    procedure DeleteBySID(aSID: string);
    function AddNameAndComment (aID,aName,aComment: string): TStrIDListItem;

    property  Items  [Index: integer]: TStrIDListItem read GetItem; default;
  end;



//==================================================================
implementation
//==================================================================

const
//  FLD_CELL1_ID='CELL1_ID';
 // FLD_CELL2_ID='CELL2_ID';

  FLD_ID='ID';
  FLD_NAME='name';
  FLD_GUID='guid';

  

//----------------------------------------------------
// TSortedStringList1
//----------------------------------------------------
constructor TSortedStringList1.Create;
begin
  inherited;
  Sorted:=true;
  Duplicates:=dupIgnore;
end;



//-------------------------------------------------------------------
procedure TIDList.LoadFromDataSet (aDataSet: TDataSet);
//-------------------------------------------------------------------
var oItem: TIDListItem;
begin
  Clear;

  if aDataSet.RecordCount=0 then
    Exit;    

  aDataSet.First;

  with aDataSet do
    while not Eof do
  begin
    oItem:=AddID (FieldByName(FLD_ID).AsInteger);
    oItem.Name:=FieldByName(FLD_NAME).AsString;
  //  oItem.GUID:=FieldByName(FLD_GUID).AsString;
    Next;
  end;
end;


//-------------------------------------------------------------------
procedure TIDList.LoadFromStrings(aStrings: TStrings);
//-------------------------------------------------------------------
var oItem: TIDListItem;
  I: Integer;
begin
  Clear;

  for I := 0 to aStrings.Count - 1 do
  begin
    oItem:=AddID ( Integer (aStrings.Objects[i]));
    oItem.Name:=aStrings[i];

  end;

(*
  if aDataSet.RecordCount=0 then
    Exit;

  aDataSet.First;

  with aDataSet do
    while not Eof do
  begin
    oItem:=AddID (FieldByName(FLD_ID).AsInteger);
    oItem.Name:=FieldByName(FLD_NAME).AsString;
  //  oItem.GUID:=FieldByName(FLD_GUID).AsString;
    Next;
  end;
*)

end;


//-------------------------------------------------------------------
procedure TIDList.LoadIDsFromDataSet(aDataSet: TDataSet; aFieldName: string =
    'ID');
//-------------------------------------------------------------------
var oItem: TIDListItem;
begin
  Clear;

  with aDataSet do
  begin
    First;

    while not Eof do
    begin
     oItem:=AddID (FieldByName(FLD_ID).AsInteger);
     Next;
    end;
  end;
end;

//-------------------------------------------------------------------
procedure TIDList.SaveToFile (aFileName: string);
//-------------------------------------------------------------------
var i: integer;
  oStrList: TStringList;
begin
  oStrList:=TStringList.Create;

  for i:=0 to Count-1 do
    oStrList.Add (IntToStr(Items[i].ID));

  oStrList.SaveToFile (aFileName);
  oStrList.Free;
end;


// -------------------------------------------------------------------
procedure TIDList.SaveIDsToIniFile(aINIFileName, aSection: String);
// -------------------------------------------------------------------
var
  oIni: TIniFile;
begin
  Assert(AINIFileName<>'');

  oIni:=TIniFile.Create(AINIFileName);

  SaveIDsToIniFile1 (oIni, aSection);

  FreeAndNil(oIni);
end;


// -------------------------------------------------------------------
procedure TIDList.SaveIDsToIniFile1(aIni: TIniFile; aSection: String);
// -------------------------------------------------------------------
var
  oIni: TIniFile;
  i: Integer;
begin

  with aIni do
  begin
    EraseSection(aSection);
    WriteInteger(aSection, 'Count', Count);

    for i:=0 to Count-1 do
    begin
      //'Item%d' - ��� �������������
      WriteInteger(aSection, Format('Item%d', [i+1]), Items[i].ID);
//      WriteInteger(aSection, Format('ItemID_%d', [i+1]), Items[i].ID);

      if Items[i].Name<>'' then
        WriteString (aSection, Format('ItemName_%d', [i+1]), Items[i].Name);

    end;
  end;

end;



// -------------------------------------------------------------------
procedure TIDList.LoadIDsFromIniFile(aINIFileName, aSection: string);
// -------------------------------------------------------------------
var
  oIniFile: TIniFile;
  i, iID,iCount: Integer;
  sName: string;
begin
  Assert(aINIFileName<>'');

  if not FileExists(AINIFileName) then
    Exit;

  oIniFile:=TIniFile.Create(AINIFileName);

  LoadIDsFromIniSection (oIniFile, aSection);

(*
  iCount:=oIniFile.ReadInteger(aSection, 'Count', 0);

  for i:=0 to iCount-1 do begin
    iID  :=oIniFile.ReadInteger(aSection, Format('Item%d', [i+1]), 0 );
//    iID  :=oIniFile.ReadInteger(aSection, Format('ItemID_%d', [i+1]), 0 );
    sName:=oIniFile.ReadString (aSection, Format('ItemName_%d', [i+1]), '');


    AddName (iID, sName);
  end;
*)
  oIniFile.Free;
end;


// -------------------------------------------------------------------
procedure TIDList.LoadIDsFromIniSection(aIniFile: TIniFile; aSection: string);
// -------------------------------------------------------------------
var
  i, iID,iCount: Integer;
  sName: string;
begin
  iCount:=aIniFile.ReadInteger(aSection, 'Count', 0);

  for i:=0 to iCount-1 do
  begin
    iID  :=aIniFile.ReadInteger(aSection, Format('Item%d', [i+1]), 0 );
//    iID  :=oIniFile.ReadInteger(aSection, Format('ItemID_%d', [i+1]), 0 );
    sName:=aIniFile.ReadString (aSection, Format('ItemName_%d', [i+1]), '');

    AddName (iID, sName);
  end;
  
end;



//-------------------------------------------------------------------
procedure TIDList.SaveToDataSet (aDataSet: TDataSet);
//-------------------------------------------------------------------
var i: integer;
begin
  aDataSet.Close;
  aDataSet.Open;

  for i:=0 to Count-1 do
  begin
    aDataSet.Append;

    aDataSet[FLD_ID]:=Items[i].ID;
    aDataSet[FLD_NAME]:=Items[i].Name;
    aDataSet[FLD_GUID]:=Items[i].GUID;

    aDataSet.Post;
  end;

end;     


function TIDList.AddGUID (aID: integer; aGUID: string): TIDListItem;
begin
  Result:=AddID (aID);
  Result.GUID:=aGUID;
end;

function TIDList.AddTag (aID: integer; aTag: integer): TIDListItem;
begin
  Result:=AddID (aID);
  Result.Tag:=aTag;
end;


function TIDList.AddName (aID: integer; aName: string): TIDListItem;
begin
  Result:=AddID (aID);
  Result.Name:=aName;
end;


function TIDList.AddObjNameAndGUID1(aID: integer; aObjName,aGUID: string):
    TIDListItem;
begin


  Result:=FindByObjName(aID, aObjName);

  if not assigned(Result) then
  begin
    Result:=TIDListItem.Create;
    Add ( Result);

    Result.ID:=aID;
    Result.ObjName:=aObjName;
    Result.GUID:=aGUID;
  end;
end;


function TIDList.Add_NameAndGUID(aName,aGUID: string): TIDListItem;
begin
  Result:=TIDListItem.Create;
  Add ( Result);

  Result.Name:=aName;
  Result.GUID:=aGUID;
end;


//----------------------------------------------------
function TIDList.AddNameAndComment (aID: integer; aName, aComment: string): TIDListItem;
//----------------------------------------------------
begin
  Result:=AddID (aID);
  Result.Name:=aName;
  Result.Comment:=aComment;
end;

//----------------------------------------------------
function TIDList.AddID (aID: integer): TIDListItem;
//----------------------------------------------------
var obj: TIDListItem;
    ind: integer;
begin
  ind:=IndexByID(aID);
  if ind < 0 then begin
    obj:=TIDListItem.Create;
    obj.ID:=aID;
    ind:=Add (obj);
  end;

  Result:=Items[ind];
end;


procedure TIDList.AddIntArray (aArr: TIntArray);
var i: integer;
begin
  for i:=0 to High(aArr) do
    AddID (aArr[i]);
end;


procedure TIDList.DeleteByID(aID: integer);
var i: integer;
begin
  for i:=Count-1 downto 0 do
    if Items[i].ID = aID then Delete(i);
end;


function TIDList.GetItem (Index: integer): TIDListItem;
begin
  Result:=TIDListItem(inherited Items[Index]);
end;


function TIDList.IndexByID (aID: integer): integer;
var i: integer;
begin
  for i:=0 to Count-1 do
    if Items[i].ID = aID then begin Result:=i; Exit end;
  Result:=-1;
end;


function TIDList.FindByObjName(aID: integer; aObjName: string): TIDListItem;
var i: integer;
begin
  Result:=nil;

  for i:=0 to Count-1 do
    if (Items[i].ID = aID) and Eq(Items[i].ObjName , aObjName) then
      begin Result:=Items[i]; Exit end;
end;



{function TIDList.FindBy_ID_and_Name (aID: integer): integer;
var i: integer;
begin
  for i:=0 to Count-1 do
    if Items[i].ID = aID then begin Result:=i; Exit end;
  Result:=-1;
end;


}
function TIDList.IDFound (aID: integer): boolean;
//var i: integer;
begin
  Result:=IndexByID(aID) >=0;

//  for i:=0 to Count-1 do
  //  if Items[i].ID = aID then begin Result:=true; Exit end;
end;


function TIDList.Compare (Source: TIDList): boolean;
var i: integer;
begin
  for i:=0 to Count-1 do
    if Source.IndexByID (Items[i].ID)<0 then begin Result:=false; Exit; end;
  for i:=0 to Source.Count-1 do
    if IndexByID (Source.Items[i].ID)<0 then begin Result:=false; Exit; end;
end;


//------------------------------------------------------
var
  loc_CompareType : (ctByID,ctByTag1,ctByObjName);


    function CompareBy (Item1, Item2: Pointer): Integer;
    var o1,o2: TIDListItem;
    begin
      o1:=TIDListItem(Item1);
      o2:=TIDListItem(Item2);

      case loc_CompareType of    //
        ctByObjName  : if o1.ObjName > o2.ObjName then Result:=+1 else
                       if o1.ObjName < o2.ObjName then Result:=-1 else Result:= 0;


        ctByID  : if o1.ID > o2.ID then Result:=+1 else
                  if o1.ID < o2.ID then Result:=-1 else Result:= 0;

        ctByTag1: if o1.Tag1 > o2.Tag1 then Result:=+1 else
                  if o1.Tag1 < o2.Tag1 then Result:=-1 else Result:= 0;

      end;    // case

    end;
//------------------------------------------------------

procedure TIDList.SortByObjName;
begin
  loc_CompareType:=ctByObjName;
  Sort(CompareBy);
end;

procedure TIDList.SortByID;
begin
  loc_CompareType:=ctByID;
  Sort(CompareBy);
end;

procedure TIDList.SortByTag1;
begin
  loc_CompareType:=ctByTag1;
  Sort(CompareBy);
end;

procedure TIDList.Assign(aSource: TIDList);
var i: integer;
begin
  Clear;

  for i:=0 to aSource.Count-1 do
    AddID (aSource.Items[i].ID);
end;


function TIDList.MakeIntArray: TIntArray;
var i: integer;
begin
  SetLength (Result, Count);
  for i:=0 to Count-1 do
    Result[i]:=Items[i].ID;
end;


//--------------------------------------------------------
function TIDList.ValuesToString111(aDelimiter: string='; '): string;
//--------------------------------------------------------
var i: integer;
begin // ������ ����������� �������� �������� : 1-23;33
  Result:='';

  for i:=0 to Count-1 do
  begin
    Result := Result+ IntTostr(Items[i].ID);
    if i<>Count-1 then
      Result := Result+ aDelimiter;
  end;
end;


//  Params.FilterRecords_List.Delimiter:=',';
//  sID:=Params.FilterRecords_List.DelimitedText;



   // for I := 0 to FParams_ref.SelectedIDList.Count - 1 do
    //  dmSQL_to_MDB_new.FilterRecords_List.Add(IntToStr(FParams_ref.SelectedIDList[i].id));


//--------------------------------------------------------
function TIDList.MakeDelimitedText_ID: string;
//--------------------------------------------------------
var
  i: Integer;
  oSList: TStringList;
begin
  oSList:=TStringList.Create;
  oSList.Delimiter:=',';


  for i:=0 to Count-1 do
    oSList.Add(IntToStr(Items[i].id));

  Result:=oSList.DelimitedText;

  FreeAndNil(oSList);


//  Result:='';
end;



//--------------------------------------------------------
function TIDList.MakeWhereString: string;
//--------------------------------------------------------
var i: integer;
begin
  Result:='';

  for i:=0 to Count-1 do
  begin
    Result := Result+ IntTostr(Items[i].ID);

    if i<>Count-1 then
      Result := Result+ ',';
  end;
end;


//--------------------------------------------------------
function TIDList.ToString: string;
//--------------------------------------------------------
var i: integer;
begin
  Result:='';

  for i:=0 to Count-1 do
  begin
    Result := Result+ IntTostr(Items[i].ID);

    if i<>Count-1 then
      Result := Result+ ',';
  end;
end;


 (*
var i: integer;
//const
//STR_BETWEEN = '(%s BETWEEN %d AND %d)';
begin
  // ������ ����������� �������� �������� : 1-23;33

  Result:='';

  for i:=0 to Count-1 do
  begin
    Result := Result+ IntTostr(Items[i].ID);
    if i<>Count-1 then
      Result := Result+ aDelimiter;
  end;
  end;
*)


//--------------------------------------------------------
function TIDList.FText (): string;
//--------------------------------------------------------
begin
  Result:= ValuesToString111(';');
end;

{
//--------------------------------------------------------
procedure TIDList.ValuesToStrings1111111111(var aOutList: TStringList;
    aDelimiter: string=','; aBlockSize: integer=100);
//--------------------------------------------------------
var i: integer;
  S: string;
begin
  if not Assigned(aOutList) then
    Raise Exception.Create('');

  aOutList.Clear;
  s:='';

  for i:=0 to Count-1 do
  begin
    s:=s+ IntTostr(Items[i].ID);
    if i<>Count-1 then
      s:=s+ aDelimiter;

    if ((i mod aBlockSize = 0) and (i <> 0)) or (i=Count-1) then
    begin
      str_DeleteLastChar(s);
      aOutList.Add(s);
      s:= '';
    end;
  end;
  
end;
}


procedure TIDList.Clear;
var i: integer;
begin
  for i:=0 to Count-1 do
    Items[i].Free;
  inherited;
end;




//-------------------------------------------------
// TStrIDList
//-------------------------------------------------

//----------------------------------------------------
function TStrIDList.GetItem (Index: integer): TStrIDListItem;
//----------------------------------------------------
begin
  Result:=TStrIDListItem(inherited Items[Index]);
end;

//----------------------------------------------------
function TStrIDList.IndexBySID(aSID: string): integer;
//----------------------------------------------------
var i: integer;
begin
  for i:=0 to Count-1 do
    if Items[i].SID = aSID then begin Result:=i; Exit end;
  Result:=-1;
end;

//----------------------------------------------------
function TStrIDList.AddSID(aSID: string): TStrIDListItem;
//----------------------------------------------------
var obj: TStrIDListItem;
    ind: integer;
begin
  ind:=IndexBySID(aSID);
  if ind < 0 then begin
    obj:=TStrIDListItem.Create;
    obj.SID:=aSID;
    ind:=Add (obj);
  end;

  Result:=Items[ind];
end;


//----------------------------------------------------
procedure TStrIDList.DeleteBySID(aSID: string);
//----------------------------------------------------
var i: integer;
begin
  for i:=Count-1 downto 0 do
    if Items[i].SID = aSID then Delete(i);
end;


//----------------------------------------------------
function TStrIDList.AddName (aID: string; aName: string): TStrIDListItem;
//----------------------------------------------------
begin
  Result:=AddSID (aID);
  Result.Name:=aName;
end;


//----------------------------------------------------
function TStrIDList.AddNameAndComment (aID,aName,aComment: string): TStrIDListItem;
//----------------------------------------------------
begin
  Result:=AddSID (aID);
  Result.Name:=aName;
  Result.Comment:=aComment;
end;



function TIDList.AddObjName(aID: integer; aObjName: string): TIDListItem;
begin
  Result:=AddID (aID);
  Result.ObjName:=aObjName;
end;


//--------------------------------------------------------------
function TIDList.CreateUsedObjectNameList: TStringList;
//--------------------------------------------------------------
var
  I: Integer;
begin

  Result := TStringList.Create;
  Result.Sorted := True;
  Result.Duplicates:=dupIgnore;

  for I := 0 to Count - 1 do
    Result.Add(Items[i].ObjName);

end;

procedure TIDList.DlgShow;
var
  I: Integer;
  S: string;
begin
  s:=s+ IntToStr(Count) + CRLF;

  for I := 0 to Count - 1 do
  begin
    s:=s+ Format('ObjName: %s; ID: %d', [Items[i].ObjName, Items[i].ID]) + CRLF;
  end;

  ShowMessage(s);
end;


begin

end.











{
procedure TIDList.SaveToXML (aNode: Variant; aTagName: string);
var vNodes: Variant;  i: integer;
begin
  vNodes:=xml_AddNode (aNode, aTagName);
  for i:=0 to Count-1 do
    xml_AddNode (vNodes, 'item', ['id'], [Items[i].ID] );
end;


procedure TIDList.LoadFromXML (aNode: Variant);
var  iID,i: integer;
begin
  Clear;
  if not VarIsNull(aNode) then
  for i:=0 to aNode.ChildNodes.Length-1 do begin
    iID:=AsInteger (xml_GetAttr (aNode.ChildNodes.Item(i), 'id'));
    AddID (iID);
  end;
end;
}





{

   function CompareListByValue (Item1,Item2: Pointer): integer;
   var obj1,obj2: TForceNeighbor;
   begin
     obj1:=TForceNeighbor(Item1); obj2:=TForceNeighbor(Item2);
     if obj1.iValue > obj2.iValue then Result:=-1 else
     if obj1.iValue < obj2.iValue then Result:=+1 else Result:=0;
   end;



   function CompareListByID (Item1,Item2: Pointer): integer;
   var obj1,obj2: TForceNeighbor;
   begin
     obj1:=TForceNeighbor(Item1); obj2:=TForceNeighbor(Item2);
     if obj1.SectorID > obj2.SectorID then Result:=+1 else
     if obj1.SectorID < obj2.SectorID then Result:=-1 else Result:=0;
   end;




}

                                
{

    db_AddRecord (aDataSet,
           [db_Par(FLD_ID,   Items[i].ID),
            db_Par(FLD_NAME, Items[i].Name),

            db_Par(FLD_GUID, Items[i].GUID)
           ]);}



           
// -------------------------------------------------------------------
procedure TIDList.SaveNamesToIniFile(aINIFileName, aSection: String);
// -------------------------------------------------------------------
var
  inifile: TIniFile;
  i: Integer;
begin{
  inifile:=TIniFile.Create(AINIFileName);

  inifile.EraseSection(aSection);
  inifile.WriteInteger(aSection, 'Count', Count);

  for i:=0 to Count-1 do
    inifile.WriteString (aSection, Format('Item%d', [i+1]), Items[i].Name);

  inifile.Free;}
end;

// -------------------------------------------------------------------
procedure TIDList.LoadNamesFromIniFile(aINIFileName, aSection: string);
// -------------------------------------------------------------------
var
  oIniFile: TIniFile;
  i, iID,iCount: Integer;
  sName: string;
begin{
  if not FileExists(AINIFileName) then
    Exit;

  oIniFile:=TIniFile.Create(AINIFileName);

  iCount:=oIniFile.ReadInteger(aSection, 'Count', 0);

  for i:=0 to iCount-1 do begin
    sName:=oIniFile.ReadString(aSection, Format('Item%d', [i+1]), '' );
    AddName (iID);
  end;

  oIniFile.Free;}
end;


