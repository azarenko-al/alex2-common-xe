unit d_Wizard1;

interface

uses
  Classes, Controls, Forms, rxPlacemnt, ActnList, StdCtrls, ExtCtrls

  ;

type
  Tdlg_Wizard1 = class(TForm)
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    pn_Top_: TPanel;
    Bevel2: TBevel;
    pn_Header: TPanel;
    lb_Action: TLabel;
    ActionList1: TActionList;
    act_Ok: TAction;
    act_Cancel: TAction;
    FormStorage1: TFormStorage;
    procedure act_CancelExecute(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  protected
    FRegPath: string;
  protected
//    procedure AddComponentProp(aComponent: TComponent; aField: string);
    procedure HideHeader;
    procedure SetActionName (aValue: string);

    procedure SetDefaultSize();
    procedure SetDefaultWidth();
  end;


(*const
  DEF_PropertiesValue ='Properties.Value';
*)

//==================================================================
implementation {$R *.DFM}
//==================================================================

const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\';
                                                                


//-------------------------------------------------------------------
procedure Tdlg_Wizard1.FormCreate(Sender: TObject);
//-------------------------------------------------------------------

begin
  inherited;

  Assert(not FormStorage1.Active);

  lb_Action.Top:=8;
  lb_Action.Left:=5;
 // pn_Header.BorderWidth:=0;
                         
  pn_Header.Visible := True;

  btn_Ok.Action:=act_Ok;

  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\';

  Assert(not FormStorage1.Active);

  FormStorage1.IniFileName:=FRegPath + FormStorage1.name;
  FormStorage1.IniSection:='Window';
  FormStorage1.Active:=True;

(*
  Assert(not cxPropertiesStore.Active);

  cxPropertiesStore.StorageName:=FRegPath + cxPropertiesStore.Name;
  cxPropertiesStore.Active := True;
*)
 // SetActionName('');

 // if not pn_Header.Visible then
//    AlignTop();

end;


procedure Tdlg_Wizard1.SetActionName(aValue: string);
begin
  lb_Action.Caption:=aValue;
  pn_Top_.Visible := True;
end;


procedure Tdlg_Wizard1.SetDefaultSize;
begin
  with Constraints do begin
    MaxHeight:=385;
    MinHeight:=MaxHeight;
  end;

  SetDefaultWidth();
end;


procedure Tdlg_Wizard1.SetDefaultWidth();
begin
  with Constraints do
  begin
    MaxWidth :=505;
    MinWidth :=MaxWidth;
  end;
end;




procedure Tdlg_Wizard1.act_CancelExecute(Sender: TObject);
begin
  Close;
end;

procedure Tdlg_Wizard1.act_OkExecute(Sender: TObject);
begin
  //!!!!!!!!!!!!!
end;

procedure Tdlg_Wizard1.HideHeader;
begin
  pn_Top_.Visible := False;
//  pn_Header.Visible := True;
end;

{
procedure Tdlg_Wizard.act_CancelExecute(Sender: TObject);
begin
  
end;
}

{
procedure Tdlg_Wizard.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;}


end.





