unit u_menu;

interface

uses
  Winapi.Windows,  Vcl.Menus, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls;

procedure CopyPopupMenu(aPopupMenu_src, aPopupMenu_dest: TPopupMenu);


implementation



procedure CopyPopupMenu(aPopupMenu_src, aPopupMenu_dest: TPopupMenu);

    function CloneMenuItem(SourceItem: TMenuItem): TMenuItem;
    var
      I: Integer;
    Begin
      with SourceItem do
      Begin
        Result := NewItem(Caption, Shortcut, Checked, Enabled, OnClick, HelpContext, Name + 'Copy');

        Result.Action:=SourceItem.Action;
        
        for I := 0 To Count - 1 do
          Result.Add(CloneMenuItem(Items[I]));
      end;
    end;
    

var
  I: Integer;
begin

  aPopupMenu_dest.Items.Clear;

  for I := 0 to aPopupMenu_src.Items.Count-1 do
    aPopupMenu_dest.Items.Add(CloneMenuItem(aPopupMenu_src.Items[I])); 
  
end;



end.
