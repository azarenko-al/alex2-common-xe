unit u_com;

interface
uses
 shellapi, Forms,  StrUtils, Windows, Variants, SysUtils, Dialogs, ActiveX; 

//  function GetComObject(aDllFileName: PChar; const CLSID, IID: TGUID; var aObj): THandle;

//  function RegisterOCX(aFileName: string): Boolean;

  function GetModuleFileName: string;

//procedure RegisterOCX_new111111111(aFileName: string);



type
  
  TComClient = class
//    constructor Create(aFileName: string);
  destructor Destroy; override;
   type
      TGetObject = function(const CLSID, IID: TGUID; var aObj): HResult; stdcall;
  private
 
      GetObject: TGetObject;
 //     vFactory: IClassFactory;

  
    dll: THandle;
//    fIntf: ITileManagerX;
  public
    procedure OpenFile(aFileName: string);

  //  constructor Create(aFileName: string);

//    procedure OpenFile;

  //  property Intf: ITileManagerX read fIntf;
  end;

  
function GetComObject(aDllFileName: string; const CLSID, IID: TGUID; var aObj):
    THandle;

//procedure FreeComObject(aHandle: THandle; var aObj);

    


implementation

//function ExecAndWait(const ExecuteFile, ParamString : string): boolean; forward;


function DoubleQuotedStr(aValue: String): String;
Begin
  Result:='"'+aValue+'"';
end;



//Code released into public domain. No attribution required.
function IsClassRegistered(const aClassID: TGUID): Boolean;
var
    unk: IUnknown;
    hr: HRESULT;
begin
    hr := CoCreateInstance(aClassID, nil, CLSCTX_INPROC_SERVER or CLSCTX_LOCAL_SERVER, IUnknown, {out}unk);
    unk := nil;

    Result := (hr <> REGDB_E_CLASSNOTREG);
end;

//
////-------------------------------------------------------------------
//procedure FreeComObject(aHandle: THandle; var aObj);
////-------------------------------------------------------------------
//begin
//  aObj:=null;
//  
//  FreeLibrary(aHandle);    
//end;
//            


//-------------------------------------------------------------------
function GetComObject(aDllFileName: string; const CLSID, IID: TGUID; var aObj):
    THandle;
//-------------------------------------------------------------------
type
  TGetObject = function(const CLSID, IID: TGUID; var aObj): HResult; stdcall;
var
  GetObject: TGetObject;
  vFactory: IClassFactory;

begin
  Assert(FileExists(aDllFileName), 'not FileExists: ' + aDllFileName);


  Result:=LoadLibrary(PChar(aDllFileName)); 
  Integer(aObj):=0;

  if Result >= 32 then
  begin
    @GetObject:=GetProcAddress(Result,'DllGetClassObject');

    if Assigned(GetObject) then
      if GetObject(CLSID,IClassFactory,vFactory) = S_OK then
        vFactory.CreateInstance(nil,IID,aObj);

    vFactory:=nil
  end
end;


// ---------------------------------------------------------------
function GetModuleFileName: string;
// ---------------------------------------------------------------
//var
//  szFileName: array[0..MAX_PATH] of Char;
begin
//  FillChar(szFileName, SizeOf(szFileName), #0);
  //unit SysInit;
  Result := GetModuleName(hInstance); //, szFileName, MAX_PATH);
//  Result := szFileName;
end;

{
// ---------------------------------------------------------------
procedure RegisterOCX_new111111111(aFileName: string);
// ---------------------------------------------------------------
type
  TRegFunc = function : HResult; stdcall;
var
  ARegFunc : TRegFunc;
  h : THandle;
 // ocxPath : string;
begin
  Assert(FileExists(aFileName), aFileName);


  try
//    ocxPath := ExtractFilePath(Application.ExeName) + 'Flash.ocx';
    h := LoadLibrary(PChar(aFileName));

    if h > 0 then
    begin
      ARegFunc := GetProcAddress(h,'DllRegisterServer');

      if Assigned(ARegFunc) then
      begin
        ExecAndWait('regsvr32','/s ' + QuotedStr(aFileName));
      end;

      //DoubleQuotedStr(sFile), iCode);
      FreeLibrary(h);
    end;
    
  except
    ShowMessage(Format('Unable to register %s', [aFileName]));
  end;
end;
 

// ---------------------------------------------------------------
function RegisterOCX(aFileName: string): Boolean;
// ---------------------------------------------------------------
type
  TDllRegisterServer = function: HResult; stdcall;
var
  h: THandle;
  RegFunc: TDllRegisterServer;
  iErr: Integer;
  iRet: Integer;
  sErr: string;
begin
  Assert(FileExists(aFileName), aFileName);


  h := LoadLibrary(PWideChar(aFileName));

  if h = 0 then
  begin
    iRet := GetLastError();

    sErr := SysErrorMessage(iRet);
    ShowMessage(sErr);
  end; //...

        
  Assert(h>0);
  
  RegFunc := GetProcAddress(h, 'DllRegisterServer');
  if @RegFunc <> nil then
    Result := RegFunc = S_OK
  else
  begin
    Result := False;
    ShowMessage('DllRegisterServer -  not done');
  end;

  Assert(Result);

  FreeLibrary(h);
end;



// ---------------------------------------------------------------
function ExecAndWait(const ExecuteFile, ParamString : string): boolean;
// ---------------------------------------------------------------
//shellapi
var
  SEInfo: TShellExecuteInfo;
  ExitCode: DWORD;
begin
  FillChar(SEInfo, SizeOf(SEInfo), 0);
  SEInfo.cbSize := SizeOf(TShellExecuteInfo);

  with SEInfo do begin
    fMask := SEE_MASK_NOCLOSEPROCESS;
    Wnd := Application.Handle;
    lpFile := PChar(ExecuteFile);
    lpParameters := PChar(ParamString);
    nShow := SW_HIDE;
  end;

  if ShellExecuteEx(@SEInfo) then
  begin
    repeat
      Application.ProcessMessages;
      GetExitCodeProcess(SEInfo.hProcess, ExitCode);
    until (ExitCode = STILL_ACTIVE) or Application.Terminated;

    Result:=True;
  end
  else Result:=False;
end;
 }

//-------------------------------------------------------------------
//-------------------------------------------------------------------
//-------------------------------------------------------------------
//-------------------------------------------------------------------


destructor TComClient.Destroy;
begin
//  if dll >= 32 then
//    FreeLibrary(dll);

  inherited;
end;

procedure TComClient.OpenFile(aFileName: string);
begin
  // TODO -cMM: TComClient.OpenFile default body inserted
end;


end.       

{  
//-------------------------------------------------------------------
function GetComObject(aDllFileName: PChar; const CLSID, IID: TGUID; var aObj): THandle;
//-------------------------------------------------------------------
type
  TGetObject = function(const CLSID, IID: TGUID; var aObj): HResult; stdcall;
var
  GetObject: TGetObject;
  vFactory: IClassFactory;

begin
  Result:=LoadLibrary(aDllFileName); 
  Integer(aObj):=0;

  if Result >= 32 then begin
    @GetObject:=GetProcAddress(Result,'DllGetClassObject');
    if Assigned(GetObject) then
    if GetObject(CLSID,IClassFactory,vFactory) = S_OK then
      vFactory.CreateInstance(nil,IID,aObj);
    vFactory:=nil
  end
end;





uses shellapi;
...
function ExecAndWait(const ExecuteFile, ParamString : string): boolean;
var
SEInfo: TShellExecuteInfo;
ExitCode: DWORD;
begin
FillChar(SEInfo, SizeOf(SEInfo), 0);
SEInfo.cbSize := SizeOf(TShellExecuteInfo);
with SEInfo do begin
fMask := SEE_MASK_NOCLOSEPROCESS;
Wnd := Application.Handle;
lpFile := PChar(ExecuteFile);
lpParameters := PChar(ParamString);
nShow := SW_HIDE;
end;
if ShellExecuteEx(@SEInfo) then
begin
repeat
Application.ProcessMessages;
GetExitCodeProcess(SEInfo.hProcess, ExitCode);
until (ExitCode STILL_ACTIVE) or Application.Terminated;
Result:=True;
end
else Result:=False;
end;


procedure RegisterOCX;
type
TRegFunc = function : HResult; stdcall;
var
ARegFunc : TRegFunc;
aHandle : THandle;
ocxPath : string;
begin
try
ocxPath := ExtractFilePath(Application.ExeName) + 'Flash.ocx';
aHandle := LoadLibrary(PChar(ocxPath));
if aHandle 0 then
begin
ARegFunc := GetProcAddress(aHandle,'DllRegisterServer');
if Assigned(ARegFunc) then
begin
ExecAndWait('regsvr32','/s ' + ocxPath);
end;
FreeLibrary(aHandle);
end;
except
ShowMessage(Format('Unable to register %s', [ocxPath]));
end;
end;