unit u_progress;

interface
uses

  d_Progress
   ;

type
  TOnAllProgressAdvEvent = procedure (aProgress1,aProgress2,aMax1,aMax2: integer; var aTerminated:boolean) of object;

  TOnProgressAdvEvent = procedure (aProgress,aMax: integer; var aTerminated: boolean) of object;
  TOnProgressEvent = procedure (aProgress,aMax: integer; var aTerminated: boolean) of object;

  TOnLogEvent         = procedure (aMsg: string) of object;


{  TOnProgressAdv1Event = procedure (aProgress,aMax: integer; var aTerminated: boolean;
                                    var aPause : boolean; var aSkiped : boolean) of object;
}

  //----------------------------------------------------------
  // ������� ����� ��� ��������
  //----------------------------------------------------------
  TProgress = class
  //----------------------------------------------------------
  private
    Fdlg_Progress_adv: Tdlg_Progress;

    FOnLog,
    FOnLog2,
    FOnProgressMsg,
    FOnProgressMsg2:  TOnLogEvent;

    procedure DoOnStartProgressDlg(Sender: TObject);
  protected
    FOnAllProgress : TOnAllProgressAdvEvent;

    FOnProgress,
    FOnProgress2: TOnProgressAdvEvent;

    procedure DoLog (aMsg: string);
    procedure DoLog2 (aMsg: string);

    procedure DoProgress (aPosition,aTotal: integer); virtual;
    procedure DoProgress2 (aPosition,aTotal: integer);
    procedure DoAllProgress (aPosition1,aPosition2,aTotal1,aTotal2 : integer);


    procedure DoProgressMsg (aMsg: string);
    procedure DoProgressMsg2 (aMsg: string);

  public
    Terminated: boolean;
    Pause     : boolean;
    Skipped : boolean;
    ProgressStep: integer;
    Progress2Step: integer;

    function ExecuteDlg (aCaption: string=''): boolean;
    // success: true 

    procedure ExecuteProc; virtual; abstract;

    procedure Terminate();

// TODO: DlgSetShowDetails
//  procedure DlgSetShowDetails (aValue: boolean);
    procedure ChangeCaption(aNewCaption : string);

    property OnProgress:  TOnProgressAdvEvent read FOnProgress write FOnProgress;
    property OnProgress2: TOnProgressAdvEvent read FOnProgress2 write FOnProgress2;
    property OnLog:      TOnLogEvent read FOnLog write FOnLog;
    property OnLog2:     TOnLogEvent read FOnLog2 write FOnLog2;

    property OnProgressMsg:  TOnLogEvent read FOnProgressMsg write FOnProgressMsg;
    property OnProgressMsg2: TOnLogEvent read FOnProgressMsg2 write FOnProgressMsg2;

  end;

var
  g_Progress: TProgress;


//==================================================================
implementation
//==================================================================

//-------------------------------------------------
// TProgress
//-------------------------------------------------

procedure TProgress.DoAllProgress (aPosition1,aPosition2,aTotal1,aTotal2 : integer);
begin
  if Assigned(FOnAllProgress) then FOnAllProgress(aPosition1,aPosition2,aTotal1,aTotal2,Terminated);
end;


// TODO: DlgSetShowDetails
//procedure TProgress.DlgSetShowDetails (aValue: boolean);
//begin
//if Assigned(Fdlg_Progress_adv) then
//  Fdlg_Progress_adv.ShowDetails (aValue);
//end;


procedure TProgress.DoProgress (aPosition,aTotal: integer); //var aTerminated: boolean);
begin
  if (ProgressStep=0) or
     (ProgressStep>0) and (aPosition mod ProgressStep=0)
  then
    if Assigned(FOnProgress) then FOnProgress(aPosition,aTotal, Terminated);
//    if Assigned(FOnProgress) then FOnProgress(aPosition,aTotal, Terminated, Pause ,Skipped);
end;

procedure TProgress.DoLog(aMsg: string);
begin
  if Assigned(FOnLog) then 
     FOnLog(aMsg);
end;

procedure TProgress.DoLog2(aMsg: string);
begin
  if Assigned(FOnLog2) then  FOnLog2(aMsg);
end;

procedure TProgress.DoProgress2(aPosition, aTotal: integer);  //var aTerminated: boolean);
begin
  if (Progress2Step=0) or
     (Progress2Step>0) and (aPosition mod Progress2Step=0)
  then
    if Assigned(FOnProgress2) then
      FOnProgress2(aPosition,aTotal, Terminated);
//      FOnProgress2(aPosition,aTotal, Terminated, Pause, Skipped);
end;

procedure TProgress.Terminate();
begin
  Terminated:=True;
end;


procedure TProgress.DoProgressMsg(aMsg: string);
begin
  if Assigned(FOnProgressMsg) then
    FOnProgressMsg(aMsg);
end;


procedure TProgress.DoProgressMsg2(aMsg: string);
begin
  if Assigned(FOnProgressMsg2) then
    FOnProgressMsg2(aMsg);
end;


procedure TProgress.DoOnStartProgressDlg(Sender: TObject);
begin
  ExecuteProc();
end;

//--------------------------------------------------------------------
procedure TProgress.ChangeCaption (aNewCaption: string);
//--------------------------------------------------------------------
begin
  Fdlg_Progress_adv.Caption := aNewCaption;
end;


//--------------------------------------------------------------------
function TProgress.ExecuteDlg (aCaption: string=''): boolean;
//--------------------------------------------------------------------
begin
  Fdlg_Progress_adv:=Tdlg_Progress.CreateForm(aCaption);
  Fdlg_Progress_adv.OnStart:=DoOnStartProgressDlg;

  OnProgress:=Fdlg_Progress_adv.DoOnProgress1;
  OnProgress2:=Fdlg_Progress_adv.DoOnProgress2;
  OnLog:=Fdlg_Progress_adv.Log;



  OnProgressMsg:=Fdlg_Progress_adv.DoOnProgressMsg;
  OnProgressMsg2:=Fdlg_Progress_adv.DoOnProgressMsg2;


  Fdlg_Progress_adv.ShowModal;
  Fdlg_Progress_adv.Free;

  Result:=(not Terminated);
end;

 
end.