unit u_FastReport;

interface

uses
  Classes, DB, SySUtils,  frxClass, frxDBSet,

  u_func
  ;

  procedure frxDBDataset_UpdateAll(aDataModule: TDataModule);

procedure frxReport_Add_Dataset(aReport : TfrxReport; aDataset: TfrxDataset);

function frxReport_ShowDesignReport(aReport : TfrxReport): Boolean;

////


implementation


procedure frxReport_Add_Dataset(aReport : TfrxReport; aDataset: TfrxDataset);
begin
  if not Assigned(aReport.DataSets.Find(aDataset)) then
    aReport.DataSets.Add(aDataset);
end;
       

// ---------------------------------------------------------------
function frxReport_ShowDesignReport(aReport : TfrxReport): Boolean;
// ---------------------------------------------------------------
var
  b: Boolean;
  oStream1: TMemoryStream;
  oStream2: TMemoryStream;
begin
//  frxReport.PrepareReport(true);
//  frxReport.ShowReport;
  oStream1:=TMemoryStream.Create;
  oStream2:=TMemoryStream.Create;

  aReport.SaveToStream(oStream1);


  aReport.DesignReport(True);
//  frxReport.m
  b:=aReport.Modified;

  aReport.SaveToStream(oStream2);


  b:=not u_func.CompareMemoryStreams(oStream1,oStream2);

  Result := b;

  FreeAndNil(oStream1);
  FreeAndNil(oStream2);

end;


// ---------------------------------------------------------------
procedure frxDBDataset_Update(aFrxDBDataset: TfrxDBDataset);
// ---------------------------------------------------------------
var
  i: Integer;
  oAliases: TStrings;
  oFields: TStringList;
  oDataSet: TDataSet;
begin
//  aFrxDBDataset.

  oDataSet := aFrxDBDataset.GetDataSet;
  Assert(Assigned(oDataSet));

  oAliases:=aFrxDBDataset.FieldAliases;


  oFields := TStringList.Create;
  oDataSet.GetFieldNames(oFields);

  oAliases.Clear;

  for I := 0 to oFields.Count - 1 do
    oAliases.Values[oFields[i]]:=oFields[i];

  oFields.Free;

end;     


// ---------------------------------------------------------------
procedure frxDBDataset_UpdateAll(aDataModule: TDataModule);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to aDataModule.ComponentCount - 1 do
    if aDataModule.Components[i] is TfrxDBDataset then
      frxDBDataset_Update(aDataModule.Components[i] as TfrxDBDataset);

end;


end.
