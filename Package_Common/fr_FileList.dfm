object frame_FileList: Tframe_FileList
  Left = 686
  Top = 367
  Width = 508
  Height = 362
  Caption = 'frame_FileList'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 500
    Height = 53
    Caption = 'ToolBar1'
    Color = clAppWorkSpace
    ParentColor = False
    TabOrder = 0
    Visible = False
  end
  object pn_Buttons: TPanel
    Left = 423
    Top = 53
    Width = 77
    Height = 256
    Align = alRight
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Button4: TButton
      Left = 4
      Top = 29
      Width = 70
      Height = 23
      Action = act_Del
      TabOrder = 0
    end
    object Button1: TButton
      Left = 4
      Top = 1
      Width = 70
      Height = 23
      Action = act_Add
      TabOrder = 1
    end
    object btn_Tools: TButton
      Left = 4
      Top = 68
      Width = 70
      Height = 23
      Action = act_Tools
      PopupMenu = pop_Tools__
      TabOrder = 2
      Visible = False
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 309
    Width = 500
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object Files: TListBox
    Left = 0
    Top = 53
    Width = 345
    Height = 256
    Align = alLeft
    ItemHeight = 13
    MultiSelect = True
    Sorted = True
    TabOrder = 3
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Left = 12
    Top = 2
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 40
    Top = 2
    object act_Add: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = act_AddExecute
    end
    object act_Del: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = act_AddExecute
    end
    object act_Tools: TAction
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080'...'
      OnExecute = act_AddExecute
    end
  end
  object pop_Tools__: TPopupMenu
    Left = 76
    Top = 2
  end
  object cxPropertiesStore1: TcxPropertiesStore
    Active = False
    Components = <
      item
        Component = Files
        Properties.Strings = (
          'Items')
      end
      item
        Component = OpenDialog1
        Properties.Strings = (
          'InitialDir')
      end>
    StorageName = 'cxPropertiesStore1'
    StorageType = stRegistry
    Left = 168
    Top = 3
  end
end
