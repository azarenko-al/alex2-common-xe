//===========================================================================//
//  ���� ��������� ������� �����
//
//===========================================================================//
unit d_Customize_Grid;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, StdCtrls, ExtCtrls,cxPropertiesStore, rxPlacemnt, ImgList,
  Registry,

  dxCntner, dxTL, dxDBCtrl, dxDBGrid, dxDBTL, dxInspct, dxDBInsp,
  dxInspRw,  dxGrClms, dxDBTLCl,


  d_Wizard,
  u_func,  ComCtrls
  ;

type

  Tdlg_Customize_dx_Grid = class(Tdlg_Wizard)
    TreeView1: TTreeView;
    act_Up: TAction;
    act_Down: TAction;
    ImageList1: TImageList;
    Label3: TLabel;
    edWidth: TEdit;
    buDown: TButton;
    buUp: TButton;
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure act_UpExecute(Sender: TObject);
    procedure act_DownExecute(Sender: TObject);
    procedure TreeView1Click(Sender: TObject);
    procedure edWidthKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    procedure FromGridToTree(aGrid: TdxDBGrid); 
    procedure FromTreeToGrid(aGrid: TdxDBGrid);

    procedure FromGridToTree_DBTreeList(aTreeList: TdxDBTreeList);
    procedure FromTreeToGrid_DBTreeList(aTreeList: TdxDBTreeList);

    procedure FreeTreeView;

    function AddNode (aParent: TTreeNode;
                      aCaption: string; aName: string;
                      aOrder, aWidth: integer;
                      aVisible: boolean;
                      aColumn: TdxDBTreeListColumn;
                      aBand:   TdxTreeListBand
                      ): TTreeNode;

  public
    class function ExecDlg_DBGrid (aGrid: TdxDBGrid): boolean;
    class function ExecDlg_DBTreeList (aTreeList: TdxDBTreeList): boolean;

  end;


// ==================================================================
implementation  {$R *.dfm}
// ==================================================================


type
  TColumnInfo = class
       ID:      integer;
       Order:   integer;
       Width:   integer;
       Name:    string;
       Visible: boolean;
     //  Item:    pointer;

       AsColumn: TdxDBTreeListColumn;
       AsBand:   TdxTreeListBand;

  end;


//===========================================================================//
//  ����������� ������� �������� ����� � �� ���������
//===========================================================================//


// -------------------------------------------------------------------
class function Tdlg_Customize_dx_Grid.ExecDlg_DBGrid (aGrid: TdxDBGrid): boolean;
// -------------------------------------------------------------------
begin
  with Tdlg_Customize_dx_Grid.Create(Application) do
  try
    FromGridToTree(aGrid);

    Result := (ShowModal = mrOK);

    if Result then
      FromTreeToGrid(aGrid);

    FreeTreeView;

  finally
    Free;
  end;
end;

// -------------------------------------------------------------------
class function Tdlg_Customize_dx_Grid.ExecDlg_DBTreeList (aTreeList: TdxDBTreeList): boolean;
// -------------------------------------------------------------------
begin
  with Tdlg_Customize_dx_Grid.Create(Application) do
  try
    FromGridToTree_DBTreeList(aTreeList);
    Result := (ShowModal = mrOK);

    if Result then
      FromTreeToGrid_DBTreeList(aTreeList);

   FreeTreeView;
  finally
    Free;
  end;
end;


procedure Tdlg_Customize_dx_Grid.FormCreate(Sender: TObject);
begin
  inherited;
  Caption:= '��������� �������';
end;


//===========================================================================//
//  ������������ ������
//===========================================================================//
procedure Tdlg_Customize_dx_Grid.FreeTreeView;
var i: integer;
    oColumnInfo: TColumnInfo;
begin

  for i := 0 to TreeView1.Items.Count - 1 do
  begin
    oColumnInfo:=TColumnInfo(TreeView1.Items[i].Data);

 ///??????????????????????

 {   if assigned (oColumnInfo) then
       oColumnInfo.Free;
}

  end;

end;

//===========================================================================//
//  ���������� ������ �� ���������� � ���������� COLUMNS � BANDS
//
//===========================================================================//
procedure Tdlg_Customize_dx_Grid.FromGridToTree(aGrid: TdxDBGrid);
// ---------------------------------------------------------------
var
  i, j: integer;
  oBand: TTreeNode;
  oColumn: TdxDBTreeListColumn;

begin
  TreeView1.Items.BeginUpdate;
  try
    TreeView1.Items.Clear;
    for i := 0 to aGrid.Bands.Count - 1 do
    begin
      with aGrid.Bands[i] do
        oBand := AddNode(NIL, Caption, DisplayName, Index, Width, Visible, nil, aGrid.Bands[i]);

      for j := 0 to aGrid.ColumnCount - 1 do
      begin
        oColumn:=aGrid.Columns[j];

        if (oColumn.BandIndex = aGrid.Bands[i].Index) and (oColumn.Tag = 0)
        then
          AddNode(oBand, oColumn.Caption, oColumn.Name, oColumn.ColIndex,
                    oColumn.Width, oColumn.Visible, oColumn, nil);
      end;
    end;

    TreeView1.Items[0].Expand(True);
  finally
    TreeView1.Items.EndUpdate;
  end;

  TreeView1.Items[0].Selected := True;
end;


// -------------------------------------------------------------------
function Tdlg_Customize_dx_Grid.AddNode (aParent: TTreeNode;
                                      aCaption: string; aName: string;
                                      aOrder, aWidth: integer;
                                      aVisible: boolean;
                                      aColumn: TdxDBTreeListColumn;
                                      aBand:   TdxTreeListBand
                                      ): TTreeNode;
// -------------------------------------------------------------------
var
  oColumnInfo: TColumnInfo;

begin
  if aCaption = '' then aCaption:= '--';

  oColumnInfo:=TColumnInfo.Create;

  Result := TreeView1.Items.AddChildObject(aParent, aCaption, oColumnInfo);
  Result.ImageIndex    := integer(aVisible);
  Result.SelectedIndex := Result.ImageIndex;

  oColumnInfo.Name   := aName;
  oColumnInfo.Order  := aOrder;
  oColumnInfo.Width  := aWidth;
  oColumnInfo.Visible:= aVisible;

  oColumnInfo.AsColumn:=aColumn;
  oColumnInfo.AsBand  :=aBand;

end;
// -------------------------------------------------------------------


// -------------------------------------------------------------------
procedure Tdlg_Customize_dx_Grid.FromGridToTree_DBTreeList (aTreeList: TdxDBTreeList);
// -------------------------------------------------------------------

var
  i, j: integer;
  oBand: TTreeNode;
  oColumn: TdxDBTreeListColumn;

begin
  TreeView1.Items.BeginUpdate;
  try
    TreeView1.Items.Clear;
    for i := 0 to aTreeList.Bands.Count - 1 do
    begin

      with aTreeList.Bands[i] do
        oBand := AddNode(NIL, Caption, DisplayName, Index, Width, Visible, nil, aTreeList.Bands[i]);


      for j := 0 to aTreeList.ColumnCount - 1 do
      begin
//        oColumn:=aTreeList.Columns[0];
        oColumn:=aTreeList.Columns[j];

        if (oColumn.BandIndex = aTreeList.Bands[i].Index) and (oColumn.Tag = 0)
        then
           AddNode(oBand, oColumn.Caption,
                   oColumn.Name, oColumn.ColIndex,
                   oColumn.Width, oColumn.Visible,
                   oColumn,nil);
      end;

    end;

    if TreeView1.Items.Count>0 then
      TreeView1.Items[0].Expand(True);
  finally
    TreeView1.Items.EndUpdate;
  end;

  if TreeView1.Items.Count>0 then
    TreeView1.Items[0].Selected := True;
end;

//===========================================================================//
// ��������� ����� �� ���������� �� ������
//
//===========================================================================//
procedure Tdlg_Customize_dx_Grid.FromTreeToGrid(aGrid: TdxDBGrid);
// -------------------------------------------------------------------
var  i: integer;
begin
  if not assigned (TreeView1)   then
   ShowMessage ( 'TreeView1 not assigned' );


  for i := 0 to TreeView1.Items.Count - 1 do
    with TreeView1.Items[i] do
      if Level = 0 then
      begin // BAND
        TColumnInfo(Data).AsBand.Visible := boolean(ImageIndex);
        TColumnInfo(Data).AsBand.Index   := Index;
      end
      else begin // COLUMN
        TColumnInfo(Data).AsColumn.Visible := boolean(ImageIndex);
        TColumnInfo(Data).AsColumn.Width   := TColumnInfo(Data).Width;
        TColumnInfo(Data).AsColumn.Index   := Index;
      end;
end;

// -------------------------------------------------------------------
procedure Tdlg_Customize_dx_Grid.FromTreeToGrid_DBTreeList(aTreeList: TdxDBTreeList);
// -------------------------------------------------------------------
var i: integer;
begin
  if not assigned (TreeView1)   then
   ShowMessage ( 'TreeView1 not assigned' );


  for i := 0 to TreeView1.Items.Count - 1 do
    with TreeView1.Items[i] do
      if Level = 0 then
      begin // BAND
        TColumnInfo(Data).AsBand.Visible := boolean(ImageIndex);
        TColumnInfo(Data).AsBand.Index   := Index;
      end
      else begin // COLUMN
        TColumnInfo(Data).AsColumn.Visible := boolean(ImageIndex);
        TColumnInfo(Data).AsColumn.Width   := TColumnInfo(Data).Width;
        TColumnInfo(Data).AsColumn.Index   := Index;
      end;
end;


//===========================================================================//
//  ��� �������� ����� ������ ������ ������� ������
//
//===========================================================================//
procedure Tdlg_Customize_dx_Grid.TreeView1Change(Sender: TObject; Node: TTreeNode);
begin
  edWidth.Text := IntToStr(TColumnInfo(Node.Data).Width);
end;


//===========================================================================//
// ����������� ���� ������ �����
//
//===========================================================================//
procedure Tdlg_Customize_dx_Grid.act_UpExecute(Sender: TObject);
begin
  TreeView1.Selected.MoveTo(TreeView1.Selected.GetPrevSibling, naInsert);
end;

//===========================================================================//
// ����������� ���� ������ ����
//
//===========================================================================//
procedure Tdlg_Customize_dx_Grid.act_DownExecute(Sender: TObject);
begin
  TreeView1.Selected.getNextSibling.MoveTo(TreeView1.Selected, naInsert);
end;

//===========================================================================//
// ��������� ������� �� StateImage - ����� column/band ��� ���
//
//===========================================================================//
procedure Tdlg_Customize_dx_Grid.TreeView1Click(Sender: TObject);
// -------------------------------------------------------------------
var t: THitTests;
begin
  t := TreeView1.GetHitTestInfoAt(TreeView1.ScreenToClient(Mouse.CursorPos).x,
                                  TreeView1.ScreenToClient(Mouse.CursorPos).y);
  if (htOnIcon in t) then
    with TreeView1.Selected do
    begin
      if (Level = 0) AND (TColumnInfo(Data).AsBand.Fixed <> bfNone) then
        exit;

      TColumnInfo(Data).Visible := NOT TColumnInfo(Data).Visible;
      ImageIndex    := integer(TColumnInfo(Data).Visible);
      SelectedIndex := ImageIndex;
    end;
end;

//===========================================================================//
//  ��������� ��������� ������  
//
//===========================================================================//
procedure Tdlg_Customize_dx_Grid.edWidthKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
var  w: integer;
begin
  w:=AsInteger (edWidth.Text);
  if (w > 0) then
    TColumnInfo(TreeView1.Selected.Data).Width := w;
end;

//===========================================================================//
// �������� Action-� �� ��������
//
//===========================================================================//
procedure Tdlg_Customize_dx_Grid.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin
  with TreeView1.Selected do
  begin
    act_Up.Enabled   := (getPrevSibling <> NIL);
    act_Down.Enabled := (getNextSibling <> NIL);
    edWidth.Enabled  := (Level > 0);
  end;
end;




end.
