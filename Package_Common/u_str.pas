unit u_str;

interface

uses
  StrUtils,SysUtils;

procedure ParseStringToDouble3(aStr: string; var aValue1,aValue2,aValue3:
    Single);

procedure ParseStringToString3(aStr: string; var aValue1,aValue2,aValue3:
    string; aDelimiter: string);

implementation

//------------------------------------------------------------------
procedure ParseStringToDouble3(aStr: string; var aValue1,aValue2,aValue3:
    Single);
//------------------------------------------------------------------
var
  i1: Integer;
  i2: Integer;
  s1: string;
  s2: string;
  s3: string;
begin
  aStr:=Trim(aStr);

  aStr:=StringReplace(aStr, '  ',' ',[]);

  i1:=Pos(' ',aStr );
  i2:=PosEx(' ',aStr, i1+1);

   s1:=Copy(aStr,1,i1-1);
   s2:=Copy(aStr,i1+1,i2-i1);
   s3:=Copy(aStr,i2+1,Length(aStr)-i1);


   aValue1:=StrToFloatDef(s1,0);
   aValue2:=StrToFloatDef(s2,0);
   aValue3:=StrToFloatDef(s3,0);


end;


//------------------------------------------------------------------
procedure ParseStringToString3(aStr: string; var aValue1,aValue2,aValue3:
    string; aDelimiter: string);
//------------------------------------------------------------------
var
  i1: Integer;
  i2: Integer;
  s1: string;
  s2: string;
  s3: string;
begin
  aStr:=Trim(aStr);

//  aStr:=StringReplace(aStr, '  ',' ',[]);

  i1:=Pos(aDelimiter,aStr );
  i2:=PosEx(aDelimiter,aStr, i1+1);

   s1:=Copy(aStr,1,i1-1);
   s2:=Copy(aStr,i1+1,i2-i1-1);
   s3:=Copy(aStr,i2+1,Length(aStr)-i1);


   aValue1:=s1;
   aValue2:=s2;
   aValue3:=s3;


end;




end.
