
unit u_db_manager;

//============================================================================//
interface
//============================================================================//
uses Classes,ADODB, DB, SysUtils, Variants, 

   u_DB,
   u_files,
   u_func
    ;
type

  //--------------------------------------------------------------------
  TDBManager = class
  //--------------------------------------------------------------------
  public
    ErrorMsg     : string;

  private
    FSQLBuffer: record SQL: string; MaxCount, Counter: integer; end;

  private
   // FStrList: TStringList;
   // FOnLog  : TOnLogEvent;



    FADOCommand   : TADOCommand;
    FADOStoredProc: TADOStoredProc;
    FQuery        : TADOQuery;
    FADOConnection: TADOCOnnection;

//    procedure DeleteChildren(aTableName: string; aParentID: integer);
    function ExecSP_Result(aProcedureName: string; aParams: array of TDBParamRec): integer;
    function UpdateRecord_(aTableName : string; aID : integer; aParamList:
        TdbParamList): boolean;

//    procedure DoLog (aMsg: string);
//    function MDB_CreateTable(aTableName: String; aDbFieldRecArr: TdbFieldRecArr): boolean;

//    procedure DeleteWithChildren (aTableName: string; aID: integer);

(*    procedure UpdateRecord_SQLSERVER_date (
                               aTableName : string;
                               aID        : integer;
                               aParams    : array of TDBParamRec);
*)
  protected
    function GetQuery: TADOQuery;
    function GetADOStoredProc: TADOStoredProc;
    function GetADOCommand: TADOCommand;

    function GetADOConnection: TADOCOnnection;
    procedure SetADOConnection(Value: TADOCOnnection);
  public
//    DatabaseType: (dbtMSSQL, dbtMSAccess);

    constructor Create(aADOCOnnection: TADOCOnnection);
    destructor Destroy; override;

    function GetMinFieldValue1111111(aTableName,aFieldName: string; aParams: array
        of TDBParamRec): Variant;

//    procedure SET_IDENTITY_INSERT_ON (ATableName: String);
  //  procedure SET_IDENTITY_INSERT_OFF (ATableName: String);

    function ObjectExists (aObjName: string): boolean;

    function GetPosByID (aTableName: string; aID: integer; var aLat,aLon: double): boolean;

    function GetGUIDByName(aTableName: string; aName: integer): string;
    function GetIDByName  (aTableName: string; aName: string): integer;
    function GetNameByID  (aTableName: string; aID: integer): string;
    function GetGUIDByID  (aTableName: string; aID: integer): string;

    function TableExists (aTableName: string): boolean;
    function FieldExist (aTableName, aFieldName: string): boolean;

    function GetMaxFieldValue (aTableName,aFieldName: string): Variant;

    function GetMaxID(aTableName: string; aParams: array of TDBParamRec): integer; overload;

    function GetMaxID(aTableName: string): integer; overload;


    function Blob_SaveToFile(aTableName: string; aID: integer; aFieldName: string; aFileName: string): boolean;
    function Blob_LoadFromFile(aTableName: string; aID: integer; aFieldName: string; aFileName: string): boolean;

    function GetIdentCurrent (aTableName: string): integer;
//    function GetIdent1: integer;

//    function MakeInsertQuery(aTableName: String; aParams: array of TDBParamRec): string;

    //--------------------------------------------------------------------
    // Delete
    //--------------------------------------------------------------------
    function  DeleteRecordByID (aTableName: string; aID: integer): boolean;

    procedure EmptyTable      (aTableName: string); overload;
    procedure EmptyTable      (aTableNameArr: array of string); overload;

    procedure EmptyTableArr   (aTableNameArr: array of string);


    function GetID(aTableName: string; aParams: array of TDBParamRec): integer;


    function GetFieldValueByID (aTableName, aFieldName : string;
                                aID: integer;
                                aDataType: string): Variant;   // = ''

    function GetIntFieldValueByID   (aTableName: string;
                                    aFieldName: string; aID: integer): Integer;

    function GetStringFieldValueByID (aTableName: string;
                                      aFieldName : string;  aID: integer): string;

    function GetDoubleFieldValueByID (aTableName: string;
                                      aFieldName : string;  aID: integer): double;

    function GetFieldValue          (aTableName, aFieldName : string;
                                      aParams    : array of TDBParamRec): Variant;
    function GetStringFieldValue    (aTableName, aFieldName : string;
                                      aParams    : array of TDBParamRec): string;
    function GetIntFieldValue       (aTableName, aFieldName : string;
                                      aParams    : array of TDBParamRec): integer;
    function GetDoubleFieldValue    (aTableName, aFieldName : string;
                                      aParams    : array of TDBParamRec): double;
    function GetDateTimeFieldValue111(aTableName, aFieldName : string; aParams :
        array of TDBParamRec): TDateTime;
    function GetBoolFieldValue      (aTableName, aFieldName : string;
                                      aParams : array of TDBParamRec): Boolean;

    function GetMaxIntFieldValueWP  (aTableName : string;
                                   aFieldName : string;
                                   aParams    : array of TDBParamRec): integer;

    function GetMaxIntFieldValue (aTableName,aFieldName : string): integer;

    function RecordExists      (aTableName: string;
                                aParams:    array of TDBParamRec): boolean;

    function UpdateRecord      (aTableName: string;
                                aID       : integer;
                                aParams   : array of TDBParamRec): boolean;

(*    function UpdateRecordByKeyValue11(aTableName: string; aKeyFieldName: string;
                                    aKeyValue: variant; aParams: array of TDBParamRec): boolean;
*)
(*
    procedure UpdateOrderIndex11111(aTableName : string;
                               aIndexField: string;
                               aParams    : array of TDBParamRec);
*)
    function  AddRecordID     (aTableName    : string;
                               aParams: array of TDBParamRec): integer;

    function AddRecord(aTableName: string; aParams: array of TDBParamRec): Boolean;

    function AddIfNotExists_ByKeyField ( aTableName: string;
                             aKeyField : string;
                             aParams:    array of TDBParamRec
                              ): integer;

    function AddIfNotExists (aTableName: string;
                             aParams   : array of TDBParamRec
                            ): integer;

    procedure MoveRecord   (aADOQuery: TADOQuery;
                            aDirectUp: boolean;
                            aTableName: string;
                            aOrderField: string);

    function AddTableField    (aTableName, aFieldName, aType: string;
                               aSizePrecision,aSizeScale: integer): boolean;

    function DropTableField (aTableName, aFieldName: string): boolean;

//    function GetTableFieldsInfo1111111111(aQuery: TADOQuery; aTableName: string):
//        boolean;

    procedure GetRecordsToList (aTableName: string;
                                aKeyField,aNameField: string;
                                aStrings: TStrings;
                                aParams: array of TDBParamRec);

    function MakeUpdateQuery (aTableName: string;
                               aID: integer; aParams: array of TDBParamRec): string;

//    function CopyRecord (aTableName: String; aID: Integer;
  //                       aParams: array of TDBParamRec): Boolean;
    function DeleteRecords(aTableName: string; aParams: array of TDBParamRec):
        boolean;

    function DeleteRecordsByFieldValue(aTableName: string; aFieldName: string;
        aFieldValue: Variant): boolean;

    function ExecCommand(aSQL: string; aParams: array of TDBParamRec): boolean; overload;
    function ExecCommand(aSQL: string): boolean; overload;

(*    function ExecSP1111(aProcedureName: string; aParams: array of TDBParamRec):
        boolean;
*)
    function MakeInsertQuery(aTableName: String; aParams: array of TDBParamRec):  string;

//    function ValueIsNULL (aTblName, aFldName: string; aParams: Array of TDBParamRec): variant;


    procedure BeginTrans ();

    procedure CommitTrans ();

//    procedure DisableInsertID(ATableName: String);

 //   procedure Disable_IDENTITY_INSERT(ATableName: String);

//    procedure EnableInsertID(ATableName: String);

//    procedure Enable_IDENTITY_INSERT(ATableName: String);
    function ExecCommandF(aSQL: string; aFormatValues: array of const): Boolean;
     function ExecCommandSimple(aSQL: string; aSetSqlCursor: boolean=false): boolean;
    function ExecCommandSimpleCRLF(aSQL: string; aBreakOnError: boolean = true;
        aSetSqlCursor: boolean = false): boolean;
     function ExecCommandSimpleCRLF1(aSQL: string; aBreakOnError: boolean=true; aSetSqlCursor:
         boolean=false): boolean;

    function ExecSQLBuf: boolean; overload;
    function ExecSQLBuf(aSQL: string; aForce: boolean=false): boolean; overload;
    function ExecSQLBuf(aSQL: string; aMaxCount: integer; aForce: boolean=false): boolean;
        overload;

    procedure RollbackTrans ();
    function UpdateViews: Integer;


    property Query: TADOQuery read GetQuery;
    property ADOCommand: TADOCommand read GetADOCommand;
    property ADOStoredProc: TADOStoredProc read GetADOStoredProc;

//    property ADOConnection: TADOCOnnection read GetADOConnection write SetADOConnection;
    property ADOConnection: TADOCOnnection read GetADOConnection;
   // property OnLog: TOnLogEvent read FOnLog write FOnLog;
  end;


//============================================================================//
implementation
//============================================================================//


var
  LDataSetPositionArr: TIntArray;



function ReplaceStr (Value, OldPattern, NewPattern: string): string;
begin
  Result:=StringReplace (Value, OldPattern, NewPattern, [rfReplaceAll]);
end;

function Eq (Value1,Value2 : string): boolean;
begin
  Result := AnsiCompareText(Trim(Value1),Trim(Value2))=0;
end;


constructor TDBManager.Create(aADOCOnnection: TADOCOnnection);
begin
  inherited Create;

  setADOConnection(aadOCOnnection);
end;

// ---------------------------------------------------------------
// TDBManager
// ---------------------------------------------------------------
(*
constructor TDBManager.Create;
begin
  inherited;

end;*)

destructor TDBManager.Destroy;
begin
  FreeAndNil(FQuery);
  FreeAndNil(FADOCommand);
  FreeAndNil(FADOStoredProc);


  inherited;
end;

//------------------------------------------------------------------
procedure TDBManager.SetADOConnection(Value: TADOCOnnection);
//------------------------------------------------------------------
begin
  FADOCOnnection:=Value;

  if not Assigned(Query) then
    FQuery:=TADOQuery.Create(nil);

  if not Assigned(ADOCommand) then
    FADOCommand:=TADOCommand.Create(nil);

  if not Assigned(FADOStoredProc) then
    FADOStoredProc:=TADOStoredProc.Create(nil);

  FQuery.ParamCheck:=True;
  FADOCommand.ParamCheck:=True;

  FQuery.Connection:=FADOCOnnection;
 // FQuery.CursorLocation:=clUseServer;

  FADOCommand.Connection:=FADOConnection;
  FADOStoredProc.Connection:=FADOConnection;


end;


//------------------------------------------------------------------
function TDBManager.GetMaxFieldValue (aTableName,aFieldName: string): Variant;
//------------------------------------------------------------------
var sWhere,sSQL: string;
begin
  sSQL:=Format('SELECT Max(%s) from %s', [aFieldName,aTableName]);
  db_OpenQuery (Query, sSQL);

  Result:=Query.Fields[0].Value; //  Integer;
end;



//------------------------------------------------------------------
function TDBManager.Blob_SaveToFile(aTableName: string; aID: integer; aFieldName: string; aFileName: string): Boolean;
//    aIsCompressed: boolean): boolean;
//------------------------------------------------------------------
var sSQL: string;
    oBlobField: TBlobField;
begin
  Assert (aFieldName<>'', 'aFieldName=''''');
  Assert (aTableName<>'', 'aTableName=''''');

  Query.Close;

  oBlobField:=TBlobField.Create(nil);
  oBlobField.FieldName:=aFieldName;

  oBlobField.DataSet:=Query;

  sSQL:=Format('SELECT %s FROM %s WHERE id=%d', [aFieldName,aTableName,aID]);
  db_OpenQuery (Query, sSQL);


 // Result:=Query.Fields[0].Value;

  ForceDirByFileName (aFileName);

  try
    oBlobField.SaveToFile (aFileName);

  {  
  if aIsCompressed then
  begin
    sFile:=GetTempFileName_('');
    zlib_CompressFile(aFileName, sFile);

    aFileName:=sFile;
  end;

}
    Result := True;
  except
    Result := False;
  end;

  oBlobField.Free;
end;

//------------------------------------------------------------------
function TDBManager.GetMaxID  (aTableName: string;
                               aParams: array of TDBParamRec): integer;
//------------------------------------------------------------------
var sWhere,sSQL: string;
begin
  sWhere:=db_MakeWhereQuery (aParams);
  if sWhere<>'' then sWhere:=' WHERE ' + sWhere;

  sSQL:=Format('SELECT Max(id) from %s ', [aTableName]) + sWhere;
  db_OpenQuery (Query, sSQL, aParams);

  Result:=Query.Fields[0].AsInteger;
end;

function TDBManager.GetMaxID(aTableName: string): integer;
begin
  Result:=GetMaxID (aTableName, [EMPTY_PARAM]);
end;


//===========================================================================//
//  ���������� ���� � ������� ��
//------------------------------------------------------------------
function TDBManager.AddTableField  (
                            aTableName, aFieldName, aType: string;
                            aSizePrecision,aSizeScale: integer): boolean;
//------------------------------------------------------------------
var sql, s: string;
begin
  if aSizePrecision=-1 then s:='' else s:=IntToStr(aSizePrecision);
  if aSizeScale<>-1 then s:=s+','+IntToStr(aSizeScale);
  if s<>'' then s:='('+s+')';

  sql:='ALTER TABLE '+aTableName+' ADD '+aFieldName+' '+aType+s+' NULL';
  try
    FADOConnection.Execute(sql);
    result:=true;
  except
    result:=false;
  end;
end;

//==================================================================
//  �������� ���� �� ������� ��
//------------------------------------------------------------------
function TDBManager.DropTableField (aTableName, aFieldName: string): boolean;
//------------------------------------------------------------------
begin
  try
    FADOConnection.Execute (Format('ALTER TABLE %s DROP COLUMN %s', [aTableName,aFieldName]));
    result:=true;
  except
    result:=false;
  end;
end;

(*
//------------------------------------------------------------------
function TDBManager.GetTableFieldsInfo1111111111(aQuery: TADOQuery; aTableName:
    string): boolean;
//------------------------------------------------------------------
const
  SQL_SELECT_TABLE_FIELDS =
    ' SELECT'+
    '   sysobjects.id AS table_id,'+
    '   sysobjects.name AS tablename,'+
    '   syscolumns.name,'+
    '   systypes.name AS type,'+
    '   syscolumns.length AS size,'+
    '   syscolumns.domain AS Domain_id,'+
    '   systypes.variable AS Variable,'+
    '   systypes.usertype AS Usertype,'+
    '   syscolumns.isnullable AS NULLS'+
    ' FROM sysobjects'+
    '   INNER JOIN syscolumns ON sysobjects.id = syscolumns.id'+
    '   INNER JOIN systypes ON syscolumns.xusertype = systypes.xusertype'+
    '   WHERE (sysobjects.xtype = ''U'') and (sysobjects.name = :tablename)'+
    ' ORDER BY sysobjects.name';

begin
  Result:=true;

  db_OpenQuery (aQuery, SQL_SELECT_TABLE_FIELDS,
               [db_Par(FLD_TABLENAME, aTableName)]);

  if aQuery.IsEmpty then
    Result:=false;

end;
*)


// ---------------------------------------------------------------
function TDBManager.RecordExists (
                          aTableName: string;
                          aParams:    array of TDBParamRec
                         ): boolean;
//------------------------------------------------------------------
var sql: string;
begin
  Assert (ADOConnection<>nil, 'ADOConnection=nil');

  sql:=Format('SELECT Count(*) FROM %s WHERE %s',
              [aTableName, db_MakeWhereQuery (aParams)]);

  db_OpenQuery (Query, sql, aParams);
  Result:=(Query.Fields[0].AsInteger>0);
end;

//===========================================================================//
function TDBManager.AddIfNotExists_ByKeyField (
                          aTableName: string;
                          aKeyField : string;
                          aParams   :    array of TDBParamRec
                         ): integer;
//------------------------------------------------------------------
begin
  if not RecordExists (aTableName, aParams) then
    AddRecord (aTableName, aParams);

  Result:=GetFieldValue (aTableName, aKeyField, aParams);

end;

//------------------------------------------------------------------
function TDBManager.AddIfNotExists (
                          aTableName: string;
                          aParams   : array of TDBParamRec
                         ): integer;
//------------------------------------------------------------------
begin
  if not RecordExists (aTableName, aParams) then
    Result:= AddRecordID (aTableName, aParams);
end;


//------------------------------------------------------------------------------
function TDBManager.ExecCommandF(aSQL: string; aFormatValues: array of const): Boolean;
//------------------------------------------------------------------------------
var sSql: string;
begin
  sSql:= Format(aSQL, aFormatValues);

  try
    ADOCommand.CommandText:= sSql;
  except
    sSql:= sSql;
  end;

  try
    ADOCommand.Execute;
    Result:=True;
  except
    on E: Exception do begin
      ErrorMsg:= E.Message;
//      g_Log.AddExceptionWithSQL('u_func_db.ExecCommand', E.Message, sSql);
      Result:=False;
    end;
  end;

//  if Assigned(RecordSet) then
//    RecordSet.Close;
end;


//----------------------------------------------------------------------------
function TDBManager.MakeUpdateQuery (aTableName: string;
                                     aID       : integer;
                                     aParams   : array of TDBParamRec): string;
//----------------------------------------------------------------------------
var str,sParamStr: string;  i: integer;
begin
  //db_MakeUpdateString

  sParamStr:='';
  for i:=0 to High(aParams) do
  begin
    str:=Format('%s=:%s', [aParams[i].FieldName,aParams[i].FieldName]);
    if i=0 then sParamStr:=str
           else sParamStr:=sParamStr + ',' + str;
  end;

  { TODO : ����! �������� ��� ��������� � ����. ����� ����� !!! }
  Assert(aID>0, 'TDBManager.MakeUpdateQuery: aID=0');

  if aID>0 then
    Result:=Format('UPDATE %s SET %s WHERE id=%d', [aTableName, sParamStr,aID]);
//  else
  //  Result:=Format('UPDATE %s SET %s', [aTableName, sParamStr])

end;

//----------------------------------------------------------------------------
function TDBManager.UpdateRecord ( aTableName : string;
                                   aID        : integer;
                                   aParams    : array of TDBParamRec): boolean;
//----------------------------------------------------------------------------
var
  s: string;
begin
  Assert (aTableName<>'', 'function TDBManager.UpdateRecord - aTableName=''''');

//  if aTableName='' then
  //  raise Exception.Create('function TDBManager.UpdateRecord - aTableName=''''');

  try
    s:= MakeUpdateQuery (aTableName, aID, aParams);
    if s <> '' then
      try
        ExecCommand (s, aParams);
      except
      //  on E: Exception do gl_Log.AddRecord('u_db', 'UpdateRecord', E.Message);
      end;

    Result:=False;
  except
    Result:=True;
  end;
end;


//----------------------------------------------------------------------------
function TDBManager.UpdateRecord_(aTableName : string; aID : integer;
    aParamList: TdbParamList): boolean;
//----------------------------------------------------------------------------
var
  s: string;
begin
  Assert (aTableName<>'', 'function TDBManager.UpdateRecord - aTableName=''''');

//  if aTableName='' then
  //  raise Exception.Create('function TDBManager.UpdateRecord - aTableName=''''');

 // try
(*  s:=db_MakeUpdateString(aTableName, aID, aParamList);

  Result := ExecCommand (s, aParams);
*)
 //   s:= MakeUpdateQuery (aTableName, aID, aParams);
  //  if s <> '' then

(*      try

      except
      //  on E: Exception do gl_Log.AddRecord('u_db', 'UpdateRecord', E.Message);
      end;

    Result:=False;
  except
    Result:=True;
  end;
*)
end;

(*
//----------------------------------------------------------------------------
function TDBManager.UpdateRecordByKeyValue11 (aTableName: string;
                                  aKeyFieldName: string; aKeyValue: variant;
                                  aParams: array of TDBParamRec): boolean;
//----------------------------------------------------------------------------
var
  s: string;
  sID, str,sParamStr: string;
  iID, i: integer;
begin
  Result:=False;

  case VarType(aKeyValue) of
    varInteger: iID:= aKeyValue;
    varString:  sID:= aKeyValue;
  else
    raise Exception.Create('aKeyValue=?');
  end;

  sParamStr:='';
  for i:=0 to High(aParams) do
  begin
    str:= Format('%s=:%s', [aParams[i].FieldName, aParams[i].FieldName]);
    if i=0 then sParamStr:= str
           else sParamStr:= sParamStr + ',' + str;
  end;

  case VarType(aKeyValue) of
    varInteger: s:= Format('UPDATE %s SET %s WHERE %s=%d', [aTableName, sParamStr, aKeyFieldName, iID]);//iID:= aID;
    varString:  s:= Format('UPDATE %s SET %s WHERE %s=''%s''', [aTableName, sParamStr, aKeyFieldName, sID]);//sID:= aID;
  end;

  try
    ExecCommand (s, aParams);
  except
   // on E: Exception do gl_Log.AddRecord('u_db', 'UpdateRecord', E.Message);
  end;

  Result:=True;
end;
*)

(*
//----------------------------------------------------------------------------
procedure TDBManager.UpdateRecord_SQLSERVER_date (
                           aTableName : string;
                           aID        : integer;
                           aParams    : array of TDBParamRec);
//----------------------------------------------------------------------------
var substr,sql: string;  i: integer;
begin
  sql:=MakeUpdateQuery (aTableName, aID, aParams);

  try
  for i:=0 to High(aParams) do begin
    substr:=aParams[i].FieldValue;
    sql:=ReplaceStr (sql, ':'+aParams[i].FieldName,  substr);
  end;
  except end;

  ExecCommand (sql, aParams);

end;
*)


//--------------------------------------------------------------------
function TDBManager.DeleteRecordByID (aTableName  : string;
                                  aID: integer): boolean;
//--------------------------------------------------------------------
begin
  Assert (aTableName<>'', 'aTableName=''''');

  Result:=ExecCommand ('DELETE FROM ' + aTableName + ' WHERE id=:id',
              [db_Par('id', aID)]);
end;

//----------------------------------------------------------------------------
procedure TDBManager.EmptyTable (aTableName: string);
//----------------------------------------------------------------------------
begin
  ExecCommand ('DELETE FROM ' + aTableName);//, [EMPTY_PARAM]);
end;

//----------------------------------------------------------------------------
procedure TDBManager.EmptyTable(aTableNameArr: array of string);
//----------------------------------------------------------------------------
var
  i: Integer;
begin
  for I := 0 to High(aTableNameArr) do
    EmptyTable(aTableNameArr[i]);
end;


//----------------------------------------------------------------------------
procedure TDBManager.EmptyTableArr(aTableNameArr: array of string);
//----------------------------------------------------------------------------
var
  i: Integer;
begin
  for I := 0 to High(aTableNameArr) do
    EmptyTable(aTableNameArr[i]);
end;

//-------------------------------------------------------------------
//  only for MS SQL server
function  TDBManager.ObjectExists (aObjName: string): boolean;
//-------------------------------------------------------------------
var
  oQuery: TAdoQuery;
begin
  Result:= false;

  oQuery := TAdoQuery.Create(nil);
  oQuery.Connection:= ADOConnection;

  db_OpenQuery (oQuery, 'SELECT OBJECT_ID ('''+aObjName+''') AS id');

  Result:=not VarIsNull(oQuery[FLD_ID]);

  oQuery.Free;
end;

//----------------------------------------------------------------------------
function TDBManager.MakeInsertQuery (aTableName: string;
                         aParams:    array of TDBParamRec): string;
//----------------------------------------------------------------------------
var sParamStr,sValueStr: string;  i: integer;  bFirst: boolean;
begin
  sParamStr:='';
  sValueStr:='';
  bFirst:=True;

  for i:=0 to High(aParams) do
    if aParams[i].FieldValue <> NULL then
  begin
{     if Eq(aParams[i].FieldName,'guid') then
       aParams[i].FieldName:='['+aParams[i].FieldName+']';
}

    if not bFirst then begin
      sParamStr:=sParamStr + ',';
      sValueStr:=sValueStr + ',';
    end;
    bFirst:=False;

    sParamStr:=sParamStr + '['+aParams[i].FieldName+']';//aParams[i].FieldName;
    sValueStr:=sValueStr + ':' + aParams[i].FieldName;
  end;

  Result:=Format('INSERT INTO %s (%s) VALUES (%s)', [aTableName, sParamStr, sValueStr]);
end;


//----------------------------------------------------------------------------
function TDBManager.AddRecord(aTableName: string; aParams: array of TDBParamRec): Boolean;
//----------------------------------------------------------------------------
var sql: string;
begin
  sql:=MakeInsertQuery (aTableName, aParams);
  Result := ExecCommand (sql, aParams);
end;

//----------------------------------------------------------------------------
function TDBManager.AddRecordID (aTableName: string;
                                 aParams:    array of TDBParamRec
                                ): integer;
//----------------------------------------------------------------------------
begin
  if AddRecord (aTableName, aParams) then
    Result:= GetIdentCurrent(aTableName)
  else
    Result := 0;
end;

//----------------------------------------------------------------------------
function TDBManager.GetIdentCurrent (aTableName: string): integer;
//----------------------------------------------------------------------------
begin
  try
    db_OpenQuery (Query, Format('SELECT IDENT_CURRENT(''%s'')',[aTableName]));

    result:=Query.Fields[0].AsInteger;
    if result=0 then
      Raise Exception.Create('GetIdentCurrent is not working');

  except
    result:=0;
  end;
end;


function TDBManager.GetMaxIntFieldValue (aTableName,aFieldName : string): integer;
begin
  Result:=GetMaxIntFieldValueWP (aTableName,aFieldName, [EMPTY_PARAM]);
end;

//--------------------------------------------------------------------
function TDBManager.GetFieldValue (
                              aTableName : string;
                              aFieldName : string;
                              aParams    : array of TDBParamRec): Variant;
//--------------------------------------------------------------------
var i,j: integer;
//    oParam: TParameter;
    sql,sWhere: string;
begin
  if aTableName='' then
    raise Exception.Create('TableName=''''');

  Assert(Length(aFieldName)>0);
  Assert(LowerCase(aFieldName)[1] in ['a'..'z']) ;


  sWhere:=db_MakeWhereQuery(aParams);
  sql:=Format('SELECT %s FROM %s', [aFieldName, aTableName]);
  if sWhere<>'' then sql:=sql + ' WHERE ' + sWhere;

  db_OpenQuery (Query, sql, aParams);
  if Query.IsEmpty then
    Result:=NULL
  else
    Result:=Query.Fields[0].Value;
end;

//--------------------------------------------------------------------
function TDBManager.GetFieldValueByID(aTableName, aFieldName : string; aID:
    integer; aDataType: string): Variant;
//--------------------------------------------------------------------
var i,j: integer;
//    oParam: TParameter;
    sql,sWhere: string;
begin
  if aTableName='' then
    raise Exception.Create('TableName=''''');

  sql:=Format('SELECT %s FROM %s', [aFieldName, aTableName]);
  sql:=sql + ' WHERE id = :id';

  db_OpenQuery (Query, sql, [db_Par(FLD_ID, aID)]);
  if Query.IsEmpty then
    Result:=IIF(aDataType = STRING_, '', 0)
  else
  begin
    Result:=Query.Fields[0].Value;

    if (Result = NULL) then
      Result:=IIF(aDataType = STRING_, '', 0)
  end;
end;

//--------------------------------------------------------------------
function TDBManager.GetID(aTableName: string; aParams: array of TDBParamRec):
    integer;
//--------------------------------------------------------------------
begin
  Result:= GetIntFieldValue(aTableName, FLD_ID, aParams);
end;

function TDBManager.GetStringFieldValueByID (aTableName: string;
                                             aFieldName : string;
                                             aID: integer): string;
begin
  Result:=GetStringFieldValue (aTableName, aFieldName, [db_par(FLD_ID, aID)] );
end;

function TDBManager.GetDoubleFieldValueByID (aTableName: string;
                                             aFieldName : string;
                                             aID: integer): Double;
begin
  Result:=GetDoubleFieldValue (aTableName, aFieldName, [db_par(FLD_ID, aID)] );
end;


//--------------------------------------------------------------------
function TDBManager.GetStringFieldValue (
                              aTableName : string;
                              aFieldName : string;
                              aParams    : array of TDBParamRec): string;
//--------------------------------------------------------------------
var v: Variant;
begin
  v:=GetFieldValue (aTableName, aFieldName, aParams);
  if VarExists(v) then Result:=v
                  else Result:='';
end;

//--------------------------------------------------------------------
function TDBManager.GetDateTimeFieldValue111(aTableName, aFieldName : string;
    aParams : array of TDBParamRec): TDateTime;
//--------------------------------------------------------------------
var v: Variant;
  dt: TDateTime;
begin
  v:=GetFieldValue (aTableName, aFieldName, aParams);
  if VarExists(v) then begin
                     dt:=v;
                     Result:=v;
                  end
                  else Result:=0;
end;

//--------------------------------------------------------------------
function TDBManager.GetIntFieldValue (
                              aTableName, aFieldName : string;
                              aParams    : array of TDBParamRec): integer;
//--------------------------------------------------------------------
var v: Variant;
begin
  assert(aTableName<>'');

  v:=GetFieldValue (aTableName, aFieldName, aParams);
  if VarExists(v) then Result:=v
                  else Result:=0;
end;

//--------------------------------------------------------------------
function TDBManager.GetDoubleFieldValue (
                              aTableName, aFieldName : string;
                              aParams    : array of TDBParamRec): double;
//--------------------------------------------------------------------
var v: Variant;
begin
  v:=GetFieldValue (aTableName, aFieldName, aParams);
  if VarExists(v) then Result:=v
                  else Result:=0;
end;

//--------------------------------------------------------------------
function TDBManager.GetMaxIntFieldValueWP (
                              aTableName, aFieldName : string;
                              aParams    : array of TDBParamRec): integer;
//--------------------------------------------------------------------
var i,j: integer;
    sql,sWhere: string;
begin
  sWhere:=db_MakeWhereQuery(aParams);
  sql:=Format('SELECT MAX(%s) as max_value FROM %s', [aFieldName, aTableName]);
  if sWhere<>'' then sql:=sql + ' WHERE ' + sWhere;

  db_OpenQuery (Query, sql, aParams);
  Result:=Query.FieldByName ('max_value').AsInteger;

// SQL_SELECT_ORDER_PAGE_OBJS = 'SELECT MAX(showorder) FROM _page_objects WHERE (
// page_id=%d)';

end;

//--------------------------------------------------------------------
// Get BLPoint from table
//--------------------------------------------------------------------
function TDBManager.GetPosByID(aTableName: string; aID: integer; var aLat,aLon:
    double): boolean;
//--------------------------------------------------------------------
begin
  db_OpenQuery (Query,
       Format('SELECT lat,lon FROM %s WHERE id=:id', [aTableName]),
      [db_Par(FLD_ID, aID)]);

  with Query do
    if not IsEmpty then begin
      aLat:=FieldByName(FLD_LAT).AsFloat;
      aLon:=FieldByName(FLD_LON).AsFloat;
      Result:=True;

    end else begin
      aLat:=0;
      aLon:=0;

      Result:=False;
    end;

end;

// ---------------------------------------------------------------
//procedure TDBManager.UpdateOrderIndex11111 (
//                               aTableName : string;
//                               aIndexField: string;
//                               aParams    : array of TDBParamRec);
// ---------------------------------------------------------------
//var iIndex, id_parent, id: integer;
//  sql,sql1,sql_main,sql_update,sWhere: string;
//  oQuery1,oQuery2: TADOQuery;
//const
//  SQL_SELECT_MAIN = 'SELECT distinct parent_id FROM :table WHERE :where ORDER BY parent_id';
//  SQL_SELECT      = 'SELECT id FROM :table WHERE :where  and (:parent) ORDER BY ';
//
//  SQL_WHERE_NULL_PARENT ='ISNULL(parent_id, 0) = 0';
//
//begin
//
//  oQuery1:=TADOQuery.Create(nil);
//  oQuery1.Connection:=FADOConnection;
//
//  oQuery2:=TADOQuery.Create(nil);
//  oQuery2.Connection:=FADOConnection;
//
//
//  sWhere:=db_MakeWhereQuery (aParams);
//  sql:=ReplaceStr (SQL_SELECT + aIndexField, ':table', aTableName);
//  sql:=ReplaceStr (sql, ':where', sWhere);
//
//  sql_main:=ReplaceStr (SQL_SELECT_MAIN, ':table', aTableName);
//  sql_main:=ReplaceStr (sql_main, ':where', sWhere);
//
//  db_OpenQuery(oQuery1, sql_main, aParams);
//
//  while not oQuery1.Eof do
//  begin
//    id_parent:=oQuery1.FieldByName(FLD_PARENT_ID).AsInteger;
//
//    if id_parent>0
//      then sql1:=ReplaceStr (sql, ':parent', Format('parent_id=%d', [id_parent]))
//      else sql1:=ReplaceStr (sql, ':parent', SQL_WHERE_NULL_PARENT);
//
//    db_OpenQuery (oQuery2, sql1, aParams);
//
//    iIndex:=0;
//    while not oQuery2.Eof do
//    begin
//      id:=oQuery2.FieldByName(FLD_ID).AsInteger;
//      sql_update:=MakeUpdateQuery (aTableName, id, [db_Par(aIndexField, 0)]);
//
//      Inc(iIndex);
//      ExecCommand (sql_update,
//                     [db_Par(aIndexField, iIndex)]);
//
//      oQuery2.Next;
//    end;
//
//    oQuery1.Next;
//  end;
//
//  oQuery1.Free;
//  oQuery2.Free;
//end;


//============================================================================//
procedure TDBManager.MoveRecord (aADOQuery: TADOQuery;
                                 aDirectUp  : boolean;
                                 aTableName : string;
                                 aOrderField: string);
//============================================================================//
var cur_id, new_id, cur_order, new_order: integer;
begin
  with aADOQuery do
  begin
    cur_order:=FieldByName(aOrderField).AsInteger;
    cur_id   :=FieldByName(FLD_ID).AsInteger;

    if aDirectUp=True then begin
      Prior;
      if Bof then Exit;
    end else begin
      Next;
      if Eof then Exit;
    end;

    new_order:=FieldByName(aOrderField).AsInteger;
    new_id   :=FieldByName(FLD_ID).AsInteger;
  end;

  UpdateRecord(aTableName, cur_id, [db_Par (aOrderField, new_order)]);
  UpdateRecord(aTableName, new_id, [db_Par (aOrderField, cur_order)]);

  db_RefreshQuery (aADOQuery, cur_id);

//  aADOQuery.Locate(FLD_ID, )
end;

{
procedure TDBManager.DoLog(aMsg: string);
begin
  if Assigned(FOnLog) then  FOnLog (aMsg);
end;}

//-------------------------------------------------------------------
function TDBManager.TableExists (aTableName: string): boolean;
//-------------------------------------------------------------------
var
  oStrList: TStringList;
begin
  oStrList:=TStringList.Create;

  FADOConnection.GetTableNames (oStrList);
  Result:=(oStrList.IndexOf (aTableName) >=0 );

  FreeAndNil(oStrList);
end;

//-------------------------------------------------------------------
function TDBManager.FieldExist (aTableName, aFieldName: string): boolean;
//-------------------------------------------------------------------
var
  oFieldList: TStringList;
begin
  oFieldList:= TStringList.Create;

  Result:= false;

  FADOConnection.GetFieldNames (aTableName, oFieldList);

  if oFieldList.IndexOf(aFieldName) > 0 then
    Result:=true;

  oFieldList.Free;
end;

//--------------------------------------------------------------------
function TDBManager.GetGUIDByID (aTableName: string; aID: integer): string;
//--------------------------------------------------------------------
begin
  Assert (aTableName<>'','aTableName=''''');

 // if FieldExist(aTableName, FLD_GUID) then
  Result:= GetFieldValueByID (aTableName, FLD_GUID, aID, STRING_);
//  else
  //  Result:= '';
end;

//--------------------------------------------------------------------
procedure TDBManager.GetRecordsToList(aTableName: string;
  aKeyField, aNameField: string;
  aStrings: TStrings; aParams: array of TDBParamRec);
//--------------------------------------------------------------------
begin
  db_OpenQuery (Query, 'SELECT * FROM '+ aTableName, aParams);

  db_GetRecordsToStrings (Query, aKeyField, aNameField, aStrings);
end;


function TDBManager.GetIDByName (aTableName: string; aName: string): integer;
begin
  Result:=GetIntFieldValue (aTableName, FLD_ID, [db_par(FLD_NAME, aNAME)] );
end;

function TDBManager.GetGUIDByName (aTableName: string; aName: integer): string;
begin
  Result:=GetStringFieldValue (aTableName, FLD_GUID, [db_par(FLD_NAME, aNAME)] );
end;

function TDBManager.GetNameByID (aTableName: string; aID: integer): string;
begin
  Result:=GetStringFieldValue (aTableName, FLD_NAME, [db_par(FLD_ID, aID)] );
end;

//----------------------------------------------------------------------------
function TDBManager.Blob_LoadFromFile(aTableName: string; aID: integer; aFieldName: string; aFileName: string): boolean;
//    aIsCompressed: boolean);
//----------------------------------------------------------------------------
var sSQL: string;
  sFile: string;
begin
  if not FileExists(aFileName) then
    raise Exception.Create('File not exist: '+ aFileName);

  sSQL:=Format('UPDATE %s SET %s=:%s WHERE id=:id',
          [aTableName,aFieldName,aFieldName]);

{
  if aIsCompressed then
  begin
    sFile:=GetTempFileName_('');
    zlib_CompressFile(aFileName, sFile);

    aFileName:=sFile;
  end;}


{

   gl_DB.Blob_SaveToFile(TableName, iID, FLD_CONTENT, sFile);

   DeleteFile(sFile);
}


  with FADOCommand do
  begin
    CommandText := sSQL;
    Parameters.ParamByName(FLD_ID).Value :=aID;
    Parameters.ParamByName(aFieldName).LoadFromFile(aFileName, ftBlob);//ftMemo); // Blob);
    Execute;
  end;

//  if aIsCompressed then
 //   DeleteFile (aFileName);




end;


//-------------------------------------------------------------------
function TDBManager.ExecSP_Result(aProcedureName: string; aParams: array of TDBParamRec):
    integer;
//-------------------------------------------------------------------
var
  i: integer;
begin
 // Assert (aObjectName<>'');


  FADOStoredProc.Close;
  FADOStoredProc.ProcedureName:=aProcedureName;

  with FADOStoredProc.Parameters do
  begin
    Refresh;

    for i := 0 to High(aParams) do
      ParamByName('@a'+aParams[i].FieldName).Value  := aParams[i].FieldValue;

    ParamByName('@RESULT').Value    := 0;
  end;


  try
    FADOStoredProc.ExecProc;
    result:= FADOStoredProc.Parameters.ParamByName('@RESULT').Value;

  //  Result:=True;
  except
     Result:=-1;
  end;



 // Result:=True;
//  Assert(Result<>-1, 'aObjectName:'+aObjectName);

end;

//----------------------------------------------------------------------------
function TDBManager.DeleteRecords(aTableName: string; aParams: array of
    TDBParamRec): boolean;
//----------------------------------------------------------------------------
begin
  ExecCommand ('DELETE FROM ' + aTableName + ' WHERE ' +
               db_MakeWhereQuery (aParams), aParams );
end;

//----------------------------------------------------------------------------
function TDBManager.DeleteRecordsByFieldValue(aTableName: string; aFieldName:
    string; aFieldValue: Variant): boolean;
//----------------------------------------------------------------------------
begin
  Result:=DeleteRecords (aTableName, [db_Par(aFieldName,aFieldValue)]);
end;


//--------------------------------------------------------------------
function TDBManager.ExecCommand(aSQL: string; aParams: array of TDBParamRec):    boolean;
//--------------------------------------------------------------------
begin
  result :=db_ExecCommand (ADOConnection, aSQL, aParams);
end;

//--------------------------------------------------------------------
function TDBManager.ExecCommand(aSQL: string): boolean;
//--------------------------------------------------------------------
begin
  result := ExecCommand (aSQL, EMPTY_PARAM);
end;




function TDBManager.GetADOConnection: TADOCOnnection;
begin
  Result := FADOConnection;
end;

function TDBManager.GetADOStoredProc: TADOStoredProc;
begin
  Result := FADOStoredProc;
end;

function TDBManager.GetADOCommand: TADOCommand;
begin
  result := FADOCommand;
end;


//------------------------------------------------------------------------------
function TDBManager.GetMinFieldValue1111111(aTableName,aFieldName: string;
    aParams: array of TDBParamRec): Variant;
//------------------------------------------------------------------------------
var sSQL: string;
begin
  sSQL:= format('SELECT MIN(%s) As MIN FROM %s', [aFieldName,aTableName]);

  try
    db_OpenQuery (Query, sSQL, aParams);
    Result:=Query.Fields[0].Value;
  except
    Result:=0;
  end;
end;


(*
procedure TDBManager.SET_IDENTITY_INSERT_OFF(ATableName: String);
begin
  raise Exception.Create('SET_IDENTITY_INSERT_OFF(ATableName: String);');
  FADOConnection.Execute ('SET IDENTITY_INSERT '+ATableName+' OFF');
end;

procedure TDBManager.SET_IDENTITY_INSERT_ON(ATableName: String);
begin
  raise Exception.Create('SET_IDENTITY_INSERT_ON(ATableName: String);');
  FADOConnection.Execute ('SET IDENTITY_INSERT '+ATableName+' ON');
end;
*)
//--------------------------------------------------------------------
function TDBManager.GetBoolFieldValue(aTableName, aFieldName : string; aParams
    : array of TDBParamRec): Boolean;
//--------------------------------------------------------------------
var v: Variant;
begin
  v:=GetFieldValue (aTableName, aFieldName, aParams);
  if VarExists(v) then Result:=v
                  else Result:=False;
end;


//------------------------------------------------------------------------------
function TDBManager.ExecCommandSimpleCRLF(aSQL: string; aBreakOnError: boolean = true;
    aSetSqlCursor: boolean = false): boolean;
//------------------------------------------------------------------------------
var
  i: Integer;
  oStrList: TStringList;
begin
  Result:= true;

  oStrList:= TStringList.Create;
  oStrList.Text:= aSql;

  for I := 0 to oStrList.Count-1 do
    if not ExecCommand (oStrList[i]) then
    begin
//    if not ExecCommandSimple (oStrList[i], aSetSqlCursor) then begin
      Result:= false;
      if aBreakOnError then
        break;
    end;

  oStrList.Free;
end;


//--------------------------------------------------------------------
function TDBManager.GetIntFieldValueByID(aTableName: string; aFieldName:
    string; aID: integer): Integer;
//--------------------------------------------------------------------
begin
  Result := GetFieldValueByID(aTableName, aFieldName, aID, '');
end;

function TDBManager.GetQuery: TADOQuery;
begin
  Result := FQuery;
end;

{
//--------------------------------------------------------------------
function TDBManager.ValueIsNULL (aTblName, aFldName: string;
                                 aParams: Array of TDBParamRec): variant;
//--------------------------------------------------------------------
var
  oQuery: TAdoQuery;
  S: string;
  i: Integer;
begin
  Result:= NULL;

  oQuery:= TAdoQuery.Create(nil);
  oQuery.Connection:= ADOConnection;

  db_OpenTable(oQuery, aTblName, aParams);

  if oQuery.IsEmpty then
    exit;

  if oQuery.RecordCount>1 then
  begin
    s:= Format('������ ��������� ������ ����� ������. ������� %s.', [aTblName]);
    s:= s+CRLF+' ���������: ';
    for i:=0 to High(aParams) do
      s:= s + aParams[i].FieldName+' - '+AsString(aParams[i].FieldValue)+' ; ';
   // gl_Log.AddRecord('TDBManager.ValueIsNULL', s);
    exit;
  end;

  Result:= oQuery[aFldName];

  oQuery.Free;
end;

 }
// -------------------------------------------------------------------
procedure TDBManager.BeginTrans ();
// -------------------------------------------------------------------
begin
  if not FADOConnection.InTransaction then
    FADOConnection.BeginTrans;
end;

// -------------------------------------------------------------------
procedure TDBManager.CommitTrans ();
// -------------------------------------------------------------------
begin
  if FADOConnection.InTransaction then
    FADOConnection.CommitTrans;
end;

//------------------------------------------------------------------------------
function TDBManager.ExecCommandSimple(aSQL: string; aSetSqlCursor: boolean = false):
    boolean;
//------------------------------------------------------------------------------
var
  oADOCommand: TADOCommand;
begin
  try
    Result:=false;

    if aSetSqlCursor then
       CursorSql;

    oADOCommand:= TADOCommand.Create(nil);
    oADOCommand.Connection     := ADOConnection;
    oADOCommand.ParamCheck     := false;
    oADOCommand.CommandText    := aSQL;
    oADOCommand.CommandTimeout := 240;
    oADOCommand.Execute;
    Result:=true;

    oADOCommand.ParamCheck:= true;

    if aSetSqlCursor then
      CursorRestore;

    oADOCommand.Free;
  except
    on e: Exception do begin
      ErrorMsg:= E.Message;

    //  gl_ErrorLog.AddExceptionWithSQL('u_func_db.TDBManager.ExecCommandSimple', E.Message, aSQL);

      if aSetSqlCursor then
        CursorRestore;
    end;
  end;

//  if Assigned(RecordSet) then
//    RecordSet.Close;
end;

//------------------------------------------------------------------------------
function TDBManager.ExecCommandSimpleCRLF1(aSQL: string; aBreakOnError: boolean = true;
    aSetSqlCursor: boolean = false): boolean;
//------------------------------------------------------------------------------
var
  i: Integer;
  oStrList: TStringList;
begin
  Result:= true;

  oStrList:= TStringList.Create;
  oStrList.Text:= aSql;
  for I := 0 to oStrList.Count-1 do begin
    if not ExecCommandSimple (oStrList[i], aSetSqlCursor) then begin
      Result:= false;
      if aBreakOnError then
        break;
    end;
  end;
  oStrList.Free;
end;

//------------------------------------------------------------------------------
function TDBManager.ExecSQLBuf: boolean;
//------------------------------------------------------------------------------
begin
  Result:= ExecSQLBuf('', 0, true);
end;

//------------------------------------------------------------------------------
function TDBManager.ExecSQLBuf(aSQL: string; aForce: boolean=false): boolean;
//------------------------------------------------------------------------------
begin
  Result:= ExecSQLBuf(aSQL, FSQLBuffer.MaxCount, aForce);
end;

//------------------------------------------------------------------------------
function TDBManager.ExecSQLBuf(aSQL: string; aMaxCount: integer; aForce: boolean=false):
    boolean;
//------------------------------------------------------------------------------
begin
  aSQL:= trim(aSQL);
  Result:= false;

  if aSQL<>'' then
  begin
    FSQLBuffer.SQL:= FSQLBuffer.SQL +' '+ aSQL;
    inc(FSQLBuffer.Counter);
  end;

  if (FSQLBuffer.SQL<>'') and ((FSQLBuffer.Counter>aMaxCount) or aForce) then
  try
//    CursorSQL;
    Result:= ExecCommandSimple(FSQLBuffer.SQL);
  finally
//    CursorRestore;
    FSQLBuffer.Counter:=0;
    FSQLBuffer.SQL:='';
  end;

end;

// -------------------------------------------------------------------
procedure TDBManager.RollbackTrans ();
// -------------------------------------------------------------------
begin
  if FADOConnection.InTransaction then
    FADOConnection.RollbackTrans;
end;
(*
//------------------------------------------------------------------------------
procedure TDBManager.DisableInsertID(ATableName: String);
//------------------------------------------------------------------------------
begin
  ADOCommand.CommandText:='SET IDENTITY_INSERT '+ATableName+' OFF';
  ADOCommand.Execute;
end;
*)

 //------------------------------------------------------------------------------
function TDBManager.UpdateViews: Integer;
//------------------------------------------------------------------------------
var
  sViewName: string;
begin
  ErrorMsg:= '';

 // Result:= true;

//  if not db_ExecStoredProc('sp__UpdateAllViews') then begin
  if not db_OpenQuery(Query, 'SELECT name FROM sysobjects WHERE xtype = ''V''') then begin
   // Result:= false;
    exit;
  end;

  Query.First;
  with Query do
    while not EOF do
  begin
    sViewName:= FieldByName(FLD_NAME).AsString;
    if (sViewName = 'sysconstraints') OR (sViewName = 'syssegments') then
    begin
      Next; Continue;
    end;

    // ������ �������� ��������� sp_refreshview � ���������� ���_VIEW
    if not ExecCommandSimple(Format('exec sp_refreshview %s', [sViewName])) then
    begin
     // ShowMessage(sViewName+'. ������: '+ErrorMsg);
   //   Result:= false;
    end;

    Next;
  end;

  Query.Close;
//  end;

end;



begin


end.




{
   sFile:=GetTempFileName_('');
   zlib_CompressFile(aRec.FileName, sFile);

   gl_DB.Blob_SaveToFile(TableName, iID, FLD_CONTENT, sFile);

   DeleteFile(sFile);
}
{
function TDBManager_Create: IDBManagerX;
begin
  result:=TDBManager.Create;
end;
}

 // function TDBManager_Create: IDBManagerX;

{
exports
  TDBManager_Create;
}
{
//--------------------------------------------------------------------
function TDBManager.MDB_CreateTable(aTableName: String; aDbFieldRecArr: TdbFieldRecArr): boolean;
//--------------------------------------------------------------------
var
  i: Integer;
  strParams, sType: string;
  bDoIndexID: boolean;
begin
//  Result := mdb_CreateTable (FADOConnection, aTableName, aDbFieldRecArr);


  if aTableName = 'dtproperties' then
   exit;


  strParams:= '';


  bDoIndexID:= false;
  for i:= 0 to Length(aDbFieldRecArr)-1 do
    if Eq(aDbFieldRecArr[i].Name, FLD_ID) then
      bDoIndexID:= true;


  for i:= 0 to Length(aDbFieldRecArr)-1 do
  begin
    case aDbFieldRecArr[i].Type_ of
      ftString:  sType := 'string';
      ftInteger: sType := 'integer';
      ftBoolean: sType := 'bit';
      ftFloat:   sType := 'double';
    else
      sType :='';
    end;

  //  if Eq(aDbFieldRecArr[i].Name, FLD_ID) then
  //    bDoIndexID:= true;

    if Eq(aDbFieldRecArr[i].Name, FLD_NUMBER) then
    begin
      strParams:=strParams +' ['+ FLD_NUMBER +'] '+ sType +',';
      continue;
    end;

    if Eq(aDbFieldRecArr[i].Name, FLD_GUID) then
    begin
      strParams:=strParams +' ['+ FLD_GUID_ +'] '+ sType +',';
      continue;
    end;

    if i = (Length(aDbFieldRecArr)-1) then
    begin
      strParams:=strParams +' ['+ aDbFieldRecArr[i].Name +'] '+ sType;
      break;
    end;

    strParams:=strParams +' ['+ aDbFieldRecArr[i].Name +'] '+ sType +',';
  end;    // for

  if strParams[Length(strParams)] = ',' then
    strParams[Length(strParams)]:=' ';


  try
     FADOConnection.Execute ('CREATE TABLE ' +aTableName+ '(' +strParams+ ')');
     if bDoIndexID then
       FADOConnection.Execute ('CREATE INDEX [id_ind] ON ' +aTableName+ ' ([id])');

     Result:= True;

  except
     Result:= False;

  end;

{
  Result:=ExecCommand ('CREATE TABLE ' +aTableName+ '(' +strParams+ ')',
                      [EMPTY_PARAM]);

  if bDoIndexID then
    ExecCommand ('CREATE INDEX [id_ind] ON ' +aTableName+ ' ([id])',
                      [EMPTY_PARAM]);
}
end;













{//--------------------------------------------------------------------
procedure TDBManager.DeleteWithChildren (aTableName: string; aID: integer);
//--------------------------------------------------------------------

    // ---------------------------------------------------------------
    procedure DoDeleteSubItems (aParentID: integer);
    // ---------------------------------------------------------------
    const
      SQL_SELECT = 'SELECT id, (SELECT COUNT(*) FROM :table WHERE (parent_id = A.id)) AS ChildCount '+
                   ' FROM :table A  WHERE parent_id=:parent_id '+
                   ' ORDER BY ChildCount';
    var
      oQuery: TADOQuery;
      iCount,iID: integer;
      sSQL: string;
    begin
      sSQL:=ReplaceStr(SQL_SELECT, ':table', aTableName);

      oQuery:=TADOQuery.Create(nil);
      oQuery.Connection:=ADOConnection;
      db_OpenQuery (oQuery, sSQL, [db_Par(FLD_PARENT_ID, aParentID)]);

      iCount:=oQuery.RecordCount;

  //SaveStringToFile(sSQL, 't:\aaa.sql');

      while not oQuery.Eof do
      begin
        iID:=oQuery.FieldByName(FLD_ID).AsInteger;

        if (oQuery.FieldByName('ChildCount').AsInteger = 0)
          then DeleteRecordByID(aTableName, iID)
          else DoDeleteSubItems(iID);

        oQuery.Next;
      end;

      oQuery.Free;
    end;


begin
  DoDeleteSubItems (aID);
  DeleteRecordByID(aTableName, aID);
end;
}








{//--------------------------------------------------------------------
function db_GetFirstID (aTableName: string): integer;
//--------------------------------------------------------------------
var
  oQuery: TAdoQuery;
begin
  oQuery:= TAdoQuery.Create(nil);
  oQuery.Connection:= ADOConnection;

  db_OpenQuery(oQuery, 'SELECT TOP 1 * FROM '+ aTableName);
  if not oQuery.IsEmpty then
    Result:= oQuery[FLD_ID]
  else
    Result:= 0;

  oQuery.Free;
end;

}


{
//--------------------------------------------------------------------
function TDBManager.MDB_CreateTable (aTableName: String; aDbFieldRecArr: TdbFieldRecArr): boolean;
//--------------------------------------------------------------------
var
  i: Integer;
  strParams, sType: string;
  bDoIndexID: boolean;
begin
  Result := mdb_CreateTable (aTableName, aDbFieldRecArr);
//  Result := mdb_CreateTable (FADOConnection, aTableName, aDbFieldRecArr);

end;
}
//---------------------------------------------------------------------
//�������� ������ aID �� ������� Src � ������� Dest ��� ID
//
//
//function TDBManager.CopyRecord (aTableName: String;   aID: Integer;
//                                aParams: array of TDBParamRec): Boolean;
////---------------------------------------------------------------------
//var
//  oQuery : TADOQuery;
//  oTable : TADOTable;
//  I: Integer;
//  sFieldName: String;
//begin
//  Result:=False;
//
//  try
//    oQuery:=TADOQuery.Create(nil);
//    oTable:=TADOTable.Create(nil);
//
//    oQuery.Connection:=ADOConnection;
//    oTable.Connection:=ADOConnection;
//    oTable.TableName :=aTableName;
//
//    db_OpenTable (oQuery,aTableName, [db_Par(FLD_ID, aID)]);
//
//    if not oQuery.IsEmpty then
//    begin
//      oTable.Open;
//      oTable.Append;
//
//      for I := 0 to oQuery.FieldCount-1 do
//      begin
//        sFieldName:=oQuery.Fields.Fields[I].FieldName;
//
//        if (not Eq(sFieldName, FLD_ID)) and
//           (not Eq(sFieldName, FLD_GUID))
//        then
//          oTable[sFieldName]:=oQuery[sFieldName];
//      end;
//
//      for I := 0 to High(aParams) do
//        oTable[aParams[I].FieldName]:=aParams[I].FieldValue;
//
//{
//
//
//      for I := 0 to High(Params) do
//      begin
//        oTable[}
//
//      oTable.Post;
//     // aRecID:=oTable[FLD_ID];
//
//      Result:=True;
//    end;
//  finally
//    oQuery.Free;
//    oTable.Free;
//  end;
//end;
//



(*
//------------------------------------------------------------------------------
procedure TDBManager.Disable_IDENTITY_INSERT(ATableName: String);
//------------------------------------------------------------------------------
begin
  ADOConnection.Execute('SET IDENTITY_INSERT '+ATableName+' OFF');
//  ADOCommand.:='SET IDENTITY_INSERT '+ATableName+' OFF';
 // ADOCommand.Execute;
//  if Assigned(RecordSet) then
//    RecordSet.Close;
end;


//------------------------------------------------------------------------------
procedure TDBManager.Enable_IDENTITY_INSERT(ATableName: String);
//------------------------------------------------------------------------------
begin
  ADOConnection.Execute('SET IDENTITY_INSERT '+ATableName+' ON');

//  ADOCommand.CommandText:='SET IDENTITY_INSERT '+ATableName+' ON';
 // ADOCommand.Execute;
//  if Assigned(RecordSet) then
//    RecordSet.Close;
end;

*)


//
////------------------------------------------------------------------------------
//procedure TDBManager.DeleteChildren(aTableName: string; aParentID: integer);
////------------------------------------------------------------------------------
//var
//    oQuery: TAdoQuery;
//
//    procedure DoDel (aParentID: integer);
//    begin
//      with oQuery do
//      begin
//        while Locate(FLD_PARENT_ID, aParentID, []) do
//          DoDel (oQuery[FLD_ID]);
//
/////////        while Locate(FLD_PARENT_ID, aParentID, []) do
//          Delete;
//      end;
//    end;
//
//begin
//  oQuery:= TADOQuery.Create(nil);
//  oQuery.Connection:= ADOConnection;
//
//  db_OpenTable(oQuery, aTableName, EMPTY_PARAM);
//
//  while oQuery.Locate(FLD_PARENT_ID, aParentID, []) do
//    DoDel (oQuery[FLD_ID]);
//
//  oQuery.Free;
//end;




//============================================================================//
// ������� ��������� ���������� ���������������� �������� IDENTITY
//
//============================================================================//
(*
function TDBManager.GetIdent1 (): integer;
const
  SQL_SELECT_IDENTITY = 'SELECT @@IDENTITY';

begin
  try
    db_OpenQuery (Query, SQL_SELECT_IDENTITY);

    result:=Query.Fields[0].AsInteger;
  except
    result:=0;
  end;

end;
*)

(*
//------------------------------------------------------------------------------
procedure TDBManager.EnableInsertID(ATableName: String);
//------------------------------------------------------------------------------
begin
  ADOCommand.CommandText:='SET IDENTITY_INSERT '+ATableName+' ON';
  ADOCommand.Execute;
//  if Assigned(RecordSet) then
//    RecordSet.Close;
end;
*)



//--------------------------------------------------------------------
//function TDBManager.ExecSP1111(aProcedureName: string; aParams: array of
//    TDBParamRec): boolean;
//--------------------------------------------------------------------
//var
//  i: Integer;
//  sSQL: string;
//begin
//  sSQL:='exec '+ aProcedureName;
//
//  for i:=0 to High(aParams) do
//  begin
//    sSQL:=sSQL+ ' :' + aParams[i].FieldName;
  ////  sSQL:=sSQL + ' :' + aParams[i].FieldName;
//    if i < High(aParams) then
//      sSQL:=sSQL+ ',';
//  end;
//
//  result := ExecCommand (sSQL, aParams);
//end;
