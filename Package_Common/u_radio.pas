unit u_radio;

interface
uses Math,
  u_Geo 
   ;

  { TODO : radio_GetRangeByFreq to shared module}
  function dbm_to_wt(aPowerInDbm: double): double;
  function wt_to_dbm(aPowerInWt: double): double;


type
  TCalcReserveRec = record
    Freq_GHz: Double;
    Power_dBm: Double;
    Gain1: Double;
    Gain2: Double;
    Sense: Double;
    Distance_km: double;
  end;

  function radio_CalcReserve(aRec: TCalcReserveRec; aFreq_GHz, aPower_dBm,
      aGain1, aGain2, aSense, aDistance_km: double; var aRxLevel_dBm: double;
      aVector: TBLVector): double;


implementation


//----------------------------------------
function dbm_to_wt(aPowerInDbm: double): double;
//----------------------------------------
begin
  if aPowerInDbm > 300 then
  begin
    Result := 0;
    Exit;
  end;

  Result:= Power(10, 0.1*(aPowerInDbm-30));
  Result:= Round(Result*1000)/1000;
end;

//----------------------------------------
function wt_to_dbm(aPowerInWt: double): double;
//----------------------------------------
begin
  if aPowerInWt>0 then
  begin
    Result:= 10*log10(aPowerInWt)+30;
    Result:= Round(Result*1000)/1000;
  end
  else
    Result:=0;
end;


//--------------------------------------------------------------------
function radio_CalcReserve(aRec: TCalcReserveRec; aFreq_GHz, aPower_dBm,
    aGain1, aGain2, aSense, aDistance_km: double; var aRxLevel_dBm: double;
    aVector: TBLVector): double;
//--------------------------------------------------------------------
//    '�������� �� ����� �������� (dBm): '+AsString(TruncFloat(POut));
//    '����� �� ��������� (dB): '+AsString(TruncFloat(Result));

//     FREQUENCY   in   GHz  !!!!!!!
//--------------------------------------------------------------------

var
  eL: double;
begin
  Result:= 0;

  if aFreq_GHz = 0 then
    exit;

  eL:=41890 * geo_Distance_km(aVector) * aFreq_GHz;
//  eL:=41890*(geo_Distance_m (aVector)/1000)*aFreq_GHz;

  if eL <> 0 then
    eL:=20*log10(eL)
  else
    exit;

  aRxLevel_dBm:=  aPower_dBm+aGain1+aGain2-eL;
  Result:= aRxLevel_dBm-aSense;
end;


end.





//--------------------------------------------------------------------
function radio_GetRangeByFreq(aFreq: double): Integer;
//--------------------------------------------------------------------
const
  RANGES: array[0..12] of record range,min,max: integer end =

  (
    (range :450;     min: 400;   max: 500),
    (range :900;     min: 900;   max: 980),
    (range :1800;    min: 1700;  max: 1900),
    (range :6;       min: 5900;  max: 6800),
    (range :4;       min: 3600;  max: 4750),
    (range :7;       min: 6000;  max: 7900),
    (range :11;      min: 10000; max: 11750),
    (range :13;      min: 12000; max: 13500),
    (range :15;      min: 14000; max: 15500),
    (range :18;      min: 17000; max: 21000),
    (range :23;      min: 22000; max: 23500),
    (range :25;      min: 24000; max: 29000),
    (range :38;      min: 37000; max: 39000)
  );


{
     range =450  where (freq>450) and (freq<500)
     range =900  where (freq>900) and (freq<980)
     range =1800  where (freq>1700) and (freq<1900)
     range =6  where (freq>5900) and (freq<6800)
     range =4  where (freq>3600) and (freq<4750)
     range =7  where (freq>7000) and (freq<7900)
     range =11  where (freq>10000) and (freq<11750)
     range =13  where (freq>12000) and (freq<13000)
     range =15  where (freq>14000) and (freq<15000)
     range =18  where (freq>17000) and (freq<19300)
     range =23  where (freq>22000) and (freq<23000)
     range =25  where (freq>25000) and (freq<29000)
     range =38  where (freq>38000) and (freq<39000)
}

var
  i: integer;
begin
  Result :=0;

  for I := 0 to High(RANGES) do    // Iterate
    if (RANGES[i].min <= aFreq) and (RANGES[i].max >= aFreq) then
    begin
      result := RANGES[i].range;
      exit;
    end;    // for


 result := 0;
end;




(*
//--------------------------------------------------------------------
function SpeedToE1(aValue: double): string;
//--------------------------------------------------------------------
begin
  case Round(aValue) of
    2:   Result:='1xE1';
    4:   Result:='2xE1';
    8:   Result:='4xE1';
    10:  Result:='5xE1';
    14:  Result:='6xE1';
    16:  Result:='8xE1';
    17:  Result:='8xE1';
    20:  Result:='10xE1';
    24:  Result:='12xE1';
    28:  Result:='14xE1';
    32:  Result:='16xE1';
    34:  Result:='16xE1';
    40:  Result:='20xE1';
    64:  Result:='32xE1';
    80:  Result:='40xE1';
    96:  Result:='48xE1';
    104: Result:='52xE1';
    128: Result:='64xE1';
    155: Result:='STM1';
    310: Result:='2xSTM1';
  else
    Result:='';
  end;
end;
*)



(*
//--------------------------------------------------------------------
function radio_CalcReserve(aFreq_GHz, aPower, aGain1, aGain2, aSense: double; aVector:
    TBLVector; var aPOut: double): double;
//--------------------------------------------------------------------
//     FREQUENCY   in   GHz  !!!!!!!
var
  eL: double;
begin
  Result:= 0;

  if aFreq_GHz = 0 then
    exit;

  eL:=41890*(geo_Distance_m (aVector)/1000)*aFreq_GHz;

  if eL <> 0 then
    eL:=20*log10(eL)
  else
    exit;

  aPOut:=  aPower+aGain1+aGain2-eL;
  Result:= aPOut-aSense;
end;

*)
