unit u_xml;

//============================================================================
interface
//============================================================================
uses SysUtils,ComObj,Variants, ActiveX,

     u_func,
     u_files //, Xml.XMLIntf
     ;


type
  TxmlAttrib = record
    Name: string;
    Value: Variant;
  end;


  IXMLNode = Variant;
  TXMLDocument = Variant;
  IXMLDocument = Variant;


  function xml_GetIntAttr    (aNode: IXMLNode; aAttrName: string; aDefault:Integer=0): Integer;
  function xml_GetFloatAttr  (aNode: IXMLNode; aAttrName: string; aDefault:string=''): Double;
  function xml_GetStringAttr (aNode: IXMLNode; aAttrName: string; aDefault:string=''): string;


  function xml_Att (aName: string; aValue: Variant): TxmlAttrib;
  function xml_Par (aName: string; aValue: Variant): TxmlAttrib;


  function xml_AddNodeTag   (aParentNode: IXMLNode; aTagName: string): IXMLNode;
  function xml_AddNode      (aParentNode: IXMLNode; aTagName: string; aAttribs: array of TxmlAttrib) : IXMLNode;

  function xml_AddNodeEx (aParentNode: IXMLNode; aTagName: string;
                          aAttribs: array of Variant) : IXMLNode;


  procedure xml_AddTextNode(aParentNode: IXMLNode; aText: string); overload;
  function xml_AddTextNode(aParentNode: IXMLNode; aTagName, aText: string): IXMLNode; overload;


  function  xml_GetAttr     (aNode: IXMLNode; aAttrName: string; aDefault:string=''): Variant;
  procedure xml_SetAttr     (aNode: IXMLNode; aAttrName: string; aAttrValue: Variant);

  function xml_GetTextByPath (aNode: IXMLNode; aPathArr: array of string): string;

  function xml_GetNodeByPath       (aNode: IXMLNode; aPathArr: array of string): IXMLNode;

  function xml_GetAttrByTagName    (aNode: IXMLNode; aTagName,aAttrName: string): string;
  function xml_GetIntAttrByTagName (aNode: IXMLNode; aTagName,aAttrName: string): integer;

  function xml_FindNode (aParentNode: IXMLNode; aTagName,aAttrName,aAttrValue: string): IXMLNode;

  function xml_GetAttrByPath (aElement: IXMLNode; aTagName,aAttrName: string): string;

  function xml_IsElement (aElement: IXMLNode): boolean;
  function xml_TagName   (aElement: IXMLNode): string;

  procedure xml_SetAttribs (aNode: IXMLNode; aAttribs: array of TxmlAttrib);
//  end;



type

  //------------------------------------------------
  TXMLDoc = class
  //------------------------------------------------
  private
//    XSLFilename : string;
    FXMLDOM:  Variant;  //  IXMLDocument;//
//    FXMLDOM:  IXMLDocument;//
  //  FXMLDOM: IDOMDocument; //Variant;
  protected

    function GetDocumentElement: IXMLNode;
    function GetXMLDOM: IXMLNode;
    procedure SetXMLText (aValue: string);
    function  GetXMLText: string;
  public
    constructor Create;

// TODO: SaveToHTML
//  procedure SaveToHTML (sXSLFilename, sFilename: string);
    procedure SaveToFile (aFilename : string);
    function  LoadFromFile (aFileName: string): boolean;

    function  SaveToText (): string;
    function  LoadFromText (aText: string): boolean;

    procedure Clear;

    property XMLDOM: IXMLNode read GetXMLDOM;
    property DocumentElement : IXMLNode read GetDocumentElement;
    property XMLText: string read GetXMLText write SetXMLText;
  end;




const
  CRLF = #13+#10;

const
  //-----------------------------------------
  // common attributes
  //-----------------------------------------

  ATT_ANGLE = 'ANGLE';

  TAG_ITEM               = 'ITEM';
  TAG_FILEDIR           = 'FILEDIR';
  TAG_FILENAME          = 'FILENAME';
  TAG_GROUP             = 'GROUP';

  ATT_ID                = 'id';
  ATT_VALUE             = 'value';

  ATT_CODE              = 'code';

  ATT_HINT              = 'hint';

  ATT_WATER_CLUTTER_CODE='WATER_CLUTTER_CODE';
  ATT_WATER_COLOR       ='WATER_CLUTTER';

  ATT_COMMENTS          = 'comments';

  ATT_HEIGHT            = 'HEIGHT';
  ATT_HEIGHT_MIN        = 'HEIGHT_MIN';
  ATT_HEIGHT_MAX        = 'HEIGHT_MAX';
  ATT_COLOR_MIN         = 'COLOR_MIN';
  ATT_COLOR_MAX         = 'COLOR_MAX';

  ATT_KUP_FILENAME      = 'KUP_FILENAME';
  ATT_KGR_FILENAME      = 'KGR_FILENAME';

  ATT_IS_FOLDER         = 'is_folder';
  ATT_CAPTION           = 'caption';
  ATT_PARENT_ID         = 'parent_id';
  ATT_children          = 'children';
  ATT_ReadOnly          ='is_readonly';
  ATT_COMMENT           = 'comment';
  ATT_TYPE              = 'type';
  ATT_NAME              = 'name';
  ATT_ITEMS             = 'items';
  ATT_PARAMS            = 'PARAMS';
  ATT_PARENT_TYPE       = 'parent_type';
  ATT_XREF_ObjectName   = 'XREF_ObjectName';
  ATT_XREF_FieldName    = 'XREF_FieldName';
  ATT_COLOR             = 'color';
  ATT_FILEDIR           = 'FILEDIR';

  ATT_LAT               = 'b';
  ATT_LON               = 'l';
  ATT_SITE_NAME         = 'SITE_NAME';

  ATT_FILENAME          = 'FILENAME';
  ATT_CHECKED           = 'CHECKED';
  ATT_EXPANDED          = 'EXPANDED';
  ATT_ENABLED           = 'ENABLED';
  ATT_KEY               = 'key';

  ATT_ALLOW_ZOOM        = 'ALLOW_ZOOM';
  ATT_AUTOLABEL         = 'AutoLabel';
  ATT_LabelBackColor    = 'LabelBackColor';
  ATT_ZOOM_MIN          = 'ZOOM_MIN';
  ATT_ZOOM_MAX          = 'ZOOM_MAX';
  ATT_Selectable        = 'SELECTABLE';

  ATT_CELL_NAME         = 'CELL_NAME';
  TAG_PARAM             = 'param';
  ATT_FREQ              = 'FREQ';


  XML_HEADER = '<?xml version="1.0" encoding="windows-1251"?>'#13#10+
//               '<?xml:stylesheet type="text/xsl" href="%s"?>'#13#10+
               '<?xml-stylesheet type="text/xsl" href="%s"?>'#13#10+
               '<Document/>';  // ����������� ��������� XML-�����

  XML_HEADER_SIMPLE = '<?xml version="1.0" encoding="windows-1251"?>'+#13#10+
               '<?xml:stylesheet type="text/xsl" ?>'+#13#10+
               '<Document/>';  // ����������� ��������� XML-�����



//============================================================================//
implementation
//============================================================================//




function xml_Att (aName: string; aValue: Variant): TxmlAttrib;
begin
  Result.Name:=LowerCase(aName);
  Result.Value:=aValue;
end;


function xml_Par (aName: string; aValue: Variant): TxmlAttrib;
begin
  Result.Name:=aName;
  Result.Value:=aValue;
end;


//-----------------------------------------------------
// TXMLDoc
//-----------------------------------------------------

//-----------------------------------------------------
function TXMLDoc.LoadFromFile (aFileName: string): boolean;
//-----------------------------------------------------
begin
  if FileExists (aFileName) then begin
    FXMLDOM.Load(aFileName);
    Result:=True;
  end else
    Result:=false;
end;

//-----------------------------------------------------
procedure TXMLDoc.Clear;
//-----------------------------------------------------
var sInitXML : string;
    pi : Variant;
begin
  sInitXML := chr(10) + '<Document></Document>';
  FXMLDOM.loadXML(sInitXML);

{
  sInitXML := ReplaceStr('type="text/xsl" href=":xsl-filename"', ':xsl-FileName', XSLFilename);
  pi := FXMLDOM.createProcessingInstruction('xml:stylesheet', sInitXML);
  FXMLDOM.insertBefore (pi, FXMLDOM.childNodes.item(0));
 }


  sInitXML := 'version="1.0" encoding="windows-1251"';
  pi := FXMLDOM.createProcessingInstruction('xml', sInitXML);
  FXMLDOM.insertBefore (pi, FXMLDOM.childNodes.item(0));

end;

//-----------------------------------------------------
constructor TXMLDoc.Create;
//-----------------------------------------------------
var sInitXML : string;
begin
  CoInitialize(nil);

  inherited Create;
  try
    FXMLDOM := CreateOleObject('Microsoft.XMLDOM');
    FXMLDOM.async := False;

//    FXMLDOM.

//    XSLFilename := aXSLFilename;
 //   if aXSLFilename='' then
  //    Clear;

    Clear;
  finally
  end;

end;

//===========================================
function TXMLDoc.GetDocumentElement: IXMLNode;
begin
  Result := FXMLDOM.documentElement;
end;

function TXMLDoc.GetXMLDOM: IXMLNode;
begin
  Result := FXMLDOM;
end;


//===========================================
procedure TXMLDoc.SaveToFile (aFileName : string);
begin
  aFileName:=ChangeFileExt(aFileName, '.xml');
  ForceDirByFileName(aFileName);
  FXMLDOM.save(aFileName);
end;

//-------------------------------------------------------------------
function TXMLDoc.SaveToText (): string;
//-------------------------------------------------------------------
var sFile: string;
begin
  sFile:=GetTempFileNameWithExt('xml');
  SaveToFile(sFile);
  Result:=TxtFileToStr (sFile);
  DeleteFile(sFile);
end;

//-------------------------------------------------------------------
function TXMLDoc.LoadFromText (aText: string): boolean;
//-------------------------------------------------------------------
var sFile: string;
begin
  sFile:=GetTempFileNameWithExt('xml');
  StrToTxtFile(aText, sFile);
  Result:=LoadFromFile(sFile);
  DeleteFile(sFile);
end;


procedure TXMLDoc.SetXMLText (aValue: string);
begin
  FXMLDOM.loadXML(aValue);
end;

function TXMLDoc.GetXMLText: string;
begin
  Result:=FXMLDOM.XML;
end;


function  xml_GetIntAttr  (aNode: IXMLNode; aAttrName: string; aDefault:Integer=0):  Integer;
begin
  Result := AsInteger(xml_GetAttr(aNode,aAttrName,IntToStr(aDefault)));
end;

function xml_GetFloatAttr  (aNode: IXMLNode; aAttrName: string; aDefault:string=''):  Double;
begin
  Result := AsFloat(xml_GetAttr(aNode,aAttrName,aDefault));
end;

function xml_GetStringAttr  (aNode: IXMLNode; aAttrName: string; aDefault:string=''):  string ;
begin
  Result := AsString(xml_GetAttr(aNode,aAttrName,aDefault));
end;

//-------------------------------------------------------------------
function GetNodeDepth(aNode: IXMLNode): integer;
//-------------------------------------------------------------------
var nd : IXMLNode;
begin
  Result := 0;

  while  (aNode.nodeTypeString <> 'document') do begin
    Result := Result + 1;

    nd := aNode.parentNode;
    aNode := nd;
  end;
end;


//--------------------------------------------------------------------
function xml_AddNodeTag (aParentNode: IXMLNode; aTagName: string): IXMLNode;
//--------------------------------------------------------------------
var
  newNode  : IXMLNode;
  iDepth,n: integer;
  sSpace  : string;
  vDoc    : IXMLDocument;
begin
  vDoc:=aParentNode.OwnerDocument;
  newNode := vDoc.createElement (aTagName);

  iDepth := GetNodeDepth(aParentNode) - 1;
  sSpace := chr(10) + StringOfChar(' ', 2*iDepth);

  if 0 = aParentNode.ChildNodes.length then
  begin
    xml_AddTextNode(aParentNode, sSpace + '  ');
    aParentNode.appendChild(newNode);
    xml_AddTextNode(aParentNode, sSpace);
  end else begin
    aParentNode.insertBefore (newNode, aParentNode.lastChild);
    aParentNode.insertBefore (vDoc.createTextNode(sSpace + '  '), newNode);
  end;

  Result := newNode;

end;


//--------------------------------------------------------------------
procedure xml_AddTextNode(aParentNode: IXMLNode; aText: string);
//--------------------------------------------------------------------
var newNode : IXMLNode;
    vDoc : IXMLDocument;
begin
  vDoc:=aParentNode.OwnerDocument;
  newNode := vDoc.createTextNode  (aText);
  aParentNode.appendChild(newNode);
 // Result := newNode;
end;

//--------------------------------------------------------------------
function xml_AddTextNode(aParentNode: IXMLNode; aTagName, aText: string):
    IXMLNode;
//--------------------------------------------------------------------
begin
  Result:=xml_AddNode (aParentNode,aTagName,[]);
  xml_AddTextNode (Result,aText);
end;



//--------------------------------------------------------------------
function xml_AddNode (aParentNode: IXMLNode; aTagName: string;               aAttribs: array of TxmlAttrib) : IXMLNode;
//--------------------------------------------------------------------
var i : integer;
begin
  Result := xml_AddNodeTag(aParentNode, aTagName);
  xml_SetAttribs (Result, aAttribs);
{
  for i := 0 to High(aAttribs) do
   if not VarIsNull (aAttribs[i].Value) then
   try
       Result.SetAttribute (aAttribs[i].Name, aAttribs[i].Value);
   except end;
}
end;


//--------------------------------------------------------------------
function xml_AddNodeEx (aParentNode: IXMLNode; aTagName: string;       aAttribs: array of Variant) : IXMLNode;
//--------------------------------------------------------------------
var i : integer;
begin
  Assert(Length(aAttribs) mod 2=0);

  Result := xml_AddNodeTag(aParentNode, aTagName);
//  xml_SetAttribs (Result, aAttribs);

  for i := 0 to (Length(aAttribs) div 2)-1 do
   if not VarIsNull (aAttribs[i*2+1]) then
   try
       Result.SetAttribute (aAttribs[i*2], aAttribs[i*2+1]);
   except end;

end;


//--------------------------------------------------------------------
procedure xml_SetAttribs (aNode: IXMLNode; aAttribs: array of TxmlAttrib);
//--------------------------------------------------------------------
var i : integer;
begin
  for i := 0 to High(aAttribs) do
   if not VarIsNull (aAttribs[i].Value) then
   try
     aNode.SetAttribute (aAttribs[i].Name, aAttribs[i].Value);
   except end;
end;


//--------------------------------------------------------------------
procedure xml_SetAttr (aNode: IXMLNode; aAttrName: string; aAttrValue: Variant);
//--------------------------------------------------------------------
begin
  if VarIsEmpty(aNode) then
    raise Exception.Create('VarIsEmpty(aNode)');

  if Trim(aAttrName)='' then
    raise Exception.Create ('Blank attrbute name');

  try
    aNode.SetAttribute (aAttrName, aAttrValue);
  except end;
end;

function xml_TagName (aElement: IXMLNode): string;
begin
  Result:=aElement.TagName;
end;

//--------------------------------------------------------------------
function xml_GetAttr (aNode: IXMLNode; aAttrName: string; aDefault: string=''): Variant;
//--------------------------------------------------------------------
var
    i: integer;
    sName: string;
    vNode: IXMLNode;
    v: Variant;
begin
  if Trim(aAttrName)='' then
    raise Exception.Create ('Blank attrbute name');

  if VarIsEmpty(aNode) then
  begin
    Result := NULL;
    Exit;
  end;


  v:=aNode.TagName;
  v:=aNode.attributes;
  i:=aNode.attributes.length;

  for i:=0 to aNode.attributes.length-1 do
  begin
    v:=aNode.attributes.item(i);
    sName:=v.Name;
    if Eq(sName, aAttrName) then begin Result:=v.Value; Exit; end;
  end;

  Result:=aDefault;
end;

//-----------------------------------------------------------
function xml_GetTextByPath (aNode: IXMLNode; aPathArr: array of string): string;
//-----------------------------------------------------------
var vNode: IXMLNode;
begin
  vNode:=xml_GetNodeByPath (aNode, aPathArr);
  if not VarIsNull(vNode) then
    Result:=vNode.Text
  else
    Result:='';
end;

//-----------------------------------------------------------
function xml_GetNodeByPath (aNode: IXMLNode; aPathArr: array of string): IXMLNode;
//-----------------------------------------------------------
var i,j,iCOunt:integer;   vElement: IXMLNode;   strArr: array of string;
  sTag: string;
begin
  Result:=null;

  if VarIsNull(aNode) then
    Exit;

  iCOunt:=aNode.childNodes.Length;

  if High(aPathArr)>=0 then

  for i:=0 to aNode.childNodes.Length-1 do
  begin
    vElement:=aNode.childNodes.Item(i);

    if not xml_IsElement(vElement) then
      Continue;

    sTag:=vElement.TagName;

    if sTag='' then Continue;

    if Eq (sTag, aPathArr[0]) then
      if High(aPathArr)=0
       then begin Result:=vElement; Exit; end
       else begin
         SetLength(strArr, High(aPathArr));
         for j:=0 to High(aPathArr)-1 do strArr[j]:=aPathArr[j+1];

         Result:=xml_GetNodeByPath (vElement, strArr);
       end;
  end;


end;


function xml_GetAttrByTagName (aNode: IXMLNode; aTagName,aAttrName: string): string;
var vNode: IXMLNode;
begin
   vNode:=xml_GetNodeByPath (aNode, [aTagName]);
   if VarIsNull(vNode) then Result:=''
                       else Result:=xml_GetAttr (vNode, aAttrName);
end;


function xml_GetIntAttrByTagName (aNode: IXMLNode; aTagName,aAttrName: string): integer;
var vNode: IXMLNode;
begin
  Result:=AsInteger(xml_GetAttrByTagName(aNode, aTagName,aAttrName));
end;



//-----------------------------------------------------------------------
function xml_FindNode (aParentNode: IXMLNode;
                       aTagName,aAttrName,aAttrValue: string): IXMLNode;
//-----------------------------------------------------------------------
var i: integer;
   vRoot,vNode: IXMLNode;   s,sTag,sType: string;
begin
  for i:=0 to aParentNode.ChildNodes.Length-1 do
  begin
    vNode:=aParentNode.ChildNodes.Item(i);

   // s:=vNode.nodeTypeString;

    if not xml_IsElement(vNode) then
      Continue;

    sTag:=vNode.TagName;

    if sTag='' then Continue;

    if Eq(sTag, aTagName) then
     if xml_GetAttr (vNode, aAttrName) = aAttrValue then begin
       Result:=vNode; Exit;
     end;
  end;

  Result:=null;
end;


function xml_GetAttrByPath (aElement: IXMLNode; aTagName,aAttrName: string): string;
begin
   Result:=xml_GetAttrByTagName (aElement,aTagName,aAttrName);
end;




function xml_IsElement (aElement: IXMLNode): boolean;
begin
  Result:=(aElement.nodeTypeString = 'element');
end;



begin

end.




// TODO: SaveToHTML
////-------------------------------------------
//procedure TXMLDoc.SaveToHTML(sXSLFilename, sFilename : string);
////-------------------------------------------
//var xsl,result,pi : Variant;
//begin
//// load style sheet
//xsl := CreateOleObject('Microsoft.XMLDOM');
//xsl.async := false;
//xsl.load(sXSLFilename);
//
//// set up the resulting document
//result := CreateOleObject('Microsoft.XMLDOM');
//result.async := false;
//result.validateOnParse := true;
//
//FXMLDOM.transformNodeToObject(xsl, result);
//pi := result.createProcessingInstruction('xml', ' version="1.0" encoding="windows-1251"');
//result.insertBefore (pi, result.childNodes.item(0));
//
//result.save(sFilename);
//end;
