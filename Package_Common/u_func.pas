
unit u_func;

interface
uses
  Messages, ShellAPI, DateUtils, IOUtils,    
  Winapi.Windows, System.SysUtils, System.Variants, System.Classes,   
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,  Vcl.ActnList, Math,         
  RxVerInf,  System.StrUtils;



const
  CRLF = #13+#10;
  LF = CRLF;  
  CR = CRLF;  

type
  TOnProgressEvent = procedure (aProgress, aMax: Integer) of object;
  

  TIntNotifyEvent = procedure ({Sender: TObject;} aValue: integer) of object;

  TStrNotifyEvent = procedure (Sender: TObject; aStr: string) of object;
  TLogEvent = procedure (aMsg: string) of object;

  //---------------------------------------------------
  // COMMON types
  //---------------------------------------------------
  TIntArray     = array of integer;
  TStrArray     = array of string;
  TDoubleArray  = array of double;
  TVariantArray = array of Variant;
 // TStr2DArray    = array of TStrArray;

  // ������� N x N �� ���������� (��-���) (������-�������)
  TBoolArray       = array of boolean;

  TDouble2DMatrix  = array of TDoubleArray;
  

  procedure CloseApplicationForms;
  
 
function CompareMemoryStreams(msOne,msTwo:TMemoryStream): boolean;


//  procedure CreateChildForm(InstanceClass: TComponentClass; var aReference; aDestControl: TWinControl); overload;
  procedure CreateChildForm(var aReference; InstanceClass: TComponentClass;  aDestControl: TWinControl);

  procedure CreateChildForm_(InstanceClass: TComponentClass; var aReference;  aDestControl: TWinControl);


  procedure SetComponentActionsExecuteProc (aComponent: TComponent; aProc: TNotifyEvent);


//  procedure CreateChildForm(var aReference; InstanceClass: TComponentClass;  aDestControl: TWinControl); overload;

  // COMMON functions
  //---------------------------------------------------
  function IIF (Condition: boolean; TrueValue,FalseValue: variant): variant; //overload;
  function IIF_NULL (aValue: variant): variant;

 ////// function IIF (Conditions: array of boolean; Values: array of variant; ElseValue:Variant): variant; overload;

  function Eq(aValue1, aValue2: string): boolean;

{
  function GetLastErrorMessage (aErrorCode: integer): string;
}
  //---------------------------------------------------
  
  //---------------------------------------------------
  function AsFloat   (aValue: Variant; aDefault:double=0): double;
//  function AsFloat_  (Value:  variant; aDefault:double=0): double;
  function AsInteger (aValue: Variant; aDefault:integer=0): integer;
  function AsBoolean (aValue: Variant): boolean;
  function AsString  (aValue: Variant): string;

  function StringIsInArr(Value:string; Args: array of string): boolean;
  

  function GetModuleFileName: string;


  function GetFileSize(const aFileName : string) : Int64;

  
  //---------------------------------------------------
  // String functions
  //---------------------------------------------------
  function StrIsDouble  (asValue: string): boolean;
  function StrIsInt     (asValue: string): boolean;

  function  VarToString     (Value: Variant): string;
  function  StringToVar     (Value:string; DestValue:Variant): Variant;
  function  ReplaceBlanks   (Value : string): string;
  procedure SeparateString  (Value,Separator:string; var Value1,Value2:string);
  function  RemoveStrHeader (SourceStr, RemovedStr: string): string;
  function  StringIsInList  (Value:string; Args: array of string): boolean;
//  function  Replace         (aSource:string; aParams,aValues: array of string): string;

  function ReplaceStr (Value, OldPattern, NewPattern: string): string; //overload;
  function ReplaceInt (aValue, aOldPattern: string; aNewPattern: integer): string; //overload;

  function RemoveSubStr   (aValue, SubStr: string): string;

  function Eq_Any(aValue1: string; Value2: array of string): boolean;


  procedure ClearArray           (var IntArr: TIntArray);

//procedure StringToStringist(aValue: string; aStrings: TStringList; aDelimiter:
//    string = ';');


  function CreateStringListFromString (aValue: string; aDelimiter: string=';'): TStringList;



  function IsStrInStrArray(aStr: string; aStrArray: array of string): boolean;

  //---------------------------------------------------
  // Misc
  //---------------------------------------------------

//  function Inc_ (var aValue: integer): integer;


  // procedure EnableChildControls (Value: boolean; aParentControl,aExceptControl: TWinControl);
  function  TruncFloat      (aValue: double; aCount: integer=2): double;
  procedure TruncFloatVar   (var aValue: double; aCount: integer=2);

  function CompareStrings   (aList1,aList2: TStrings): integer;

  procedure CursorHourGlass;
  procedure CursorDefault;
  procedure CursorSQL;
  procedure CursorRestore;

  function VarExists (aValue: Variant): boolean;

//  function FindForm (aClassName: string): TForm;

//  procedure SetComponentActionsExecuteProc (aComponent: TComponent; aProc: TNotifyEvent); //overload;
 // procedure SetActionsExecuteProc (aActionArr: array of TAction; aProc: TNotifyEvent); //overload;


  function CompareDates      (aValue1,aValue2: TDateTime): integer;
  function FormatDateVar     (aValue: Variant): string;
  function FormatDateTimeVar (aValue: Variant): string;


//procedure StrListCopy (aSrcList, aDestList: TStringList);

  // DIALOGS ---------------------------------------------------------
//  procedure CopyControls(aSrcForm: TForm; aDest: TWinControl);
//  procedure CopyDocking1(aSrcForm: TForm; aDest: TWinControl);


//  function IsFormExists (aClassName: string): TForm;


  function str_DeleteLastChar(var aKeyFields: string): boolean;

  procedure SaveTime();
  function ShowTime: string;

  function GetTimeDiff(): string;

  
  function GetFileVersion: string;

//  procedure CreateChildForm(InstanceClass: TComponentClass; var aReference; aDestControl: TWinControl);
//  procedure CopyDockingEx (aSrcForm: TControl; aDest: TWinControl);



//procedure CreateChildForm1(InstanceClass: TComponentClass; var aReference; aOwner, aDestControl: TWinControl);


  function PickVarArrayItem(Value: Variant; SrcValues,DestValues: array of
      Variant): Variant;

  function ExtractStrByIndex(aStr: string; aIndex: integer): string;

  //------------------------------------------------------
//  function IsFormExists(aApplication: TApplication; aClassName: string): TForm;
  
  function FindForm(aClassName: string): TForm;
  function GetAppVersionStr (): string;

//  procedure ShellExec(aFileName: string; aFileParams: string = '');

  function IsFormExists(aClassName: string): TForm;


//procedure AssertFileExists(aFileName: string);


function PosRight(aSubStr, aValue: string): Integer;



procedure ExchangeInt(var aValue1,aValue2: integer);

procedure ExchangeStr(var aValue1,aValue2: string);

function GetComputerNetName: string;

function IIF_NOT_0(aValue: integer): variant;

function GetApplicationDir: string;

function ConfirmDlg(aMsg: string): boolean;

procedure Show_Application_DataModules;

procedure Application_DataModules_Free;

function AsString_(aValue: Variant): string;

function Eq_Any_variant(aValue: variant; Value2: array of variant): boolean;

function String_Reverse1(aValue: String): String;

function GetApplicationIniFileName(aExeName: string = ''): string;

function DoubleQuotedStr(aValue: String): String;

//function DateTimeRemoveSec(aValue1: TDateTime): TDateTime;

//procedure CreateChildForm_(var aReference; InstanceClass: TComponentClass;
//    aDestControl: TWinControl);

function SArr(Values: array of string): TStrArray; overload;

function SArr(aValue1,aValue2: string): TStrArray; overload;

//  function MakeStrArray         (Values: array of string): TStrArray;
  function StrArray             (Values: array of string): TStrArray;

//function StringToStringList___(aValue: string; aDelimiter: string=';'):
//  TStringList;


procedure StrToFile_(aFileName: string; aStr: string);


procedure ShellExec_Notepad(aFileName: string);

function RoundFloat(Value: double; aCount: integer=3): double;

function StringListToStrArray(aStringList: TStringList): TStrArray;

function StringToStringList(aValue: string; aDelimiter: char=';'): TStringList;

//procedure SetActionsEnabled1(aActionArr: array of TAction; aEnabled: Boolean);

//procedure SetFormActionsEnabled(aComponent: TComponent; aEnabled: Boolean);

//function ChecksumOfDouble1111111111(const ADouble: Double): Integer;

procedure ShowApplicationComponents;

function MakeDoubleArray(aArr: array of double): TDoubleArray;

function FileToStr(aFileName: string; var aStr: string): Boolean;

function Pos_(const aSubstr, aStr: string): boolean;

function StringToStrArray_new(aValue: string; aDelimiter: char=';'): TStrArray;


function SearchFileInPath(aFileName: string): string;

function TranslitRus2Lat(const aStr: string): string;


//function GenerateGUID: string;


(*
procedure SetActionsArray(aActions: array of TAction; var aDestActionsArr:
    array of TAction);
*)

//procedure CopyControls(aSrcForm: TForm; aDest: TWinControl);

//function CompareSqlText(aValue1, aValue2: String; var APos: Integer): Boolean;

const
  DEF_EXCEL_Filter = 'Excel (*.xls,*.xlsx)|*.xls;*.xlsx';


procedure StrToTextFile(aFileName, aStr: string);



var
//  ApplicationName : string;
  CommonApplicationDataDir : string;



//========================================================
implementation
//========================================================


uses
  u_files, Vcl.ExtCtrls;
 

{
function GenerateGUID: string;
var
  Uid: TGuid;
begin
  CreateGuid(Uid);

  Result := GuidToString(Uid);
end;
 }
  

// ---------------------------------------------------------------
procedure StrToTextFile(aFileName, aStr: string);
// ---------------------------------------------------------------
var
  list: TstringList;
begin
  ForceDirByFileName(aFileName);

  IOUtils.TFile.WriteAllText(aFileName, aStr);

//  
//  list:=TstringList.Create;
//  list.Text:=aStr;
//  list.SaveToFile (aFileName);
//  list.Free;
// 
 
end;




 

//--------------------------------------------------------
function IIF (Condition: boolean; TrueValue,FalseValue: variant): variant;
//--------------------------------------------------------
begin
//  Result:=IfThen (Condition, TrueValue, FalseValue);
  if Condition then Result:=TrueValue else Result:=FalseValue;
end;


//--------------------------------------------------------
function Eq(aValue1, aValue2: string): boolean;
//--------------------------------------------------------
begin
  Result := AnsiCompareText(Trim(aValue1),Trim(aValue2))=0;
end;
 
//---------------------------------------------------------
procedure SetComponentActionsExecuteProc (aComponent: TComponent; aProc: TNotifyEvent);
//---------------------------------------------------------
var i: integer;
begin
  with aComponent do
  for i :=0  to ComponentCount-1 do
    if (Components[i].ClassName = 'TAction') then
      (TAction(Components[i])).OnExecute:=aProc;
end;




//--------------------------------------------------------
function AsFloat (aValue: Variant; aDefault:double=0): double;
//--------------------------------------------------------
var err: integer;
  i: Integer;
   S: string;
begin
  i:=VarType(aValue);

 
  

  case VarType(aValue) of
    varUString ,
    varString,
    varOleStr:  begin
                  s:=Trim (aValue);
                  s:=ReplaceStr (s, ',','.');
                  Val(s, Result, err);

                  if err<>0 then Result:=aDefault;
                end;

    varInteger,
    varSingle,
    varCurrency,
    varByte,
    varDouble : Result:= aValue;

    varNull: Result:=0;
  else
    Result:=0;

//CodeSite.    
    
  //  raise Exception.Create('VarType(aValue) is not found: '+ VarTypeAsText(VarType(aValue)));
//    ShowMessage('function AsFloat (aValue: Variant; aDefault:double=0): double;');
 //   Result:=0;
  end;
end;

// ---------------------------------------------------------------
function AsInteger(aValue: Variant; aDefault:integer=0): integer;
// ---------------------------------------------------------------
//var i: integer;
begin
//  i:=VarType(Value);

  case VarType(aValue) of

    varUString,
    varString,
    varOleStr:
              begin
                aValue:=ReplaceStr(aValue, #9, '');
                Result:=StrToIntDef (aValue,aDefault);
              end;

    varDouble: Result:=Trunc(aValue);

    varInteger,
    varByte: Result:=aValue;

    varNull: Result:=0;
  else
  //  raise Exception.Create('function AsInteger (Value: Variant; aDefault:integer=0): integer; code:'+ Format('%d', [VarType(aValue)]) );
    Result:=0;
  end;
end;

//--------------------------------------------------------
function AsBoolean(aValue: Variant): boolean;
//--------------------------------------------------------
var
  i: integer;
begin
  if VarIsNull(aValue) or (aValue='') then
    result := False
  else
  begin
    i:=VarType(aValue);

    case VarType(aValue) of
      varEmpty    : result := False;
      varBoolean  : Result:=aValue;
      varInteger  : Result:=aValue<>0;

      varOleStr,
      varUString,
      varString   : begin
                      aValue:=Trim(aValue);
                      Result:=(aValue='1') or (aValue='-1') or (Eq(aValue,'True'));
                    end;
    else
      ShowMessage(IntToStr(i));
      raise Exception.Create('function AsBoolean(aValue: Variant): boolean; - '+IntToStr(VarType(aValue)));
    end;
  end;
end;

//--------------------------------------------------------
function StringIsInList (Value:string; Args: array of string): boolean;
//--------------------------------------------------------
var i:integer;
begin
  Result:=false;

  for I:=0 to High(Args) do
    if AnsiCompareText(Value,Args[I])=0 then
    begin
      Result:=true;
      break;
    end;
end;

//--------------------------------------------------------
function VarToString(Value : Variant): string;
//--------------------------------------------------------
begin
  case VarType(Value) of
    varString  : Result:=Trim(Value);
    varInteger : Result:=IntToStr(Value);
    varDouble  : Result:=FloatToStr(Value);
    varBoolean : Result:=IIF(Value=true,1,0);
    varDate    : Result:=DateTimeToStr (Value);
    varNull    : Result:='';
  end;
end;

function ReplaceBlanks (Value : string): string;
begin
  Result:=StringReplace (Value, ' ', '_', [rfReplaceAll]);
end;


function RemoveStrHeader (SourceStr, RemovedStr: string): string;
// ������� ��������� ���������
begin
  if Pos(RemovedStr,SourceStr) = 1 then Delete(SourceStr,1,Length(RemovedStr));
  Result:=SourceStr;
end;

//--------------------------------------------------------
function StringToVar (Value:string; DestValue:Variant): Variant;
//--------------------------------------------------------
begin
  case VarType(DestValue) of
    varString  : Result:=Trim(Value);
    varInteger : Result:=StrToIntDef(Value,0);
    varDouble  : Result:=AsFloat(Value);
    varBoolean : Result:=Eq(Value,'-1') or Eq(Value,'1') or Eq(Value,'��') or Eq(Value,'yes');
    varDate    : try Result:=StrToDateTime (Value); except Result:=Now(); end
  end;
end;

//--------------------------------------------------------
procedure SeparateString (Value,Separator:string; var Value1,Value2:string);
//--------------------------------------------------------
var i:integer;
begin
  Value:=StringReplace (Value, #9,'', [rfReplaceAll]);
  i:=Pos(Separator,Value);
  if i>0 then begin Value1:=Copy(Value,1,i-1); Value2:=Copy(Value,i+1,Length(Value)); end
         else begin Value1:=Value; Value2:=''; end;
end;



function ReplaceInt(aValue, aOldPattern: string; aNewPattern: integer): string;
begin
  Result:=ReplaceStr(aValue, aOldPattern, IntToStr(aNewPattern));
end;

function ReplaceStr (Value, OldPattern, NewPattern: string): string;
begin
  Result:=StringReplace (Value, OldPattern, NewPattern, [rfReplaceAll, rfIgnoreCase]);
end;

function RemoveSubStr(aValue, SubStr: string): string;
begin
  Result:=ReplaceStr (aValue, SubStr, '');
end;

function FirstUpperCase(aValue: string): string;
begin
  Result:=AnsiUpperCase(Copy(aValue,1,1)) + AnsiLowerCase(Copy(aValue,2,1000));
end;


//--------------------------------------------------------
function RemoveCharFromString (Ch:char; SrcStr: string): string;
//--------------------------------------------------------
begin
  Result:=StringReplace (SrcStr, Ch, '', [rfReplaceAll] );
end;

//--------------------------------------------------------
function PickVarArrayItem(Value: Variant; SrcValues,DestValues: array of
    Variant): Variant;
//--------------------------------------------------------
var i: integer;
begin
  Result:='';
  for i:=0 to High(SrcValues) do
    if SrcValues[i]=Value then begin Result:=DestValues[i]; Exit; end;
end;


//---------------------------------------------
function IsSingleApplication (AClassName: string): boolean;
// only 1 copy of the program should be loaded
//---------------------------------------------
var Handle: THandle;
begin
  Result:=false;

  Handle:=FindWindow(PChar(AClassName),nil);
  if Handle <> 0
    then SetForegroundWindow(Handle)
    else Result:=true;

  if FindWindow(PChar('TAppBuilder'),nil)<>0
    then Result:=true;
end;


procedure ClearArray (var IntArr:TIntArray);
var i:integer;
begin
  for i:=0 to High(IntArr) do IntArr[i]:=0; // clear array
end;


function TruncFloat (aValue: double; aCount: integer=2): double;
var
  d: Double;
begin
  d:=Power(10,aCount);
  Result:=Trunc(aValue * d) / d;
end;


//------------------------------------------------------------------------------
// �������� ���������� ������� SQL
//------------------------------------------------------------------------------
function RoundFloat(Value: double; aCount: integer=3): double;
//------------------------------------------------------------------------------
var dExt: double;
begin
  dExt:= Power(10, aCount);

  Result:= round(Value*dExt) / dExt;

  //Result:= AsFloat (format('%.'+IntToStr(aCount)+'f',[Value]));
end;



procedure TruncFloatVar (var aValue: double; aCount: integer=2);
begin
  aValue:=Trunc(aValue*Power(10,aCount)) / Power(10,aCount);
end;


procedure MoveRect (var aRect: TRect; aLeft,aTop: integer);
var
  dH,dW: integer;
begin
  dH:=Abs(aRect.Top - aRect.Bottom);
  dW:=Abs(aRect.Left - aRect.Right);

  aRect.Top :=aTop;
  aRect.Left:=aLeft;

  aRect.Bottom:=aRect.Top  + dH;
  aRect.Right :=aRect.Left + dW;
end;


//-----------------------------------------------------------
function CompareStrings (aList1,aList2: TStrings): integer;
//-----------------------------------------------------------
var i: integer;
begin
  Result:=-1;

  if aList1.Count <> aList2.Count then Exit;

  for i:=0 to aList1.Count-1 do
   if not Eq (aList1[i], aList2[i]) then Exit;

  Result:=0;
end;


procedure CursorHourGlass;
begin
  Screen.Cursor:=crHourGlass;
//  Application.ProcessMessages;
end;

procedure CursorDefault;
begin
  Screen.Cursor:=crDefault;
//  Application.ProcessMessages;
  
end;


function VarExists (aValue: Variant): boolean;
begin
  Result:=(not VarIsNull(aValue));
//          (aValue<>0);
end;


// without boolean conversion
function AsString_(aValue: Variant): string;
begin
  if VarIsNull(aValue) then
    Result:='' else

  case VarType(aValue) of
    varInteger: Result:=IntToStr(aValue);
    varDouble : begin
      Result:= FloatToStr(aValue);
      Result:= ReplaceStr(Result, ',', '.');
    end;
    varOleStr,varString:  Result:=aValue;
    varBoolean: Result:= IIF(aValue, '1', '0');
  end;
end;


function AsString(aValue: Variant): string;
var i: integer;
begin
  if VarIsNull(aValue) then
    Result:=''

  else

    case VarType(aValue) of
      varEmpty,
      varNull:          Result:='';

      varByte,
      varInteger,
      varSmallint:      Result:=IntToStr(aValue);

      varDouble :       Result:=FloatToStr(aValue);   //san

      varUString,
      varOleStr,
      varString:        Result:=aValue;

      varBoolean:       Result:= IIF(aValue, 'True', 'False');
    else
      raise Exception.Create('Error Message' + IntToStr(VarType(aValue))); 
//      Result:='';
//      ShowMessage('function AsString(aValue: Variant): string;;  VarType(aValue)=?' + IntToStr(VarType(aValue)));
    end;
end;



function ExtractStrByIndex(aStr: string; aIndex: integer): string;
var
  oList: TStringList;
begin
  oList:=TStringList.Create;
  oList.Text:=aStr;
  if aIndex<oList.Count then Result:=oList[aIndex]
                        else Result:='';
  oList.Free;
end;


//--------------------------------------------------------
function CreateStringListFromString (aValue: string; aDelimiter: string=';'): TStringList;
//--------------------------------------------------------
begin
  Assert(length(aDelimiter)>0);

  Result:=TStringList.Create;
  Result.Delimiter:=aDelimiter[1];
  Result.DelimitedText:=aValue;
end;





//---------------------------------------------------------
function GetAppVersionStr(): string;
//---------------------------------------------------------
var obj: TVersionInfo;
begin
  obj:=AppVerInfo();
  Result:=RemoveSubStr (obj.FileVersion, '.0');
  Result:=Format(' [ver %s]', [Result]);
  obj.Free;
end;

//---------------------------------------------------------
function FormatDateVar (aValue: Variant): string;
//---------------------------------------------------------
begin
  if VarExists(aValue) then
    Result:=FormatDateTime ('dd:mm:yyyy', aValue)
  else
    Result:='';
end;

//---------------------------------------------------------
function FormatDateTimeVar (aValue: Variant): string;
//---------------------------------------------------------
begin
  if VarExists(aValue) then
    Result:=FormatDateTime ('dd:mm:yyyy hh:mm', aValue)
  else
    Result:='';
end;

//--------------------------------------------------------
function CompareDates (aValue1,aValue2: TDateTime): integer;
//--------------------------------------------------------
//  1: Val1 > Val2
// -1: Val1 < Val2
//--------------------------------------------------------
var
  y1,m1,d1,y2,m2,d2: Word;
  h1, min1, s1, ms1: Word;
  h2, min2, s2, ms2: Word;

begin
//function CompareDateTime(const A, B: TDateTime): TValueRelationship;

//-1	LessThanValue	The first value is less than the second value.
//0	EqualsValue	The two values are equal.
//1	GreaterThanValue	The first value is greater than the second Value.


  DecodeDate (aValue1, y1,m1,d1);
  DecodeDate (aValue2, y2,m2,d2);

  DecodeTime (aValue1, h1, min1, s1, ms1);
  DecodeTime (aValue2, h2, min2, s2, ms2);

  if aValue1=aValue2 then
    Result:=0

  else
    if (y1>=y2) or (m1>=m2) or (d1>=d2) or (h1>=h2) or (min1>=min2) or (s1>=s2) then
      Result:=1
    else
      Result:=-1
end;



function Inc_1111 (var aValue: integer): integer;
begin
  Inc(aValue);
  Result:=aValue;
end;    // 


// ---------------------------------------------------------------
procedure Show_Application_DataModules;
// ---------------------------------------------------------------
var
  i : Integer;
begin
  i := Application.ComponentCount;

  // ShowMessage('DBLogin.Destroy;'+IntToStr(Application.ComponentCount));

  for I := Application.ComponentCount - 1  downto 0 do
    if Application.Components[i] is TDataModule then
    begin
      ShowMessage(Application.Components[i].ClassName +'  '+ Application.Components[i].Name);

//      FreeAndNil(Application.Components[i]);
    //     ShowMessage(Application.Components[i].ClassName +'  '+ Application.Components[i].Name);

    end;
end;


// ---------------------------------------------------------------
procedure Application_DataModules_Free;
// ---------------------------------------------------------------
var
  i : Integer;
begin
  i := Application.ComponentCount;

  // ShowMessage('DBLogin.Destroy;'+IntToStr(Application.ComponentCount));

  for I := Application.ComponentCount - 1  downto 0 do
    if Application.Components[i] is TDataModule then
    begin
    //  ShowMessage(Application.Components[i].ClassName +'  '+ Application.Components[i].Name);

      (Application.Components[i] as TDataModule).Free;

    //     ShowMessage(Application.Components[i].ClassName +'  '+ Application.Components[i].Name);

    end;
end;

//-------------------------------------------------------------------
function FindForm(aClassName: string): TForm;
//-------------------------------------------------------------------
var i: integer;
begin
  result:=nil;

//  Application.
  
  for i:=0 to Application.ComponentCount-1 do
    if (Application.Components[i] is TForm) then
      if Eq (Application.Components[i].ClassName, aClassName) then
    begin
      Result:=TForm (Application.Components[i]);
      Exit;
    end;

  if Assigned(Application.MainForm) then
    for i:=0 to Application.MainForm.ComponentCount-1 do
      if (Application.MainForm.Components[i] is TForm) then
        if Eq (Application.MainForm.Components[i].ClassName, aClassName) then
      begin
        Result:=TForm (Application.MainForm.Components[i]);
        Exit;
      end;
end;


function str_DeleteLastChar(var aKeyFields: string): boolean;
begin
  Result:= true;

  if aKeyFields = '' then
  begin
    Result:= false;
    exit;
  end;

  SetLength(aKeyFields, Length(aKeyFields)-1);
end;    //





function StrIsDouble (asValue: string): boolean;
var err: integer;
  dval: double;
begin
  asValue:= Trim (asValue);
  asValue:= ReplaceStr (asValue, ',', '.');
  Val(asValue, dval, err);
  Result:= (err=0);
end;


function StrIsInt (asValue: string): boolean;
var err: integer;
  iVal: Integer;
begin
  asValue:= Trim (asValue);
  asValue:= ReplaceStr (asValue, ',', '.');
  Val(asValue, iVal, err);
  Result:= (err=0);
end;



//-------------------------------------------------------
function  IIF_NULL (aValue: variant): variant;
//-------------------------------------------------------
begin
  if VarIsNull(aValue) or
     ((VarType(aValue)=varInteger)  and (aValue=0)) or
     ((VarType(aValue)=varSingle)   and (aValue=0)) or
     ((VarType(aValue)=varDouble)   and (aValue=0)) or
     ((VarType(aValue)=varSmallInt) and (aValue=0)) or
     ((VarType(aValue)=varString)   and (aValue=''))
  then
    Result:= NULL
  else
    Result:= aValue;
end;


//-------------------------------------------------------
function IIF_NOT_0(aValue: integer): variant;
//-------------------------------------------------------
begin
  if aValue=0 then
    Result:= NULL
  else
    Result:= aValue;
end;


procedure CursorSQL;
begin
  Screen.Cursor:=crSQLWait;
end;


var
  LTime_dt: TDateTime;

procedure SaveTime();
begin             
  LTime_dt  := Now;
end;


function GetTimeDiff(): string;
begin 
  Result:= intToStr( SecondsBetween(LTime_dt, Now)) + ' sec'            
//  LTime_dt  := Now;
end;




function ShowTime: string;
var
  dt: TDateTime;
begin
  dt:= Now - LTime_dt;
  result := FormatDateTime ('hh:mm:ss:zzz', dt);

   //Assert(SecondsBetween(MyAnswer, RRUZsAnswer)=7)
  
//  ShowMessage (FormatDateTime ('hh:mm:ss:zzz', dt));
  ShowMessage (result);

end;


function GetFileVersion: string;
var obj: TVersionInfo;
begin
  obj:=TVersionInfo.Create(Application.ExeName);

//  obj:=AppVerInfo();
  Result:='������  '+RemoveSubStr (obj.FileVersion, '.0');
  obj.Free;
end;




procedure CursorRestore;
begin
  Screen.Cursor:=crDefault;
end;


//--------------------------------------------------------
function IsStrInStrArray(aStr: string; aStrArray: array of string): boolean;
//--------------------------------------------------------
var i: integer;
begin
  Result:= false;
  for i:=0 to High(aStrArray) do
  begin
    Result:= Eq(aStr,aStrArray[i]);
    if Result then  Exit;
  end;
end;



//-------------------------------------------------------------------
function IsFormExists(aClassName: string): TForm;
//-------------------------------------------------------------------
var i: integer;
begin
  Result:=nil;

 
  for i:=0 to Application.ComponentCount-1 do
    if Eq (Application.Components[i].ClassName, aClassName) then
    begin
      Result:=TForm (Application.Components[i]);
      Exit;
    end;

    
  if Assigned(Application.MainForm) then
    for i:=0 to Application.MainForm.ComponentCount-1 do
      if Eq (application.MainForm.Components[i].ClassName, aClassName) then
      begin
        Result:=TForm (Application.MainForm.Components[i]);
        Exit;
      end;
end;


//-------------------------------------------------------------------
procedure CloseApplicationForms;
//-------------------------------------------------------------------
var i: integer;
begin
   for i:=0 to Application.ComponentCount-1 do
    if Application.Components[i] is TForm then
//    begin
      PostMessage((Application.Components[i] as TForm).Handle, WM_CLOSE, 0,0);


//  for i:=Application.ComponentCount-1 downto 0 do
//    if Application.Components[i] is TForm then
//    begin      
//      Application.Components[i].Free;
//    end;

//  if Assigned(aApplication.MainForm) then
//    for i:=0 to aApplication.MainForm.ComponentCount-1 do
//      if Eq (Aapplication.MainForm.Components[i].ClassName, aClassName) then
//      begin
//        Result:=TForm (aApplication.MainForm.Components[i]);
//        Exit;
//      end;

end;



{
procedure ShellExec(aFileName: string; aFileParams: string = '');
var
  r: Integer;
begin
  Assert(aFileName<>'');
 // Assert(FileExists( aFileName));

//  if aFileName<>'' then

//function ShellExecute(hWnd: HWnd; Operation, FileName, Parameters, Directory: PChar; ShowCmd: Integer): HINST; stdcall;

  r := ShellExecute(0,nil, pChar(string(aFileName)), pChar(string(aFileParams)), nil, SW_NORMAL);
  r:=0;

//  ShellExecute(0,nil, pChar(aFileName), pChar(aFileParams), nil, SW_NORMAL);
end;

}

procedure ShellExec_Notepad(aFileName: string);
begin
 // ShellExecute(Handle,
 // zz'open', 'c:\MyDocuments\Letter.doc', nil, nil, SW_SHOWNORMAL);

  ShellExecute(0, 'open',
     'notepad.exe', PChar(aFileName), nil, SW_SHOWNORMAL);

 // if aFileName<>'' then
 //   ShellExecute(0,nil, pChar(string(aFileName)), pChar(string(aFileParams)), nil, SW_NORMAL);

//  ShellExecute(0,nil, pChar(aFileName), pChar(aFileParams), nil, SW_NORMAL);
end;


//--------------------------------------------------------
function GetApplicationDir: string;
//--------------------------------------------------------
begin
  Result :=IncludeTrailingBackslash(ExtractFileDir(Application.ExeName));
end;


//--------------------------------------------------------
function GetApplicationIniFileName(aExeName: string = ''): string;
//--------------------------------------------------------
begin
  if aExeName='' then
    Result := ChangeFileExt(Application.ExeName, '.ini')
  else
    Result := GetApplicationDir + aExeName;


end;


procedure ExchangeInt(var aValue1,aValue2: integer);
var
  i: Integer;
begin
  i:=aValue1;
  aValue1:=aValue2;
  aValue2:=i;
end;


procedure ExchangeStr(var aValue1,aValue2: string);
var
  s: string;
begin
  s:=aValue1;
  aValue1:=aValue2;
  aValue2:=s;
end;


function GetComputerNetName: string;
var
  buffer: array[0..255] of char;
  size: dword;
begin
  size := 256;
  if GetComputerName(buffer, size) then
    Result := buffer
  else
    Result := ''
end;


function ConfirmDlg(aMsg: string): boolean;
begin
  Result:=(MessageDlg (aMsg, mtConfirmation, [mbYes,mbNo],0)=mrYes);
end;


function Eq_Any(aValue1: string; Value2: array of string): boolean;
var
  i: Integer;
begin
  Result:= false;

  for i := 0 to High(Value2) do begin
    if Eq(aValue1, Value2[i]) then begin
      Result:= true; exit;
    end;
  end;
end;


function Eq_Any_variant(aValue: variant; Value2: array of variant): boolean;
var
  i: Integer;
begin
  Result:= false;

  for i := 0 to High(Value2) do begin
    if Eq(AsString(aValue), AsString(Value2[i]) ) then begin
      Result:= true; exit;
    end;
  end;
end;


function String_Reverse1(aValue: String): String;
Var
 i : Integer;
Begin
  Result := '';
  For i := Length(aValue) DownTo 1 Do
    Result := Result + Copy(aValue,i,1) ;
end;


function DoubleQuotedStr(aValue: String): String;
Begin
  Result:='"'+aValue+'"';
end;


function StrArray (Values: array of string): TStrArray;
var i: integer;
begin
  SetLength (Result, High(Values)+1);
  for i:=0 to High(Values) do
    Result[i]:=Values[i];
end;


function StringListToStrArray(aStringList: TStringList): TStrArray;
var i: Integer;
begin
  SetLength (Result, aStringList.Count);
  for i:=0 to aStringList.Count-1 do
    Result[i]:=aStringList[i];
end;




function SArr(Values: array of string): TStrArray;
begin
  Result := StrArray(Values);
end;

function SArr(aValue1,aValue2: string): TStrArray;
begin
  Result := StrArray([aValue1,aValue2]);
end;



//--------------------------------------------------------
function StringToStringList_new(aValue: string; aDelimiter: char=';'): TStringList;
////--------------------------------------------------------
var
  i: Integer;
begin
  Result:=TStringList.Create;
  Result.Delimiter:=aDelimiter;
  Result.DelimitedText:=Trim(aValue);

  for I := 0 to Result.Count - 1 do
    Result[i] := Trim(Result[i]);
end;
         

//--------------------------------------------------------
function StringToStrArray_new(aValue: string; aDelimiter: char=';'): TStrArray;
////--------------------------------------------------------
var
  i: Integer;
  oSL: TStringList;
begin
  oSL:=StringToStringList(aValue, aDelimiter);

  SetLength(Result, oSL.Count);

  for I := 0 to oSL.Count - 1 do
    Result[i] := Trim(oSL[i]);


  FreeAndNil(oSL);  
end;



//--------------------------------------------------------
function StringToStringList(aValue: string; aDelimiter: char=';'): TStringList;
////--------------------------------------------------------
var
  i: Integer;
  iOffset: Integer;
  j: Integer;

  s: string;
  s1: string;
begin
  Assert(length(aDelimiter)>0);

  aValue := Trim(aValue);

  Result:=TStringList.Create;

 // iOffset:=1;

  iOffset:=1;

  repeat
    while (iOffset<Length(aValue)) and (aValue[iOffset] = aDelimiter) do
      Inc(iOffset);

    if (iOffset<=Length(aValue)) then
    begin
      j:=PosEx(aDelimiter, aValue, iOffset+1);
      if j=0 then
        j:=Length(aValue)+1;

      s1:=Trim(Copy(aValue,iOffset,j-iOffset));
      Result.Add(s1);

      iOffset :=j+1;
    end;
 // repeat



  until
     iOffset>Length(aValue);



 // StrToTxtFile(Result.Text, 'c:\123.txt');


//  ShowMessage(Result.Text);

//  Result.Delimiter:=aDelimiter[1];
 // Result.DelimitedText:=aValue;
end;


// ---------------------------------------------------------------
procedure ShowApplicationComponents;
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  s:='';

  for I := 0 to Application.ComponentCount - 1 do
  begin
    s:=s+ Format('%s-%s',
           [Application.Components[i].Name,
            Application.Components[i].ClassName ]) + sLineBreak;

  end;

  ShowMessage(s);
end;

//------------------------------------------------------------------
function Pos_(const aSubstr, aStr: string): boolean;
//------------------------------------------------------------------
begin
  Result:= Pos(AnsiUpperCase(aSubstr), AnsiUpperCase(aStr))>0;
end;


function MakeDoubleArray(aArr: array of double): TDoubleArray;
var I: Integer;
begin
  SetLength(Result, High(aArr)+1);

  for I := 0 to High(aArr) do
    Result[i] := aArr[i];

end;

// ---------------------------------------------------------------
procedure StrToFile_(aFileName: string; aStr: string);
// ---------------------------------------------------------------
//var
 // list: TstringList;        
//  oStream: TStringStream;    
begin
  IOUtils.TFile.WriteAllText(aFileName, aStr);


//
//  Assert(aFileName<>'');
//
//  ForceDirByFileName(aFileName);
//
//  oStream:=TStringStream.Create;
//  oStream.WriteString(aStr);
//  
//  oStream.SaveToFile(aFileName);
//  
//  FreeAndNil(oStream);

  
//  DeleteFile(aFileName);
//  IOUtils.TFile.AppendAllText(aFileName, s, TEncoding.GetEncoding(1251));
//   
  
//  
//  list:=TstringList.Create;
//  list.Text:=aStr;
//  list.SaveToFile (aFileName);
//  list.Free;

end;

// ---------------------------------------------------------------
function FileToStr(aFileName: string; var aStr: string): Boolean;
//function TextFileToStr(aFileName: string; var aStr: string): Boolean;
// ---------------------------------------------------------------
var 
  oList: TstringList;
  
  oStream: TStringStream;
    
begin
//    IOUtils.TFile.ReadAllText(


  Result := FileExists(aFileName);

  oStream:=TStringStream.Create;
  oStream.LoadFromFile(aFileName);
  

  aStr:=oStream.DataString;

    
  FreeAndNil(oStream);

//
//  oList:=TstringList.Create;
//  oList.LoadFromFile (aFileName);
//  aStr:=oList.Text;
//  oList.Free;

end;

//
//procedure AssertFileExists(aFileName: string);
//begin
//  Assert(FileExists(aFileName), 'File not exists: ' + aFileName);
//end;   


//procedure CreateChildForm_(var aReference; InstanceClass: TComponentClass;
//    aDestControl: TWinControl);
//begin
//  CreateChildForm(InstanceClass, aReference, aDestControl);
//end;


// -------------------------------------------------------------------
procedure CopyDocking1(aSrcForm: TForm; aDest: TWinControl);
// -------------------------------------------------------------------
var
  s: string;
begin
  Assert(aDest<>nil);

  s:=aDest.ClassName;

  if aDest.ClassName='TPanel' then
    TPanel(aDest).BevelOuter:=bvNone;


  aSrcForm.Align:=alClient;
  aSrcForm.ManualDock(aDest);
  aSrcForm.Show;
end;
        


//-------------------------------------------------------------------
//procedure CreateChildForm(InstanceClass: TComponentClass; var aReference; aDestControl: TWinControl);
procedure CreateChildForm_(InstanceClass: TComponentClass;
    var aReference;  aDestControl: TWinControl);
//-------------------------------------------------------------------
begin  
  CreateChildForm(aReference, InstanceClass,  aDestControl);


end;


function GetFileSize(const aFileName : string) : Int64;
var
  Reader: TFileStream;
begin
  Reader := TFile.OpenRead(aFileName);
  try
    result := Reader.Size;
  finally
    Reader.Free;
  end;
end;

        

//-------------------------------------------------------------------
//procedure CreateChildForm(InstanceClass: TComponentClass; var aReference; aDestControl: TWinControl);
procedure CreateChildForm(var aReference; InstanceClass: TComponentClass;  aDestControl: TWinControl);
//-------------------------------------------------------------------
var
  s: string;
  oInstance: TComponent;
begin  
  Assert(aDestControl<>nil);

  oInstance := TComponent(InstanceClass.NewInstance);
  TComponent(aReference) := oInstance;
  try
      oInstance.Create(aDestControl);   

  except
    TComponent(aReference) := nil;
    raise;
  end;


  CopyDocking1(TForm(aReference),aDestControl);

end;


//
//// ---------------------------------------------------------------
//function GetModuleName: string;
//// ---------------------------------------------------------------
//var
//  szFileName: array[0..MAX_PATH] of Char;
//begin
//  FillChar(szFileName, SizeOf(szFileName), #0);
//  //unit SysInit;
//  GetModuleFileName(hInstance, szFileName, MAX_PATH);
//  Result := szFileName;
//end;



// ---------------------------------------------------------------
function GetModuleFileName: string;
// ---------------------------------------------------------------
var
  szFileName: array[0..MAX_PATH] of Char;
begin

  Result :=System.SysUtils.GetModuleName(HInstance);
  
//
//  FillChar(szFileName, SizeOf(szFileName), #0);
//  GetModuleFileName( HInstance, szFileName, MAX_PATH);
//  Result := szFileName;
//
//  
end;


// ---------------------------------------------------------------
function SearchFileInPath(aFileName: string): string;
// ---------------------------------------------------------------
var
  SPPath:Array[0..255] of char;
  pPathPtr:PChar;
  
begin
  if SearchPath(nil, PChar(aFileName), nil, 255, SPPath, pPathPtr)>0 then 
    Result:=StrPas(SPPath)
  else 
    Result:=aFileName;
end;

//--------------------------------------------------------
function StringIsInArr(Value:string; Args: array of string): boolean;
//--------------------------------------------------------
var i:integer;
begin
  Result:=false;

  for I:=0 to High(Args) do
    if AnsiCompareText(Value,Args[I])=0 then
    begin
      Result:=true;
      break;
    end;
end;


//------------------------------------------------------------------------
function PosRight(aSubStr, aValue: string): Integer;
//------------------------------------------------------------------------
var
  k: Integer;
  s: string;
begin
//  PosEx()

  s:= ReverseString(aValue);
  k:=Pos(aSubStr,s);

  if k>0 then
    Result:=Length(aValue)-k+1
  else
    Result:=-1;          
  
                        
end;


//uses
//  System.Classes, System.IOUtils;



// ---------------------------------------------------------------
function CompareMemoryStreams(msOne,msTwo:TMemoryStream): boolean;
// ---------------------------------------------------------------
var
  i:Integer;
  p1,p2:LongInt;
  buffer1,buffer2:Char;
begin
  result := false;

  if msOne.size <> msTwo.size then
    Exit;

  try
    p1 := msOne.position; //temp storage for position
    msOne.position := 0; //start at the beginning
    p2 := msOne.position; //temp storage for position
    msTwo.position := 0; //start at the beginning

    for i := 0 to msOne.size-1 do
    begin

      if msOne.read(buffer1,sizeOf(buffer1)) <> msTwo.read(buffer2,sizeOf(buffer2)) then
        exit;

      if buffer1 <> buffer2 then
        exit;

    end;
      result := true;
  finally
  end;
end;


//---------------------------------------------------------------------
function TranslitRus2Lat(const aStr: string): string;
//---------------------------------------------------------------------
const
  RArrayL = '��������������������������������';
  RArrayU = '�����Ũ��������������������������';
  colChar = 33;
  arr: array[1..2, 1..ColChar] of string =
  (('a', 'b', 'v', 'g', 'd', 'e', 'yo', 'zh', 'z', 'i', 'y',
    'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f',
    'kh', 'ts', 'ch', 'sh', 'shch', '''', 'y', '''', 'e', 'yu', 'ya'),
    ('A', 'B', 'V', 'G', 'D', 'E', 'Yo', 'Zh', 'Z', 'I', 'Y',
    'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F',
    'Kh', 'Ts', 'Ch', 'Sh', 'Shch', '''', 'Y', '''', 'E', 'Yu', 'Ya'));
var
  i: Integer;
  LenS: Integer;
  p: integer;
  d: byte;
begin
  result := '';
  LenS := length(aStr);

  for i := 1 to lenS do
  begin
    d := 1;
    p := pos(aStr[i], RArrayL);
    if p = 0 then
    begin
      p := pos(aStr[i], RArrayU);
      d := 2
    end;
    if p <> 0 then
      result := result + arr[d, p]
    else
      result := result + aStr[i]; //���� �� ������� �����, �� ����� ��������
  end;
end;


var
  I: Integer;
  v: Variant;
  e: double;

  sDir : string;
  

initialization
//  sDir:=IncludeTrailingBackslash(ExtractFileDir(Application.ExeName));


  FormatSettings.DecimalSeparator:='.';


  Randomize; // to make random color


end.


