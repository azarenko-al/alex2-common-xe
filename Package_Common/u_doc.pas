unit u_doc;

interface
uses ComObj, Dialogs, Variants;


procedure Rtf_to_Doc(aFileName_RTF, aFileName_DOC: string);

implementation

procedure Rtf_to_Doc(aFileName_RTF, aFileName_DOC: string);
var
  WordApp: Variant;
begin
  try
    WordApp := CreateOleObject('Word.Basic');

    if not VarIsEmpty(WordApp) then
    begin
      WordApp.FileOpen(aFileName_RTF);
      WordApp.FileSaveAs(Name := aFileName_DOC, Format := 0);
//      WordApp.FileSaveAs(Name := aFileName_RTF, Format := 6);

      WordApp.AppClose;
      WordApp := Unassigned;
   //   WordEditor.Lines.LoadFromFile('c:\temp_bb.rtf');
    end
    else
      ShowMessage('Could not start MS Word');

  finally
    WordApp:=null;
  end;
  // TODO -cMM: Rtf_to_Doc default body inserted
end;

end.


{


Format      File 
Argument    Format 
--------    ------ 

   0        ���������� (������ Word)
   1        ������ ���������
   2        ������ ����� (extended characters saved in ANSI character set) 
   3        �����+�������� (plain text with line breaks; extended 
            characters saved in ANSI character set) 
   4        ������ ����� (PC-8) (extended characters saved in IBM PC 
            character set) 
   5        �����+�������� (PC-8) (text with line breaks; extended 
            characters saved in IBM PC character set) 
   6        ������ Rich-text (RTF) 


   
procedure TImport_Form.ToolButton3Click(Sender: TObject);
var
  WordApp: Variant;
begin
  if OpenDialog1.Execute then
  begin
    Edit1.Text := ExtractFileName(OpenDialog1.FileName);
    StatusBar1.SimpleText := OpenDialog1.FileName;
    WordApp := CreateOleObject('Word.Basic');
    if not VarIsEmpty(WordApp) then
    begin
      WordApp.FileOpen(OpenDialog1.FileName);
      WordApp.FileSaveAs(Name := 'c:\temp_bb.rtf', Format := 6);
      WordApp.AppClose;
      WordApp := Unassigned;
      WordEditor.Lines.LoadFromFile('c:\temp_bb.rtf');
    end
    else
      ShowMessage('Could not start MS Word');
  end;
end;

}