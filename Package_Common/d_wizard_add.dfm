inherited dlg_Wizard_add: Tdlg_Wizard_add
  Left = 623
  Top = 225
  Width = 513
  Height = 407
  ActiveControl = ed_Name_
  Caption = 'dlg_Wizard_add'
  Constraints.MinHeight = 371
  Constraints.MinWidth = 446
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 338
    Width = 505
    inherited Bevel1: TBevel
      Width = 505
    end
    inherited Panel3: TPanel
      Left = 309
      Width = 196
    end
  end
  inherited pn_Top_: TPanel
    Width = 505
    Height = 57
    Constraints.MinHeight = 57
    inherited Bevel2: TBevel
      Top = 54
      Width = 505
      Shape = bsBottomLine
    end
    inherited pn_Header: TPanel
      Width = 505
      Height = 55
      Constraints.MaxHeight = 55
      Constraints.MinHeight = 55
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 57
    Width = 505
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Constraints.MaxHeight = 50
    Constraints.MinHeight = 50
    TabOrder = 2
    DesignSize = (
      505
      50)
    object lb_Name: TLabel
      Left = 5
      Top = 3
      Width = 31
      Height = 13
      Caption = 'Name:'
    end
    object ed_Name_: TEdit
      Left = 5
      Top = 22
      Width = 495
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
    end
  end
  inherited ActionList1: TActionList
    Left = 388
  end
  inherited FormStorage1: TFormStorage
    Left = 360
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 415
  end
end
